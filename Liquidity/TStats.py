#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# TStats 
#
# Module comprising plot and helper functions
#############################################################################################      


#Common Libraries
import numpy as np
import pandas as pd
import time

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path+"/")

import Plib.Utils.Tools as tls


    
def getMarginStats(link=''):
    df=tls.getExcelFromUrl(link,'Customer Margin Balances',1,reqd=True,save_dir='//Users/rob',myname='FINRA')
    df.columns=['Date','Debit in Margin Accounts','Free Credit in Cash Accounts','Free Credit in Margin Accounts']
    df=df[['Date','Debit in Margin Accounts','Free Credit in Margin Accounts','Free Credit in Cash Accounts']]
    df=df.set_index('Date')
    df = df.sort_index(ascending=True)
    df=df.reset_index()
    df['Date'] = pd.to_datetime(df['Date']).dt.normalize()
    df.set_index('Date',inplace=True)
    return df
    
#def getMarginStats():
#    import time
#    from selenium import webdriver
#    from selenium.webdriver.chrome import service
#    from datetime import datetime
#    from selenium.webdriver.common.by import By
#    import pandas as pd

    # chmod 0755 operadriver
#    path2operad=tls.getPath2OperaDrv()
#    wait=1
#    webdriver_service = service.Service(path2operad)
#    webdriver_service.start()
#    driver = webdriver.Remote(webdriver_service.service_url, webdriver.DesiredCapabilities.OPERA)

#    driver.get('https://www.finra.org/investors/learn-to-invest/advanced-investing/margin-statistics')
#    input_txt = driver.page_source
#    time.sleep(wait) #see the result

#    Cols=['Date', 'Debit in Margin Accounts', 'Free Credit in Margin Accounts', 'Free Credit in Debit Accounts']
#    df=pd.DataFrame(columns=Cols)

#    for index in range(9):
#        for x in range(12):
#            print(str(x+1))
#            table = driver.find_elements_by_xpath("//div[@id='block-finra-bootstrap-sass-system-main']/div/article/div/div/div[4]/div/div/div/div/div/table/tbody/tr[" + str(x+1)+"]")
#            myrow=table[index].text.split('\n')
#            r=myrow[0].split(' ')
#            print(r[0])
#            d=datetime.strptime(str(r[0]).replace("June", "Jun").replace("Sept", "Sep"), '%b-%y')
#            df.loc[x+(12*(index))] = [d, float(r[1].replace(',', '')), float(r[2].replace(',', '')), float(r[3].replace(',', ''))]
#    driver.quit()  
#    df=df.set_index('Date')
#    df = df.sort_index(ascending=True)
#    return df
     
def getLargestMutualFunds():
    import time
    from selenium import webdriver
    from selenium.webdriver.chrome import service
    from datetime import datetime
    from selenium.webdriver.common.by import By
    import pandas as pd
    import Plib.Utils.Tools as t

    # chmod 0755 operadriver
    path2operad=tls.getPath2OperaDrv()
    wait=1
    webdriver_service = service.Service(path2operad)
    webdriver_service.start()
    driver = webdriver.Remote(webdriver_service.service_url, webdriver.DesiredCapabilities.OPERA)

    #Test in chrome console
    #$x("//div[@id='FundTable']/div[2]/table/tbody/tr")
    driver.get('https://markets.on.nytimes.com/research/markets/mutualfunds/mutualfunds.asp')
    input_txt = driver.page_source
    time.sleep(wait) #see the result

    Cols=['Ticker', 'Mutual Fund']
    df=pd.DataFrame(columns=Cols)
    for i in range(20):
        mysearch="//div[@id='FundTable']/div[2]/table/tbody/tr[" + str(i+1) + "]/td"
        table = driver.find_elements_by_xpath(mysearch)
        myrow=table[0].text.split('\n')
        try:
            #print(myrow[0])
            ticker=t.getTickerByGoogleSearch(myrow[0])
            #print(ticker)
            df.loc[i] = [ticker,str(myrow[0].replace('®', ''))]
        except:
            print('')
    return df
    
def getICIfundsFlows(link='',r1=9,r2=42,r3=44,r4=52,debug=False):
    import requests
    import xlrd
    import urllib
    import shutil
    import datetime
    now = datetime.datetime.now()

    if link == '':
        try:
            link = 'https://www.ici.org/system/files/private/'+ str(now.year) +'-'+ str('%02d' % now.month) +'/combined_flows_data_'+ str(now.year) +'.xls'
        except:
            link = 'https://www.ici.org/system/files/private/'+ str(now.year-1) +'-12/combined_flows_data_'+ str(now.year-1) +'.xls'
    
    #df=tls.getExcelFromUrl(link,'Customer Margin Balances',1,reqd=True,save_dir='//Users/rob',myname='ICI')    
    file_name, headers = urllib.request.urlretrieve(link)
    workbook = xlrd.open_workbook(file_name)
    
    mdate=str(datetime.date.today()).replace('-','')
    shutil.copyfile(file_name, '//Users/rob/ICI'+mdate+'.xls')
    #sheet_names = workbook.sheet_names()
    #print('Sheet Names', sheet_names)
    
    xl_sheet = workbook.sheet_by_index(0)
    row = xl_sheet.row(0)  # 1st row
    from xlrd.sheet import ctype_text 
    Cols=['Date', 'Total MF + ETF','Equity Total','Equity Domestic','Equity World',
          'Hybrid','Bond Total','Bond Taxable','Muni','Commodity']
    df1=pd.DataFrame(columns=Cols)
    df2=pd.DataFrame(columns=Cols)
    
    #Monthly flows: rows 8-42 cols 0-18
    for row_idx in range(r1, r2):    # Iterate through rows
        try:
            for col_idx in range(0, 18):  # Iterate through columns
                cell_obj = xl_sheet.cell(row_idx, col_idx)  # Get cell object by row, col
                if debug: print ('Row: [%s] Column: [%s] cell_obj: [%s]' % (row_idx,col_idx, cell_obj))
                if col_idx == 0:
                    date=datetime.datetime.strptime(str(cell_obj.value).replace(' ',''), '%m/%d/%Y')
                if col_idx == 1:
                    v1=cell_obj.value
                if col_idx == 3:
                    v2=cell_obj.value
                if col_idx == 5:
                    v3=cell_obj.value
                if col_idx == 7:
                    v4=cell_obj.value
                if col_idx == 9:
                    v5=cell_obj.value
                if col_idx == 11:
                    v6=cell_obj.value
                if col_idx == 13:
                    v7=cell_obj.value
                if col_idx == 15:
                    v8=cell_obj.value
                if col_idx == 17:
                    v9=cell_obj.value
            df1.loc[row_idx-8] = [date,v1,v2,v3,v4,v5,v6,v7,v8,v9]   
        except:
            pass;
            
    #Weekly flows: rows 45-50 cols 0-18
    for row_idx in range(r3, r4):    # Iterate through rows
        try:
            for col_idx in range(0, 18):  # Iterate through columns
                cell_obj = xl_sheet.cell(row_idx, col_idx)  # Get cell object by row, col
                if debug: print ('Row: [%s] Column: [%s] cell_obj: [%s]' % (row_idx,col_idx, cell_obj))
                if col_idx == 0:
                    date=datetime.datetime.strptime(str(cell_obj.value).replace(' ',''), '%m/%d/%Y')
                if col_idx == 1:
                    v1=cell_obj.value
                if col_idx == 3:
                    v2=cell_obj.value
                if col_idx == 5:
                    v3=cell_obj.value
                if col_idx == 7:
                    v4=cell_obj.value
                if col_idx == 9:
                    v5=cell_obj.value
                if col_idx == 11:
                    v6=cell_obj.value
                if col_idx == 13:
                    v7=cell_obj.value
                if col_idx == 15:
                    v8=cell_obj.value
                if col_idx == 17:
                    v9=cell_obj.value
            df2.loc[row_idx-8] = [date,v1,v2,v3,v4,v5,v6,v7,v8,v9]   
        except:
            pass;
            
    df1=df1.set_index('Date')
    df1.index = pd.to_datetime(df1.index)
    df1=df1.sort_index(ascending=True)
    df2=df2.set_index('Date')
    df2.index = pd.to_datetime(df2.index)
    df2=df2.sort_index(ascending=True)
    
    return df1,df2