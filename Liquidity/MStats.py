#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# MStats 
#
# Module comprising plot and helper functions
#############################################################################################      


#Common Libraries
import numpy as np
import pandas as pd
import time

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path+"/")

import Plib.Brokers.IBapi as ib

import warnings
warnings.filterwarnings("once")

TTADExch=['AMEX','NASD','NDX','NYSE','ARCA','BATS','TSE']
VolExch=['AMEX','NASD','NDX','NYSE']

def getMSExchanges():
    df=pd.DataFrame(TTADExch)
    df.columns=['Exchange']
    df['Tick Trin AD']=1
    df['Vol']=0
    df.loc[0,'Vol'] = 1
    df.loc[1,'Vol'] = 1
    df.loc[2,'Vol'] = 1
    df.loc[3,'Vol'] = 1
    return df

def getMStatsByExchange(exchange='AMEX',currency='USD',duration='3 Y',stype=0,ib_link='STT'):
    import pandas as pd
    
    port=ib.IBConn[ib_link][1]
    ip=ib.IBConn[ib_link][2]
    client=ib.IBConn[ib_link][0]
    
    app=ib.ibConnect(port,ip,client)
    
    if stype==0:
        msym='TICK'
    elif stype==1:
        msym='TRIN'
    elif stype==2:    
        msym='AD'
    elif stype==3:    
        msym='VOL'
    else:
        df=pd.DataFrame()
        app.disconnect()
        return df
    
    #assign id
    id_index=1
    
    #Sleep interval to allow time for connection to server
    time.sleep(1) 
    
    #Create contract object and get data
    try:
        #index_contract = ib.getIndex(symbol=str(msym) + '-' + str(exchange),secType='IND',exchange=exchange,currency=currency)
        #df=ib.getDailyData(id_index,index_contract,duration=duration,bartype='TRADES')
        #ret2=df.copy().set_index('Date').resample('D').mean().dropna()
        #ret2=ret2[['Open','High','Low','Close','Adjusted_close','Volume']]
        #ret2=ret2.reset_index()
        #df=ret2
        df=ib.getIndexHData(id_index,symbol=msym,exchange=exchange,cur=currency,duration=duration)
    except:
        df=pd.DataFrame()
    app.disconnect()
    return df

def getMTick(exchange='AMEX'):
    df=getMStatsByExchange(exchange,stype=0)
    return df

def getMTrin(exchange='AMEX'):
    df=getMStatsByExchange(exchange,stype=1)
    return df

def getMAD(exchange='AMEX'):
    df=getMStatsByExchange(exchange,stype=2)
    return df

def getMVolume(exchange='AMEX'):
    df=getMStatsByExchange(exchange,stype=3)
    return df
    
def get_MarketVolume(series):
    from datetime import datetime

    #Daily U.S Equity Matched Volumes (millions, includes CS2)
    url_NYSE = 'https://www.nyse.com/publicdocs/nyse/US_Equities_Volumes.csv'
    
    if series == 'NYSE':
        #Trade Date	Nyse Tape A	Nyse Arca Tape A	Nyse American Tape A	Nyse National Tape A	
        #Consolidated Tape A	Nyse Tape B	Nyse Arca Tape B	Nyse American Tape B	
        #Nyse National Tape B	Consolidated Tape B	Nyse Tape C	Nyse Arca Tape C	
        #Nyse American Tape C	Nyse National Tape C	Consolidated Tape C
        url = url_NYSE
        data = pd.read_csv(url)
        
        do = datetime.strptime(str(data['Trade Date'].iloc[0]), '%Y%m%d')
        data['Date']=do
        data['Cons_Tape_A']=0
        data['Cons_Tape_B']=0
        data['Cons_Tape_C']=0
        data['Total_Cons_Tapes']=0
        row=0
        while row <= len(data)-1:
            r=datetime.strptime(str(data['Trade Date'].iloc[row]), '%Y%m%d')
            fa = float(str(data['Consolidated Tape A'].iloc[row]).replace(',', ''))
            fb = float(str(data['Consolidated Tape B'].iloc[row]).replace(',', ''))
            fc = float(str(data['Consolidated Tape C'].iloc[row]).replace(',', ''))
            data['Date'].iloc[row]=r
            data['Cons_Tape_A'].iloc[row]=fa
            data['Cons_Tape_B'].iloc[row]=fb
            data['Cons_Tape_C'].iloc[row]=fc
            data['Total_Cons_Tapes'].iloc[row]=fa+fb+fc
            row=row+1
        del data['Trade Date']
        del data['Consolidated Tape A']
        del data['Consolidated Tape B']
        del data['Consolidated Tape C']
        data=data.set_index('Date')
        data = data.sort_index(ascending=True)
        #data = data.dropna()
    return data
