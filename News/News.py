#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# News 
#
# Module to collect news from email and rss feeds
#############################################################################################      

import pandas as pd

#IMAP Configuration for news mailbox
imap_host = 'imap.mail.me.com'
imap_user = 'rg.iphone.email@icloud.com'
imap_pass = 'mbxo-bdho-olat-ptvr'
EMAIL_FOLDER = "News"
max_body_size=25000
max_sub_size=1000
max_source_size=50
max_id_size=150


#Feeds
rss_data=[['Bankruptcy News','http://bankruptcy.einnews.com/rss/aAb0IZY04xu6LRWW','uid','pwd'],
['Banker Data','http://www.thebanker.com/layout/set/rss/Banker-Data','uid','pwd'],
['Reports','http://www.thebanker.com/layout/set/rss/Reports','uid','pwd'],
['World','http://www.thebanker.com/layout/set/rss/World','uid','pwd'],
['Commodities articles','https://www.thestreet.com/topic/45581/rss.html','uid','pwd'],
['Platts','https://www.platts.com/RSSFeedDetail/xml/whatsnewrss','uid','pwd'],
['ETF','http://www.etf.com/home.feed','uid','pwd'],
['Economy','http://www.cnbc.com/id/20910258/device/rss/rss.html','uid','pwd'],
['WSJ.com: World News','https://feeds.a.dj.com/rss/RSSWorldNews.xml','uid','pwd'],
['WSJ.com: Business','https://feeds.a.dj.com/rss/WSJcomUSBusiness.xml','uid','pwd'],
['WSJ.com: World Markets','https://feeds.a.dj.com/rss/RSSMarketsMain.xml','uid','pwd'],
['Reuters: Business News','https://www.reuters.com/news/archive/rates-rss','uid','pwd'],
['Finance','http://www.cnbc.com/id/10000664/device/rss/rss.html','uid','pwd'],
['DailyFX - Forex Market News','https://rss.dailyfx.com/feeds/forex_market_news','uid','pwd'],
['Mergers Acquisitions News','http://mergersacquisitions.einnews.com/rss/AWkdE3SIOdATY7Vk','uid','pwd'],
['CBOE-TV RSS Feeds','http://www.cboe.com/rss/RSSCBOETV.aspx','uid','pwd'],
['Real Estate News','https://realestate.einnews.com/rss/WOYpfvRg7H2J5rSV','uid','pwd'],
['Dow Jones Indices - Hedge Funds','https://us.spindices.com/rss/rss-details/?rssFeedName=hedge-funds-traders','uid','pwd'],
['Dow Jones Indices - Research','https://us.spindices.com/rss/rss-details/?rssFeedName=research','uid','pwd'],
['Dow Jones Indices - Strategy','https://us.spindices.com/rss/rss-details/?rssFeedName=strategy','uid','pwd'],
['Switzerland','https://world.einnews.com/rss/JTy1EpeYR-Dqr2_w','uid','pwd'],
['Feed for all opinions','https://www.tradingfloor.com/opinions/rss/extract','uid','pwd'],
['Feed for all trade views','https://www.tradingfloor.com/trade-views/rss/extract','uid','pwd']]

##############################################
# Store and retrieve nrecords and last update
##############################################
def storeParam(param,fname='mailbox_news.param', dname3='SSD/sent',ddate='2020-10-01'):
    import os, sys, os.path
    import pandas as pd
    import datetime as dt
    
    dirName2 = '//Volumes/'+ dname3
    
    with open(dirName2 + '/' + fname, 'w') as writer:
        writer.write(str(param)+'\n')
        writer.write(ddate)

def readParam(fname='mailbox_news.param', dname3='SSD/sent'):
    import os, sys, os.path
    import pandas as pd
    
    dirName2 = '//Volumes/'+ dname3
    if os.path.isfile(dirName2 + '/' + fname):
        with open(dirName2 + '/' + fname, 'r') as reader:
            line1 = int(reader.readline())
            line2 = str(reader.readline())
    else:
        line1=0
        line2='2000-01-01'
    return line1,line2

##############################################
# Store data on HDF file
##############################################
def saveData(df,dc,delete=False, fname='mailbox', dname3='SSD/sent',ddate='2020-10-01'):
    import os, sys, os.path
    import pandas as pd
    from shutil import copyfile
    import datetime
    
    dirName2 = '//Volumes/'+ dname3
    fname1=dirName2 + '/' + fname + '.hdf'
    
    #command2="osascript -e 'mount volume \"smb://IPA/storage/DNAME\"'"
    #command2=command2.replace('DNAME',dname2).replace('IPA',ipa)
    #os.system(command2)

    #print(fname1)
    # Print the files
    if not os.path.isfile(fname):
        if delete:
            try:
                os.remove(fname1)
            except:
                print('')
            #init store recordset
            #mid="x" * max_id_size
            msub = "x" * max_sub_size
            mbody = "x" * max_body_size
            msource = "x" * max_source_size
            mmdate = datetime.datetime.strptime('2000-01-01', '%Y-%m-%d')
            df1 = pd.DataFrame()
            df1 = df1.append([[0,msource, mmdate,msub,mbody, 1]])
            df1.columns=dc
            df1.set_index('id',inplace=True)
            #print(df1)
            df1.to_hdf(fname1, key='id', format='table', mode='w',data_columns=dc)
            #df.to_hdf(fname1, key='id', format='table', mode='w',data_columns=dc)
            df.to_hdf(fname1, key='id', append=True, mode='r+', format='t')
            storeParam(len(df),fname=fname+'.param', dname3=dname3, ddate=ddate)
        else:
            df.to_hdf(fname1, key='id', append=True, mode='r+', format='t')
            recs,vv=readParam(fname=fname+'.param', dname3=dname3)
            storeParam(len(df)+int(recs),fname=fname+'.param', dname3=dname3, ddate=ddate)
    else:
        print('Already Processed: ',fname1)
    #except:
        #print('')
    print('Mails saved.')
 
##############################################
# Retrieve Feeds from internet
##############################################    
def parseRssFeeds(myfeed,max_date,maxno=5,count=0):
    import urllib, feedparser
    from datetime import datetime
    from time import mktime
    from bs4 import BeautifulSoup
    import pandas as pd

    columns=['id','feed', 'Date', 'Subject','body','relevant']
    df = pd.DataFrame()
         
    try:
        if myfeed[2]!='uid':
            #auth = urllib.request.HTTPDigestAuthHandler()
            auth = urllib.request.HTTPBasicAuthHandler()
            auth.add_password('BasicTest', myfeed[0], myfeed[2], myfeed[3])
            NewsFeed = feedparser.parse(myfeed[1],handlers=[auth])
            #print(ms[0],d.status)
        else:
            NewsFeed = feedparser.parse(myfeed[1])
            #print(ms[0],d.status)
        if len(NewsFeed.entries)>0:
            entry = NewsFeed.entries[1]
            #print('      Entry 1: ',entry.title)
            #entry.id
        else:
           print('       No feeds...') 
    except:
        print('No feed for ',myfeed[0])
        return df,columns
            
                
    #try:
    #    NewsFeed = feedparser.parse(myfeed[1])
    #except:
    #    print('No feed')
    #    return df,columns
    
    print('Sync Feed :', myfeed[0],len(NewsFeed.entries),NewsFeed.status)
    
    if len(NewsFeed.entries)==0:
        #df.columns=columns
        #df.set_index('id',inplace=True)
        return df,columns   
    
    entry = NewsFeed.entries[1]
    index=count
    
    for entry in NewsFeed.entries:
        #print('Post Title :',entry.title)
        dt = datetime.fromtimestamp(mktime(entry.published_parsed))
        
        #print('Date: ', dt)
        #print('Text: ',entry.summary)
        remove_html=''.join(BeautifulSoup(entry.summary, "html.parser").stripped_strings)
        
        if dt > max_date:
            df = df.append([[index,myfeed[0],dt,entry.title,remove_html,1]])
        index=index+1
        if index > maxno:
            if len(df) >0:
                df.columns=columns
                df.set_index('id',inplace=True)
            return df,columns
    if len(df) >0:
        df.columns=columns
        df.set_index('id',inplace=True)
    return df,columns


##############################################
# Connect to Server Imap
##############################################    
def connectImap():
    from imap_tools import MailBox, AND
    
    mailbox = MailBox(imap_host)
    mailbox.login(imap_user, imap_pass, initial_folder=EMAIL_FOLDER)  

    return mailbox

##############################################
# Retrieve Emails from internet
##############################################    
def process_mailbox(mailbox,new=True, maxno=5,count=0,md='2020-10-10'):
    """
    Dump all emails in the folder to files in output directory.
    """

    from imap_tools import MailBox, AND
    import datetime
    import pandas as pd
    
    ma=md.split('-')
    flags = AND(seen=True, flagged=False,date=datetime.date(int(ma[0]), int(ma[1]), int(ma[2])))
    if new:
        flags = AND(seen=False, flagged=False,date=datetime.date(int(ma[0]), int(ma[1]), int(ma[2])))
    
    #with mybox as mailbox:
    columns=['id','email', 'Date','Subject','body', 'Relevant']
    df = pd.DataFrame()
    
    index=count
    emails=mailbox.fetch(flags)
    #print('Output: ',(emails))

    for msg in emails:
        #print('Retrieved Message: ', msg.uid)              # str or None: '123'
        #print(msg.subject)          # str: 'some subject 你 привет'
        #print(msg.from_) 
        #print(msg.date)             # datetime.datetime: 1900-1-1 for unparsed, may be naive or with tzinfo
        #print(msg.date_str)         # str: original date - 'Tue, 03 Jan 2017 22:26:59 +0500'
        #print(msg.text)             # str: 'Hello 你 Привет'

        try:
            mdate=msg.date
            #mdate=mdate1.strftime('%Y-%m-%d')
            
            bodytext=msg.text
            bodytext[0:max_body_size]
            index=index+1

            df = df.append([[index,msg.from_, mdate,msg.subject,bodytext, 1]])
        except:
            print('')
        if index > maxno:
            if len(df) >0:
                df.columns=columns
                df.set_index('id',inplace=True)
            print('Searched:', flags)
            return df,columns
    #mailbox.close()
    #mailbox.logout()
    if len(df) >0:
        df.columns=columns
        df.set_index('id',inplace=True)
    print('Searched:', flags)
    return df,columns
    
##############################################
# Update Feeds from internet
##############################################    
def updateFeeds(firstRun,start_date):
    #First Run: delete feeds_news.param and set overwrite=True
    #Update: set overwrite=False
    import datetime
    
    store='feeds_news'
    n_rec,mdate=readParam(fname=store + '.param', dname3='SSD/sent')
    #t_date=datetime.datetime.strptime(mdate, '%Y-%m-%d')
    df = pd.DataFrame()
    max_records=300
    overwrite=False
    if firstRun:
        overwrite=True
        n_rec=0
        
    for ms in rss_data:
        temp,col=parseRssFeeds(ms,start_date,maxno=max_records,count=n_rec)
        df=df.append(temp)
        n_rec=n_rec+len(temp)
    #df['date'] = pd.to_datetime(df["Date"].dt.strftime('%Y-%m-%d'))
    #str(datetime.datetime.now().strftime("%Y-%m-%d"))
    saveData(df,col,overwrite,fname=store,ddate=start_date.strftime("%Y-%m-%d"))
    return len(df)  
    
##############################################
# Update Emails from internet
##############################################        
def updateEmails(firstRun=False,days_interval=30):    
    #First Run: n_rec=0 max_records=4000 overwrite=True only_new=False
    #Update: n_rec=FirstRun max_records=4000 overwrite=False only_new=True
    import datetime
    from datetime import date, timedelta
    
    store='mailbox_news'
    n_rec,md=readParam(fname=store + '.param', dname3='SSD/sent')
    max_records=600
    only_new=False
    overwrite=False
    if firstRun:
        overwrite=True
        n_rec=0
    
    mb=connectImap()
    start_date = datetime.datetime.strptime(md, '%Y-%m-%d')
    end_date = datetime.datetime.strptime(md, '%Y-%m-%d') + timedelta(days=days_interval)
    delta = timedelta(days=1)
    while start_date <= end_date:
        try:
            df,columns=process_mailbox(mb,new=only_new, maxno=max_records,count=n_rec,md=start_date.strftime("%Y-%m-%d"))
            start_date += delta
            if len(df) > 0: 
                df['Date'] = pd.to_datetime(df["Date"], utc=True)
                saveData(df,columns,overwrite,fname=store,ddate=str(start_date.strftime("%Y-%m-%d")))
            else:
                n_rec,md=readParam(fname=store + '.param', dname3='SSD/sent')
                storeParam(n_rec,fname=store+'.param', dname3='SSD/sent', ddate=str(start_date.strftime("%Y-%m-%d")))
        except:
            print('Error while downloading messages....')
    mb.logout()
    
##############################################
# Read stored data from hdf file
##############################################        
def getdata(ddate,fname='feeds_news',dname2='SSD/sent'):
    import os, sys, os.path
    import pandas as pd
    
    dirName2 = '//Volumes/'+ dname2
    
    ssymbol='Date>="A"'
    hdf_data=pd.read_hdf(dirName2 + '/' + fname+ '.hdf', key='id',where = [ssymbol.replace('A',ddate)])
    hdf_data['Date'] = pd.to_datetime(hdf_data.Date)
    hdf_data=hdf_data.drop_duplicates(subset=['Date', 'Subject'], keep=False)

    return hdf_data
 