#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# CBOE 
#
# Module comprising plot and helper functions
#############################################################################################      


#Common Libraries
import numpy as np
import pandas as pd

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path+"/")
#import seaborn

# Need to import the matplotlib_converters from pandas,
# whenever you try to plot the columns of a dataframe using a for loop
#from pandas.plotting import register_matplotlib_converters
#register_matplotlib_converters()

import matplotlib.style
import matplotlib as mpl
#mpl.style.use('classic')
#Gridstyle
mpl.rcParams['grid.color'] = 'k'
mpl.rcParams['grid.linestyle'] = ':'
mpl.rcParams['grid.linewidth'] = 0.5
mpl.rcParams['axes.grid'] = True
#Resolution
mpl.rcParams['figure.dpi'] = 80
mpl.rcParams['savefig.dpi'] = 100

##############################################
# Options for Jupyter
##############################################      
#This would display the graph in the notebook, but it was no longer interactive.
#%matplotlib inline
#With this the figures will now show up in the notebook , and still be interactive. 
#%matplotlib notebook


##############################################
# CBOE series data download functions
##############################################
def filterFutCols(futures):
    import datetime as dt

    f1 = futures[(futures['Close'] != 0) & (futures['Settle'] != 0) & (futures['Total Volume'] != 0) & (
                futures['Open Interest'] != 0)]
    f2 = f1.reset_index(drop=True)
    f3 = f2.groupby([f2['Group']], as_index=False).mean()
    f3 = f3.sort_values(by='Group', ascending=True)
    f3.columns = ['Future', 'Open', 'High', 'Low', 'Close', 'Settle', 'Change']
    return f3

def getThirdWednesday(year, month):
    import calendar

    c = calendar.Calendar(firstweekday=calendar.SUNDAY)
    monthcal = c.monthdatescalendar(year, month)
    third_friday = \
    [day for week in monthcal for day in week if day.weekday() == calendar.WEDNESDAY and day.month == month][2]
    return third_friday

def getDatesFutures():
    import calendar
    import datetime as dt
    from dateutil.relativedelta import relativedelta

    today = dt.date.today()
    index = 0
    df = pd.DataFrame([], columns=['Date'])
    for i in range(-12, 8):
        mydate = today + relativedelta(months=i)
        year = mydate.year
        month = mydate.month
        # print(month,year)
        df.loc[index] = getThirdWednesday(year, month).strftime("%Y-%m-%d")
        index = index + 1
    df.sort_values(by='Date', ascending=True)

    return df

def get_CBOE(series):
               #https://ww2.cboe.com/publish/scheduledtask/mktdata/datahouse/implied_correlation_hist.csv
    url_VVIX = 'https://ww2.cboe.com/publish/scheduledtask/mktdata/datahouse/vvixtimeseries.csv'
    url_VIX = 'https://ww2.cboe.com/publish/scheduledtask/mktdata/datahouse/vixcurrent.csv'
    url_VIX3 = 'https://ww2.cboe.com/publish/scheduledtask/mktdata/datahouse/vix3mdailyprices.csv'
    url_VXN = 'https://ww2.cboe.com/publish/ScheduledTask/MktData/datahouse/vxncurrent.csv'
    url_SKEW = 'https://ww2.cboe.com/publish/scheduledtask/mktdata/datahouse/skewdailyprices.csv'
    url_IC = 'https://ww2.cboe.com/publish/scheduledtask/mktdata/datahouse/implied_correlation_hist.csv'
    #https://markets.cboe.com/us/futures/market_statistics/historical_data/products/csv/VX/2021-05-19/
    url_FUTURES = 'https://markets.cboe.com/us/futures/market_statistics/historical_data/products/csv/VX/'
    url_PCR_ETP='https://ww2.cboe.com/publish/scheduledtask/mktdata/datahouse/etppc.csv'
    url_PCR_Equity='https://ww2.cboe.com/publish/scheduledtask/mktdata/datahouse/equitypc.csv'
    url_PCR_VIX='https://ww2.cboe.com/publish/scheduledtask/mktdata/datahouse/vixpc.csv'
    url_PCR_Total='https://ww2.cboe.com/publish/scheduledtask/mktdata/datahouse/totalpc.csv'
    
    if series == 'PCR_TOTAL':
        #DATE,CALLS,PUTS,TOTAL,P/C Ratio
        url = url_PCR_Total
        data = pd.read_csv(url)
        data2 = data
        data2['Date'] = data.iloc[2:, 0]
        data2['Date'] = pd.to_datetime(data2['Date'])
        data2['Calls'] = data.iloc[2:, 1]
        data2['Puts'] = data.iloc[2:, 2]
        data2['Total'] = data.iloc[2:, 3]
        data2['PCR'] = data.iloc[2:, 4]
        data = data2[['Date', 'Calls', 'Puts', 'Total', 'PCR']]
        data['Calls'] = data.Calls.astype(float)
        data['Puts'] = data.Puts.astype(float)
        data['Total'] = data.Total.astype(float)
        data['PCR'] = data.PCR.astype(float)
        data = data.set_index('Date')
        data = data.dropna()
    elif series == 'PCR_ETP':
        #DATE,CALLS,PUTS,TOTAL,P/C Ratio
        url = url_PCR_ETP
        data = pd.read_csv(url)
        data2 = data
        data2['Date'] = data.iloc[2:, 0]
        data2['Date'] = pd.to_datetime(data2['Date'])
        data2['Calls'] = data.iloc[2:, 1]
        data2['Puts'] = data.iloc[2:, 2]
        data2['Total'] = data.iloc[2:, 3]
        data2['PCR'] = data.iloc[2:, 4]
        data = data2[['Date', 'Calls', 'Puts', 'Total', 'PCR']]
        data['Calls'] = data.Calls.astype(float)
        data['Puts'] = data.Puts.astype(float)
        data['Total'] = data.Total.astype(float)
        data['PCR'] = data.PCR.astype(float)
        data = data.set_index('Date')
        data = data.dropna()
    elif series == 'PCR_EQUITY':
        #DATE,CALLS,PUTS,TOTAL,P/C Ratio
        url = url_PCR_Equity
        data = pd.read_csv(url)
        data2 = data
        data2['Date'] = data.iloc[2:, 0]
        data2['Date'] = pd.to_datetime(data2['Date'])
        data2['Calls'] = data.iloc[2:, 1]
        data2['Puts'] = data.iloc[2:, 2]
        data2['Total'] = data.iloc[2:, 3]
        data2['PCR'] = data.iloc[2:, 4]
        data = data2[['Date', 'Calls', 'Puts', 'Total', 'PCR']]
        data['Calls'] = data.Calls.astype(float)
        data['Puts'] = data.Puts.astype(float)
        data['Total'] = data.Total.astype(float)
        data['PCR'] = data.PCR.astype(float)
        data = data.set_index('Date')
        data = data.dropna()
    elif series == 'PCR_VIX':
        #DATE,CALLS,PUTS,TOTAL,P/C Ratio
        url = url_PCR_VIX
        data = pd.read_csv(url)
        data2 = data
        data2['Date'] = data.iloc[2:, 0]
        data2['Date'] = pd.to_datetime(data2['Date'])
        data2['Calls'] = data.iloc[2:, 3]
        data2['Puts'] = data.iloc[2:, 2]
        data2['Total'] = data.iloc[2:, 4]
        data2['PCR'] = data.iloc[2:, 1]
        data = data2[['Date', 'Calls', 'Puts', 'Total', 'PCR']]
        data['Calls'] = data.Calls.astype(float)
        data['Puts'] = data.Puts.astype(float)
        data['Total'] = data.Total.astype(float)
        data['PCR'] = data.PCR.astype(float)
        data = data.set_index('Date')
        data = data.dropna()
    elif series == 'FUTURES':
        data = pd.DataFrame([], columns=['Trade Date', 'Futures', 'Open', 'High', 'Low', 'Close', 'Settle',
                                         'Change', 'Total Volume', 'EFP', 'Open Interest', 'Group'])
        md = pd.DataFrame(getDatesFutures())
        md['Date'] = pd.to_datetime(md['Date'])
        index = 0
        for i in range(0, len(md)):
            try:
                url = url_FUTURES + str(md.iloc[i].Date.date())
                data2 = pd.read_csv(url)
                data2['Group'] = md.iloc[i].Date.date()
                data = data.append(data2)
                index = index + 1
            except:
                print('Downloaded all data...')
    elif series == 'VVIX':
        url = url_VVIX
        data = pd.read_csv(url)
        data2 = data
        data2['Date'] = pd.to_datetime(data2.iloc[1:, 0])
        data2['VVIX'] = data2.iloc[1:, 1]
        data = data2[['Date', 'VVIX']]
        data['VVIX'] = data2['VVIX'].astype(float)
        data = data.set_index('Date')
        data = data.dropna()
    elif series == 'IC':
        # DATE,SPX,VIX,(JCJ) January 2008,(KCJ) January 2009,(ICJ) January 2010,(JCJ)
        url = url_IC
        data = pd.read_csv(url)
        data=data.reset_index()
        data.index = data.index.map(str)
        data['index']=data['index'].astype(str)
        data2 = data
        data2['Date'] = data.iloc[2:, 0]
        #data2['Date'] = pd.to_datetime(data2['Date'])
        data2['SPX'] = data.iloc[2:, 1]
        data2['VIX'] = data.iloc[2:, 2]
        data2['JCJ1'] = data.iloc[2:, 3]
        data2['KCJ1'] = data.iloc[2:, 4]
        data2['ICJ1'] = data.iloc[2:, 5]
        data2['JCJ2'] = data.iloc[2:, 6]
        data2['KCJ2'] = data.iloc[2:, 7]
        data2['ICJ2'] = data.iloc[2:, 8]
        data2['JCJ3'] = data.iloc[2:, 9]
        data2['KCJ3'] = data.iloc[2:, 10]
        data2['ICJ3'] = data.iloc[2:, 11]
        data2['JCJ4'] = data.iloc[2:, 12]
        data2['KCJ4'] = data.iloc[2:, 13]
        data2['ICJ4'] = data.iloc[2:, 14]
        data2['JCJ5'] = data.iloc[2:, 15]
        data2['KCJ5'] = data.iloc[2:, 16]
        data2 = data2.replace(r'^\s*$', np.nan, regex=True)
        data3 = data2[
            ['Date', 'JCJ1', 'KCJ1', 'ICJ1', 'JCJ2', 'KCJ2', 'ICJ2', 'JCJ3', 'KCJ3', 'ICJ3', 'JCJ4', 'KCJ4', 'ICJ4',
             'JCJ5', 'KCJ5']]
        data3 = data3.set_index('Date')
        data3['JCJ1'] = data3.JCJ1.astype(float)
        data3['KCJ1'] = data3.KCJ1.astype(float)
        data3['ICJ1'] = data3.ICJ1.astype(float)
        data3['JCJ2'] = data3.JCJ2.astype(float)
        data3['KCJ2'] = data3.KCJ2.astype(float)
        data3['ICJ2'] = data3.ICJ2.astype(float)
        data3['JCJ3'] = data3.JCJ3.astype(float)
        data3['KCJ3'] = data3.KCJ3.astype(float)
        data3['ICJ3'] = data3.ICJ3.astype(float)
        data3['JCJ4'] = data3.JCJ4.astype(float)
        data3['KCJ4'] = data3.KCJ4.astype(float)
        data3['ICJ4'] = data3.ICJ4.astype(float)
        data3['JCJ5'] = data3.JCJ5.astype(float)
        data3['KCJ5'] = data3.KCJ5.astype(float)
        data3 = pd.DataFrame(data3.mean(axis=1))
        data3['Corr'] = data3.iloc[:, 0]
        data3 = data3[['Corr']]

        data = data2[['Date', 'SPX', 'VIX']]
        data = data.merge(data3, left_on='Date', right_on='Date', how='inner')
        data['SPX'] = data.SPX.astype(float)
        data['VIX'] = data.VIX.astype(float)
        data = data.set_index('Date')
        data = data.dropna()
        data.index = pd.to_datetime(data.index)
    elif series == 'SKEW':
        url = url_SKEW
        data = pd.read_csv(url)
        data2 = data
        data2['Date'] = pd.to_datetime(data2.iloc[1:, 0])
        data2['SKEW'] = data2.iloc[1:, 1]
        data = data2[['Date', 'SKEW']]
        data['SKEW'] = data2['SKEW'].astype(float)
        data = data.set_index('Date')
        data = data.dropna()
    elif series == 'VIX':
        url = url_VIX
        data = pd.read_csv(url)
        data2 = data
        # print(data2)
        data2['Date'] = data.iloc[2:, 0]
        data2['Date'] = pd.to_datetime(data2['Date'])
        data2['Open'] = data.iloc[2:, 1]
        data2['High'] = data.iloc[2:, 2]
        data2['Low'] = data.iloc[2:, 3]
        data2['Close'] = data.iloc[2:, 4]
        data = data2[['Date', 'Open', 'High', 'Low', 'Close']]
        data['Open'] = data.Open.astype(float)
        data['High'] = data.High.astype(float)
        data['Low'] = data.Low.astype(float)
        data['Close'] = data.Close.astype(float)
        data = data.set_index('Date')
        data = data.dropna()
    elif series == 'VIX3':
        url = url_VIX3
        data = pd.read_csv(url)
        data2 = data
        data2['Date'] = data.iloc[2:, 0]
        data2['Date'] = pd.to_datetime(data2['Date'])
        data2['Open'] = data.iloc[2:, 1]
        data2['High'] = data.iloc[2:, 2]
        data2['Low'] = data.iloc[2:, 3]
        data2['Close'] = data.iloc[2:, 4]
        data = data2[['Date', 'Open', 'High', 'Low', 'Close']]
        data['Open'] = data.Open.astype(float)
        data['High'] = data.High.astype(float)
        data['Low'] = data.Low.astype(float)
        data['Close'] = data.Close.astype(float)
        data = data.set_index('Date')
        data = data.dropna()
    elif series == 'VXN':
        url = url_VXN
        data = pd.read_csv(url)
        data2 = data
        data2['Date'] = data.iloc[2:, 0]
        data2['Date'] = pd.to_datetime(data2['Date'])
        data2['Open'] = data.iloc[2:, 1]
        data2['High'] = data.iloc[2:, 2]
        data2['Low'] = data.iloc[2:, 3]
        data2['Close'] = data.iloc[2:, 4]
        data = data2[['Date', 'Open', 'High', 'Low', 'Close']]
        data['Open'] = data.Open.astype(float)
        data['High'] = data.High.astype(float)
        data['Low'] = data.Low.astype(float)
        data['Close'] = data.Close.astype(float)
        data = data.set_index('Date')
        data = data.dropna()
    return data

def addVix2Future(f):
    VIX2 = get_CBOE('VIX')
    VIX2 = VIX2[['Close']]
    VIX2.columns = ['VIX']
    VIX2 = VIX2.resample('1M', how='mean')
    VIX2['FDate'] = getThirdWednesday(2019, 1).strftime("%Y-%m-%d")
    for i in range(0, len(VIX2) - 1):
        VIX2['FDate'].iloc[i] = getThirdWednesday(int(VIX2.index[i].year), int(VIX2.index[i].month)).strftime(
            "%Y-%m-%d")
    f2 = f
    f2['VIX'] = 0.0
    for i in range(0, len(f2) - 1):
        try:
            # print(f2['Future'].iloc[i])
            ret1 = VIX2[VIX2['FDate'] == f2['Future'].iloc[i].strftime("%Y-%m-%d")]
            # print(float(ret1.iloc[0,0]))
            f2['VIX'].iloc[i] = float(ret1.iloc[0, 0])
        except:
            print('')
    f2 = f2.set_index('Future')
    return f2

##############################################
# CBOE series plots
##############################################
def plotCBOE_INDEXES(data, label):
    import matplotlib.pyplot as plt
    
    VIX=get_CBOE('VIX')
    VVIX=get_CBOE('VVIX')
    R=VIX.merge(VVIX,left_on='Date',right_on='Date',how='inner')
    R['Ratio']=R['VVIX']/R['Close']
    Corr=get_CBOE('IC')
    SK=get_CBOE('SKEW')
    futures=get_CBOE('FUTURES')
    f=filterFutCols(futures)
    f2=addVix2Future(f)
    
    #Implied Corrleation with Index 
    plt.figure(figsize=(20,10))
    ax = Corr.Corr.plot(color='green',label='Implied Correlation')
    ax2=ax.twinx()
    data.Close.plot(ax=ax2,ls='--',marker='+',color='blue',label=label)
    ax.set_ylabel('Implied Correlation')
    ax2.set_ylabel(label + ' Level')
    plt.title("Implied Correlation of S&P500",fontsize=20)
    ax.legend(loc='upper left')
    ax2.legend(loc='upper right')
    plt.show()
    
    #Volatility of index
    plt.figure(figsize=(20,10))
    ax = VIX.Close.plot(color='green',label='VIX')
    ax2=ax.twinx()
    data.Close.plot(ax=ax2,ls='--',marker='+',color='blue',label=label)
    ax.set_ylabel('Volatility')
    ax2.set_ylabel(label + ' Level')
    plt.title("Volatility of S&P500 - VIX and S&P500",fontsize=20)
    ax.legend(loc='upper left')
    ax2.legend(loc='upper right')

    plt.show()
    
    #Volatility Ratio
    plt.figure(figsize=(20,10))
    ax = R.Ratio.plot(color='green',label='VVIX/VIX Ratio')
    ax2=ax.twinx()
    data.Close.plot(ax=ax2,ls='--',marker='+',color='blue',label=label)
    ax.set_ylabel('Ratio')
    ax2.set_ylabel(label + ' Level')
    plt.title("Volatility Ratio VVIX/VIX and S&P500",fontsize=20)
    ax.legend(loc='upper left')
    ax2.legend(loc='upper right')
    plt.show()
    
    #Volatility, VVIX, and Ratio
    plt.figure(figsize=(20,15))
    ax = R.Ratio.plot(ls='-',marker='*',label='VVIX/VIX Ratio')
    ax2=ax.twinx()
    VIX.Close.plot(ax=ax2,color='black',label='VIX')
    VVIX.plot(ax=ax2,color='green',label='VVIX')
    ax.set_ylabel('Volatility')
    plt.title("Volatility Index, Volatility of Volatility, and Ratio",fontsize=20)
    ax.legend(loc='upper left')
    ax2.legend(loc='upper right')
    plt.show()
    
    #Skew
    plt.figure(figsize=(20,10))
    ax = SK.SKEW.plot(label='SKEW Index')
    ax.set_ylabel('Skew Index')
    ax2=ax.twinx()
    data.Close.plot(ax=ax2,ls='--',marker='+',color='blue',label=label)
    plt.title("Skew Index and S&P500",fontsize=20)
    ax.legend(loc='upper left')
    ax2.legend(loc='upper right')
    plt.show()
    
    #Futures
    plt.figure(figsize=(20,10))
    ax = f2['Close'].plot(color='green',label='VIX Futures Close Price')
    #f2['Close'].rolling(window=2).mean().plot(color='blue',label='MA VIX Futures Close Price')
    ax.set_ylabel('Futures Close Prices')
    #ax2=ax.twinx()
    f2.VIX.plot(ax=ax,marker='*',color='black',label='VIX')
    plt.title("VIX Futures Curve against VIX",fontsize=20)
    ax.legend(loc='upper left')
    ax2.legend(loc='upper right')
    plt.show() 


##############################################
# CBOE series single plots
##############################################
def plotCBOE_ImpliedCorrelation(data, label):
    import matplotlib.pyplot as plt
    
    Corr=get_CBOE('IC')
    
    #Implied Corrleation with Index 
    plt.figure(figsize=(20,10))
    ax = Corr.Corr.plot(color='green',label='Implied Correlation')
    ax2=ax.twinx()
    data.Close.plot(ax=ax2,ls='--',marker='+',color='blue',label=label)
    ax.set_ylabel('Implied Correlation')
    ax2.set_ylabel(label + ' Level')
    plt.title("Implied Correlation of S&P500",fontsize=20)
    ax.legend(loc='upper left')
    ax2.legend(loc='upper right')
    plt.show()
    
    
def plotCBOE_VIX(data, label):
    import matplotlib.pyplot as plt
    
    VIX=get_CBOE('VIX')
        
    #Volatility of index
    plt.figure(figsize=(20,10))
    ax = VIX.Close.plot(color='green',label='VIX')
    ax2=ax.twinx()
    data.Close.plot(ax=ax2,ls='--',marker='+',color='blue',label=label)
    ax.set_ylabel('Volatility')
    ax2.set_ylabel(label + ' Level')
    plt.title("Volatility of S&P500 - VIX and S&P500",fontsize=20)
    ax.legend(loc='upper left')
    ax2.legend(loc='upper right')

    plt.show()
  
    
def plotCBOE_VIX_RATIO(data, label):
    import matplotlib.pyplot as plt
    
    VIX=get_CBOE('VIX')
    VVIX=get_CBOE('VVIX')
    R=VIX.merge(VVIX,left_on='Date',right_on='Date',how='inner')
    R['Ratio']=R['VVIX']/R['Close']
     
    #Volatility Ratio
    plt.figure(figsize=(20,10))
    ax = R.Ratio.plot(color='green',label='VVIX/VIX Ratio')
    ax2=ax.twinx()
    data.Close.plot(ax=ax2,ls='--',marker='+',color='blue',label=label)
    ax.set_ylabel('Ratio')
    ax2.set_ylabel(label + ' Level')
    plt.title("Volatility Ratio VVIX/VIX and S&P500",fontsize=20)
    ax.legend(loc='upper left')
    ax2.legend(loc='upper right')
    plt.show()
    
    
def plotCBOE_VIX_VVIX(data, label):
    import matplotlib.pyplot as plt
    
    VIX=get_CBOE('VIX')
    VVIX=get_CBOE('VVIX')
    R=VIX.merge(VVIX,left_on='Date',right_on='Date',how='inner')
    R['Ratio']=R['VVIX']/R['Close']
    Corr=get_CBOE('IC')
    SK=get_CBOE('SKEW')
    futures=get_CBOE('FUTURES')
    f=filterFutCols(futures)
    f2=addVix2Future(f)
    
    #Volatility, VVIX, and Ratio
    plt.figure(figsize=(20,15))
    ax = R.Ratio.plot(ls='-',marker='*',label='VVIX/VIX Ratio')
    ax2=ax.twinx()
    VIX.Close.plot(ax=ax2,color='black',label='VIX')
    VVIX.plot(ax=ax2,color='green',label='VVIX')
    ax.set_ylabel('Volatility')
    plt.title("Volatility Index, Volatility of Volatility, and Ratio",fontsize=20)
    ax.legend(loc='upper left')
    ax2.legend(loc='upper right')
    plt.show()
    
def plotCBOE_SKEW(data, label):
    import matplotlib.pyplot as plt
    
    SK=get_CBOE('SKEW')
    
    #Skew
    plt.figure(figsize=(20,10))
    ax = SK.SKEW.plot(label='SKEW Index')
    ax.set_ylabel('Skew Index')
    ax2=ax.twinx()
    data.Close.plot(ax=ax2,ls='--',marker='+',color='blue',label=label)
    plt.title("Skew Index and S&P500",fontsize=20)
    ax.legend(loc='upper left')
    ax2.legend(loc='upper right')
    plt.show()
    

def plotCBOE_FUTURES(data, label):
    import matplotlib.pyplot as plt
    
    futures=get_CBOE('FUTURES')
    f=filterFutCols(futures)
    f2=addVix2Future(f)
    
    #Futures
    plt.figure(figsize=(20,10))
    ax = f2['Close'].plot(color='green',label='VIX Futures Close Price')
    #f2['Close'].rolling(window=2).mean().plot(color='blue',label='MA VIX Futures Close Price')
    ax.set_ylabel('Futures Close Prices')
    #ax2=ax.twinx()
    f2.VIX.plot(ax=ax,marker='*',color='black',label='VIX')
    plt.title("VIX Futures Curve against VIX",fontsize=20)
    ax.legend(loc='upper left')
    ax2=ax.twinx()
    data.Close.plot(ax=ax2,ls='--',marker='+',color='blue',label=label)
    ax2.set_ylabel(label + ' Level')
    ax2.legend(loc='upper right')
    plt.show() 
    
