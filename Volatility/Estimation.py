#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Estimation 
#
# Module comprising helper functions and plots to estimate implied volatility
#############################################################################################      


#Common Libraries
import numpy as np
import pandas as pd
pd.options.mode.chained_assignment = None  # default='warn'

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path+"/")
import Plib.Utils.Tools as tls
from Plib.Plotting.Plots import customize_grid

import matplotlib.style
import matplotlib as mpl
#mpl.style.use('classic')
#Gridstyle
mpl.rcParams['grid.color'] = 'k'
mpl.rcParams['grid.linestyle'] = ':'
mpl.rcParams['grid.linewidth'] = 0.5
mpl.rcParams['axes.grid'] = True
#Resolution
mpl.rcParams['figure.dpi'] = 80
mpl.rcParams['savefig.dpi'] = 100

##############################################
# Options for Jupyter
##############################################      
#This would display the graph in the notebook, but it was no longer interactive.
#%matplotlib inline
#With this the figures will now show up in the notebook , and still be interactive. 
#%matplotlib notebook


##############################################
# Helper methods 
##############################################      
def getExpDates(options):
    return list([pd.to_datetime(str(i)).strftime("%Y-%m-%d") for i in options['expirationDate'].unique()])

##############################################
# Volatility Functions
##############################################      
def volrealized(ret):
    import math
    
    N = ret.shape[0]
    df = (ret) ** 2
    sigma = math.sqrt((1 / (N - 1)) * df.sum())
    return sigma

def rollVolRealized(returns, days):
    mfun = lambda x: volrealized(x)
    sigma = (returns['Adjusted_close'].rolling(window=days).agg(mfun)).dropna()
    return sigma

def volSampleVar(ret):
    import math
    
    N = ret.shape[0]
    aret = ret.sum() * (1 / N)
    df = (ret - aret) ** 2
    sigma = math.sqrt((1 / (N - 1)) * df.sum())
    return sigma

def rollVolSampleVar(returns, days):
    mfun = lambda x: volSampleVar(x)
    sigma = (returns['Adjusted_close'].rolling(window=days).agg(mfun)).dropna()
    return sigma

def volSampleVarDemeaned(ret, aret):
    import math
    N = ret.shape[0]
    df = (ret - aret) ** 2
    sigma = math.sqrt((1 / (N - 1)) * df.sum())
    return sigma

def rollVolSampleVarDemeaned(returns, areturns, days):
    mfun = lambda x: volSampleVarDemeaned(x, areturns.Adjusted_close)
    sigma = (returns['Adjusted_close'].rolling(window=days).agg(mfun)).dropna()
    return sigma

def volParkinson(price):
    import math
    
    N = price.shape[0]
    df = np.log(price.iloc[:, 0] / price.iloc[:, 1]) ** 2
    sigma = math.sqrt((1 / (4 * N * math.log(2))) * df.sum())
    if N <= 5:
        sigma = sigma * 0.55
    elif N <= 10:
        sigma = sigma * 0.65
    elif N <= 20:
        sigma = sigma * 0.74
    elif N <= 50:
        sigma = sigma * 0.82
    elif N <= 100:
        sigma = sigma * 0.86
    elif N <= 200:
        sigma = sigma * 0.92
    return sigma

def vpark(df):
    import math
    
    N = df.shape[0]
    sigma = math.sqrt((1 / (4 * N * math.log(2))) * df.sum())
    if N <= 5:
        sigma = sigma * 0.55
    elif N <= 10:
        sigma = sigma * 0.65
    elif N <= 20:
        sigma = sigma * 0.74
    elif N <= 50:
        sigma = sigma * 0.82
    elif N <= 100:
        sigma = sigma * 0.86
    elif N <= 200:
        sigma = sigma * 0.92
    return sigma

def rollVolParkinson(price, days):
    dpd = np.log(price.iloc[:, 0] / price.iloc[:, 1]) ** 2
    sigma = (dpd.rolling(window=days).agg(vpark)).dropna()
    return sigma

def volYangZhang(price):
    import math
    
    N = price.shape[0]
    lopen = np.log(price.iloc[:, 0] / price.iloc[:, 0].shift(1)).dropna()
    slopen = (1 / N) * lopen.sum()
    sigmao = (lopen - slopen) ** 2
    ssigmao = (1 / (N - 1)) * sigmao.sum()
    lclose = np.log(price.iloc[:, 1] / price.iloc[:, 1].shift(1))
    slclose = (1 / N) * lclose.sum()
    sigmac = (lclose - slclose) ** 2
    ssigmac = (1 / (N - 1)) * sigmac.sum()
    lhc = np.log(price.iloc[:, 2] / price.iloc[:, 1])
    lho = np.log(price.iloc[:, 2] / price.iloc[:, 0])
    llc = np.log(price.iloc[:, 3] / price.iloc[:, 1])
    llo = np.log(price.iloc[:, 3] / price.iloc[:, 0])
    sigmars1 = lhc * lho
    sigmars2 = llc * llo
    sigmars = sigmars1 + sigmars2
    ssigmars = (1 / N) * sigmars.sum()
    k = 0.34 / (1.34 + ((N + 1) / (N - 1)))
    sigma = math.sqrt(ssigmao + k * ssigmac + (1 - k) * ssigmars)
    return sigma

def rollVolYangZhang(price, days):
    N = days
    lopen = np.log(price.iloc[:, 0] / price.iloc[:, 0].shift(1)).dropna()
    slopen = lopen.rolling(window=days).sum() * (1 / N)
    sigmao = (lopen - slopen) ** 2
    ssigmao = sigmao.rolling(window=days).sum() * (1 / (N - 1))
    lclose = np.log(price.iloc[:, 1] / price.iloc[:, 1].shift(1))
    slclose = lclose.rolling(window=days).sum() * (1 / N)
    sigmac = (lclose - slclose) ** 2
    ssigmac = sigmac.rolling(window=days).sum() * (1 / (N - 1))
    lhc = np.log(price.iloc[:, 2] / price.iloc[:, 1])
    lho = np.log(price.iloc[:, 2] / price.iloc[:, 0])
    llc = np.log(price.iloc[:, 3] / price.iloc[:, 1])
    llo = np.log(price.iloc[:, 3] / price.iloc[:, 0])
    sigmars1 = lhc * lho
    sigmars2 = llc * llo
    sigmars = sigmars1 + sigmars2
    ssigmars = sigmars.rolling(window=days).sum() * (1 / N)
    k = 0.34 / (1.34 + ((N + 1) / (N - 1)))
    sigma = (ssigmao + k * ssigmac + (1 - k) * ssigmars).apply(np.sqrt).dropna()
    return sigma

def conesAdjFactor(days, ssize):
    import math
    
    m = math.sqrt((1 / (1 - (days / (ssize - days - 1)) + ((days ** 2) - 1) / (3 * (ssize - days + 1) ** 2))))
    return m

def volconesYZ(price):
    import math 
    windows = [30, 60, 90, 120]
    quantiles = [0.25, 0.75]
    columns = ['maturity', 'min', 'quartile25', 'median', 'quartile75', 'max']
    cones = pd.DataFrame(columns=columns)
    cones = cones.fillna(0)

    returns = np.log(price / price.shift(1)).dropna()
    roll_VolRealized = rollVolRealized(returns, windows[0]) * conesAdjFactor(windows[0], returns.shape[0]) * math.sqrt(
        252)

    roll_Vol30 = rollVolYangZhang(price, windows[0]) * conesAdjFactor(windows[0], returns.shape[0]) * math.sqrt(252)
    roll_Vol60 = rollVolYangZhang(price, windows[1]) * conesAdjFactor(windows[1], returns.shape[0]) * math.sqrt(252)
    roll_Vol90 = rollVolYangZhang(price, windows[2]) * conesAdjFactor(windows[2], returns.shape[0]) * math.sqrt(252)
    roll_Vol120 = rollVolYangZhang(price, windows[3]) * conesAdjFactor(windows[3], returns.shape[0]) * math.sqrt(252)

    cones.loc[len(cones)] = [windows[0], roll_Vol30.min(), roll_Vol30.quantile(quantiles[0]), roll_Vol30.median(),
                             roll_Vol30.quantile(quantiles[1]), roll_Vol30.max()]
    cones.loc[len(cones)] = [windows[1], roll_Vol60.min(), roll_Vol60.quantile(quantiles[0]), roll_Vol60.median(),
                             roll_Vol60.quantile(quantiles[1]), roll_Vol60.max()]
    cones.loc[len(cones)] = [windows[2], roll_Vol90.min(), roll_Vol90.quantile(quantiles[0]), roll_Vol90.median(),
                             roll_Vol90.quantile(quantiles[1]), roll_Vol90.max()]
    cones.loc[len(cones)] = [windows[3], roll_Vol120.min(), roll_Vol120.quantile(quantiles[0]), roll_Vol120.median(),
                             roll_Vol120.quantile(quantiles[1]), roll_Vol120.max()]

    return cones, (roll_Vol30, roll_Vol60, roll_Vol90, roll_Vol120, roll_VolRealized)

def volconesPK(price):
    import math
    
    windows = [30, 60, 90, 120]
    quantiles = [0.25, 0.75]
    columns = ['maturity', 'min', 'quartile25', 'median', 'quartile75', 'max']
    cones = pd.DataFrame(columns=columns)
    cones = cones.fillna(0)

    returns = np.log(price / price.shift(1)).dropna()
    roll_VolRealized = rollVolRealized(returns, windows[0]) * conesAdjFactor(windows[0], returns.shape[0]) * math.sqrt(
        252)

    roll_Vol30 = rollVolParkinson(price, windows[0]) * conesAdjFactor(windows[0], returns.shape[0]) * math.sqrt(252)
    roll_Vol60 = rollVolParkinson(price, windows[1]) * conesAdjFactor(windows[0], returns.shape[0]) * math.sqrt(252)
    roll_Vol90 = rollVolParkinson(price, windows[2]) * conesAdjFactor(windows[0], returns.shape[0]) * math.sqrt(252)
    roll_Vol120 = rollVolParkinson(price, windows[3]) * conesAdjFactor(windows[0], returns.shape[0]) * math.sqrt(252)

    cones.loc[len(cones)] = [windows[0], roll_Vol30.min(), roll_Vol30.quantile(quantiles[0]), roll_Vol30.median(),
                             roll_Vol30.quantile(quantiles[1]), roll_Vol30.max()]
    cones.loc[len(cones)] = [windows[1], roll_Vol60.min(), roll_Vol60.quantile(quantiles[0]), roll_Vol60.median(),
                             roll_Vol60.quantile(quantiles[1]), roll_Vol60.max()]
    cones.loc[len(cones)] = [windows[2], roll_Vol90.min(), roll_Vol90.quantile(quantiles[0]), roll_Vol90.median(),
                             roll_Vol90.quantile(quantiles[1]), roll_Vol90.max()]
    cones.loc[len(cones)] = [windows[3], roll_Vol120.min(), roll_Vol120.quantile(quantiles[0]), roll_Vol120.median(),
                             roll_Vol120.quantile(quantiles[1]), roll_Vol120.max()]

    return cones, (roll_Vol30, roll_Vol60, roll_Vol90, roll_Vol120, roll_VolRealized)

def volconesSV(price):
    import math
    
    windows = [30, 60, 90, 120]
    quantiles = [0.25, 0.75]
    columns = ['maturity', 'min', 'quartile25', 'median', 'quartile75', 'max']
    cones = pd.DataFrame(columns=columns)
    cones = cones.fillna(0)

    returns = np.log(price / price.shift(1)).dropna()
    roll_VolRealized = rollVolRealized(returns, windows[0]) * conesAdjFactor(windows[0], returns.shape[0]) * math.sqrt(
        252)

    roll_Vol30 = rollVolSampleVar(returns, windows[0]) * conesAdjFactor(windows[0], returns.shape[0]) * math.sqrt(252)
    roll_Vol60 = rollVolSampleVar(returns, windows[1]) * conesAdjFactor(windows[0], returns.shape[0]) * math.sqrt(252)
    roll_Vol90 = rollVolSampleVar(returns, windows[2]) * conesAdjFactor(windows[0], returns.shape[0]) * math.sqrt(252)
    roll_Vol120 = rollVolSampleVar(returns, windows[3]) * conesAdjFactor(windows[0], returns.shape[0]) * math.sqrt(252)

    cones.loc[len(cones)] = [windows[0], roll_Vol30.min(), roll_Vol30.quantile(quantiles[0]), roll_Vol30.median(),
                             roll_Vol30.quantile(quantiles[1]), roll_Vol30.max()]
    cones.loc[len(cones)] = [windows[1], roll_Vol60.min(), roll_Vol60.quantile(quantiles[0]), roll_Vol60.median(),
                             roll_Vol60.quantile(quantiles[1]), roll_Vol60.max()]
    cones.loc[len(cones)] = [windows[2], roll_Vol90.min(), roll_Vol90.quantile(quantiles[0]), roll_Vol90.median(),
                             roll_Vol90.quantile(quantiles[1]), roll_Vol90.max()]
    cones.loc[len(cones)] = [windows[3], roll_Vol120.min(), roll_Vol120.quantile(quantiles[0]), roll_Vol120.median(),
                             roll_Vol120.quantile(quantiles[1]), roll_Vol120.max()]

    return cones, (roll_Vol30, roll_Vol60, roll_Vol90, roll_Vol120, roll_VolRealized)

##############################################
# Put-Call Plots
##############################################      
def getPutCallRatio(optionsAll):
    CALL = optionsAll[optionsAll['type'] == 'CALL']
    PUT = optionsAll[optionsAll['type'] == 'PUT']
    df1 = pd.DataFrame(CALL.groupby(['expirationDate'])['openInterest'].sum().interpolate())
    df2 = pd.DataFrame(PUT.groupby(['expirationDate'])['openInterest'].sum().interpolate())
    PCR = df1.merge(df2, left_on='expirationDate', right_on='expirationDate')
    PCR['P/C Ratio (OI)'] = PCR['openInterest_y'] / PCR['openInterest_x']
    df1 = pd.DataFrame(CALL.groupby(['expirationDate'])['volume'].sum().interpolate())
    df2 = pd.DataFrame(PUT.groupby(['expirationDate'])['volume'].sum().interpolate())
    PCR2 = df1.merge(df2, left_on='expirationDate', right_on='expirationDate')
    PCR2['P/C Ratio (V)'] = PCR2['volume_y'] / PCR2['volume_x']
    PCR = PCR.merge(PCR2, left_on='expirationDate', right_on='expirationDate')
    del PCR['openInterest_x']
    del PCR['openInterest_y']
    del PCR['volume_x']
    del PCR['volume_y']
    return PCR

def plotPCRatio(symbol, optionsAll, pcr_type='PCR_EQUITY'):
    import matplotlib.pyplot as plt
    import Plib.Volatility.CBOE as cb
    
    try:
        PCR_CBOE = cb.get_CBOE('PCR_EQUITY')
        PCR_CBOE = PCR_CBOE.tail(365)
    except:
        pcr_type=''
    PCR = getPutCallRatio(optionsAll)

    fig = plt.figure(figsize=(20, 10))
    ax = fig.add_subplot(111)

    if pcr_type != '':
        ax.plot(PCR_CBOE.index, PCR_CBOE.PCR.rolling(window=2).mean(), c='black', ls='-',
            label='CBOE Equity Put-Call Ratio')
        ax.set_title("Put-Call Ratio - CBOE Equity and " + symbol, fontsize=16)
    else:
        ax.set_title("Put-Call Ratio - " + symbol, fontsize=16)
    ax.plot(PCR.index, PCR['P/C Ratio (OI)'].rolling(window=2).mean(), c='brown', marker="^", ls='--',
            label=symbol + ' OI Put-Call Ratio')
    ax.plot(PCR.index, PCR['P/C Ratio (V)'].rolling(window=2).mean(), c='b', marker="+", ls='-',
            label=symbol + ' Vol Put-Call Ratio')
    plt.legend(loc=2)
    plt.show()

def plotPCVolOI(symbol, optionsAll, u_price):
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    from scipy.stats import norm
    from scipy.stats import lognorm

    fig = plt.figure(figsize=(20, 10))
    ax = fig.add_subplot(111)
    # ax.hist(prices.Close, bins=50,normed=1,orientation=u'horizontal',alpha = 0.4,label='Histogram of Prices')

    c_options = optionsAll[optionsAll['type'] == 'CALL']
    p_options = optionsAll[optionsAll['type'] == 'PUT']
    callVolumes = pd.DataFrame(c_options.groupby(['strike'])['volume'].sum().dropna())
    putVolumes = pd.DataFrame(p_options.groupby(['strike'])['volume'].sum().dropna())
    callOI = pd.DataFrame(c_options.groupby(['strike'])['openInterest'].sum().dropna())
    putOI = pd.DataFrame(p_options.groupby(['strike'])['openInterest'].sum().dropna())

    ax.bar(callVolumes.index - 0.9, callVolumes.volume, width=0.9, color='red', label='CALL VOL', alpha=0.4)
    ax.bar(putVolumes.index, putVolumes.volume, width=0.9, color='green', label='PUT VOL', alpha=0.4)
    ax.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))

    ax.axvline(x=u_price, linestyle='--', color='r', linewidth=2, label='Underlying price')

    ax2 = ax.twinx()
    ax2.bar(callOI.index - 0.9, callOI.openInterest, width=0.9, color='orange', label='CALL OI', alpha=0.4)
    ax2.bar(putOI.index, putOI.openInterest, width=0.9, color='yellow', label='PUT OI', alpha=0.4)
    ax2.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))

    ax.set_title("Put-Call Volume and Open Interest for " + symbol, fontsize=16)
    ax.legend(loc=2)
    ax2.legend(loc=1)

    plt.show()

##############################################
# Smile Plots
##############################################      
def plotSmile(putCall, dd, symbol, expdata,u_price,interp=True,strike_sel=0.20,fs=(20,7)):
    from matplotlib import gridspec
    import matplotlib.pyplot as plt
    import matplotlib.ticker as mticker
    import Plib.Signals.Filters as flt
    import numpy as np
    import math
    
    IV1 = dd[(dd['type'] == putCall) & (dd['expirationDate'] == expdata)]
    IV2 = dd[(dd['expirationDate'] == expdata) & (dd['strike'] >= u_price * (1-strike_sel)) & (
                dd['strike'] <= u_price * (1+strike_sel)) & (dd['type'] == putCall)]
    IV3 = dd[(dd['expirationDate'] == expdata) & (dd['type'] == putCall) & (
                dd['strike'] >= u_price * (1+strike_sel))]
    IV4 = dd[(dd['expirationDate'] == expdata) & (dd['type'] == putCall) & (
                dd['strike'] <= u_price * (1-strike_sel))]

    IV1=IV1.dropna()
    IV2=IV2.dropna()
    IV3=IV3.dropna()
    IV4=IV4.dropna()
    
    fig = plt.figure(figsize=fs)
    gs = gridspec.GridSpec(2, 2)
    
    fig.suptitle(" Smile of Implied Volatility for " + symbol + " with Expiration " + expdata)
    fig.text(0.5, 0.04, 'Strikes', ha='center',fontsize=16)
    
    ax1 = plt.subplot(gs[0, 0])
    IV1.sort_values(by=['strike','impliedVolatility'], inplace=True, ascending=True)
    if interp:
        yhat=flt.movingaverage(IV1['impliedVolatility'].values,3)
        try: yhat=flt.savitzkyGolay(yhat)
        except: pass
        x,yhat2=flt.lowess(IV1['strike'],yhat)
        if not math.isnan(sum(yhat2)):
            yhat=yhat2
        plt.plot(IV1['strike'],yhat)
    else:
        plt.plot(IV1['strike'], IV1['impliedVolatility'])
    ax1.yaxis.set_major_formatter(mticker.PercentFormatter(xmax=1.0, decimals=1))
    plt.title(putCall + " IAO-TM ")
    fig.add_subplot(ax1)

    ax2 = plt.subplot(gs[0, 1])
    IV2.sort_values(by=['strike','impliedVolatility'], inplace=True, ascending=True)
    if interp:
        yhat=flt.movingaverage(IV2['impliedVolatility'].values,3)
        try: yhat=flt.savitzkyGolay(yhat)
        except: pass
        x,yhat2=flt.lowess(IV2['strike'],yhat)
        if not math.isnan(sum(yhat2)):
            yhat=yhat2
        plt.plot(IV2['strike'],yhat)
    else:
        plt.plot(IV2['strike'], IV2['impliedVolatility'])
    ax2.yaxis.set_major_formatter(mticker.PercentFormatter(xmax=1.0, decimals=1))
    plt.title(putCall + " ATM ")
    fig.add_subplot(ax2)

    ax3 = plt.subplot(gs[1, 0])
    IV3.sort_values(by=['strike','impliedVolatility'], inplace=True, ascending=True)
    if interp:
        yhat=flt.movingaverage(IV3['impliedVolatility'].values,3)
        try: yhat=flt.savitzkyGolay(yhat)
        except: pass
        x,yhat2=flt.lowess(IV3['strike'],yhat)
        if not math.isnan(sum(yhat2)):
            yhat=yhat2
        plt.plot(IV3['strike'],yhat)
    else:
        plt.plot(IV3['strike'], IV3['impliedVolatility'])
    ax3.yaxis.set_major_formatter(mticker.PercentFormatter(xmax=1.0, decimals=1))
    plt.title(putCall + " DEEP OTM ")
    fig.add_subplot(ax3)

    ax4 = plt.subplot(gs[1, 1])
    IV4.sort_values(by=['strike','impliedVolatility'], inplace=True, ascending=True)
    if interp:
        yhat=flt.movingaverage(IV4['impliedVolatility'].values,3)
        try: yhat=flt.savitzkyGolay(yhat)
        except: pass
        x,yhat2=flt.lowess(IV4['strike'],yhat)
        if not math.isnan(sum(yhat2)):
            yhat=yhat2
        plt.plot(IV4['strike'],yhat)
    else:
        plt.plot(IV4['strike'], IV4['impliedVolatility'])
    ax4.yaxis.set_major_formatter(mticker.PercentFormatter(xmax=1.0, decimals=1))
    plt.title(putCall + " DEEP ITM ")
    fig.add_subplot(ax4)
    
    fig.tight_layout(rect=[0, 0.03, 1, 0.95])

    plt.show()
    
    pd.options.display.float_format = '{:,.2f}'.format
    idx=['Price','Mean IV Deep OTM (%)','Mean IV ATM (%)','Mean IV IAO-TM (%)','Mean IV Deep ITM (%)']
    ar=[u_price,
        round(100*IV3['impliedVolatility'].mean(),4),
        round(100*IV2['impliedVolatility'].mean(),4),
        round(100*IV1['impliedVolatility'].mean(),4),
        round(100*IV4['impliedVolatility'].mean(),4)]
    df=pd.DataFrame(ar,columns=['Values'],index=idx)
    print(df)

def plotSmileEATM(putCall, dd, symbol, expdata,u_price,interp=True,strike_sel=0.20,fs=(20,7)):
    from matplotlib import gridspec
    import matplotlib.pyplot as plt
    import matplotlib.ticker as mticker
    import Plib.Signals.Filters as flt
    import numpy as np
    import math
    
    
    expdata1,expdata2,expdata3,expdata4=expdata[0],expdata[1],expdata[2],expdata[3]
    
    IV1 = dd[(dd['expirationDate'] == expdata1) & (dd['strike'] >= u_price * (1-strike_sel)) & (
                dd['strike'] <= u_price * (1+strike_sel)) & (dd['type'] == putCall)]
    IV2 = dd[(dd['expirationDate'] == expdata2) & (dd['strike'] >= u_price * (1-strike_sel)) & (
                dd['strike'] <= u_price * (1+strike_sel)) & (dd['type'] == putCall)]
    IV3 = dd[(dd['expirationDate'] == expdata3) & (dd['strike'] >= u_price * (1-strike_sel)) & (
                dd['strike'] <= u_price * (1+strike_sel)) & (dd['type'] == putCall)]
    IV4 = dd[(dd['expirationDate'] == expdata4) & (dd['strike'] >= u_price * (1-strike_sel)) & (
                dd['strike'] <= u_price * (1+strike_sel)) & (dd['type'] == putCall)]
    
    IV1=IV1.dropna()
    IV2=IV2.dropna()
    IV3=IV3.dropna()
    IV4=IV4.dropna()
    
    fig = plt.figure(figsize=fs)
    gs = gridspec.GridSpec(2, 2)
    
    fig.suptitle(" Smile of Implied Volatility for " + symbol +' '+ putCall+ "s at the money")
    fig.text(0.5, 0.04, 'Strikes', ha='center',fontsize=16)
    
    ax1 = plt.subplot(gs[0, 0])
    IV1.sort_values(by=['strike','impliedVolatility'], inplace=True, ascending=True)
    if interp:
        yhat=flt.movingaverage(IV1['impliedVolatility'].values,3)
        try: yhat=flt.savitzkyGolay(yhat)
        except: pass
        x,yhat2=flt.lowess(IV1['strike'],yhat)
        if not math.isnan(sum(yhat2)):
            yhat=yhat2
        plt.plot(IV1['strike'],yhat)
    else:
        plt.plot(IV1['strike'], IV1['impliedVolatility'])
    ax1.yaxis.set_major_formatter(mticker.PercentFormatter(xmax=1.0, decimals=1))
    plt.title(expdata1)
    fig.add_subplot(ax1)

    ax2 = plt.subplot(gs[0, 1])
    IV2.sort_values(by=['strike','impliedVolatility'], inplace=True, ascending=True)
    if interp:
        yhat=flt.movingaverage(IV2['impliedVolatility'].values,3)
        try: yhat=flt.savitzkyGolay(yhat)
        except: pass
        x,yhat2=flt.lowess(IV2['strike'],yhat)
        if not math.isnan(sum(yhat2)):
            yhat=yhat2
        plt.plot(IV2['strike'],yhat)
    else:
        plt.plot(IV2['strike'], IV2['impliedVolatility'])
    ax2.yaxis.set_major_formatter(mticker.PercentFormatter(xmax=1.0, decimals=1))
    plt.title(expdata2)
    fig.add_subplot(ax2)

    ax3 = plt.subplot(gs[1, 0])
    IV3.sort_values(by=['strike','impliedVolatility'], inplace=True, ascending=True)
    if interp:
        yhat=flt.movingaverage(IV3['impliedVolatility'].values,3)
        try: yhat=flt.savitzkyGolay(yhat)
        except: pass
        x,yhat2=flt.lowess(IV3['strike'],yhat)
        if not math.isnan(sum(yhat2)):
            yhat=yhat2
        plt.plot(IV3['strike'],yhat)
    else:
        plt.plot(IV3['strike'], IV3['impliedVolatility'])
    ax3.yaxis.set_major_formatter(mticker.PercentFormatter(xmax=1.0, decimals=1))
    plt.title(expdata3)
    fig.add_subplot(ax3)

    ax4 = plt.subplot(gs[1, 1])
    IV4.sort_values(by=['strike','impliedVolatility'], inplace=True, ascending=True)
    if interp:
        yhat=flt.movingaverage(IV4['impliedVolatility'].values,3)
        try: yhat=flt.savitzkyGolay(yhat)
        except: pass
        x,yhat2=flt.lowess(IV4['strike'],yhat)
        if not math.isnan(sum(yhat2)):
            yhat=yhat2
        plt.plot(IV4['strike'],yhat)
    else:
        plt.plot(IV4['strike'], IV4['impliedVolatility'])
    ax4.yaxis.set_major_formatter(mticker.PercentFormatter(xmax=1.0, decimals=1))
    plt.title(expdata4)
    fig.add_subplot(ax4)
    
    fig.tight_layout(rect=[0, 0.03, 1, 0.95])

    plt.show()
    
    pd.options.display.float_format = '{:,.2f}'.format
    idx=['Price','Mean IV '+expdata1+' (%)','Mean IV '+expdata2+' (%)','Mean IV '+expdata3+' (%)','Mean IV '+expdata4+' (%)']
    ar=[u_price,
        round(100*IV3['impliedVolatility'].mean(),4),
        round(100*IV2['impliedVolatility'].mean(),4),
        round(100*IV1['impliedVolatility'].mean(),4),
        round(100*IV4['impliedVolatility'].mean(),4)]
    df=pd.DataFrame(ar,columns=['Values'],index=idx)
    print(df)
    
##############################################
# Volatility Plots
##############################################      
def plot3DVol(options,maxdate='2021-09-01',otype='PUT',desc='',point=None,angle=45, fs=(20,12)):
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt
    import pandas as pd
    import seaborn as sns

    #Filter options from Orats format
    df=options[(options['expirationDate'] <= maxdate) & 
            (options['impliedVolatility']>0) &
            (options['type']==otype)
           ]
    df=df.sort_values(by=['strike', 'dte'])
    df=df[['strike', 'dte','impliedVolatility']]
    df.columns=['Y','X','Z']

    # Make the plot
    fig = plt.figure(figsize=fs)
    ax = fig.gca(projection='3d')
    
    surf=ax.plot_trisurf(df['Y'], df['X'], df['Z'],cmap=plt.cm.inferno, linewidth=1.9, antialiased=True)
    if point is not None:
        ax.plot(point[0], point[1], point[2], markerfacecolor=point[4], markeredgecolor=point[4], marker='o', markersize=15)
        ax.text(point[0], point[1], point[2], point[3], color=point[4])
    
    #Add labels, title, point, and colorbar
    ax.set_xlabel('Strike', labelpad=15)
    ax.set_ylabel('Date to Maturity', labelpad=15)
    ax.set_zlabel('Implied Volatility', labelpad=5)
    ax.set_title(desc + ' - ' + otype, pad=50)
    fig.colorbar( surf, shrink=0.5, aspect=5)
    # Rotate and show
    ax.view_init(30, angle)
    plt.show()
    return df,surf

def plotVols(vols,data,what,implied,desc):
    import matplotlib.pyplot as plt
    import matplotlib.ticker as mticker
    
    fig = plt.figure(figsize=(20, 10))
    ax = fig.add_subplot(111)

    if what[0]:
        ax.plot(vols[0], c='m', marker="o", ls='--', label='30-Days', fillstyle='none')
    if what[1]:
        ax.plot(vols[1], c='r', marker="v", ls='--', label='60-Days')
    if what[2]:
        ax.plot(vols[2], c='y', marker="*", ls='--', label='90-Days')
    if what[3]:
        ax.plot(vols[3], c='g', marker=(8, 2, 0), ls='--', label='120-Days')
    if what[4]:
        ax.plot(vols[4], color='yellow', marker=(8, 2, 0), ls='-', label='30-D Realized')
    if what[5]:
        ax.plot(implied, color='brown', marker="*", ls='-', label='30-D Implied')

    ax2 = ax.twinx()
    #ax2.invert_xaxis()
    ax2.plot(data['Adjusted_close'], color='black', ls='-', label='Adjusted Price')

    ax.yaxis.set_major_formatter(mticker.PercentFormatter(xmax=1.0, decimals=1))
    ax.set_ylabel('Volatility')
    plt.title("Price against Rolling Annualized Volatility - (" + desc + ")",fontsize=20)
    ax.legend(loc='upper left')
    ax2.legend()

    plt.show()
     
def plotCones(cones, vols,what,implied,desc):
    import matplotlib.ticker as mticker
    import matplotlib.pyplot as plt
    
    windows = [30, 60, 90, 120]
    quantiles = [0.25, 0.75]

    x = windows

    fig = plt.figure(figsize=(20, 10))
    ax = fig.add_subplot(111)

    ax.plot(x, cones['max'], c='m', marker="o", ls='-', label='Max', fillstyle='none')
    ax.plot(x, cones['quartile75'], c='r', marker="v", ls='--', label='75% Quartile')
    ax.plot(x, cones['median'], c='k', ls='--', label='Median')
    ax.plot(x, cones['quartile25'], c='g', marker=(8, 2, 0), ls='--', label='25% Quartile')
    ax.plot(x, cones['min'], c='b', marker="^", ls='-', label='Min', fillstyle='none')

    if what[0] or what[1] or what[2]:
        ax2 = ax.twiny()
    if what[2]:
        ax2.plot(vols[4], color='yellow', marker='*', ls='--', label='30-Day Roll Realized')
    if what[0]:
        ax2.plot(vols[0], color='grey', marker='*', ls='--', label='30-Day Roll Vol')
    if what[1]:
        ax2.plot(implied, color='orange', marker='*', ls='--', label='30-Day Implied')
        ax2.text(0.03, 0.01, 'might use expanding instead of rolling for shorter chains',
                verticalalignment='bottom', horizontalalignment='left',
                transform=ax.transAxes,
                color='green', fontsize=12)
    if what[0] or what[1] or what[2]:
        ax2.legend(loc='upper left', ncol=2)
        #ax2.legend(loc='lower left', bbox_to_anchor=(0.0, 1.1), ncol=2)
        #ax2.legend(loc='lower left', bbox_to_anchor=(0.0, 1.1), ncol=2, borderaxespad=0, frameon=False)
        customize_grid(ax2)
        
    ax.set_xticks(windows)
    ax.yaxis.set_major_formatter(mticker.PercentFormatter(xmax=1.0, decimals=1))
    ax.set_ylabel('Volatility')
    ax.set_xlabel('Moving Average Windows of Volatility')
    if what[0] or what[1] or what[2]:
        plt.title("Rolling Annualized Volatility Cones - (" + desc + ")", y=1.08,fontsize=20)
    else:
        plt.title("Rolling Annualized Volatility Cones - (" + desc + ")", y=1.02,fontsize=20)
    ax.legend(loc='lower right', ncol=3)
    #ax.legend(loc='lower left', bbox_to_anchor=(0.0, -0.2), ncol=5)
    #ax.legend(loc='lower left', bbox_to_anchor=(0.0, -1.05), ncol=5, borderaxespad=0, frameon=False)
    ax.invert_xaxis()
    
    customize_grid(ax)
    
    plt.show()

def plotDiagCones(cones,vols,desc,prices,prangePK):
    what = [False, False,False]
    plotCones(cones, vols, what, None, desc)
    what = [True, True, True, True, True,False]
    plotVols(vols, prices, what, None, desc)
    print('Current Price and Estimated std:',prices.Adjusted_close.iloc[len(prices)-1],round(prangePK,4))
    print('Price Range in one standard deviation:',round(prices.Adjusted_close.iloc[len(prices)-1]*(1+1*round(prangePK,4)),4),round(prices.Adjusted_close.iloc[len(prices)-1]*(1-1*round(prangePK,4)),4))
  
def plotVolConesPK(symbol,dt_start,udata,iVol,options,prices,iSVol):
    cones, vols = volconesPK(prices)
    u_price=prices.Adjusted_close.iloc[len(prices)-1]
    desc = 'Parkinson - ' + symbol + " " + dt_start + "-" + udata
    #roll_Vol30, impl_Vol30, roll_Vol30Realized
    what = [False, False, False]
    plotCones(cones, vols, what, iVol, desc)

def plotVolConesSV(symbol,dt_start,udata,iVol,options,prices,u_price=0):
    cones, vols = volconesSV(prices)
    desc = symbol + " " + dt_start + "-" + udata
    what = [False, True, True]
    plotCones(cones, vols, what, iVol, desc)
    if u_price==0: u_price=prices.Adjusted_close[len(prices)-1]
    printIVStats(iVol,u_price,udata)

def compute30DVol(optionsAll,dtype,dt_start,dt_end,u_price,threshold=0.12,rollwin=30):
    
    def getATM(df,u_price,threshold=0.08,otype='CALL'):  
        df1=df[(df['type']==otype) & (df['strike']>=df['strike']*(1-u_price*threshold)) & (df['strike']<=df['strike']*(1+u_price*threshold))]
        return df1
    
    c_optionsATM=getATM(optionsAll.copy(),u_price,threshold,dtype)
    iVol=pd.DataFrame(c_optionsATM.groupby(['expirationDate'])['impliedVolatility'].mean().rolling(window=rollwin).mean().dropna())
    iVol.columns=['impliedVolatility']
    iSVol=pd.DataFrame(c_optionsATM.groupby(['expirationDate'])['smvIV'].mean().rolling(window=rollwin).mean().dropna())
    iSVol.columns=['impliedVolatility']
    if len(iVol)==0:
        iVol=pd.DataFrame(c_optionsATM.groupby(['expirationDate'])['impliedVolatility'].mean().expanding().mean().dropna())
        iVol.columns=['impliedVolatility']
        iSVol=pd.DataFrame(c_optionsATM.groupby(['expirationDate'])['smvIV'].mean().expanding().mean().dropna())
        iSVol.columns=['impliedVolatility']
    return iSVol,iVol,c_optionsATM
    
def compute30DVol2(dtype,dt_start,dt_end,symbol,u_price,greeks=True,rollwin=20,threshold=0.08):
    from Plib.DataFarm import Orats as o
    
    def getATM(df,u_price,threshold=0.08,otype='CALL'):  
        df1=df[(df['type']==otype) & (df['strike']>=df['strike']*(1-u_price*threshold)) & (df['strike']<=df['strike']*(1+u_price*threshold))]
        return df1
    
    #u_price=options.und.iloc[0]
    optionsAll=o.getOptionsOrats2(dt_start,dt_end,symbol,greeks=True)
    
    c_optionsATM=getATM(optionsAll.copy(),u_price,threshold*2,dtype)
    iVol=pd.DataFrame(c_optionsATM.groupby(['expirationDate'])['impliedVolatility'].mean().rolling(window=rollwin).mean().dropna())
    iVol.columns=['impliedVolatility']
    iSVol=pd.DataFrame(c_optionsATM.groupby(['expirationDate'])['smvIV'].mean().rolling(window=rollwin).mean().dropna())
    iSVol.columns=['impliedVolatility']
    if len(iVol)==0:
        iVol=pd.DataFrame(c_optionsATM.groupby(['expirationDate'])['impliedVolatility'].mean().expanding().mean().dropna())
        iVol.columns=['impliedVolatility']
        iSVol=pd.DataFrame(c_optionsATM.groupby(['expirationDate'])['smvIV'].mean().expanding().mean().dropna())
        iSVol.columns=['impliedVolatility']
    return iSVol,iVol,optionsAll

##############################################
# Liquidity Plots
##############################################         
def plotOptionLiquidity(ddf1,symbol,otype,exp):
    from matplotlib import gridspec
    import matplotlib.pyplot as plt
    import matplotlib.ticker as mticker
    import matplotlib.ticker as ticker
    import numpy as np
    
    #volume	PBA	DTV	AILLIQ
    
    df1=ddf1[(ddf1['expirationDate']==exp)]
    
    fig = plt.figure(figsize=(20, 5))
    gs = gridspec.GridSpec(2, 2)
    
    if otype=='':otype='OPTION'
    fig.suptitle(" Liquidity of " + otype +"s with exp. " +exp+ " for "+ symbol)
    
    ax1 = plt.subplot(gs[0, 0])
    plt.bar(df1['strike'], df1['PBA'])
    ax1.yaxis.set_major_formatter(mticker.PercentFormatter(xmax=1.0, decimals=1))
    plt.title("Proportional Bid-Ask Spread")
    ax1.xaxis.set_tick_params(rotation=12)
    fig.add_subplot(ax1)

    ax2 = plt.subplot(gs[0, 1])
    plt.bar(df1['strike'], df1['volume'])
    formatter = ticker.FormatStrFormatter('$%1.0f')
    ax2.yaxis.set_major_formatter(formatter)
    plt.title("Volume")
    ax2.xaxis.set_tick_params(rotation=12)
    fig.add_subplot(ax2)

    ax3 = plt.subplot(gs[1, 0])
    plt.bar(df1['strike'], df1['DTV'])
    formatter = ticker.FormatStrFormatter('$%1.0f')
    ax3.yaxis.set_major_formatter(formatter)
    plt.title("Dollar Trading Volume")
    ax3.xaxis.set_tick_params(rotation=12)
    fig.add_subplot(ax3)

    ax4 = plt.subplot(gs[1, 1])
    plt.bar(df1['strike'], df1['AILLIQ'])
    ax4.yaxis.set_major_formatter(mticker.PercentFormatter(xmax=1.0, decimals=1))
    plt.title("AILLIQ")
    ax4.xaxis.set_tick_params(rotation=12)
    fig.add_subplot(ax4)
    
    fig.tight_layout(rect=[0, 0.03, 1, 0.95])
    
    plt.show()
    
def plotStockLiquidity(df1,symbol):
    from matplotlib import gridspec
    import matplotlib.pyplot as plt
    import matplotlib.ticker as mticker
    import matplotlib.ticker as ticker
    import numpy as np
    from matplotlib.ticker import FuncFormatter
    
    scale_y = 1e6
    def millions(x, pos):
        'The two args are the value and tick position'
        return '%1.1fM' % (x * 1e-6)

    #volume	PBA	DTV	AILLIQ
    fig = plt.figure(figsize=(20, 5))
    gs = gridspec.GridSpec(2, 2)
    
    fig.suptitle(" Liquidity of " + symbol)
    
    ax1 = plt.subplot(gs[0, 0])
    plt.plot(df1.index, df1['PBA'])
    ax1.yaxis.set_major_formatter(mticker.PercentFormatter(xmax=1.0, decimals=1))
    plt.title("Proportional Bid-Ask Spread")
    ax1.xaxis.set_tick_params(rotation=12)
    fig.add_subplot(ax1)

    ax2 = plt.subplot(gs[0, 1])
    plt.plot(df1.index, df1['Volume'])
    formatter = ticker.FuncFormatter(lambda x, pos: '{0:g}mln'.format(x/scale_y))
    ax2.yaxis.set_major_formatter(formatter)
    
    plt.title("Volume")
    ax2.xaxis.set_tick_params(rotation=12)
    fig.add_subplot(ax2)

    ax3 = plt.subplot(gs[1, 0])
    plt.plot(df1.index, df1['DTV'])
    #formatter = ticker.FormatStrFormatter('$%1.0f')
    formatter = ticker.FuncFormatter(lambda x, pos: '${0:g}mln'.format(x/scale_y))
    ax3.yaxis.set_major_formatter(formatter)
    plt.title("Dollar Trading Volume")
    ax3.xaxis.set_tick_params(rotation=12)
    fig.add_subplot(ax3)

    #df1=df1['AILLIQ'].replace([np.inf, -np.inf], np.nan).dropna()
    ax4 = plt.subplot(gs[1, 1])
    plt.plot(df1.index, df1['AILLIQ'])
    ax4.yaxis.set_major_formatter(mticker.PercentFormatter(xmax=1.0, decimals=1))
    plt.title("AILLIQ")
    ax4.xaxis.set_tick_params(rotation=12)
    fig.add_subplot(ax4)
    
    fig.tight_layout(rect=[0, 0.03, 1, 0.95])
    
    plt.show()
    
##############################################
# Statistics
##############################################      
def printIVStats(res,u_price,udata):
    import math as m
    print('Options IVol Stats (see note in graph, if present)')
    #print('----------------------------------------------------------------------------')
    res["impliedVolatility"] = res.impliedVolatility.astype(float)
    
    tres=res.tail(252)
    miniv,maxiv=round(tres.impliedVolatility.min(),4),round(tres.impliedVolatility.max(),4)
    #print("Min 52W Implied Vol: ", miniv)
    #print("Max 52W Implied Vol: ", maxiv)
    
    index=tls.nearestDate(res,udata)
    cvol=res.impliedVolatility.iloc[index]
    #print("Last Implied Vol: ", round(cvol,4))
    IVRank=((cvol-miniv)/(maxiv-miniv))*100
    #print("IV Rank (over 252 days): " + str(round((IVRank),0)) + 'th')
    df1=pd.DataFrame(res)
    df1.sort_values('impliedVolatility',ascending=False)
    t_days=len(df1)
    df2=df1[df1['impliedVolatility']<=cvol]
    m_days=len(df2)
    IVPerc=m_days/t_days
    #print("IV Perc (over 252 days): " + str(round(float(IVPerc),0)) + ' day(s)')
    #print('Current Price and Estimated Mean iVol:',u_price,round(res.impliedVolatility.mean(),4))
    #1 StdDev Move = (Stock Price X Implied Volatility X the Square Root of 'how many days') all divided by the Square Root of 365.
    irange=(u_price*res.impliedVolatility.mean()*m.sqrt(30))/m.sqrt(365)/2
    EPrice_range1=u_price+irange
    EPrice_range2=u_price-irange
    #print("Price 30-Days Expected Range: ", round(EPrice_range2,2),round(EPrice_range1,2))
    #print('')
    
    pd.options.display.float_format = '{:,.2f}'.format
    idx=['Min 52W Implied Vol (%)','Max 52W Implied Vol (%)','Last Implied Vol (%)','IV Rank (over 252 days)','IV Perc (over 252 days) (%)','Current Price','Estimated Mean iVol (%)','Min Expected Price 30-Days','Max Expected Price 30-Days']
    ar=[miniv*100,maxiv*100,cvol*100,IVRank,IVPerc*100,u_price,res.impliedVolatility.mean()*100,EPrice_range2,EPrice_range1]
    df=pd.DataFrame(ar,columns=['Values'],index=idx)
    print(df)
    
def portfolioGreeks(sLegs,LongShort):
    fstrike1,fcallp1,fiv1,fdtm1,deltaf1,vegaf1,gammaf1,thetaf1,ftype1=sLegs[0:9]
    bstrike1,bcallp1,biv1,bdtm1,deltab1,vegab1,gammab1,thetab1,btype1=sLegs[9:18]
    setup = (LongShort*bcallp1*100 - LongShort*fcallp1*100)
    delta = (LongShort*100*deltab1-LongShort*100*deltaf1)#/100   #on 100 scale
    vega = (LongShort*100*vegab1-LongShort*100*vegaf1)/100
    gamma = (LongShort*100*gammab1-LongShort*100*gammaf1)/100
    theta = (LongShort*100*thetab1-LongShort*100*thetaf1)/100
    hedge=0
    if delta != 0: hedge=100/df['delta'] 

def plotOptBidAskSpread(options,fs=(15,3)):
    import matplotlib.pyplot as plt
    
    def getBASpread(options,ctype='CALL'):
        tmax=options.dte.max()

        bdak=options[(options.type==ctype) & (options.dte > 2*(tmax/3)) & (options.dte < 3*(tmax/3))][['expirationDate','bid','ask']]
        bdak['spread']=abs(bdak.bid-bdak.ask)
        bdak1=bdak.groupby('expirationDate').mean()
        bdak1['time']=3
        bdak1=bdak1.reset_index()
        bdak1=bdak1.set_index('expirationDate','time')

        bdak=options[(options.type==ctype) & (options.dte > 1*(tmax/3)) & (options.dte < 2*(tmax/3))][['expirationDate','bid','ask']]
        bdak['spread']=abs(bdak.bid-bdak.ask)
        bdak2=bdak.groupby('expirationDate').mean()
        bdak2['time']=2
        bdak2=bdak2.reset_index()
        bdak2=bdak2.set_index('expirationDate','time')

        bdak=options[(options.type==ctype) & (options.dte > (tmax/3)/2) & (options.dte < 1*(tmax/3))][['expirationDate','bid','ask']]
        bdak['spread']=abs(bdak.bid-bdak.ask)
        bdak3=bdak.groupby('expirationDate').mean()
        bdak3['time']=1
        bdak3=bdak3.reset_index()
        bdak3=bdak3.set_index('expirationDate','time')

        bdak=options[(options.type==ctype) & (options.dte >= 0) & (options.dte < (tmax/3)/2)][['expirationDate','bid','ask']]
        bdak['spread']=abs(bdak.bid-bdak.ask)
        bdak4=bdak.groupby('expirationDate').mean()
        bdak4['time']=0
        bdak4=bdak4.reset_index()
        bdak4=bdak4.set_index('expirationDate','time')

        spread=bdak1.append(bdak2).append(bdak3).append(bdak4).reset_index()
        spread=spread[['spread','time']].groupby('time').mean()
        return spread
    
    fig, ax = plt.subplots(1, 2,sharey=True,figsize=fs)

    spread=getBASpread(options,ctype='CALL')
    ax[0].plot(spread.index, spread.spread)
    ax[0].set_title("CALL Bid-Ask Spread against Expiration")
    ax[0].invert_xaxis()
    ax[0].set_ylabel('Spread')
    ax[0].set_xlabel('Time')

    spread=getBASpread(options,ctype='PUT')
    ax[1].plot(spread.index, spread.spread)
    ax[1].set_title("PUT Bid-Ask Spread against Expiration")
    ax[1].invert_xaxis()
    ax[1].set_xlabel('Time')

    fig.tight_layout()
    plt.show()

def printOptLiqStats(options,otype='',symbol='',exp=''):
    def filterCols(df):
        df["strike"] = df.strike.astype(float)
        df["bid"] = df.bid.astype(float)
        df["ask"] = df.ask.astype(float)
        df["lastPrice"] = df.lastPrice.astype(float)
        df["impliedVolatility"] = df.impliedVolatility.astype(float)
        df["volume"] = df.volume.astype(float)
        df=df[['type','expirationDate','strike','bid','ask','lastPrice','impliedVolatility','volume']]
        return df
    
    if otype=='':
        ddepth=options[(options['bid']!=0) &(options['ask']!=0) &(options['volume']!=0)]
    else:
        ddepth=options[(options['type']==otype) &(options['bid']!=0) &(options['ask']!=0) &(options['volume']!=0)]
    ddepth=filterCols(ddepth)

    ddepth['PBA']=(ddepth['volume']*(ddepth['ask']-ddepth['bid'])/(ddepth['ask']+ddepth['bid'])/2)/ddepth['volume']
    ddepth['DTV']=ddepth['volume']*(ddepth['ask']+ddepth['bid'])/2
    ddepth['AILLIQ']=ddepth['volume']*abs(ddepth['lastPrice']/ddepth['lastPrice'].shift(1))/ddepth['DTV']
    if otype=='':
        print('Options Liquidity Measures - Mean/Median')
    else:
        print(otype + 's Liquidity Measures - Mean/Median')
        
    if exp !='': plotOptionLiquidity(ddepth,symbol,otype,exp)    
    
    #print('----------------------------------------')
    #print('Proportional Bid-Ask Spread: ',ddepth['PBA'].mean().round(4), ddepth['PBA'].median().round(4))
    #print('Volume: ',ddepth['volume'].mean().round(4),ddepth['volume'].median().round(4))
    #print('Dollar Trading Volume: ',ddepth['DTV'].mean().round(4),ddepth['DTV'].median().round(4))
    ddepth2=ddepth['AILLIQ'].replace([np.inf, -np.inf], np.nan).dropna()
    #print('AILLIQ: ',ddepth2.mean().round(4),ddepth2.median().round(4))
    #print('')
    
    pd.options.display.float_format = '{:,.4f}'.format
    idx=['Proportional Bid-Ask Spread','Volume','Dollar Trading Volume','AILLIQ']
    ar=[[ddepth['PBA'].mean().round(4), ddepth['PBA'].median().round(4)],
        [ddepth['volume'].mean().round(4),ddepth['volume'].median().round(4)],
        [ddepth['DTV'].mean().round(4),ddepth['DTV'].median().round(4)],
        [ddepth2.mean().round(4),ddepth2.median().round(4)]]
    df=pd.DataFrame(ar,columns=['Mean','Median'],index=idx)
    print(df)
    
    return ddepth

def printSecurityLiqStats(prices,symbol=''):
    
    ddepthp=prices[(prices['Open']!=0) & (prices['High']!=0) & (prices['Low']!=0) & (prices['Close']!=0) & (prices['Volume']!=0)]
    ddepthp['PBA']=(ddepthp['Volume']*(ddepthp['High']-ddepthp['Low'])/(ddepthp['High']+ddepthp['Low'])/2)/ddepthp['Volume']
    ddepthp['DTV']=ddepthp['Volume']*(ddepthp['High']+ddepthp['Low'])/2
    ddepthp['AILLIQ']=ddepthp['Volume']*abs(ddepthp['Adjusted_close']/ddepthp['Adjusted_close'].shift(1))/ddepthp['DTV']
    
    plotStockLiquidity(ddepthp,symbol)
    
    print('Stock Liquidity Measures')
    #print('----------------------------------------')
    #print('Proportional Bid-Ask Spread: ',ddepthp['PBA'].mean().round(4), ddepthp['PBA'].median().round(4))
    #print('Volume: ',ddepthp['Volume'].mean().round(4),ddepthp['Volume'].median().round(4))
    #print('Dollar Trading Volume: ',ddepthp['DTV'].mean().round(4),ddepthp['DTV'].median().round(4))
    ddepthp2=ddepthp['AILLIQ'].replace([np.inf, -np.inf], np.nan).dropna()
    #print('AILLIQ: ',ddepthp2.mean().round(4),ddepthp2.median().round(4))
    #print('')
    
    pd.options.display.float_format = '{:,.4f}'.format
    idx=['Proportional Bid-Ask Spread','Volume','Dollar Trading Volume','AILLIQ']
    ar=[[ddepthp['PBA'].mean().round(4), ddepthp['PBA'].median().round(4)],
        [ddepthp['Volume'].mean().round(4),ddepthp['Volume'].median().round(4)],
        [ddepthp['DTV'].mean().round(4),ddepthp['DTV'].median().round(4)],
        [ddepthp2.mean().round(4),ddepthp2.median().round(4)]]
    df=pd.DataFrame(ar,columns=['Mean','Median'],index=idx)
    print(df)



