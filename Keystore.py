#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Keystore 
#
# Module including keys for data and apis
#############################################################################################  

class Keystore:
    kibot_root = 'http://api.kibot.com'
    kibot_key = '&user=dsfsffdsf&password=sdfsfsdf'
    
    cotxls_root = '//Volumes/Datafarm/COT'
    cotxls_file = 'TradersCommitment2.hdf'
    cotxls_url = 'https://www.cftc.gov/files/dea/history/'
    
    cot_root='//Volumes/Datafarm/COT'
    cot_file='TradersCommitment.hdf'
    cot_url_latest="http://www.cftc.gov/dea/futures/deacmesf.htm"
    cot_url='http://www.cftc.gov/files/dea/cotarchives/%year%/futures/deacmesf%month%%day%%Year%.htm'
    
    hcurr_root='//Volumes/Datafarm/Compustat_exports'
    hcurr_file='forex since 1960.csv'
    
    hfutsp_root='//Volumes/Datafarm/hfutures/Continuous Futures/Spliced'
    hfutcash_root='//Volumes/Datafarm/hfutures/Cash'
    hsgfut_root='//Volumes/Datafarm/hfutures/Futures'
    hfuturedata_root='//Volumes/Datafarm/hfutures'
    
    frd_ip='https://firstratedata.com/c/main/sdfsdf-sdfsdfsf'
    save_dir='//Volumes/Datafarm/frdata'
    store_dir='//Volumes/Datafarm'
    backup_dir='//Volumes/Datafarm/frdata'
    
    frd_indices='https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/14718'
    frd_indices_all=['https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/12248']
    frd_indices_name='us_indices'
    frd_indices_cols=['Date','Open', 'High', 'Low', 'Close']
    frd_indices_tz='US/Eastern'
    
    frd_etfs='https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/14038'
    frd_etfs_all=['https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13502',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13507',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13512',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13517',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13522',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13527']   
    frd_etfs_uadj='https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/14037'
    frd_etfs_all_uadj=['https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13506',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13511',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13516',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13521',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13526',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13531']
    frd_etfs_name='etf'
    frd_etfs_cols=['Date','Open', 'High', 'Low', 'Close', 'Volume']
    frd_etfs_tz='US/Eastern'
    
    frd_forex='https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/14704'
    frd_forex_all=['https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/12260']
    frd_forex_name='forex'
    frd_forex_cols=['DateNH','Hour','Open', 'High', 'Low', 'Close', 'Volume']
    frd_forex_tz='UTC'
    
    frd_stocks='https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/14033'
    frd_stocks_all=['https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13580',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13585',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13590',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13595',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13600',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13605',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/12509',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13610',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13615',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13620',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13625',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13630',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13635']
    frd_stocks_uadj='https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/14032'
    frd_stocks_all_uadj=['https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13584',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13589',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13594',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13599',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13604',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13609',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/12513',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13614',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13619',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13624',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13629',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13634',
                'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13639']
    frd_stocks_name='us_stocks'
    frd_stocks_cols=['Date','Open', 'High', 'Low', 'Close', 'Volume']
    frd_stocks_tz='US/Eastern'
    
    frd_futures='https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/14832'
    frd_futures_uadj='https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/14832'
    frd_futures_all_uadj=['https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13742'] #cont unadj
    frd_futures_all=['https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13738']
    frd_futures_name='us_futures'
    frd_futures_cols=['Date','Open', 'High', 'Low', 'Close', 'Volume']
    frd_futures_tz='US/Eastern'
    
    frd_Ifutures='https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/11422'  #post2021
    frd_Ifutures_uadj='https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/11422'  #post2021
    frd_Ifutures_all=['https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/11421'] #pre2021 2.2gb
    frd_Ifutures_all_uadj=['https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/11421'] #pre2021 2.2gb
    frd_Ifutures_name='us_Ifutures'
    frd_Ifutures_cols=['Date','Open', 'High', 'Low', 'Close', 'Volume']
    frd_Ifutures_tz='US/Eastern'
    
    frd_crypto='https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/13788'
    frd_crypto_all=['https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/12627']
    frd_crypto_name='crypto'
    frd_crypto_cols=['Date','Open', 'High', 'Low', 'Close', 'Volume']
    frd_crypto_tz='UTC'
    
    #frd_index='https://firstratedata.com/c/main/fsdfsd-sdfsdfsf#nav-index-us3000'
    frd_index='https://financialdatadb.com/files/index_change_ujgyh9i.zip'
    frd_index_name='us_indexchanges'
    
    frd_splits_etfs='https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/14990'
    frd_splits_etfs_name='us_etfssplits'
    frd_divs_etfs='https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/14989'
    frd_divs_etfs_name='us_etfsdivs'
    
    frd_splits_stocks='https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/14998'
    frd_splits_stocks_name='us_stockssplits'
    frd_divs_stocks='https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/14997'
    frd_divs_stocks_name='us_stocksdivs'
    
    frd_fdata='https://financialdatadb.com/files/us_financials_jgnh7gj.zip'
    frd_fdata_name='us_financials'
    
    frd_delisted='https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/14838'
    frd_delisted_all=['https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/14838',
                      'https://firstratedata.com/datafile/fsdfsd-sdfsdfsf/15001']
    frd_delisted_name='us_delisted'
    frd_delisted_cols=['Date','Open', 'High', 'Low', 'Close', 'Volume']
    frd_delisted_tz='US/Eastern'    
    
    orats_ftp='ftp.HostedFTP.com'   # connect to host, default port
    orats_uid='sdfsdfsdf'
    orats_pwd='sdfsfsdf@'  
    orats_dir1='smvstrikes_2007_2012'
    orats_dir2='smvstrikes'
    orats_savedir='//Volumes/Datafarm/orats'
    orats_storedir='//Volumes/Datafarm/options'
    orats_backupdir='//Volumes/Datafarm/orats'  
    
    alpha_vantage_key='sdfsf'
    alpha_vantage_root='https://www.alphavantage.co/query?function='

    barchart_key='sdfsdfs'
    barchart_root='https://marketdata.websol.barchart.com/service?wsdl'   

    eod_key='sdffs.sdfsfd'
    eod_root='https://eodhistoricaldata.com/api/'   
    
    finnhub_key='sdfsd'
    finnhub_key2='sandbox_bujvm3sdfsdf0'
    finnhub_root='https://finnhub.io/api/v1'

    fprep_key='sdfsdf'
    fprep_root='https://financialmodelingprep.com/api/v3'
    fprep_root2='https://fmpcloud.io/api/v3/'

    fred_api="sdfsdffs"
    
    eia_key="sdfsd"
    eia_root='https://api.eia.gov/series'

    iex_key='sdfsdfs'
    iex_root='https://cloud.iexapis.com/stable'
    iex_keyp='sdfsdf'
    iex_rootp='https://sandbox.iexapis.com/stable'

    mboum_key='sdfsdfsdfsdfsdf'
    mboum_root='https://mboum.com/api/v1'
                
    mstack_key='sdfsdfsf'
    mstack_root='https://api.marketstack.com/v1' 

    quandl_key = 'sdfsdff'
    quandl_csv_path= '/Users/rob/Desktop/PythonCode'

    tiingo_key='sdfsdfsdfs'
    tiingo_root='https://api.tiingo.com/tiingo'

    imf_root1= 'http://dataservices.imf.org/REST/SDMX_JSON.svc/'
    imf_root2 = 'http://dataservices.imf.org/REST/SDMX_JSON.svc/CompactData/'

    cal_path='/Plib/Utils'
    # If modifying these scopes, delete the file token.pickle.
    cal_SCOPES = ['https://www.googleapis.com/auth/calendar']
    cal_cred_file1 = '/token.pickle'
    cal_cred_file2 = '/credentials.json'

    web_driver_path='/Plib/Utils/webdrivers/'
    #https://github.com/operasoftware/operachromiumdriver/releases
    #chmod 0755 operadriver
    
    def __init__(self):
        pass
    

    
