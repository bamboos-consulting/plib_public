#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# TAnalysis 
#
# Module including functions to obtain technical analysis signals
#############################################################################################      

import pandas as pd
import numpy as np
import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

#################################################
# Exprected returns
#################################################         
def ER(ret,wins=0):
    er=abs(ret-ret[-1])/abs(ret-ret.shift(-1)).sum()
    if wins>0:
        er=er.clip(lower=er.quantile(wins),
            upper=er.quantile(1-wins),axis=0) #was axis=1
    return er

#################################################
# Computed Adjusted OHLCV from Adj Close
#################################################      
def adjustOHLCV(df,lbl_adj='Adj Close',lbl_unadj='Close'):
    #The effect of a series of adjustments due to dividend payments on the price series 
    #is a non-linear compression from current to past data or, in other terms, past data 
    #were changed based on future data.. Thus, adjusted data is not appropriate for 
    #technical analysis or backtesting but it is only good for buy and hold performance
    #Only adjusted for split data is good for technical analysis and backtesting because
    #it is forward looking only
    df['factor'] = df[lbl_adj] / df[lbl_unadj]
    df['Adj High']= df['High'] * df['factor']
    df['Adj Low']= df['Low'] * df['factor']
    df['Adj Open']= df['Open'] * df['factor']
    df['Adj Volume']= df['Volume'] / df['factor']
    return df

#################################################
# Fast and Brute removal of outliers
#################################################         
def removeOutliers(df,params={'method':'stdev','arg1': 3,'iq':(0.25,0.75)}):
    import numpy as np
    
    #params1={'method':'stdev','arg1': 3}
    #params2={'method':'iqrange','arg1': 3}

    series=df.columns
    for s in series:
        if params['method']=='stdev':
            data_mean,data_std=np.mean(df[s]),np.std(df[s])
            cut_off = data_std * params['arg1']
            lower,upper=(data_mean - cut_off),(data_mean + cut_off)
        elif params['method']=='iqrange':
            q25, q75 = df[s].quantile(params['iq'][0]),df[s].quantile(params['iq'][1])
            iqr = q75 - q25
            cut_off = iqr * params['arg1']
            lower, upper = (q25 - cut_off), (q75 + cut_off)
        outliers=[x for x in df[s] if (x > (upper)) or (x < (lower))]
        print('Series: '+str(s)+' outliers: %d' % len(outliers))
        print(outliers)
        median=df[s].median()
        df[s]=np.where((df[s] > upper), median, df[s])
    return df

#################################################
# Slow but Easy to Rolling any function
#################################################         
def rolling(df,fun,lbl_comp='Close',win=5,lbl_out=''):
    lbl=lbl_out
    if lbl_out=='': lbl='roll_fun_'+str(win)
    cols=df.columns
    index=df.index
    df=df.reset_index()
    df[lbl]=0
    for i in range(0,len(df)-win+1):
        df[lbl].iloc[i+win-1]=fun(df.iloc[i:i+win])
    df=df[list(cols)+[lbl]]
    df.index=index
    return df
    
def rolling2(s1,s2,fun,win=5,lbl_out=''):
    sr=s1.copy()
    sr.name='corr'
    for i in range(0,len(df)-win+1):
        sr.iloc[i:i+win],p = fun(s1.iloc[i:i+win], s2.iloc[i:i+win])
    return sr

def rollRegr(s1,fun,win=5,lbl_out=''):
    from numpy import NaN

    sr=s1.copy()
    sr.name='roll_reg'
    for i in range(0,len(df)-win+1):
        yhat,mse,rlm = fun(s1.iloc[i:i+win])
        sr.iloc[i:i+win] = float(rlm.steps[1][1].coef_)
    return sr
    
#################################################
# Simple moving average 
#################################################      
def getSMA(df,lbl_comp='Close',win=15,lbl_out='',s=0):
    # shifted to avoid bias
    # zeroed in NaN
    lbl=lbl_out
    if lbl_out=='': lbl='sma_'+str(win)
    df[lbl]=df[lbl_comp].shift(s).rolling(win).mean()
    df[lbl][:win]=0
    return df

#################################################
# Multiple SMA
#################################################          
def getSMAs(df,lbl_comp='Close',periods=[3,15,60],lbl_out=''):
    for p in periods:
        df=getSMA(df,lbl_comp=lbl_comp,win=p,lbl_out=lbl_out)
    return df

#################################################
# Exp moving average 
#################################################      
def getEMA(df,lbl_comp='Close',win=15,lbl_out='',s=0):
    # shifted to avoid bias
    # zeroed in NaN
    lbl=lbl_out
    if lbl_out=='': lbl='ema_'+str(win)
    df[lbl]=df[lbl_comp].shift(s).ewm(span=win, adjust=False).mean()
    df[lbl][0]=0
    return df

#################################################
# Velocity, Acceleration, Jerk
#################################################      
def vaj(df,n, lbl='Close'):
    import Plib.Signals.Filters as flt

    def change(df,n, lbl='Close'):
        return (df[lbl]/df[lbl].shift(n))
    
    df['velocity_'+str(n)]= flt.movingaverage(change(df,n, lbl=lbl),3)
    df['acceleration_'+str(n)]=flt.movingaverage(change(df,n, lbl='velocity_'+str(n)),3)
    df['jerk_'+str(n)]=flt.movingaverage(change(df,n, lbl='acceleration_'+str(n)),3)
    df['velocity_'+str(n)]=df['velocity_'+str(n)]-1
    df['acceleration_'+str(n)]=df['acceleration_'+str(n)]-1
    df['jerk_'+str(n)]=df['jerk_'+str(n)]-1
    return df

#################################################
# Bollinger Bands - TP/SL by using bands
#################################################      
def getBollingerBands(df,lbl_comp='Close', winma=20, sdev=2,lbl_out=[],s=0):
    # shifted to avoid bias
    # zeroed in NaN
    
    if lbl_out==[]: 
        lbl1='BB_ma_'+str(winma)
        lbl2='BB_hb_sd_'+str(sdev)
        lbl3='BB_lb_sd_'+str(sdev)
    else:
        lbl1=lbl_out[0]
        lbl2=lbl_out[1]
        lbl3=lbl_out[2]    
    ser = pd.Series(df[lbl_comp])
    ma = ser.shift(s).rolling(winma).mean()
    std = ser.shift(s).rolling(winma).std()
    lower = pd.Series(ma - sdev * std).fillna(method='bfill').values
    upper = pd.Series(ma + sdev * std).fillna(method='bfill').values
    
    df[lbl1]=ma
    df[lbl2]=upper
    df[lbl3]=lower
    df[lbl1][:winma]=0
    df[lbl2][:winma]=0
    df[lbl3][:winma]=0
    return df

#################################################
# Average True Range - TP/SL by adding ATR
#################################################          
def getATR(df,lbl_comp='Close', winma=5,type='SMA',lbl_out='',s=0):
    # shifted to avoid bias
    # zeroed in NaN
    # considers gaps 
    lbl=lbl_out
    if lbl_out=='': 
        lbl='ATR'+str(winma)
        
    high = df['High']
    low = df['Low']
    close = df[lbl_comp]
    df['tr0'] = abs(high - low)
    df['tr1'] = abs(high - close.shift(1))
    df['tr2'] = abs(low - close.shift(1))
    df['TR']=df[['tr0', 'tr1', 'tr2']].max(axis=1)
    if type=='EMA':
        df[lbl] = df['TR'].shift(s).ewm(alpha=1/winma, min_periods=winma, adjust=False).mean()
        df[lbl][:winma]=0
    else:
        df[lbl] = df['TR'].shift(s).rolling(winma).mean()
        df[lbl] = ( df[lbl].shift(1)*(winma-1) + df['TR'] ) /  winma
        df[lbl][:winma+1]=0
        del df['TR']
    del df['tr0']
    del df['tr1']
    del df['tr2']
    df['HCW']=df['High'].shift(s).rolling(winma).max()
    df['LCW']=df['Low'].shift(s).rolling(winma).min()
    
    return df

#################################################
# Midpoint Support/Resistance
#################################################          
def getMidpointSR(df, winroll=252, sr=0.25, lbl_out='', s=0):
    # shifted to avoid bias
    # zeroed in NaN
    # considers gaps 
    lbl=lbl_out
    if lbl_out=='': 
        lbl='MDP'+str(winroll)
        lbls='MDP_supp'
        lblr='MDP_resist'
        
    df[lbl] = (df['High'].shift(s).rolling(winroll).max() + df['Low'].shift(s).rolling(winroll).min())/2
    df[lbl][:winroll]=0
    df[lbls] = df[lbl] * (1-sr)
    df[lblr] = df[lbl] * (1+sr)
    
    return df

#################################################
# VWAP with Std Bands 
#################################################      
def getVWAP(df,bands=True,sd_range=1.28,s=0):
    # shifted to avoid bias
    # zeroed in NaN
    
    import math
    #sd_range1 = 1.28
    #sd_range2 = 2.01
    #sd_range3 = 2.51
    
    v = df.Volume.values
    h = df.High.values
    c = 0 #df.Close.values
    l = df.Low.values
    df['VWAP'] = np.cumsum(v*(h+l+c)/3) / np.cumsum(v)
    df['VWAP'] = df.VWAP.shift(s)
    if bands:
        df['VWAP_MEAN_DIFF'] = ((df.High.shift(s) + df.Low.shift(s)) / 2) - df.VWAP
        df['SQ_DIFF'] = df.VWAP_MEAN_DIFF.apply(lambda x: math.pow(x, 2))
        df['SQ_DIFF_MEAN'] = df.SQ_DIFF.expanding().mean()
        df['STDEV_TT'] = df.SQ_DIFF_MEAN.apply(math.sqrt)
        df['STDEV_H'] = df.VWAP + sd_range * df['STDEV_TT']
        df['STDEV_L'] = df.VWAP - sd_range * df['STDEV_TT']    
        del df['VWAP_MEAN_DIFF']
        del df['SQ_DIFF']
        del df['SQ_DIFF_MEAN']
        del df['STDEV_TT']
        df['STDEV_H'][0]=0
        df['STDEV_L'][0]=0
    df['VWAP'][0]=0
    return df 

#################################################
# RSI with Exp, Sma, Fisher calculation 
#################################################          
def getRSI(df,lbl_comp='Close', winma=15,type='EMA',lbl_out='',s=0):
    # shifted to avoid bias
    # zeroed in NaN
    lbl=lbl_out
    if lbl_out=='': 
        lbl='RSI_'+str(winma)
    
    ret = df[lbl_comp].diff().fillna(0)
    up = []
    down = []
    for i in range(len(ret)):
        if ret[i] < 0:
            up.append(0)
            down.append(ret[i])
        else:
            up.append(ret[i])
            down.append(0)
    up_series = pd.Series(up)
    down_series = pd.Series(down).abs()
    if type=='EMA':
        up_ewm = up_series.shift(s).ewm(com = winma - 1, adjust = False).mean()
        down_ewm = down_series.shift(s).ewm(com = winma - 1, adjust = False).mean()
    else:
        up_ewm = up_series.shift(s).rolling(winma).mean()
        down_ewm = down_series.shift(s).rolling(winma).mean()
    rs = up_ewm/down_ewm
    rsi = 100 - (100 / (1 + rs))
    rsi_df = pd.DataFrame(rsi).rename(columns = {0:'rsi'}).set_index(df[lbl_comp].index)
    df[lbl]=rsi_df.fillna(100)
    if type=='EMA':
        df[lbl][:4]=0
    else:
        df[lbl][:winma]=0
    return df 

def getRSIFT(df,lbl_comp='Close', winma=15,type='EMA',lbl_out='',s=0):
    from Plib.Signals.Filters import ifishT
    from Plib.Utils.Tools import scale_number
    
    lbl=lbl_out
    if lbl_out=='': 
        lbl='RSI_'+str(winma)
        
    df=getRSI(df,lbl_comp, winma,type,lbl_out,s)
    df[lbl]=scale_number(df[lbl], -5, 5, 0, 100)
    df[lbl]=df[lbl].apply(ifishT)
    df[lbl]=scale_number(df[lbl], 0, 100, -5, 5)
    
    return df
    
#################################################
# Volume Gap indicator
#################################################          
def getVolumeGap(df,lbl_comp='Volume',lbl_out='',s=0):
    # shifted to avoid bias
    # zeroed in NaN
    lbl=lbl_out
    if lbl_out=='': lbl='volumeGap_'
    df[lbl]=np.log(df[lbl_comp].shift(s) / df[lbl_comp].shift(1+s))
    df[lbl][:1]=0
    return df

#################################################
# On Balance Volume indicator
#################################################          
def getOnBalanceVol(df,lbl_comp1='Close',lbl_comp2='Volume',lbl_out='',s=0):
    # shifted to avoid bias
    # zeroed in NaN    
    lbl=lbl_out
    if lbl_out=='': lbl='onbalVol'
    df[lbl]=np.where(df[lbl_comp1] > df[lbl_comp1].shift(1), df[lbl_comp2], 
                   np.where(df[lbl_comp1] < df[lbl_comp1].shift(1), -df[lbl_comp2], 0)).cumsum()
    if s !=0:
        df[lbl]=df[lbl].shift(s)
    if s>0:
        df[lbl][:s]=0
    elif s <0:
        df[lbl][s:]=0
    return df

#################################################
# Daily Change indicator
#################################################          
def getDailyChange(df,lbl_comp1='Close',lbl_comp2='Open',lbl_out='',s=0):
    lbl=lbl_out
    if lbl_out=='': lbl='dailyChange_'
    df[lbl]=(df[lbl_comp1].shift(s) - df[lbl_comp2].shift(s)) / df[lbl_comp2].shift(s)
    df[lbl][:1]=0
    return df

#################################################
# Market Up/Down indicator (1,-1)
#################################################          
def getMarketUpDown(df,lbl_comp='Close',lbl_out='',s=0):
    lbl=lbl_out
    if lbl_out=='': lbl='MarketUpDown_'+lbl_comp+str(1+s)+'day'
    df[lbl]= np.where(df[lbl_comp].shift(-1-s)>df[lbl_comp].shift(-s),1,-1)
    return df

#################################################
# Open Spread indicator
#################################################          
def getOpenSpread(df,lbl_comp1='High',lbl_comp2='Open',lbl_comp3='Low',lbl_out=['','','',''],s=0):
    lbl1=lbl_out[0]
    lbl2=lbl_out[1]
    lbl3=lbl_out[2]
    lbl4=lbl_out[3]
    if lbl_out[0]=='': lbl1='OD_'
    if lbl_out[1]=='': lbl2='OL_'
    if lbl_out[2]=='': lbl3='fractHigh_'
    if lbl_out[3]=='': lbl4='fractLow_'
        
    df[lbl1] = df[lbl_comp2].shift(s)-df[lbl_comp2].shift(1+s)
    df[lbl2] = df[lbl_comp2].shift(s)-df[lbl_comp1].shift(1+s)
    df[lbl1][:1]=0
    df[lbl2][:1]=0
    df[lbl3] = (df[lbl_comp1] - df[lbl_comp2]) / df[lbl_comp2]
    df[lbl4] = (df[lbl_comp2] -  df[lbl_comp3]) / df[lbl_comp2]
    return df

#################################################
# Lagged Returns
#################################################          
def getLaggedRets(df,lbl_comp='Close',periods=[3,15,30],lbl_out='',wins=0):
    lbl=lbl_out
    if lbl_out=='': lbl='lagRets_'
    for p in periods:
        df[lbl+str(p)]=df[lbl_comp].pct_change(periods=p).mul(100)
        df[lbl+str(p)][:p]=0
        if wins>0:
            er=df[lbl+str(p)]
            er=er.clip(lower=er.quantile(wins),
                upper=er.quantile(1-wins),axis=0) # was axis=1
            df[lbl+str(p)]=er
    return df

#################################################
# Lagged LogReturns
#################################################          
def getLaggedLogRets(df,lbl_comp='Close',periods=[3,15,30],lbl_out='',wins=0):
    lbl=lbl_out
    if lbl_out=='': lbl='lagRets_'
    for p in periods:
        df[lbl+str(p)]=np.log(df[lbl_comp]).diff(p).mul(100)
        df[lbl+str(p)][:p]=0
        if wins>0:
            er=df[lbl+str(p)]
            er=er.clip(lower=er.quantile(wins),
                upper=er.quantile(1-wins),axis=0) # was axis=1
            df[lbl+str(p)]=er
    return df

#################################################
# Internal bar Strength
#################################################          
def getIntBarStrength(df,lbl_comp1='Close',lbl_comp2='Low',lbl_comp3='High',lbl_out=''):
    lbl=lbl_out
    if lbl_out=='': lbl='intBarStr_'
    df[lbl]=(df[lbl_comp1] - df[lbl_comp2]) / (df[lbl_comp3] - df[lbl_comp2])
    return df

#################################################
# Average of the bars
#################################################              
def getAverageBar(df,lbl_comp1='Close',lbl_comp2='Low',lbl_comp3='High',lbl_comp4='Open',lbl_out=''):
    lbl=lbl_out
    if lbl_out =='': lbl='avgBar_'
    df[lbl] = (df[lbl_comp1] + df[lbl_comp2] + df[lbl_comp3] + df[lbl_comp4]) / 4
    return df

#################################################
# Momentum: Annual. exponential regression slope, 
#            multiplied by the R2 bar Strength
#################################################          
def getMomentum(df,lbl_comp='Close',lbl_out=''):
    lbl=lbl_out
    if lbl_out=='': lbl='momentum_'
    y = np.diff(np.log(df[lbl_comp]))
    x = np.arange(y.shape[0])
    A = np.column_stack((x, np.ones(x.shape[0])))
    model, resid = np.linalg.lstsq(A, y)[:2]
    r2 = 1 - resid / (y.size * y.var())
    df[lbl]= (((1 + model[0]) ** 252) * r2)[0]
    return df

def getSlope(df,lbl_comp='Close',lbl_out=''):
    lbl=lbl_out
    if lbl_out=='': lbl='momentum_'
    y = np.diff(np.log(df[lbl_comp]))
    x = np.arange(y.shape[0])
    A = np.column_stack((x, np.ones(x.shape[0])))
    model, resid = np.linalg.lstsq(A, y)[:2]
    return model[0]

#################################################
# Percentile and Rank
#################################################          
def getPercentile(df,lbl_comp='Close',p=10,lbl_out=''):
    lbl=lbl_out
    if lbl_out =='': lbl='percentile_'+str(p)+'_'
    df[lbl] = np.percentile(df[lbl_comp], p)
    return df

def getRank(df,lbl_comp='Close',method='average',lbl_out=''):
    from scipy.stats import rankdata
    lbl=lbl_out
    if lbl_out =='': lbl='rank_'+str(method)+'_'
    df[lbl] = rankdata(df[lbl_comp], method=method)
    return df

#################################################
# Rolling Min and Max
#################################################          
def getRollingMin(df,lbl_comp='Close',win=10,lbl_out=''):
    lbl=lbl_out
    if lbl_out =='': lbl='rollMin_'
    df[lbl] =df[lbl_comp].rolling(window = win).min()
    return df

def getRollingMax(df,lbl_comp='Close',win=10,lbl_out=''):
    lbl=lbl_out
    if lbl_out =='': lbl='rollMax_'
    df[lbl] =df[lbl_comp].rolling(window = win).max()
    return df

#################################################
# Rolling Realized Volatility
#################################################          
def getRollVol(df,lbl_comp='Close',win=21,lbl_out=''):
    from Plib.Volatility.Estimation import volrealized
    lbl=lbl_out
    if lbl_out =='': lbl='rollVol_'+str(win)
    mfun = lambda x: volrealized(x)
    df[lbl] = (df[lbl_comp].pct_change().rolling(window=win).agg(mfun)).dropna()
    return df

#################################################
# Rolling VWAP
#################################################          
def rollingVWAP(df,lbl_comp='Close',win=5,lbl_out=''):
    if lbl_out == '': lbl_out='rollVWAP_'+str(win)
    mfun=lambda x: np.asarray(getVWAP(x,bands=False).VWAP)[-1]
    return rolling(df,mfun,lbl_comp=lbl_comp,win=win,lbl_out=lbl_out)
 
#################################################
# Estimated Autocorrelation
#################################################              
def estimated_autocorrelation(df,lbl_comp='Open',lbl_out=''):
    #http://stackoverflow.com/q/14297012/190597
    #http://en.wikipedia.org/wiki/Autocorrelation#Estimation
    x=df[lbl_comp].values
    lbl=lbl_out
    if lbl_out=='': lbl='estAutocorr_'+lbl_comp
    
    n = len(x)
    variance = x.var()
    x = x-x.mean()
    r = np.correlate(x, x, mode = 'full')[-n:] #spearman is better
    #assert np.allclose(r, np.array([(x[:n-k]*x[-(n-k):]).sum() for k in range(n)]))
    result = r/(variance*(np.arange(n, 0, -1)))
    df[lbl]=result
    return df

#################################################
# Close Price Correlation with SMA
#################################################          
def getSMACorr(df,lbl_comp='Close',win=10,lbl_sma='',lbl_out='',s=0):
    lbl=lbl_out
    if lbl_out=='': lbl='corr_'+lbl_sma
    df[lbl] = df[lbl_comp].shift(1+s).rolling(window=win).corr(df[lbl_sma].shift(1+s)) #spearman
    df[lbl][:win]=0
    return df

#################################################################
# Peaks Detection
# A point is considered a maximum peak if it has the maximal
#        value, and was preceded (to the left) by a value lower by
#       DELTA.
# Eli Billauer, 3.4.05 (Explicitly not copyrighted).
# This function is released to the public domain; Any use is allowed.
#################################################################    
def findPeaks(v, delta, x = None, start_max= False, plot=False):
    from numpy import NaN, Inf, arange, isscalar, asarray, array
    import numpy as np
    
    maxtab = []
    mintab = []
       
    if x is None:
        x = arange(len(v))
    
    v = asarray(v)
    
    if len(v) != len(x):
        print('Input vectors v and x must have same length')
        return [],[]
    
    if not isscalar(delta):
        print('Input argument delta must be a scalar')
        return [],[]
    
    if delta <= 0:
        print('Input argument delta must be positive')
        return [],[]
    
    mn, mx = Inf, -Inf
    mnpos, mxpos = NaN, NaN
    
    lookformax = start_max
    
    for i in arange(len(v)):
        this = v[i]
        if this > mx:
            mx = this
            mxpos = x[i]
        if this < mn:
            mn = this
            mnpos = x[i]
        
        if lookformax:
            if this < mx-delta:
                maxtab.append((mxpos, mx))
                mn = this
                mnpos = x[i]
                lookformax = False
        else:
            if this > mn+delta:
                mintab.append((mnpos, mn))
                mx = this
                mxpos = x[i]
                lookformax = True
                
    maxtab, mintab = array(maxtab), array(mintab)
    if plot:
        from matplotlib.pyplot import plot, scatter, show
        plot(v)
        scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
        scatter(array(mintab)[:,0], array(mintab)[:,1], color='red')
        show()
    return maxtab, mintab

#################################################################
# Peaks Detection
# Detect peaks with minimum height and distance filters    
#################################################################     
def findPeaksHD(v,height=7, distance=2.1, plot=False):
    import scipy.signal
    import numpy as np
    import matplotlib.pyplot as plt 

    maxtab, _ = scipy.signal.find_peaks(v, height=height, distance=distance)
    mintab, _ = scipy.signal.find_peaks(-v, height=height, distance=distance)
    if plot:
        plt.plot(v)
        plt.plot(maxtab, v[maxtab], "x")
        plt.plot(mintab, v[mintab], "xr")
        plt.show()
    return maxtab, mintab

#################################################################
# Peaks and valleys
# up_thresh: minimum relative change necessary to define a peak
# down_thesh: minimum relative change necessary to define a valley
# idx: index of the data
#################################################################     
def peak_valley_pivots(X,up_thresh, down_thresh, idx, plot=False,stats=True):

    PEAK = 1
    VALLEY = -1

    def identify_initial_pivot( X,up_thresh,down_thresh):
        #The first and last elements are guaranteed to be annotated as peak or
        #valley even if the segments formed do not have the necessary relative
        #changes. This is a tradeoff between technical correctness and the
        #propensity to make mistakes in data analysis. The possible mistake is
        #ignoring data outside the fully realized segments, which may bias
        #analysis.
     
        
        x_0 = X[0]
        x_t = x_0
        max_x = x_0
        min_x = x_0
        max_t = 0
        min_t = 0

        up_thresh += 1
        down_thresh += 1
        for t in range(1, len(X)):
            x_t = X[t]

            if x_t / min_x >= up_thresh:
                return VALLEY if min_t == 0 else PEAK
            if x_t / max_x <= down_thresh:
                return PEAK if max_t == 0 else VALLEY
            if x_t > max_x:
                max_x = x_t
                max_t = t
            if x_t < min_x:
                min_x = x_t
                min_t = t
        t_n = len(X)-1
        return VALLEY if x_0 < X[t_n] else PEAK

    if down_thresh > 0:
        raise ValueError('The down_thresh must be negative.')
    initial_pivot = identify_initial_pivot(X,up_thresh, down_thresh)
    t_n = len(X)
    pivots = np.zeros(t_n)
    trend = -initial_pivot
    last_pivot_t = 0
    last_pivot_x = X[0]
    pivots[0] = initial_pivot
    
    up_thresh += 1
    down_thresh += 1

    for t in range(1, t_n):
        x = X[t]
        r = x / last_pivot_x
        if trend == -1:
            if r >= up_thresh:
                pivots[last_pivot_t] = trend
                trend = PEAK
                last_pivot_x = x
                last_pivot_t = t
            elif x < last_pivot_x:
                last_pivot_x = x
                last_pivot_t = t
        else:
            if r <= down_thresh:
                pivots[last_pivot_t] = trend
                trend = VALLEY
                last_pivot_x = x
                last_pivot_t = t
            elif x > last_pivot_x:
                last_pivot_x = x
                last_pivot_t = t
    if last_pivot_t == t_n-1:
        pivots[last_pivot_t] = trend
    elif pivots[t_n-1] == 0:
        pivots[t_n-1] = -trend
    
    ts_pivots = pd.Series(X, index=idx)
    
    if plot:
        #plt.xlim(0, len(X))
        #plt.ylim(X.min()*0.99, X.max()*1.01)
        #plt.plot(np.arange(len(X)), X, 'k:', alpha=0.5) #series
        #plt.plot(np.arange(len(X))[pivots != 0], X[pivots != 0], 'k-') #zigzag
        #ts_pivots = pd.Series(X, index=idx)
        ts_pivots.plot()
        ts_pivots = ts_pivots[pivots != 0]
        ts_pivots.plot(style='g-o');
        #plt.scatter(np.arange(len(X))[pivots == 1], X[pivots == 1], color='g')
        #plt.scatter(np.arange(len(X))[pivots == -1], X[pivots == -1], color='r')
    result=ts_pivots
    
    if stats:
        df_res=pd.DataFrame(ts_pivots)
        df_res['Returns']=ts_pivots.pct_change()
        df_res['Cumrets']=(1+ts_pivots.pct_change()).cumprod()-1
        df_res['Duration']=df_res.index
        df_res['Duration']=df_res.Duration.shift(-1)-df_res.Duration
        win=df_res[df_res.Returns > 0]
        win_mean=win['Returns'].mean()
        win_durat=win.Duration.mean()
        win_consist=len(win)/len(df_res)
        los=df_res[df_res.Returns < 0]
        los_mean=los['Returns'].mean()
        los_durat=los.Duration.mean()
        los_consist=len(los)/len(df_res)
        stats=(win_mean,win_durat,win_consist,los_mean,los_durat,los_consist)
        result=ts_pivots,pd.DataFrame(stats,index=['Peaks returns','Peaks duration','Peaks consistency','Valleys returns','Valleys duration','Valleys consistency',],columns=['Stats'])
    return result

def peak_valley_pivots_candlestick(df, up_thresh, down_thresh):
    #Finds the peaks and valleys of a series of HLC (open is not necessary).
    #TR: This is modified peak_valley_pivots function in order to find peaks and valleys for OHLC.
    #Returns an array with 0 indicating no pivot and -1 and 1 indicating valley and peak
    #The first and last elements are guaranteed to be annotated as peak or
    #valley even if the segments formed do not have the necessary relative
    #changes.
    
    
    close, high, low = df.Close, df.High, df.Low
    
    if down_thresh > 0:
        raise ValueError('The down_thresh must be negative.')

    initial_pivot = _identify_initial_pivot(close, up_thresh, down_thresh)

    t_n = len(close)
    pivots = np.zeros(t_n, dtype='i1')
    pivots[0] = initial_pivot

    # Adding one to the relative change thresholds saves operations. Instead
    # of computing relative change at each point as x_j / x_i - 1, it is
    # computed as x_j / x_1. Then, this value is compared to the threshold + 1.
    # This saves (t_n - 1) subtractions.
    up_thresh += 1
    down_thresh += 1

    trend = -initial_pivot
    last_pivot_t = 0
    last_pivot_x = close[0]
    for t in range(1, len(close)):

        if trend == -1:
            x = low[t]
            r = x / last_pivot_x
            if r >= up_thresh:
                pivots[last_pivot_t] = trend#
                trend = 1
                #last_pivot_x = x
                last_pivot_x = high[t]
                last_pivot_t = t
            elif x < last_pivot_x:
                last_pivot_x = x
                last_pivot_t = t
        else:
            x = high[t]
            r = x / last_pivot_x
            if r <= down_thresh:
                pivots[last_pivot_t] = trend
                trend = -1
                #last_pivot_x = x
                last_pivot_x = low[t]
                last_pivot_t = t
            elif x > last_pivot_x:
                last_pivot_x = x
                last_pivot_t = t


    if last_pivot_t == t_n-1:
        pivots[last_pivot_t] = trend
    elif pivots[t_n-1] == 0:
        pivots[t_n-1] = trend


    #pivots = peak_valley_pivots_candlestick(df.Close, df.High, df.Low ,.01,-.01)
    #df['Pivots'] = pivots
    #df['Pivot Price'] = np.nan  # This line clears old pivot prices
    #df.loc[df['Pivots'] == 1, 'Pivot Price'] = df.High
    #df.loc[df['Pivots'] == -1, 'Pivot Price'] = df.Low
    
    return pivots
       
#################################################################
# ZigZag Indicator
# Return peaks min-max where the absolute difference between 
# the previous and current extreme needs to be bigger than p%
# and k   
#################################################################     
def getZigZag(df, lbl='Close', p=0.07, k=1.2):
    df1=df[[lbl]]
    myfunct = lambda y: create_zigzag(y, p=p, k=k)
    dfzigzag = df1.apply(myfunct)
    dfzigzag['ZigZag']=np.where(dfzigzag[lbl] < dfzigzag[lbl].shift(-1),-1,1)
    dfzigzag.columns=['ZZ_values','ZigZag']
    df1=pd.merge(df, dfzigzag, how="outer", left_index=True, right_index=True).fillna(0)
    df2=df1[['ZZ_values']]
    df2=df2[df2.values.sum(axis=1) != 0]
    return df1,df2

def create_zigzag(col, p=0.07, k=1.2):
    
    def islocalmax(x):
        #Both neighbors are lower,
        #assumes a centered window of size 3"""
        return (x[0] < x[1]) & (x[2] < x[1])

    def islocalmin(x):
        #"""Both neighbors are higher,
        #assumes a centered window of size 3"""
        return (x[0] > x[1]) & (x[2] > x[1])

    def isextrema(x):
        return islocalmax(x) or islocalmin(x)
    
    # Find the local min/max
    # converting to bool converts NaN to True, which makes it include the endpoints    
    ext_loc = col.rolling(3, center=True).apply(isextrema, raw=False).astype(np.bool_)

    # extract values at local min/max
    ext_val = col[ext_loc]

    # filter locations based on threshold
    thres_ext_loc = (ext_val.diff().abs() > (ext_val.shift(-1).abs() * p)) & (ext_val.diff().abs() > k)

    # Keep the endpoints
    thres_ext_loc.iloc[0] = True
    thres_ext_loc.iloc[-1] = True

    thres_ext_loc = thres_ext_loc[thres_ext_loc]

    # extract values at filtered locations 
    thres_ext_val = col.loc[thres_ext_loc.index]

    # again search the extrema to force the zigzag to always go from high > low or vice versa,
    # never low > low, or high > high
    ext_loc = thres_ext_val.rolling(3, center=True).apply(isextrema, raw=False).astype(np.bool_)
    thres_ext_val  =thres_ext_val[ext_loc]

    return thres_ext_val


#################################################
# Issues Selection Functions 
#################################################                     
def getZigZagStats(df, lbl='Close', perc=0.07):
    return peak_valley_pivots(df[lbl], perc, -perc, df.index, plot=False,stats=True)[1]

def getHighs(df, lbl='Close', periods=60, last=20):
    ret = df[lbl].rolling(min_periods=1, window=periods, center=False).max().tail(last).unique()
    return len(ret)

def getDVolume(df, lbl1='Close', lbl2='Volume', periods=60, treshold=10**7):
    avgP = df[lbl1].rolling(min_periods=1, window=periods, center=False).mean()
    avgV = df[lbl2].rolling(min_periods=1, window=periods, center=False).mean()
    DV=(avgP*avgV)[-1]
    return round(DV)

def tradeRoundNumbers(df, perc = 0.1, fract = 2, periods = 1, side = 'h'):
    import math
    
    def getNumbers(avg, fract=0):
        if fract==0:
            pr_frc, pr_int = math.modf(avg)
            return pr_int, pr_frc
        elif fract>=1:
            pr_frc, pr_int = math.modf(avg)
            pr_frc, pr_int = math.modf(pr_int/(10**fract))
        return round(pr_int),round(pr_frc*(10**fract))

    avg = df['Close'].rolling(min_periods=1, window=periods, center=False).mean()[-1]

    if fract==0:
        pr_int, pr_dec = getNumbers(avg, fract)
        if side == 'h':
            h = math.ceil(pr_dec*10**(fract+1))/10**(fract+1)
            delta = h - pr_dec
        else:
            h = math.floor(pr_dec*10**(fract+1))/10**(fract+1)
            delta = pr_dec - h
        treshold=h*perc
        #print(avg, pr_dec, h, delta,treshold)
    else:
        pr_int = round(avg)
        if side == 'h':
            h = math.ceil(avg/10**(fract))*10**(fract)
            delta = h - pr_int
            avg_int, avg_dec = getNumbers(pr_int, fract)
            treshold = (avg_dec+delta)*perc
        else:
            h = math.floor(avg/10**(fract))*10**(fract)
            delta = pr_int - h
            avg_int, avg_dec = getNumbers(pr_int, fract+1)
            treshold = (avg_dec-delta)*perc
        #print(avg, pr_int, h, delta, treshold)
    
    ret = delta <= treshold
    
    return ret

def getTradedMonths(df, min_months = 10):
    months = ((df.index.max()-df.index.min()).days)/20
    return months >= min_months

def getMinOverTime(df, lbl='Close',periods=2520):
    hmin = df[lbl].rolling(min_periods=1, window=periods, center=False).min()[-1]
    last = df[lbl][-1]
    return hmin < last

def getMinOverLastYear(df, lbl='Close',periods=2520):
    try:
        hmin = df[lbl][:-252].rolling(min_periods=1, window=periods, center=False).min()[-1]
        lastYearMin = df[lbl][-252:].rolling(min_periods=1, window=252, center=False).min()[-1]
    except:
        hmin=0
        lastYearMin=0
    return hmin < lastYearMin

def getMaxOverLastYear(df, lbl='Close',periods=2520):
    try:
        hmax = df[lbl][:-252].rolling(min_periods=1, window=periods, center=False).max()[-1]
        lastYearMax = df[lbl][-252:].rolling(min_periods=1, window=252, center=False).max()[-1]
    except:
        hmax=0
        lastYearMax=0
    return hmax<lastYearMax

    
#################################################################
# Group bar series by number, volume, and dollar volume
#################################################################        
class Bars():
    
    @staticmethod
    def prepare(data):
        df=data[['Close','Volume']]
        df['DVolume']=df['Close']*df['Volume']
        df.columns=['price','v','dv']
        return df
    
    @staticmethod
    def prepareWAP(data,lbl_wap,rmin=240,offs=30):
        df=Bars.prepare(data)
        ohlc_dict = {'price': 'last', 'v': 'sum', 'dv': 'sum', 'TWAP': 'mean'}
        df['TWAP']=data[['High','Low','Close']].mean(axis=1)
        df.resample(str(rmin)+'T', offset=str(offs)+'m').agg(ohlc_dict)
        df['VWAP']=df['TWAP']*df['v']
        df['VWAP']=(df['VWAP']+df['VWAP'].cumsum())/df['v'].cumsum()
        
        if lbl_wap != '':
            df['price']=df[lbl_wap]
            del df['TWAP']
            del df['VWAP']
        return df
    
    @staticmethod
    def mad_outlier(y, thresh=3.):
        #mad = mad_outlier(df.price.values.reshape(-1,1))

        median = np.median(y)
        diff = np.sum((y - median)**2, axis=-1)
        diff = np.sqrt(diff)
        med_abs_deviation = np.median(diff)

        modified_z_score = 0.6745 * diff / med_abs_deviation

        return modified_z_score > thresh
    
    @staticmethod
    def getCoeff(df):
        import Plib.Stats.Regression as r
    
        X,Y=r.splitMatrix(df[['Close','Volume']],'Volume')
        yhat,mse,lm=r.linReg(X,Y,table=True,Plot=True)
        ret=r.printStats(lm,X,Y)
        return ret[1]['Coefficients'][0]/dfd['Close'].mean(),ret[1]['Coefficients'][1]/dfd['Close'].mean()
    
    @staticmethod
    def getVolume(close,coeff0=6409.3371, coeff1=-41.8343):
        volatility=(1+close.pct_change().rolling(5).std(ddof=0))
        intercept=coeff0
        coefang=coeff1
        volume=close*intercept + close*coefang
        return (volume*volatility).round(0)
    
    @staticmethod
    def make_timeBars(df):
        sub = df.price
        ref = df.price
        bars = Bars.get_ohlc(ref, sub)
        bars=bars[['end','open','high','low','close']]
        bars.columns=['date','open','high','low','close']
        bars=bars.set_index('date')
        bars['volume']=df.v
        return bars
    
    @staticmethod
    def get_ohlc(ref, sub):
        from tqdm import tqdm, tqdm_notebook
        
        ## uncomment below to run (takes about 5-6 mins on my machine)
        #sub = tick_df.price
        #ref = df.price
        #tick_bars_ohlc = get_ohlc(ref, sub)
        #tick_bars_ohlc.head(2)

        ohlc = []
        for i in tqdm(range(sub.index.shape[0]-1)):
            start,end = sub.index[i], sub.index[i+1]
            tmp_ref = ref.loc[start:end]
            max_px, min_px = tmp_ref.max(), tmp_ref.min()
            o,h,l,c = sub.iloc[i], max_px, min_px, sub.iloc[i+1]
            ohlc.append((end,start,o,h,l,c))
        cols = ['end','start','open','high','low','close']
        return (pd.DataFrame(ohlc,columns=cols))
    
    @staticmethod
    def tick_bars(df, price_column, m):
        from tqdm import tqdm, tqdm_notebook
        
        t = df[price_column]
        ts = 0
        idx = []
        for i, x in enumerate(tqdm(t)):
            ts += 1
            if ts >= m:
                idx.append(i)
                ts = 0
                continue
        return idx
    
    @staticmethod
    def tick_bar_df(df, price_column, m):
        idx = Bars.tick_bars(df, price_column, m)
        return df.iloc[idx].drop_duplicates()
    
    @staticmethod
    def dollar_bars(df, dv_column, m):
        from tqdm import tqdm, tqdm_notebook
        
        t = df[dv_column]
        ts = 0
        idx = []
        
        for i, x in enumerate(tqdm(t)):
            ts += x
            if ts >= m:
                idx.append(i)
                ts = 0
                continue
        return idx
    
    @staticmethod
    def dollar_bar_df(df, dv_column, m):
        idx = Bars.dollar_bars(df, dv_column, m)
        return df.iloc[idx].drop_duplicates()
    
    @staticmethod
    def make_dollarBars(df,lbl='dv',dollar_M='3_000'):
        dv_bar_df = Bars.dollar_bar_df(df, lbl, dollar_M)
        sub = dv_bar_df.price
        ref = df.price
        tick_bars_ohlc = Bars.get_ohlc(ref, sub)
        tick_bars_ohlc['duration']=tick_bars_ohlc.end-tick_bars_ohlc.start
        tick_bars_ohlc.columns=['date','start','open','high','low','close','duration']
        dollar_bars_ohlc=tick_bars_ohlc.set_index('date')
        del dollar_bars_ohlc['start']
        return dollar_bars_ohlc
    
    @staticmethod
    def make_volumeBars(df,lbl='v',volume_M=50_000):
        v_bar_df = Bars.dollar_bar_df(df, lbl, volume_M)
        sub = v_bar_df.price
        ref = df.price
        tick_bars_ohlc = Bars.get_ohlc(ref, sub)
        tick_bars_ohlc['duration']=tick_bars_ohlc.end-tick_bars_ohlc.start
        tick_bars_ohlc.columns=['date','start','open','high','low','close','duration']
        volume_bars_ohlc=tick_bars_ohlc.set_index('date')
        del volume_bars_ohlc['start']
        return volume_bars_ohlc
    
    @staticmethod
    def plotComparison(df,vbars,dbars,volume_M,dollar_M,year='2021'):
        import matplotlib.pylab as plt

        fig = plt.figure(figsize=(18,6))
        sub_plot = fig.add_subplot(1, 1, 1)
        sub_plot.plot(df[year].price,label='Minute Price')
        sub_plot.plot(vbars[year].close, label=str(volume_M)+ ' Volume Bars')
        sub_plot.plot(dbars[year].close, label=str(dollar_M)+ ' Dollar Bars')
        sub_plot.legend()
        sub_plot.set_title='Comparison of Time, Volume, and Dollar Close Price'
    
    @staticmethod
    def plotDuration(vbars,which='Volume',year='2021-01'):
        import matplotlib.pylab as plt

        fig = plt.figure(figsize=(18,6))
        sub_plot = fig.add_subplot(1, 1, 1)
        sub_plot.plot(vbars[year].duration, label=which +' Bars Duration')
        sub_plot.legend()
        sub_plot.set_title='Bars Duration'
        print('Mean duration: ',vbars.duration.mean())
        print('Median duration: ',vbars.duration.median())
    
    @staticmethod
    def resample(bars,freq='D'):
        ohlc_dict = {
         'open': 'first',
         'high': 'max',
         'low': 'min',
         'close': 'last'
        }
        
        ohlcv_dict = {
         'open': 'first',
         'high': 'max',
         'low': 'min',
         'close': 'last',
         'volume': 'sum'
        } 
        
        rdict=ohlc_dict
        if 'volume' in bars.columns:
            rdict=ohlcv_dict
            
        #10T for 10min, 30T, H for hourly, D for daily, W for weekly, M for monthly 
        if freq != '1min':
            if freq=='H':
                df = bars.resample('60Min',base=30).agg(rdict)
            else:
                df = bars.resample(freq).agg(rdict)
        return df
        
    @staticmethod
    def printStats(df,volume_M = 500000,dollar_M = 100_000_000):
        n_ticks = df.shape[0]
        volume_ratio = (df.v.sum()/n_ticks).round()
        dollar_ratio = (df.dv.sum()/n_ticks).round()
        print(f'num ticks: {n_ticks:,}')
        print(f'volume ratio: {volume_ratio}')
        print(f'dollar ratio: {dollar_ratio}')
        #volume_M = 500000 # arbitrary
        print(f'volume threshold: {volume_M:,}')
        #dollar_M = 100_000_000 # arbitrary 
        print(f'dollar threshold: {dollar_M:,}')
        df.head(2)

#################################################
# Compute MAE, MAXFE, MINFE from trades (df) 
#################################################                     
def getMAEstats(d1,df):
    df['MAE']=0
    df['MAXFE']=0
    df['MINFE']=0
    
    for q in range(0,len(df)-1):
        k=df['sec'].iloc[q]
        price=df['pricei'].iloc[q]
        longShort=df['side'].iloc[q]

        x=d1[d1.index>=df['tstart'].iloc[q]]
        x=x[x.index<=df['tend'].iloc[q]]

        x['l_MAE']=0
        x['s_MAE']=0
        x['l_MAXFE']=0
        x['s_MAXFE']=0
        x['l_MINFE']=0
        x['s_MINFE']=0
        for i in range(1,len(x)): 
            if longShort==1:
                x.l_MAE.iloc[i]=max(0,price-x['Low_'+str(k)].iloc[i],x.l_MAE.iloc[i-1])
                x.l_MAXFE.iloc[i]=max(0,x['High_'+str(k)].iloc[i]-price,x.l_MAXFE.iloc[i-1])
                x.l_MINFE.iloc[i]=max(0,x['Low_'+str(k)].iloc[i]-price,x.l_MINFE.iloc[i-1])
            else:
                x.s_MAE.iloc[i]=max(0,x['High_'+str(k)].iloc[i]-price,x.s_MAE.iloc[i-1])
                x.s_MAXFE.iloc[i]=max(0,price-x['Low_'+str(k)].iloc[i],x.s_MAXFE.iloc[i-1])
                x.s_MINFE.iloc[i]=max(0,price-x['High_'+str(k)].iloc[i],x.s_MINFE.iloc[i-1])
        
        if longShort==1:
            ret=x.l_MAE.iloc[-1],x.l_MAXFE.iloc[-1],x.l_MINFE.iloc[-1]
        else:
            ret=x.s_MAE.iloc[-1],x.s_MAXFE.iloc[-1],x.s_MINFE.iloc[-1]
     
        df['MAE'].iloc[q]=ret[0]
        df['MAXFE'].iloc[q]=ret[1]
        df['MINFE'].iloc[q]=ret[2]
    return df

####################################################
# Combined plots of MAE/MFE trades and frequencies 
####################################################                     
def plotMAEStats(df,ticker='AAPL',stype='MAE',fs=(16,4),scl=10,scp=10):
    
    winners=df[df['wl']==1]
    loosers=df[df['wl']!=1]

    wins=winners[winners['sec']==ticker]
    losses=loosers[loosers['sec']==ticker]
    plotMAE(ticker,wins,losses,stype,fs,scl)
    plotMAEFreq(ticker,wins,losses,stype,fs,scp)


def plotMAE(symbol,winners,loosers,stype='MAE',fs=(16,4),scl=10):
    import matplotlib.pyplot as plt
    import matplotlib.ticker as ticker
    import matplotlib.ticker as plticker

    profit1=list(loosers.perc)
    if stype=='MAE':
        mae1=list(loosers.MAE)
    elif stype=='MFE':
        mae1=list(loosers.MAXFE)
    c1='red'

    profit2=list(winners.perc)
    if stype=='MAE':
        mae2=list(winners.MAE)
    elif stype=='MFE':    
        mae2=list(winners.MAXFE)
    c2='green'

   
    loc = plticker.MultipleLocator(base= (max(profit1)-min(profit1))/scl) # this locator puts ticks at regular intervals
    loc.MAXTICKS = 3000
    loc2 = plticker.MultipleLocator(base=(max(profit2)-min(profit2))/scl) # this locator puts ticks at regular intervals
    loc2.MAXTICKS = 3000
    

    fig, (ax1, ax2) = plt.subplots(1, 2,sharey=True,figsize=fs,gridspec_kw={'hspace': 0, 'wspace': 0})
    if stype=='MAE':
        fig.suptitle('Maximum Adverse Excursion - ' + symbol)
    elif stype=='MFE': 
        fig.suptitle('Maximum Favorable Excursion - ' + symbol)
    ax1.scatter(profit1, mae1, c=c1,alpha=0.5)
    if stype=='MAE':
        ax1.set(xlabel='MAE')
    elif stype=='MFE': 
        ax1.set(xlabel='MFE')
    ax1.set(ylabel='losses/profits')
    ax1.xaxis.set_major_formatter(ticker.FormatStrFormatter('%0.3f'))
    ax1.xaxis.set_major_locator(loc)
    ax1.tick_params(axis='x', labelrotation=30)
    ax1.grid(which="major", color='k', linestyle='-.', linewidth=0.5)
    ax2.scatter(profit2, mae2, c=c2,alpha=0.5)
    ax2.set(xlabel='profits')
    ax2.xaxis.set_major_formatter(ticker.FormatStrFormatter('%0.3f'))
    ax2.xaxis.set_major_locator(loc2)
    ax2.grid(which="major", color='k', linestyle='-.', linewidth=0.5)
    ax2.tick_params(axis='x', labelrotation=30)

def plotMAEFreq(symbol,wins,losses,stype='MAE',fs=(16,6),scp=10):
    import matplotlib.pyplot as plt
    import matplotlib.ticker as ticker
    import matplotlib.ticker as plticker

    tickr=symbol
    if stype=='MAE':
        cols1=['MAE','perc','usd']
    elif stype=='MFE':
        cols1=['MAXFE','perc','usd']
    
    df1=wins[cols1]
    if stype=='MAE':
        df1.columns=['Winners MAE','% Profits','Profit']
    elif stype=='MFE':
        df1.columns=['Winners MFE','% Profits','Profit']
    
    df2=losses[cols1]
    if stype=='MAE':
        df2.columns=['Loosers MAE','% Profits','Profit']
    elif stype=='MFE':
        df2.columns=['Loosers MFE','% Profits','Profit']
    
    if stype=='MAE':
        ns1=(df1['Winners MAE'].max()-df1['Winners MAE'].min())/scp
        ns2=(df2['Loosers MAE'].max()-df2['Loosers MAE'].min())/scp
    elif stype=='MFE':
        ns1=(df1['Winners MFE'].max()-df1['Winners MFE'].min())/scp
        ns2=(df2['Loosers MFE'].max()-df2['Loosers MFE'].min())/scp
            
    loc = plticker.MultipleLocator(base=ns1) # this locator puts ticks at regular intervals
    loc.MAXTICKS = 3000
    loc2 = plticker.MultipleLocator(base=ns2) # this locator puts ticks at regular intervals
    loc2.MAXTICKS = 3000
    
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2,sharex='col',figsize=fs)
    #fig.suptitle('Maximum Adverse Excursion - Frequency')

    r=ax1.hist(df1['Profit'],bins=20)
    ax1.set(xlabel='Profits')
    ax1.set(ylabel='Trades')
    ax1.xaxis.set_major_formatter(ticker.FormatStrFormatter('%0.2f'))
    ax1.grid(which="major", color='k', linestyle='-.', linewidth=0.5)
    ax1.tick_params(axis='x', labelrotation=20)
    
    if stype=='MAE':
        r=ax2.hist(df1['Winners MAE'],bins=20)
        ax2.set(xlabel='Winners MAE')
    elif stype=='MFE':
        r=ax2.hist(df1['Winners MFE'],bins=20)
        ax2.set(xlabel='Winners MFE')
    ax2.xaxis.set_major_formatter(ticker.FormatStrFormatter('%0.2f'))
    ax2.xaxis.set_major_locator(loc)
    ax2.grid(which="major", color='k', linestyle='-.', linewidth=0.5)
    ax2.tick_params(axis='x', labelrotation=20)
    
    r=ax3.hist(df2['Profit'],bins=20)
    ax3.set(xlabel='Losses')
    ax3.set(ylabel='Trades')
    ax3.xaxis.set_major_formatter(ticker.FormatStrFormatter('%0.2f'))
    ax3.grid(which="major", color='k', linestyle='-.', linewidth=0.5)
    ax3.tick_params(axis='x', labelrotation=20)
    
    if stype=='MAE':
        r=ax4.hist(df2['Loosers MAE'],bins=20)
        ax4.set(xlabel='Loosers MAE')
    elif stype=='MFE':
        r=ax4.hist(df2['Loosers MFE'],bins=20)
        ax4.set(xlabel='Loosers MFE')
    ax4.xaxis.set_major_formatter(ticker.FormatStrFormatter('%0.2f'))
    ax4.xaxis.set_major_locator(loc2)
    ax4.grid(which="major", color='k', linestyle='-.', linewidth=0.5)
    ax4.tick_params(axis='x', labelrotation=20)
    
    fig.tight_layout(rect=[0, 0.03, 1, 0.95])    

