#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Filters 
#
# Module including helper functions to filter timeseries
#############################################################################################      

from math import sqrt
from warnings import catch_warnings
from warnings import filterwarnings
from numpy import array
from ast import literal_eval
import itertools
import multiprocessing
from itertools import product
from typing import List

import numpy as np
import pandas as pd
import scipy

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

DEBUG_PRINT=0

import Plib.Keystore as kst
ks=kst.Keystore()
ltz="Europe/Rome"
webdriver = module_path + ks.web_driver_path


#################################################################
# Diagnostic Plots
#################################################################    
def plotSmoother(x,y,yhat, title='', fs=(10,4),linkedh=False):
    import matplotlib.pyplot as pll
    import Plib.Plotting.Plots as pl
    
    #plt.figure(figsize=fs)
    fig, plt = pll.subplots(figsize=fs)
    if not linkedh:
        plt.plot(x, y, '+', label='historical data')
    else:
        plt.plot(x, y, '+')
        plt.plot(x, y, label='historical data')
    
    l=min([len(x),len(yhat)])
    plt.plot(x[:l],yhat[:l], color='red', label='forecasted data')
    plt.legend(loc='best')
    if title != '': plt.title(title)
    pl.customize_grid(plt)
    pll.show()
    
#################################################################
# Fractional Stationarity 
# Adapted from the work of Marcos Lopéz dePrado
#################################################################    
def fracDiff(series,d,thres=.01,plot=False): 
    #Increasing width window, with treatment of NaNs 
    #Note 1: For thres=1, nothing is skipped. 
    #Note 2: d can be any positive fractional, not necessarily bounded [0,1]. 
    
    def getWeights(d,size): 
        # thres>0 drops insignificant weights 
        w=[1.] 
        for k in range(1,size): 
            w_=-w[-1]/k*(d-k+1) 
            w.append(w_) 
        w=np.array(w[::-1]).reshape(-1,1) 
        return w 
    
    def plotWeights(dRange,nPlots,size): 
        import matplotlib.pyplot as mpl
        w=pd.DataFrame() 
        for d in np.linspace(dRange[0],dRange[1],nPlots): 
            w_=getWeights(d,size=size) 
            w_=pd.DataFrame(w_,index=range(w_.shape[0])[::-1],columns=[d]) 
            w=w.join(w_,how='outer') 
            ax=w.plot() 
            ax.legend(loc='upper left');
            mpl.show() 
        return

    #1) Compute weights for the longest series 
    w=getWeights(d,series.shape[0]) 
    #2) Determine initial calcs to be skipped based on weight-loss threshold 
    w_=np.cumsum(abs(w)) 
    w_/=w_[-1] 
    skip=w_[w_>thres].shape[0] 
    #3) Apply weights to values 
    df={} 
    for name in series.columns: 
        seriesF,df_=series[[name]].fillna(method='ffill').dropna(),pd.Series() 
        for iloc in range(skip,seriesF.shape[0]): 
            loc=seriesF.index[iloc] 
            if not np.isfinite(series.loc[loc,name]):continue # exclude NAs 
            df_[loc]=np.dot(w[-(iloc+1):,:].T,seriesF.loc[:loc])[0,0] 
        df[name]=df_.copy(deep=True) 
    df=pd.concat(df,axis=1) 
    
    if plot:
        plotWeights(dRange=[0,1],nPlots=11,size=6) 
        plotWeights(dRange=[1,2],nPlots=11,size=6)

    return df
    
def fracDiff_FFD(df,lbl,d=0.35,thres=1e-5,all_series=True,plot=False): 
    #Constant width window (new solution) 
    #Note 1: thres determines the cut-off weight for the window 
    #Note 2: d can be any positive fractional, not necessarily bounded [0,1]. 
    
    ##Step1: Estimate d with a graph
    #best_d=fracDiff_FFD(gold.copy(),'Close',plot=True)
    #Step2: use that d
    #fracD_gold=fracDiff_FFD(gold.copy(),'Close',best_d)
    #gold['frac_close']=fracD_gold+gold.Close.mean()
    #p=gold[['Close','frac_close']].plot(figsize=(12,8))
    
    def getWeights_FFD(d,thres,size): 
        # thres>0 drops insignificant weights 
        w=[1.] 
        for k in range(1,100): 
            w_=-w[-1]/k*(d-k+1) 
            w.append(w_) 
            if abs(w_)<= thres:
                break;
        w=np.array(w[::-1]).reshape(-1,1) 
        return w 
    
    def plotMinFFD(df,lbl,fs=(10,8)): 
        import matplotlib.pyplot as mpl
        from statsmodels.tsa.stattools import adfuller  

        out=pd.DataFrame(columns=['adfStat','pVal','lags','nObs','95% conf','corr']) 
        for d in np.linspace(0,1,11): 
            df1=np.log(df[[lbl]]).resample('1D').last() 
            # downcast to daily obs 
            #df2=fracDiff_FFD(df1,d,thres=.01) 
            df2=fracDiff_FFD(df1,lbl,d,thres=.01) 
            corr=np.corrcoef(df1.loc[df2.index,'Close'],df2[lbl])[0,1] 
            df2=adfuller(df2[lbl],maxlag=1,regression='c',autolag=None) 
            out.loc[d]=list(df2[:4])+[df2[4]['5%']]+[corr] # with critical value 
        
        fig, ax1 = plt.subplots(figsize=fs)
        fig.suptitle('d-Fractional Differencing for Stationarity', fontsize=12)
        ax2 = ax1.twinx()
        l1=ax2.plot(out[['adfStat']], 'g-',label='adfstat')
        l2=ax1.plot(out[['corr']], 'b-',label='corr')
        ax1.set_xlabel('d values')
        ax2.set_ylabel('ADF Statistic as f(d)', color='g')
        ax1.set_ylabel('Correlation', color='b')
        l3=ax2.axhline(out['95% conf'].mean(),linewidth=1,color='r',linestyle='dotted',label='95% Conf: '+str(out['95% conf'].mean().round(2))) 
        ax1.legend()
        ax2.legend()
        #fig.tight_layout()
        plt.show()
        return out[out['adfStat']>=out['95% conf'].mean()].tail(1).index[0]
        
    series=df[[lbl]] ##
    if plot:
        best_param=plotMinFFD(df,lbl)
        print('Best d :', best_param)
        return best_param
    else:
        #1) Compute weights for the longest series 
        if all_series:
            w=getWeights_FFD(d,thres,series.shape[0]) 
        else:
            w=getWeights_FFD(d,thres,int(series.shape[0]*0.06))
        width=len(w)-1 
        #2) Apply weights to values 
        df={}
        for name in series.columns: 
            seriesF,df_=series[[name]].fillna(method='ffill').dropna(),pd.Series() 
            for iloc1 in range(width,seriesF.shape[0]): 
                loc0,loc1=seriesF.index[iloc1-width],seriesF.index[iloc1] 
                if not np.isfinite(series.loc[loc1,name]):continue # exclude NAs 
                df_[loc1]=np.dot(w.T,seriesF.loc[loc0:loc1])[0,0] 
            df[name]=df_.copy(deep=True) 
        df=pd.concat(df,axis=1) 
    return df

#################################################
# Inverse Fisher Transformation
#################################################          
def ifishT(signal):
    from math import exp

    return (exp(2*signal)-1)/(exp(2*signal)+1)
    
#################################################################
# Simpler Smoothers without Forecasting
#################################################################    
def movingaverage(data, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(data, window, 'same')

def movingaverage2(data,window_size):
    cumsum_vec = np.cumsum(np.insert(data, 0, 0)) 
    return (cumsum_vec[window_size:] - cumsum_vec[:-window_size]) / window_size

def savitzkyGolay(data,win_filter=53,order_polyn=3):
    from scipy.signal import savgol_filter
    
    return savgol_filter(data, win_filter, order_polyn) # window size 51, polynomial order 3
    
def smoothTriangle(data, degree):
    triangle=np.concatenate((np.arange(degree + 1), np.arange(degree)[::-1])) # up then down
    smoothed=[]

    for i in range(degree, len(data) - degree * 2):
        point=data[i:i + len(triangle)] * triangle
        smoothed.append(np.sum(point)/np.sum(triangle))
    # Handle boundaries
    smoothed=[smoothed[0]]*int(degree + degree/2) + smoothed
    while len(smoothed) < len(data):
        smoothed.append(smoothed[-1])
    return smoothed

def wavelet(signal, wavelet = "db6", scale=.5, plot=False):
    import pywt
    import matplotlib.pyplot as plt
    import seaborn as sns

    #signal=df.Close
    coefficients = pywt.wavedec(signal, wavelet, mode='per')
    coefficients[1:] = [pywt.threshold(i, value=scale*signal.max(), mode='soft') for i in coefficients[1:]]
    reconstructed_signal = pywt.waverec(coefficients, wavelet, mode='per')
    if plot:
        fig, axes = plt.subplots(ncols=1, figsize=(14, 5))
        signal.plot(color="b", alpha=0.5, label='original signal', lw=2, 
                 title=f'Threshold Scale: {scale:.1f}')
        pd.Series(reconstructed_signal, index=signal.index).plot(c='k', label='DWT smoothing}', linewidth=1)
        fig.tight_layout()
        sns.despine();
    #df['wvlt_'+wavelet]=reconstructed_signal
    return reconstructed_signal

def kalmanFltr(signal, plot=False):
    from pykalman import KalmanFilter
    import matplotlib.pyplot as plt
    import seaborn as sns

    #signal=df[lbl]
    kf = KalmanFilter(transition_matrices = [1],
                      observation_matrices = [1],
                      initial_state_mean = 0,
                      initial_state_covariance = 1,
                      observation_covariance=1,
                      transition_covariance=.01)

    state_means, _ = kf.filter(signal)
    #df['Kalman Filter'] = state_means
    if plot:
        signal_smoothed = signal.to_frame(lbl)
        signal_smoothed['Kalman Filter'] = state_means
        for months in [1,2,3]:
            signal_smoothed[f'MA ({months}m)'] = signal.rolling(window=months*21).mean()

        ax = signal_smoothed.plot(title='Kalman Filter vs Moving Average', figsize=(14,6), lw=1, rot=0)
        ax.set_xlabel('')
        ax.set_ylabel('Signal')
        plt.tight_layout()
        sns.despine();
    return state_means[0]

#################################################################
# Complex Smoothers without Forecasting
#################################################################    
def fft(x,y,spectrum_pow=2,cutoff_interv=5):
    import scipy.fftpack
    
    N=len(y)
    w = scipy.fftpack.rfft(y)
    f = scipy.fftpack.rfftfreq(N, x[1]-x[0])
    spectrum = w**spectrum_pow

    cutoff_idx = spectrum < (spectrum.max()/cutoff_interv)
    w2 = w.copy()
    w2[cutoff_idx] = 0

    return scipy.fftpack.irfft(w2)

def lowess(x,y,frac=0.1):
    import statsmodels.api as sm
    l=min([len(x),len(y)])
    return np.hsplit(sm.nonparametric.lowess(y[:l], x[:l], frac=frac),2) 

def kernelReg(x,y):
    from statsmodels.nonparametric.kernel_regression import KernelReg
    kr = KernelReg(y,x,'c')
    return kr.fit(x)

def rollFilter(data,win=20,periods=6,rtype='median',gauss_std=3):
    if rtype=='median':
        yhat=pd.DataFrame(data).rolling(window=win, min_periods=periods, center=True).median()
    elif rtype=='mean':
        yhat=pd.DataFrame(data).rolling(window=win, min_periods=periods, center=True).mean()    
    elif rtype=='triang':
        yhat=pd.DataFrame(data).rolling(window=win, min_periods=periods, center=True,win_type='triang').mean()    
    elif rtype=='gaussian':
        yhat=pd.DataFrame(data).rolling(window=win, min_periods=periods, center=True,win_type='gaussian').mean(std=gauss_std)    
    elif rtype=='exp':
        yhat=pd.DataFrame(data).ewm(span=win, min_periods=periods,adjust=False).mean()      
    return yhat.iloc[:,0]
    
def cubicSpline(x,y,ctype='natural'):
    from scipy.interpolate import CubicSpline
    
    #ctype: not-a-knot,periodic,clamped
    cs = CubicSpline(x, y, bc_type=ctype)
    return cs(x)

#################################################################
# Complex Smoothers - Quarterly and Monthly data
#################################################################    
def STLSeriesDecomp(valid_data, lbl='adj_close', freq=360, w=30, fs=(15,10),plot=True):
    from statsmodels.tsa.seasonal import STL
    decomposed_series = STL(valid_data[lbl],period=freq).fit()
    
    trend = decomposed_series.trend
    seasonal = decomposed_series.seasonal
    residual = decomposed_series.resid
    
    if plot:
        fig, ax = plt.subplots(5, 1, figsize=fs)

        decomposed_series.observed.plot(ax=ax[0])
        ax[0].set_title("Time series", fontsize=16)
        ax[0].set(xlabel="", ylabel="Price")

        decomposed_series.trend.plot(ax=ax[1])
        ax[1].set(xlabel="", ylabel="Trend")

        decomposed_series.seasonal.plot(ax=ax[2])
        ax[2].set(xlabel="", ylabel="Seasonal")

        decomposed_series.seasonal[0:w].plot(ax=ax[3])
        ax[3].set(xlabel="", ylabel="Seasonal window="+str(w))

        decomposed_series.resid.plot(ax=ax[4])
        ax[4].set(xlabel="Date", ylabel="Residual")

        plt.tight_layout()
    
    return {'STL_trend':trend,'STL_cycle':seasonal,'STL_residual':residual}

def HodrickPrescottSeriesDecomp(dta,lbl='realgdp',freq='Q',plot=True):
    import statsmodels.api as sm
    #Best for trend-stationary series
    if freq=='Q':
        lambda_=1600
    elif freq=='Y':
        lambda_=6.25 #(1600/4**4) 
    elif freq=='M':
        lambda_=129600 #(1600*3**4)
    
    #Hodrick-Prescott
    cycle, trend = sm.tsa.filters.hpfilter(dta[lbl], lambda_)
    decomposed_series = dta[[lbl]]
    
    if plot:
        fig, ax = plt.subplots(3, 1, figsize=(15, 10))

        decomposed_series.plot(ax=ax[0])
        ax[0].set_title("Time series", fontsize=16)
        ax[0].set(xlabel="", ylabel="Series")

        trend.plot(ax=ax[1])
        ax[1].set(xlabel="", ylabel="Trend Hodrick-Prescott")

        cycle.plot(ax=ax[2])
        ax[2].set(xlabel="", ylabel="Cycle Hodrick-Prescott")

        plt.tight_layout()
    
    return {'HP_trend':trend,'HP_cycle':cycle}

def CFBKSeriesDecomp(dta,lbl='infl',freq='Q',plot=True):
    import statsmodels.api as sm
    #Best for stationary series - only for quarterly data
    
    #Christiano Fitzgerald asymmetric random walk filter
    cycle, trend = sm.tsa.filters.cffilter(dta[[lbl]],low=6, high=32, drift=True)
    #Baxter-King filter
    cycles = sm.tsa.filters.bkfilter(dta[[lbl]], low=6, high=32, K=12)
    decomposed_series = dta[[lbl]]
    
    if plot:
        fig, ax = plt.subplots(4, 1, figsize=(15, 10))

        decomposed_series.plot(ax=ax[0])
        ax[0].set_title("Time series", fontsize=16)
        ax[0].set(xlabel="", ylabel="Series")

        trend.plot(ax=ax[1])
        ax[1].set(xlabel="", ylabel="Trend")

        cycle.plot(ax=ax[2])
        ax[2].set(xlabel="", ylabel="Cycle Christiano-Fitzgerald")

        cycles.plot(ax=ax[3])
        ax[3].set(xlabel="", ylabel="Cycle Baxter-King")

        plt.tight_layout()
    
    return {'CF_trend':trend,'CF_cycle':cycle,'BK_cycle':cycles.iloc[:,0]}
       
