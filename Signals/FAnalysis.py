#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# FAnalysis 
#
# Module including functions to obtain fundamental analysis signals
#############################################################################################      

import pandas as pd
import numpy as np

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')
    
import Plib.DataFarm.FRData as fr


#################################################
# Total payout from dividends and repurchases
#################################################         
def getNetPayout(df):
    fhist=fr.getCashflowStat(df)[['DividendsPaid','CommonStockRepurchased']]
    fhist['netPayout']=fhist['DividendsPaid']+fhist['CommonStockRepurchased']
    del fhist['DividendsPaid']
    del fhist['CommonStockRepurchased']
    fhist=fr.getRatios(df)[['MarketCapitalization']].join(fhist)
    fhist=fhist.fillna(0)
    return fhist*-1

#################################################
# Net Profitability
#################################################         
def getProfitability(df):
    fhist=fr.getIncomeStat(df)[['Revenues']].join(fr.getBalanceSheet(df)[['Receivables']]).join(fr.getIncomeStat(df)[['NetIncome']])
    fhist['Profitability']=fhist['NetIncome']/(fhist['Revenues']-fhist['Receivables'])
    del fhist['Revenues']
    del fhist['Receivables']
    del fhist['NetIncome']
    fhist=fr.getRatios(df)[['MarketCapitalization']].join(fhist)
    fhist=fhist.fillna(0)
    return fhist

#################################################
# Compute the Beta with respect to an index
#################################################             
def getBeta(stk,indx,freq='M',adj=False,rnd=2):
    import statsmodels.api as sm
    from statsmodels import regression

    #raw beta       = 1.064926914258738
    #bloomberg adj  = 1.0435010325533545    (0.67*beta + 0.33*1)
    #generic adj    = 1.0486951856940536    (0.75*beta + 0.25*1)
    def getRets(stk,lbl='Close',freq='W'):
        ohlcv_dict = {
            'Open': 'first',
            'High': 'max',
            'Low': 'min',
            'Close': 'last',
            'Adjusted_close': 'last',
            'Volume': 'sum'}
    
        if freq=='M':
            stk = stk.asfreq('B')
            stk.ffill(inplace=True)
            stk['year'] = stk.index.year
            stk['month'] = stk.index.month 
            stk_returns = stk.asfreq('BM').set_index(['year', 'month'])
        elif freq=='W':
            stk = stk.resample('W-MON').apply(ohlcv_dict)
            stk.ffill(inplace=True)
            stk['year'] = stk.index.year
            stk['month'] = stk.index.month 
            stk['week'] = stk.index.weekofyear 
            stk_returns = stk.set_index(['year', 'month','week'])
        return stk_returns[lbl].pct_change()[1:]

    def linreg(x,y):
        x = sm.add_constant(x)
        if len(x)==len(y):
            model = regression.linear_model.OLS(y,x).fit()
        else:
        #    print('error')
            return 0,0
        return model.params[0], model.params[1]
    
    temp=stk.join(indx,rsuffix='_1').dropna() #assure same length
    stk_rets=getRets(temp[stk.columns],freq=freq)
    temp=temp[[str(c)+'_1' for c in indx.columns]]
    temp.columns=indx.columns
    idx_rets=getRets(temp,freq=freq)
    alpha,beta= linreg(idx_rets.values,stk_rets.values)

    if adj:
        beta=0.67*beta+0.33*1
    return round(beta,rnd)

#################################################
# Rolls the beta function, day by day
#################################################             
def getRollingBeta(stk,indx, freq='W', win=252*2, adj=True):
    df=pd.DataFrame(stk.index)
    df.index=stk.index
    df.columns=['Beta']
    df['Beta']=0
    
    for i in reversed(range(len(df))):
        if i<win:
            df.iloc[i]=np.NaN
        else:   
            df.iloc[i]=getBeta(stk1.iloc[i-win:i],indx1.iloc[i-win:i],freq=freq,adj=adj)
    return df

##########################################################
# Rolls the beta function, every monday and copy forward
##########################################################             
def getRollingBetaW(stk,indx, freq='W', win=252*2, adj=True):
    df=pd.DataFrame(stk.index)
    df.index=stk.index
    df.columns=['Beta']
    df['Beta']=np.NaN
    df['Beta'][df.index.to_series().dt.weekday==0]=0
    for i in reversed(range(len(df))):
        if i<win:
            df['Beta'].iloc[i]=np.NaN
        else:   
            if df['Beta'].iloc[i] == 0.0:
                df['Beta'].iloc[i] = getBeta(stk1.iloc[i-win:i],indx1.iloc[i-win:i],freq=freq,adj=adj)
    df.fillna(method = 'ffill',inplace=True)
    return df
    
