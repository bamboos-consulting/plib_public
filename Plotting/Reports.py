#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Reports 
#
# Module comprising plot and helper functions
#############################################################################################      


#Common Libraries
import numpy as np
import pandas as pd

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path+"/")

# Plib imports
import Plib.Plotting.Plots as pl
import Plib.Liquidity.MStats as mst
import Plib.Liquidity.TStats as mst2

##############################################
# Options for Jupyter
##############################################      
#This would display the graph in the notebook, but it was no longer interactive.
#%matplotlib inline
#With this the figures will now show up in the notebook , and still be interactive. 
#%matplotlib notebook

#other libraries
import pandas as pd
import numpy as np
import warnings
warnings.filterwarnings("once")

##############################################
# Plots FX series
##############################################           
def plotETFToFutureRatio(exch1,sym1,month1,tit1,exch2,sym2,tit2,dt_start, dt_end,resample='D'):
    #import Plib.DataFarm.Quandl as api
    #import Plib.DataFarm.IEXdata as api2
    import Plib.DataFarm.Wrapper as api
    import Plib.DataFarm.Wrapper as api2
    
    #Alphavantage stock data
    exchange=exch2
    symbol = sym2
    title2=tit2 + ' - ' + exch2 + ' - '+ sym2 
    series2 = pd.DataFrame(api2.get_eod_data(symbol, dt_start, dt_end).interpolate())

    #Quandl Futures CHRIS databases
    exchange=exch1
    symbol = sym1
    month=month1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1 + ' - Month: ' + str(month1)
    series1 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
    
    plot=pl.plotRatioStudy(symbol=title1, dt_start=dt_start, dt_end=dt_end, d1=series1,d2=series2,tit2=title2)
    return series1,series2,plot

def plot2ETFToFutureRatio(exch1,sym1,month1,tit1,exch2,sym2,tit2,exch3,sym3,tit3,dt_start, dt_end,resample='D'):
    #import Plib.DataFarm.Quandl as api
    #import Plib.DataFarm.IEXdata as api2
    import Plib.DataFarm.Wrapper as api
    import Plib.DataFarm.Wrapper as api2
    
    #Alphavantage stock data
    exchange=exch2
    symbol = sym2
    title2=tit2 + ' - ' + exch2 + ' - '+ sym2 
    series1 = pd.DataFrame(api2.get_eod_data(symbol, dt_start, dt_end).interpolate())

    #Alphavantage stock data
    exchange=exch3
    symbol = sym3
    title3=tit3 + ' - ' + exch3 + ' - '+ sym3 
    series2 = pd.DataFrame(api2.get_eod_data(symbol, dt_start, dt_end).interpolate())

    #Quandl Futures CHRIS databases
    exchange=exch1
    symbol = sym1
    month=month1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1 + ' - Month: ' + str(month1)
    series3 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
        series3 = series3.resample(resample).mean()
    
    symbols=sym2 + ' - ' + sym3
    plot=pl.plotRatioStudy2(scal=6,ref=title1, dt_start=dt_start, dt_end=dt_end, d1=series1,d2=series2,d3=series3,tit=symbols,tit2=tit2,tit3=tit3)
    return series1,series2,plot

def plot2ETFToFutureRatio2(exch1,sym1,month1,tit1,exch2,sym2,tit2,exch3,sym3,tit3,dt_start, dt_end,resample='D'):
    #import Plib.DataFarm.Quandl as api
    #import Plib.DataFarm.IEXdata as api2
    import Plib.DataFarm.Wrapper as api
    import Plib.DataFarm.Wrapper as api2
    
    #Alphavantage stock data
    exchange=exch2
    symbol = sym2
    title2=tit2 + ' - ' + exch2 + ' - '+ sym2 
    series1 = pd.DataFrame(api2.get_eod_data(symbol, dt_start, dt_end).interpolate())

    #Alphavantage stock data
    exchange=exch3
    symbol = sym3
    title3=tit3 + ' - ' + exch3 + ' - '+ sym3 
    series2 = pd.DataFrame(api2.get_eod_data(symbol, dt_start, dt_end).interpolate())

    #Quandl Futures CHRIS databases
    exchange=exch1
    symbol = sym1
    month=month1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1 + ' - Month: ' + str(month1)
    series3 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
        series3 = series3.resample(resample).mean()
    
    symbols=sym2 + ' - ' + sym3

    #def plotRatioStudy1(symbol, dt_start, dt_end, d1,d2,d3,c_win=60, lt_win=50, rsi_win=14, winma=25, alpha=2,tit2=''):
    plot=pl.plotRatioStudy1(symbol=title1, dt_start=dt_start, dt_end=dt_end, d1=series1,d2=series2,d3=series3,tit2=symbols,ds1=tit2,ds2=tit3)
    return series1,series2,plot
     
##############################################
# Plots FX series
##############################################           
def plot3FXSeriesRatio(cross1,cross2,cross3,exch1,sym1,month1,tit1,dt_start, dt_end,resample='D'):
    #import Plib.DataFarm.Quandl as api
    #import Plib.DataFarm.IEXdata as api2
    import Plib.DataFarm.Wrapper as api
    
    cross=cross1.split('/')
    cur1,cur2=cross[0],cross[1]
    series1 = pd.DataFrame(api.get_eod_dataFX(cur1,cur2,dt_start, dt_end).interpolate())
    
    cross=cross2.split('/')
    cur1,cur2=cross[0],cross[1]
    series2 = pd.DataFrame(api.get_eod_dataFX(cur1,cur2,dt_start, dt_end,which='avant').interpolate())
    
    cross=cross3.split('/')
    cur1,cur2=cross[0],cross[1]
    series3 = pd.DataFrame(api.get_eod_dataFX(cur1,cur2,dt_start, dt_end).interpolate())
    
    #Quandl Future CHRIS databases
    exchange=exch1
    symbol = sym1
    month=month1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1 + ' - Month: ' + str(month1)
    series4 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
        series3 = series3.resample(resample).mean()
        series4 = series4.resample(resample).mean()
    
    #return series1,series2,series3,series4
    tit=cross1 + ' - ' + cross2 + ' - ' + cross3
    ref=tit1
    #return pl.plotRatioStudy3(tit,ref, dt_start, dt_end, series1,series2,series3,series4)
    return pl.plotRatioStudy3(tit=tit,ref=ref, dt_start=dt_start, dt_end=dt_end, d1=series1,d2=series2,d3=series3,d4=series4),series1,series2,series3,series4
    
##############################################
# Plots continuous futures
##############################################           
def plotETFContFutures(exch1,sym1,month1,tit1,exch2,sym2,tit2,dt_start, dt_end,resample='D'):
    import Plib.DataFarm.Wrapper as api
    
    exchange=exch2
    symbol = sym2
    title2=tit2 + ' - ' + exch2 + ' - '+ sym2 
    series2 = pd.DataFrame(api.get_eod_data(symbol, dt_start, dt_end).interpolate())
    
    exchange=exch1
    symbol = sym1
    month=month1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1 + ' - Month: ' + str(month1)
    series1 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
    
    plot=pl.plotUnderlyingStudy(symbol=title1, dt_start=dt_start, dt_end=dt_end, prices=series1,prices2=series2,pr2lbl=title2,switch_axes=True)
    return series1,series2,plot

def plot2ETFRatiotoContFutures(exch1,sym1,month1,tit1,exch2,sym2,tit2,exch3,sym3,tit3,dt_start, dt_end,resample='D'):
    import Plib.DataFarm.Wrapper as api
    
    exchange=exch3
    symbol = sym3
    title3=tit3 + ' - ' + exch3 + ' - '+ sym3 
    series3 = pd.DataFrame(api.get_eod_data(symbol, dt_start, dt_end).interpolate())
    
    exchange=exch2
    symbol = sym2
    title2=tit2 + ' - ' + exch2 + ' - '+ sym2 
    series2 = pd.DataFrame(api.get_eod_data(symbol, dt_start, dt_end).interpolate())
    
    exchange=exch1
    symbol = sym1
    month=month1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1 + ' - Month: ' + str(month1)
    series1 = pd.DataFrame(api.getContFuture(exchange,symbol, month1,dt_start, dt_end).interpolate())
    
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
        series3 = series3.resample(resample).mean()
    
    series2=series2/series3
    title2='Ratio of ' + tit2 + '('+ sym2 + '-'+ exch2 +') to ' + tit3 + '('+ sym3 + '-'+ exch3 +')'
    
    plot=pl.plotUnderlyingStudy(symbol=title1, dt_start=dt_start, dt_end=dt_end, prices=series1,prices2=series2,pr2lbl=title2,switch_axes=True)
    return series1,series2,plot

def plot2ContFutures(exch1,sym1,month1,tit1,exch2,sym2,month2,tit2,dt_start, dt_end,resample='D'):
    #import Plib.DataFarm.Quandl as api
    import Plib.DataFarm.Wrapper as api
    
    #Quandl Future CHRIS databases
    exchange=exch2
    symbol = sym2
    month=month2
    title2=tit2 + ' - ' + exch2 + ' - '+ sym2 + ' - Month: ' + str(month2)
    series2 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    
    #Quandl Future CHRIS databases
    exchange=exch1
    symbol = sym1
    month=month1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1 + ' - Month: ' + str(month1)
    series1 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
    
    plot=pl.plotUnderlyingStudy(symbol=title1, dt_start=dt_start, dt_end=dt_end, prices=series1,prices2=series2,pr2lbl=title2,switch_axes=True)
    return series1,series2,plot

def plot2ContFutures2(exch1,sym1,month1,tit1,exch2,sym2,tit2,dt_start, dt_end,resample='D'):
    #import Plib.DataFarm.Quandl as api
    import Plib.DataFarm.Wrapper as api
    
    #Quandl Future single future
    exchange=exch2
    symbol = sym2
    title2=tit2 + ' - ' + exch2 + ' - '+ sym2
    series2 = pd.DataFrame(api.getSimpleFuture(exchange,symbol, dt_start, dt_end).interpolate())
    
    #Quandl Future CHRIS databases
    exchange=exch1
    symbol = sym1
    month=month1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1 + ' - Month: ' + str(month1)
    series1 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
        
    plot=pl.plotUnderlyingStudy(symbol=title1, dt_start=dt_start, dt_end=dt_end, prices=series1,prices2=series2,pr2lbl=title2,switch_axes=True)
    return series1,series2,plot

def plotFredAndContFutures(exch1,sym1,month1,tit1,fred_code,tit2,dt_start, dt_end,resample='D',switch=False):
    #import Plib.DataFarm.Quandl as api
    import Plib.DataFarm.Wrapper as api
    import Plib.DataFarm.Fred as f
    
    #Quandl Future CHRIS databases
    exchange=exch1
    symbol = sym1
    month=month1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1 + ' - Month: ' + str(month1)
    series1 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    
    #FRED databases
    title2= tit2 +' ('+str(fred_code)+')'
    series2 = f.get_eod_data_with_fred2(fred_code,dt_start, dt_end)
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
    
    df11 = series1.Adjusted_close.rename(sym1)
    df12 = series2.Adjusted_close.rename(fred_code)
    index_levels=pd.concat([df11, df12], axis=1).dropna()
    lreturns=np.log(index_levels/index_levels.shift(1))
    lreturns.dropna(inplace=True)
    plot=pl.plotUnderlyingStudy(symbol=title1, dt_start=dt_start, dt_end=dt_end, prices=series1,prices2=series2,pr2lbl=title2,switch_axes=True)
    
    return series1,series2,plot

def plotFredAndContFutures2(exch1,sym1,month1,tit1,fred_code,tit2,dt_start, dt_end,resample='D',switch=False):
    #import Plib.DataFarm.Quandl as api
    import Plib.DataFarm.Wrapper as api
    import Plib.DataFarm.Fred as f
    
    #Quandl Future CHRIS databases
    exchange=exch1
    symbol = sym1
    month=month1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1 + ' - Month: ' + str(month1)
    series1 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    
    #FRED databases
    title2= tit2 +' ('+str(fred_code)+')'
    series2 = f.get_eod_data_with_fred2(fred_code,dt_start, dt_end)
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
    
    df11 = series1.Adjusted_close.rename(sym1)
    df12 = series2.Adjusted_close.rename(fred_code)
    index_levels=pd.concat([df11, df12], axis=1).dropna()
    lreturns=np.log(index_levels/index_levels.shift(1))
    lreturns.dropna(inplace=True)
    plot=pl.plotUnderlyingStudy(symbol=title1, dt_start=dt_start, dt_end=dt_end, prices=series1,prices2=series2,pr2lbl=title2)
    
    return series1,series2,plot

def plotFredAndContFutures3(exch1,sym1,tit1,fred_code,tit2,dt_start, dt_end,resample='D',switch=False):
    #import Plib.DataFarm.Quandl as api
    import Plib.DataFarm.Wrapper as api
    import Plib.DataFarm.Fred as f
    
    #Quandl Single Future
    exchange=exch1
    symbol = sym1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1
    series1 = pd.DataFrame(api.getSimpleFuture(exchange,symbol, dt_start, dt_end).interpolate())
    
    #FRED databases
    title2= tit2 +' ('+str(fred_code)+')'
    series2 = f.get_eod_data_with_fred2(fred_code,dt_start, dt_end)
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
    
    df11 = series1.Adjusted_close.rename(sym1)
    df12 = series2.Adjusted_close.rename(fred_code)
    index_levels=pd.concat([df11, df12], axis=1).dropna()
    lreturns=np.log(index_levels/index_levels.shift(1))
    lreturns.dropna(inplace=True)
    plot=pl.plotUnderlyingStudy(symbol=title1, dt_start=dt_start, dt_end=dt_end, prices=series1,prices2=series2,pr2lbl=title2,switch_axes=True)
    
    return series1,series2,plot
  
def plot2ContFutures2CorrStudy(exch1,sym1,month1,tit1,exch2,sym2,tit2,dt_start, dt_end,resample='D'):
    #import Plib.DataFarm.Quandl as api
    import Plib.DataFarm.Wrapper as api
    
    #Quandl Single Future
    exchange=exch2
    symbol = sym2
    title2=tit2 + ' - ' + exch2 + ' - '+ sym2
    series2 = pd.DataFrame(api.getSimpleFuture(exchange,symbol, dt_start, dt_end).interpolate())

    #Quandl Futures CHRIS databases
    exchange=exch1
    symbol = sym1
    month=month1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1 + ' - Month: ' + str(month1)
    series1 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
        
    title=tit1 +' ('+str(sym1)+') ' + ' vs ' + tit2 +' ('+str(sym2)+')'
    df11 = series1.Adjusted_close.rename(sym1)
    df12 = series2.Adjusted_close.rename(sym2)
    index_levels=pd.concat([df11, df12], axis=1).dropna()
    lreturns=np.log(index_levels/index_levels.shift(1))
    lreturns.dropna(inplace=True)
    plot=pl.corrStudy(title,index_levels,lreturns,sym1,sym2,dt_start,dt_end,[])
    
    return series1,series2,plot

def plot2ContFuturesCorrStudy(exch1,sym1,month1,tit1,exch2,sym2,month2,tit2,dt_start, dt_end,resample='D'):
    #import Plib.DataFarm.Quandl as api
    import Plib.DataFarm.Wrapper as api
    
    #Quandl Futures CHRIS databases
    exchange=exch2
    symbol = sym2
    month=month2
    title2=tit2 + ' - ' + exch2 + ' - '+ sym2 + ' - Month: ' + str(month2)
    series2 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())

    #Quandl Futures CHRIS databases
    exchange=exch1
    symbol = sym1
    month=month1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1 + ' - Month: ' + str(month1)
    series1 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
    
    title=tit1 +' ('+str(sym1)+') ' + ' vs ' + tit2 +' ('+str(sym2)+')'
    df11 = series1.Adjusted_close.rename(sym1)
    df12 = series2.Adjusted_close.rename(sym2)
    index_levels=pd.concat([df11, df12], axis=1).dropna()
    lreturns=np.log(index_levels/index_levels.shift(1))
    lreturns.dropna(inplace=True)
    plot=pl.corrStudy(title,index_levels,lreturns,sym1,sym2,dt_start,dt_end,[])
    
    return series1,series2,plot

def plot3ContFuturesRatio(exch1,sym1,month1,tit1,exch2,sym2,month2,tit2,exch3,sym3,month3,tit3,dt_start, dt_end,resample='D'):
    #import Plib.DataFarm.Quandl as api
    import Plib.DataFarm.Wrapper as api
    
    #Quandl Futures CHRIS databases
    exchange=exch3
    symbol = sym3
    month=month3
    title3=tit3 + ' - ' + exch3 + ' - '+ sym3 + ' - Month: ' + str(month3)
    series3 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    
    #Quandl Futures CHRIS databases
    exchange=exch2
    symbol = sym2
    month=month2
    title2=tit2 + ' - ' + exch2 + ' - '+ sym2 + ' - Month: ' + str(month2)
    series2 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())

    #Quandl Futures CHRIS databases
    exchange=exch1
    symbol = sym1
    month=month1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1 + ' - Month: ' + str(month1)
    series1 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
        series3 = series3.resample(resample).mean()
    
    series2=series2/series3
    title2='Ratio of ' + tit2 + '('+ sym2 +str(month2) + '-'+ exch2 +') to ' + tit3 + '('+ sym3 +str(month3) + '-'+ exch3 +')'
    
    plot=pl.plotUnderlyingStudy(symbol=title1, dt_start=dt_start, dt_end=dt_end, prices=series1,prices2=series2,pr2lbl=title2,switch_axes=True)
    return series1,series2,plot

def plot3ContFuturesRatio2(exch1,sym1,month1,tit1,exch2,sym2,month2,tit2,exch3,sym3,tit3,dt_start, dt_end,resample='D'):
    #import Plib.DataFarm.Quandl as api
    import Plib.DataFarm.Wrapper as api
    
    #Quandl Single Future
    exchange=exch3
    symbol = sym3
    title3=tit3 + ' - ' + exch3 + ' - '+ sym3 
    series3 = pd.DataFrame(api.getSimpleFuture(exchange,symbol, dt_start, dt_end).interpolate())
    
    #Quandl Futures CHRIS databases
    exchange=exch2
    symbol = sym2
    month=month2
    title2=tit2 + ' - ' + exch2 + ' - '+ sym2 + ' - Month: ' + str(month2)
    series2 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())

    #Quandl Futures CHRIS databases
    exchange=exch1
    symbol = sym1
    month=month1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1 + ' - Month: ' + str(month1)
    series1 = pd.DataFrame(api.getContFuture(exchange,symbol, month1,dt_start, dt_end).interpolate())
    
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
        series3 = series3.resample(resample).mean()
    
    series2=series2/series3
    title2='Ratio of ' + tit2 + '('+ sym2 +str(month2) + '-'+ exch2 +') to ' + tit3 + '('+ sym3 + '-'+ exch3 +')'
    
    plot=pl.plotUnderlyingStudy(symbol=title1, dt_start=dt_start, dt_end=dt_end, prices=series1,prices2=series2,pr2lbl=title2,switch_axes=True)
    return series1,series2,plot
    
def plot3ContFuturesRatio3(fred_code,tit1,exch2,sym2,month2,tit2,exch3,sym3,tit3,dt_start, dt_end,resample='D'):
    #import Plib.DataFarm.Quandl as api
    import Plib.DataFarm.Wrapper as api
    import Plib.DataFarm.Fred as f
    
    #Quandl Single Future
    exchange=exch3
    symbol = sym3
    title3=tit3 + ' - ' + exch3 + ' - '+ sym3 
    series3 = pd.DataFrame(api.getSimpleFuture(exchange,symbol, dt_start, dt_end).interpolate())
    
    #Quandl Futures CHRIS databases
    exchange=exch2
    symbol = sym2
    month=month2
    title2=tit2 + ' - ' + exch2 + ' - '+ sym2 + ' - Month: ' + str(month2)
    series2 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())

    #FRED databases
    symbol = fred_code
    title1=tit1 + ' - '+ fred_code 
    series1 = f.get_eod_data_with_fred2(fred_code,dt_start, dt_end)
    
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
        series3 = series3.resample(resample).mean()
    
    series2=series2/series3
    title2='Ratio of ' + tit2 + '('+ sym2 +str(month2) + '-'+ exch2 +') to ' + tit3 + '('+ sym3 + '-'+ exch3 +')'
    
    plot=pl.plotUnderlyingStudy(symbol=title1, dt_start=dt_start, dt_end=dt_end, prices=series1,prices2=series2,pr2lbl=title2,switch_axes=True)
    return series1,series2,plot

def plot2ContFuturesRatio(exch1,sym1,month1,tit1,exch2,sym2,month2,tit2,dt_start, dt_end,resample='D'):
    #import Plib.DataFarm.Quandl as api
    import Plib.DataFarm.Wrapper as api
    
    #Quandl Futures CHRIS databases
    exchange=exch2
    symbol = sym2
    month=month2
    title2=tit2 + ' - ' + exch2 + ' - '+ sym2 + ' - Month: ' + str(month2)
    series2 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())

    #Quandl Futures CHRIS databases
    exchange=exch1
    symbol = sym1
    month=month1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1 + ' - Month: ' + str(month1)
    series1 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
    
    #def plotRatioStudy(symbol, dt_start, dt_end, d1,d2,c_win=60, lt_win=50, rsi_win=14, winma=25, alpha=2):    
    plot=pl.plotRatioStudy(symbol=title1, dt_start=dt_start, dt_end=dt_end, d1=series1,d2=series2,tit2=title2)
    return series1,series2,plot

def plotFredAndContFuturesCorrStudy(exch1,sym1,month1,tit1,fred_code,tit2,dt_start, dt_end,resample='D',switch=False):
    #import Plib.DataFarm.Quandl as api
    import Plib.DataFarm.Wrapper as api
    import Plib.DataFarm.Fred as f
    
    #Quandl Futures CHRIS databases
    exchange=exch1
    symbol = sym1
    month=month1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1 + ' - Month: ' + str(month1)
    series1 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    
    #FRED databases
    series2 = f.get_eod_data_with_fred2(fred_code,dt_start, dt_end)
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
    
    title=tit1 +' ('+str(sym1)+') ' + ' vs ' + tit2 +' ('+str(fred_code)+')'
    df11 = series1.Adjusted_close.rename(sym1)
    df12 = series2.Adjusted_close.rename(fred_code)
    index_levels=pd.concat([df11, df12], axis=1).dropna()
    lreturns=np.log(index_levels/index_levels.shift(1))
    lreturns.dropna(inplace=True)
    plot=pl.corrStudy(title,index_levels,lreturns,sym1,fred_code,dt_start,dt_end,[])
    
    return series1,series2,plot

def plotFredAndContFutures2CorrStudy(exch1,sym1,tit1,fred_code,tit2,dt_start, dt_end,resample='D',switch=False):
    #import Plib.DataFarm.Quandl as api
    import Plib.DataFarm.Wrapper as api
    import Plib.DataFarm.Wrapper as api2
    
    import Plib.DataFarm.Fred as f
    
    #Quandl Single Future
    exchange=exch1
    symbol = sym1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1
    series1 = pd.DataFrame(api.getSimpleFuture(exchange,symbol, dt_start, dt_end).interpolate())
    
    #FRED databases
    series2 = f.get_eod_data_with_fred2(fred_code,dt_start, dt_end)
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
    
    title=tit1 +' ('+str(sym1)+') ' + ' vs ' + tit2 +' ('+str(fred_code)+')'
    df11 = series1.Adjusted_close.rename(sym1)
    df12 = series2.Adjusted_close.rename(fred_code)
    index_levels=pd.concat([df11, df12], axis=1).dropna()
    lreturns=np.log(index_levels/index_levels.shift(1))
    lreturns.dropna(inplace=True)
    plot=pl.corrStudy(title,index_levels,lreturns,sym1,fred_code,dt_start,dt_end,[])
    
    return series1,series2,plot

##############################################
# Volatility Report functions 
##############################################           
def plotETFToFuture(exch1,sym1,month1,tit1,exch2,sym2,tit2,dt_start, dt_end,resample='D'):
    import Plib.DataFarm.Wrapper as api
    
    title2=tit2 + ' - ' + exch2 + ' - '+ sym2 
    series2 = api.get_eod_data(sym2, dt_start, dt_end,which='invest',country='united states').interpolate()

    exchange=exch1
    symbol = sym1
    month=month1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1 + ' - Month: ' + str(month1)
    series1 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
    
    plot=pl.plotUnderlyingStudy(symbol=title1, dt_start=dt_start, dt_end=dt_end, prices=series1,prices2=series2,pr2lbl=title2,switch_axes=True)
    return series1,series2,plot

def plotSP500vsIC(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'ES'
    title1='S&P 500 Continuous Future'
    month1=1
    exchange2='CBOE'
    symbol2 = 'COR3M'
    title2='Cboe Implied Correlation Index'
    try:
        s1,s2, plot=plotETFToFuture(exchange1,symbol1,month1,title1,exchange2,symbol2,title2,dt_start,dt_end,resample)
    except:
        s1,s2, plot = [], [], None
        pass;
    return s1,s2, plot
    
def plotSP500vsVIXF(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'ES'
    title1='S&P 500 Continuous Future'
    month1=1
    exchange2='CBOE'
    symbol2 = 'VIX Futures'
    title2='Cboe SP500 VIX Future'
    return plotETFToFuture(exchange1,symbol1,month1,title1,exchange2,symbol2,title2,dt_start,dt_end,resample)
    
def plotSP500vsVIX(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'ES'
    title1='S&P 500 Continuous Future'
    month1=1
    exchange2='CBOE'
    symbol2 = '^VIX'
    title2='Cboe Volatility Index'
    return plotETFContFutures(exchange1,symbol1,month1,title1,exchange2,symbol2,title2,dt_start,dt_end,resample)

def plotSP500vsSKEW(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'ES'
    title1='S&P 500 Continuous Future'
    month1=1
    exchange2='CBOE'
    symbol2 = '^SKEW'
    title2='Cboe Skew Index'
    return plotETFContFutures(exchange1,symbol1,month1,title1,exchange2,symbol2,title2,dt_start,dt_end,resample)

def plotVolofVolRatiotoSP500(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'ES' 
    month1=1
    title1='S&P 500 Continuous Future'
    exchange2='CBOE'
    symbol2 = '^VVIX'
    title2='VIX Volatility Index'
    exchange3='CBOE'
    symbol3 = '^VIX'
    title3='VIX Index'
    
    return plot2ETFRatiotoContFutures(exchange1,symbol1,month1,title1,exchange2,symbol2,title2,exchange3,symbol3,title3,dt_start,dt_end,resample)

# Nations Indices Plots
def plotSP500vsNVIX(dt_start, dt_end,link, dwnld=True, resample='D'):
    exchange1='CME'
    symbol1 = 'ES'
    title1='S&P 500 Continuous Future'
    month1=1
    exchange2='Nations'
    symbol2 = 'VDEX'
    title2='Nations Large Cap Volatility Index'
    return plotETFContFuturesXLS(exchange1,symbol1,month1,title1,exchange2,symbol2,title2,dt_start,dt_end,link=link, dwnld=dwnld,resample=resample)

def plotSP500vsNSKEW(dt_start, dt_end,link, dwnld=True, resample='D'):
    exchange1='CME'
    symbol1 = 'ES'
    title1='S&P 500 Continuous Future'
    month1=1
    exchange2='Nations'
    symbol2 = 'SDEX'
    title2='Nations Large Cap Volatility Index'
    return plotETFContFuturesXLS(exchange1,symbol1,month1,title1,exchange2,symbol2,title2,dt_start,dt_end,link=link, dwnld=dwnld,resample=resample)

def plotETFContFuturesXLS(exch1,sym1,month1,tit1,exch2,sym2,tit2,dt_start, dt_end,link, dwnld,resample='D'):
    import Plib.DataFarm.Wrapper as api
    
    exchange=exch2
    symbol = sym2
    title2=tit2 + ' - ' + exch2 + ' - '+ sym2 
    df=getNationsIndices(link=link,dwnld=dwnld)
    if sym2 == 'VDEX':
        df=df[[df.columns[0]]]
    elif sym2 == 'SDEX':
        df=df[[df.columns[1]]]
    df.columns=['Adjusted_close']
    df=df[(df.index >= dt_start) & (df.index <= dt_end)]
    series2 = df.interpolate()
    
    exchange=exch1
    symbol = sym1
    month=month1
    title1=tit1 + ' - ' + exch1 + ' - '+ sym1 + ' - Month: ' + str(month1)
    series1 = pd.DataFrame(api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    
    if resample != 'D':
        series1 = series1.resample(resample).mean()
        series2 = series2.resample(resample).mean()
    
    plot=pl.plotUnderlyingStudy(symbol=title1, dt_start=dt_start, dt_end=dt_end, prices=series1,prices2=series2,pr2lbl=title2,switch_axes=True)
    return series1,series2,plot

def getNationsIndices(link='', dwnld=False):
    import datetime
    from Plib.Utils.Tools import getExcelFromUrl
    from pandas import read_excel,to_datetime
    
    save_dir='//Users/rob'
    myname='NATIONS'
    excel_sheet='Large Cap 30-Day Values'
    frow=1
    
    if dwnld:
        df=getExcelFromUrl(link,excel_sheet,frow,reqd=True,save_dir=save_dir,myname=myname)
    else:
        myname=myname+str(datetime.date.today()).replace('-','')
        df = read_excel(save_dir+'/'+myname+'.xls',
                       sheet_name=excel_sheet,
                       skiprows=range(int(frow)-1),
                       skipfooter=0)
    df.rename(columns = {'Trade Date':'Date'}, inplace = True)
    df['Date'] = to_datetime(df['Date'])
    df.set_index('Date',inplace=True)
    df = df.sort_index(ascending=True)
    return df

##############################################
# Report functions 
##############################################           
def plotSP500vs10YNotes(dt_start, dt_end,resample='D'):
    exchange2='CME'
    symbol2 = 'ZN'
    month2=1
    title2='US 10 Years Notes Future'
    exchange1='CME'
    symbol1 = 'ES'
    month1=1
    title1='S&P 500 Continuous Future'
    return plot2ContFutures(exchange1,symbol1,month1,title1,exchange2,symbol2,month2,title2,dt_start,dt_end,resample)
   
def plotDollarIndexvsCommoditiesIndex(dt_start, dt_end,resample='D'):
    exchange1='RICI'
    symbol1 = 'GD'
    title1='S&P-GSCI Commodity Index'
    symbol2 = 'DTWEXBGS'#'RTWEXBGS'
    title2='Nominal Trade Weighted U.S. Dollar Index'
    return plotFredAndContFutures3(exchange1,symbol1,title1,symbol2,title2,dt_start,dt_end,resample)
     
def plotSP500vsCrudeOil(dt_start, dt_end,resample='D'):
    exchange2='CME'
    symbol2 = 'CL'
    month2=1
    title2='Crude Oil Futures'
    exchange1='CME'
    symbol1 = 'ES'
    month1=1
    title1='S&P 500 Continuous Future'
    return plot2ContFutures(exchange1,symbol1,month1,title1,exchange2,symbol2,month2,title2,dt_start,dt_end,resample)

def plotSP500vsCommoditiesIndex(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'ES'
    month1=1
    title1='S&P 500 Continuous Future'
    exchange2='RICI'
    symbol2 = 'GD'
    title2='S&P-GSCI Commodity Index'
    return plot2ContFutures2(exchange1,symbol1,month1,title1,exchange2,symbol2,title2,dt_start,dt_end,resample)
    
def plot10YNotesvsCommoditiesIndex(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'ZN'
    month1=1
    title1='US 10 Years Notes Future'
    exchange2='RICI'
    symbol2 = 'GD'
    title2='S&P-GSCI Commodity Index'
    return plot2ContFutures2(exchange1,symbol1,month1,title1,exchange2,symbol2,title2,dt_start,dt_end,resample)
    
def plotSP500vsCopper(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'ES'
    month1=1
    title1='S&P 500 Continuous Future'
    exchange2='HKEX'
    symbol2 = 'HG'
    title2='Copper Future'
    return plot2ContFutures2(exchange1,symbol1,month1,title1,exchange2,symbol2,title2,dt_start,dt_end,resample)

def plotSilverToGold(dt_start, dt_end,resample='D'):
    exchange2='CME'
    symbol2 = 'GC'
    month2=1
    title2='Gold Future'
    exchange1='CME'
    symbol1 = 'SI'
    month1=1
    title1='Silver Future'
    return plot2ContFuturesRatio(exchange1,symbol1,month1,title1,exchange2,symbol2,month2,title2,dt_start,dt_end,resample)

def plotGoldToSilver(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'GC'
    month1=1
    title1='Gold Future'
    exchange2='CME'
    symbol2 = 'SI'
    month2=1
    title2='Silver Future'
    return plot2ContFuturesRatio(exchange1,symbol1,month1,title1,exchange2,symbol2,month2,title2,dt_start,dt_end,resample)

def plotSP500vsSilverGoldRatio(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'ES'
    month1=1
    title1='S&P 500 Continuous Future'
    exchange3='CME'
    symbol3 = 'GC'
    month3=1
    title3='Silver Future'
    exchange2='CME'
    symbol2 = 'SI'
    month2=1
    title2='Silver Future'
    
    return plot3ContFuturesRatio(exchange1,symbol1,month1,title1,exchange2,symbol2,month2,title2,exchange3,symbol3,month3,title3,dt_start,dt_end,resample)

def plotDollarIndexvsGoldCommRatio(dt_start, dt_end,resample='D'):
    symbol1 = 'DTWEXBGS' #'RTWEXBGS'
    title1='Nominal Trade Weighted U.S. Dollar Index'
    exchange2='CME'
    symbol2 = 'GC'
    month2=1
    title2='Gold Future'
    exchange3='RICI'
    symbol3 = 'GD'
    title3='S&P-GSCI Commodity Index'
    
    return plot3ContFuturesRatio3(symbol1,title1,exchange2,symbol2,month2,title2,exchange3,symbol3,title3,dt_start,dt_end,resample)

def plotSP500vsGoldCommRatio(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'ES'
    month1=1
    title1='S&P 500 Continuous Future'
    exchange2='CME'
    symbol2 = 'GC'
    month2=1
    title2='Gold Future'
    exchange3='RICI'
    symbol3 = 'GD'
    title3='S&P-GSCI Commodity Index'
    
    return plot3ContFuturesRatio2(exchange1,symbol1,month1,title1,exchange2,symbol2,month2,title2,exchange3,symbol3,title3,dt_start,dt_end,resample)

def plotDollarIndexvsCommoditiesCorrStudy(dt_start, dt_end,resample='D'):
    exchange1='RICI'
    symbol1 = 'GD'
    title1='S&P-GSCI Commodity Index'
    symbol2 = 'RTWEXBGS'
    title2='Real Trade Weighted U.S. Dollar Index'
    return plotFredAndContFutures2CorrStudy(exchange1,symbol1,title1,symbol2,title2,dt_start,dt_end,resample)
 
def plotSP500vs10YNotesCorrStudy(dt_start, dt_end,resample='D'):
     exchange2='CME'
     symbol2 = 'ZN'
     month2=1
     title2='US 10 Years Notes Future'
     exchange1='CME'
     symbol1 = 'ES'
     month1=1
     title1='S&P 500 Continuous Future'
     return plot2ContFuturesCorrStudy(exchange1,symbol1,month1,title1,exchange2,symbol2,month2,title2,dt_start,dt_end,resample)

def plotSP500vsCrudeOilCorrStudy(dt_start, dt_end,resample='D'):
    exchange2='CME'
    symbol2 = 'CL'
    month2=1
    title2='Crude Oil Futures'
    exchange1='CME'
    symbol1 = 'ES'
    month1=1
    title1='S&P 500 Continuous Future'
    return plot2ContFuturesCorrStudy(exchange1,symbol1,month1,title1,exchange2,symbol2,month2,title2,dt_start,dt_end,resample)

def plotSP500vsCommoditiesCorrStudy(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'ES'
    month1=1
    title1='S&P 500 Continuous Future'
    exchange2='RICI'
    symbol2 = 'GD'
    title2='S&P-GSCI Commodity Index'
    return plot2ContFutures2CorrStudy(exchange1,symbol1,month1,title1,exchange2,symbol2,title2,dt_start,dt_end,resample)
    
def plot10YNotesvsCommoditiesCorrStudy(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'ZN'
    month1=1
    title1='US 10 Years Notes Future'
    exchange2='RICI'
    symbol2 = 'GD'
    title2='S&P-GSCI Commodity Index'
    return plot2ContFutures2CorrStudy(exchange1,symbol1,month1,title1,exchange2,symbol2,title2,dt_start,dt_end,resample)
    
def plotSP500vsCopperCorrStudy(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'ES'
    month1=1
    title1='S&P 500 Continuous Future'
    exchange2='HKEX'
    symbol2 = 'HG'
    title2='Copper Future'
    return plot2ContFutures2CorrStudy(exchange1,symbol1,month1,title1,exchange2,symbol2,title2,dt_start,dt_end,resample)

def plotCurrenciestoGoldFuture(dt_start, dt_end,resample='D'):
    cross1='EUR/USD'
    cross2='JPY/USD' #'YEN/USD'
    cross3='CHF/USD'
    exchange1='CME'
    symbol1 = 'GC'
    month1=1
    title1='Gold Future'
    return plot3FXSeriesRatio(cross1,cross2,cross3,exchange1,symbol1,month1,title1,dt_start,dt_end)
                            
def plotMinersToGold(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'GC'
    month1=1
    title1='Gold Future'
    
    exchange2=''
    symbol2 = 'GDX'
    title2='Gold Miners ETF'
    return plotETFToFutureRatio(exchange1,symbol1,month1,title1,exchange2,symbol2,title2,dt_start,dt_end,resample)

def plotEMArketsToCommIndex(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'GD'
    month1=1
    title1='S&P-GSCI Commodity Index'
    
    exchange2=''
    symbol2 = 'FXI'
    title2='China Shares ETF'
    
    exchange3=''
    symbol3 = 'EWX'
    title3='Brazil Shares ETF'
    
    return plot2ETFToFutureRatio(exchange1,symbol1,month1,title1,exchange2,symbol2,title2,exchange3,symbol3,title3,dt_start,dt_end,resample)

def plotEMArketsToGoldFuture(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'GC'
    month1=1
    title1='Gold Future'
    
    exchange2=''
    symbol2 = 'FXI'
    title2='China Shares ETF'
    
    exchange3=''
    symbol3 = 'EWX'
    title3='Brazil Shares ETF'
    
    return plot2ETFToFutureRatio(exchange1,symbol1,month1,title1,exchange2,symbol2,title2,exchange3,symbol3,title3,dt_start,dt_end,resample)

def plotHYBondsToSP500(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'ES'
    month1=1
    title1='S&P 500 Continuous Future'
    
    exchange2=''
    symbol2 = 'HYG'
    title2='HighYield Bond ETF'
    
    exchange3=''
    symbol3 = 'LQD'
    title3='Corporate Bond ETF'
    
    return plot2ETFToFutureRatio2(exchange1,symbol1,month1,title1,exchange2,symbol2,title2,exchange3,symbol3,title3,dt_start,dt_end,resample)

def plotIGBondsToSP500(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'ES'
    month1=1
    title1='S&P 500 Continuous Future'
    
    exchange2=''
    symbol2 = 'LQD'
    title2='Corporate Bond'
    
    exchange3=''
    symbol3 = 'IEF'
    title3='Treasury Bond ETF'
    
    return plot2ETFToFutureRatio2(exchange1,symbol1,month1,title1,exchange2,symbol2,title2,exchange3,symbol3,title3,dt_start,dt_end,resample)

def plot30YBondsToSP500(dt_start, dt_end,resample='D'):
    exchange1='CME'
    symbol1 = 'ES'
    month1=1
    title1='S&P 500 Continuous Future'
    
    exchange2=''
    symbol2 = 'TLT'
    title2='30Y Treasuries ETF'
    
    exchange3=''
    symbol3 = 'HYG'
    title3='HighYield Bond ETF'
    
    return plot2ETFToFutureRatio2(exchange1,symbol1,month1,title1,exchange2,symbol2,title2,exchange3,symbol3,title3,dt_start,dt_end,resample)

##############################################
# Report graphical functions
##############################################           
def prepareDataTreemap(exchange='XNYS',save_to=''):
    import Plib.DataFarm.MStack as ms    
    import Plib.DataFarm.IEXdata as iex  
    
    df=ms.getExchangeTickers('XNYS')
    df['AssetType']=''
    df['Sector']=''
    df['Industry']=''
    
    i=0
    for sym in df['symbol']:
        try:
            temp=iex.getMetaByTicker(sym)
        except:
            print()
        if len(temp.columns)>0:
            #df.iloc[i,4]=temp.iloc[0,1]
            df.iloc[i,5]=temp.iloc[0,9]
            df.iloc[i,6]=temp.iloc[0,3]
        i=i+1  
        
    df2=pd.DataFrame()
    i=0
    for sym in df['symbol']:
        temp=pd.DataFrame()
        try:
            temp=iex.getStatsByTicker(sym)
        except:
            print()
        if len(temp.columns)>0:
            temp['symbol']=df.iloc[i,1]
            df2=df2.append(temp)
        i=i+1
    
    if save_to != '':
        df.to_pickle(save_to + 'treemap')
        df2.to_pickle(save_to + 'treemap2')   
        
    df=df.set_index('symbol')
    df2=df2.set_index('symbol')
    result = pd.concat([df, df2], axis=1, join='inner')
    result['symbol']=result.index
    return result,result.columns

def plotTreemapByExchange(result,key,limit=12):
    #Available Statistics:
    #day50MovingAvg-day200MovingAvg,marketcap,avg10Volume,beta,dividendYield,float
    #sharesOutstanding,ttmDividendRate,ttmEPS,week52change,week52high-week52low
    #'maxChangePercent'
    #For grouped data only:
    #'day5ChangePercent','maxChangePercent', 
    #'month1ChangePercent', 'month3ChangePercent','month6ChangePercent',
    #pl1=plotTreemap(result,label='symbol',key='avg10Volume',groupBy='Industry',gtype='sum',limit=12)

    pl1=pl.plotTreemap(result,label='symbol',key=key,limit=limit)
    return pl1

def plotTreemapByIndustry(result,key,limit=12):
    stats=['day50MovingAvg','day200MovingAvg','marketcap','avg10Volume',
           'float','sharesOutstanding','week52change',
           'week52high','week52low']
    if key in stats:
        gtype='sum'
    else:
        gtype='mean'
    pl1=pl.plotTreemap(result,key=key,groupBy='Industry',gtype=gtype,limit=limit)
    return pl1

def plotTreemapBySector(result,key,limit=12):
    stats=['day50MovingAvg','day200MovingAvg','marketcap','avg10Volume',
           'float','sharesOutstanding','week52change',
           'week52high','week52low']
    if key in stats:
        gtype='sum'
    else:
        gtype='mean'
    pl1=pl.plotTreemap(result,key=key,groupBy='Sector',gtype=gtype,limit=limit)
    return pl1

def plotTreemapSectorComponents(result,key,sector,limit=12):
    pl1=pl.plotTreemap(result,key=key,filter='Sector',filter_val=sector,limit=limit)
    return pl1

def plotTreemapIndustryComponents(result,key,industry,limit=12):
    pl1=pl.plotTreemap(result,key=key,filter='Industry',filter_val=industry,limit=limit)
    return pl1
    
##############################################
# Report market statistics
##############################################           
def plotStatsLiqNYSE(dt_start,dt_end):
    #import Plib.DataFarm.Wrapper as api
    import Plib.DataFarm.Kibot as api
    import pandas as pd
    
    trin=api.getindex('$TRIN',dt_start=dt_start,dt_end=dt_end)
    tick=api.getindex('$JTNT',dt_start=dt_start,dt_end=dt_end)
    volumes=api.getindex('$JVNT',dt_start=dt_start,dt_end=dt_end)
    volumes['Volume']=volumes['Close']
    #trin=api.get_eod_data('NYSE TRIN', dt_start, dt_end,which='invest',country='united states')
    #tick=api.get_eod_data('NYSE Tick Index', dt_start, dt_end,which='invest',country='united states')
    #volumes=api.get_eod_data('^NYA', dt_start, dt_end,which='yahoo')
    
    title='Liquidity - '
    #exchange='CME'
    #symbol = 'ES'
    #month=1
    #spy_prices = pd.DataFrame(
    #    api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    
    pl.plotMarketStatistics(title, trin, 'TRIN INDEX',tick,'TICK INDEX',volumes,'NYSE Index (Rescaled by 100)',volumes,'Total NYSE Volume')
    
    return trin,tick,volumes
     
##############################################
# Report market statistics
##############################################           
def plotStatsLiqMarginAcc(link):
    import Plib.DataFarm.Wrapper as api
      
    df=mst2.getMarginStats(link)
    df1=df[['Debit in Margin Accounts']]
    df1.columns=['Debit_MarginA']
    df2=df[['Free Credit in Margin Accounts']]
    df2.columns=['FreeCredit_MarginA']
    df3=df[['Free Credit in Cash Accounts']]
    df3.columns=['FreeCredit_CashA']

    
    dt_start=str(df.index.min())[:10]
    dt_end=str(df.index.max())[:10]
    exchange='CME'
    symbol = 'ES'
    month=1
    spy_prices = pd.DataFrame(
        api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())

    tit='Available Debit and Credit in Trading Accounts'

    pl.plotLiquidityAcc(tit,df1,df2,df3,spy_prices,'Debit in Margin Accounts','Free Credit in Margin Accounts','Free Credit in Cash Accounts','S&P 500 Cont Future')
    return df

##############################################
# Report Funds Flows
##############################################              
def plotMonthlyFundsFlows(link='',r1=8,r2=34,r3=36,r4=43, what='D',debug=False):
    import Plib.DataFarm.Wrapper as api
    import pandas as pd

    d1,d2=mst2.getICIfundsFlows(link,r1,r2,r3,r4,debug=debug)  

    dt_start=str(d1.index.min())[:10]
    dt_end=str(d1.index.max())[:10]
    exchange='CME'
    symbol = 'ES'
    month=1
    spy_prices = pd.DataFrame(
        api.getContFuture(exchange,symbol, month, dt_start, dt_end).interpolate())
    spy_prices = spy_prices.resample('M').mean()

    tit='Net Inflows from Mutual Funds and ETFs'
    if what=='D': tit=tit+' (Domestic)'
    if what=='T':
        df1=pd.DataFrame(d1['Total MF + ETF'])
        df1.columns=['Tot_MF_ETF']
        df2=pd.DataFrame(d1['Equity Total'])
        df2.columns=['Equity_Tot']
        df3=pd.DataFrame(d1['Bond Total'])
        df3.columns=['Bond_Tot']
    elif what=='D':
        df1=pd.DataFrame(d1['Total MF + ETF'])
        df1.columns=['Tot_MF_ETF']
        df2=pd.DataFrame(d1['Equity Domestic'])
        df2.columns=['Equity_Tot']
        df3=pd.DataFrame(d1['Bond Total'])
        df3.columns=['Bond_Tot']        
    f1=spy_prices
    pl.plotFundsFlows(tit,df1,df2,df3,f1,'Total MF + ETF Net Inflows','Equity Total Net Inflows','Bond Total Net Inflows','S&P500 Cont Future')

    return d1,spy_prices

##############################################
# Report MDD History
##############################################              
def getMDDRetsTable(df,suffix='_1',days1sem=120,top=15,bydate=False,ascending=False,contrast=(0.7,1)):
    from Plib.Utils.Tools import exportDF
    i=0
    for t in df.columns:
        dd,dds=getMDDHistoryRets(df[[t]].copy(),days1sem=days1sem,top=top,bydate=bydate,ascending=ascending,contrast=contrast)
        dd['Symbol']=t
        if i==0:
            if len(dd)>0:
                res=dd.copy().head(top)
                i=i+1
        else:
            if len(dd)>0:
                res=res.append(dd.head(top))
    cols=['Symbol','Max Drawdown %','Begin','End','CumRets-1m','CumRets-3m','CumRets-6m','CumRets-1y','CumRets-3y','CumRets-5y','CumRets-10y']
    res=res[cols]

    res2=res.reset_index().copy()
    del res2['index']
    res2=res2.reset_index()
    res2=res2.set_index('index','symbol')
    res2['Begin'] = res2['Begin'].astype(str)
    res2['End'] = res2['End'].astype(str)

    cols=['CumRets-1m','CumRets-3m','CumRets-6m','CumRets-1y','CumRets-3y','CumRets-5y','CumRets-10y']
    ress=res2.copy().style.format({
                "Max Drawdown %": "{:20,.2f}", 
                "CumRets-1m": "{:20,.2f}",
                "CumRets-3m": "{:20,.2f}", 
                "CumRets-6m": "{:20,.2f}", 
                "CumRets-1y": "{:20,.2f}", 
                "CumRets-3y": "{:20,.2f}", 
                "CumRets-5y": "{:20,.2f}", 
                "CumRets-10y": "{:20,.2f}" 
               }).background_gradient(subset=cols, cmap='RdYlGn', vmin=None, vmax=None, low=contrast[0], high=contrast[1])

    png='mddT'+suffix+'.png'
    png=exportDF(ress, png)
    return res2,ress,png

def plotMDDRets(df,suffix='_1',days1sem=120,top=15,bydate=False,ascending=False,contrast=(0.7,1),fs=(60,70)):
    import matplotlib.pyplot as plt
    import matplotlib.image as mpimg
    
    fname=[]
    for t in df.columns:
        dd,dds=getMDDHistoryRets(df[[t]].copy(),days1sem=days1sem,top=top,bydate=bydate,ascending=ascending,contrast=contrast)
        if len(dd)>=1:
            fname.append('./output/' + 'mdd'+str(t)+'.png')
    
    mod=len(fname)%2
    rows=int(len(fname)/2)
    #if mod>0:
    #    rows=rows+1

    fnames=fname[:len(fname)-mod]

    imgs=[]
    for i in range(0,len(fnames)):
        imgs.append(mpimg.imread(fnames[i]))

    fig, axs = plt.subplots(nrows=rows,ncols=2,figsize=fs)
    ax = axs.flatten()
    for i in range(0,len(fnames)):
        ax[i].imshow(imgs[i])
        ax[i].set_title(fnames[i].split('.')[0].replace('mdd',''), fontdict={'fontsize': 30}, pad=20)
        ax[i].axis('off') 

    #plt.gcf().set_size_inches(50, 20)
    #plt.subplots_adjust(hspace=0,wspace=0.05)
    #plt.title('Assets Drawdown and Subsequent Returns', fontdict={'fontsize': 48}, pad=20)
    plt.tight_layout(pad=0.1, h_pad=3, w_pad=3)
    plt.show()
    
def getMDDHistoryRets(mdf,days1sem=120,top=15,bydate=True,ascending=False,contrast=(0.7,1)):
    from datetime import timedelta
    from Plib.Utils.Tools import exportDF
    
    png='mdd'+str(mdf.columns[0])+'.png'           
    
    def max_dd_with_dates(returns):
        r = returns.add(1).cumprod()
        dd = r.div(r.cummax()).sub(1)
        mdd = dd.min()
        end = dd.idxmin()
        start = r.loc[:end].idxmax()
        return mdd, start, end

    def getRets(mdf,dd,i,mdays=60,lbl='AAPL'):
        def cumProdRt(ret1=[],lbl_pr='',log_rets=False, as_pct=False):
            if log_rets:
                cumprod_ret = np.exp(ret1[lbl_pr].cumsum())-1
            else:
                cumprod_ret = (1+ret1[lbl_pr]).cumprod()-1
            if as_pct:
                cumprod_ret = cumprod_ret*100
            return cumprod_ret[-1].round(2)

        mmds=mdf[mdf.index >= dd.DE.iloc[i]].copy()
        ret1=mmds[mmds.index <= mmds.index.min() + timedelta(days=mdays)].pct_change().fillna(0)
        cprod= cumProdRt(ret1,lbl_pr=lbl,as_pct=True)
        #print(i,mdays,ret1,cprod)
        return cprod
        
    # Compute MDD for each subperiod
    semesters=int(len(mdf)/days1sem)
    offset=0
    mddl1=[]
    mddl2=[]
    mddl3=[]
    for i in range(1,semesters+1):
        df=mdf.iloc[offset:offset+days1sem].copy()

        lastd=df.index.min()
        dindex=df.index.max()
        #print(lastd,dindex)
        while dindex != lastd:
            dd,ds,de=max_dd_with_dates(df[df.index<=dindex][mdf.columns[0]].pct_change().fillna(0))
            #print(ds,de,dd)
            mddl1.append(ds)
            mddl2.append(de)
            mddl3.append(100*dd.round(4))
            dindex=ds

        offset=offset+days1sem

    ds=pd.DataFrame(mddl1)
    ds.columns=['DS']
    de=pd.DataFrame(mddl2)
    de.columns=['DE']
    dd=pd.DataFrame(mddl3)
    dd.columns=['MDD']
    dd=dd.join(ds.join(de))
    dd=dd.sort_values('MDD')
    dd=dd[dd.MDD <= -10]

    # Cumulative returns after the end of the MDD
    lr3=[]
    lr1=[]
    lr6=[]
    lr12=[]
    lr36=[]
    lr60=[]
    lr120=[]
    for i in range(0, len(dd)):
        lr1.append(getRets(mdf,dd,i,mdays=30,lbl=mdf.columns[0]))
        lr3.append(getRets(mdf,dd,i,mdays=90,lbl=mdf.columns[0]))
        lr6.append(getRets(mdf,dd,i,mdays=180,lbl=mdf.columns[0]))
        lr12.append(getRets(mdf,dd,i,mdays=360,lbl=mdf.columns[0]))
        lr36.append(getRets(mdf,dd,i,mdays=360*3,lbl=mdf.columns[0]))
        lr60.append(getRets(mdf,dd,i,mdays=360*5,lbl=mdf.columns[0]))
        lr120.append(getRets(mdf,dd,i,mdays=360*10,lbl=mdf.columns[0]))
    cdd=dd.copy()
    cdd['1M Rets']=lr1
    cdd['3M Rets']=lr3
    cdd['6M Rets']=lr6
    cdd['1Y Rets']=lr12
    cdd['3Y Rets']=lr36
    cdd['5Y Rets']=lr60
    cdd['10Y Rets']=lr120
    cdd.columns=['Max Drawdown %','Begin','End','CumRets-1m','CumRets-3m','CumRets-6m','CumRets-1y','CumRets-3y','CumRets-5y','CumRets-10y']

    
    # Sorting, Ranking, and Styling the table
    cdd2=cdd.copy()
    if top==0: 
        top=len(cdd2)
    if bydate:
        cdd2=cdd2.sort_values(['Begin','Max Drawdown %'],ascending=ascending)   
    cdd2['Begin'] = cdd2['Begin'].astype(str)
    cdd2['End'] = cdd2['End'].astype(str)

    cols=['CumRets-1m','CumRets-3m','CumRets-6m','CumRets-1y','CumRets-3y','CumRets-5y','CumRets-10y']
    cdds=cdd2.head(top).style.format({
                "Max Drawdown %": "{:20,.2f}", 
                "CumRets-1m": "{:20,.2f}",
                "CumRets-3m": "{:20,.2f}", 
                "CumRets-6m": "{:20,.2f}", 
                "CumRets-1y": "{:20,.2f}", 
                "CumRets-3y": "{:20,.2f}", 
                "CumRets-5y": "{:20,.2f}", 
                "CumRets-10y": "{:20,.2f}" 
               }).background_gradient(subset=cols, cmap='RdYlGn', vmin=None, vmax=None, low=contrast[0], high=contrast[1])

    png=exportDF(cdds, png)
    return cdd,cdds
    
##############################################
# Watchlists
##############################################              
def getWL(tickers, suffix='_1', refresh=False,years_history=1,realtime=True,price_dec=False,contrast=(0.7,0.5),provider='mstack',exch=['US']):
    import Plib.Utils.Tools as t
    import Plib.Plotting.Plots as pl
    import Plib.Plotting.Reports as rpt
    from Plib.Utils.Tools import wpkl,rpkl,exportDF

    dsh,de=t.getDates(offset=-360*11)
    ds,de=t.getDates(offset=-360*years_history)
    if refresh: 
        df,df2,df3,names,exchanges=rpt.getData(tickers,dsh,de,lbl=['Close','Volume'],provider=provider,exch=exch)
        wpkl('prices'+suffix, df); wpkl('volumes'+suffix, df2);wpkl('pricesRT'+suffix, df3);wpkl('names'+suffix, names);wpkl('exchanges'+suffix, exchanges)
    else:
        df=rpkl('prices'+suffix);df2=rpkl('volumes'+suffix);df3=rpkl('pricesRT'+suffix);names=rpkl('names'+suffix);exchanges=rpkl('exchanges'+suffix)
        if realtime:
            df3=getWLUpdate(df.columns,tickers,exchanges,provider=provider)
    wl,wls= rpt.getWatchlist(df[df.index>=ds].copy(),df2[df2.index>=ds].copy(),df3.copy(),names.copy(),realtime=realtime,contrast=contrast,price_dec=price_dec)
    
    png='assets'+suffix+'.png'
    png=exportDF(wls, png)
    return wls,(df,df2,df3,names,wl,png)

def getWLUpdate(cols,tickers,exchanges,provider='eod'):  
    import Plib.DataFarm.MStack as ms
    import Plib.DataFarm.EODQuotes as eod
    import Plib.DataFarm.FRData as fr
    import Plib.DataFarm.Kibot as kb
    
    df3=pd.DataFrame()
    if provider=='kibot':
        df3=kb.getAllRT(tname='', tickers=tickers)
    else:
        for t in tickers:
            exchange=exchanges[exchanges.index==t].head(1).Exchange[0]
            df3=df3.append(eod.getRTData(t+'.'+str(exchange)))
        df3['Tickers']=cols
        df3['Symbol']=df3.Tickers
        del df3['Tickers']
    return df3

def getData(tickers,dt_start,dt_end,lbl=['Close','Volume'],provider='mstack',exch=['US']):  
    import Plib.DataFarm.MStack as ms
    import Plib.DataFarm.EODQuotes as eod
    import Plib.DataFarm.FRData as fr
    import Plib.DataFarm.Kibot as kb
    import string
    
    # If all tickers are in the same exchange pass only the exchange code
    if len(exch)==1: exch=[exch[0] for t in tickers]
    
    ln=[]
    lne=[]
    lnt=[]
    i=0
    for t in tickers:
        if provider=='kibot':
            sr=kb.search(t)
        else:
            sr=eod.search(eod.gs(t),exch[i])
        ln.append(string.capwords(str(sr.head(1).values[0][2])))
        lne.append(string.capwords(str(sr.head(1).values[0][1])))
        lnt.append(str(sr.head(1).values[0][0]))
        i=i+1
    names=pd.DataFrame(ln)
    names.columns=['Name']
    names.index=tickers
    exchanges=pd.DataFrame(lne)
    exchanges.columns=['Exchange']
    exchanges.index=tickers
    #print(names,exchanges)
    
    df3=pd.DataFrame()
    for t in tickers:
        if provider=='kibot':
            df3=df3.append(kb.getRTData([t]))
        else:
            exchange=exchanges[exchanges.index==t].head(1).Exchange[0]
            df3=df3.append(eod.getRTData(t+'.'+str(exchange)))

    temp=[]
    i=0
    for s in tickers:
        if provider=='frdata':
            d1 = fr.getData('etf',s,d1=dt_start,d2=dt_end,freq='D') # 00:00:00
        elif provider=='mstack':
            d1 = ms.get_mstackHdata(s,dt_start,dt_end)
        elif provider=='kibot':
            d1 = kb.get_eod_data(s,dt_start,dt_end)    
        else:
            exchange=exchanges[exchanges.index==s].head(1).Exchange[0]
            d1 = eod.getData(s+'.'+str(exchange),sd=dt_start,ed=dt_end)
        d1c=d1[[lbl[0]]]
        d1c.columns=[s]
        d1v=d1[[lbl[1]]]
        d1v.columns=[s]
        if i==0:
            tempc=d1c.copy()
            tempv=d1v.copy()
        else:
            framesc = [tempc, d1c]
            tempc = pd.concat(framesc, axis=1,join='outer').interpolate().ffill().fillna(0)     # outer join to accomodate for tickers with less history
            framesv = [tempv, d1v]
            tempv = pd.concat(framesv, axis=1,join='outer').interpolate().ffill().fillna(0)
        i=i+1
    
    tempc=tempc.interpolate().dropna()
    colsc=tempc.columns
    tempv=tempv.interpolate().dropna()
    colsv=tempv.columns
    df3['Tickers']=tempc.columns
    df3['Symbol']=df3.Tickers
    del df3['Tickers']
    return tempc,tempv,df3,names,exchanges

def getWatchlist(df,df2,df3,names,realtime=True,contrast=(0.7,0.5),price_dec=False):
    
    def cumProdRt(s,log_rets=False, as_pct=True):
        if log_rets:
            cumprod_ret = np.exp(s.cumsum())-1
        else:
            cumprod_ret = (1+s).cumprod()-1
        if as_pct:
            cumprod_ret = cumprod_ret*100
        return cumprod_ret.round(2)
    
    def trend(s):
        import Plib.Stats.Regression as r

        M=df[[s.name]].reset_index().reset_index().set_index('Date')[['index',s.name]]
        X,Y=r.splitMatrix(M,s.name)
        ridge=1e-15
        yhat,mse,rlm=r.ridgeReg(X,Y,ridge_a=ridge,table=False,Plot=False)
        return rlm.steps[1][1].coef_[0][0].round(4)

    def rsi(s,wma=14,mw=1):
        import Plib.Signals.TAnalysis as ta
        return int(ta.getRSI(df.copy(),lbl_comp=s.name, winma=wma,type='EMA',lbl_out='',s=0)[['RSI_'+str(wma)]].iloc[-mw,-1].mean())

    def HL(s,win=50):
        import Plib.Signals.TAnalysis as ta
        L=ta.getRollingMin(df.copy(),lbl_comp=s.name,win=win,lbl_out='').iloc[-1,-1]
        H=ta.getRollingMax(df.copy(),lbl_comp=s.name,win=win,lbl_out='').iloc[-1,-1]
        return str(int(H))+'-'+str(int(L))

    def cRets(s,days=3):
        import Plib.Signals.TAnalysis as ta
        from Plib.Utils.Tools import getDatesYTD
        if days==0:
            d2,d1,ytd = getDatesYTD()
            days=len(df[df.index>=d1])
        dfr=df[s.name].pct_change()[-days:].copy()
        return cumProdRt(dfr).iloc[-1:].round(2)
        
    def VlmPerc(s):
        import Plib.Signals.TAnalysis as ta
        ranks = ta.getRank(df2.copy(),lbl_comp=s.name,method='ordinal',lbl_out='').iloc[:,-1]
        return int((100*(ranks - ranks.min())/ (ranks.max() - ranks.min()))[-1])

    def VolPerc(s,days=20):
        import Plib.Signals.TAnalysis as ta
    
        def volrealized(ret):
            import math
            N = ret.shape[0]
            df = (ret) ** 2
            sigma = math.sqrt((1 / (N - 1)) * df.sum())
            return sigma* math.sqrt(252)

        def rollVolRealized(df, days):
            import numpy as np
            mfun = lambda x: volrealized(x)
            rets = np.log(df / df.shift(1)).dropna()
            sigma = (rets.rolling(window=days).agg(mfun)).dropna()
            return sigma.round(2)
    
        vol=pd.DataFrame(rollVolRealized(df[s.name],days))
        ranks = ta.getRank(vol,lbl_comp=s.name,method='ordinal',lbl_out='').iloc[:,-1]
        return int((100*(ranks - ranks.min())/ (ranks.max() - ranks.min()))[-1])
    
    if not realtime:
        prices=pd.DataFrame(df.T.iloc[:,-1])
        rt=df3.set_index('Symbol')[['Close']]
        rt.columns=['Change %']
        rt['Change %']=0
    else:
        prices=df3.set_index('Symbol')[['Close']]
        rt=df.T.iloc[:,-1:].join(df3.set_index('Symbol')[['Close']])
        rt.columns=['LastEOD','RTClose']
        rt['Change %']=((rt.RTClose/rt.LastEOD)-1)
        rt['Change %']=rt['Change %'].astype(float)
        rt['Change %']=100*rt['Change %'].round(4)
    prices.columns=['Price']
    
    wl=rt[['Change %']].join(
    pd.DataFrame(df.apply(trend),columns=['Trend']).join(
    pd.DataFrame(df.apply(rsi),columns=['RSI(14)'])).join(
    pd.DataFrame(df.apply(HL),columns=['H-L'])).join(
    pd.DataFrame(df.apply(lambda x: cRets(x,3)).T.rename(columns={df.apply(cRets).T.columns[0]:'3D Rets %'}))).join(
    pd.DataFrame(df.apply(lambda x: cRets(x,5)).T.rename(columns={df.apply(cRets).T.columns[0]:'1W Rets %'}))).join(
    pd.DataFrame(df.apply(lambda x: cRets(x,0)).T.rename(columns={df.apply(cRets).T.columns[0]:'YTD Rets %'}))).join(
    pd.DataFrame(df2.apply(VlmPerc),columns=['Volm Rank'])).join(
    pd.DataFrame(df.apply(VolPerc),columns=['Vol Rank'])))
    wl=names.join(prices).join(wl)
    wl=wl[['Name','Price','RSI(14)','H-L','Volm Rank','Vol Rank','Trend','Change %','3D Rets %','1W Rets %','YTD Rets %']]
    
    if price_dec:
        mydec='2'
    else:
        mydec='0'
    ws=wl.style.format({"Price": "{:20,."+mydec+"f}", 
                "Trend": "{:20,.4f}", 
                "Change %": "{:20,.2f}",
                "3D Rets %":"{:20,.2f}",
                "1W Rets %":"{:20,.2f}",
                "YTD Rets %":"{:20,.2f}"
               }).background_gradient(subset=['Trend','Change %','3D Rets %','1W Rets %','YTD Rets %'], 
               cmap='RdYlGn', vmin=None, vmax=None, low=contrast[0], high=contrast[1])

    return wl,ws
    
