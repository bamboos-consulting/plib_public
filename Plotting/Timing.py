#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Report Timing
#
# Module comprising report and helper functions
#############################################################################################      

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path+"/")

#Common Libraries
import numpy as np
import pandas as pd
import math
from scipy import stats
import datetime

# Plib imports
import Plib.DataFarm.MStack as ms
import Plib.DataFarm.Kibot as kb
import Plib.Signals.TAnalysis as sg
import Plib.DataFarm.FRData as fr
import Plib.Plotting.Timing as pt
import Plib.Signals.FAnalysis as fa
import Plib.Utils.Tools as tl

##############################################
# Options for Jupyter
##############################################      
#This would display the graph in the notebook, but it was no longer interactive.
#%matplotlib inline
#With this the figures will now show up in the notebook , and still be interactive. 
#%matplotlib notebook

#other libraries
import warnings
warnings.filterwarnings("once")


##############################################
# Helper functions
##############################################           
def reportStyler(df):
    df = df.style.bar(subset=["Uptrend",], color='#2ECC71')\
                             .bar(subset=["Downtrend"], color='#EE1F5F')
    df.set_table_styles([dict(selector='th', props=[('text-align', 'left')])])
    return df
    
def dictdf2df(d,lcols=[]):
    md={key: d[key] for key in d.keys() & lcols}
    t=d[list(d.keys())[0]]
    for k in list(d.keys())[1:]:
        t=t.join(d[k]).interpolate().ffill().fillna(0)
    return t
    
def dictdf2df2(d,dt_start, dt_end, lcols=[]):
    md={key: d[key] for key in d.keys() & lcols}
    t=d[list(d.keys())[0]]
    t=t[t.index >= dt_start]
    t=t[t.index <= dt_end]
    for k in list(d.keys())[1:]:
        r=d[k]
        r=r[(r.index >= dt_start) & (r.index <= dt_end)]
        t=t.join(r).interpolate().ffill().fillna(0)
    return t
    
def getUpdate(df):
    data=df.copy()
    data=data.reset_index()
    data=data.set_index('Symbol')
    update=data[data.index.isin(cols)][['Close']].T
    update['Date']=datetime.date.today()
    update=update.set_index('Date')
    update.columns=list(update.columns)
    return update
    
##############################################
# Create a weighted index
##############################################           
def getMCIndex(result,dt_start,dt_end,universe,mweeks=52, mc_index=0, last_reporting='2022-04-30'):
    
    if mc_index==1:
        myselFunction = lambda s: fr.getRatios(s)[['MarketCapitalization']]    
        myrankFunction = fa.getTopNTickers
    elif mc_index==2:
        myselFunction = fa.getProfitability 
        myrankFunction = fa.getTopNTickers2
    elif mc_index==3:
        myselFunction = fa.getNetPayout        
        myrankFunction = fa.getTopNTickers2
    
        fdata=fr.getFundDataMarket(last_reporting=last_reporting, selFunc=myselFunction, debug=True)
        tickers_stocks=list(fr.getTickers('stock').Tickers.values)
        tickers=[t for t in list(fdata.keys()) if t in tickers_stocks]
        stocks=pt.getHistComp(universe, dt_start, dt_end, provider='frdata')
        midx=fa.getIndex(stocks, fdata, iname='Universe', Z=5, top=500, selfunction=myrankFunction, lbl='MarketCapitalization')
    
    else:
        midx = fr.getData('indices','SPTMI',d1=dt_start,d2=dt_end,freq='D')
        midx = midx[['Adjusted_close']]

    midx=midx.resample("W-MON").agg(["mean"])
    midx.columns=['MC.INDX']
    return midx.tail(mweeks)
    
##############################################
# Obtain data for constituents and indices
##############################################           
def getIndexes(result,dt_start,dt_end,universe,mweeks=52,provider='',mc_index=0,last_reporting='2022-04-30'):
    
    if last_reporting=='':
        last_reporting=str(pd.date_range(start =dt_start, end =dt_end, freq ="Q")[-1]+ pd.DateOffset(months=1))[0:10]
    
    if provider=='frdata': 
        tickers=pd.DataFrame(['SPX','OEX','DJI','MID','SMLG','RUI','NDX','SPSPG','SPSPV','DVS','XAX','RUA'],columns=['Ticker'])
    else:
        tickers=pd.DataFrame(['SPTMI.INDX','US500.INDX','OEX.INDX','US30.INDX','MID.INDX','SML.INDX','RUI.INDX','NDX.INDX','RLG.INDX','RLV.INDX','SPDAUDT.INDX','XAX.INDX','MSCIWORLD.INDX'],columns=['Ticker'])
        
    temp=[]
    i=0
    for s in list(tickers.Ticker):
        try:
            if provider=='frdata':
                d1 = fr.getData('indices',s,d1=dt_start,d2=dt_end,freq='D') # 00:00:00
            else:
                d1 = ms.get_mstackHdata(s,dt_start,dt_end)
            d1=d1[['Adjusted_close']]
            d1.columns=[s]
            if i==0:
                temp=d1.copy()
            else:
                frames = [temp, d1]
                temp = pd.concat(frames, axis=1)
        except:
            pass;
        i=i+1
    
    temp=temp.interpolate().dropna()
    cols=temp.columns
    temp=temp.resample("W-MON").agg(["mean"])
    temp.columns=cols
    
    temp=temp.tail(mweeks)
    temp=pd.concat([getMCIndex(result,dt_start,dt_end,universe,mweeks,mc_index),temp],axis=1)
    
    return temp
   
def getHistComp(tickers,dt_start,dt_end,lbl='Close', provider='', min_days=252, debug=True):
    import time
    
    flist={}
    start=time.perf_counter()
    temp=[]
    i=0
    tickers=[t for t in tickers if not ((str(t).find('-')>0) or (str(t).find('_')>0) or (str(t).find('.')>0))]
    for s in tickers:
        #try:
        if provider == 'frdata':
            d1 = fr.getData('stock',s,d1=dt_start,d2=dt_end,freq='D') # 00:00:00
        elif provider == 'kibot':
            d1 = kb.get_eod_data(s,dt_start=dt_start,dt_end=dt_end, adj=0) # 00:00:00
        else:
            d1 = ms.get_mstackHdata(s,dt_start,dt_end,limit=800)
        if len(d1) > min_days:
            d1=d1[[lbl]]
            d1.columns=[s] #.replace('.','')
            flist[s]=d1.interpolate().dropna()
            i=i+1
            if (i%500==0) and (debug):
                end=time.perf_counter()
                print('Collecting '+str(i),' of ',str(len(tickers)), ' - ',str(end-start),' seconds')
        #except:
        #    pass;
        
    return flist #temp.interpolate().dropna()

def getSectorsHistComp(dt_start, dt_end, labl='Close',mweeks=26, provider='',data={}, debug=True):
    import time
    
    universe=[]
    if provider=='frdata':
        tickers_stocks=list(fr.getTickers('stock').Tickers.values)
        Sectors=fr.getSectorsConstituents()
    elif provider=='kibot':
        tickers_stocks=kb.getTickers('stock')
        Sectors=kb.getSectorsConstituents()
    if data != {}:
        tickers_data=list(data.keys())
        Sectors=kb.getSectorsConstituents()
    start=time.perf_counter()
    
    j=0
    for sec in Sectors:
        tickers=list(Sectors[[sec]].dropna()[sec])
        tickers=[t for t in tickers if not ((str(t).find('-')>0) or (str(t).find('_')>0) or (str(t).find('.')>0))]
        tickers=[t for t in tickers if t in tickers_stocks]
        
        flist={}
        i=0
        mmod=int(len(tickers)/6)
        for s in tickers:
            #try:
            if data == {}:
                if provider == 'frdata':
                    d1 = fr.getData('stock',s,d1=dt_start,d2=dt_end,freq='D') # 00:00:00
                elif provider == 'kibot':
                    d1 = kb.get_eod_data(s,dt_start=dt_start,dt_end=dt_end, adj=0) # 00:00:00
                else:
                    d1 = ms.get_mstackHdata(s,dt_start,dt_end,limit=800)
            else:
                if s in tickers_data:
                    d1=data[s].copy()
                    d1=d1[(d1.index >= dt_start) & (d1.index <= dt_end)]
                else:
                    d1=pd.DataFrame()
            if len(d1)>(mweeks*5):
                if data == {}:
                    d1=d1[[labl]]
                d1.columns=[s] #.replace('.','')
                flist[s]=d1.interpolate().fillna(method="ffill",limit=20)
                i=i+1
                universe.append(s)
                if (i%mmod==0) and (debug):
                    end=time.perf_counter()
                    print(str(sec)+' - Collecting '+str(i),' of ',str(len(tickers)), ' - ',str(end-start),' seconds')
                #print(i,s,sec,len(d1))
            #except:
            #    pass;
            
        #if j==2: break;
        if len(flist)>0:
            #temp = temp.interpolate().fillna(method="ffill",limit=20)
            nrec = len(pd.date_range(start =dt_start, end =dt_end, freq ="W-MON"))
            temp2=getMarketTimingMACE(flist, nrec, mweeks, w_ema1=20, w_ema2=65, w_ema3=130, avgw=4)
            temp2=temp2.dropna().tail(1)     ##  modified to account for midweek runs
            #temp2=temp2.fillna(0).tail(1)   ##
            temp2=temp2.reset_index()
            del temp2['Date']
            temp2['Sector']=sec
            temp2=temp2.set_index('Sector')

            if j==0:
                SecTrend=temp2.copy()
            else:
                SecTrend=SecTrend.append(temp2)
            j=j+1
    
    SecTrend['Uptrend']=(SecTrend['U1']+SecTrend['U2']+SecTrend['U3'])/(SecTrend['U1']+SecTrend['U2']+SecTrend['U3']+SecTrend['D1']+SecTrend['D2']+SecTrend['D3'])
    SecTrend['Downtrend']=(SecTrend['D1']+SecTrend['D2']+SecTrend['D3'])/(SecTrend['U1']+SecTrend['U2']+SecTrend['U3']+SecTrend['D1']+SecTrend['D2']+SecTrend['D3'])
    
    return reportStyler(SecTrend),universe
   
def getHistUpdates(result):
    ret=kb.getAllRT(tname='', tickers=list(result.keys()))
    #pd.to_pickle(ret,'update.pkl')
    rtlist=list(ret.Symbol.values)
    for k in result.keys():
        df=result[k]
        if k in rtlist:
            value=ret[ret['Symbol']==k][['Close']].iloc[0][0]
        else:
            value=result[k].iloc[-1][0]
        df=tl.addRowDF(df, value=value, offset='1', freq='days')
        result[k]=df
    return result    
     
#################################################
# Prepare Timing Stats from stocks constituents
#################################################           
def getMarketTiming(result, nrec, mweeks=52, nweeks=52, avgw=10):
    symbols=list(result.keys())
    i=0
    universe=[]
    for s in symbols:
        d1=result[s].copy().interpolate().dropna()
        d1w=d1.resample("W-MON").agg(["min", "max"])
        d1w.columns = d1w.columns.to_flat_index()
        d1w.columns = ['min','max']
        
        if not len(d1w.dropna())<nrec:    
            #nweeks=len(d1w)
            d1w['NewMax']=d1w['max'].rolling(min_periods=1, window=nweeks, center=False).max()
            d1w['NewMin']=d1w['min'].rolling(min_periods=1, window=nweeks, center=False).min()
            d1w['cNewMax']=np.where(d1w.NewMax > d1w['max'], 0, 1)
            d1w['cNewMin']=np.where(d1w.NewMin < d1w['min'], 0, 1)
            if i==0: 
                temp=d1w.copy()
            else:
                temp['cNewMax']=temp['cNewMax']+d1w['cNewMax']
                temp['cNewMin']=temp['cNewMin']+d1w['cNewMin']
            i=i+1
            universe.append(s)

    temp=temp[['cNewMax','cNewMin']]
    temp[str(avgw)+'WavgMax']=temp['cNewMax'].rolling(avgw).mean()
    temp[str(avgw)+'WavgMin']=temp['cNewMin'].rolling(avgw).mean()
    temp=temp[['cNewMax',str(avgw)+'WavgMax','cNewMin',str(avgw)+'WavgMin']]

    return temp.tail(mweeks),universe
        
def getMarketTiming2(result, nrec, mweeks=52, w_ema=40, avgw=10):
    
    symbols=list(result.keys())
    i=0
    for s in symbols:
        d1=result[s].copy().interpolate().dropna()

        d1=sg.getEMA(d1,s,w_ema,'emaFast').fillna(0)
        d1w=d1.resample("W-MON").agg(["mean"])
        d1w.columns = d1w.columns.to_flat_index()
        d1w.columns = ['price','ema']
        
        if not len(d1w.dropna())<nrec:   
            nweeks=len(d1w)
            d1w['++']=np.where((d1w['price']>d1w['ema']) & (d1w['price'].shift(1)>d1w['ema'].shift(1)), 1, 0)
            d1w['+-']=np.where((d1w['price']<d1w['ema']) & (d1w['price'].shift(1)>d1w['ema'].shift(1)), 1, 0)
            d1w['--']=np.where((d1w['price']<d1w['ema']) & (d1w['price'].shift(1)<d1w['ema'].shift(1)), 1, 0)
            d1w['-+']=np.where((d1w['price']>d1w['ema']) & (d1w['price'].shift(1)<d1w['ema'].shift(1)), 1, 0)
            if i==0: 
                temp=d1w.copy()
            else:
                temp['++']=temp['++']+d1w['++']
                temp['+-']=temp['+-']+d1w['+-']
                temp['-+']=temp['-+']+d1w['-+']
                temp['--']=temp['--']+d1w['--']
            i=i+1

    temp['Net+']=temp['++']+temp['-+']
    temp['Net%']=temp['Net+']/(temp['++']+temp['+-']+temp['-+']+temp['--'])
    temp[str(avgw)+'WavgNet+']=temp['Net+'].rolling(avgw).mean()
    del temp['price']
    del temp['ema']
    return temp.tail(mweeks)

def getMarketTimingMACE(result, nrec, mweeks=52, w_ema1=20, w_ema2=65, w_ema3=130, avgw=4):
    
    symbols=list(result.keys())
    i=0
    for s in symbols:
        d1=result[s].copy().interpolate().dropna()

        d1=sg.getEMA(d1,s,w_ema1,'emaFast').fillna(0)
        d1=sg.getEMA(d1,s,w_ema2,'emaMid').fillna(0)
        d1=sg.getEMA(d1,s,w_ema3,'emaSlow').fillna(0)

        d1w=d1.resample("W-MON").agg(["mean"])
        d1w.columns = d1w.columns.to_flat_index()
        d1w.columns = ['price','emaFast','emaMid','emaSlow']

        if not len(d1w.dropna())<nrec:   
            nweeks=len(d1w)
            d1w['U1']=np.where((d1w['emaFast']>d1w['emaMid']) & (d1w['emaFast'].shift(1)<d1w['emaMid'].shift(1)), 1, 0)
            d1w['U2']=np.where((d1w['emaFast']>d1w['emaSlow']) & (d1w['emaFast'].shift(1)<d1w['emaSlow'].shift(1)), 1, 0)
            d1w['U3']=np.where((d1w['emaFast']>d1w['emaSlow']) & (d1w['emaMid'].shift(1)<d1w['emaSlow'].shift(1)), 1, 0)

            d1w['D1']=np.where((d1w['emaFast']<d1w['emaMid']) & (d1w['emaFast'].shift(1)>d1w['emaMid'].shift(1)), 1, 0)
            d1w['D2']=np.where((d1w['emaFast']<d1w['emaSlow']) & (d1w['emaFast'].shift(1)>d1w['emaSlow'].shift(1)), 1, 0)
            d1w['D3']=np.where((d1w['emaFast']<d1w['emaSlow']) & (d1w['emaMid'].shift(1)>d1w['emaSlow'].shift(1)), 1, 0)

            if i==0: 
                temp=d1w.copy()
            else:
                temp['U1']=temp['U1']+d1w['U1']
                temp['U2']=temp['U2']+d1w['U2']
                temp['U3']=temp['U3']+d1w['U3']

                temp['D1']=temp['D1']+d1w['D1']
                temp['D2']=temp['D2']+d1w['D2']
                temp['D3']=temp['D3']+d1w['D3']
            i=i+1

    temp['U2+U3']=temp['U2']+temp['U3']
    temp['U2+U3/4WK']=temp['U2+U3'].rolling(4).mean()
    temp['U2+U3/%']=np.where((temp['U1']+temp['U2']+temp['U3']+temp['D1']+temp['D2']+temp['D3']) == 0, 0, temp['U2+U3']/(temp['U1']+temp['U2']+temp['U3']+temp['D1']+temp['D2']+temp['D3']))    
    #temp['U2+U3/%']=temp['U2+U3']/(temp['U1']+temp['U2']+temp['U3'])
    temp['U2+U3/%/4WK']=temp['U2+U3/%'].rolling(4).mean()
    del temp['price']
    del temp['emaFast']
    del temp['emaMid']
    del temp['emaSlow']

    return temp.tail(mweeks)

def prepareMarketTiming(result, dt_start, dt_end, mweeks=52, nweeks=26, avgw=10, w_ema=40, w_ema1=20, w_ema2=65, w_ema3=130, avgwi=4):
    nrec = len(pd.date_range(start =dt_start, end =dt_end, freq ="W-MON"))
    mt1,universe=getMarketTiming(result,nrec,mweeks,nweeks,avgw)
    mt2=getMarketTiming2(result,nrec, mweeks, w_ema, avgw)
    mt3=getMarketTimingMACE(result,nrec,mweeks,w_ema1, w_ema2, w_ema3, avgwi)
    frame=[mt1,mt2,mt3]
    report1=pd.concat(frame,axis=1)
    return report1,universe

#################################################
# Prepare Timing Reports in PDF
#################################################           
def printReportTiming(report1, mweeks='', nweeks='', porfdes=''):
    import pdfkit
    import datetime 
    
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', 2000)
    pd.set_option('display.colheader_justify', 'right')
    pd.set_option('display.precision', 2)

    options = {
        'page-size': 'A4',
        'margin-top': '0.5in',
        'margin-right': '0.1in',
        'margin-bottom': '0.5in',
        'margin-left': '0.1in',
    }

    f = open('./output/ReportTiming1.html','w')
    report=report1.reset_index()
    
    title='Market Timing Data Over '
    title=str(title)+str(mweeks)+' Weeks for '+ str(porfdes) + '. New max/min over ' + str(nweeks) + ' Weeks.'
    htitle1='<h2>'+title+'</h2>'
    legend='<br><tr><th colspan="8">Legend:</th></tr><br>'
    l1='cNewMax,cNewMin: refer to New max/min over the specified period (26/52 weeks)'
    l2='10WavgMax,10WavgMin: refer to the 10 weeks MA'
    l3='++, +-, --, -+: refer to the components crossover against the 40 weeks MA'
    l4='Net+, Net%, 10WavgNet+: refer to the sum of ++, +-'
    l5='U and D columns: refer to the MA cycle evaluation against fast, mid, and slow MA with '
    l6='U1, U2, U3: refer to uptrend of fast/mid, fast/slow, and mid/slow MA'
    l7='D1, D2, D3: refer to downtrend of fast/mid, fast/slow, and mid/slow MA'
    l8='U2+U3, U2+U3/4WK, U2+U3/%, U2+U3/%/4WK: refer to the confirmed and established trends'
    legend=legend + '<tr><th colspan="8">'+l1+'</th></tr><br>'
    legend=legend + '<tr><th colspan="8">'+l2+'</th></tr><br>'
    legend=legend + '<tr><th colspan="8">'+l3+'</th></tr><br>'
    legend=legend + '<tr><th colspan="8">'+l4+'</th></tr><br>'
    legend=legend + '<tr><th colspan="8">'+l5+'</th></tr><br>'
    legend=legend + '<tr><th colspan="8">'+l6+'</th></tr><br>'
    legend=legend + '<tr><th colspan="8">'+l7+'</th></tr><br>'
    legend=legend + '<tr><th colspan="8">'+l8+'</th></tr><br>'
    
    a = report.to_html(classes=['table-bordered','table-striped','table-hover fixed'],index=False,index_names=False,
                       col_space=[200,75,75,75,75,75,30,30,30,30,30,30,75,30,30,30,30,30,30,75,75,75])
    a=a.replace('<tr>','<tr style="text-align: right;">').replace('border="1"','border="0"')
    f.write(htitle1 + a + legend)
    f.close()
    mydate=str(datetime.date.today()).replace('-','_')
    pdfkit.from_file('./output/ReportTiming1.html', './output/ReportTiming_'+ mydate +'.pdf', options=options)

def printReportIndices(report2, mweeks='', porfdes=''):
    import pdfkit
    import datetime 
    
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', 2000)
    pd.set_option('display.colheader_justify', 'right')
    pd.set_option('display.precision', 2)

    options = {
        'page-size': 'A4',
        'margin-top': '0.5in',
        'margin-right': '0.1in',
        'margin-bottom': '0.5in',
        'margin-left': '0.1in',
    }

    f = open('./output/ReportTiming2.html','w')
    report=report2.reset_index()
    
    title='Indices Timing Data Over '
    title=str(title)+str(mweeks)+' Weeks for '+ str(porfdes)
    htitle1='<h2>'+title+'</h2>'
    legend='<br><tr><th colspan="8">Legend:</th></tr><br>'
    l1='S&P 500 Futures                                 US500.INDX'
    l2='S&P 100                                           OEX.INDX'
    l3='Dow Jones 30 Futures                             US30.INDX'
    l4='S&P Midcap 400                                    MID.INDX'
    l5='S&P Small-Cap 600  Index                          SML.INDX'
    l6='Russell 1000                                      RUI.INDX '
    l7='Nasdaq 100                                        NDX.INDX '
    l8='RUSSELL 1000 GROWTH INDX                          RLG.INDX'
    l9='RUSSELL 1000 VALUE INDEX                          RLV.INDX'
    l10='S&P 500 Dividends Aristocrats Total Return   SPDAUDT.INDX'
    l11='NYSE AMEX Composite                              XAX.INDX'
    l12='MSCI WORLD                                 MSCIWORLD.INDX'
    legend=legend + '<tr><th colspan="8">'+l1+'</th></tr><br>'
    legend=legend + '<tr><th colspan="8">'+l2+'</th></tr><br>'
    legend=legend + '<tr><th colspan="8">'+l3+'</th></tr><br>'
    legend=legend + '<tr><th colspan="8">'+l4+'</th></tr><br>'
    legend=legend + '<tr><th colspan="8">'+l5+'</th></tr><br>'
    legend=legend + '<tr><th colspan="8">'+l6+'</th></tr><br>'
    legend=legend + '<tr><th colspan="8">'+l7+'</th></tr><br>'
    legend=legend + '<tr><th colspan="8">'+l8+'</th></tr><br>'
    legend=legend + '<tr><th colspan="8">'+l9+'</th></tr><br>'
    legend=legend + '<tr><th colspan="8">'+l10+'</th></tr><br>'
    legend=legend + '<tr><th colspan="8">'+l11+'</th></tr><br>'
    legend=legend + '<tr><th colspan="8">'+l12+'</th></tr><br>'

    a = report.to_html(classes=['table-bordered','table-striped','table-hover fixed'],index=False,index_names=False,
                       col_space=[200,90,90,90,90,90,90,90,90,90,90,90,90,90])
    a=a.replace('<tr>','<tr style="text-align: right;">').replace('border="1"','border="0"')
    f.write(htitle1 + a + legend)
    f.close()
    mydate=str(datetime.date.today()).replace('-','_')
    pdfkit.from_file('./output/ReportTiming2.html', './output/ReportIndices_'+ mydate +'.pdf', options=options)

def printReportTrending(sectrend, title):
    import pdfkit
    import datetime 

    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', 2000)
    pd.set_option('display.colheader_justify', 'right')
    pd.set_option('display.precision', 2)

    options = {
        'page-size': 'A4',
        'margin-top': '0.5in',
        'margin-right': '0.15in',
        'margin-bottom': '0.5in',
        'margin-left': '0.15in',
    }
    f = open('./output/ReportTiming3.html','w')

    htitle1='<h2>'+title+'</h2>'

    my_html_string = htitle1 + sectrend.render()

    import re
    p = re.compile('background: (.*);')
    unique_bg_statements = list(set(p.findall(my_html_string)))
    for bs in unique_bg_statements:
        my_html_string = my_html_string.replace(bs,bs+" !important") #add !important
    my_html_string = my_html_string.replace("transparent","white")
    
    f.write(my_html_string )
    f.close()
    mydate=str(datetime.date.today()).replace('-','_')
    pdfkit.from_file('./output/ReportTiming3.html', './output/ReportTrending_'+ mydate +'.pdf', options=options)


