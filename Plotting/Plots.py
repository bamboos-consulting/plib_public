#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Plots 
#
# Module comprising plot and helper functions
#############################################################################################      


#Common Libraries
import numpy as np
import pandas as pd
pd.options.mode.chained_assignment = None  # default='warn'

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path+"/")
#import seaborn

# Need to import the matplotlib_converters from pandas,
# whenever you try to plot the columns of a dataframe using a for loop
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

import matplotlib.style
import matplotlib as mpl

#Gridstyle
mpl.rcParams['grid.color'] = 'k'
mpl.rcParams['grid.linestyle'] = ':'
mpl.rcParams['grid.linewidth'] = 0.5
mpl.rcParams['axes.grid'] = True
#Resolution
mpl.rcParams['figure.dpi'] = 200
mpl.rcParams['savefig.dpi'] = 300
#Fonts
SMALL_SIZE = 16
MEDIUM_SIZE = 18
BIGGER_SIZE = 20
import matplotlib.pyplot as plt
#base stylers
plt.style.use(['seaborn-ticks'])
#['seaborn-dark', 'seaborn-darkgrid', 'seaborn-ticks', 'fivethirtyeight', 'seaborn-whitegrid', 'classic', '_classic_test', 'fast', 'seaborn-talk', 'seaborn-dark-palette', 'seaborn-bright', 'seaborn-pastel', 'grayscale', 'seaborn-notebook', 'ggplot', 'seaborn-colorblind', 'seaborn-muted', 'seaborn', 'Solarize_Light2', 'seaborn-paper', 'bmh', 'tableau-colorblind10', 'seaborn-white', 'dark_background', 'seaborn-poster', 'seaborn-deep']
plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

##############################################
# Options for Jupyter
##############################################      
#This would display the graph in the notebook, but it was no longer interactive.
#%matplotlib inline
#With this the figures will now show up in the notebook , and still be interactive. 
#%matplotlib notebook

linestyle_tuple = [
     ('loosely dotted',        (0, (1, 10))),
     ('dotted',                (0, (1, 1))),
     ('densely dotted',        (0, (1, 1))),

     ('loosely dashed',        (0, (5, 10))),
     ('dashed',                (0, (5, 5))),
     ('densely dashed',        (0, (5, 1))),

     ('loosely dashdotted',    (0, (3, 10, 1, 10))),
     ('dashdotted',            (0, (3, 5, 1, 5))),
     ('densely dashdotted',    (0, (3, 1, 1, 1))),

     ('dashdotdotted',         (0, (3, 5, 1, 5, 1, 5))),
     ('loosely dashdotdotted', (0, (3, 10, 1, 10, 1, 10))),
     ('densely dashdotdotted', (0, (3, 1, 1, 1, 1, 1)))]

##############################################
# Correlation Function
##############################################              
def rolling(s1,s2,win=5):
    from scipy.stats import spearmanr
    sr=s1.copy()
    sr.name='corr'
    for i in range(0,len(s1)-win+1):
        sr.iloc[i:i+win],p = spearmanr(s1.iloc[i:i+win], s2.iloc[i:i+win])
    return sr

##############################################
# Correlation Studies Plots
##############################################              
def corrStudy(title,index_levels, lreturns, tick1, tick2, d1, d2, recess):
    from matplotlib.gridspec import GridSpec
    import numpy as np
    import matplotlib.pyplot as plt
    #from pandas.plotting import register_matplotlib_converters
    #register_matplotlib_converters()

    fig = plt.figure(figsize=(10, 13), clear=True, constrained_layout=False)

    gs = GridSpec(5, 10, figure=fig)

    ax1 = plt.subplot(gs.new_subplotspec((0, 0), colspan=10))
    ax2 = ax1.twinx()
    ax1.plot(index_levels.iloc[:, 0], linestyle='--', color='g', linewidth=1, label=tick1)
    ax2.plot(index_levels.iloc[:, 1], color='b', linewidth=1,label=tick2)
    ax1.legend(loc='upper left')
    ax2.legend(loc='upper right')

    ax2 = plt.subplot(gs.new_subplotspec((2, 0), colspan=5))
    ax2 = rolling(lreturns.iloc[:, 0],lreturns.iloc[:, 1],win=20).plot()
    #ax2 = lreturns.iloc[:, 0].rolling(window=20).corr(lreturns.iloc[:, 1]).plot()
    ax2.axhline(lreturns.corr().iloc[0, 1], c='r');
    ax2.legend(['20-Days Rolling Correlation', 'Historical Correlation'], loc='upper left')
    ax2.axes.get_xaxis().set_visible(False)

    ax3 = plt.subplot(gs.new_subplotspec((2, 5), colspan=5))
    ax3 = lreturns.iloc[:, 0].rolling(window=60).corr(lreturns.iloc[:, 1]).plot()
    ax3.axhline(lreturns.corr(method='spearman').iloc[0, 1], c='r');
    ax3.legend(['60-Days Rolling Correlation', 'Historical Correlation'], loc='upper left')
    ax3.yaxis.tick_right()
    ax3.axes.get_xaxis().set_visible(False)

    ax4 = plt.subplot(gs.new_subplotspec((3, 0), colspan=10))
    ax4 = rolling(lreturns.iloc[:, 0],lreturns.iloc[:, 1],win=252*4).plot()
    #ax4 = lreturns.iloc[:, 0].rolling(window=252 * 4).corr(lreturns.iloc[:, 1]).plot()
    ax4.axhline(lreturns.corr().iloc[0, 1], c='r');
    ax4.legend(['4-Years Rolling Correlation', 'Historical Correlation'], loc='upper left')

    ax5 = plt.subplot(gs.new_subplotspec((1, 0), rowspan=1, colspan=10))
    ols = np.polyfit(lreturns.iloc[:, 0], lreturns.iloc[:, 1], deg=1)
    ax6 = ax5.twinx()
    plt.scatter(lreturns.iloc[:, 0], lreturns.iloc[:, 1])
    ax6.plot(lreturns.iloc[:, 0], np.polyval(ols, lreturns.iloc[:, 0]), 'r', lw=2)
    
    mcorr=round(lreturns.corr(method='spearman').iloc[0,1],4)
    title = 'Correlation Study' + '\n' + \
        title + '\n' + \
        d1 + ' to ' + d2 + '\nCorrelation = ' + str(mcorr)
    fig.suptitle(title, fontsize=16)

    for i in range(len(recess)):
        d = recess[i]
        ax1.axvspan(d[0], d[1], color='y', alpha=0.5, lw=0)
        ax2.axvspan(d[0], d[1], color='y', alpha=0.5, lw=0)
        ax3.axvspan(d[0], d[1], color='y', alpha=0.5, lw=0)
        ax4.axvspan(d[0], d[1], color='y', alpha=0.5, lw=0)

    name = tick1.replace("^", "") + tick2.replace("^", "") + d2
    fig.savefig('corr' + name + '.png', dpi=fig.dpi, bbox_inches='tight', pad_inches=0.3)

    
    return fig

##############################################
# Helper functions
##############################################      
def customize_grid(ax,x=2,y=2):
    from matplotlib.ticker import AutoMinorLocator,MultipleLocator, FormatStrFormatter
    #xmin,xmax=ax.get_xlim()
    #delta=xmax-xmin
    if x>0:
        ax.xaxis.set_minor_locator(AutoMinorLocator(x))
    if y>0:
        ax.yaxis.set_minor_locator(AutoMinorLocator(y))
    if x > 0 or y >0:
        ax.grid(which='minor', color='#CCCCCC', linestyle='--')

def get_bollinger(data, winma=25, alpha=2):
    ser = pd.Series(data)
    ma = ser.rolling(winma).mean()
    std = ser.rolling(winma).std()
    lower = pd.Series(ma - alpha * std).fillna(method='bfill').values
    upper = pd.Series(ma + alpha * std).fillna(method='bfill').values
    return lower, upper

def get_rsi(prices, period=14):
    rsi = []
    rsi = pd.DataFrame(columns=['Date', 'RSI'])
    for i in range(len(prices.Adjusted_close) - period):
        gains = 0.0
        losses = 0.0
        window = prices.Adjusted_close[i:i + period + 1]
        pdate = prices.index[i]
        for year_one, year_two in zip(window, window[1:]):
            diff = year_two - year_one

            if diff > 0:
                gains += diff
            else:
                losses += abs(diff)
        # Check if `losses` is zero. If so, `100/(1 + RS)` will practically be 0.
        if not losses:
            # rsi.append(100.00)
            rsi = rsi.append({'Date': pdate, 'RSI': 100.00}, ignore_index=True)
        else:
            # rsi.append(round(100 - (100 / (1 + gains / losses)), 2))
            rsi = rsi.append({'Date': pdate, 'RSI': round(100 - (100 / (1 + gains / losses)), 2)}, ignore_index=True)
    rsi = rsi.set_index('Date')
    return rsi

##############################################
# Options Strategy Plot
##############################################         
def plotUnderlyingStudy2(symbol, dt_start, dt_end, prices, option,do='',prb=0.50,lt_win=50, winma=25, alpha=2):
    import warnings
    import matplotlib.dates as mdates
    import matplotlib as mpl
    from mplfinance.original_flavor import candlestick_ohlc
    #from mpl_finance import candlestick_ohlc
    from matplotlib import gridspec
    from scipy.stats import norm
    from scipy.stats import lognorm
    import matplotlib.pyplot as plt
    import Plib.Options.Models as om
    
    # prepare data
    df = prices[['Open', 'High', 'Low', 'Adjusted_close']]
    df.columns=['Open', 'High', 'Low', 'Close']
    #df['SMA20'] = prices['Adjusted_close'].rolling(20).mean()
    # Reset the index and convert date into float
    df.reset_index(inplace=True)
    df['Date'] = df['Date'].map(mdates.date2num)

    # The top plot consisting of daily candlesticks
    top = plt.subplot2grid((9, 6), (0, 0), rowspan=5, colspan=4)
    candlestick_ohlc(top, df.values, width=2, colorup='g', colordown='r')
    top.xaxis_date()
    top.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    # top.plot(prices.index, prices.Adjusted_close, color='black',label='Adjusted_close')
    plt.title(symbol + ' Adjusted_close Price (' + dt_start + '-' + dt_end + ')')

    # Bollinger Bands
    low, up = get_bollinger(prices.Adjusted_close, winma, alpha)
    minb, maxb = low.mean(), up.mean()
    top.plot(prices.index, prices.Adjusted_close.rolling(window=winma).mean(), linestyle=':', color='grey', linewidth=1,
             label='Price '+str(winma)+' MA')
    top.plot(prices.index, prices.Adjusted_close.rolling(window=lt_win).mean(), linestyle='-', color='red', linewidth=1,
             label='Price '+str(lt_win)+' MA')
    x_axis = prices.index.get_level_values(0)
    top.fill_between(x_axis, low, up, alpha=0.3, color='silver', label=str(lt_win-winma)+'MA Bollinger Bands with ' + str(alpha) + ' SD')
    # linestyle=linestyle, color=color, linewidth=3 ['-', '--', '-.', ':']
    top.axhline(y=minb, xmin=0.0, xmax=1.0, linestyle='--', color='r', linewidth=1, label='MinMax BB')
    top.axhline(y=maxb, xmin=0.0, xmax=1.0, linestyle='--', color='r', linewidth=1)
    top.legend(loc=2,prop={'size': 10})

    # The bottom plot consisting of daily trading volume
    bottom = plt.subplot2grid((9, 6), (5, 0), rowspan=2, colspan=4, sharex=top)
    bottom.bar(prices.index, prices.Volume, color='orange')
    bottom.plot(prices.index, prices.Volume.rolling(window=winma).mean(), linestyle='--', color='red', linewidth=3,
                label=str(winma)+' MA')
    bottom.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    bottom.legend(loc=2,prop={'size': 10})
    plt.title(symbol + ' Daily Trading Volume')
    
    # The side upper plot of the option
    stop = plt.subplot2grid((9, 6), (0, 4), rowspan=5, colspan=3, sharey=top)
    stop.plot(option.payoff, option.und_price, color='black')#,label='Option Payoff')
    
    maxS,minS=round(option[option['payoff']>=0]['und_price'].max(),4),round(option[option['payoff']>=0]['und_price'].min(),4)
    stop.yaxis.tick_right()
    stop.annotate(str(maxS), xy=(0, maxS),  xycoords='data',
            xytext=(0.5*option['payoff'].max(), maxS), textcoords='data',
            arrowprops=dict(facecolor='black', shrink=0.50),
            horizontalalignment='right', verticalalignment='top',
            )
    stop.annotate(str(minS), xy=(0, minS),  xycoords='data',
            xytext=(0.5*option['payoff'].max(), minS), textcoords='data',
            arrowprops=dict(facecolor='black', shrink=0.50),
            horizontalalignment='right', verticalalignment='top',
            )
    stop.vlines(x=0, ymin=minS, ymax=maxS, color='red', zorder=2,linestyles='dotted')
    
    # Plot Histogram of underlying prices
    stop2=stop.twiny()
    n, bins, patches = stop2.hist(prices.Adjusted_close, bins=50,density=1,orientation=u'horizontal',alpha = 0.4,label='Histogram of Prices')
    
    #shape, loc, scale = lognorm.fit(prices.Adjusted_close, floc=0) #x0 is rawdata x-axis floc is demeaning
    ret = om.getLogNormProb(prices, option, prb)
    x = np.linspace(bins.min(), bins.max(), num=500) # values for x-axis
    y = lognorm.pdf(x, ret['shape'], loc=0, scale=ret['scale']) # probability distribution
    stop2.plot(y, x, 'r--',alpha = 0.3,label='Fitted LogNorm')
    stop2.xaxis.set_major_formatter(plt.NullFormatter())
    
    #Pop x% of the strategy
    stop.hlines(y=ret['p50_M'], xmin=0, xmax=prb*ret['maxPO'], linestyle='dotted', color='r', zorder=2, label='Underlying '+ str(round(prb*100)) +'% PoP Window')
    stop.hlines(y=ret['p50_m'], xmin=0, xmax=prb*ret['maxPO'], linestyle='dotted', color='r', zorder=2)
     
    #Overlayed Stats
    stop.text(0.03, 0.105, 'POP(1 cent): ' + str('{:03.1f}'.format(ret['pop'])), horizontalalignment='left',
              verticalalignment='bottom', transform=stop.transAxes)
    stop.text(0.03, 0.08, 'POP(MaxPO): ' + str('{:03.1f}'.format(ret['popMax'])), horizontalalignment='left',
              verticalalignment='bottom', transform=stop.transAxes)
    stop.text(0.03, 0.055, 'POP('+ str(round(prb*100)) +'%): ' + str('{:03.1f}'.format(ret['pop50'])), horizontalalignment='left',
              verticalalignment='bottom', transform=stop.transAxes)
    try:
        stop.text(0.03, 0.03,'Hedge:  ' + str('{:05.3f}'.format(option.hedge.iloc[0])), horizontalalignment='left',
              verticalalignment='bottom', transform=stop.transAxes)
    except:
        pass
    stop.text(0.35, 0.105, 'Delta: ' + str('{:03.2f}'.format(option.delta.iloc[0])), horizontalalignment='left',
              verticalalignment='bottom', transform=stop.transAxes)
    stop.text(0.35, 0.08, 'Gamma: ' + str('{:05.3f}'.format(option.gamma.iloc[0])), horizontalalignment='left',
              verticalalignment='bottom', transform=stop.transAxes)
    stop.text(0.35, 0.055, 'Vega: ' + str('{:05.3f}'.format(option.vega.iloc[0])), horizontalalignment='left',
              verticalalignment='bottom', transform=stop.transAxes)
    stop.text(0.35, 0.03,'Theta:  ' + str('{:05.3f}'.format(option.theta.iloc[0])), horizontalalignment='left',
              verticalalignment='bottom', transform=stop.transAxes)
    stop.text(0.65, 0.105, 'Cost: ' + str('{:05.2f}'.format(option.setup.iloc[0])), horizontalalignment='left',
              verticalalignment='bottom', transform=stop.transAxes)
    stop.text(0.65, 0.08, 'Risk Reward: ' + str('{:03.1f}'.format(ret['RR'])), horizontalalignment='left',
              verticalalignment='bottom', transform=stop.transAxes)
    stop.text(0.65, 0.055, 'MaxProf: ' + str('{:05.2f}'.format(ret['maxprof'])), horizontalalignment='left',
              verticalalignment='bottom', transform=stop.transAxes)
    stop.text(0.65, 0.03,'MinProf:  ' + str('{:05.2f}'.format(ret['minprof'])), horizontalalignment='left',
              verticalalignment='bottom', transform=stop.transAxes)
  
    stop2.legend(loc=1,prop={'size': 8})
    stop.legend(loc=2,prop={'size': 8})
    
    plt.title(symbol + ' PayOff ' + do)
    
    customize_grid(top)
    customize_grid(stop)
    customize_grid(bottom)
    customize_grid(stop2)
    
    # adjust size and spacing
    plt.gcf().set_size_inches(20, 20)
    plt.subplots_adjust(hspace=0.75,wspace=0.1)

##############################################
# COT Futures Analysis Plots
##############################################     
def plotUnderlyingStudyCOT(symbol, dt_start, dt_end, prices, cot, weather=[], wasde=([],''),sdecomp={}, sde_p=(False, 0.65), stats=[],ci=0.10, c_win1=10,c_win2=52,lt_win=50, winma=25, bb_sd=2):
    import warnings
    from matplotlib.ticker import FormatStrFormatter
    import matplotlib.dates as mdates
    import matplotlib as mpl
    from mplfinance.original_flavor import candlestick_ohlc
    from matplotlib import gridspec
    from scipy.stats import norm
    from scipy.stats import lognorm
    import scipy.stats as scs
    import matplotlib.pyplot as plt
    import numpy as np
    
    prices=prices.sort_index()
    cot=cot.sort_index()
    
    def jarquebera(residuals, ci=0.05):
        from statsmodels.stats.stattools import jarque_bera

        jb_test_stat, pvalue, _, _ = jarque_bera(residuals)
        print(f"Jarque-Bera statistic: {jb_test_stat:.2f} with p-value: {pvalue:.2f}")

        if pvalue < ci:
            print("Cot net OI likely not normally distributed.")
        else:
            print("Cot net OI likely normally distributed.")
    
    plotsize=(9,6)
    if len(sdecomp) > 0:
        plotsize=(11,6)
        
    # The top plot consisting of daily candlesticks
    # Reset the index and convert date into float
    df = prices[['Open', 'High', 'Low', 'Close']]
    #df.columns=['Open', 'High', 'Low', 'Close']
    df.reset_index(inplace=True)
    df['Date'] = df['Date'].map(mdates.date2num)
    top = plt.subplot2grid(plotsize, (2, 0), rowspan=5, colspan=4)
    candlestick_ohlc(top, df.values, width=8, colorup='green', colordown='red', alpha=1)
    top.xaxis_date()
    top.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    plt.title('Weekly Candlesticks')
    
    # Bollinger Bands
    low, up = get_bollinger(prices.Close, winma, bb_sd)
    minb, maxb = low.mean(), up.mean()
    top.plot(prices.index, prices.Close.rolling(window=winma).mean(), linestyle=':', color='grey', linewidth=1,
             label='Price '+str(winma)+' MA')
    top.plot(prices.index, prices.Close.rolling(window=lt_win).mean(), linestyle='-', color='red', linewidth=1,
             label='Price '+str(lt_win)+' MA')
    x_axis = prices.index.get_level_values(0)
    top.fill_between(x_axis, low, up, color='silver', alpha=0.3,label=str(lt_win-winma)+'MA Bollinger Bands with ' + str(bb_sd) + ' SD')
    if len(sdecomp) > 0:
        prices=prices.join(sdecomp['STL_trend']).dropna()
        top.plot(prices.index, prices['trend'], linestyle='dashdot', color='brown', linewidth=1, label='Trend')
    top.legend(loc=2,prop={'size': 10})
    
    # Cot indicator
    top2=top.twinx()
    top2.plot(cot.index, cot.OI_Net, linestyle=':', color='green', linewidth=2, label='Net Open Interest')
    top2.get_yaxis().set_visible(False)
    
    # Weather overlay
    if len(weather) > 0:
        weather=weather[['Severe', 'Extreme', 'Terrible']]
        prices2=prices.join(weather).dropna()
        topw=top.twinx()
        #topw.spines["right"].set_position(("axes", -0.2))        
        topw.plot(prices2.index, prices2.Terrible, linestyle='dashdot', color='blue', linewidth=1, label='Terrible drought')
        topw.plot(prices2.index, prices2.Extreme, linestyle='--', color='blue', linewidth=1, label='Extreme drought')
        topw.plot(prices2.index, prices2.Severe, linestyle=':', color='blue', linewidth=1, label='Severe drought')
        x_axis = prices2.index.get_level_values(0)
        low=prices2.Severe
        up=prices2.Terrible
        topw.fill_between(x_axis, low, up, color='blue', alpha=0.2)
        topw.set_ylim(0, 100)
        topw.legend(loc=3,prop={'size': 10})
        topw.get_yaxis().set_visible(False)
        
    if len(wasde[0]) > 0:
        prices3=prices.join(wasde[0]).dropna()
        prices3['zero']=0
        topw2=top.twinx()
        topw2.plot(prices3.index, prices3[wasde[0].columns[0]], linestyle=':', color='red', linewidth=1, label=str(wasde[0].columns[0])+' '+str(wasde[1]))
        x_axis = prices3.index.get_level_values(0)
        low=prices3.zero
        up=prices3[wasde[0].columns[0]]
        topw2.fill_between(x_axis, low, up, color='red', alpha=0.1)
        topw2.set_ylim(0, float(prices3[wasde[0].columns[0]].max())*4)
        topw2.legend(loc=4,prop={'size': 10})
        topw2.get_yaxis().set_visible(False)
    
    # The mid bottom plot consisting of indicator
    if len(sdecomp) > 0:
        prices=prices.join(sdecomp['STL_residual']).interpolate()
        limit=sde_p[1]
        if sde_p[0]:
            prices['resid']=np.interp(prices['resid'], (prices['resid'].min(), prices['resid'].max()), (-100, +100))
        bottom3 = plt.subplot2grid(plotsize, (7, 0), rowspan=2, colspan=4, sharex=top)
        bottom3.plot(prices.index, prices.resid, color='black', linewidth=1, label='Oscillator') 
        bottom3.axhline(y=prices['resid'].min()*limit, xmin=0.0, xmax=1.0, linestyle='--', color='red', linewidth=1)
        bottom3.axhline(y=0, xmin=0.0, xmax=1.0, linestyle='--', color='black', linewidth=1)
        bottom3.axhline(y=prices['resid'].max()*limit, xmin=0.0, xmax=1.0, linestyle='--', color='green', linewidth=1)
        x_axis = prices['resid'].index.get_level_values(0)
        bottom3.fill_between(x_axis, int(prices['resid'].min()*limit), int(prices['resid'].max()*limit), color='lavender', label='OverBought/Sold', alpha=0.3)
        bottom3.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
        bottom3.legend(loc=2,prop={'size': 10})
        plt.title('Trend Oscillator')
        
    # The bottom plot consisting of trading volume
    
    if len(sdecomp) > 0:
        bottom = plt.subplot2grid(plotsize, (9, 0), rowspan=2, colspan=4, sharex=top)
    else:
        bottom = plt.subplot2grid(plotsize, (7, 0), rowspan=2, colspan=4, sharex=top)
    bottom.bar(prices.index, prices.Volume, color='orange')    
    bottom.plot(prices.index, prices.Volume.rolling(window=winma).mean(), linestyle='--', color='red', linewidth=3, label=str(winma)+' MA') 
    bottom.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    bottom.legend(loc=2,prop={'size': 10})
    plt.title('Trading Volume')
    
    # The bottom plot consisting of correlations
    bottom2 = plt.subplot2grid(plotsize, (0, 0), rowspan=2, colspan=4, sharex=top)
    corr=rolling(prices.Close, cot.OI_Net,win=c_win1)
    bottom2.plot(corr.index, corr.values, linestyle='--', color='red', linewidth=2,
                label=str(c_win1) +'W Correlation')
    corr=rolling(prices.Close, cot.OI_Net,win=c_win2)
    bottom2.plot(corr.index, corr.values, linestyle='-', color='green', linewidth=2,
                label=str(c_win2) +'W Correlation')
    bottom2.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    bottom2.legend(loc=1,prop={'size': 8})
    plt.title(str(c_win1) +'/'+ str(c_win2) +' Weeks Correlation')
    
    # The side upper plot consists of net open interest distribution
    stop = plt.subplot2grid(plotsize, (2, 4), rowspan=5, colspan=3, sharey=top2)
    cot_range = np.linspace(min(cot['OI_Net']), max(cot['OI_Net']), num=1000)
    μ = cot['OI_Net'].mean()
    σ = cot['OI_Net'].std()
    norm_pdf = scs.norm.pdf(cot_range, loc=μ, scale=σ)
    n, bins, patches = stop.hist(cot['OI_Net'], bins=50,density=1,orientation=u'horizontal',alpha = 0.4, label='Histogram of cot_range')
    stop.plot(norm_pdf,cot_range , 'r--',alpha = 0.4,label=f"N({μ:.4f}, {σ**2:.5f})",color='green')
    stop.yaxis.tick_right()
    stop.legend(loc=2,prop={'size': 8})
    stop.xaxis.set_major_formatter(plt.NullFormatter())
    plt.title('Net Open Interest')
    
    # Add cot indicator bands
    y90=norm.ppf(1-ci, loc=μ, scale=σ)
    y10=norm.ppf(ci, loc=μ, scale=σ)
    top2.axhline(y=y90, linestyle=':', color='green', linewidth=2, label='Net OI at '+ str(ci)+' conf.int.')
    top2.axhline(y=y10, linestyle=':', color='green', linewidth=2)
    top2.legend(loc=1,prop={'size': 10})
    
    customize_grid(top,4,4)
    customize_grid(top2,4,4)
    customize_grid(stop,4,4)
    customize_grid(bottom,4,4)
    customize_grid(bottom2,4,4)
    customize_grid(bottom3,4,4)
    
    wdesc=' with Drought Overlay (%)'
    title=symbol + ' Weekly Resampled from ' + str(dt_start)+' to '+str(dt_end)
    if len(weather)>0:
        title=title+wdesc
    plt.suptitle(title, y=0.93);
    
    # adjust size and spacing
    plt.gcf().set_size_inches(20, 20)
    plt.subplots_adjust(hspace=0.75,wspace=0.1)
    
    plt.show()
    
    jarquebera(cot.OI_Net, ci=0.05)
    if len(stats)>0:
        display(stats)
        print('Statistics computer over last 30 weeks.')
    
##############################################
# Stock Analysis Plots
##############################################     
def plotUnderlyingStudy(symbol, dt_start, dt_end, prices, lt_win=50, rsi_win=14, winma=25, alpha=2,sigma=0.0454,prices2=None,pr2lbl='',pstats=True,switch_axes=False):
    import matplotlib.dates as mdates
    from mplfinance.original_flavor import candlestick_ohlc
    #from mpl_finance import candlestick_ohlc
    import matplotlib as mpl
    import matplotlib.patches as mpatches
    import datetime
    from matplotlib.patches import Ellipse 
    import matplotlib.pyplot as plt
    
    # prepare data
    df = prices[['Open', 'High', 'Low', 'Close']]
    df['SMA20'] = prices['Adjusted_close'].rolling(20).mean()
    # Reset the index and convert date into float
    df.reset_index(inplace=True)
    df['Date'] = df['Date'].map(mdates.date2num)

    rsi = get_rsi(prices, rsi_win)

    # The Upper plot consisting of indicators
    upper = plt.subplot2grid((10, 5), (0, 0), rowspan=2, colspan=5)
    upper.plot(rsi.index, rsi.RSI, color='black', label='RSI('+str(rsi_win)+')')
    upper.axhline(y=40, xmin=0.0, xmax=1.0, linestyle='--', color='red', linewidth=1)
    upper.axhline(y=60, xmin=0.0, xmax=1.0, linestyle='--', color='green', linewidth=1)
    x_axis = rsi.index.get_level_values(0)
    upper.fill_between(x_axis, 25, 75, color='lavender', label='RSI OverBought/Sold', alpha=0.5)
    plt.title(symbol + ' RSI('+str(rsi_win)+')')

    # The top plot consisting of daily candlesticks and an optional second series
    top = plt.subplot2grid((10, 5), (2, 0), rowspan=6, colspan=5, sharex=upper)
    mma=candlestick_ohlc(top, df.values, width=2, colorup='g', colordown='r')
    
    top.xaxis_date()
    top.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    plt.title(symbol + ' Adj Close Price (' + dt_start + '-' + dt_end + ')')
    
    #Second series
    if prices2 is not None:
        top2=top.twinx()
        top2.plot(prices2.index, prices2.Adjusted_close,color='blue',label=pr2lbl)
        top2.set_ylabel(pr2lbl,color='blue',fontsize=14)
        customize_grid(top2)
        
    # Bollinger Bands
    low, up = get_bollinger(prices.Adjusted_close, winma, alpha)
    minb, maxb = low.mean(), up.mean()
    mma,=top.plot(prices.index, prices.Adjusted_close.rolling(window=winma).mean(), linestyle=':', color='grey', linewidth=1,
             label='Price '+str(winma)+' MA with 1 StDev Bubble of Last Price')
    top.plot(prices.index, prices.Adjusted_close.rolling(window=lt_win).mean(), linestyle='-', color='red', linewidth=1,
             label='Price '+str(lt_win)+' MA')
    x_axis = prices.index.get_level_values(0)
    top.fill_between(x_axis, low, up, alpha=0.3, color='silver', label=str(lt_win-winma)+'MA Bollinger Bands')
    # linestyle=linestyle, color=color, linewidth=3 ['-', '--', '-.', ':']
    top.axhline(y=minb, xmin=0.0, xmax=1.0, linestyle='--', color='r', linewidth=1, label='Average Min/Max Bollinger Bands')
    top.axhline(y=maxb, xmin=0.0, xmax=1.0, linestyle='--', color='r', linewidth=1)
    top.legend(loc=2)

    #Variance Bubble
    #1800x1440
    if switch_axes:
        top.yaxis.set_label_position("right")
        top.yaxis.tick_right()
        top2.yaxis.set_label_position("left")
        top2.yaxis.tick_left()
        
    if sigma == 0.0454:
        sigma=prices.Adjusted_close.std(ddof=1)/prices.Adjusted_close[-1] 
        #print(sigma)
    y_data=prices.Adjusted_close[-1]
    x_data=prices.index[-1] + datetime.timedelta(days=20)
    delta=int(y_data*(1+1*sigma)-y_data*(1-1*sigma))
    ells = Ellipse(xy = (x_data, y_data), 
                width = 40, 
                height = delta,alpha=0.2, clip_on=True) 
    top.add_artist(ells)   
    
    # The bottom plot consisting of daily trading volume
    bottom = plt.subplot2grid((10, 5), (8, 0), rowspan=2, colspan=5, sharex=upper)
    bottom.bar(prices.index, prices.Volume, color='orange')
    bottom.plot(prices.index, prices.Volume.rolling(window=winma).mean(), linestyle='--', color='red', linewidth=3,
                label=str(winma)+' MA')
    bottom.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    bottom.legend(loc=2)
    plt.title(symbol + ' Daily Trading Volume')

    # Stats
    if pstats:
        print('Underlying Price Measures')
        #print('----------------------------------------')
        #print('Mean Price: ', round(prices.Adjusted_close.mean(), 4), round(prices.Adjusted_close.median(), 4))
        #print('Daily Avg BB Range: ', round(maxb.mean() - minb.mean(), 4))
        #print('Monthly BB MA Mean: ', round(prices.Adjusted_close.rolling(window=winma).mean().mean(), 4))
        #print('Monthly ' + str(lt_win) + ' MA Mean: ', round(prices.Adjusted_close.rolling(window=lt_win).mean().mean(), 4))
        #print('Last Price: ', round(prices.tail(1).Adjusted_close.iloc[0], 4))
        #print('')
        
        pd.options.display.float_format = '{:,.4f}'.format
        idx=['Mean Price','Median Price','Daily Avg BB Range','Monthly BB MA Mean','Monthly ' + str(lt_win) + ' MA Mean','Last Price']
        ar=[prices.Adjusted_close.mean(), 
            prices.Adjusted_close.median(),
            round(maxb.mean() - minb.mean(), 4),
            round(prices.Adjusted_close.rolling(window=winma).mean().mean(), 4),
            round(prices.Adjusted_close.rolling(window=lt_win).mean().mean(),4),
            round(prices.tail(1).Adjusted_close.iloc[0], 4)]
        df=pd.DataFrame(ar,columns=['Values'],index=idx)
        print(df)

    customize_grid(upper)
    customize_grid(top)
    customize_grid(bottom)
    
    # adjust size and spacing
    plt.gcf().set_size_inches(25, 20)
    plt.subplots_adjust(hspace=0.75)
    
def plotUnderlyingStudyTL(symbol, dt_start, dt_end, prices, lt_win=50, rsi_win=14, winma=25, alpha=2):
    
    from matplotlib.widgets import RectangleSelector
    import matplotlib.dates as mdates
    import matplotlib.pyplot as plt
    from mplfinance.original_flavor import candlestick_ohlc
    #from mpl_finance import candlestick_ohlc
    import matplotlib as mpl
    
    def line_select_callback(eclick, erelease):
        import matplotlib.lines as mlines

        x1, y1 = eclick.xdata, eclick.ydata
        x2, y2 = erelease.xdata, erelease.ydata
        a, b = [x1,x2], [y1,y2]
        l = mlines.Line2D(a, b)
        top.add_line(l)

    def toggle_selector(event):
        
        if event.key in ['Q', 'q'] and toggle_selector.RS.active:
            toggle_selector.RS.set_active(False)
        if event.key in ['A', 'a'] and not toggle_selector.RS.active:
            toggle_selector.RS.set_active(True)


    # prepare data
    df = prices[['Open', 'High', 'Low', 'Close']]
    df['SMA20'] = prices['Adjusted_close'].rolling(20).mean()
    # Reset the index and convert date into float
    df.reset_index(inplace=True)
    df['Date'] = df['Date'].map(mdates.date2num)

    rsi = get_rsi(prices, rsi_win)

    # The Upper plot consisting of indicators
    upper = plt.subplot2grid((10, 5), (0, 0), rowspan=2, colspan=5)
    upper.plot(rsi.index, rsi.RSI, color='black', label='RSI('+str(rsi_win)+')')
    upper.axhline(y=40, xmin=0.0, xmax=1.0, linestyle='--', color='red', linewidth=1)
    upper.axhline(y=60, xmin=0.0, xmax=1.0, linestyle='--', color='green', linewidth=1)
    x_axis = rsi.index.get_level_values(0)
    upper.fill_between(x_axis, 25, 75, color='lavender', label='RSI OverBought/Sold', alpha=0.5)
    # upper.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    # top.plot(prices.index, prices.Adjusted_close, color='black',label='Adjusted_close')
    plt.title(symbol + ' RSI('+str(rsi_win)+')')

    # The top plot consisting of daily candlesticks
    top = plt.subplot2grid((10, 5), (2, 0), rowspan=6, colspan=5, sharex=upper)
    candlestick_ohlc(top, df.values, width=2, colorup='g', colordown='r')
    top.xaxis_date()
    top.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    # top.plot(prices.index, prices.Adjusted_close, color='black',label='Adjusted_close')
    plt.title(symbol + ' Adjusted_close Price (' + dt_start + '-' + dt_end + ')')

    # Bollinger Bands
    low, up = get_bollinger(prices.Adjusted_close, winma, alpha)
    minb, maxb = low.mean(), up.mean()
    top.plot(prices.index, prices.Adjusted_close.rolling(window=winma).mean(), linestyle=':', color='grey', linewidth=1,
             label='Price '+str(winma)+' MA')
    top.plot(prices.index, prices.Adjusted_close.rolling(window=lt_win).mean(), linestyle='-', color='red', linewidth=1,
             label='Price '+str(lt_win)+' MA')
    x_axis = prices.index.get_level_values(0)
    top.fill_between(x_axis, low, up, alpha=0.3, color='silver', label=str(lt_win-winma)+'MA Bollinger Bands')
    # linestyle=linestyle, color=color, linewidth=3 ['-', '--', '-.', ':']
    top.axhline(y=minb, xmin=0.0, xmax=1.0, linestyle='--', color='r', linewidth=1, label='MinMax BB')
    top.axhline(y=maxb, xmin=0.0, xmax=1.0, linestyle='--', color='r', linewidth=1)
    top.legend(loc=2)

    # The bottom plot consisting of daily trading volume
    bottom = plt.subplot2grid((10, 5), (8, 0), rowspan=2, colspan=5, sharex=upper)
    bottom.bar(prices.index, prices.Volume, color='orange')
    bottom.plot(prices.index, prices.Volume.rolling(window=winma).mean(), linestyle='--', color='red', linewidth=3,
                label=str(winma)+' MA')
    bottom.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    bottom.legend(loc=2)
    plt.title(symbol + ' Daily Trading Volume')

    # Stats
    print('Underlying Price Measures')
    #print('----------------------------------------')
    #print('Mean Price: ', round(prices.Adjusted_close.mean(), 4), round(prices.Adjusted_close.median(), 4))
    #print('Daily Avg BB Range: ', round(maxb.mean() - minb.mean(), 4))
    #print('Monthly BB MA Mean: ', round(prices.Adjusted_close.rolling(window=winma).mean().mean(), 4))
    #print('Monthly ' + str(lt_win) + ' MA Mean: ', round(prices.Adjusted_close.rolling(window=lt_win).mean().mean(), 4))
    #print('Last Price: ', round(prices.tail(1).Adjusted_close.iloc[0], 4))
    #print('')
    
    pd.options.display.float_format = '{:,.4f}'.format
    idx=['Mean Price','Median Price','Daily Avg BB Range','Monthly BB MA Mean','Monthly ' + str(lt_win) + ' MA Mean','Last Price']
    ar=[prices.Adjusted_close.mean(), 
        prices.Adjusted_close.median(),
        round(maxb.mean() - minb.mean(), 4),
        round(prices.Adjusted_close.rolling(window=winma).mean().mean(), 4),
        round(prices.Adjusted_close.rolling(window=lt_win).mean().mean(),4),
        round(prices.tail(1).Adjusted_close.iloc[0], 4)]
    df=pd.DataFrame(ar,columns=['Values'],index=idx)
    print(df)
    
    customize_grid(upper)
    customize_grid(top)
    customize_grid(bottom)
    
    # adjust size and spacing
    plt.gcf().set_size_inches(10, 10)
    plt.subplots_adjust(hspace=2)
    
    a,b=0,0
    toggle_selector.RS = RectangleSelector(top, line_select_callback,
                                       drawtype='line', useblit=True,
                                       button=[1, 3],  # don't use middle button
                                       minspanx=5, minspany=5,
                                       spancoords='pixels',
                                       interactive=False)
    plt.connect('key_press_event', toggle_selector)
    toggle_selector.RS.to_draw.set_visible(True)

##############################################
# Plots the ratio of two series
##############################################     
def plotRatioStudy(symbol, dt_start, dt_end, d1,d2,c_win=60, lt_win=50, rsi_win=14, winma=25, alpha=2,tit2='',plsrs=True):
    import matplotlib.pyplot as plt
    from matplotlib.ticker import FormatStrFormatter
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()
    
    # prepare data
    prices = (d1 / d2).interpolate()
    rsi = get_rsi(prices, rsi_win)

    # The Upper plot consisting of indicators
    upper = plt.subplot2grid((10, 5), (0, 0), rowspan=2, colspan=5)
    upper.plot(rsi.index, rsi.RSI, color='black', label='RSI('+str(rsi_win) +')')
    upper.axhline(y=40, xmin=0.0, xmax=1.0, linestyle='--', color='red', linewidth=1)
    upper.axhline(y=60, xmin=0.0, xmax=1.0, linestyle='--', color='green', linewidth=1)
    x_axis = rsi.index.get_level_values(0)
    upper.fill_between(x_axis, 25, 75, color='lavender', label='RSI OverBought/Sold', alpha=0.5)
    plt.title('Ratio of ' + symbol + ' to ' + tit2 + ' - RSI('+str(rsi_win)+')')
        
    # The top plot consisting of the ratio
    top = plt.subplot2grid((10, 5), (2, 0), rowspan=6, colspan=5, sharex=upper)
    top.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    top.plot(prices.index, prices.Adjusted_close, linewidth=3,color='black',label='Ratio')
    plt.title('Ratio of ' + symbol + ' to ' + tit2 + ' - Adjusted_close Price (' + dt_start + '-' + dt_end + ')')
    top.set_ylabel('Ratio',color='black',fontsize=14)
    #Plot series
    if plsrs:
        top2=top.twinx()
        top2.plot(d1.index, d1.Adjusted_close,color='blue',linestyle='dashdot',linewidth=2,label=symbol)
        top2.plot(d2.index, d2.Adjusted_close,color='orange',linestyle='dashed',linewidth=2,label=tit2)
        top2.set_ylabel(symbol,color='blue',fontsize=14)
        top2.legend(loc=1)
        customize_grid(top2)
    
    # Bollinger Bands
    low, up = get_bollinger(prices.Adjusted_close, winma, alpha)
    minb, maxb = low.mean(), up.mean()
    top.plot(prices.index, prices.Adjusted_close.rolling(window=winma).mean(), linestyle=':', color='grey', linewidth=1,
             label='Price '+str(winma) +' MA')
    top.plot(prices.index, prices.Adjusted_close.rolling(window=lt_win).mean(), linestyle='-', color='red', linewidth=1,
             label='Price '+str(lt_win) +' MA')
    x_axis = prices.index.get_level_values(0)
    top.fill_between(x_axis, low, up, alpha=0.3, color='silver', label=str(lt_win-winma) +'MA Bollinger Bands')
    top.axhline(y=minb, xmin=0.0, xmax=1.0, linestyle='--', color='r', linewidth=1, label='MinMax BB')
    top.axhline(y=maxb, xmin=0.0, xmax=1.0, linestyle='--', color='r', linewidth=1)
    top.legend(loc=2)

    # The bottom plot consisting of correlation
    bottom = plt.subplot2grid((10, 5), (8, 0), rowspan=2, colspan=5, sharex=upper)
    corr=rolling(d1.iloc[:, 0],d2.iloc[:, 1],win=c_win)
    #corr=d1.iloc[:, 0].rolling(window=c_win).corr(d2.iloc[:, 1],method="spearman")
    bottom.plot(corr.index, corr.values, linestyle='--', color='red', linewidth=3,
                label=str(c_win) +'D Correlation')
    #bottom.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    bottom.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    bottom.legend(loc=2)
    plt.title('Ratio of ' + symbol + ' to ' + tit2 + ' - ' + str(c_win) + 'D Correlation')
    
    customize_grid(upper)
    customize_grid(top)
    customize_grid(bottom)
    
    # adjust size and spacing
    plt.gcf().set_size_inches(25, 20)
    plt.subplots_adjust(hspace=0.75)

##############################################
# Plots the ratios of three series
##############################################     
def plotRatioStudy3(tit,ref, dt_start, dt_end, d1,d2,d3,d4,c_win=60, lt_win=50, rsi_win=14, winma=25, alpha=2):
    import matplotlib.pyplot as plt
    from matplotlib.ticker import FormatStrFormatter
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()
    
    lt=linestyle_tuple
    
    # prepare data
    prices1 = (d1 / d4).interpolate()
    prices2 = (d2 / d4).interpolate()
    prices3 = (d3 / d4).interpolate()
    prices4 = d4.interpolate()
    stit=tit.split(' - ')
    
    rsi = get_rsi(prices4, rsi_win)

    # The Upper plot consisting of indicators
    upper = plt.subplot2grid((10, 5), (0, 0), rowspan=2, colspan=5)
    upper.plot(rsi.index, rsi.RSI, color='black', label='RSI('+str(rsi_win) +')')
    upper.axhline(y=40, xmin=0.0, xmax=1.0, linestyle='--', color='red', linewidth=1)
    upper.axhline(y=60, xmin=0.0, xmax=1.0, linestyle='--', color='green', linewidth=1)
    x_axis = rsi.index.get_level_values(0)
    upper.fill_between(x_axis, 25, 75, color='lavender', label='RSI OverBought/Sold', alpha=0.5)
    plt.title(ref + ' - RSI('+str(rsi_win)+')')
        
    # The top plot consisting of the ratio
    top = plt.subplot2grid((10, 5), (2, 0), rowspan=6, colspan=5, sharex=upper)
    top.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    top.plot(prices4.index, prices4.Close, linewidth=3,color='black',label=ref)
    plt.title('Ratios of ' + tit + ' to ' + ref + ' - Close Price (' + dt_start + '-' + dt_end + ')')
    top.set_ylabel('Ratios',color='black',fontsize=14)
    #Plot series
    top2=top.twinx()
    top2.plot(prices1.index, prices1.Close,color='blue',linestyle=lt[3][1],linewidth=2,label=stit[0]  + ' to ' + ref)
    top2.plot(prices2.index, prices2.Close,color='orange',linestyle=lt[6][1],linewidth=2,label=stit[1]  + ' to ' + ref)
    top2.plot(prices3.index, prices3.Close,color='brown',linestyle=lt[1][1],linewidth=2,label=stit[2]  + ' to ' + ref)
    top2.set_ylabel(tit,color='blue',fontsize=14)
    top2.legend(loc=1)
    top.legend(loc=2)

    customize_grid(upper)
    customize_grid(top)
    customize_grid(top2)
    
    # adjust size and spacing
    plt.gcf().set_size_inches(25, 20)
    plt.subplots_adjust(hspace=0.75)      

##############################################
# Plots a ratios and a series
##############################################     
def plotRatioStudy1(symbol, dt_start, dt_end, d1,d2,d3,c_win=60, lt_win=50, rsi_win=14, winma=25, alpha=2,tit2='',ds1='',ds2=''):
    import matplotlib.pyplot as plt
    from matplotlib.ticker import FormatStrFormatter
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()
    
    # prepare data
    prices = (d1 / d2).interpolate()
    rsi = get_rsi(prices, rsi_win)
    stit=tit2.split(' - ')
    
    # The Upper plot consisting of indicators
    upper = plt.subplot2grid((10, 5), (0, 0), rowspan=2, colspan=5)
    upper.plot(rsi.index, rsi.RSI, color='black', label='RSI('+str(rsi_win) +')')
    upper.axhline(y=40, xmin=0.0, xmax=1.0, linestyle='--', color='red', linewidth=1)
    upper.axhline(y=60, xmin=0.0, xmax=1.0, linestyle='--', color='green', linewidth=1)
    x_axis = rsi.index.get_level_values(0)
    upper.fill_between(x_axis, 25, 75, color='lavender', label='RSI OverBought/Sold', alpha=0.5)
    plt.title('Ratio of ' + ds1 + ' ('+stit[0] + ') to ' + ds2 + '('+stit[1] + ') - RSI('+str(rsi_win)+')')
        
    # The top plot consisting of the ratio
    top = plt.subplot2grid((10, 5), (2, 0), rowspan=6, colspan=5, sharex=upper)
    #top.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.00f}'))
    top.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    top.plot(prices.index, prices.Adjusted_close, linewidth=3,color='black',label='Ratio')
    plt.title('Ratio of ' + ds1 + ' ('+stit[0] + ') to ' + ds2 + '('+stit[1] + ') - Adjusted_close Price (' + dt_start + '-' + dt_end + ')')
    top.set_ylabel('Ratio',color='black',fontsize=14)
    #Plot series
    top2=top.twinx()
    top2.plot(d3.index, d3.Adjusted_close,color='blue',linestyle='dashdot',linewidth=2,label=symbol)
    top2.set_ylabel(symbol,color='blue',fontsize=14)
    top2.legend(loc=1)
    
    # Bollinger Bands
    low, up = get_bollinger(prices.Adjusted_close, winma, alpha)
    minb, maxb = low.mean(), up.mean()
    top.plot(prices.index, prices.Adjusted_close.rolling(window=winma).mean(), linestyle=':', color='grey', linewidth=1,
             label='Price '+str(winma) +' MA')
    top.plot(prices.index, prices.Adjusted_close.rolling(window=lt_win).mean(), linestyle='-', color='red', linewidth=1,
             label='Price '+str(lt_win) +' MA')
    x_axis = prices.index.get_level_values(0)
    top.fill_between(x_axis, low, up, alpha=0.3, color='silver', label=str(lt_win-winma) +'MA Bollinger Bands')
    top.axhline(y=minb, xmin=0.0, xmax=1.0, linestyle='--', color='r', linewidth=1, label='MinMax BB')
    top.axhline(y=maxb, xmin=0.0, xmax=1.0, linestyle='--', color='r', linewidth=1)
    top.legend(loc=2)

    # The bottom plot consisting of correlation
    bottom = plt.subplot2grid((10, 5), (8, 0), rowspan=2, colspan=5, sharex=upper)
    corr=rolling(d1.iloc[:, 0],d2.iloc[:, 1],win=c_win)
    #corr=d1.iloc[:, 0].rolling(window=c_win).corr(d2.iloc[:, 1],method="spearman")
    bottom.plot(corr.index, corr.values, linestyle='--', color='red', linewidth=3,
                label=str(c_win) +'D Correlation')
    #bottom.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    bottom.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    bottom.legend(loc=2)
    plt.title('Ratio of ' + symbol + ' to ' + tit2 + ' - ' + str(c_win) + 'D Correlation')
    
    customize_grid(upper)
    customize_grid(top)
    customize_grid(bottom)
    customize_grid(top2)
    
    # adjust size and spacing
    plt.gcf().set_size_inches(25, 20)
    plt.subplots_adjust(hspace=0.75)
  
##############################################
# Plots three series
##############################################     
def plotRatioStudy2(tit,ref, dt_start, dt_end, d1,d2,d3,tit2,tit3,scal=1,c_win=60, lt_win=50, rsi_win=14, winma=25, alpha=2):
    import matplotlib.pyplot as plt
    from matplotlib.ticker import FormatStrFormatter
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()
    
    lt=linestyle_tuple
    
    # prepare data
    prices1 = (d1 / d3).interpolate()
    prices2 = (d2 / d3).interpolate()
    prices1a = (scal*d1).interpolate()
    prices2a = (scal*d2).interpolate()
    prices3 = d3.interpolate()
    stit=tit.split(' - ')
    
    rsi = get_rsi(prices3, rsi_win)

    # The Upper plot consisting of indicators
    upper = plt.subplot2grid((10, 5), (0, 0), rowspan=2, colspan=5)
    upper.plot(rsi.index, rsi.RSI, color='black', label='RSI('+str(rsi_win) +')')
    upper.axhline(y=40, xmin=0.0, xmax=1.0, linestyle='--', color='red', linewidth=1)
    upper.axhline(y=60, xmin=0.0, xmax=1.0, linestyle='--', color='green', linewidth=1)
    x_axis = rsi.index.get_level_values(0)
    upper.fill_between(x_axis, 25, 75, color='lavender', label='RSI OverBought/Sold', alpha=0.5)
    plt.title(ref + ' - RSI('+str(rsi_win)+')')
        
    # The top plot consisting of the ratio
    top = plt.subplot2grid((10, 5), (2, 0), rowspan=6, colspan=5, sharex=upper)
    top.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    top.plot(prices3.index, prices3.Close, linewidth=3,color='black',label=ref)
    top.plot(prices1a.index, prices1a.Close, linewidth=3,color='grey',label=stit[0] + ' scaled: ' + str(scal) + 'x')
    top.plot(prices2a.index, prices2a.Close, linewidth=3,color='brown',label=stit[1]+ ' scaled: ' + str(scal) + 'x')
    
    plt.title('Ratios of ' + tit2 + '('+stit[0]+')' + ' and ' +tit2 + '('+stit[0]+')'+' to ' + ref + ' - Close Price (' + dt_start + '-' + dt_end + ')')
    top.set_ylabel('Ratios',color='black',fontsize=14)
    #Plot series
    top2=top.twinx()
    top2.plot(prices1.index, prices1.Close,color='blue',linestyle=lt[3][1],linewidth=2,label=stit[0]  + ' to ' + ref)
    top2.plot(prices2.index, prices2.Close,color='orange',linestyle=lt[6][1],linewidth=2,label=stit[1]  + ' to ' + ref)
    
    top2.set_ylabel(tit,color='blue',fontsize=14)
    top2.legend(loc=1)
    top.legend(loc=2)
    
    customize_grid(upper)
    customize_grid(top)
    customize_grid(top2)

    # adjust size and spacing
    plt.gcf().set_size_inches(25, 20)
    plt.subplots_adjust(hspace=0.75)      
      
##############################################
# Heatmap helper functions
##############################################          
def setHeatmapIndexes(df,maxcols,nitems):
    i=0
    hcol=1
    hrow=1
    while (i < nitems):
        if hcol <= maxcols: 
            df.loc[i, 'Yrows'] = hrow
            df.loc[i, 'Xcols'] = hcol
            hcol=hcol+1
        else:
            hcol=1
            hrow=hrow+1
            df.loc[i, 'Yrows'] = hrow
            df.loc[i, 'Xcols'] = hcol
            hcol=hcol+1
        i=i+1
    return df

def plotSectorHeatMap(df,heatmap_rows,heatmap_cols,t = "Sector Heat Map",fs=(18,9)):
    import matplotlib.pyplot as plt
    import numpy as np; np.random.seed(0)
    import seaborn as sns; sns.set()

    # Create an array of stock symbols & their respective percentage price change
    symbol = ((np.asarray(df['Symbol'])).reshape(heatmap_rows,heatmap_cols))
    perchange = ((np.asarray(df['Change'])).reshape(heatmap_rows,heatmap_cols))
    # Create a pivot table
    result = df.pivot(index='Yrows',columns='Xcols',values='Change')
    #print(result)

    # Create an array to annotate the heatmap
    labels = (np.asarray(["{0} \n {1:.2f}".format(symb,value)
                          for symb, value in zip(symbol.flatten(),
                                                   perchange.flatten())])
             ).reshape(heatmap_rows,heatmap_cols)

    # Define the plot
    fig, ax = plt.subplots(figsize=fs)

    # Add title to the Heat map
    title = t

    # Set the font size and the distance of the title from the plot
    plt.title(title,fontsize=18)
    ttl = ax.title
    ttl.set_position([0.5,1.05])

    # Hide ticks for X & Y axis
    ax.set_xticks([]) 
    ax.set_yticks([]) 

    # Remove the axes
    ax.axis('off')
    
    # Use the heatmap function from the seaborn package
    sns.heatmap(result,annot=labels,fmt="",cmap='RdYlGn',linewidths=0.30,ax=ax)

    # Display the Sector Heatmap
    plt.show()
    return result

# Sector,Indices,Comparisons: absolute return over period, symbols in rows
def plotAbsRetHeatMap(df, heatmap_rows=5, heatmap_cols=6, title="Absolute Return Heat Map (%)",fs=(18,9)):
    if len(df) == heatmap_cols*heatmap_rows:
        df=setHeatmapIndexes(df,heatmap_cols,heatmap_cols*heatmap_rows)
        data=plotSectorHeatMap(df,heatmap_rows,heatmap_cols,t=title,fs=fs)
    else:
        print('ERROR: the heat map size must match the dataframe size: df_rows = heatmap_cols*heatmap_rows')

# helpers for pivot by month and week
def prepareMonthlyData(df, lbl='Close', demeaned=False):
    import pandas as pd
    import numpy as np
    import calendar

    from pandas.tseries.offsets import CustomBusinessMonthBegin
    from pandas.tseries.holiday import USFederalHolidayCalendar
    bmth_us = CustomBusinessMonthBegin(calendar=USFederalHolidayCalendar())

    import numpy as np; np.random.seed(0)
    
    df=df.reset_index()
    df.rename(columns={'Date': 'date','Open': 'open','High': 'high','Low': 'low','Close': 'close',
                      'Adjusted_close': 'adjusted_close','Volume': 'volume'
                      }, inplace=True)
    df.rename(columns={'index': 'date','Open': 'open','High': 'high','Low': 'low','Close': 'close',
                      'Adjusted_close': 'adjusted_close','Volume': 'volume'
                      }, inplace=True)
    lbl=lbl.lower()
    
    asset_prices=df[['date',lbl]]
    asset_prices['date'] = pd.to_datetime(asset_prices.date)
    asset_prices.set_index('date',inplace=True)
    asset_returns = pd.DataFrame(data=np.zeros(shape=(len(asset_prices.index), asset_prices.shape[1])), 
                                 columns=asset_prices.columns.values,
                                 index=asset_prices.index)

    normed_returns = asset_returns
    asset_returns=asset_prices.pct_change()[1:]
    asset_returns=asset_returns.replace([np.inf, -np.inf], np.nan).interpolate()
    if demeaned:
        normed_returns = (asset_returns - asset_returns.mean()) / asset_returns.std()
        normed_returns=normed_returns.replace([np.inf, -np.inf], np.nan).interpolate()
    else:
        normed_returns = asset_returns*100

    # Compute monthly average
    normed_returns=normed_returns.resample(bmth_us).mean()
    normed_returns.reset_index(inplace=True)
    normed_returns['month2'] = normed_returns['date'].dt.month
    normed_returns['year'] = normed_returns['date'].dt.year

    normed_returns['month'] = normed_returns['month2'].apply(lambda x: calendar.month_name[x])
    normed_returns.reset_index(inplace=True)
    normed_returns=normed_returns[['year','month',lbl]]
    normed_returns.rename(columns={lbl: 'values'}, inplace=True)
    return normed_returns

def prepareWeeklyData(df, lbl='Close', demeaned=False):
    import pandas as pd
    import numpy as np
    import calendar
    import numpy as np; np.random.seed(0)
    
    lbl=lbl.lower()
    df.columns=[str(c).lower() for c in df.columns]
    df=df[[lbl]]
    
    df['index']=df.index
    df['day'] = df['index'].dt.day_name()
    df['month'] = df['index'].dt.month
    df['month'] = df['month'].apply(lambda x: calendar.month_name[x])
    df['returns']=df[lbl].pct_change()[1:]
    df['returns']=df['returns'].replace([np.inf, -np.inf], np.nan).interpolate()
    if demeaned:
        df['returns'] = (df['returns'] - df['returns'].mean()) / df['returns'].std()
        df['returns'] = df['returns'].replace([np.inf, -np.inf], np.nan).interpolate()
    else:
        df['returns'] = df['returns']*100     
    df=df.groupby(['month','day'])['returns'].agg(['mean']).rename({'mean':'values'},axis=1)
    df=df.reset_index()
    return df

def prepareHourlyData(df, lbl='Close', demeaned=False):
    import pandas as pd
    import numpy as np
    import calendar
    import numpy as np; np.random.seed(0)
    
    lbl=lbl.lower()
    df.columns=[str(c).lower() for c in df.columns]
    df=df[[lbl]]
    
    df['index']=df.index
    df['day'] = df['index'].dt.day_name()
    df['hour'] = df['index'].dt.hour
    df['returns']=df[lbl].pct_change()[1:]
    df['returns']=df['returns'].replace([np.inf, -np.inf], np.nan).interpolate()
    if demeaned:
        df['returns'] = (df['returns'] - df['returns'].mean()) / df['returns'].std()
        df['returns'] = df['returns'].replace([np.inf, -np.inf], np.nan).interpolate()
    else:
        df['returns'] = df['returns']*100     
    df=df.groupby(['day','hour'])['returns'].agg(['mean']).rename({'mean':'values'},axis=1)
    df=df.reset_index()
    return df

def plotHeatMapCumRets(df, t='Yearly Cumulative Returns Heatmap (%)',fs=(18,9)):
    import matplotlib.pyplot as plt
    import seaborn as sns; sns.set()
    import numpy as np
    
    def cumProdRt(s,log_rets=False, as_pct=True):
        if log_rets:
            cumprod_ret = np.exp(s.cumsum())-1
        else:
            cumprod_ret = (1+s).cumprod()-1
        if as_pct:
            cumprod_ret = cumprod_ret*100
        return cumprod_ret.round(2)

    cols=df.columns
    df=df.pct_change().dropna()
    df=df.apply(cumProdRt)
    df['Year']=df.index.year
    df=df.groupby(['Year']).agg(['last'])
    df.columns=cols
    
    fig, ax = plt.subplots(figsize=fs)
    title = t
    plt.title(title,fontsize=18)
    ttl = ax.title
    ttl.set_position([0.5,1.05])
    sns.heatmap(df, linewidths=.5,cmap="RdYlGn",annot=True,ax=ax)
    return df

def plotMonthlyHeatmap(df,lbl='Close',demeaned=False, fs=(12,6)):
    returns=prepareMonthlyData(df, lbl, demeaned)
    hmaprets=plotHeatMapReturns(returns,idx='month',lbl='year', fs=fs)

def plotWeeklyHeatmap(df,lbl='Close',demeaned=False,fs=(12,6)):
    returns=prepareWeeklyData(df,lbl=lbl,demeaned=demeaned)
    hmaprets=plotHeatMapReturns(returns,idx='month',lbl='day', t='Weekdays Average Returns Heat Map (%)', fs=fs)

def plotHourlyHeatmap(df,lbl='Close',demeaned=False,fs=(12,6)):
    returns=prepareHourlyData(df)
    hmaprets=plotHeatMapReturns(returns,idx='day', lbl='hour',t='Hourly Average Returns Heat Map (%)',fs=fs)
    
##############################################
# Heatmap plot
##############################################                  
def plotHeatMapReturns(rets,idx='month', lbl='year',t='Monthly Average Returns Heat Map (%)',fs=(18,9)):
    import matplotlib.pyplot as plt
    import numpy as np; np.random.seed(0)
    import seaborn as sns; sns.set()
    
    rets = rets.pivot_table(index=idx,columns=lbl,values='values', aggfunc=lambda x:x)
    month_order = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    day_order = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
    if idx=='month': 
        rets=rets.reindex(month_order, axis=0)
    elif idx=='day':
        rets=rets.reindex(day_order, axis=0)
    if lbl=='day':   rets=rets.T.reindex(day_order, axis=0).dropna().T

    # Define the plot
    fig, ax = plt.subplots(figsize=fs)
    title = t
    plt.title(title,fontsize=18)
    ttl = ax.title
    ttl.set_position([0.5,1.05])
    sns.heatmap(rets, linewidths=.5,cmap="RdYlGn",annot=True,ax=ax) #center=rets.loc[ref_month, ref_year],
    plt.show()
    return rets

##############################################
# Correlation Heatmap
##############################################                  
def plotCorrelationTable(df, periods=0, title='Correlation Table (Spearman Non-param)', fs=(18,9)):
    import matplotlib.pyplot as plt
    import numpy as np; np.random.seed(0)
    import seaborn as sns; sns.set()
    
    if periods != 0:
        title=title + ' over last ' + str(periods) +' periods'
        df=df.iloc[-periods:,:]
        
    corr = df.corr(method='spearman')

    fig, ax = plt.subplots(figsize=fs)
    plt.title(title,fontsize=18)
    ttl = ax.title
    ttl.set_position([0.5,1.05])
    sns.heatmap(corr, xticklabels=corr.columns.values, 
               yticklabels=corr.columns.values,
                linewidths=.5,cmap="RdYlGn",annot=True,ax=ax)
    plt.show()
    return corr


##############################################
# Interest Rates Heatmap
##############################################                  
def plotHeatmapYC(df, title='Yield Curve Heatmap', fs=(16,0)):
    import matplotlib.pyplot as plt
    import numpy as np; np.random.seed(0)
    import seaborn as sns; sns.set()

    title=title + ' (last '+str(len(df))+' periods)'
    df.index=df.index.date

    fig, ax = plt.subplots(figsize=fs)
    plt.title(title,fontsize=18)
    ttl = ax.title
    ttl.set_position([0.5,1.05])
    sns.heatmap(df, xticklabels=df.columns, 
               yticklabels=df.index.values,
                linewidths=.5,cmap="RdYlGn",annot=True,ax=ax)
    plt.show()

##############################################
# Scatter plot with distributions
##############################################          
def scatterDist(x,y):
    import numpy as np
    import matplotlib.pyplot as plt
    import pandas as pd

    # definitions for the axes
    left, width = 0.1, 0.65
    bottom, height = 0.1, 0.65
    spacing = 0.005


    rect_scatter = [left, bottom, width, height]
    rect_histx = [left, bottom + height + spacing, width, 0.2]
    rect_histy = [left + width + spacing, bottom, 0.2, height]

    # start with a rectangular Figure
    plt.figure(figsize=(8, 8))

    ax_scatter = plt.axes(rect_scatter)
    ax_scatter.tick_params(direction='in', top=True, right=True)
    ax_histx = plt.axes(rect_histx)
    ax_histx.tick_params(direction='in', labelbottom=False)
    ax_histy = plt.axes(rect_histy)
    ax_histy.tick_params(direction='in', labelleft=False)

    # the scatter plot:
    ax_scatter.scatter(x, y)

    # now determine nice limits by hand:
    binwidth = 0.25
    lim = np.ceil(np.abs([x, y]).max() / binwidth) * binwidth
    ax_scatter.set_xlim((-lim, lim))
    ax_scatter.set_ylim((-lim, lim))

    bins = np.arange(-lim, lim + binwidth, binwidth)
    ax_histx.hist(x, bins=bins)
    ax_histy.hist(y, bins=bins, orientation='horizontal')

    ax_histx.set_xlim(ax_scatter.get_xlim())
    ax_histy.set_ylim(ax_scatter.get_ylim())
       
##############################################
# Treemap plot
##############################################          
def plotTreemap(result,label='symbol',key='avg10Volume',groupBy='',gtype='sum',filter='',filter_val='',limit=12):
    import matplotlib.pyplot as plt
    import squarify
    
    res=result[[label,'name','Sector','Industry',key]]
    if groupBy != '' and gtype=='sum':
        res=res[['Sector','Industry',key]]
        res = res.groupby(groupBy).sum()
        res[groupBy]=res.index
        label=groupBy
    if groupBy != '' and gtype=='mean':
        res=res[['Sector','Industry',key]]
        res = res.groupby(groupBy).mean()
        res[groupBy]=res.index
        label=groupBy
    
    if filter != '':
        res = res[res[filter]==filter_val]
        
    res=res.mask(res.eq('None')).dropna()
    res.sort_values(key, ascending=False, inplace=True)
    
    fig, ax = plt.subplots(1, figsize = (12,12))
    sq1=squarify.plot(sizes=res[key], 
                  label=res[label][:limit], 
                  alpha=.8 )
    plt.axis('off')
    plt.show()
    #print('Data is not capitalization weighted!')
    return sq1,res.index[:limit]
    
##############################################
# Plot the yield curve animation
##############################################         
def plotYieldCurve(mydf,fs=(10,7)):
    #add to notebook
    #%matplotlib notebook
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    mpl.rcParams['figure.dpi'] = 60
    plt.style.use(['seaborn-ticks'])
    from matplotlib.widgets import Slider, Button, RadioButtons
    
    result = mydf.transpose()
    axis_color = 'lightgoldenrodyellow'
    x = result.index
    fig = plt.figure(figsize=fs)
    fig.subplots_adjust(left=0.25, bottom=0.25)

    ax = fig.add_subplot(1, 1, 1)
    line, = ax.plot(x, result[mydf.index[0]],label=mydf.index[0])
    line.axes.set_ylim(min(mydf.min()),max(mydf.max()))
    ax.set_xlabel(mydf.index[0].strftime('%Y-%m-%d'))
    ax.set_title('Treasuries Term Structure (' + mydf.index.min().strftime('%y-%m-%d') + ' to ' + mydf.index.max().strftime('%y-%m-%d') + ')')
    plt.ylabel("Yield")
    plt.grid()
    
    amp_slider_ax  = fig.add_axes([0.25, 0.1, 0.65, 0.03], facecolor=axis_color)
    amp_slider = Slider(amp_slider_ax, 'Days', 1, len(mydf), valinit=len(mydf)-1,valfmt="%i")
    strFile=module_path+ '/yield_curve.png'
    if os.path.isfile(strFile):
       os.remove(strFile)
    fig.savefig(strFile, dpi=fig.dpi)

    def update(val):
        line.set_ydata(result[mydf.index[int(amp_slider.val)]])
        ax.set_xlabel(mydf.index[int(amp_slider.val)].strftime('%Y-%m-%d'))
        fig.canvas.draw_idle()
        #strFile=module_path+ '/yield_curve.png'
        #if os.path.isfile(strFile):
        #   os.remove(strFile)
        #fig.savefig(strFile, dpi=fig.dpi)
        #fig.canvas.draw()

    amp_slider.on_changed(update);
    #return slider to avoid garbaging the object
    return amp_slider
    
##############################################
# CAPE and FED Models plot
##############################################          
def plotCAPE(years=200, fs=(25,25), reqd=True, dwnld=False, data=[]):    
    import Plib.DataFarm.Fred as datafarm2
    import Plib.Macro.Indicators as mic
    
    from dateutil import parser
    from time import gmtime, strftime
    from datetime import timedelta, date
    import numpy as np
    import matplotlib.pyplot as plt
    import pandas as pd
    pd.options.mode.chained_assignment = None
    
    today_date=strftime("%Y-%m-%d %H:%M:%S", gmtime())
    dt_start = (parser.parse(today_date) + timedelta(days=-years*252)).strftime("%Y-%m-%d")
    dt_end = (parser.parse(today_date) + timedelta(days=-1)).strftime("%Y-%m-%d")
    
    if dwnld:
        data=mic.getShillerCAPE(reqd)
    CAPE=data.copy()
    CAPE=CAPE[CAPE.index>=(parser.parse(today_date) + timedelta(days=-years*252))]
    
    fig = plt.figure(figsize=fs)
    #ax = fig.add_subplot(1, 1, 1)
    ax = plt.subplot2grid(fs, (0, 0), rowspan=int(fs[0]/4), colspan=int(fs[1]))
    ax.plot(CAPE.index, CAPE['RealPrice'],label='Real S&P Composite Price Index')
    ax.set_ylabel('Real S&P Composite Price Index')
    ax2 = ax.twinx()
    ax2.set_ylabel('Real Earnings')
    ax2.plot(CAPE.index, CAPE['RealEarnings'],label='Real Earnings',linestyle='dotted', color='r')
    ax.legend(loc=2)#,prop={'size': 12})
    ax2.legend(loc=1)#,prop={'size': 12})
    
    ax3 = plt.subplot2grid(fs, (int(fs[0]/4), 0), rowspan=int(fs[0]/4), colspan=int(fs[1]), sharex=ax)
    ax3.plot(CAPE.index, CAPE['CAPE'],label='CAPE 10Y')
    ax3.set_ylabel('CAPE 10Y')
    ax3.legend(loc=2)
    ax4 = ax3.twinx()
    ax4.set_ylabel('Long Term Rates')
    ax4.plot(CAPE.index, CAPE['LT Rates'],label='Long Term Rates',linestyle='dotted', color='r')
    ax4.legend(loc=1)
    
    ax5 = plt.subplot2grid(fs, (2*int(fs[0]/4), 0), rowspan=int(fs[0]/4), colspan=int(fs[1]), sharex=ax)
    ax5.plot(CAPE.index, CAPE['ExcessCapeYield'],label='Excess CAPE 10Y Returns')
    ax5.set_ylabel('Excess CAPE Returns')
    ax5.legend(loc=2)
    ax6 = ax5.twinx()
    ax6.set_ylabel('Subsequent 10Y Returns')
    ax6.plot(CAPE.index, CAPE['ExcessAnnReal10YRetsS-B'],label='Subsequent 10Y Annualized Excess Returns (S-B)',linestyle='dotted', color='r')
    ax6.legend(loc=1)
    
    symbol = 'DGS10'
    us10y_prices = pd.DataFrame(datafarm2.get_eod_data_with_fred(symbol, dt_start, dt_end).interpolate())[['Close']]
    us10 = us10y_prices.resample('M').mean()
    us10=us10.reset_index()
    us10["Date"] = us10["index"].dt.strftime("%Y-%m")
    us10['Date']=pd.to_datetime(us10['Date'])
    us10=us10.set_index('Date').sort_index().interpolate().ffill().backfill()[['Close']]
    us10=us10.sort_index()
    us10.columns=[symbol]
    FED=us10.merge(CAPE.copy(),left_index=True, right_index=True).interpolate().ffill().backfill()
    FED['Ratio']=FED['CAPE']/FED[symbol]
    FED['Ratio2']=FED['Price']/FED[symbol]
    
    ax7 = plt.subplot2grid(fs, (3*int(fs[0]/4), 0), rowspan=int(fs[0]/4), colspan=int(fs[1]), sharex=ax)
    ax7.plot(FED.index, FED['Ratio'],label='CAPE 10Y to DGS10Y Treasuries')
    ax7.set_ylabel('CAPE to Treasuries')
    ax7.legend(loc=2)
    ax8 = ax7.twinx()
    ax8.set_ylabel('S&P Index to Treasuries')
    ax8.plot(FED.index, FED['Ratio2'],label='S&P Composite Price Index to DGS10Y Treasuries',linestyle='dotted', color='r')
    ax8.legend(loc=1)
    
    customize_grid(ax,x=1,y=2)
    customize_grid(ax2,x=1,y=2)
    customize_grid(ax3,x=1,y=2)
    customize_grid(ax4,x=1,y=2)
    customize_grid(ax5,x=1,y=2)
    customize_grid(ax6,x=1,y=2)
    customize_grid(ax7,x=1,y=2)
    customize_grid(ax8,x=1,y=2)
    
    ax.set_title('CAPE 10Y Ratios and Analysis')
    plt.gcf().set_size_inches(20, 20)
    plt.subplots_adjust(hspace=1.10,wspace=0.1)
    
    return CAPE,FED

##############################################
# Inflation and Corporate Bond Distress plot
##############################################          
def plotInflationRates(years=20, fs=(25,25), reqd=True, dwnld=False, data=[]):    
    import Plib.DataFarm.Fred as datafarm2
    
    from dateutil import parser
    from time import gmtime, strftime
    from datetime import timedelta, date
    import numpy as np
    import matplotlib.pyplot as plt
    import pandas as pd
    pd.options.mode.chained_assignment = None
    
    today_date=strftime("%Y-%m-%d %H:%M:%S", gmtime())
    dt_start = (parser.parse(today_date) + timedelta(days=-years*252)).strftime("%Y-%m-%d")
    dt_end = (parser.parse(today_date) + timedelta(days=-1)).strftime("%Y-%m-%d")
    
    if dwnld:
        CBDI=mic.getCBondsDistressIndex(reqd)
    CBDI=data.copy()
    CBDI=CBDI[CBDI.index>=(parser.parse(today_date) + timedelta(days=-years*252))]
    
    fig = plt.figure(figsize=fs)
    ax = plt.subplot2grid(fs, (0, 0), rowspan=int(fs[0]/4), colspan=int(fs[1]))
    ax.plot(CBDI.index, CBDI['Market CMDI'],label='Corporate Bond Market Distress Index')
    ax.set_ylabel('Corp.Bond Market Distress Index')
    ax2 = ax.twinx()
    ax2.set_ylabel('Institutional Grade')
    ax2.plot(CBDI.index, CBDI['IG CMDI'],label='Institutional Grade',linestyle='dotted', color='r')
    ax.legend(loc=2)#,prop={'size': 12})
    ax2.legend(loc=1)#,prop={'size': 12})
    
    ax3 = plt.subplot2grid(fs, (int(fs[0]/4), 0), rowspan=int(fs[0]/4), colspan=int(fs[1]), sharex=ax)
    ax3.plot(CBDI.index, CBDI['Market CMDI'],label='Corporate Bond Market Distress Index')
    ax3.set_ylabel('Corp.Bond Market Distress Index')
    ax3.legend(loc=2)
    ax4 = ax3.twinx()
    ax4.set_ylabel('High Yield')
    ax4.plot(CBDI.index, CBDI['HY CMDI'],label='High Yield Grade',linestyle='dotted', color='r')
    ax4.legend(loc=1)
    
    ax5 = plt.subplot2grid(fs, (2*int(fs[0]/4), 0), rowspan=int(fs[0]/4), colspan=int(fs[1]), sharex=ax)
    ax5.plot(CBDI.index, CBDI['IG CMDI'],label='Institutional Grade')
    ax5.set_ylabel('Institutional Grade')
    ax5.legend(loc=2)
    ax6 = ax5.twinx()
    ax6.set_ylabel('High Yield Grade')
    ax6.plot(CBDI.index, CBDI['HY CMDI'],label='High Yield Grade',linestyle='dotted', color='r')
    ax6.legend(loc=1)
    
    symbol = 'T10YIE'
    us10y_prices = pd.DataFrame(datafarm2.get_eod_data_with_fred(symbol, dt_start, dt_end).interpolate())[['Close']]
    us10 = us10y_prices.resample('W').mean()
    us10=us10.reset_index()
    us10["Date"] = us10["index"].dt.strftime("%Y-%m-%d")
    us10['Date']=pd.to_datetime(us10['Date'])
    us10=us10.set_index('Date').sort_index().interpolate().ffill().backfill()[['Close']]
    us10=us10.sort_index()
    us10.columns=[symbol]
    symbol = 'T5YIFR'
    us5y_prices = pd.DataFrame(datafarm2.get_eod_data_with_fred(symbol, dt_start, dt_end).interpolate())[['Close']]
    us5 = us5y_prices.resample('W').mean()
    us5=us5.reset_index()
    us5["Date"] = us5["index"].dt.strftime("%Y-%m-%d")
    us5['Date']=pd.to_datetime(us5['Date'])
    us5=us5.set_index('Date').sort_index().interpolate().ffill().backfill()[['Close']]
    us5=us5.sort_index()
    us5.columns=[symbol]
    
    INF=us10.merge(us5,left_index=True, right_index=True).interpolate().ffill().backfill()
    
    ax7 = plt.subplot2grid(fs, (3*int(fs[0]/4), 0), rowspan=int(fs[0]/4), colspan=int(fs[1]), sharex=ax)
    ax7.plot(INF.index, INF['T10YIE'],label='10-year breakeven inflation rate (T10YIE)')
    ax7.set_ylabel('10-year inflation')
    ax7.legend(loc=2)
    ax8 = ax7.twinx()
    ax8.set_ylabel('5-year forward inflation')
    ax8.plot(INF.index, INF['T5YIFR'],label='5-year forward inflation expectation rate (T5YIFR)',linestyle='dotted', color='r')
    ax8.legend(loc=1)
    
    customize_grid(ax,x=1,y=2)
    customize_grid(ax2,x=1,y=2)
    customize_grid(ax3,x=1,y=2)
    customize_grid(ax4,x=1,y=2)
    customize_grid(ax5,x=1,y=2)
    customize_grid(ax6,x=1,y=2)
    customize_grid(ax7,x=1,y=2)
    customize_grid(ax8,x=1,y=2)
    
    ax.set_title('Inflation and Corporate Bond Distress Index Analysis')
    plt.gcf().set_size_inches(20, 20)
    plt.subplots_adjust(hspace=1.10,wspace=0.1)
    
    return CBDI,INF
##############################################
# Plot Market Statistic and Liquidity
##############################################          
def plotMarketStatistics(title, prices, ref='', prices2=None,ref2='',future=None, ref3='',volumes=None, ref4='',rsi_win=14):
    import matplotlib.dates as mdates
    import matplotlib as mpl
    import matplotlib.patches as mpatches
    import datetime
    from matplotlib.patches import Ellipse 
    import matplotlib.pyplot as plt
    
    # prepare data
    rsi = get_rsi(prices, rsi_win)
    
    # The Upper plot consisting of indicators
    upper = plt.subplot2grid((12, 5), (0, 0), rowspan=2, colspan=5)
    upper.plot(rsi.index, rsi.RSI, color='black', label='RSI('+str(rsi_win)+')')
    upper.axhline(y=40, xmin=0.0, xmax=1.0, linestyle='--', color='red', linewidth=1)
    upper.axhline(y=60, xmin=0.0, xmax=1.0, linestyle='--', color='green', linewidth=1)
    x_axis = rsi.index.get_level_values(0)
    upper.fill_between(x_axis, 25, 75, color='lavender', label='RSI OverBought/Sold', alpha=0.5)
    plt.title(ref + ' RSI('+str(rsi_win)+')')

    # The top plot consisting of daily trin
    top = plt.subplot2grid((12, 5), (2, 0), rowspan=6, colspan=5, sharex=upper)
    top.xaxis_date()
    top.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    top.plot(prices.index, prices.Close, linewidth=1,color='black',label=ref)
    plt.title(title + '(' + str(prices.index.min())[:10] + '-' + str(prices.index.max())[:10] + ')')
    #Second series - future reference
    if not future.empty:
        top2=top.twinx()
        top2.plot(future.index, future.Adjusted_close/100,color='blue',label=ref3)
        top2.set_ylabel(ref3,color='blue',fontsize=14)
        
    # High-Low Bands
    low, up = prices.Low,prices.High
    x_axis = prices.index.get_level_values(0)
    top.fill_between(x_axis, low, up, color='silver', label='High-Low Bands - ' + ref)
    top.legend(loc=2)

    # The bottom plot consisting of tick index
    prices2=prices2.loc[prices.index.min():prices.index.max()]
    bottom = plt.subplot2grid((12, 5), (8, 0), rowspan=2, colspan=5, sharex=upper)
    bottom.plot(prices2.index, prices2.Close.rolling(window=3).mean(), linestyle='--', color='red', linewidth=1, label=ref2)
    bottom.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    
    # High-Low Bands
    blow, bup = prices2.Low,prices2.High
    x_axis2 = prices2.index.get_level_values(0)
    bottom.fill_between(x_axis2, blow, bup, color='silver', label='High-Low Bands - ' + ref2)    
    bottom.legend(loc=2)
    plt.title(ref2)
    
    # The bottom plot consisting of daily trading volume
    volumes2=volumes.loc[prices.index.min():prices.index.max()]
    bottom2=bottom.twinx()
    bottom2 = plt.subplot2grid((12, 5), (10, 0), rowspan=2, colspan=5, sharex=upper)
    bottom2.bar(volumes2.index, volumes2.Volume, color='orange', label=ref4)
    bottom2.plot(volumes2.index, volumes2.Volume.rolling(window=25).mean(), linestyle='--', color='red', linewidth=1, label='Rolling (25) Mean ' + ref4)
    bottom2.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    bottom2.legend(loc=2)
    plt.title(ref4)
    
    # adjust size and spacing
    top.grid()
    top.minorticks_on()
    top.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    bottom.grid()
    bottom.minorticks_on()
    bottom.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    bottom2.grid()
    bottom2.minorticks_on()
    bottom2.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    upper.grid()
    upper.minorticks_on()
    upper.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    
    plt.gcf().set_size_inches(25, 20)
    plt.subplots_adjust(hspace=1)
    
##############################################
# Plots Liquidity in trading accounts
##############################################     
def plotLiquidityAcc(tit,df1,df2,df3,f1,ref1,ref2,ref3,ref4,rsi_win=3):
    import warnings
    warnings.filterwarnings("once")

    import matplotlib.pyplot as plt
    from matplotlib.ticker import FormatStrFormatter
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()
    
    lt=linestyle_tuple
    df1['Adjusted_close']=df1['Debit_MarginA']
    rsi = get_rsi(df1.interpolate(), rsi_win)

    # The Upper plot consisting of indicators
    upper = plt.subplot2grid((10, 5), (0, 0), rowspan=2, colspan=5)
    upper.plot(rsi.index, rsi.RSI, color='black', label='RSI('+str(rsi_win) +')')
    upper.axhline(y=40, xmin=0.0, xmax=1.0, linestyle='--', color='red', linewidth=1)
    upper.axhline(y=60, xmin=0.0, xmax=1.0, linestyle='--', color='green', linewidth=1)
    x_axis = rsi.index.get_level_values(0)
    upper.fill_between(x_axis, 25, 75, color='lavender', label='RSI OverBought/Sold', alpha=0.5)
    plt.title(ref1 + ' - RSI('+str(rsi_win)+')')
        
    # The top plot consisting of the  series
    top = plt.subplot2grid((10, 5), (2, 0), rowspan=6, colspan=5, sharex=upper)
    #top.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    top.plot(df1.index, df1.Debit_MarginA, linewidth=3,color='black',label=ref1)
    top.plot(df2.index, df2.FreeCredit_MarginA, linewidth=3,color='grey',label=ref2)
    top.plot(df3.index, df3.FreeCredit_CashA, linewidth=3,color='brown',label=ref3)
    plt.title(tit)
    top.set_ylabel('Accounts',color='black',fontsize=14)
    #Plot series
    top2=top.twinx()
    top2.plot(f1.index, f1.Close,color='blue',linestyle=lt[3][1],linewidth=2,label=ref4)
    top2.set_ylabel(ref4,color='blue',fontsize=14)
    top2.legend(loc=1)
    top.legend(loc=2)

    # adjust size and spacing
    top.grid()
    top.minorticks_on()
    top.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    top2.grid()
    top2.minorticks_on()
    top2.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    upper.grid()
    upper.minorticks_on()
    upper.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    
    
    plt.gcf().set_size_inches(25, 20)
    plt.subplots_adjust(hspace=0.75)   

##############################################
# Plots Funds Flows
##############################################     
def plotFundsFlows(tit,df1,df2,df3,f1,ref1,ref2,ref3,ref4,rsi_win=3):
    import warnings
    warnings.filterwarnings("once")
    
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    from matplotlib.ticker import FormatStrFormatter
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()
    
    lt=linestyle_tuple
    df1['Adjusted_close']=df1['Tot_MF_ETF']
    rsi = get_rsi(df1.interpolate(), rsi_win)

    # The Upper plot consisting of indicators
    upper = plt.subplot2grid((10, 5), (0, 0), rowspan=2, colspan=5)
    upper.plot(rsi.index, rsi.RSI, color='black', label='RSI('+str(rsi_win) +')')
    upper.axhline(y=40, xmin=0.0, xmax=1.0, linestyle='--', color='red', linewidth=1)
    upper.axhline(y=60, xmin=0.0, xmax=1.0, linestyle='--', color='green', linewidth=1)
    x_axis = rsi.index.get_level_values(0)
    upper.fill_between(x_axis, 25, 75, color='lavender', label='RSI OverBought/Sold', alpha=0.5)
    upper.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    plt.title(ref1 + ' - RSI('+str(rsi_win)+')')
        
    # The top plot consisting of the  series
    top = plt.subplot2grid((10, 5), (2, 0), rowspan=6, colspan=5, sharex=upper)
    top.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    top.plot(df1.index, df1.Tot_MF_ETF, linestyle=lt[10][1],linewidth=1.5,color='black',label=ref1)
    top.plot(df2.index, df2.Equity_Tot, linestyle=lt[8][1],linewidth=1.5,color='green',label=ref2)
    top.plot(df3.index, df3.Bond_Tot, linestyle=lt[5][1],linewidth=1.5,color='red',label=ref3)
    plt.title(tit)
    top.set_ylabel('Funds Flows',color='black',fontsize=14)
    #Plot series
    top2=top.twinx()
    top2.plot(f1.index, f1.Close,color='blue',linestyle=lt[3][1],linewidth=2,label=ref4)
    top2.set_ylabel(ref4,color='blue',fontsize=14)
    top2.legend(loc=1)
    top.legend(loc=2)
 
    # adjust size and spacing
    top.grid()
    top.minorticks_on()
    top.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    top2.grid()
    top2.minorticks_on()
    top2.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    upper.grid()
    upper.minorticks_on()
    upper.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    
    
    plt.gcf().set_size_inches(25, 20)
    plt.subplots_adjust(hspace=0.75)   
    
##############################################
# Plots Two series by group
##############################################     
def bivariateKDE(df,x_name,y_name,groups_name):
    import warnings
    warnings.filterwarnings("once")
    
    import seaborn as sns

    # Apply the default theme
    sns.set_theme()
    sns.jointplot(
        data=df,
        x=x_name, y=y_name, hue=groups_name,
        kind="kde"
    )