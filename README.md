Plib, a simple framework for quantitative investing 
===================================================

[![Twitter URL](https://img.shields.io/twitter/url?style=social&url=https://twitter.com/BamboosLtd)](https://twitter.com/BamboosLtd)

[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/U7U0349DL)

Plib is a simple trading and economic research library written in Python,
that supports various **backtesting protocols**, as well as paper and live trading via
[Interactive Brokers](https://www.interactivebrokers.com).

I developed Plib because I was experimenting with other tools and unsatisfied with the results or the fees required. Thus, I turned to python and attempted to develop a powerful, trading library that will let me focus on research and ignore everything else.

-----

**You can check my blog where I discuss the bits of Plib and other economic issues:**
[www.bamboos-consulting.com](https://www.bamboos-consulting.com)


-----

Features
========

- Download free and paid data for Jupyter research (with your keys).
- Strategies and trades data (prices, executions, commissions, etc.) is stored in Sqlite for later analysis and backtesting.
- **Support for any granularity of data, including dollar weighted ones**.
- Works with your favorite library of indicators (TA-Lib, etc.).
- Post to Wordpress or Telegram your results or events.
- Can import any Python library in your work.
- Walk Away, Anchored Walk Away, and Seasonal Backtesting with Brute Force optimization
- Strategy Replayer
- Many endpoints for Jupyter reports
- Basic options' strategies for portfolio protection

-----

Examples
==========

Various Jupyter notebooks allow to quickly understanding the functionalities and explain how to implement your reserach and algorithms. See the examples directory.



```python
#Robustness Test
mc,mp=b.montecarloAnalysis(d,N=1000,fs=(12, 8))
```

![png](images/output_8_0.png)
        

```python
#Stop loss and Profit target optimization
df=signalsfarm.getMAEstats(d.copy(),b.trades.copy())
```


```python
#Stop Loss
for sec in tickers:
    signalsfarm.plotMAEStats(df,sec,'MAE',(16,4))
```

    
![png](images/output_12_0.png)
    
    
![png](images/output_12_1.png)
    
    
![png](images/output_12_2.png)
    
    
![png](images/output_12_3.png)
    

```python
#Profit Targets
for sec in tickers:
    signalsfarm.plotMAEStats(df,sec,'MFE',(16,4))
```

    
![png](images/output_13_0.png)
    

    
![png](images/output_13_1.png)
    
    
![png](images/output_13_2.png)
    
    
![png](images/output_13_3.png)
    

```python
#Volatility Cones
pl2.plotVolConesSV(symbol,dt_start,udata,iVol,options,prices)
```

![png](images/output_9c_0.png)


```python
#Calendar Spread
psLegs=sLegs
psLegs[3],psLegs[12]=0.0001,(sLegs[12]-(sLegs[3]-0.0001))
desc='Expiring IV Unchanged'
r2=o.calendarSpread(psLegs,u_price,iRate,desc,0.2,LongShort)
clear_output(wait=True)
pl1.plotUnderlyingStudy2(symbol, dt_start, dt_end, prices,r2,desc,prb=0.25)
```

![png](images/output_9b_0.png)



```python
#Optimizing parameters and Testing the strategy over different times and markets
t=bt.Tester(dt_start,dt_end,tickers,'SPY','60')
#Define the grid for parameters search
grid = ((30, 40, 5),(1.5, 2, 0.5),(30, 40, 5),(1.5, 2, 0.5),(0.01, 0.10, 0.03),(0.01, 0.05, 0.01))
params=[35,1.5,35,1.5,0.05,0.03]
lbls=['Win_1','Sd_1','Win_2','Sd_2','Prof.Target','Stop Loss']
```


```python
#OPTIMIZATION of parameters for Profits/MDD
ret1,elab1=t.optimizer(setupData,logic,grid,params,opt_type='prtgt')
t.plotOptimization(elab1,-1,measure='Prof/MDD',labels=lbls)
```

    Downloading data...
    Maximum at:  [1.5e+00 3.0e+01 1.5e+00 1.0e-02 2.0e-02]  Corresponding to:  0.0018521314355508828
    Maximum at:  [3.5e+01 3.5e+01 1.5e+00 1.0e-02 2.0e-02]  Corresponding to:  0.0019134011822560721
    Maximum at:  [3.5e+01 1.5e+00 1.5e+00 1.0e-02 2.0e-02]  Corresponding to:  0.0019867479078799996
    Maximum at:  [3.0e+01 1.5e+00 3.5e+01 1.0e-02 2.0e-02]  Corresponding to:  0.0018141171837426859
    Maximum at:  [3.0e+01 1.5e+00 3.5e+01 1.5e+00 2.0e-02]  Corresponding to:  0.005463606756947378
    Maximum at:  [35.    1.5  30.    1.5   0.04]  Corresponding to:  0.005274652625949283

![png](images/output_15_1.png)
        
![png](images/output_15_2.png)
    
    
![png](images/output_15_3.png)
    





Installation
============

Install IntelPython within your home dir, best in the opt directory
Install TWS in the same directory, i.e. HOME/opt
Clone the repository in the same directory, i.e. HOME/opt
Add HOME/opt/Plib to the system path
Set apis keys and other stuff in Keystore.py


Requirements
------------

* [Intel Python Distribution 2020.4 ](https://software.intel.com/content/www/us/en/develop/tools/distribution-for-python.html)
* `Lets Be Rational`
* `Py-Vollib`
* `Sqlite3`
* `cvxpy`
* `mplfinance`

* Latest Interactive Brokers’ TWS` 
* `A valid key for api services like IEXCloud, AlphaVantage, Finnhub, Fprep, Fred, IMF, Marketboum, Marketstack, Orats, Quandl, and Tiingo`

-----

Legal Stuff
===========

Plib is licensed under the **Apache License, Version 2.0**. A copy of which is included in LICENSE.txt.

Plib is not a product of Interactive Brokers, nor is it affiliated with Interactive Brokers.