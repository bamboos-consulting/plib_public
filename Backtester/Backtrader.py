#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Backtrader 
#
# Module including functions to perform backtesting
#############################################################################################      

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import pickle
import random
import warnings
import pandas as pd
import numpy as np
import Plib.Utils.Tools as tls
import Plib.Portfolio.Analysis as pmp
import Plib.Ledger.Transactions as tr
import Plib.Ledger.AOrders as aor
import Plib.Ledger.EOrders as eor
    
warnings.filterwarnings("once")
#pd.options.mode.chained_assignment = None  # default='warn'

DEBUG_PRINT=0

app=None
brkr=0
exch='SMART'
cur='USD'
clnt=0
    
    
#############################################################################################
# Module Backtrader 
#
# Methods to manipulate Buffer objects
#############################################################################################          
 
   
########################################################
# Run the Strategy over multiple securities in parallel
########################################################      
def nstocksAnalysis(stocks,hdata,setupData,logic,params=[],algo_params={},stat_params={},init_params={},NProc=4):
    import multiprocessing
    from itertools import product
    import time
    
    #stocks=sec
    results =[]
    
    args = [(s,hdata,setupData,logic,params,algo_params,stat_params,init_params) for s in stocks]
    
    starttime = time.time()
    
    with multiprocessing.Pool(processes=NProc) as pool:
        results = pool.starmap(helper_nstocksAnalysis, args)
        pool.close()

    df=pd.DataFrame()
    dfr=pd.DataFrame()
    for i in range(0,len(results)):
        s,mdf,r=results[i]
        mdf.columns=[s]
        df[s] = mdf[s].values
        if len(dfr)==0:
            dfr=r[['Returns']].copy()
            dfr.columns=[s]
        else:
            dfr[s]=r[['Returns']]
    df.index=mdf.index
    if DEBUG_PRINT==2:
        print('Pseudo-Parallel Execution time: {} seconds'.format(round(time.time() - starttime),2))
    return df,dfr      
    
def helper_nstocksAnalysis(s,hdata,setupData,logic,params,algo_params,stat_params,init_params):
    data=hdata[s].copy()
    #data.pop('tickers', None)
    #tickers=hdata[s]['tickers']
    tickers=algo_params['Issues']
    tickers[0]=s
    b=setupData(data,tickers,params,{},algo_params,init_params)
    
    b.run(logic)
    r,ts=b.printSnapshot(type=stat_params['SnapType'],MAR=stat_params['MAR'],ConfLev=stat_params['ConfLev'],net_comm=stat_params['net_comm'],log_rets=stat_params['log_rets'],leverage=stat_params['Leverage'])
    mdf=ts.T
    return s,mdf,r
    
########################################################
# Run the Strategy over multiple securities in parallel
########################################################
def KellyAnalysis(sec,setupData,logic,data,tickers,params=[],algo_params={},stat_params={},init_params={},NProc=4):
    import multiprocessing
    from itertools import product
    import time
    
    kellyF=[0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45]
    results =[]
    
    starttime = time.time()
    b=setupData(data,tickers,params,{},algo_params,init_params)
    args = [(s,b,sec,logic,stat_params,algo_params,init_params) for s in kellyF]
    
    with multiprocessing.Pool(processes=NProc) as pool:
        results = pool.starmap(helper_KellyAnalysis, args)
        pool.close()

    df=pd.DataFrame()
    for i in range(0,len(results)):
        mdf=results[i].copy()
        K=mdf.columns[0]
        df[K] = mdf[K].values
        df.index=mdf.index
    if DEBUG_PRINT==2:
        print('Pseudo-Parallel Execution time: {} seconds'.format(round(time.time() - starttime),2))
    return df  
    
def helper_KellyAnalysis(K,b,sec,logic,stat_params,algo_params,init_params):
    algo_params[sec]['FRAC']=K
    b.run(logic)
    r,ts=b.printSnapshot(type='silent',MAR=stat_params['MAR'],ConfLev=stat_params['ConfLev'],net_comm=stat_params['net_comm'],log_rets=stat_params['log_rets'],leverage=stat_params['Leverage'])
    mdf=(ts).T[:-1]
    mdf.columns=[K]
    return mdf
  
#############################################################################################
# Class Buffer 
#
# Class including functions to store data and perform backtesting
#############################################################################################      
class Buffer:
    
    # Variables for internal reference to class parameters and initialization    
    buffer_blinders={}
    buffer_params={}
    str_params={}
    index=[]
    buffer=[]
    tz=''
    freq=''
    running_copy=[]
    stat1=[]
    stat2=[]
    stat3=[]
    weights=[]
    tracker={}
    trades=[]
    dt_start=''
    dt_end=''
    rules={'Squaring':0,'Slippage':0,'Commissions':0,'Bracketing':0}
    
    # Variables for database connection and initialization
    sid=0
    path = '/Plib/Ledger'
    filedb = (module_path + path + '/ledgerBT.db').replace('/Plib/Plib/','/Plib/')
    hcols=[]
    
    ##################################################
    # Constructor Initializing all shared variables
    ##################################################      
    def __init__(self,init_params={},rules={}):  
             
        self.dt_start=init_params['sdate']
        self.dt_end=init_params['edate']                            # The index of buffer should be always more dense than the
        self.freq=init_params['freq']                               # timeseries added. For instance, index typically includes
        if rules != {}: self.rules=rules                            # weekdays and holidays; as a result, values are copied forward.  
        self.buffer_blinders={}                                     # Conversely, if timeseries are in minute frequency, index will be in half minute. 
        self.buffer_params={}
        df=pd.date_range(start=init_params['sdate'],end=init_params['edate'], freq=init_params['freq'], tz=init_params['tz'], normalize=True)
        self.index=pd.DataFrame(df,columns=['Date'])
        self.index=self.index.set_index('Date')
        self.index = self.index.sort_index(ascending=True)
        
        self.buffer=self.index.copy()
        self.tz=init_params['tz']
        self.buffer_blinders={}
        self.buffer_params={}
        self.running_copy=[]
        self.stat1=[]
        self.stat2=[]
        self.algo_params=[]
        self.str_params={}
        
        if init_params['db']=='': db=self.filedb
        aor.setParams(db,init_params['brk'],init_params['exc'],init_params['cli'],init_params['in_mem'])
        eor.setParams(db,init_params['brk'],init_params['exc'],init_params['cli'],init_params['in_mem'])
        
        tr.startLedger(init_params['brk'],init_params['exc'],db,[1,init_params['cli']]) #init_params[in_mem]
        self.sid=tr.createStr(init_params['str_name'],init_params['str_desc'])
    
    ##################################################
    # Add a time series as available to the algorithm
    ##################################################      
    def addData(self,df,label,sec_params=[],freq='D',fix_tz=True,fix_frq=True):
        #Convert to tz aware (localize with tze) and convert to the trader tz
        if fix_tz:
            if df.index.tz != None:
                    df.index=df.index.tz_convert(self.tz)
            else:
                df.index=df.index.tz_localize('America/New_York', nonexistent='shift_forward')
                df.index=df.index.tz_convert(self.tz)
        if freq=='D' or freq=='1D': df.index=df.index.normalize()
        if fix_frq:
            #replicate last trading day over non-trading days
            #temp=dflf.set_index(dflf.index.shift(0, freq=freq)).resample(freq).ffill()
            #return pd.merge(dfhf,temp,left_index=True, right_index=True)
            temp=tls.mergeDFHigherFreq(self.index,df,freq).add_suffix('_'+label)
            temp.index = pd.to_datetime(temp.index)
            self.buffer_blinders[label]=temp
        else:
            self.buffer_blinders[label]=df.add_suffix('_'+label)
        params={'Commission':sec_params[0],'Slippage':sec_params[1],'ProfTarget':sec_params[2],'StopLoss':sec_params[3]}
        self.buffer_params[label]=params
        self.buffer=pd.merge(self.buffer,self.buffer_blinders[label],left_index=True, right_index=True).copy()
        
    ##################################################
    # Add a time series as an index to the algorithm
    ##################################################      
    def addTracker(self,df,label,freq='D',fix_tz=True,fix_frq=True):
        #Convert to tz aware (localize with tze) and convert to the trader tz
        if fix_tz:
            if df.index.tz != None:
                df.index=df.index.tz_convert(self.tz)
            else:
                df.index=df.index.tz_localize('UTC', nonexistent='shift_forward')
                df.index=df.index.tz_convert(self.tz)
        if fix_frq:
            temp=tls.mergeDFHigherFreq(self.index,df,freq).add_suffix('_'+label)
            temp.index = pd.to_datetime(temp.index)
        self.tracker[label]=df.add_suffix('_'+label)
        self.tracker[label]['Returns'] = 0
        self.tracker[label]['Returns'] = self.tracker[label]['Adjusted_close_'+label].pct_change()
        self.tracker[label]['Returns'] = self.tracker[label]['Returns'].astype('float')
        
    ##################################################
    # Prepare the accounts for running
    ##################################################      
    def finalizeData(self,usd_account=100000,params={'Type':'VOID'}):
        self.hcols=self.buffer.columns.to_list()
        
        self.buffer['Date']=self.buffer.index
        self.buffer=self.buffer.dropna()
        self.buffer['Traded']=0
        self.buffer['Account']=usd_account
        self.buffer['Long']=0
        self.buffer['Short']=0
        self.buffer['Invested']=0 
        self.buffer['Portfolio']=0 
        for i in self.buffer_blinders.keys():
            self.buffer[str(i)+'_sh']=0
            self.buffer[str(i)+'_pr']=0
            self.buffer[str(i)+'_cm']=0
            self.buffer[str(i)+'_sp']=0
            self.buffer[str(i)+'_prt']=0
            self.buffer[str(i)+'_last']=0
            self.buffer[str(i)+'_signal']=0
            self.buffer[str(i)+'_TP']=0
            self.buffer[str(i)+'_SL']=0
            self.buffer[str(i)+'_OT']=0
            self.buffer[str(i)+'_CK']=0
            
            if params['Type']!='VOID':
                self.algo_params=params
                self.str_params['Type']=params['Type']
            self.buffer[str(i)+'_sh']=self.buffer[str(i)+'_sh'].astype('float')
            self.buffer[str(i)+'_pr']=self.buffer[str(i)+'_pr'].astype('float')
            self.buffer[str(i)+'_cm']=self.buffer[str(i)+'_cm'].astype('float')
            self.buffer[str(i)+'_sp']=self.buffer[str(i)+'_sp'].astype('float')
            self.buffer[str(i)+'_prt']=self.buffer[str(i)+'_prt'].astype('float')
            self.buffer[str(i)+'_last']=self.buffer[str(i)+'_last'].astype('float')
            self.buffer[str(i)+'_signal']=self.buffer[str(i)+'_last'].astype('float')
            self.buffer[str(i)+'_TP']=self.buffer[str(i)+'_TP'].astype('float')
            self.buffer[str(i)+'_SL']=self.buffer[str(i)+'_SL'].astype('float')
            self.buffer[str(i)+'_OT']=self.buffer[str(i)+'_OT'].astype('float')
            self.buffer[str(i)+'_CK']=self.buffer[str(i)+'_OT'].astype('float')
            
        self.buffer['Traded'] = self.buffer['Traded'].astype('float')
        self.buffer['Account']=self.buffer['Account'].astype('float')
        self.buffer['Long']=self.buffer['Long'].astype('float')
        self.buffer['Short']=self.buffer['Short'].astype('float')
        self.buffer['Invested']=self.buffer['Invested'].astype('float')
        self.buffer['Portfolio']=self.buffer['Portfolio'].astype('float')
        self.dt_start=str(self.buffer.index.min().date())
        self.dt_max=str(self.buffer.index.max().date()) 
            
    ##################################################
    # Execute the strategy
    ##################################################      
    def run(self, algo_logic):
        
        # By contruction the buffer is passed as a reference to an object
        # inner functions and algo_logic must not reassign x to avoid working on a copy

        
        # Custom rules - others to be added in rules factory
        def bracketingATR(i:int,sec:str,price:float,side:int):
            #to be implemented as follows:
            #HCW = max(HIGHS,win=30); LCW = min(LOWS,win=30) 
            #buy:   sl = HCW - (ATR x sl_factor) ; tp = HCW + (ATR x tp_factor)
            #sell:  sl = LCW + (ATR x sl_factor) ; tp = LCW - (ATR x tp_factor)
            if side==1:
                sl=x['HCW_'+sec].iloc[i]-(x['ATR_'+sec].iloc[i]*self.buffer_params[sec]['StopLoss'])
                tp=x['HCW_'+sec].iloc[i]+(x['ATR_'+sec].iloc[i]*self.buffer_params[sec]['ProfTarget'])
            else:
                sl=x['LCW_'+sec].iloc[i]+(x['ATR_'+sec].iloc[i]*self.buffer_params[sec]['StopLoss'])
                tp=x['LCW_'+sec].iloc[i]-(x['ATR_'+sec].iloc[i]*self.buffer_params[sec]['ProfTarget'])    
            return tp,sl
            
        def squaringAlt(i:int,sec:str,side:int,price:float,open:bool=True):
            ret=0
            if open:
                if (side != x[sec+'_last'].iloc[i-1]) and (x[sec+'_prt'].iloc[i-1] >0):
                    trade(i,str(sec),side,float(price),float(1),False)
                    ret=0
                elif x[sec+'_prt'].iloc[i-1] ==0:
                    ret=0
                else:    
                    ret=-1
            if DEBUG_PRINT> 3: print('Alternating Squaring ', ret,i,side,x[sec+'_last'].iloc[i-1],x[sec+'_prt'].iloc[i-1])
            return ret
        
        def squaringTPSL(i:int,sec:str,side:int,price:float,open:bool=True):
            ret=0
            if open:
                n=0
                if x[sec+'_last'].iloc[i] != 0:
                    n=checkStops(sec,price,i)
                #if DEBUG_PRINT> 2: print('Checkstops ', i, n,x[sec+'_last'].iloc[i])
                #TP/SL Precedence - Ignore signal if already traded
                if n>0:
                    #checkStops closed a trade
                    x[sec+'_last'].iloc[i]=0
                    ret= -1
                elif n==0 and x[sec+'_last'].iloc[i]!=0:
                    #checkStops did not closed a trade, skip
                    ret= -1
                elif n==0 and x[sec+'_last'].iloc[i]==0:
                    #no open trades 
                    x[sec+'_last'].iloc[i]=int(side)
                    ret=0 
            return ret                                   

        def squaringATR(i:int,sec:str,side:int,price:float,open:bool=True):
            ret=0
            if open:
                n=0
                if x[sec+'_last'].iloc[i] != 0:
                    n=checkStopsATR(sec,price,x['High_'+sec].iloc[i],x['Low_'+sec].iloc[i],i)
                #if DEBUG_PRINT> 2: print('Checkstops ', i, n,x['High_'+sec].iloc[i],x['Low_'+sec].iloc[i])
                #TP/SL Precedence - Ignore signal if already traded
                if n>0:
                    #checkStops closed a trade
                    x[sec+'_last'].iloc[i]=0
                    ret= -1
                elif n==0 and x[sec+'_last'].iloc[i]!=0:
                    #checkStops did not closed a trade, skip
                    ret= -1
                elif n==0 and x[sec+'_last'].iloc[i]==0:
                    #no open trades 
                    x[sec+'_last'].iloc[i]=int(side)
                    ret=0
            return ret                                   
        
        def slippageFixed(sec:str,price:float,side:int):
            if side==1:
                slippage=round(float(price)*(1+float(self.buffer_params[sec]['Slippage'])),4)
            else:
                slippage=round(float(price)*(1-float(self.buffer_params[sec]['Slippage'])),4)
            return 
        
        def commissionsFixed(sec:str,price:float,qty:float):
            return float(self.buffer_params[sec]['Commission'])
        
        def kellyFraction(args={'price':0,'kelly':0,'wealth':0,'fraction':0,'qty':0}):
            qty=args['qty']
            if args['price'] >0:
                bet=args['kelly']*args['wealth']
                qty=np.trunc(args['fraction']*bet/args['price'])
            return qty
    
        def fixedPercent(args={'price':0,'wealth':0,'fraction':0,'qty':0}):
            qty=args['qty']
            if args['price'] >0:
                bet=args['fraction']*args['wealth']
                qty=np.trunc(bet/args['price'])
            return qty
                              
        # Prebuilt rules
        def bracketingRule(i:int,sec:str,price:float,side:int):
            tp=price+(side)*price*float(self.buffer_params[sec]['ProfTarget'])/100
            sl=price-(side)*price*float(self.buffer_params[sec]['StopLoss'])/100
            return tp,sl
        
        def squaringRule(i:int,sec:str,side:int,price:float,open:bool=True):
            if DEBUG_PRINT> 3: print('No Squaring ', 0)
            x[sec+'_last'].iloc[i]=int(side)
            return 0
        
        def slippageRule(sec:str,price:float,side:int):
            def slippage(price:float,side:int,slippage:float):
                def slip(price,side,slippage):
                    return abs(price * (side+slippage))
                def noslip(price,side,slippage):
                    return price    
                return random.choice([slip,noslip])(price,side,slippage)
                
            return round(float(slippage(price,side,float(self.buffer_params[sec]['Slippage']))),4)
        
        def commissionsRule(sec:str,price:float,qty:float):
            return round(abs(price*qty*float(self.buffer_params[sec]['Commission'])),4)
        
        def tradefilterRule(i:int,sec:str,side:int,price:float,open:bool=True):
            if DEBUG_PRINT> 3: print('No Filter ', 0)
            return 0
        
        def moneyMgmt(args={'qty':1}):
            return args['qty']
            
        # Method to perform standard operations                     
        def trade(i:int,sec:str,side:int,price:float,qty:float,open:bool=True,aoid:int=0,pri:float=0):                                                        
            x[sec+'_signal'].iloc[i]=side
            if squaringRule(i,sec,side,price,open)!=0: 
                return                                #Check squaring rule
            if tradefilterRule(i,sec,side,price,open)!=0: 
                return                             
            if DEBUG_PRINT> 2: print('Trading ',sec,x[sec+'_signal'].iloc[i],qty,open,x['Date'].iloc[i],i)
            x.Traded.iloc[i]=side                                                            #Used for montecarlo analysis
            x[sec+'_last'].iloc[i]=side
            price_ns=price
            price=slippageRule(sec,price,side)
            comm=commissionsRule(sec,price,qty)                                              #Computing commissions
            slipg=round(abs(price_ns-price),4)                                               #Computing slippage
            x[sec+'_cm'].iloc[i]=x[sec+'_cm'].iloc[i]+comm                                   #Updating commissions
            x[sec+'_sp'].iloc[i]=x[sec+'_sp'].iloc[i]+slipg                                  #Updating slippage
            x[sec+'_pr'].iloc[i]=price
            x[sec+'_sh'].iloc[i]= qty  
            tp,sl=bracketingRule(i,sec,price,side)
            x[sec+'_TP'].iloc[i]= tp
            x[sec+'_SL'].iloc[i]= sl
            x[sec+'_OT'].iloc[i]= 0
            
            params=[0,1,tp,sl,x['Date'].iloc[i],comm,slipg]
            if open:
                if side ==-1:   #Opening a short position                                          
                    x.Account.iloc[i]=x.Account.iloc[i] - price*qty                          #Decreasing account of invested amount
                    x.Short.iloc[i]=x.Short.iloc[i] + price*qty                              #Increasing short
                    x[sec+'_prt'].iloc[i]=x[sec+'_prt'].iloc[i]-qty                          #Updating Shares Running Total
                else:           #Opening a long position 
                    x.Account.iloc[i]=x.Account.iloc[i] - price*qty                          #Decreasing account of invested amount
                    x.Long.iloc[i]=x.Long.iloc[i] + price*qty                                #Increasing long
                    x[sec+'_prt'].iloc[i]=x[sec+'_prt'].iloc[i]+qty
                if DEBUG_PRINT> 3: print('Filled/Runn_total ',x[sec+'_sh'].iloc[i],x[sec+'_prt'].iloc[i])
                aoid=aor.createAOrder(self.sid,sec, price, qty, side, 'STK','','',0,params,'O')
                eoid=eor.createEOrder(self.sid,aoid,'','',str(price),qty,str('sotype'),str('STK'),x['Date'].iloc[i],comm,slipg)
                if DEBUG_PRINT> 3: print('Recorded ',sec,side,qty,open)
            else:
                if side ==-1:   #closing a long with a short
                    x.Account.iloc[i]=x.Account.iloc[i] + (price)*qty                        #increasing account of gain/loss
                    x.Long.iloc[i]=x.Long.iloc[i] - pri*qty                                  #decreasing long of initial amount
                    x[sec+'_prt'].iloc[i]=x[sec+'_prt'].iloc[i]-qty
                else:           #closing a short with a long
                    x.Account.iloc[i]=x.Account.iloc[i] - (price-pri)*qty                    #increasing account of gain/loss
                    x.Short.iloc[i]=x.Short.iloc[i] - pri*qty                                #decreasing short of initial amount
                    x[sec+'_prt'].iloc[i]=x[sec+'_prt'].iloc[i]+qty
                if DEBUG_PRINT> 3: print('Filled/Runn_total ',x[sec+'_sh'].iloc[i],x[sec+'_prt'].iloc[i])
                aoid=aor.createAOrder(self.sid,sec, price, qty, side, 'STK','','',0,params,'C')
                eoid=eor.createEOrder(self.sid,aoid,'','',str(price),qty,str('sotype'),str('STK'),x['Date'].iloc[i],comm,slipg)
                if DEBUG_PRINT> 3: print('Recorded ',sec,side,qty,open)
                
        def checkStops(l:str,mp:float,i:int):
            sel=x.iloc[:i-1,]
            df=sel[['Date',str(l)+'_sh',str(l)+'_pr',str(l)+'_last',str(l)+'_TP',str(l)+'_SL',str(l)+'_CK']].copy()
            #df=df.reset_index()
            df=df.reset_index(drop=True)
            df.columns=['DateI','QtyI','PrI','SideI','TP','SL','CK']
            df.QtyI.replace(0,np.nan,inplace=True)
            df.CK.replace(1,np.nan,inplace=True)
            df=df.dropna()
            short=df[(df.SideI==-1)&((df.TP>mp)|(df.SL<mp))]
            long=df[(df.SideI==1)&((df.TP<mp)|(df.SL>mp))]
            trades=short.append(long)
            N=len(trades)
            if N>0:
                for k in range(N-1,N):
                    trade(i,l,-1*int(trades.SideI.iloc[k]),float(mp),float(trades.QtyI.iloc[k]),False,0,float(trades.PrI.iloc[k]))
                    x[l+'_CK'].loc[x.Date==trades.DateI.iloc[k]]=1
            return len(trades)

        def checkStopsATR(l:str,mp:float,high:float,low:float,i:int):
            sel=x.iloc[:i-1,]
            df=sel[['Date',str(l)+'_sh',str(l)+'_pr',str(l)+'_last',str(l)+'_TP',str(l)+'_SL',str(l)+'_CK']].copy()
            #df=df.reset_index()
            df=df.reset_index(drop=True)
            df.columns=['DateI','QtyI','PrI','SideI','TP','SL','CK']
            df.QtyI.replace(0,np.nan,inplace=True)
            df.CK.replace(1,np.nan,inplace=True)
            df=df.dropna()
            short=df[(df.SideI==-1)&((df.TP>low)|(df.SL<high))]
            long=df[(df.SideI==1)&((df.TP<low)|(df.SL>high))]
            trades=short.append(long)
            N=len(trades)
            if N>0:
                for k in range(N-1,N):
                    if DEBUG_PRINT> 2: print('Closing TP/SL -----------------',i,l,int(trades.SideI.iloc[k]),1,mp,high,low)
                    trade(i,l,-1*int(trades.SideI.iloc[k]),float(mp),float(trades.QtyI.iloc[k]),False,0,float(trades.PrI.iloc[k]))
                    x[l+'_CK'].loc[x.Date==trades.DateI.iloc[k]]=1
            return len(trades)
        
        def closeAllTrades(sec:str,mp:float,i:int):
            for l in self.buffer_blinders.keys():
                qty=abs(x[str(l)+'_prt'].iloc[i-1])
                side=x[str(l)+'_last'].iloc[i-1]*-1
                if qty != 0:
                    if DEBUG_PRINT> 3: print('Squaring trade...',sec,side,qty)
                    trade(i,str(sec),int(side),float(mp),float(qty),False)
                    #update db with open orders executed to be closed with check on quantity                  
            if self.str_params != {}:
                if self.str_params['Type']=='CURRW':
                    x[sec+'_pr']=x['Adjusted_close_'+str(sec)]       
        
        # Initialization of variables        
        self.running_copy=self.buffer.copy()
        x=self.running_copy
        hdata=x[x.columns.intersection(self.hcols)] 

        
        if self.rules['Squaring'] == 1:
            squaringRule=squaringAlt
        elif self.rules['Squaring'] == 2:
            squaringRule=squaringTPSL
        elif self.rules['Squaring'] == 3:
            squaringRule=squaringATR
        if self.rules['Bracketing'] == 1:
            bracketingRule=bracketingATR
        if self.rules['Slippage'] == 1:
            slippageRule=slippageFixed
        if self.rules['Commissions'] == 1:
            commissionsRule=commissionsFixed
        if self.rules['MoneyMgmt'] == 1:
            moneyMgmt=kellyFraction
        elif self.rules['MoneyMgmt'] == 2:
            moneyMgmt=fixedPercent
        
        
        for i in range(1,self.running_copy.shape[0]):     
            if i>=1:
                x.Account.iloc[i]=x.Account.iloc[i-1]
                x.Long.iloc[i]=x.Long.iloc[i-1]
                x.Short.iloc[i]=x.Short.iloc[i-1]
                x.Invested.iloc[i]=x.Long.iloc[i-1] + x.Short.iloc[i-1]
                
                for l in self.buffer_blinders.keys():
                    x[str(l)+'_last'].iloc[i]=x[str(l)+'_last'].iloc[i-1]
                    x[str(l)+'_prt'].iloc[i]=x[str(l)+'_prt'].iloc[i-1]
                
                if i==1:
                    if self.str_params != {}:
                        if self.str_params['Type']=='CURRW':
                            #Purchase/sell the initial weights
                            for s in self.algo_params.keys():
                                if s != 'Type':
                                    side=1
                                    mprice=x['Adjusted_close_'+str(s)].iloc[i]  
                                    qty=int(self.algo_params[str(s)]['AVAL'])
                                    if qty < 0: side=-1
                                    trade(i,s,side,mprice,qty)
                
                #Run trade logic
                algo_logic(i,x,None,None,hdata,trade,checkStops,closeAllTrades,moneyMgmt,self.algo_params)
                
                
    ##################################################
    # Extract trades from system
    ##################################################  
    def extractTrades(self,x,i=0,log_rets=False,net_comm=False):
        #sec,wl,perc,usd,pricei,pricef,commpi,commpf,slipi,slipf,side,tstart,tend,QtyI,QtyF,duration
        if i >0:
            x=x.iloc[:i,]
        trades=pd.DataFrame()
        for l in self.buffer_blinders.keys():
            df1=x[['Date',str(l)+'_sh',str(l)+'_pr',str(l)+'_signal',str(l)+'_TP',str(l)+'_SL',str(l)+'_cm',str(l)+'_sp']].copy()
            #df1=df1.reset_index()
            df1=df1.reset_index(drop=True)
            df1.columns=['DateI','QtyI','PrI','SideI','TP','SL','slipI','commI']
            df1.QtyI.replace(0,np.nan,inplace=True)
            df1=df1.dropna()

            df2=df1.copy()
            df2=df2.shift(-1)
            df2.columns=['DateF','QtyF','PrF','SideF','TP','SL','slipF','commF']                
            df2.QtyF.replace(0,np.nan,inplace=True)
            df2=df2.dropna()
            
            df1=df1.reset_index()
            df2=df2.reset_index()
            
            if self.rules['Squaring'] == 2 or self.rules['Squaring'] == 3:
                df1['drop']=np.where(df1.index % 2 == 1,1,0)
                df1=df1[df1['drop']==0].copy()
                df2['drop']=np.where(df2.index % 2 == 1,1,0)
                df2=df2[df2['drop']==0].copy()
    
            df1=df1[['DateI','QtyI','PrI','SideI','TP','SL','slipI','commI']]
            df2=df2[['DateF','QtyF','PrF','SideF','TP','SL','slipF','commF']]

            ttrades=pd.concat([df1,df2],axis=1,join='inner')
            ttrades=ttrades[['DateI','DateF','QtyI','QtyF','PrI','PrF','SideI','slipI','commI','slipF','commF']]
            
            ttrades['usd']=0.
            ttrades['perc']=0.
            ttrades['wl']=0
            
            if self.freq in ('D','W','M'):
                ttrades['duration']=(ttrades.DateF-ttrades.DateI).astype('timedelta64[D]') #D,m,h,s
            elif self.freq in ('60T','H','2H','4H','6H'):
                ttrades['duration']=(ttrades.DateF-ttrades.DateI).astype('timedelta64[h]') #D,m,h,s
            elif self.freq in ('1T','2T','4T','20T'):
                ttrades['duration']=(ttrades.DateF-ttrades.DateI).astype('timedelta64[m]') #D,m,h,s
            else:
                ttrades['duration']=(ttrades.DateF-ttrades.DateI).astype('timedelta64[D]') #D,m,h,s
                
            ttrades['sec']=l
            
            ttrades=ttrades[['sec','wl','perc','usd','PrI','PrF','commI','commF','slipI','slipF','SideI','DateI','DateF','QtyI','QtyF','duration']]
            ttrades.columns=['sec','wl','perc','usd','pricei','pricef','commpi','commpf','slipi','slipf','side','tstart','tend','QtyI','QtyF','duration']

            if log_rets:
                #Slippage is already included whereas commissions are not
                if net_comm:
                    ttrades['usd']=((ttrades.QtyF*ttrades.pricef)-(ttrades.QtyI*ttrades.pricei))*ttrades.side
                    #(((PriceF*QtyF)-CommPF)/((PriceI*QtyI)-CommPI))'
                    ttrades.perc=np.log((ttrades.pricef*ttrades.QtyF)/(ttrades.pricei*ttrades.QtyI))
                else:
                    ttrades['usd']=(((ttrades.QtyF*ttrades.pricef)-ttrades.commpf)-((ttrades.QtyI*ttrades.pricei)+ttrades.commpi))*ttrades.side
                    #((PriceF*QtyF)/(PriceI*QtyI))'
                    ttrades.perc=np.log(((ttrades.pricef*ttrades.QtyF)-ttrades.commpf)/((ttrades.pricei*ttrades.QtyI)-ttrades.commpi))
            else:
                if net_comm:
                    ttrades['usd']=((ttrades.QtyF*ttrades.pricef)-(ttrades.QtyI*ttrades.pricei))*ttrades.side
                    ttrades['perc']=((ttrades.pricef*ttrades.QtyF)/(ttrades.pricei*ttrades.QtyI))-1
                else:
                    ttrades['usd']=(((ttrades.QtyF*ttrades.pricef)-ttrades.commpf)-((ttrades.QtyI*ttrades.pricei)+ttrades.commpi))*ttrades.side
                    ttrades['perc']=(((ttrades.QtyF*ttrades.pricef)-ttrades.commpf)/((ttrades.QtyI*ttrades.pricei)+ttrades.commpi))-1
            
            ttrades['wl']=np.where(ttrades.usd>0,1,0)
            
            trades=trades.append(ttrades)
            
        return trades.dropna()
        
                   
    ##################################################
    # Present a summary of the execution
    ##################################################      
    def printSnapshot(self,type='none', MAR=-0.001,ConfLev=0.95,net_comm=False,fs=(15,4),leverage=1.25,fs2=(10,3),log_rets=False):
        result,comm,slipp,r=self.getTotProfit(net_comm,log_rets)
        self.trades=self.extractTrades(r,0,log_rets,net_comm)
        
        if log_rets:
            for l in self.tracker.keys():
                self.tracker[l]['Returns'] = 0
                self.tracker[l]['Returns']=np.log(self.tracker[l]['Adjusted_close_'+l] / self.tracker[l]['Adjusted_close_'+l].shift(1)).fillna(0)
                self.tracker[l]['Returns'] = self.tracker[l]['Returns'].astype('float')       
                
        if type in ['simple','risk','rplots','all','all-pa']:
            self.simpleStats(r.copy(),result,slipp,comm,log_rets=log_rets)
        
        if type in ['simple','all','all-pa','tearsh','silent','silent_ts']:
            if type in ['silent','tearsh','silent_ts']:
                self.tradesPlots(r[['Account','Invested','Long','Short','Position']].copy(),self.trades.copy(),fs,True,profits=result,log_rets=log_rets)
            else:
                self.tradesPlots(r[['Account','Invested','Long','Short','Position']].copy(),self.trades.copy(),fs,profits=result,log_rets=log_rets)
        
        if type in ['rplots','all','all-pa','tearsh','silent','silent_rs']:
            trk_lbl=str(list(self.tracker.keys())[0])
            if type in ['silent','tearsh','silent_rs']:
                self.returnPlots(r[['Portfolio']].copy(),self.tracker[trk_lbl][['Adjusted_close_'+trk_lbl]].copy(),result,ConfLev,leverage,MAR,fs,True,log_rets=log_rets)
            else:
                self.returnPlots(r[['Portfolio']].copy(),self.tracker[trk_lbl][['Adjusted_close_'+trk_lbl]].copy(),result,ConfLev,leverage,MAR,fs,log_rets=log_rets)
        
        if type in ['pattr','all']:
            if len(r[['Portfolio']].copy().resample('M').sum()) > 12:
                self.perfAttrib(r[['Portfolio']].copy(),stats=True,fs=fs2)
            
        if type in ['tearsh']:
            tearsheet=(self.stat1.T)
            print(tearsheet)
        
        stats=None
        if type in ['all','all-pa','tearsh','silent']:
            cols=list(self.stat2['Parameters'])
            df=(self.stat2.T)[1:]
            df.columns=cols
            self.stat3=self.stat1.join(df)
            stats=self.stat3
        elif type in ['silent_ts']:
            stats=self.stat1
        elif type in ['silent_rs']:
            stats=self.stat2
        elif type in ['simple']:
            stats=self.stat1
            
        return r,stats

    ##################################################
    # Strategy replayer
    ##################################################                      
    def replayStrategy(self,d,s=250):
        #%matplotlib notebook
        #import pandas as pd
        import mplfinance as mpf
        import matplotlib.animation as animation

        df=d.copy()
        df['Open']=df.Portfolio
        df['High']=df.Portfolio
        df['Low']=df.Portfolio
        df['Close']=df.Portfolio

        df['Volume']=0
        for i in range(len(d)):
            for k in self.buffer_blinders.keys():
                df['Volume'].iloc[i]=df['Volume'].iloc[i]+df[str(k)+'_sh'].iloc[i]
        df=df[1:]

        fig = mpf.figure(style='charles',figsize=(7,4))
        ax1 = fig.add_subplot(2,1,1)
        ax2 = fig.add_subplot(3,1,3)

        def animate(ival):
            if (20+ival) > len(df):
                print('no more data to plot')
                ani.event_source.interval *= 3
                if ani.event_source.interval > 12000:
                    exit()
                return
            data = df.iloc[0:(20+ival)]
            ax1.clear()
            ax2.clear()
            mpf.plot(data,ax=ax1,volume=ax2,type='line')
    
        ani = animation.FuncAnimation(fig, animate, interval=s)

        mpf.show()
    
    ##################################################
    # Trades and Stocks Analysis
    ##################################################   
    def bootstrapAnalysis(self,d,N=100,conf_lev=0.95,W=4,fs=(12, 8),bins=50,log_rets=False):
        import time
        rule=d[['Traded','Returns']].copy()
        bret=d[['Returns']].copy()
        if self.freq=='D':
            rule=rule.resample('D').sum()
        elif self.freq=='H':
            rule=rule.resample('H').sum()
        sret=(self.stat1['Avg Daily Rets'].iloc[0])
        mdd=(self.stat1['Max Drawdown %'].iloc[0])
        
        from Plib.Portfolio.Analysis import bootstrapAnalysis 
        from Plib.Portfolio.Plots import plotMontecarlo
        from Plib.Utils.Tools import parallelExecution2
        
        starttime = time.time()
        simfun_args=rule, sret
        cols=[]
        cols=[i for i in range(len(rule))]
        dmc,dmcp=parallelExecution2(N,simfun_args,['Returns','MaxDD'],cols,bootstrapAnalysis,W)
        if DEBUG_PRINT==2:
            print('Parallel Execution time: {} seconds'.format(round(time.time() - starttime),2))
        plotMontecarlo(bret,sret,mdd,dmc,dmcp,N,fs=fs,bins=50,title='Montecarlo',conf_lev=conf_lev,log_rets=log_rets)
        return dmc,dmcp
                         
    def montecarloAnalysis(self,d,ml,N=100,W=4,fs=(12, 8),bins=50,lt_avgrets=0,log_rets=False):
        import time
        dmc=0
        if self.tracker == {}:
            print("A tracker must be specified to perform Montecarlo Analysis")
        else:
            mkt=self.tracker[ml][['Returns']].copy()
            mkt.columns=['MRet']
            rule=d[['Traded','Returns']].copy()
            bret=d[['Returns']].copy()
            if self.freq=='D':
                mkt=mkt.resample('D').sum()
                rule=rule.resample('D').sum()
                res=mkt.join(rule, how='right', rsuffix='r',lsuffix='l')
            elif self.freq=='H':
                mkt=mkt.resample('H').sum()
                rule=rule.resample('H').sum()
                res=mkt.join(rule, how='right', rsuffix='r',lsuffix='l')
            sret=(self.stat1['Avg Daily Rets'].iloc[0])
            mdd=(self.stat1['Max Drawdown %'].iloc[0])
            
            from Plib.Portfolio.Analysis import montecarloAnalysis 
            from Plib.Portfolio.Plots import plotMontecarlo
            from Plib.Utils.Tools import parallelExecution2
            
            starttime = time.time()
            simfun_args=res, lt_avgrets
            cols=[]
            cols=[i for i in range(len(res))]
            dmc,dmcp=parallelExecution2(N,simfun_args,['Returns','MaxDD'],cols,montecarloAnalysis,W)
            if DEBUG_PRINT==2:
                print('Parallel Execution time: {} seconds'.format(round(time.time() - starttime),2))
            plotMontecarlo(bret,sret,mdd,dmc,dmcp,N,fs=fs,bins=50,title='Montecarlo',conf_lev=0,log_rets=log_rets)
        return dmc,dmcp
    
    def volAnalysis(self,ticker,periods=21,silent=False,fs=(8,8),tz='America/New_York'):
        df=self.buffer_blinders[ticker].copy()
        df.rename(columns={'Adjusted_close_'+ticker:'Adjusted_close'}, inplace=True)
        
        if self.freq=='H':
            retv=pmp.volatilityAnalysis(df,ticker,periods,silent,fs,tz)
        elif self.freq=='D':
            retv=pmp.volatilityMAnalysis(df,ticker,periods,silent,fs,tz)
            
        return retv 
        
    def hwd_filterHeatmap(self,trades,silent=False,fs=(8, 8),tz='America/New_York'):
        return pmp.tradesFilters(trades,silent,fs,tz)
    
    def hitratioAnalysis(self,hr,silent=False,fs=(14,8)):
        ba=pmp.breakupAnalysis(hr,period='M')
        df=ba['Hit Ratio'].to_frame()
        retv=pmp.hitratioMAnalysis(df,silent=False,fs=(14,8))
        return retv
        
    def hitratioAnalysisPlot(self,df,pf,hr,silent=False,fs=(18,4)):
        ret=pmp.selHRMonths(df,pf,hr,silent,fs)
        return ret
        
    def tseqAnalysis(self,sumt,top,silent=False,fs=(18, 4),data={}):
        trades=self.trades.copy()
        ret=pmp.tradeSeqAnalysis(trades,sumt,top,fs,silent,data)
        return ret
    
    def rollingWindowAnalysis(self,r,MAR=-0.001,ConfLev=0.95,net_comm=False,leverage=1.25,log_rets=False): 
        trk_lbl=str(list(self.tracker.keys())[0])
    
        wins=[30,90,180,365,365*2,365*3,365*4,365*5]
        coln=['1 Month','3 Months','6 Months','12 Months','24 Months','36 Months','48 Months','60 Months']
    
        i=0
        d1=pd.DataFrame()
        for w in wins:
            result=0 #r['Account'].iloc[0]
            self.returnPlots(r[['Portfolio']].copy()[:w],self.tracker[trk_lbl][['Adjusted_close_'+trk_lbl]].copy()[:w],result,ConfLev,leverage,MAR,0,True,log_rets=log_rets)
            ret=self.stat1.T
            ret.columns=[str(w) + ' days']
            if i==0:
                d1=ret
            else:
                d1=d1.join(ret)
            i=i+1
        
        i=0
        d2=pd.DataFrame()
        for w in wins:
            trades=self.extractTrades(r,w,log_rets,net_comm)
            self.tradesPlots(r[['Account','Invested','Long','Short','Position']].copy()[:w],trades,0,True,profits=result,log_rets=log_rets)
            ret=self.stat2.set_index('Parameters',inplace=False)[1:]       
            ret.columns=[str(w) + ' days']
            if i==0:
                d2=ret
            else:
                d2=d2.join(ret)
            i=i+1
    
        d1.columns=coln
        d2.columns=coln
    
        return d1,d2
        
    ##################################################
    # Helper functions
    ##################################################                      
    def getTotProfit(self,net_comm=False,log_rets=False):
        r=self.running_copy.copy()
        self.dt_start=str(r.index.min().date())
        self.dt_max=str(r.index.max().date())
        result,comm,slipp=self.computePortValue(r,net_comm,log_rets)
        return result,comm,slipp,r
    
    def computePortValue(self,r,net_comm=False,log_rets=False):
        comm,slipp=0.0,0.0                              #Init totals
        for i in self.buffer_blinders.keys():
            comm = comm + r[str(i)+'_cm'].sum()                             #running total for commissions
            slipp = slipp + r[str(i)+'_sp'].sum()                           #running total for slippage (already included in prices)
            
        #for montecarlo simulation we need a traded flag (-1,1)
        #when trading multiple securities we assign the prevailing 
        #direction (buy/sell) based on the considered instant i
        if len(list(self.buffer_blinders.keys()))>1:
            for i in range(1,len(r)-1):    
                if r['Traded'].iloc[i] != 0:    
                    long=r.Long.iloc[i]-r.Long.iloc[i-1]
                    short=r.Short.iloc[i]-r.Short.iloc[i-1]
                    if long>short:
                        r['Traded'].iloc[i]=1
                    elif short<long:
                        r['Traded'].iloc[i]=-1
                    else:
                        pass
        r['Position'] = r['Traded'].replace(to_replace=0, method='ffill')                
        
        #Compute returns by considering the cash flows
        r['Portfolio']= r['Account']                        
        if log_rets:
            r['Returns']=np.log(r['Portfolio'] / r['Portfolio'].shift(1)).fillna(0)
        else:
            r['Returns']=r['Portfolio'].pct_change()
        r['Portfolio'] = r['Portfolio'].astype('float')
        r['Returns'] = r['Returns'].astype('float')
        result=r['Portfolio'].iloc[-1]
        return result,comm,slipp
    
    def simpleStats(self,r,result,slipp,comm,log_rets=False):
        #Simple stats
        string='Initial Positions: ' + str(r.Account.iloc[0]); print(string)
        string='Final Positions: ' + str(round(result,2)); print(string)
        perc= ((result/r.Account.iloc[0])-1)
        string='Countervalue (increase/decrease %): ' + str(round(result,2)) + ' (' + str(round(100*perc,4))+'%)  ';print(string)
        string='Commissions (CUR): ' + str(round(comm,2)) + '  Slippage: ' + str(round(slipp,2));print(string)
        
        ret = pd.DataFrame({'Parameters': ['Profits','Inc./Decr. %'],
                              'Value': [result,100*perc]})   
        df=ret.T
        df.columns=list(ret.Parameters)
        self.stat1=df.tail(1)
        
    def tradesPlots(self,r,trades,fs=(15,4),silent=False,profits=0,log_rets=False):
        #Liquidity plot
        tradesheet=pmp.tradesAnalysis(r,trades,silent,profits,fs=fs,log_rets=log_rets)
        self.stat2=tradesheet
    
    def returnPlots(self,rp,rm,profits,ConfLev,lev,MAR,fs=(10,7),silent=False,log_rets=False):
        #Resample to daily returns
        t_days=252
        if log_rets:
            rp=rp.resample('D').last().dropna()
            rm=rm.resample('D').last().dropna()
            rp=np.log(rp / rp.shift(1)).fillna(0)
            rm=np.log(rm / rm.shift(1)).fillna(0)
        else:    
            rp=rp.resample('D').last().dropna().pct_change().fillna(0)
            rm=rm.resample('D').last().dropna().pct_change().fillna(0)
        rp.columns=['portfolio']
        rp['date']=pd.to_datetime(rp.index)
        rp=rp.set_index('date')
        rm.columns=['market']
        #rp.index=rp.index.date
        #rm.index=rm.index.date
        #pa=pd.merge(rp,rm,left_index=True, right_index=True).dropna()
        
        #pa=pd.merge(rp,rm,left_index=True, right_index=True)
        #pa.columns=['portfolio','market']      
        #pa = pa.replace([np.inf, -np.inf], np.nan)
        #pa=pa.dropna()
        #Create the right time index by resampling daily to daily
        #pa=pa.reset_index()
        #pa['date'] = pa['Date'].astype('datetime64[ns]')
        #pa=pa[['portfolio','market','date']]
        #pa = pa.groupby(pd.Grouper(freq='D', key='date')).mean().dropna()
        #pa.index = pd.to_datetime(pa.index)
        
        #MAR*t_days
        ret=pmp.returnAnalysis(MAR,t_days,rp,rm,profits,ConfLev,lev,fs,'portfolio','market',silent,log_rets=log_rets)
        df=ret.T
        df.columns=list(ret.Parameters)
        self.stat1=df.tail(1)
    
    def perfAttrib(self,rp,stats=True,fs=(10,3)):        
        tz='America/New_York'
        rp=rp.reset_index()
        rp['Date'] = pd.to_datetime(rp['Date'],utc=True)
        rp['Date'] = rp['Date'].astype('datetime64[ns]')
        rp['Date'] = rp.Date.dt.tz_localize('UTC').dt.tz_convert(tz)
        rp['Date'] = rp['Date'].astype('datetime64[ns]')
        rp=rp.set_index('Date')
        rp = rp.sort_index(ascending=True)
        return pmp.perfAttribution('','','',stats=stats,silent=False,fs=fs,dwld=False,price_data=rp)
        
        
#############################################################################################
# Class Tester 
#
# Class including functions to analyze backtesting
#############################################################################################                      
class Tester:
    
    dt_start=''
    dt_end=''
    tickers=[]
    tracker=''
    debug={}
    secl=[]
    hist={}
        
    def __init__(self,sdate,edate,tickrs,trk,data):
        self.dt_start=sdate
        self.dt_end=edate
        self.tickers=tickrs
        self.tracker=trk
        t=tickrs.copy()
        t.append(trk)
        self.secl=t
        
        self.hist={}
        #Acquire data for the whole period
        self.hist['0']={'sdate':sdate,'edate':edate}
        index=0
        for sec in self.secl:    
            self.hist['0'][sec]=data[str(index)]
        
    ##################################################
    # Wrappers functions
    ##################################################                      
    def walkAwayBT(self,setupData, logic,grid,params,n_bt=6,pct_oos=0.1,optm=True,W=1,algo_params={'Type':'VOID'},stat_params={'freq':'D'},init_params={'freq':'D'}):
        btype='waway'
        return self.backtester(setupData, logic, params,grid, btype,n_bt,pct_oos,optm=optm,W=W,algo_params=algo_params,stat_params=stat_params,init_params=init_params)

    def anchoredWalkAwayBT(self,setupData, logic,grid,params,n_bt=6,pct_oos=0.1,optm=True,W=1,algo_params={'Type':'VOID'},stat_params={'freq':'D'},init_params={'freq':'D'}):
        btype='awaway'
        return self.backtester(setupData, logic, params,grid, btype,n_bt,pct_oos,optm=optm,W=W,algo_params=algo_params,stat_params=stat_params,init_params=init_params)

    def crossValidationBT(self,setupData, logic,grid,params,n_bt=2,optm=True,W=1,algo_params={'Type':'VOID'},stat_params={'freq':'D'},init_params={'freq':'D'}):
        btype='crossv'
        return self.backtester(setupData, logic, params,grid, btype,n_bt,0,optm=optm,W=W,algo_params=algo_params,stat_params=stat_params,init_params=init_params)

    def seasonalValidationBT(self,setupData, logic,grid,params,d1='',d2='',n_bt=3,unit='month',optm=True,W=1,algo_params={'Type':'VOID'},stat_params={'freq':'D'},init_params={'freq':'D'}):
        btype='seas'
        unit=d1+'#'+d2+'#'+unit
        return self.backtester(setupData, logic, params,grid, btype,n_bt,unit,optm=optm,W=W,algo_params=algo_params,stat_params=stat_params,init_params=init_params)

    def plotOptimization(self,r,lbl='',angle=45,measure='net_profit',labels=['Win_1','Sd_1','Win_2','Sd_2']):
        df=r.copy().reset_index()
        c=0
        r=self.plot3DParams(df[[c,c+1,measure]],[labels[c],labels[c+1],measure],'Optimization Plot (Run: '+str(lbl)+') - Strategy Parameters '+labels[c]+', '+labels[c+1]+' for ' + measure,angle=angle)
        
        #if i==-1:
        #    df=r.copy().reset_index()
        #    i=1
        #else:
        #    df=r[i].copy().reset_index()
        #c=0
        #for i in range(1,int((len(df.columns)-1)/2)+1):
        #    r=self.plot3DParams(df[[c,c+1,measure]],[labels[c],labels[c+1],measure],'Optimization Plot (Run: '+str(i)+') - Strategy Parameters '+labels[c]+', '+labels[c+1]+' for ' + measure,angle=angle)
        #    c=c+2
     
    def plot3DParams(self,df,labels=[],title='',point=None,angle=45):
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        
        df.columns=['Y','X','Z']

        # Make the plot
        fig = plt.figure(figsize=(20,8))
        ax = fig.gca(projection='3d')
        surf=ax.plot_trisurf(df['Y'], df['X'], df['Z'],cmap=plt.cm.inferno, linewidth=1.9, antialiased=True)
        #Add labels, title, point, and colorbar
        if labels==[]:
            ax.set_xlabel('Par_0', labelpad=15)
            ax.set_ylabel('Par_1', labelpad=15)
            ax.set_zlabel('Net Profits', labelpad=5)
        else:
            ax.set_xlabel(labels[0], labelpad=15)
            ax.set_ylabel(labels[1], labelpad=15)
            ax.set_zlabel(labels[2], labelpad=5)    
        ax.set_title(title, pad=50)
        if point is not None:
            ax.text(point[0], point[1], point[2], point[3], color=point[4])
        fig.colorbar( surf, shrink=0.5, aspect=5)
    
        # Rotate and show
        ax.view_init(60, angle)
        plt.show()
        return df,surf
    
    ##################################################
    # Data Management
    ##################################################                      
    def createData(self,hist,dt_start,dt_end,pct_oos,n_bt,bt_type,sec_list,optm=True):
        import datetime
        
        tot_units = int((pd.to_datetime(dt_end)-pd.to_datetime(dt_start)).days)
        if bt_type=='waway' or bt_type=='awaway':
            oos=int(tot_units*pct_oos)
            roll_win = int((tot_units-oos)/n_bt)
        elif bt_type=='seqd' or bt_type=='seas':
            seq=int(tot_units/n_bt)
            unit=str(pct_oos)
            
        index=1
        for i in range(0,n_bt):
            if optm==True:
                if bt_type=='waway' or bt_type=='awaway':
                    if bt_type=='waway':
                        ds1=(pd.to_datetime(dt_start)+i*datetime.timedelta(days=roll_win)).strftime("%Y-%m-%d")
                        de1=(pd.to_datetime(dt_start)+(i+1)*datetime.timedelta(days=roll_win)).strftime("%Y-%m-%d")
                        ds2=(pd.to_datetime(dt_start)+(i+1)*datetime.timedelta(days=roll_win)).strftime("%Y-%m-%d")
                        de2=(pd.to_datetime(dt_start)+(i+1)*datetime.timedelta(days=roll_win)+datetime.timedelta(days=oos)).strftime("%Y-%m-%d")
                    else:
                        ds1=(pd.to_datetime(dt_start)).strftime("%Y-%m-%d")
                        de1=(pd.to_datetime(dt_start)+(i+1)*datetime.timedelta(days=roll_win)).strftime("%Y-%m-%d")
                        ds2=(pd.to_datetime(dt_start)+(i+1)*datetime.timedelta(days=roll_win)).strftime("%Y-%m-%d")
                        de2=(pd.to_datetime(dt_start)+(i+1)*datetime.timedelta(days=roll_win)+datetime.timedelta(days=oos)).strftime("%Y-%m-%d")    
                
                    hist[str(index)]={'sdate':ds1,'edate':de1}
                    for sec in sec_list:
                        hist[str(index)][sec]=hist['0'][sec][ds1:de1]
                    hist[str(index+1)]={'sdate':ds2,'edate':de2}
                    for sec in sec_list:
                        hist[str(index+1)][sec]=hist['0'][sec][ds2:de2]
                    index=index+1
            
                elif bt_type=='seqd' or bt_type=='seas':
                    if bt_type=='seqd':
                        ds=(pd.to_datetime(dt_start)+i*datetime.timedelta(days=seq)).strftime("%Y-%m-%d")
                        de=(pd.to_datetime(dt_start)+(i+1)*datetime.timedelta(days=seq)).strftime("%Y-%m-%d")
                    else:
                        if unit=='month':
                            ds = (pd.to_datetime(dt_start) + pd.DateOffset(months=-i)).strftime("%Y-%m-%d")
                            de = (pd.to_datetime(dt_end) + pd.DateOffset(months=-i)).strftime("%Y-%m-%d")
                        elif unit =='week':
                            ds = (pd.to_datetime(dt_start) + pd.DateOffset(weeks=-i)).strftime("%Y-%m-%d")
                            de = (pd.to_datetime(dt_end) + pd.DateOffset(weeks=-i)).strftime("%Y-%m-%d")
                        elif unit =='day':
                            ds = (pd.to_datetime(dt_start) + pd.DateOffset(days=-i)).strftime("%Y-%m-%d")
                            de = (pd.to_datetime(dt_end) + pd.DateOffset(days=-i)).strftime("%Y-%m-%d")
                        elif unit =='year':
                            ds = (pd.to_datetime(dt_start) + pd.DateOffset(years=-i)).strftime("%Y-%m-%d")
                            de = (pd.to_datetime(dt_end) + pd.DateOffset(years=-i)).strftime("%Y-%m-%d")

                    hist[str(index)]={'sdate':ds,'edate':de}
                    for sec in sec_list:
                        hist[str(index)][sec]=hist['0'][sec][ds:de]
        
            else:
                if bt_type=='waway':
                    ds=(pd.to_datetime(dt_start)+i*datetime.timedelta(days=roll_win)).strftime("%Y-%m-%d")
                    de=(pd.to_datetime(dt_start)+(i+1)*datetime.timedelta(days=roll_win)).strftime("%Y-%m-%d")
                elif bt_type=='awaway':
                    ds=(pd.to_datetime(dt_start)).strftime("%Y-%m-%d")
                    de=(pd.to_datetime(dt_start)+(i+1)*datetime.timedelta(days=roll_win)).strftime("%Y-%m-%d")
                elif bt_type=='seqd':
                    ds=(pd.to_datetime(dt_start)+i*datetime.timedelta(days=seq)).strftime("%Y-%m-%d")
                    de=(pd.to_datetime(dt_start)+(i+1)*datetime.timedelta(days=seq)).strftime("%Y-%m-%d")
                elif bt_type=='seas':
                    if unit=='month':
                        ds = (pd.to_datetime(dt_start) + pd.DateOffset(months=-i)).strftime("%Y-%m-%d")
                        de = (pd.to_datetime(dt_end) + pd.DateOffset(months=-i)).strftime("%Y-%m-%d")
                    elif unit =='week':
                        ds = (pd.to_datetime(dt_start) + pd.DateOffset(weeks=-i)).strftime("%Y-%m-%d")
                        de = (pd.to_datetime(dt_end) + pd.DateOffset(weeks=-i)).strftime("%Y-%m-%d")
                    elif unit =='day':
                        ds = (pd.to_datetime(dt_start) + pd.DateOffset(days=-i)).strftime("%Y-%m-%d")
                        de = (pd.to_datetime(dt_end) + pd.DateOffset(days=-i)).strftime("%Y-%m-%d")
                    elif unit =='year':
                        ds = (pd.to_datetime(dt_start) + pd.DateOffset(years=-i)).strftime("%Y-%m-%d")
                        de = (pd.to_datetime(dt_end) + pd.DateOffset(years=-i)).strftime("%Y-%m-%d")
              
                hist[str(index)]={'sdate':ds,'edate':de}
                for sec in sec_list:
                    hist[str(index)][sec]=hist['0'][sec][ds:de]
                    #print(hist['0'][sec][ds:de])
                        
            index=index+1
        return hist,index-1
    
    ##################################################
    # Optimizer
    ##################################################                      
    @staticmethod
    def fnSingle(x,hist,k,shapeData,Tradinglogic,add_par={},algo_params={},stat_params={},init_params={},selab=[]):
        #nonlocal elab #to save stats

        dt_start,dt_end=hist[str(k)]['sdate'],hist[str(k)]['edate']
        tickers=list(hist[str(k)].keys())[2:]
        data={}
        i=0 #Runs from i=0; the index i refers to the raw data structure feeded into shapedata
        for tickr in tickers:
            data[str(i)]=hist[str(k)][tickr]    
            i=i+1

        #set the fix parameter in its position
        if add_par !={}:
            x=np.insert(x,int(list(add_par.keys())[0]),int(add_par[str(list(add_par.keys())[0])]))

        b=shapeData(data,tickers, x,add_par,algo_params,init_params)
        b.run(Tradinglogic)

        #stat_params={'freq':'D','MAR':-0.001,'ConfLev':0.95,'net_comm':False,'Leverage':1.25,'log_rets':False}
        d,ts=b.printSnapshot(type=stat_params['SnapType'],MAR=stat_params['MAR'],ConfLev=stat_params['ConfLev'],net_comm=stat_params['net_comm'])

        df=(pd.DataFrame(data=x)).T
        lbl=stat_params['OPMS']
        min_stat=ts[lbl].iloc[0]
        df[lbl]=min_stat
        selab.append(list(df.iloc[0].values))
        
        return -(min_stat)
        
    @staticmethod
    def fnRatio(x,hist,k,shapeData,Tradinglogic,add_par={},algo_params={},stat_params={},init_params={},selab=[]):
        #nonlocal elab #to save stats 

        dt_start,dt_end=hist[str(k)]['sdate'],hist[str(k)]['edate']
        tickers=list(hist[str(k)].keys())[2:]
        data={}
        i=0 #Runs from i=0; the index i refers to the raw data structure feeded into shapedata
        for tickr in tickers:
            data[str(i)]=hist[str(k)][tickr]    
            i=i+1

        #set the fix parameter in its position
        if add_par !={}:
            x=np.insert(x,int(list(add_par.keys())[0]),int(add_par[str(list(add_par.keys())[0])]))

        b=shapeData(data,tickers, x,add_par,algo_params,init_params)
        b.run(Tradinglogic)

        #stat_params={'freq':'D','MAR':-0.001,'ConfLev':0.95,'net_comm':False,'Leverage':1.25,'log_rets':False}
        d,ts=b.printSnapshot(type=stat_params['SnapType'],MAR=stat_params['MAR'],ConfLev=stat_params['ConfLev'],net_comm=stat_params['net_comm'])

        df=(pd.DataFrame(data=x)).T
        lbl1=stat_params['OPMS1']
        lbl2=stat_params['OPMS2']
        lbl3=lbl1+'/'+lbl2
        profits=ts[lbl1].iloc[0]
        mdd=ts[lbl2].iloc[0]
        if profits !=0:
            if mdd !=0:
                min_stat=profits/(mdd*profits)
            else:
                min_stat=0
        else:
            min_stat=0
        df[lbl3]=min_stat
        selab.append(list(df.iloc[0].values))
        #elab=elab.append(df)

        return -(min_stat)

    def optimizer(self,shapeData,Tradinglogic,grid=(),add_par=[],hist={},index=0,N=5,W=4,silent=False,algo_params={'Type':'VOID'},stat_params={'freq':'D'},init_params={'freq':'D'}):
        from multiprocessing import Manager
        from scipy import optimize
        
        #Initialize the variable storing the data
        #Returning values with parallel execution require a shared memory
        manager = Manager()
        selab = manager.list()
        
        #Define a function for optimization
        #def setupData(dt_start,dt_end,data={},tickers=[],params=[],opt_param={},freq='H'):
        #def f(hist,k,shapeData,Tradinglogic,params,opt_param={}):
        #def fnSingle(x,hist,k,shapeData,Tradinglogic,add_par={},algo_params={},stat_params={},selab=[])
        #def fnRatio(x,hist,k,shapeData,Tradinglogic,add_par={},algo_params={},stat_params={},selab=[])
        
        #Select measure to minimize
        #{'OPTM':'SINGLE','OPMS':'Max Drawdown %'}
        #{'OPTM':'RATIO','OPMS1':'Profits','OPMS2':'Max Drawdown %'}
        if stat_params['OPTM']=='SINGLE':
            lbl=stat_params['OPMS']
            opt_f=self.fnSingle
        elif stat_params['OPTM']=='RATIO':
            lbl1=stat_params['OPMS1']
            lbl2=stat_params['OPMS2']
            lbl=lbl1+'/'+lbl2
            opt_f=self.fnRatio
        
        #Obtain suitable data for optimization
        if hist=={}:
            hist,index=self.createData(self.hist.copy(),self.dt_start,self.dt_end,pct_oos=0,n_bt=0,bt_type='optm',sec_list=self.secl)
        #Setup ranges for optimization parameters ---------------------------------------------------------
        #grid = ((18, 30, 4),(18, 30, 4),(1, 2, 0.5),(1, 2, 0.5))
        #Scipy_optimizer-------------------------------------------------------------------
        if add_par == []:
            ret = optimize.brute(opt_f, grid, args=(hist,index,shapeData,Tradinglogic,{},algo_params,stat_params,init_params,selab),full_output=True,finish=None,Ns=N,workers=W)
            if (DEBUG_PRINT>0 or silent): print('Maximum at: ',ret[0],' Corresponding to: ',abs(ret[1]))
        else:
            for i in range(1,len(add_par)+1):
                n_add_par={}
                n_add_par[str(i-1)]=add_par[i-1]
                n_grid=grid[:i-1]+grid[i:]
                ret = optimize.brute(opt_f, n_grid, args=(hist,index,shapeData,Tradinglogic,n_add_par,algo_params,stat_params,init_params,selab),full_output=True,finish=None,Ns=N,workers=W)
                if (DEBUG_PRINT>0 or silent): print('Maximum at: ',ret[0],' Corresponding to: ',abs(ret[1]))
        #----------------------------------------------------------------------------------
        #Each process computation is re-assembled here
        elab=pd.DataFrame(list(selab),columns =[0, 1,lbl])
        return ret,elab       

    ##################################################
    # Backtetster
    ##################################################                      
    def seasonalValidation(self,f,shapedata,logic,params,histdata={},grid=(),nset=2,optm=True,W=4,algo_params={'Type':'VOID'},stat_params={'freq':'D'},init_params={'freq':'D'}):
        
        r=pd.DataFrame()
        elab={}
        i=1
        e_i=0
        if optm:
            if stat_params['OPTM']=='SINGLE':
                lblm=stat_params['OPMS']
            elif stat_params['OPTM']=='RATIO':
                lblm=stat_params['OPMS1']+'/'+stat_params['OPMS2']
            
            for k in range(0,nset):
                if DEBUG_PRINT>0: 
                    e_i=1+e_i
                    desc='Elaboration '+ str(e_i)+'/'+str(nset*nset)+' on interval:' 
                    desc=desc+str(histdata[str(i)]['sdate']+' to '+histdata[str(i)]['edate']) + '(Optimizing parameters)'
                    print(desc)
                ret,elab[str(i)]=self.optimizer(shapedata,logic,grid,params,histdata,i,silent=True,W=W,algo_params=algo_params,stat_params=stat_params,init_params=init_params)
                elab[str(i)][lblm]=elab[str(i)][lblm].round(2)
                elmax=elab[str(i)][elab[str(i)][lblm]==elab[str(i)][lblm].max()]
                params_opt=(elmax.iloc[0:1,:-1].values)[0]
                r1i=f(histdata,i,shapedata,logic,params_opt,{},algo_params,stat_params,init_params) 
                r=r.append(r1i)
                j=1
                for m in range(0,nset):
                    if (i!=j):
                        if DEBUG_PRINT>0: 
                            e_i=e_i+1
                            desc='Elaboration '+ str(e_i)+'/'+str(nset*nset)+' on interval:' 
                            desc=desc+str(histdata[str(j)]['sdate']+' to '+histdata[str(j)]['edate']) + '(Using optimized params)'
                            print(desc)
                        r2i=f(histdata,j,shapedata,logic,params_opt,{},algo_params,stat_params,init_params) 
                        r=r.append(r2i)
                    j=j+1
                i=i+1
                
                
        else:
            for k in range(1,nset+1):
                params_opt=params
                if DEBUG_PRINT>0: 
                    desc='Elaboration '+ str(k)+'/'+str(nset)+' on interval:' 
                    desc=desc+str(histdata[str(i)]['sdate']+' to '+histdata[str(i)]['edate']) + '(No optimization requested)'
                    print(desc)
                r1i=f(histdata,i,shapedata,logic,params_opt,{},algo_params,stat_params,init_params) 
                r=r.append(r1i)
                i=i+1
                   
        return r,elab     
            
    def crossValidation(self,f,shapedata,logic,params,histdata={},grid=(),nset=2,optm=True,W=4,algo_params={'Type':'VOID'},stat_params={'freq':'D'},init_params={'freq':'D'}):
        
        #Optimization
        #grid = ((15, 40, 5),(1, 2, 0.5),(15, 40, 5),(1, 2, 0.5))
        #params=[21,2,22,2.1]
        r=pd.DataFrame()
        elab={}
        #elab1,elab2,elab3,elab4=[],[],[],[]
        
        if optm:
            if stat_params['OPTM']=='SINGLE':
                lblm=stat_params['OPMS']
            elif stat_params['OPTM']=='RATIO':
                lblm=stat_params['OPMS1']+'/'+stat_params['OPMS2']
                
            for i in range(1,nset):
                if DEBUG_PRINT>0: 
                    desc='Elaboration '+ str(i)+'/'+str(nset*2)+' on interval:' 
                    desc=desc+str(histdata[str(i)]['sdate']+' to '+histdata[str(i)]['edate']) + '(Optimizing parameters)'
                    print(desc)
                ret,elab[str(i)]=self.optimizer(shapedata,logic,grid,params,histdata,i,silent=True,W=W,algo_params=algo_params,stat_params=stat_params,init_params=init_params)
                elab[str(i)][lblm]=elab[str(i)][lblm].round(2)
                elmax=elab[str(i)][elab[str(i)][lblm]==elab[str(i)][lblm].max()]
                params_opt=(elmax.iloc[0:1,:-1].values)[0]
                r11=f(histdata,i,shapedata,logic,params_opt,{},algo_params,stat_params,init_params)   
                r=r.append(r11) 
                if DEBUG_PRINT>0: 
                    desc='Elaboration '+ str(i+1)+'/'+str(nset*2)+' on interval:' 
                    desc=desc+str(histdata[str(i+1)]['sdate']+' to '+histdata[str(i+1)]['edate']) + '(Using optimized params)'
                    print(desc)
                r21=f(histdata,i+1,shapedata,logic,params_opt,{},algo_params,stat_params,init_params)  
                r=r.append(r21)    

                if DEBUG_PRINT>0: 
                    desc='Elaboration '+ str(i+2)+'/'+str(nset*2)+' on interval:' 
                    desc=desc+str(histdata[str(i+1)]['sdate']+' to '+histdata[str(i+1)]['edate']) + '(Optimizing parameters)'
                    print(desc)
                ret,elab[str(i+2)]=self.optimizer(shapedata,logic,grid,params,histdata,i+1,silent=True,W=W,algo_params=algo_params,stat_params=stat_params,init_params=init_params)
                elab[str(i+2)][lblm]=elab[str(i+2)][lblm].round(2)
                elmax=elab[str(i+2)][elab[str(i+2)][lblm]==elab[str(i+2)][lblm].max()]
                params_opt=(elmax.iloc[0:1,:-1].values)[0]
                r11=f(histdata,i+1,shapedata,logic,params_opt,{},algo_params,stat_params,init_params)   
                r=r.append(r11) 
                if DEBUG_PRINT>0: 
                    desc='Elaboration '+ str(i+3)+'/'+str(nset*2)+' on interval:' 
                    desc=desc+str(histdata[str(i)]['sdate']+' to '+histdata[str(i)]['edate']) + '(Using optimized params)'
                    print(desc)
                r21=f(histdata,i,shapedata,logic,params_opt,{},algo_params,stat_params,init_params)  
                r=r.append(r21)                        
        else:
            params_opt=params
            for i in range(1,nset+1):
                if DEBUG_PRINT>0: 
                    desc='Elaboration '+ str(i)+'/'+str(nset)+' on interval:' 
                    desc=desc+str(histdata[str(i)]['sdate']+' to '+histdata[str(i)]['edate']) + '(No optimization requested)'
                    print(desc)
                r21=f(histdata,i,shapedata,logic,params_opt,{},algo_params,stat_params,init_params)  
                r=r.append(r21)
            
        return r,elab
        
    def walkAway(self,f,shapedata,logic,params,histdata={},grid=(),nset=7,optm=True,W=4,algo_params={'Type':'VOID'},stat_params={'freq':'D'},init_params={'freq':'D'}):
        r=pd.DataFrame()
        elab={}
        i=1
        if optm:
            if stat_params['OPTM']=='SINGLE':
                lblm=stat_params['OPMS']
            elif stat_params['OPTM']=='RATIO':
                lblm=stat_params['OPMS1']+'/'+stat_params['OPMS2']
                
            for k in range(0,int(nset/2)):
                if DEBUG_PRINT>0:
                    e_i=1+k*2 
                    desc='Elaboration '+ str(e_i)+'/'+str(nset)+' on interval:' 
                    desc=desc+str(histdata[str(i)]['sdate']+' to '+histdata[str(i)]['edate']) + '(Optimizing params)'
                    print(desc)
                ret,elab[str(i)]=self.optimizer(shapedata,logic,grid,params,histdata,i,silent=True,W=W,algo_params=algo_params,stat_params=stat_params,init_params=init_params)
                elab[str(i)][lblm]=elab[str(i)][lblm].round(2)
                elmax=elab[str(i)][elab[str(i)][lblm]==elab[str(i)][lblm].max()]
                params_opt=(elmax.iloc[0:1,:-1].values)[0]
                r1i=f(histdata,i,shapedata,logic,params_opt,{},algo_params,stat_params,init_params) 
                r=r.append(r1i)
                if DEBUG_PRINT>0: 
                    desc='Elaboration '+ str(e_i+1)+'/'+str(nset)+' on interval:' 
                    desc=desc+str(histdata[str(i+1)]['sdate']+' to '+histdata[str(i+1)]['edate']) + '(Using optimized params)'
                    print(desc)
                r2i=f(histdata,i+1,shapedata,logic,params_opt,{},algo_params,stat_params,init_params) 
                r=r.append(r2i)
                i=i+2
        else:
            for k in range(1,nset+1):
                if DEBUG_PRINT>0: 
                    desc='Elaboration '+ str(k)+'/'+str(nset)+' on interval:' 
                    desc=desc+str(histdata[str(i)]['sdate']+' to '+histdata[str(i)]['edate']) + '(No optimization requested)'
                    print(desc)
                params_opt=params
                r1i=f(histdata,i,shapedata,logic,params_opt,{},algo_params,stat_params,init_params) 
                r=r.append(r1i)
                i=i+1            

        return r,elab
            
    def backtester(self,setupData, logic,params=[35,1,30,1], grid=(),btype='waway',n_bt=6,pct_oos=0.1,stats_type=1,optm=True,W=4,algo_params={'Type':'VOID'},stat_params={'freq':'D'},init_params={'freq':'D'}):
        
        ret=[]
        hist_bkt={}
        #stats_type=1 or two
        
        #Define the function for Backtesting
        def f(hist,k,shapeData,Tradinglogic,params,add_par={},algo_params={'Type':'VOID'},stat_params={'freq':'D'},init_params={'freq':'D'}):
            dt_start,dt_end=hist[str(k)]['sdate'],hist[str(k)]['edate']
            tickers=list(hist[str(k)].keys())[2:]
            data={}
            i=0 #Runs from i=0; the index i refers to the raw data structure feeded into shapedata
            for tickr in tickers:
                data[str(i)]=hist[str(k)][tickr]    
                i=i+1
            #b=shapeData(dt_start,dt_end,data,tickers, x,add_par,algo_params,stat_params['freq'])
            #def setupData(dt_start,dt_end,data={},tickers=[],params=[],opt_params={},algo_params={},freq='D'):
            b=shapeData(data,tickers, params,add_par,algo_params,init_params)
            b.run(Tradinglogic)
            lbl=str(dt_start)+' '+str(dt_end)
            #stat_params={'freq':'D','MAR':-0.001,'ConfLev':0.95,'net_comm':False,'Leverage':1.25,'log_rets':False}
            mstats=(b.printSnapshot(type=stat_params['SnapType'],MAR=stat_params['MAR'],ConfLev=stat_params['ConfLev'],net_comm=stat_params['net_comm']))[stats_type]
            mstats.index=[str(lbl)]
            return mstats
        
        if btype=='awaway':
            #Obtain suitable data for walkAway Backtesting: overlapping intervals
            hist_bkt,index=self.createData(self.hist.copy(),self.dt_start,self.dt_end,pct_oos=pct_oos,n_bt=n_bt,bt_type='awaway',sec_list=self.secl,optm=optm)
            ret=self.walkAway(f,setupData,logic,params,hist_bkt,grid,index,optm=optm,W=W,algo_params=algo_params,stat_params=stat_params,init_params=init_params)
        elif btype=='waway':
            #Obtain suitable data for walkAway Backtesting: overlapping intervals
            hist_bkt,index=self.createData(self.hist.copy(),self.dt_start,self.dt_end,pct_oos=pct_oos,n_bt=n_bt,bt_type='waway',sec_list=self.secl,optm=optm)
            ret=self.walkAway(f,setupData,logic,params,hist_bkt,grid,index,optm=optm,W=W,algo_params=algo_params,stat_params=stat_params,init_params=init_params)
        elif btype=='crossv':
            #Obtain suitable data for cross-validation Backtesting
            hist_bkt,index=self.createData(self.hist.copy(),self.dt_start,self.dt_end,pct_oos=pct_oos,n_bt=n_bt,bt_type='seqd',sec_list=self.secl,optm=optm)
            ret=self.crossValidation(f,setupData,logic,params,hist_bkt,grid,index,optm=optm,W=W,algo_params=algo_params,stat_params=stat_params,init_params=init_params)
        elif btype=='seas':
            #Obtain suitable data for cross-validation Backtesting
            #unit=d1+'-'+d2+'-'+unit
            spar=pct_oos.split('#')
            hist_bkt,index=self.createData(self.hist.copy(),spar[0],spar[1],pct_oos=spar[2],n_bt=n_bt,bt_type='seas',sec_list=self.secl,optm=optm)
            ret=self.seasonalValidation(f,setupData,logic,params,hist_bkt,grid,index,optm=optm,W=W,algo_params=algo_params,stat_params=stat_params,init_params=init_params)
            
        return ret, hist_bkt
        
            
        
        
                    