#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone 
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# VectScreener
#
# Module including functions to perform vectorized backtesting and screening
# Based on ideas provided by the team at QuantInsti
#############################################################################################      

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import random
import warnings
import pandas as pd
import numpy as np
import Plib.Utils.Tools as tls
import Plib.Signals.TAnalysis as signalsfarm
import Plib.Utils.Tools as tls
from Plib.Backtester.Backtrader import Buffer

import matplotlib.pyplot as plt
plt.rcParams["figure.figsize"] = (15,5)
plt.rcParams['axes.grid'] = False
import seaborn as sns
sns.set_style("whitegrid", {'axes.grid' : False})

from IPython.core.display import display, HTML
display(HTML("<style>.container { width:100% !important; }</style>"))
from IPython.display import HTML

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

warnings.filterwarnings("once")
#pd.options.mode.chained_assignment = None  # default='warn'

DEBUG_PRINT=0

    
#############################################################################################
# Module VectScreener 
#
# Methods to manipulate VectScreener objects
#############################################################################################          
 
   
########################################################
# Run the Strategy over multiple securities in parallel
########################################################      
def nstocksAnalysis(stocks,hdata,setupData,logic,str_params={},algo_params={},NProc=4):
    import multiprocessing
    from itertools import product
    import time
    
    #stocks=sec
    results =[]
    
    args = [(s,hdata,str_params,algo_params) for s in stocks]
    
    starttime = time.time()
    
    with multiprocessing.Pool(processes=NProc) as pool:
        results = pool.starmap(helper_nstocksAnalysis, args)
        pool.close()

    df=pd.DataFrame()
    dfr=pd.DataFrame()
    for i in range(0,len(results)):
        mdf,s=results[i]
        mdf.columns=[s]
        df[s] = mdf[s].to_numpy(copy=False)
    df.index=mdf.index
    if DEBUG_PRINT==2:
        print('Pseudo-Parallel Execution time: {} seconds'.format(round(time.time() - starttime),2))
    
    return df      
    
def helper_nstocksAnalysis(s,hdata,str_params,algo_params):
    data=hdata[s].copy()
    data.pop('tickers', None)
    #tickers=hdata[s]['tickers']
    data['0']['Date']=data['0'].index
    strg=vecTrader(data['0'], str_params=str_params,algo_params=algo_params,symbol=s)
    strg.run()
    #strg.signal_performance()
    return strg.getPerformance(),s

###################################################################
# Filter and Run the Strategy over multiple securities in parallel
###################################################################      
def nstocksSelector(stocks,hdata,setupData,logic,str_params={},algo_params={},screen_params={},NProc=4):
    import multiprocessing
    from itertools import product
    import time
    
    #stocks=sec
    results =[]
    
    args = [(s,hdata,str_params,algo_params,screen_params) for s in stocks]
    
    starttime = time.time()
    
    with multiprocessing.Pool(processes=NProc) as pool:
        results = pool.starmap(helper_nstocksSelector, args)
        pool.close()

    df=pd.DataFrame()
    dfr=pd.DataFrame()
    for i in range(0,len(results)):
        mdf, s=results[i]
        if s != '':
            mdf.columns=[s]
            df[s] = mdf[s].to_numpy(copy=False)
    if len(df) >0:
        df.index=mdf.index
    if DEBUG_PRINT==2:
        print('Pseudo-Parallel Execution time: {} seconds'.format(round(time.time() - starttime),2))
    
    return df      
    
def helper_nstocksSelector(s,hdata,str_params,algo_params,screen_params):
    data=hdata[s].copy()
    data.pop('tickers', None)
    #tickers=hdata[s]['tickers']
    data['0']['Date']=data['0'].index
    strg=vecTrader(data['0'], str_params=str_params,algo_params=algo_params,symbol=s)
    if strg.preFilters(screen_params):
        strg.run()
        r1=strg.getPerformance()
        r2=strg.getAddPerformance(screen_params)
        r2.columns=[s]
        ret=r1.append(r2)
    else:
        ret=[]
        s=''
    return ret,s

def prepareData(assets='etf', startd='2000-04-10', endd='2022-07-15', freq='D', bck1='scan_etf', download=True, first=5, show=True,tz='America/New_York',indext='SPY',provider='frdata'):
    import pickle
    import Plib.DataFarm.FRData as fr
    import Plib.DataFarm.Kibot as kb
    
    hdata={}
    if download:
        if provider=='frdata':
            tickers=list(fr.getTickers(assets).Tickers.values)
        else:
            tickers=kb.getTickers(tname=assets)
        if first==0: first=len(tickers)
        #tickers=tickers.head(first)
        i=1
        for s in tickers:
            #s=s[0]
            if show: print('Obtaining data for ', s, ' ',i)
            if provider=='frdata':
                df=fr.getData(assets,s,d1=startd,d2=endd,freq=freq,tz=tz)
            else:
                df=kb.get_eod_data(s, dt_start=startd, dt_end=endd, adj=0,tz=tz)
            if indext != '':
                if provider=='frdata':
                    dfi=fr.getData('etf',indext,d1=startd,d2=endd,freq=freq,tz=tz)
                else:
                    dfi=kb.get_eod_data(indext, dt_start=startd, dt_end=endd, adj=0,tz=tz)
                temp={'0':df,'1':dfi}
            else:
                temp={'0':df,'1':0}
            hdata[s]=temp
            i=i+1
        #stocks=[x[0] for x in list(tickers.values)]
        hdata['Tickers']=tickers
        stocks=tickers
        with open(bck1, 'wb') as handle:
            pickle.dump(hdata, handle, protocol=pickle.HIGHEST_PROTOCOL)
    else:
        with open(bck1, 'rb') as handle:
            hdata = pickle.load(handle)
        try:
            stocks=hdata['Tickers']
        except:
            stocks=list(hdata.keys())
        hdata.pop('Tickers', None)
    if show:
        n_ = 2
        print('Asset prices shape', hdata[list(hdata.keys())[0]]['0'].shape)
        display(hdata[list(hdata.keys())[0]]['0'].tail(n_))
        
    return hdata,stocks
    
#############################################################
# Optimize the Strategy over multiple securities in parallel
#############################################################      
#def nstocksOptimizer(stocks,hdata,setupData,logic,param1_range,param2_range,metrics,str_params={},algo_params={},NProc=4):
def nstocksOptimizer(stocks,hdata,setupData,logic,range_idx,ranges,metrics,str_params={},algo_params={},NProc=4):
    import multiprocessing
    from itertools import product
    import time
    
    #stocks=sec
    results =[]
    plist=list(product(*ranges))
    args = [(s,hdata,range_idx,ranges,metrics,str_params,algo_params) for s in stocks]
    #args = [(s,hdata,param1_range,param2_range,metrics,str_params,algo_params) for s in stocks]
    
    starttime = time.time()
    
    with multiprocessing.Pool(processes=NProc) as pool:
        results = pool.starmap(helper_nstocksOptimizer, args)
        pool.close()

    dfr=pd.DataFrame()
    for i in range(0,len(results)):
        ret,s=results[i]
        if len(ret[0]) > 0:
            df=pd.DataFrame(np.zeros(len(metrics)))
            for k in ret[0].keys():
                df[k]=ret[0][k].iloc[ret[1][0],ret[1][1]]
                #df[k]=ret[0][k].loc[param1_range[ret[-2]],param2_range[ret[-1]]]
            del df[0]
    
            df=df.T    
            label=f"{range_idx[ret[1][0]]}, {plist[ret[1][1]]}".replace('(','').replace(')','')
            label=str(s) +' ('+ label + ')'
            #label=str(s) +' ('+ str(param2_range[ret[-1]]) +',' + str(param1_range[ret[-2]]) +')'
            #print(label)
            res=pd.DataFrame(df[0].to_numpy(copy=False), columns=[label])
            res.index=df.index
            if i==0:
                dfr=res
            else:
                dfr[label]=df[0].to_numpy(copy=False)
        
    if DEBUG_PRINT==2:
        print('Pseudo-Parallel Execution time: {} seconds'.format(round(time.time() - starttime),2))
    
    return dfr[dfr.index != 'Signal']      
    
#def helper_nstocksOptimizer(s,hdata,param1_range,param2_range,metrics,str_params,algo_params):
def helper_nstocksOptimizer(s,hdata,range_idx,ranges,metrics,str_params,algo_params):
    data=hdata[s].copy()
    data.pop('tickers', None)
    #tickers=hdata[s]['tickers']
    data['0']['Date']=data['0'].index
    #ret = vecTrader.Performance_matrix(data['0'], param1_range=param1_range,param2_range=param2_range,
    ret = vecTrader.Performance_matrix(data['0'], range_idx=range_idx, ranges=ranges,
                             metrics =metrics, 
                             str_params=str_params,
                             algo_params=algo_params,
                            optimal_sol = True,tables=False)
    return ret,s
         
#############################################################################################
# Class VectScreener 
#
# Class including functions to store data and perform screening
#############################################################################################      
class vecTrader(Buffer):
    ## class attribute: We can know the possible values for moving_average and metrics 
    ## before creating an object of the class 
    moving_average = ("Simple","exponential") 
    metrics = {'Sharpe':'Sharpe Ratio',
               'CAGR':'Compound Average Growth Rate',
               'MDD':'Maximum Drawdown',
               'NHR':'Normalized Hit Ratio',
               'OTS':'Optimal trade size'}
    
    ##################################################
    # Constructor Initializing all shared variables
    ##################################################      
    def __init__(self,data,str_params={},algo_params={},symbol = 'SYM', start = None,end = None ):
        self.buffer=data                                #the dataframe
        self.buffer_name=symbol
        
        self.allocation=str_params['allocation']
        self.str_params=str_params
        self.algo_params=algo_params
        
        self.n_days = (self.buffer.Date.iloc[-1] - self.buffer.Date.iloc[0])
        
        self.algo_logic=str_params['signals'].__get__(self, vecTrader)
        self.shape_data=str_params['data']
        
        self.buffer=self.shape_data(self.buffer,str_params)
        self.buffer_blinders[self.buffer_name]=[]
        
        self.buffer['yr'] = self.buffer['Date'].dt.year
        super(vecTrader, self).finalizeData(self.allocation,algo_params)
    
    ##################################################
    # Static method to shape data for the strategy
    ##################################################      
    @staticmethod
    def shape_data():
        return
    
    ##################################################
    # Method generating trading signal
    ##################################################      
    def algo_logic(self,x,hdata):
        return
        
    ##################################################
    # Method to start the strategy
    ##################################################      
    def run(self):
        x=self.buffer
        hdata=x[x.columns.intersection(self.hcols)] 
        
        #Run trade logic
        x=self.algo_logic(x,hdata,self.algo_params)
        self.running_copy=x
        self.signal_performance()
        
        #Comptibility rewrites
        self.running_copy['Account']=self.running_copy['Portfolio Value']
        self.running_copy['Account'][0]=self.allocation
        self.running_copy[self.buffer_name+'_sh']=abs(self.running_copy.Traded.fillna(0))
        self.running_copy[self.buffer_name+'_signal']=self.running_copy.Traded.fillna(0)
        self.running_copy[self.buffer_name+'_pr']=abs(self.running_copy.Traded.fillna(0))*x['Close']
        
    
    ##################################################
    # Displays strategy outcomes
    ##################################################      
    def chartSignals(self, idx=['Date'], cols=['SMA','LMA'],params=[10,100],fs=(12,8)):
        ## display chart
        fig = plt.figure(figsize=fs)
        
        for i in range(0,len(cols)):
            plt.plot(self.buffer[idx[0]],self.buffer[cols[i]],label=cols[i]+'('+str(params[i])+')')
        
        plt.title('%s Strategy Backtest'%(self.buffer_name))
        plt.legend(loc=0)
        d_color = {}
        d_color[1] = '#90ee90'  ## light green
        d_color[-1] = "#ffcccb" ## light red

        j = 0
        for i in range(1,self.buffer.shape[0]):
            if np.isnan(self.buffer.Traded[i-1]):
                j=i
            elif (self.buffer.Traded[i-1] == self.buffer.Traded[i]) and (i< (self.buffer.shape[0]-1)):
                continue
            else:
                plt.axvspan(self.buffer['Date'][j], self.buffer['Date'][i], 
                           alpha=0.5, color=d_color[self.buffer.Traded[i-1]], label="interval")
                j = i
                
        plt.show()

    ##################################################
    # Extract strategy performance
    ##################################################      
    def signal_performance(self):

        #if self.long_window <= self.short_window: return
        #self.allocation=allocation
        #creating returns and portfolio value series
        self.buffer['Return']=np.log(self.buffer['Close']/self.buffer['Close'].shift(1))
        self.buffer['S_Return']=self.buffer['Traded'].shift(1)*self.buffer['Return']
        self.buffer['Market_Return']=self.buffer['Return'].expanding().sum()
        self.buffer['Strategy_Return']=self.buffer['S_Return'].expanding().sum()
        self.buffer['Portfolio Value']=((self.buffer['Strategy_Return']+1)*self.allocation)
        self.buffer['Wins']=np.where(self.buffer['S_Return'] > 0,1,0)
        self.buffer['Losses']=np.where(self.buffer['S_Return']<0,1,0)
        
        ## Daywise Performance
        d_perform = {}
        d_perform['TotalWins']=self.buffer['Wins'].sum()
        d_perform['TotalLosses']=self.buffer['Losses'].sum()
        d_perform['TotalTrades']=d_perform['TotalWins']+d_perform['TotalLosses']
        if d_perform['TotalTrades'] != 0:
            d_perform['HitRatio']=round(d_perform['TotalWins']/d_perform['TotalTrades'],2)
        else:
            d_perform['HitRatio']=0
        d_perform['SharpeRatio'] = self.buffer["S_Return"].mean() / self.buffer["S_Return"].std() * (252**.5)        
        
        if self.buffer['Portfolio Value'].iloc[-1] <= 0:
            d_perform['CAGR'] = np.NaN
        else:
            d_perform['CAGR'] = (1+self.buffer['Strategy_Return'].fillna(0)).iloc[-1]**(365.25/self.n_days.days) -1
        d_perform['MaxDrawdown']=(1.0-self.buffer['Portfolio Value']/self.buffer['Portfolio Value'].cummax()).max()
        self.daywise_performance = pd.Series(d_perform)

        ## Tradewise performance
        mask = (self.buffer.Traded != self.buffer.Traded.shift(1)) & (self.buffer.Traded.notnull())
        self.buffer['trade_num'] = np.where(mask,1,0).cumsum()
        _df = self.buffer.groupby(['Traded',"trade_num"]).S_Return.sum().reset_index()
        _df['Wins']=np.where(_df['S_Return'] > 0,1,0)
        _df['Losses']=np.where(_df['S_Return']<0,1,0)
        d_tp = {}
        d_tp.update(_df[["Wins","Losses"]].sum().rename({'Wins':'TotalWins','Losses':'TotalLosses'}).to_dict())
        d_tp['TotalTrades'] = d_tp["TotalWins"] + d_tp["TotalLosses"]
        if d_tp['TotalTrades'] !=0:
            d_tp['HitRatio'] =  np.round(d_tp["TotalWins"] / d_tp['TotalTrades'],4)
        else:
            d_tp['HitRatio'] =0
        d_tp['AvgWinRet'] = np.round(_df[_df.Wins==1].S_Return.mean(),4)
        d_tp['AvgLossRet'] = np.round(_df[_df.Losses==1].S_Return.mean(),4)
        d_tp['WinByLossRet'] = np.round(abs(d_tp['AvgWinRet']/d_tp['AvgLossRet']),2)
        d_tp['StdRets'] = np.round(_df.S_Return.std(),4)
        _sum = _df.groupby("Wins").S_Return.sum()
        if d_tp['HitRatio'] !=0:
            d_tp['NormHitRatio'] = np.round(_sum[1]/_sum.abs().sum(),4)
        else:
            d_tp['NormHitRatio'] =0
        d_tp['OptimalTradeSize'] = self.kelly(p = d_tp['HitRatio'], b = d_tp['WinByLossRet'])
        self.tradewise_performance = pd.Series(d_tp)

    def getPerformance(self):
        self.signal_performance()
        #yearly performance
        self.yearly_performance()
        
        df1=pd.DataFrame(self.tradewise_performance)
        df1.columns=[self.buffer_name]
        df1T=df1.T
        df1T=df1T[['TotalTrades','TotalWins', 'TotalLosses', 'HitRatio', 'AvgWinRet',
               'AvgLossRet', 'WinByLossRet', 'StdRets', 'NormHitRatio',
               'OptimalTradeSize']]

        df2=pd.DataFrame(self.daywise_performance)
        df2.columns=[self.buffer_name]
        df2T=df2.T
        df2T=df2T[['CAGR','SharpeRatio', 'MaxDrawdown']]

        df3=pd.concat([df1T, df2T]).ffill().bfill()[:1]
        df3=df3[['CAGR','SharpeRatio', 'MaxDrawdown','TotalTrades','TotalWins', 'TotalLosses', 'HitRatio', 'AvgWinRet',
               'AvgLossRet', 'WinByLossRet', 'StdRets', 'NormHitRatio',
               'OptimalTradeSize']]
        return df3.T
    
    @staticmethod
    def kelly(p,b):
        try:
            kellyk = np.round(p - (1-p)/b,4)
        except:
            kellyk = -1
        return kellyk
    
    def getAddPerformance(self,screen_params={}):
        res=signalsfarm.getZigZagStats(self.buffer, lbl=screen_params['lbl'], perc=screen_params['perc'])
        highs = signalsfarm.getHighs(self.buffer, screen_params['lbl'], periods=screen_params['h_periods'], last=screen_params['h_last'])
        trn = signalsfarm.tradeRoundNumbers(self.buffer, perc = 0.1, fract = 2, side = 'h')
        dv = signalsfarm.getDVolume(self.buffer, screen_params['lbl'], lbl2=screen_params['lbl2'], periods=screen_params['h_periods'], 		treshold=screen_params['vol_tr'])
        data = [['Number of Highs', highs], ['Trade Round Numbers', trn], ['Dollar Volume', dv]]
        ftrs = pd.DataFrame(data, columns = ['','Stats'])
        ftrs = ftrs.set_index('')
        return res.append(ftrs)
        
    ##################################################
    # Filter Issues
    ##################################################     
    def preFilters(self, screen_params={}):
        weights=screen_params['weights']
        res = signalsfarm.getTradedMonths(self.buffer, min_months = screen_params['min_tmonths'])*weights[0]
        res = res + signalsfarm.getMinOverTime(self.buffer, lbl=screen_params['lbl'],periods=screen_params['min_max_overtime'])*weights[1]
        res = res + signalsfarm.getMinOverLastYear(self.buffer, lbl=screen_params['lbl'],periods=screen_params['min_max_overtime'])*weights[2]
        res = res + signalsfarm.getMaxOverLastYear(self.buffer, lbl=screen_params['lbl'],periods=screen_params['min_max_overtime'])*weights[3]
        return res >= screen_params['treshold']
     
    ##################################################
    # Plot strategy results
    ##################################################      
    def plot_performance(self, allocation=0,fs=(12,8)):
        #intializing a variable for initial allocation
        #to be used to create equity curve
        if allocation != 0:
            self.allocation=allocation
        self.signal_performance()
        
        #yearly performance
        self.yearly_performance()
        
        fig = plt.figure(figsize=fs)
        #Plotting the Performance of the strategy
        plt.plot(self.buffer['Date'],self.buffer['Market_Return'],color='black', label='Market Returns')
        plt.plot(self.buffer['Date'],self.buffer['Strategy_Return'],color='blue', label= 'Strategy Returns')
        plt.title('%s Strategy Backtest'%(self.buffer_name))
        plt.legend(loc=0)
        
        #plt.show()

        fig = plt.figure(figsize=fs)
        plt.plot(self.buffer['Date'],self.buffer['Portfolio Value'], color='blue')
        plt.title('%s Portfolio Value'%(self.buffer_name))
        
        plt.tight_layout()
        plt.show()
    
    ##################################################
    # Table out strategy yearly performance
    ##################################################      
    def yearly_performance(self):
        _yearly_df = self.buffer.groupby(['yr','Traded']).S_Return.sum().unstack()
        _yearly_df.rename(columns={-1.0:'Sell',1.0:'Buy'}, inplace=True)
        _yearly_df['Return'] = _yearly_df.sum(1)

        # yearly_df
        self.yearly_df = _yearly_df.style.bar(color=["#ffcccb",'#90ee90'], align = 'zero').format({
            'Sell': '{:,.2%}'.format,'Buy': '{:,.2%}'.format,'Return': '{:,.2%}'.format})
    
    def update_metrics(self):
        d_field = {}
        #if self.long_window > self.short_window:
        d_field['Sharpe'] = self.daywise_performance.SharpeRatio
        d_field['CAGR'] = self.daywise_performance.CAGR
        d_field['MDD'] = self.daywise_performance.MaxDrawdown
        d_field['NHR'] = self.tradewise_performance.NormHitRatio
        d_field['OTS'] = self.tradewise_performance.OptimalTradeSize

        return d_field
    
    ##################################################
    # Optimization Matrix
    ##################################################      
    @classmethod
    def Performance_matrix(cls,data, range_idx, ranges=[], metrics=[], str_params={}, algo_params={}, data_params={}, optimal_sol = True, tables=True):
    #def Performance_matrix(cls,data, param1_range, param2_range, metrics, str_params={}, algo_params={}, data_params={}, optimal_sol = True, tables=True):
        import itertools
        
        c_green = sns.light_palette("green", as_cmap=True)
        c_red = sns.light_palette("red", as_cmap=True)
        
        d_mats = {m:[] for m in metrics}
        plist=list(itertools.product(*ranges))

        for l in range_idx:
            d_row = {m:[] for m in metrics}
            for pt in plist:
                str_params[str_params['params'][0]]=l
                for i in range(1, len(pt)):
                    str_params[str_params['params'][i]]=pt[i]
                obj = cls(data,str_params,algo_params,'STOCK') ## object being created from the class
                obj.run()
                d_field = obj.update_metrics()
                for m in metrics: d_row[m].append(d_field.get(m,np.nan))
            for m in metrics: d_mats[m].append(d_row[m])

        try:
            d_df = {m:pd.DataFrame(d_mats[m], index=range_idx, columns=plist) for m in metrics}
        except:
            d_df = pd.DataFrame()
            
        #d_mats = {m:[] for m in metrics}
        #for l in param1_range:
        #    d_row = {m:[] for m in metrics}
        #    for s in param2_range:
        #        str_params['shortw']=s
        #        str_params['longw']=l
        #        obj = cls(data,str_params,algo_params,'STOCK') ## object being created from the class
        #        obj.run()
        #        #obj.signal_performance()
        #        d_field = obj.update_metrics()
        #        print(d_field)
        #        for m in metrics: d_row[m].append(d_field.get(m,np.nan))
        #        print(d_row)
        #    for m in metrics: d_mats[m].append(d_row[m])
        #    print(d_mats)
        #    
        #d_df = {m:pd.DataFrame(d_mats[m], index=param1_range, columns=param2_range) for m in metrics}

        def optimal(_df):
            _df = _df.stack().rank()
            _df = (_df - _df.mean())/_df.std()
            return _df.unstack()

        if optimal_sol:
            if len(d_df) > 0:
                d_df['Signal'] = 0
                if 'Sharpe' in metrics: d_df['Signal'] += 2*optimal(d_df['Sharpe'])
                if 'NHR' in metrics: d_df['Signal'] += optimal(d_df['NHR'])
                if 'CAGR' in metrics: d_df['Signal'] += optimal(d_df['CAGR'])
                if 'MDD' in metrics: d_df['Signal'] -= 2*optimal(d_df['MDD'])
            
                #s_i,l_i = np.unravel_index(np.argmax(d_df['Signal'].to_numpy(copy=False)), d_df['Signal'].shape)
                if len(d_df['Signal']) > 0:
                    sol = np.unravel_index(np.argmax(d_df['Signal'].to_numpy(copy=False)), d_df['Signal'].shape)
                    metrics.insert(0,'Signal')
                else:
                    sol=(0,0)
                    d_df = pd.DataFrame()
            else:
                sol=(0,0)
                d_df = pd.DataFrame()
            
        if tables:
            print(f"Optimal solution: {range_idx[sol[0]]}, {plist[sol[1]]}".replace('(','').replace(')',''))
            for m in metrics:
                display(HTML(d_df[m].style.background_gradient(axis=None, cmap=
                    c_red if m=="MDD" else c_green).format(
                    ("{:,.2}" if m in ["Sharpe","Signal"] else "{:.2%}")).set_caption(m).render()))
        
        return d_df,sol
        #return d_df,l_i,s_i


