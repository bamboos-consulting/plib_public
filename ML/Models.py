#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone 
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# ML
#
# Module including classes and functions to implement Machine Learning Algos
# Based on ideas provided by the team at QuantInsti
#############################################################################################      

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import warnings
import pandas as pd
import numpy as np
import Plib.Signals.TAnalysis as ta
import Plib.Signals.Filters as flt
warnings.filterwarnings("ignore")

#when changing data on a view or reference python 
#warns that changes are not saved to the original
#pd.options.mode.chained_assignment = None  # default='warn'

#Intel(R) Extension for Scikit-learn dynamically patches scikit-learn estimators to use oneDAL as the underlying solver
#from daal4py.sklearn import patch_sklearn
from sklearnex import patch_sklearn
patch_sklearn()

DEBUG_PRINT=0

####################################################################
# Class including encoding, scaling, splitting, and filter  methods
####################################################################      
class dataProfiler():
    
    ########################################################
    # Encoders
    ########################################################      
    @staticmethod
    def encoder_avgfreq(df,lbl,lbl_mean):
        df[lbl+'_count'] = df[lbl].apply(lambda x: df[lbl].value_counts()[x])
        df[lbl+'_mean'] = df[lbl].apply(lambda x: df.groupby(lbl)[lbl_mean].mean()[x])
        return df

    @staticmethod
    def encoder_ordinal(df,lbl,mapping={'Small': 0,'Medium': 1,'High': 2}):
        df[lbl] = df[lbl].map(mapping)
        return df
    
    @staticmethod
    def make_dense(df,lbl,params={'threshold':0,'max_dummies':10,'lbl_other':'other'}):
        max_dummies=params['max_dummies']
        threshold=params['threshold']
        lbl_other=params['lbl_other']
        
        if threshold==0: threshold=df[lbl].mean()/3
        lbl_other='other'

        if df[lbl].nunique() > max_dummies:
            df[lbl+'_count'] = df[lbl].apply(lambda x: df[lbl].value_counts()[x])
            for i in range(0, len(df)):
                if df[lbl+'_count'][i] < threshold:
                    df[lbl][i] = lbl_other
        return df

    @staticmethod
    def encoder_alphab(df,lbl,reduce=False, params={'threshold':0,'max_dummies':10,'lbl_other':'other'}):
        from sklearn.preprocessing import LabelEncoder
        
        if reduce: df=make_dense(df,lbl,params)
        df[lbl+'_enc']=LabelEncoder().fit_transform(df[lbl])
        return df
    
    @staticmethod
    def encoder_dummies(df,lbl,prefix='',rem_lbl=True,reduce=False, params={'threshold':0,'max_dummies':10,'lbl_other':'other'}):
        if reduce: df=make_dense(df,lbl,params)
        df_dummy=pd.get_dummies(df[lbl],prefix=lbl,drop_first=True)
        df=pd.concat([df,df_dummy],axis=1)
        if rem_lbl: del df[lbl]
        return df
    
    @staticmethod
    def encoder_binning(df,lbl,bins=[0,15,30,90],gnames=['child','young_adult','middle_aged'],suffix='_category'):
        df[lbl+suffix] = pd.cut(df[lbl], bins, labels=group)
        return df
        
    ########################################################
    # Scalers
    ########################################################      
    @staticmethod
    def scaling(df,params={'method':'std','exclude':[]}):
        from sklearn.preprocessing import normalize
        from sklearn.preprocessing import MinMaxScaler
        from sklearn.preprocessing import StandardScaler

        cols=list(set(list(df.columns))-set(params['exclude']))
        bcols=df.columns
        
        if params['method']=='norm':
            df1=dataProfiler.missingv(df)
            df1=pd.DataFrame(normalize(df[[*cols]]),columns=cols)
        if params['method']=='minmax':
            df1= pd.DataFrame(MinMaxScaler().fit_transform(df[[*cols]]),columns=cols)
        if params['method']=='std':
            df1= pd.DataFrame(StandardScaler().fit_transform(df[[*cols]]),columns=cols)
        if len(params['exclude'])>0:
            for s in params['exclude']:
                df1[s]=0
                df1[s]=np.asarray(df[s])
        df1.index=df.index
        df1=df1[bcols]
        return df1

    ########################################################
    # Splitters
    ########################################################      
    @staticmethod
    def splitMatrix(data,y_col):
        predictors = list(set(list(data.columns))-set([y_col]))
        #X = data[predictors].values
        #y = data[target_column].values
        X=np.asarray(data[predictors])
        y=np.asarray(data[y_col])
        return X,y
    
    @staticmethod
    def splitDataset(df,target_lbl,t = 0.85,splits=7,border=0.01, how='ss'):
        # Note: border should be greater than the longest backward 
        # measure (i.e., ema, etc.) to properly purge/embargo
        X,y=dataProfiler.splitMatrix(df,target_lbl)
        cols = list(set(list(df.columns))-set([target_lbl]))
        bd=int(X.shape[0]*border)
    
        xl_train,yl_train,xl_test,yl_test=[],[],[],[]
        if how=='ss':               # simple split
            bd=0
            t=int(len(df)*t)
            xl_train,yl_train=X[:t-bd],y[:t-bd]
            xl_test,yl_test=X[1+t:],y[1+t:]
            #gd_train,gd_test=df[list(cols)].iloc[:t],df[list(cols)].iloc[1+t:]
            return bd,cols,xl_train,yl_train,xl_test,yl_test
        elif how=='awf':            # anchored walkforward
            bd=0
            train_win = int(X.shape[0]/splits)
            test_win = int((1-t)*train_win)
            s1=0
            mylist=[]
            for i in range(0,splits):
                s1=0
                e1=s1+(1+i)*train_win
                s2=e1-(1+i)*test_win
                e2=e1
                e1=s2-1
                xl_train,yl_train=X[s1:e1-bd],y[s1:e1-bd]
                xl_test,yl_test=X[s2:e2],y[s2:e2]
                mylist.append([xl_train,yl_train,xl_test,yl_test])
                return bd,cols,mylist
        elif how=='cv':             # cross validation with border
            test_starts=[(i[0],i[-1]+1) for i in np.array_split(np.arange(X.shape[0]),splits)] 
            #[(0, 34), (34, 67), (67, 100)]
            mylist=[]
            for i in test_starts:
                if i[0]==0: #last
                    s1=i[1]+1+bd
                    s2=X.shape[0]
                    xl_train,yl_train=X[s1:s2],y[s1:s2]
                elif i[1]==X.shape[0]: # first
                    s1=0
                    s2=i[0]-1-bd
                    xl_train,yl_train=X[s1:s2],y[s1:s2]
                else:
                    s1,s2=0,i[0]-1-bd
                    s3,s4=i[1]+1+bd,X.shape[0]
                    xl_train1,yl_train1=X[s1:s2],y[s1:s2]
                    xl_train2,yl_train2=X[s3:s4],y[s3:s4]
                    xl_train,yl_train=xl_train1.append(xl_train2),yl_train1.append(yl_train2)
                xl_test,yl_test=X[i[0]:i[1]],y[i[0]:i[1]]
                mylist.append([xl_train,yl_train,xl_test,yl_test])
                return bd,cols,mylist
        return

    ########################################################
    # Filters specific for ML
    ########################################################   
    @staticmethod   
    def fracDiff(series,d,thres=.01,plot=False): 
        #Increasing width window, with treatment of NaNs 
        #Note 1: For thres=1, nothing is skipped. 
        #Note 2: d can be any positive fractional, not necessarily bounded [0,1]. 
        df=flt.fracDiff(series,d,thres,plot)
        
    @staticmethod   
    def fracDiff_FFD(df,lbl,d=0.35,thres=1e-5,all_series=True,plot=False): 
        #Constant width window (new solution) 
        #Note 1: thres determines the cut-off weight for the window 
        #Note 2: d can be any positive fractional, not necessarily bounded [0,1]. 
        df=flt.fracDiff_FFD(df,lbl,d,thres,all_series,plot)
        
    ########################################################
    # Utils: missing values and putliers detection/removal
    ########################################################      
    @staticmethod
    def missingv(df,how='linear',max_na_inseq=5):
        #{‘linear’, ‘time’, ‘index’, ‘values’, ‘nearest’, ‘zero’, ‘slinear’, 
        #‘quadratic’, ‘cubic’, ‘barycentric’, ‘krogh’, ‘polynomial’, ‘spline’, 
        #‘piecewise_polynomial’,‘from_derivatives’, ‘pchip’, ‘akima’}
        res = df.interpolate(method=how,limit=max_na_inseq)\
        .fillna(method='ffill')\
        .fillna(method='bfill')
        return res
    
    @staticmethod
    def outliers(df,params={'method':'stdev','arg1': 3,'iq':(0.25,0.75)},table=False):
        import numpy as np

        series=df.columns
        for s in series:
            if params['method']=='stdev':
                data_mean,data_std=np.mean(df[s]),np.std(df[s])
                cut_off = data_std * params['arg1']
                lower,upper=(data_mean - cut_off),(data_mean + cut_off)
            elif params['method']=='iqrange':
                q25, q75 = df[s].quantile(params['iq'][0]),df[s].quantile(params['iq'][1])
                iqr = q75 - q25
                cut_off = iqr * params['arg1']
                lower, upper = (q25 - cut_off), (q75 + cut_off)
            outliers=[x for x in df[s] if (x > (upper)) or (x < (lower))]
            if table: print('Series: '+str(s)+' outliers: %d' % len(outliers))
            if table: print(outliers)
            median=df[s].median()
            df[s]=np.where((df[s] > upper), median, df[s])
        return df

#########################################################################
# Class including features analysis, generation, and selection methods
# p-value or R-squared for Regression, F1-score for Classification
#########################################################################      
class mlProfiler(dataProfiler):
    
    rng = None
    rnds = 0
    def __init__(self, rnds=0):
        # to be used for reproducibile results with random_state=self.rng
        # it is preferable to evaluate the cross-validation preformance by 
        # letting the estimator use a different RNG on each fold. This is 
        # done by passing a RandomState instance (or None) to the estimator 
        # initialization. This is also true for any tool that performs model 
        # selection via cross-validation (GridSearchCV / RandomizedSearchCV):
        # scores are not comparable fold-to-fold across different calls to 
        # search.fit, since cv.split would have been called multiple times. 
        # Within a single call to search.fit fold-to-fold comparison is 
        #possible since the search estimator only calls cv.split once.
        if rnds == 0:
            mlProfiler.rng = None
        else:
            mlProfiler.rng = np.random.RandomState(rnds)
            self.rnds=rnds
    
    ########################################################
    # Preliminary Analysis
    ########################################################        
    @staticmethod
    def fs_analysis(df,params={'var_t':0.006,'corr_t':0.6},drop=True,table=True):
        from IPython.display import display_html 
        from sklearn.preprocessing import normalize
        import pandas as pd

        df1=pd.DataFrame(normalize(df),columns=df.columns).describe()
        if drop:
            df1=df1.drop(df[[column for column in df1.columns if df1[column][2] > params['var_t']]], axis=1)
        df1.index = df1.index.rename('LowVar')

        df2=(df.corr(method='spearman').abs()).where(np.triu(np.ones((df.corr(method='spearman').abs()).shape), k=1).astype(np.bool))
        if drop:
            df2=df.drop(df[[column for column in df2.columns if any(df2[column] > params['corr_t'])]], axis=1)
        df2.index = df2.index.rename('LowCorr')

        if table:
            df1_styler = (df1.round(4)).style.set_table_attributes("style='display:inline'").set_caption('Features with Lowest Variance')
            df2_styler = ((df2.describe()).round(4)).style.set_table_attributes("style='display:inline'").set_caption('Features with Lowest Correlation')
            space = "\xa0" * 10
            display_html(df1_styler._repr_html_()+ space + df2_styler._repr_html_(), raw=True)
        
        return df1.round(4),df2
    
    @staticmethod
    def fs_relations(data,metrics=[],target='',fs=(12,12)):
        import matplotlib.pyplot as plt
        import matplotlib.gridspec as gridspec
        from scipy.stats import spearmanr
        
        #metrics = ['volumeGap_','dailyChange_','OD_','OL_']
        #target='Close'
    
        if len(metrics) % 2 != 0: 
            print('Only square plots are allowed. Try whith a even number of metrics...')
            return
    
        columns=2
        rows=int(len(metrics)/2)
    
        df=data.copy()
        t_fsize=int(fs[0]*1.2)
        m_fsize=t_fsize-1
        a_fsize=int(t_fsize*0.6)

        fig = plt.figure(figsize=fs)
        fig.suptitle('Features Analysis with respect to ' + target,fontsize=18)

        outer_grid = gridspec.GridSpec(rows,columns) 

        m=0
        for r in range(0,rows):
            for c in range(0,columns):
                # Attempt to plot only series
                if m < len(metrics):
                    x=df[metrics[m]].values
                    x_name=df[metrics[m]].name
                    y=df[target].values
                    y_name=df[target].name
                    rho, pvalue = spearmanr(x, y)
                    corr1='SpearmanR ' + f'{rho:,.2}'
                    corr2='Pvalue ' + f'{pvalue:.2}'

                    left_cell = outer_grid[r,c]

                    inner_grid = gridspec.GridSpecFromSubplotSpec(3,3, left_cell)

                    # From here we can plot usinginner_grid's SubplotSpecs
                    ax_main = plt.subplot(inner_grid[1:3, :2])
                    ax_xDist = plt.subplot(inner_grid[0, :2],sharex=ax_main)
                    ax_yDist = plt.subplot(inner_grid[1:3, 2],sharey=ax_main)

                    ax_main.scatter(x,y,marker='.')
                    #ax_main.set(xlabel=x_name, ylabel=y_name)
                    ax_main.set(ylabel=y_name)
                    ax_main.annotate(corr1+'\n'+corr2, (1.2, 1.2),
                           xycoords='axes fraction',fontsize=a_fsize)

                    ax_xDist.hist(x,bins=100,align='mid')
                    ax_xDist.set(ylabel='count')
                    ax_xCumDist = ax_xDist.twinx()
                    ax_xCumDist.hist(x,bins=100,cumulative=True,histtype='step',density=True,color='r',align='mid')
                    ax_xCumDist.tick_params('y', colors='r')
                    ax_xCumDist.set_ylabel('cumulative',color='r')
                    ax_xCumDist.set_title(x_name, fontsize=m_fsize)

                    ax_yDist.hist(y,bins=100,orientation='horizontal',align='mid')
                    ax_yDist.set(xlabel='count')
                    ax_yCumDist = ax_yDist.twiny()
                    ax_yCumDist.hist(y,bins=100,cumulative=True,histtype='step',density=True,color='r',align='mid',orientation='horizontal')
                    ax_yCumDist.tick_params('x', colors='r')
                    ax_yCumDist.set_xlabel('cumulative',color='r')

                    m=m+1
        outer_grid.tight_layout(fig, rect=[0, 0.03, 1, 0.95])  
        plt.show()
        
    @staticmethod
    def fs_mkdatetime(df,lbl,merge=False):
        new_df = pd.DataFrame({"year": df[lbl].dt.year,
                  "month": df[lbl].dt.month,
                  "day": df[lbl].dt.day,
                  "hour": df[lbl].dt.hour,
                  "dayofyear": df[lbl].dt.dayofyear,
                  "week": df[lbl].dt.week,
                  "dayofweek": df[lbl].dt.dayofweek,
                  "dayofweekname": df[lbl].dt.weekday_name,
                  "quarter": df[lbl].dt.quarter,
                 })
        new_df['is_weekday']=0 
        for i in range(0, len(new_df)):
            if ((new_df['dayofweek'][i] == 5) | (new_df['dayofweek'][i] == 6)):
                new_df['is_weekday'][i] = 0
            else: 
                new_df['is_weekday'][i] = 1
        if merge: new_df=pd.concat([df,new_df],axis=1)
        return new_df

    @staticmethod
    def fs_lda(df,features,rg1,components=4,test_periods=252,nsplit=5):
        from sklearn.preprocessing import LabelEncoder
        from sklearn.model_selection import GridSearchCV,cross_val_score
        from sklearn.model_selection import StratifiedShuffleSplit
        from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
        from matplotlib import pyplot
        from sklearn.pipeline import Pipeline
        from sklearn.naive_bayes import GaussianNB
    
        df=df[[rg1]+features][:-test_periods]
        X = df.drop([rg1], axis=1)
        le = LabelEncoder()
        y = le.fit_transform(df[rg1])

        mresults,results, names = list(),list(), list()
        models = dict()
        mmax=0
        for i in range(1,components+1):
            steps = [('lda', LinearDiscriminantAnalysis(n_components=i,solver='eigen',shrinkage='auto')), ('m', GaussianNB())]
            models[str(i)] = Pipeline(steps=steps)
            cv = StratifiedShuffleSplit(n_splits=nsplit)
            scores = cross_val_score(models[str(i)], X, y, scoring='accuracy', cv=cv, n_jobs=-1, error_score='raise')
            results.append(scores)
            names.append(str(i))
            mmean=np.round(np.mean(scores),4)
            mresults.append(mmean)
            mstd=np.round(np.std(scores),4)
            if mmean > mmax: mmax=i
            print('Model ',i,'Mean:',mmean,'Std:',mstd)

        # plot model performance for comparison
        pyplot.boxplot(results, labels=names, showmeans=True)
        pyplot.show()

        model = LinearDiscriminantAnalysis(n_components=mmax,solver='eigen')
        cv = StratifiedShuffleSplit(n_splits=nsplit)
        # define grid
        grid = dict()
        grid['shrinkage'] = np.arange(0, 1, 0.01)
        # define search
        search = GridSearchCV(model, grid, scoring='accuracy', cv=cv, n_jobs=-1)
        # perform the search
        gresults = search.fit(X, y)
        # summarize
        print('Model ',1+mresults.index(max(mresults) ) )
        print('Mean Accuracy: %.3f' % gresults.best_score_)
        print('Config: %s' % gresults.best_params_)
    
    ########################################################
    # Targets Generation
    ########################################################        
    @staticmethod
    def fs_mkforward(df,lbl_i,fwd_periods=[1,3,21]):
        for i in fwd_periods:
            df['tgt_'+lbl_i+'_forward'+str(i)]=df[lbl_i].shift(i)
        return df
    
    @staticmethod  
    def fs_mkbounded(df, lbl='Close', lbl_z='Vol', hold_days={'weeks':0,'days':10,'hours':0,'minutes':0}, z=[2,-2],upper_label=1, lower_label=-1,log_scale=True):
    
        if log_scale:
            series = np.log(df[lbl])
        else:
            series=df[lbl]
        event_index=df.index
        barrier=df[lbl_z]
        timedelta = pd.Timedelta(**hold_days)
    
        labels = list()
        label_dates = list()
        for event_date in event_index:
            date_barrier = event_date + timedelta
        
            start_price = series.loc[event_date]
            trade_window = series.loc[event_date:date_barrier] - start_price
            trades: List[Tuple[int, pd.Timestamp]] = list()
            if z[0]:
                upper_barrier = z[0] * barrier[event_date]
                _date = trade_window[trade_window > upper_barrier].first_valid_index()
                if _date:
                    trades.append((upper_label, _date))
            if z[1]:
                lower_barrier = z[1] * barrier[event_date]
                _date = trade_window[trade_window < lower_barrier].first_valid_index()
                if _date:
                    trades.append((lower_label, _date))
            if trades:
                label, label_date = min(trades, key=lambda x: x[1])
            else:
                label, label_date = 0, date_barrier
            labels.append(label)
            label_dates.append(label_date)
        label_series = pd.Series(labels, index=event_index)
        event_spans = pd.Series(label_dates, index=event_index)
        return pd.DataFrame(label_series,index=event_spans,columns=['barrier'])

    ########################################################
    # Filter methods
    ########################################################         
    @staticmethod
    def fs_anova(df,features,rg1,test_periods=252,tsplit=0.8,plot=True):
        from sklearn.feature_selection import f_classif, SelectKBest
        from sklearn.model_selection import RepeatedStratifiedKFold,StratifiedKFold, GridSearchCV
        from sklearn.pipeline import Pipeline
        from sklearn.linear_model import LogisticRegression
        from sklearn.model_selection import train_test_split
        
        df=df[[rg1]+features][:-test_periods]
        X = np.asarray(df[features])
        y = np.asarray(df[rg1])
        
        X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=tsplit)
        
        # Univariate feature selection with F-test for feature scoring
        fs = SelectKBest(score_func=f_classif, k='all')
        fs.fit(X_train, y_train)
        # transform train/test input data
        X_train_fs = fs.transform(X_train)
        X_valid_fs = fs.transform(X_valid)
        importances = np.asarray(fs.scores_)
        fi=mlProfiler.fs_fimp(importances,df,X,features,plot)
        return fi
    
    @staticmethod
    def fs_chi2(df,features,tocat_features,rg1,test_periods=252,plot=True):
        from sklearn.feature_selection import chi2, SelectKBest
        from sklearn.preprocessing import LabelEncoder
    
        df=df[[rg1]+features][:-test_periods]
        X = np.asarray(df[features])
        y = np.asarray(df[rg1])
        
        y = LabelEncoder().fit_transform(df[rg1])
        for f in tocat_features:    #Transform the selected features into categorical feature
            df[f] = df[f].astype('object')
        df.dropna(inplace = True)
        #Retrieve all the categorical columns except the target
        categorical_columns = df[features].select_dtypes(exclude='number').columns
        if len(categorical_columns)>0:
            for c in categorical_columns:
                df[c]=LabelEncoder().fit_transform(df[c])
    
        fs = SelectKBest(score_func=chi2, k=len(features))
        X_kbest = fs.fit_transform(X, y)
        result=fs.scores_
        importances = np.asarray(fs.scores_)
        fi=mlProfiler.fs_fimp(importances,df,X,features,plot)
        return fi
                
    ########################################################
    # Non-Linear Relationships
    ########################################################      
    @staticmethod
    def fs_minform(df,features,rg1,test_periods=252,plot=True,imodel='r'):
        from sklearn.feature_selection import mutual_info_regression, mutual_info_classif
        
        df=df[[rg1]+features][:-test_periods]
        X = np.asarray(df[features])
        y = np.asarray(df[rg1])

        high_score_features = []
        if imodel=='c':
            feature_scores = mutual_info_classif(X, y, random_state=0)
        else:
            feature_scores = mutual_info_regression(X, y, random_state=0)
        #for score, f_name in sorted(zip(feature_scores, df[features].columns), reverse=True)[:top_feat]:
        #    high_score_features.append(f_name)
        #result = list(df[high_score_features].columns)
        importances = np.asarray(feature_scores)
        fi=mlProfiler.fs_fimp(importances,df,X,features,plot)
        return fi
    
    ########################################################
    # Embedded methods
    ########################################################      
    @staticmethod
    def fs_lasso(df,features,rg1,test_periods=252,split=0.85,plot=True,imodel='r'):
        from sklearn.linear_model import LogisticRegression
        #from sklearn.feature_selection import SelectFromModel
        import Plib.Stats.Regression as r
        from matplotlib import pyplot
    
        df=df[[*([rg1]+features)]][:-test_periods]
        X = np.asarray(df[features])
        y = np.asarray(df[rg1])
        
        if imodel=='r':
            glasso=r.gridSearch(df,rg1,r.lassoReg_GS)
            model_name=glasso.model.steps[1][0]
            model=glasso.model.steps[1][1]
            cols=df.columns
            l=list([])
            importance=model.coef_
            for i in range(0,len(model.coef_.round(4))):
                if model.coef_[i].round(4)>0:
                    l.append(cols[i])
            result= l,glasso.result
            cols=list([''])+list(df.columns[1:])
        else:
            logistic = LogisticRegression(C=1, penalty='l1',solver='liblinear').fit(X,y)
            importance = logistic.coef_[0]
            result=pd.DataFrame(importance,index=df.columns[1:])
            cols=df.columns[1:]
        fi=mlProfiler.fs_fimp(importance,df,X,features,plot)
        return fi
    
    @staticmethod
    def fs_randF(df,features,rg1,test_periods=252,threshold=0.8,test_size=0.8,plot=True,imodel='c'):
        from sklearn.datasets import load_breast_cancer
        from sklearn.model_selection import train_test_split
        from sklearn.ensemble import RandomForestClassifier
        from sklearn.ensemble import RandomForestRegressor
        from sklearn.feature_selection import SelectFromModel
        from sklearn.metrics import classification_report
        

        df=df[[rg1]+features][:-test_periods]
        X = np.asarray(df[features])
        y = np.asarray(df[rg1])
    
        X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=test_size)
        
        if imodel=='c':
            rf = RandomForestClassifier(n_estimators = 100, class_weight='balanced')
            rf.fit(X_train, y_train)
        else:
            rf = RandomForestRegressor(n_estimators = 100)
            rf.fit(X_train, y_train)
        importances = rf.feature_importances_
        fi=mlProfiler.fs_fimp(importances,df,X,features,plot)
        if plot:
            sfm = SelectFromModel(rf, threshold=threshold)
            sfm.fit(X_train, y_train)
            X_important_train = sfm.transform(X_train)
            X_important_test = sfm.transform(X_valid)
            if imodel=='c':
                try:
                    rf = RandomForestClassifier(n_estimators = 100, class_weight='balanced')
                    rf.fit(X_important_train, y_train)
                    y_pred = rf.predict(X_important_test)
                    print(classification_report(y_valid, y_pred))
                except:
                    print('Not enough classes found...')
                    pass
        return fi
    
    ########################################################
    # Wrapper methods
    ########################################################          
    @staticmethod
    def fs_combin(df,min_features,rg1,add_features,model,params,imeasure='mse',split=0.85,test_periods=252):
        from itertools import combinations

        combinations = sum([list(map(list, combinations(add_features, i))) for i in range(len(add_features) + 1)], [])
        
        df=df[:-test_periods]
        
        i=0
        x={}
        for c in combinations:
            dfk=df[[rg1]+min_features+c]
            #print('Iteration: ',i,len(combinations),' with ',params)
            stats_mod=model(dfk,rg1,split,*params) 
            i=i+1
            x[i]=stats_mod[imeasure] 
        result= {k: v for k, v in sorted(x.items(), key=lambda item: item[1])}
        i=int(list(result.keys())[0])
        return min_features+combinations[i-1]
    
    @staticmethod
    def fs_fwdSel(model,params,df, features, rg1, threshold=0.05,test_periods=252,split=0.85,imeasure='pvalues'):
        
        df=df[:-test_periods]
        initial_features = features
        best_features = []

        while (len(initial_features)>0):
            remaining_features = list(set(initial_features)-set(best_features))
            new_meas = pd.Series(index=remaining_features)
            for new_column in remaining_features:
                stats_mod=model(df[[rg1]+best_features+[new_column]],rg1,split,*params) 
                if imeasure=='pvalues':
                    new_meas[new_column] = stats_mod['pvalues']['Probabilities'].iloc[-1]
            min_meas = new_meas.min()
            if(min_meas<threshold):
                best_features.append(new_meas.idxmin())
            else:
                break
        return best_features

    @staticmethod
    def fs_bwdSel(model,params,df, features, rg1, threshold=0.05,test_periods=252,split=0.85,imeasure='pvalues'):

        df=df[:-test_periods]
        while(len(features)>0):
            stats_mod=model(df[[rg1]+features],rg1,split,*params) 
            if imeasure=='pvalues':
                new_meas=stats_mod['pvalues']['Probabilities'][1:] #exclude intercept for regression
            max_meas = new_meas.max()
            if(max_meas >= threshold):
                excluded_feature =np.argmax(new_meas, axis=0) #p_values.idxmax()
                features.remove(features[excluded_feature-1])
            else:
                break 
        return features
    
    @staticmethod
    def fs_ga(df,features,rg1,model,scoring,test_periods=252,max_iter=10000,C=10,plot=True,verbose=2):
        from genetic_selection import GeneticSelectionCV
        from sklearn.model_selection import StratifiedShuffleSplit, train_test_split
        from sklearn.metrics import classification_report
        import warnings
        warnings.filterwarnings("ignore")

        df=df[[rg1]+features][:-test_periods]
        X = np.asarray(df[features])
        y = np.asarray(df[rg1])

        scv = StratifiedShuffleSplit(n_splits=5)
        selector = GeneticSelectionCV(model,
                                      cv=scv,
                                      verbose=verbose,
                                      scoring=scoring,
                                      max_features=5,
                                      n_population=50,    #120
                                      crossover_proba=0.5,
                                      mutation_proba=0.2,
                                      n_generations=40,
                                      crossover_independent_proba=0.5,
                                      mutation_independent_proba=0.05,
                                      tournament_size=3,
                                      n_gen_no_change=10,
                                      caching=True,
                                      n_jobs=-1)
        selector = selector.fit(X, y)
        # get the selected features
        dff=df[features]
        dff=dff.loc[:, selector.support_]
        cols = list(dff.columns)
        X = np.asarray(df[cols])
        y = np.asarray(df[rg1])
        X_train, X_valid, y_train, y_valid = train_test_split(X, 
                                                            y, 
                                                            test_size=0.2,
                                                            stratify=y) ######### past features will be relevant in future? Assume Yes=>y

        # train and test
        model.fit(X_train, y_train)
        preds = model.predict(X_valid)
        print(classification_report(y_valid, preds))
        importance = model.coef_[0]
        fi=mlProfiler.fs_fimp(importance,df,X,features,plot)
        return fi
    
    ########################################################
    # Intepretability 
    ########################################################          
    #LIME
    def fs_localInt(df, features, rg1,nclasses=[],which_obs=10, model=None, mtype='c', test_periods=252):
        from lime.lime_tabular import LimeTabularExplainer
        
        df=df[:-test_periods]
        X = df[features]
        y = df[rg1]

        if mtype=='r':
            explainer = LimeTabularExplainer(np.asarray(X),mode="regression", 
                                             feature_names=features,
                                            class_names=nclasses,
                                            discretize_continuous=False)
            method=model.predict
        else:
            explainer = LimeTabularExplainer(np.asarray(X), 
                                             feature_names=features,
                                            class_names=nclasses,
                                            discretize_continuous=False)
            method=model.predict_proba
    
        model.fit(X.values,y.values) #model.fit(X,y)
        X_observation=X.iloc[[which_obs], :] #pd.DataFrame(X.iloc[which_obs]).T
        explanation = explainer.explain_instance(X_observation.values[0], method)

        explanation.show_in_notebook(show_table=True, show_all=False)
        print('Explanation Score: ',explanation.score)
        
        imp=np.array([x[1] for x in explanation.as_list()])
        fi=mlProfiler.fs_fimp(imp,df,X,features,True)
        return fi
    
    def fs_globalInt(df, features, rg1,nclasses=[],ninsta=10, model=None, mtype='c', thresh=0.05, test_periods=252):
        from lime.lime_tabular import LimeTabularExplainer

        df=df[:-test_periods]
        X = df[features]
        y = df[rg1]

        if mtype=='c':
            explainer = LimeTabularExplainer(np.asarray(X), 
                                             feature_names=features,
                                            class_names=nclasses,
                                            discretize_continuous=False)
            model.fit(X.values,y.values) #model.fit(X,y)
            proba_signal = model.predict_proba(np.asarray(X))[:,1] #X
            bcond=proba_signal > 0.5
        else:
            explainer = LimeTabularExplainer(np.asarray(X),mode="regression", 
                                             feature_names=features,
                                            class_names=nclasses,
                                            discretize_continuous=False)
            model.fit(X.values,y.values) #model.fit(X,y)
            pred_signal = (y-model.predict(np.asarray(X)))**2  #X
            bcond=pred_signal < thresh

        importances = { i: 0.0 for i in range(X.shape[1]) }
        num_explain = ninsta
        for i in range(num_explain):
            # collect feature importances for one class only
            if bcond[i]:
                X_observation=X.iloc[[i], :]
                exp = explainer.explain_instance(X_observation.values[0],
                                                 model.predict_proba,
                                                 num_features=X.shape[1])
                exp_map = exp.as_map()
        
                if mtype=='c':
                    # for classification, get all feature labels/weights of class "1"
                    feat = [exp_map[1][m][0] for m in range(len(exp_map[1]))]   
                    weight = [exp_map[1][m][1] for m in range(len(exp_map[1]))] 
                    for m in range(len(feat)):
                        importances[feat[m]] = importances[feat[m]] + weight[m]   
                else:        
                    for cls in range(0,len(exp_map)):
                        feat = [exp_map[1][m][0] for m in range(len(exp_map[cls]))]   
                        weight = [exp_map[1][m][1] for m in range(len(exp_map[cls]))] 
                        for m in range(len(feat)):
                            importances[feat[m]] = importances[feat[m]] + weight[m]       
        for i in range(X.shape[1]):
            importances[i] = importances[i] / (num_explain*1.0)
        imp=np.array(list(importances.items()))[:,1]
        fi=mlProfiler.fs_fimp(imp,df,X,features,True)
        return fi

    def fs_permImp(df, features, rg1, metric, model, test_periods=252):
        
        df=df[:-test_periods]
        X_train = df[features]
        y_train = df[rg1]
        
        baseline = metric(model, X_train, y_train)
        imp = []
        for col in X_train.columns:
            save = X_train[col].copy()
            X_train[col] = np.random.permutation(X_train[col])
            m = metric(model, X_train, y_train)
            X_train[col] = save
            imp.append(baseline - m)
        return np.array(imp)
    
    #ALE
    def fs_alePlot(model, X_test, feat_name='', gs=100, size=(10,6)):
        from PyALE import ale
        import matplotlib.pyplot as plt
    
        ## 1D - continuous - no CI
        ale_eff = ale(
            X=X_test, model=model, feature=[feat_name], grid_size=gs, include_CI=False
        )
        plt.gcf().set_size_inches(size[0],size[1])
        return ale_eff

    def fs_alePlot2(model, X_test, feat_names=[], gs=100,size=(8,6)):
        from PyALE import ale
        import matplotlib.pyplot as plt
        
        ale_eff= ale(X=X_test, model=model, feature=feat_names, grid_size=gs)
        plt.gcf().set_size_inches(size[0],size[1])
        return ale_eff

    def fs_alePlotP(model, X_test, feat_names=[], gs=100, size=(10,6)):
        from PyALE import ale
        import matplotlib.pyplot as plt 
    
        #mymodel.predict_proba(lime_gold[features_set])[110:120, :]

        class clf_dummy():
            def predict(df):
                return(model.predict_proba(df)[:, 1]) #2 for thre classes...
    
        feat_eff = ale(X=X_test, model=clf_dummy, feature=feat_names, grid_size=gs)
        plt.ylabel('Effect on the predicted probability \nthat the Feature Class is down (0)')
        plt.gcf().set_size_inches(size[0],size[1])
        return feat_eff
    
    #SHAP    
    def fs_shapley_imp(model, X, resampling=False, N=100, plot=True):
        import shap   #conda install -c conda-forge shap :-()
    
        if resampling:
            X_test= shap.utils.sample(X, N) # N=100 instances for use as the background distribution
        else:
            X_test=X.values
        
        explainer = shap.Explainer(model.predict, X_test, algorithm='partition')
        shap_values = explainer(X)
        if plot:
            shap.plots.bar(shap_values, max_display=len(X.columns))
        return shap_values
        
    def fs_shapley_pdp(model, X, feat_name='',sample_ind=0, shap_values=[]):
        import shap
        
        if sample_ind == 0:
            shap.plots.partial_dependence(
                feat_name, model.predict, X, ice=False,
                model_expected_value=True, feature_expected_value=True
            )    
        else:
            shap.partial_dependence_plot(
            feat_name, model.predict, X, model_expected_value=True,
            feature_expected_value=True, ice=False,
            shap_values=shap_values[sample_ind:sample_ind+1,:]
            )
    
    def fs_shapley_local(shap_values, obs, ptype='bars',nf_displ=14):
        import shap
        
        if ptype=='bars':
            shap.plots.bar(shap_values[obs], max_display=nf_displ)
        else:
            shap.plots.waterfall(shap_values[obs], max_display=nf_displ)
        
    def fs_shapley_global(shap_values, obs1=0,obs2=0):
        import shap
        
        if obs1==0 and obs2==0:
            shap.plots.heatmap(shap_values[:])
        else:
            shap.plots.heatmap(shap_values[obs1:obs2])
    
    def fs_shapley_impTree(model, X, plot=True,fs=(8,10)):
        import shap   #conda install -c conda-forge shap :-()
    
        explainer = shap.TreeExplainer(model)
        shap_values = explainer.shap_values(X)

        fi0 = np.abs(shap_values[0]).mean(axis=0)
        fi1 = np.abs(shap_values[1]).mean(axis=0)
        fi = fi0 + fi1
    
        imp = pd.DataFrame({"feature": X.columns.tolist(), "mean": fi})
        imp = imp.set_index("feature")

        if plot:
            top_shap_feat = (imp[(imp["mean"] > imp["mean"].mean())].squeeze().sort_values(ascending=False))
            top_shap_feat.plot.barh(figsize=fs)
            imp=top_shap_feat
        return imp,shap_values
    
    def fs_shapley_dpscatter(shap_values, feat_name1='', feat_name2=''):
        import shap
        # create a dependence scatter plot to show the effect 
        # of a single feature across the whole dataset
        if feat_name2=='':
            shap.plots.scatter(shap_values[:,feat_name1], color=shap_values)
        else:
            shap.plots.scatter(shap_values[:,feat_name1], color=shap_values[:,feat_name2])
    
    ########################################################
    # Diagnostic Plots
    ########################################################          
    @staticmethod
    def plot_mcRoc(y_valid_, y_pred, classes, ax):
        from sklearn.metrics import roc_curve
        from sklearn.metrics import auc
        
        # structures
        fpr = dict()
        tpr = dict()
        roc_auc = dict()

        # calculate dummies once
        y_valid__dummies = pd.get_dummies(y_valid_, drop_first=False).values
        for i, label in zip(range(len(classes)), classes):
            fpr[i], tpr[i], _ = roc_curve(y_valid__dummies[:, i], y_pred[:, i])
            roc_auc[i] = auc(fpr[i], tpr[i])

        # roc for each class
        ax.plot([0, 1], [0, 1], "r--")
        ax.set_xlim([0.0, 1.0])
        ax.set_ylim([0.0, 1.05])
        ax.set_xlabel("False Positive Rate")
        ax.set_ylabel("True Positive Rate")
        for i, label in zip(range(len(classes)), classes):
            ax.plot(
                fpr[i],
                tpr[i],
                label='ROC curve (area = '+str(roc_auc[i].round(2))+') for label '+str(label)
            )
        ax.legend(loc="best")
        ax.set_title("ROC-AUC")

    @staticmethod
    def plot_mcPRcurve(y_valid_, y_pred, classes, ax):
        from sklearn.metrics import precision_recall_curve
        # structures
        fpr = dict()
        tpr = dict()

        # precision recall curve
        precision = dict()
        recall = dict()

        # calculate dummies once
        y_valid__dummies = pd.get_dummies(y_valid_, drop_first=False).values
        for i, label in zip(range(len(classes)), classes):
            precision[i], recall[i], _ = precision_recall_curve(y_valid__dummies[:, i], y_pred[:, i])
            ax.plot(recall[i], precision[i], lw=2, label=f"class {label}")

        ax.set_xlabel("recall")
        ax.set_ylabel("precision")
        ax.legend(loc="best")
        ax.set_title("precision vs. recall curve")
        
    @staticmethod
    def plot_confMat(conf_mat, labels, text_array,ax):
        import seaborn as sns
        g = sns.heatmap(conf_mat,annot=text_array,fmt="s",linewidths=0.5,
                    cmap="Blues",square=True,cbar=False,ax=ax,
                                annot_kws={"ha": "center", "va": "center"})
        ax.set(xlabel="Predicted value/label", ylabel="Actual value/label", title="Confusion Matrix")
        ax.xaxis.set_ticklabels(labels)
        ax.yaxis.set_ticklabels(labels)
        
    @staticmethod
    def md_Diag(model,X_valid_,y_valid_,labels=None,custom_threshold=None,average=None,plot=True,pr_curve=False,table=True,fs=(15,8)):
        import sklearn.metrics as metrics
        import matplotlib.pyplot as plt
        import matplotlib.gridspec as gridspec
        
        # labels : List with the class names.
        # show_pr_curve : also show the PR-curve
        # Return the most important evaluation metrics
        # Courtesy of Brian Christopher of Blackarbs LLC
        
        if custom_threshold is None:  # default is 50%
            y_pred = model.predict(X_valid_)
        else:
            # TODO UPDATE FOR THE MULTICLASS CASE
            y_pred = (model.predict_proba(X_valid_)[:, 1] > threshold).astype(int)
            y_pred = np.where(y_pred == 0, -1, 1)

        y_pred_prob = model.predict_proba(X_valid_)  
        #print(y_valid_, len(y_valid_), y_pred,len(y_pred),y_pred_prob,len(y_pred_prob))
        conf_mat = metrics.confusion_matrix(y_valid_, y_pred)
        # REF:https://stackoverflow.com/questions/50666091/true-positive-rate-and-false-positive-rate-tpr-fpr-for-multi-class-data-in-py
        fp = conf_mat.sum(axis=0) - np.diag(conf_mat)
        fn = conf_mat.sum(axis=1) - np.diag(conf_mat)
        tp = np.diag(conf_mat)
        tn = conf_mat.sum() - (fp + fn + tp)
        fp = fp.astype(float)
        fn = fn.astype(float)
        tp = tp.astype(float)
        tn = tn.astype(float)

        tpr = tp / (tp + fn)    # Sensitivity, hit rate, recall, or true positive rate
        tnr = tn / (tn + fp)    # Specificity or true negative rate
        ppv = tp / (tp + fp)    # Precision or positive predictive value
        npv = tn / (tn + fn)    # Negative predictive value
        fpr = fp / (fp + tn)    # Fall out or false positive rate
        fnr = fn / (tp + fn)    # False negative rate
        fdr = fp / (tp + fp)    # False discovery rate
        acc = (tp + tn) / (tp + fp + fn + tn)   # Overall accuracy

        precision = (metrics.precision_score(y_valid_, y_pred, average=average),)
        recall = (metrics.recall_score(y_valid_, y_pred, average=average),)
    
        stats = {"accuracy": np.round(acc, 4), "precision": np.round(ppv, 4),
            "recall": np.round(tpr, 4),
            "mcc": round(metrics.matthews_corrcoef(y_valid_, y_pred), 4),
            "specificity": np.round(tnr, 4),
            "f1_score": np.round(metrics.f1_score(y_valid_, y_pred, average=average), 4),
            "cohens_kappa": round(metrics.cohen_kappa_score(y_valid_, y_pred), 4)}

        if plot:
            ncols = 3 if pr_curve else 2
            nrows = 1
            fig = plt.figure(figsize=fs, tight_layout=True)
            gs = gridspec.GridSpec(nrows, ncols)
            ax0 = fig.add_subplot(gs[0, 0])
            ax1 = fig.add_subplot(gs[0, 1])
            if ncols >2: ax2 = fig.add_subplot(gs[0, 2])
            fig.suptitle("Performance Evaluation", fontsize=16, y=1.05)
    
            if labels is None: labels = ["Negative", "Positive"]
            total_samples = conf_mat.sum(axis=1)[:, np.newaxis]
            normed_conf_mat = conf_mat.astype("float") / total_samples
            text_array = np.empty_like(conf_mat, dtype="object")
            for i in range(conf_mat.shape[0]):
                for j in range(conf_mat.shape[1]):
                    norm_val = normed_conf_mat[i, j]
                    int_val = conf_mat[i, j]
                    text_array[i, j] = f"({norm_val:.1%})\n{int_val}"
        
            mlProfiler.plot_confMat(conf_mat, labels, text_array,ax0)
        
            _ = mlProfiler.plot_mcRoc(y_valid_, y_pred_prob, labels, ax1)
            ax1.plot(fp / (fp + tn), tp / (tp + fn), "ro", markersize=8, label="Decision Point")

            if pr_curve:
                _ = mlProfiler.plot_mcPRcurve(y_valid_, y_pred_prob, labels, ax2)

        if table:
            print('------------------------------------------')
            print('Accuracy', stats['accuracy'])
            print('Recall', stats['recall'])
            print('MCC', stats['mcc'])
            print('Specificity', stats['specificity'])
            print('F1_score', stats['f1_score'])
            print('Cohens Kappa', stats['cohens_kappa'])
            print('------------------------------------------')
        
        return stats
    
    @staticmethod
    def md_cmpmetrics(idx,model,df_elab1,df_elab2,i,train_y,test_y):
        from sklearn.metrics import f1_score
        from sklearn.metrics import accuracy_score
        from sklearn.metrics import recall_score
        from sklearn.metrics import precision_score
        from sklearn.metrics import roc_auc_score
        from scipy.stats import spearmanr
        from numpy import array
    
        ret=model(df_elab1,*i)
        tmp_train=ret['yhat']
        mse,y_score_train=ret['mse'],ret['probs']

        ret=model(df_elab2,*i)
        tmp_test=ret['yhat']
        mse,y_score_test=ret['mse'],ret['probs']

        train_f1=f1_score(tmp_train,train_y)
        train_acc=accuracy_score(tmp_train,train_y)
        train_recall=recall_score(tmp_train,train_y)
        train_prec=precision_score(tmp_train,train_y)
        test_f1=f1_score(tmp_test,test_y)
        test_acc=accuracy_score(tmp_test,test_y)
        test_recall=recall_score(tmp_test,test_y)
        test_prec=precision_score(tmp_test,test_y)

        test_auc=roc_auc_score(y_score=y_score_test, y_true=test_y)
        ic,pval=spearmanr(y_score_test, test_y)
        return array([train_f1,test_f1,train_acc,test_acc,train_recall,test_recall,train_prec,test_prec,test_auc,ic,pval,idx])
    
    @staticmethod
    def md_htDiag(df,target_lbl,model,params,t = 0.80,splits=7,how='cv',Nproc=4):
        from sklearn.metrics import f1_score
        from sklearn.metrics import accuracy_score
        from sklearn.metrics import recall_score
        from sklearn.metrics import precision_score
        from sklearn.metrics import roc_auc_score
        from scipy.stats import spearmanr
        from Plib.Utils.Tools import parallelExecution1
    
        import multiprocessing
        from itertools import product
        import time
                    
        #cols,train_x,train_y,test_x,test_y,gd_train,gd_test=mlProfiler.splitDataset(df,target_lbl,t = t)
        bd,cols,mylist=mlProfiler.splitDataset(df,target_lbl,t=t,splits=splits,how=how)    
    
        tscore=pd.DataFrame()
        for ld in mylist:

            train_x,train_y,test_x,test_y=ld

            x=pd.DataFrame(train_x)
            x.columns=cols
            y=pd.DataFrame(train_y)
            y.columns=[target_lbl]
            df_elab1=pd.concat([x,y],axis=1)

            x=pd.DataFrame(test_x)
            x.columns=cols
            y=pd.DataFrame(test_y)
            y.columns=[target_lbl]
            df_elab2=pd.concat([x,y],axis=1)
            
            results =[]
            if Nproc==0:
                for i in params:
                    temp=mlProfiler.md_cmpmetrics(int(params.index(i)),model,df_elab1,df_elab2,i,train_y,test_y)
                    results.append(temp)
            else:
                args = [(int(params.index(i)),model,df_elab1,df_elab2,i,train_y,test_y) for i in params]
                with multiprocessing.Pool(processes=Nproc) as pool:
                    results = pool.starmap(mlProfiler.md_cmpmetrics, args)
                    pool.close()
        
            acols=['train F1 score', 'test F1 score','train Accuracy score', 'test Accuracy score',
                    'train Recall score', 'test Recall score','train Precision score','test Precision score',
                    'test AUC score','test IC score','test IC Pvalue score','idxparams']
            for r in results:
                ascores=r
                score = pd.DataFrame(ascores).T
                score.columns=acols
                tscore=tscore.append(score)
                                                                            
            tscore=tscore.groupby("idxparams").mean()                                         
            tscore=tscore.reset_index()                                                     
            #del tscore['idxparams']       #del tscore['idxparams']                               
            tscore.index = range(1,len(tscore)+1)
        
        return tscore                                                              
           
    @staticmethod
    def md_htDiagPlot(score,fs=(12,10),best='F1'):
        import matplotlib.pyplot as plt
        import seaborn as sns; sns.set()
        
        def plot_ic_distribution(df,lbl='', ax=None):
            if ax is not None:
                sns.distplot(df[lbl], ax=ax)    
            else:
                ax = sns.distplot(df[lbl])
            mean, median = df[lbl].mean(), df[lbl].median()
            ax.axvline(0, lw=1, ls='--', c='k')
            ax.text(x=.05, y=.9, s=f'Mean: {mean:8.2f}\nMedian: {median:5.2f}',
                    horizontalalignment='left',
                    verticalalignment='center',
                    transform=ax.transAxes)
            ax.set_xlabel('Information Coefficient')
            sns.despine()
            plt.tight_layout()
        
        
        fig, axs = plt.subplots(3, 2, figsize=fs)
        fig.suptitle('Assessing Model Behavior in Train-Test')

        best_param1=(abs(score['train F1 score']-score['test F1 score'])).idxmin()
        axs[0, 0].axvline(x=best_param1,label='Best Solution:'+str(best_param1))
        axs[0, 0].plot(score['test F1 score'], color = 'red' , label = 'test')
        axs[0, 0].plot(score['train F1 score'], color = 'green', label = 'train')
        axs[0, 0].set_title('F1 Curve')
        axs[0, 0].legend()

        best_param2=(abs(score['train Accuracy score']-score['test Accuracy score'])).idxmin()
        axs[0, 1].axvline(x=best_param2,label='Best Solution:'+str(best_param2))
        axs[0, 1].plot(score['test Accuracy score'], color = 'red' , label = 'test')
        axs[0, 1].plot(score['train Accuracy score'], color = 'green', label = 'train')
        axs[0, 1].set_title('Accuracy Curve')
        axs[0, 1].legend()

        best_param3=(abs(score['train Recall score']-score['test Recall score'])).idxmin()
        axs[1, 0].axvline(x=best_param3,label='Best Solution:'+str(best_param3))
        axs[1, 0].plot(score['test Recall score'], color = 'red' , label = 'test')
        axs[1, 0].plot(score['train Recall score'], color = 'green', label = 'train')
        axs[1, 0].set_title('Recall Curve')
        axs[1, 0].legend()

        best_param4=(abs(score['train Precision score']-score['test Precision score'])).idxmin()
        axs[1, 1].axvline(x=best_param4,label='Best Solution:'+str(best_param4))
        axs[1, 1].plot(score['test Precision score'], color = 'red' , label = 'test')
        axs[1, 1].plot(score['train Precision score'], color = 'green', label = 'train')
        axs[1, 1].set_title('Precision Curve')
        axs[1, 1].legend()
        
        #mean_median = score['test AUC score'].agg(['mean', 'median'])
        mean_median = score['test AUC score'].agg(['mean', 'median']).round(2)
        index_mean = abs(score['test AUC score'] - mean_median[0]).idxmin()
        index_median = abs(score['test AUC score'] - mean_median[1]).idxmin()
        mean_value=score['test AUC score'].iloc[index_mean-1].round(2)
        median_value=score['test AUC score'].iloc[index_median-1].round(2)
        axs[2, 0].axvline(x=index_mean,linestyle='-',color = 'black', label='Mean:'+str(index_mean))
        axs[2, 0].axvline(x=index_median,linestyle=':',color = 'gray',label='Median:'+str(index_median))
        axs[2, 0].plot(score['test AUC score'], color = 'red' , label = 'test')
        axs[2, 0].set_title('AUC Curve')
        axs[2, 0].legend()

        plot_ic_distribution(score,'test IC score',axs[2, 1])
        axs[2, 1].set_title('Information Coefficient ')
        
        for ax in axs.flat:
            ax.set(xlabel='ML Model', ylabel='Score')
            ax.locator_params(nbins=14, axis='x')

        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        
        if best=='F1':
            best_param=best_param1
        elif best=='ACC':
            best_param=best_param2
        elif best=='REC':
            best_param=best_param3
        elif best=='PR':
            best_param=best_param4
        else:
            best_param=best_param1    
        return int(score['idxparams'].loc[best_param])
        #fig.tight_layout()

    @staticmethod
    def md_targetDiag(train_y,test_y):
        import matplotlib.pyplot as plt

        fig, (ax1, ax2) = plt.subplots(1, 2)
        fig.suptitle('Check Distribution of Target Class (y)')

        dist1 = pd.DataFrame(train_y)
        dist1.plot.kde(ax=ax1, legend=False, title='Histogram: train_y')
        dist1.plot.hist(density=True, ax=ax1)
        ax1.set_ylabel('Probability')
        ax1.grid(axis='y')
        ax1.set_facecolor('#d8dcd6')

        dist2 = pd.DataFrame(test_y)
        dist2.plot.kde(ax=ax2, legend=False, title='Histogram: test_y')
        dist2.plot.hist(density=True, ax=ax2)
        ax2.set_ylabel('Probability')
        ax2.grid(axis='y')
        ax2.set_facecolor('#d8dcd6')
        #print(test_y.value_counts()/len(test_y))
    
    @staticmethod
    def fs_fimp(imp,df,X,features,plot=False):
        import matplotlib.pyplot as plt

        indices = np.argsort(imp)[::-1]
        fi=pd.DataFrame(np.sort(imp)[::-1],index=df[features].columns[indices],columns=['imp'])
        if plot:
            plt.figure()
            plt.title("Feature importances")
            plt.bar(range(len(fi)), fi.imp,
                    color="lightsalmon", align="center")
            plt.xticks(range(df.shape[1]), df[features].columns[indices], rotation=90) 
            plt.xlim([-1, df.shape[1]])
            plt.show()
        return fi
            
    ########################################################
    # Probability Miscalibration Diagnostics 
    ########################################################          
    @staticmethod
    def getprobCal(model,df,features,regrnd1,n_bins=50,strategy='quantile', normalize=True, plot=False, fs=(12,6)):
        from sklearn.calibration import calibration_curve
        #OBJ.plot_probCal(model,df,cols,target_lbl,plot=True)
        if hasattr(model, "decision_function"):
            probs=model.decision_function(np.asarray(df[features]))
        else:
            probs=model.predict_proba(np.asarray(df[features]))[:, 1]
        y=df[regrnd1].values
        y_means, proba_means = calibration_curve(y, probs, n_bins=n_bins, strategy=strategy, normalize=normalize)
    
        if plot:
            plot_probCal(y_means, proba_means, fs=fs)
        return y_means, proba_means
        
    @staticmethod
    def plot_probCal(y, probs, n_bins=50,strategy='quantile', normalize=True, fs=(12,6)):
        import matplotlib.pyplot as plt
        from sklearn.calibration import calibration_curve
        
        y_means, proba_means = calibration_curve(y, probs, n_bins=n_bins, strategy=strategy, normalize=normalize)
        
        fig, ax = plt.subplots(figsize=fs)
        ax.plot([0, 1], [0, 1], linestyle = '--', label = 'Perfect calibration')
        ax.plot(proba_means, y_means, marker='.',label = 'Model Predicted Probability')
        ax.set_title("Probability Calibration Curve")
        ax.text(0.50,0.15, "Overestimation of True Probability")
        ax.text(0.05,0.85, "Underestimation of True Probability")
        plt.xlabel("Bin's Mean of Predicted Probability")
        plt.ylabel("Bin's Mean of Target Variable")
        ax.legend(loc=1)
        plt.show()
        return y_means, proba_means
    
    @staticmethod
    def calibIsoReg(model, proba_test, y_test, X_test):
        from sklearn.isotonic import IsotonicRegression
        iso_reg = IsotonicRegression(y_min = 0, y_max = 1, out_of_bounds = 'clip').fit(proba_test, y_test)
        
        if hasattr(model, "decision_function"):
            probs=model.decision_function(X_test)
        else:
            probs=model.predict_proba(X_test)[:, 1]
        proba_test_isoreg = iso_reg.predict(probs)
        #proba_test_isoreg = iso_reg.predict(model.predict_proba(X_test)[:, 1])
        return proba_test_isoreg
    
    @staticmethod
    def calibLogReg(model, proba_test, y_test, X_test):
        from sklearn.linear_model import LogisticRegression
        log_reg = LogisticRegression().fit(proba_test.reshape(-1, 1), y_test)
        
        if hasattr(model, "decision_function"):
            probs=model.decision_function(X_test)
        else:
            probs=model.predict_proba(X_test)[:, 1]
        proba_test_logreg = log_reg.predict_proba(probs.reshape(-1, 1))[:, 1]
        #proba_test_logreg = log_reg.predict_proba(model.predict_proba(X_test)[:, 1].reshape(-1, 1))[:, 1]
        return proba_test_logreg
    
    @staticmethod
    def calibPlatScaling(model, trainX, trainy, y_test, X_test):
        from sklearn.calibration import CalibratedClassifierCV
        calibrated = CalibratedClassifierCV(model, method='sigmoid', cv=5)
        calibrated.fit(trainX, trainy)
        probs = calibrated.predict_proba(X_test)[:, 1]
        return probs
    
    @staticmethod
    def expected_calibration_error(y, proba, bins = 'fd'): 
        import numpy as np
    
        bin_count, bin_edges = np.histogram(proba, bins = bins) 
        n_bins = len(bin_count)
        bin_edges[0] -= 1e-8 # because left edge is not included 
        bin_id = np.digitize(proba, bin_edges, right = True) - 1
        bin_ysum = np.bincount(bin_id, weights = y, minlength = n_bins)
        bin_probasum = np.bincount(bin_id, weights = proba, minlength = n_bins)
        bin_ymean = np.divide(bin_ysum, bin_count, out = np.zeros(n_bins), where = bin_count > 0)
        bin_probamean = np.divide(bin_probasum, bin_count, out = np.zeros(n_bins), where = bin_count > 0)
        ece = np.abs((bin_probamean - bin_ymean) * bin_count).sum() / len(proba)
        return ece
    
    @staticmethod
    def computeCalibrated(model, proba_test, trainX, trainy, y_test, X_test):
        ece_c0=mlModel.expected_calibration_error(y_test, proba_test, bins = 'fd')
        c1=mlModel.calibIsoReg(model, proba_test, y_test, X_test)
        ece_c1=mlModel.expected_calibration_error(y_test, c1, bins = 'fd')
        c2=mlModel.calibLogReg(model, proba_test, y_test, X_test)
        ece_c2=mlModel.expected_calibration_error(y_test, c2, bins = 'fd')
        try:
            c3=mlModel.calibPlatScaling(model, trainX, trainy, y_test, X_test)
            ece_c3=mlModel.expected_calibration_error(y_test, c3, bins = 'fd')
        except:
            ece_c3=-999
        print('Transformation of Probabilities:')
        print('------------------------------------------')
        print('Model                                ', round(ece_c0,2))
        print('Model + Isotonic regression          ', round(ece_c1,2))
        print('Model + Logistic regression          ', round(ece_c2,2))
        print('Platt Scaling * Model                ', round(ece_c3,2))
        print('------------------------------------------')
    
########################################################
# Base ML Model Forecasting
########################################################      
class mlModel(mlProfiler):
    
    @staticmethod
    def cleanData(df):
        return mlModel.outliers(mlModel.missingv(df))
    
    @staticmethod
    def getProbs(model,df,features):
        return model.predict_proba(np.asarray(df[features]))[:, 1]
    
    @staticmethod
    def getPreds(model,df,features):
        return model.predict(np.asarray(df[features]))
    
    @staticmethod
    def getDistances(model,df,features):
        return model.decision_function(np.asarray(df[features]))
    
    @staticmethod
    def applySignal(df,signal,benchmark_level='Close'):
    
        df['Signal'] = signal
        # Compute GLD returns 
        df['Ret'] = (df[benchmark_level]-df[benchmark_level].shift(1))/df[benchmark_level].shift(1)

        # Compute strategy returns(
        df['Ret1'] = df['Ret']*(df['Signal'].shift(1))
        return df

    @staticmethod
    def plotReturns(df):
        import matplotlib.pyplot as plt
        
        plt.figure(figsize=(10,5))
        plt.plot(((1+df['Ret']).cumprod()),color='r',label='Asset Returns')
        plt.plot(((1+df['Ret1']).cumprod()),color='g',label='Strategy Returns')
        plt.legend()
        plt.show()
        df.head(5)
    
    @staticmethod
    def plotLevels(df, benchmark_level='Close'):
        import matplotlib.pyplot as plt
        
        df[benchmark_level].plot(figsize=(10, 7),color='g')
        plt.ylabel("Levels")
        plt.title(benchmark_level + " Levels Series")
        plt.show()
        df.head(3)
        
########################################################
# Regression Forecasting
########################################################      
class linreg(mlModel):
    
    @staticmethod
    def regression(df,target_lbl,t = 0.7,params={},table=False):
        import Plib.Stats.Regression as r
        
        # Make array the data and retrieve explanators names
        bd,cols,xl_train,yl_train,xl_test,yl_test=linreg.splitDataset(df,target_lbl,t = t)

        #Fit on the train part and print equation
        yhat,MSE,lm = r.linReg(xl_train, yl_train, params, t=1, Plot=False, table=table)
        ret={'model':lm,'mse':MSE,'pvalues':None,'yhat':yhat}
        
        try:
            MSE,c_df,r_df=r.printStats(lm, xl_train, yl_train, table=table)
            ret['pvalues']=c_df
        except:
            pass
        return ret

########################################################
# Multinomial Logistic Regression Forecasting
########################################################      
class LogRegC(mlModel):
    
    @staticmethod
    def make_params(rg1='',C=3):
        import itertools

        regr_ = [rg1]
        split_ = [1]
        C_ = [round(num, 4) for num in [0]+list(np.linspace(0.001,0.1,C))[:-1] + list(np.linspace(0.1,1,C))]    
        tbl_ = [False]
        lparams=[regr_,split_,C_,tbl_]
        cfg_param_list = list(itertools.product(*lparams))  
        return cfg_param_list
            
    @staticmethod
    def LogisticRegressionClassifier(df,target_lbl,t = 0.7, C=1,table=False):
        from sklearn.linear_model import LogisticRegression as LRC
        
        bd,cols,xl_train,yl_train,xl_test,yl_test=LogRegC.splitDataset(df,target_lbl,t = t)
        
        penalty='l2'
        if C==0: penalty='none'
        clf  = LRC(multi_class='multinomial', solver='lbfgs', penalty=penalty, C=C, random_state=LogRegC.rng)
        model=clf.fit(xl_train,yl_train)
        yhat=clf.predict(xl_train)
        y_score=clf.predict_proba(xl_train)[:, 1]
        
        score=0
        if t < 1:  score = np.mean((yhat - yl_train)**2)
        ret={'model':clf,'mse':score,'stats':None,'yhat':yhat,'probs':y_score}
        
        if table: 
            sts=LogRegC.md_Diag(model, xl_test, yl_test,plot=True,pr_curve=True,table=True)
            y_means, proba_means=LogRegC.plot_probCal(yl_test, clf.predict_proba(xl_test)[:, 1])
            LogRegC.computeCalibrated(model, clf.predict_proba(xl_test)[:, 1], xl_train,yl_train, yl_test, xl_test)
            ret['stats']=sts
            
        return ret #[score,pred,y_score,model]
        
########################################################
# KNeighborsClassifier Forecasting
########################################################      
class KNNC(mlModel):
    
    @staticmethod
    def make_params(rg1='',k=10, p=5):
        import itertools

        regr_ = [rg1]
        split_ = [1]
        k_ = list(range(1,k))
        mtr_ = ['chebyshev', 'hamming']
        p_ = [2]
        w_ = ['uniform', 'distance']
        tbl_ = [False]
        lparams=[regr_,split_,k_,mtr_,p_,w_,tbl_]
        cfg_param_list1 = list(itertools.product(*lparams))  
        
        regr_ = [rg1]
        split_ = [1]
        k_ = list(range(1,k))
        mtr_ = ['minkowski']
        p_ = list(range(1,p))
        w_ = ['uniform', 'distance']
        tbl_ = [False]
        lparams=[regr_,split_,k_,mtr_,p_,w_,tbl_]
        cfg_param_list2 = list(itertools.product(*lparams))  
        
        return cfg_param_list1 + cfg_param_list2 
            
    @staticmethod
    def kneighborsClassifier(df,target_lbl,t = 0.7,n_neighbors=5,mtr='minkowski',p=2,wgt='uniform', table=False):
        from sklearn.neighbors import KNeighborsClassifier as KNNCl
        
        bd,cols,xl_train,yl_train,xl_test,yl_test=KNNC.splitDataset(df,target_lbl,t = t)

        clf  = KNNCl(n_neighbors = n_neighbors, weights=wgt, p=p, metric=mtr) #, random_state=KNNC.rng
        model=clf.fit(xl_train,yl_train)
        yhat=clf.predict(xl_train)
        y_score=clf.predict_proba(xl_train)[:, 1]
        
        score=0
        if t < 1:  score = np.mean((yhat - yl_train)**2)
        ret={'model':clf,'mse':score,'stats':None,'yhat':yhat,'probs':y_score}
        
        if table: 
            sts=KNNC.md_Diag(model, xl_test, yl_test,plot=True,pr_curve=True,table=True)
            y_means, proba_means=KNNC.plot_probCal(yl_test, clf.predict_proba(xl_test)[:, 1])
            KNNC.computeCalibrated(model, clf.predict_proba(xl_test)[:, 1], xl_train,yl_train, yl_test, xl_test)
            ret['stats']=sts
            
        return ret #[score,pred,y_score,model]

########################################################
# SupportVector MAchine Forecasting
########################################################      
class SVMC(mlModel):
    
    @staticmethod
    def make_params(rg1='', kernel_='linear', C=5, gamma=5,table=False):
        import itertools
        
        #Kernel: linear or rbf
        regr_ = [rg1]
        split_ = [1]
        tbl_ = [False]
        a=0
        b=3
        C_ = list(np.logspace(a,b,num=b-a+1,base=C,dtype='int')) #[1, 5, 25, 125]
        if kernel_=='linear':
            kernel_ = ['linear']
            gamma_ = ['auto']
        else:
            kernel_ = ['rbf']
            a=1
            gamma_ = list(np.logspace(a,b,num=b-a+1,base=gamma,dtype='int')) #[0, 5, 25, 125]
        lparams=[regr_,split_,kernel_,C_,gamma_,tbl_]
        cfg_param_list = list(itertools.product(*lparams))  
        return cfg_param_list
    
    @staticmethod
    def svmClassifier(df,target_lbl,t = 0.7,kernel='linear', C=1, gamma='auto', table=False):
        from sklearn.svm import SVC as SVCC
    
        bd,cols,xl_train,yl_train,xl_test,yl_test=SVMC.splitDataset(df,target_lbl,t = t)

        clf  = SVCC(kernel=kernel, C=C, gamma=gamma, probability=True, random_state=SVMC.rng)
        model=clf.fit(xl_train,yl_train)
        yhat=clf.predict(xl_train)
        y_score=clf.predict_proba(xl_train)[:, 1]
        
        score=0
        if t < 1:  score = np.mean((yhat - yl_train)**2)
        ret={'model':clf,'mse':score,'stats':None,'yhat':yhat,'probs':y_score}
        
        if table: 
            sts=SVMC.md_Diag(model, xl_test, yl_test,plot=True,pr_curve=True, table=True)
            y_means, proba_means=SVMC.plot_probCal(yl_test, clf.decision_function(xl_test))
            SVMC.computeCalibrated(model, clf.decision_function(xl_test), xl_train,yl_train, yl_test, xl_test)
            ret['stats']=sts
            
        return ret #[score,pred,y_score,model]

########################################################
# Decision Tree Forecasting
########################################################      
class DTC(mlModel): 
    
    @staticmethod
    def make_params(rg1='', max_depth=1000, max_features=10, min_samples_split=2, min_samples_leaf=1, ccp_alpha=0):
        import itertools

        regr_ = [rg1]
        split_ = [1]
        crit_ = ['gini', 'entropy']
        splitter_ = ['best','random']
        max_depth_ =[max_depth, None]
        if min_samples_split < 2 : min_samples_split=2
        if min_samples_leaf < 1 : min_samples_leaf=1
        minss_= list(range(2,min_samples_split+1))
        minsl_ = list(range(1,min_samples_leaf+1))
        max_feat_ = ['sqrt','log2',max_features]
        ccpa_ = [ccp_alpha]
        tbl_ = [False]
        lparams=[regr_,split_,crit_,splitter_,max_depth_,minss_,minsl_,max_feat_,ccpa_,tbl_]
        cfg_param_list = list(itertools.product(*lparams))  
        return cfg_param_list
            
    @staticmethod
    def decisionTreeClassifier(df,target_lbl,t = 0.7, crit='gini',splt='best',md=10,mss=2,msl=1,maxf='sqrt',ccpa=0,table=False):
        from sklearn.tree import DecisionTreeClassifier as DTCC

        bd,cols,xl_train,yl_train,xl_test,yl_test=DTC.splitDataset(df,target_lbl,t = t)

        clf  = DTCC(criterion=crit, splitter=splt, max_depth=md, min_samples_split=mss, min_samples_leaf=msl, 
                            max_features=maxf, ccp_alpha=ccpa, random_state=DTC.rng)
        model=clf.fit(xl_train,yl_train)
        yhat=clf.predict(xl_train)
        y_score=clf.predict_proba(xl_train)[:, 1]
        
        score=0
        if t < 1:  score = np.mean((yhat - yl_train)**2)
        ret={'model':clf,'mse':score,'stats':None,'yhat':yhat,'probs':y_score}
        
        if table: 
            sts=DTC.md_Diag(model, xl_test, yl_test,plot=True,pr_curve=True,table=True)
            y_means, proba_means=DTC.plot_probCal(yl_test, clf.predict_proba(xl_test)[:, 1])
            DTC.computeCalibrated(model, clf.predict_proba(xl_test)[:, 1], xl_train,yl_train, yl_test, xl_test)
            DTC.pruningCostCompl(model, xl_train, yl_train)
            ret['stats']=sts
            
        return ret #[score,pred,y_score,model]

    @staticmethod
    def pruningCostCompl(dtree, X_train, y_train, plot=True, fs=(16,6)):
        from sklearn.tree import DecisionTreeClassifier
        import matplotlib.pyplot as plt

        path = dtree.cost_complexity_pruning_path(X_train, y_train)
        ccp_alphas, impurities = path.ccp_alphas, path.impurities
        if plot:
            fig, ax = plt.subplots(figsize=fs)
            ax.plot(ccp_alphas[:-1], impurities[:-1], marker="o", drawstyle="steps-post")
            ax.set_xlabel("effective alpha")
            ax.set_ylabel("total impurity of leaves")
            ax.set_title("Total Impurity vs effective alpha for training set")
            plt.show()

        clfs = []
        for ccp_alpha in ccp_alphas:
            clf = DecisionTreeClassifier(ccp_alpha=ccp_alpha)
            clf.fit(X_train, y_train)
            clfs.append(clf)
        print("Number of nodes in the last tree is: {} with ccp_alpha: {}".format(
                clfs[-1].tree_.node_count, ccp_alphas[-1]))
        return ccp_alphas[-1]

    @staticmethod    
    def plotDTree(dtree, df, regrnd1, features_set, max_depth=10, precision=4, filled=True, fs=(80,65)):
        import matplotlib.pyplot as plt
        from sklearn.tree import plot_tree
        from pandas.api.types import is_numeric_dtype

        #import sklearn
        #sklearn.__version__
        #dtree.feature_names_in_    # only in version 1.0
        #list(dtree.classes_)
        #dtree.n_features_in_

        plt.figure(figsize = fs)
        myvals=df[regrnd1].unique()
        if is_numeric_dtype(myvals):
            class_names=[str(num) for num in list(myvals)] 
        else:
            class_names=myvals
        if len(features_set)==0:
            dec_tree = plot_tree(decision_tree=dtree, max_depth=max_depth, #feature_names = features_set, 
                             class_names = class_names,
                              filled = filled , precision = precision, rounded = True)
        else:
            dec_tree = plot_tree(decision_tree=dtree, max_depth=max_depth, feature_names = features_set, 
                             class_names = class_names,
                              filled = filled , precision = precision, rounded = True)
                              
    @staticmethod
    def tree_to_code(tree, feature_names):
        from sklearn.tree import _tree
        from sklearn.tree import DecisionTreeRegressor, DecisionTreeClassifier

        if isinstance(tree, DecisionTreeClassifier):
            model = 'clf'
        elif isinstance(tree, DecisionTreeRegressor):
            model = 'reg'
        else:
            raise ValueError('Need Regression or Classification Tree')
        
        tree_ = tree.tree_
        feature_name = [
            feature_names[i] if i != _tree.TREE_UNDEFINED else "undefined!"
            for i in tree_.feature
        ]
        print("def tree({}):".format(", ".join(feature_names)))
        print('')

        def recurse(node, depth):
            indent = "  " * depth
            if tree_.feature[node] != _tree.TREE_UNDEFINED:
                name = feature_name[node]
                threshold = tree_.threshold[node]
                print(indent, f'if {name} <= {threshold:.2%}')
                recurse(tree_.children_left[node], depth + 1)
                print(indent, f'else:  # if {name} > {threshold:.2%}')
                recurse(tree_.children_right[node], depth + 1)
            else:
                pred = tree_.value[node][0]
                val = pred[1]/sum(pred) if model == 'clf' else pred[0]
                print(indent, f'return {val:.2%}')
        recurse(0, 1)
    
########################################################
# Neural Network Base Class
########################################################      
class NNBC(mlModel):
    
    opt = None
    X = None
    Y = None
    out = None
    net = None
    
    epochs = 100
    batch_size = 100
    
    def __init__(self, epochs=10,batch_size=100):
        NNBC.epochs=epochs
        NNBC.batch_size=batch_size
    
    @staticmethod
    def make_params(rg1='',nparmz=False, initz=False,optz=False, options={}):
        import itertools

        regr_ = [rg1]
        split_ = [1]
    
        listn=[]
        lnneurons = [16,32,64,128]
        for nneurons in lnneurons:
            nparams3={'hlayers': 3, 'nnlayers': [nneurons,nneurons,nneurons]}
            a = nparams3.copy()
            a.update(options)
            nparams4={'hlayers': 4, 'nnlayers': [nneurons,nneurons,nneurons,nneurons]}
            b = nparams4.copy()
            b.update(options)
            nparams5={'hlayers': 5, 'nnlayers': [nneurons,nneurons,nneurons,nneurons,nneurons]}
            c = nparams5.copy()
            c.update(options)
            nparams6={'hlayers': 6, 'nnlayers': [nneurons,nneurons,nneurons,nneurons,nneurons,nneurons]}
            d = nparams5.copy()
            d.update(options)
            tlist=[a,b,c,d]
            listn.append(tlist)
        nparmz_=tlist.copy()
        if nparmz: nparmz_ = listn
    
        sigma=1
        initializer1={'sigma':sigma,'mode':'fan_avg', 'distribution':'uniform'}
        initializer2={'sigma':sigma/2,'mode':'fan_avg', 'distribution':'uniform'}
        initz_ =[initializer1]
        if initz: initz_ =[initializer1,initializer2]
    
        optimizer1={'optimizer':'adam', 'loss':'mse','learning_rate' : 0.15, 'metrics':['mae']}
        optimizer2={'optimizer':'adam', 'loss':'mse','learning_rate' : 0.015, 'metrics':['mae']}
        optimizer3={'optimizer':'adam', 'loss':'mse','learning_rate' : 0.0015, 'metrics':['mae']}
        optz_ = [optimizer3]
        if optz: optz_ = [optimizer1,optimizer2,optimizer3]
    
        tbl_ = [False]
        lparams=[regr_,split_,nparmz_,initz_,optz_,tbl_]
        cfg_param_list = list(itertools.product(*lparams))  
    
        return cfg_param_list
        
    @staticmethod
    def fit(xl_train,yl_train):
        return NNBC
    
    @staticmethod
    def plot_costHist(cost_history, fs=(12,6)):
        import matplotlib.pyplot as plt
    
        df=pd.DataFrame(cost_history)
        cols=df.columns
        if len(cols)==1:
            df.columns=['Cost History']
            cols=df.columns

        fig, ax = plt.subplots(figsize=fs)
        for c in cols:
            ax.plot(df.index, df[c], marker='.',label = c)
        ax.set_title("Cost Function vs Epochs")
        plt.xlabel("Epochs")
        plt.ylabel("Cost")
        plt.legend(loc=2)
        plt.show()

########################################################
# Recurrent Neural Network Forecasting (tensorflow V1)
########################################################      
class RNNC(NNBC):
        
    @staticmethod
    def predict(X_data):
        ret= 1*(RNNC.net.run(RNNC.out, feed_dict={RNNC.X: X_data})[0] > 0.5)
        return ret 
    
    @staticmethod
    def predict_proba(X_data):
        import math

        def gf(x):
            if x <= 0: 
                x=0
            return x

        def stable_sigmoid(x):
            if x >= 0:
                z = math.exp(-x)
                sig = 1 / (1 + z)
                return sig
            else:
                z = math.exp(x)
                sig = z / (1 + z)
                return sig
        
        nn_preds = RNNC.net.run(RNNC.out, feed_dict={RNNC.X: X_data})[0] 
        treshold=stable_sigmoid(0.5)
        probs=[stable_sigmoid(e) for e in nn_preds]
        probs=np.asarray(probs)
        probs=(probs-(treshold-0.5)).round(3) 
        probs= np.asarray([gf(e) for e in probs])
        return np.asarray([[e,1-e] for e in probs]) 
    
    @staticmethod
    def getProbs(model,df,features):
        # for the first column [:, 0] depending on construction and requirment
        return RNNC.predict_proba(np.asarray(df[features]))[:, 1]
        
    @staticmethod
    def initNetwork(n_features,nparams,initializer,optimizer):   #nparams:  'hlayers': 3, 'nnlayers': [512,256,128]
        import tensorflow.compat.v1 as tf                        #initializer  'learning_rate'
        tf.disable_v2_behavior()                                 #optimizer={'optimizer':'adam', 'loss':'mse','learning_rate' : 0.001, 'metrics':['mae']}
        
        tf.set_random_seed(RNNC.rng)
        
        net = tf.InteractiveSession()
        X = tf.placeholder(dtype=tf.float32, shape=[None, n_features])
        Y = tf.placeholder(dtype=tf.float32, shape=[None])
        
        weight_initializer = tf.variance_scaling_initializer(mode=initializer['mode'], distribution=initializer['distribution'], scale=initializer['sigma'])
        bias_initializer = tf.zeros_initializer()
        prev_n_neurons=0
        for i in range(0,nparams['hlayers']): 
            n_neurons=nparams['nnlayers'][i]
            if i==0:
                W_hidden_n = tf.Variable(weight_initializer([n_features, n_neurons ]))
                bias_hidden_n = tf.Variable(bias_initializer([n_neurons]))
                hidden_n = tf.nn.relu(tf.add(tf.matmul(X, W_hidden_n), bias_hidden_n))
                prev_n_neurons=n_neurons
                prev_hidden=hidden_n
            else:
                W_hidden_n = tf.Variable(weight_initializer([prev_n_neurons, n_neurons ]))
                bias_hidden_n = tf.Variable(bias_initializer([n_neurons]))
                hidden_n = tf.nn.relu(tf.add(tf.matmul(prev_hidden, W_hidden_n), bias_hidden_n))
                prev_n_neurons=n_neurons
                prev_hidden=hidden_n
                
        W_out = tf.Variable(weight_initializer([n_neurons, 1]))
        bias_out = tf.Variable(bias_initializer([1]))
        out = tf.transpose(tf.add(tf.matmul(hidden_n, W_out), bias_out))
        
        mse = tf.reduce_mean(tf.squared_difference(out, Y))
        opt = tf.train.AdamOptimizer(learning_rate = optimizer['learning_rate']).minimize(mse)
        net.run(tf.global_variables_initializer())
        
        RNNC.out=out
        RNNC.X=X
        RNNC.Y=Y
        RNNC.opt=opt
        RNNC.net=net
        RNNC.mse=mse
        return net

    @staticmethod
    def rnnClassifier(df,target_lbl,t = 0.7, 
                      nparams={'hlayers': 3, 'nnlayers': [512,256,128], 'dropOut':-1, 
                                'earlyStop':False, 'esMetric':'mae', 'batchNorm': -1, 'sAlgo':'mbgd', 'sParam': 100},
                      initializer={'sigma':1,'mode':'fan_avg', 'distribution':'uniform'},
                      optimizer={'optimizer':'adam', 'loss':'mse','learning_rate' : 0.001, 'metrics':['mae']}, 
                      table=False):
        
        bd,cols,X_train,y_train,X_test,y_test=RNNC.splitDataset(df,target_lbl,t = t)

        if nparams['sAlgo']=='mbgd':                #any value greater than 1 for Minibatch Gradient Descent
            RNNC.batch_size=nparams['sParam']
        elif nparams['sAlgo']=='sgd':
            RNNC.batch_size=1                      #batch_size argument to 1 for stochastic gradient descent
        elif nparams['sAlgo']=='bgd':
            RNNC.batch_size=len(X_train)           #len(X_train) for Batch Gradient Descent 

        net = RNNC.initNetwork(len(cols),nparams,initializer,optimizer)
        count=0
        cost_history = np.empty(shape=[1], dtype = float)
        for e in range(RNNC.epochs):
            shuffle_data = np.random.permutation(np.arange(len(y_train)))
            X_train = X_train[shuffle_data]
            y_train = y_train[shuffle_data]
            for i in range(0, len(y_train) // RNNC.batch_size):
                start = i * RNNC.batch_size
                batch_x = X_train[start:start + RNNC.batch_size]
                batch_y = y_train[start:start + RNNC.batch_size]
                net.run(RNNC.opt, feed_dict={RNNC.X: batch_x, RNNC.Y: batch_y}) 
            cost_ = net.run(RNNC.mse, feed_dict={ RNNC.X:X_train, RNNC.Y: y_train})
            cost_history = np.append(cost_history, cost_)
            count=count+1
            if (count % 1000 == 0):
                    print("Reached epoch",count,"cost J =", cost_)
        RNNC.net=net
        
        pred = net.run(RNNC.out, feed_dict={RNNC.X: X_train}) 
        yhat = (1*(pred[0] > 0.5)).astype(int)
        y_score = RNNC.predict_proba(X_train)[:, 1] # for the first column [:, 0] depending on construction and requirment
        score=0
        if t < 1:  score = np.mean((yhat - y_train)**2)
        ret={'model':RNNC,'mse':score,'stats':None,'yhat':yhat,'probs':y_score,'cost_h':cost_history}
        
        if table: 
            sts=RNNC.md_Diag(RNNC, X_test, y_test,plot=True,pr_curve=True,table=True)
            y_means, proba_means=RNNC.plot_probCal(y_test, RNNC.predict_proba(X_test)[:, 1])
            RNNC.computeCalibrated(RNNC, RNNC.predict_proba(X_test)[:, 1], X_train,y_train, y_test, X_test)
            RNNC.plot_costHist(cost_history)
            ret['stats']=sts
        return ret
        
########################################################
# Recurrent Neural Network Forecasting (tensorflow V2)
########################################################      
class RNNCK(NNBC):
    
    @staticmethod
    def getProbs(model,df,features):
        return RNNCK.predict_proba(np.asarray(df[features]))[:, 1]

    @staticmethod
    def predict(X_data):
        return (1*(RNNCK.net.predict(X_data) > 0.5)).astype(int)[:, 0]
        
    @staticmethod
    def predict_proba(X_data):
        import math

        def gf(x):
            if x <= 0: 
                x=0
            return x

        def stable_sigmoid(x):
            if x >= 0:
                z = math.exp(-x)
                sig = 1 / (1 + z)
                return sig
            else:
                z = math.exp(x)
                sig = z / (1 + z)
                return sig
        
        treshold=stable_sigmoid(0.5)
        nn_preds = RNNCK.net.predict(X_data)
        probs=[stable_sigmoid(e) for e in nn_preds]
        probs=np.asarray(probs)
        probs=(probs-(treshold-0.5)).round(3) 
        probs= np.asarray([gf(e) for e in probs])
        return np.asarray([[e,1-e] for e in probs]) 
    
    @staticmethod
    def rnnkClassifier(df,target_lbl, t = 0.7, 
                       nparams={'hlayers': 3, 'nnlayers': [512,256,128], 'dropOut':-1, 'earlyStop':False, 
                                'esMetric':'mae', 'batchNorm': -1, 'sAlgo':'mbgd', 'sParam': 100},
                       initializer={'sigma':1,'mode':'fan_avg', 'distribution':'uniform'},
                       optimizer={'optimizer':'adam', 'loss':'mse','learning_rate' : 0.001, 'metrics':['mae']}, 
                       table=False):
        
        from tensorflow import keras as k
        from tensorflow.keras.callbacks import EarlyStopping
        import tensorflow as tf
        
        tf.random.set_seed(RNNCK.rng)
        
        bd,cols,X_train,y_train,X_test,y_test=RNNCK.splitDataset(df, target_lbl, t = t)
        
        n_features = len(cols)
        if nparams['sAlgo']=='mbgd':                #any value greater than 1 for Minibatch Gradient Descent
            RNNCK.batch_size=nparams['sParam']
        elif nparams['sAlgo']=='sgd':
            RNNCK.batch_size=1                      #batch_size argument to 1 for stochastic gradient descent
        elif nparams['sAlgo']=='bgd':
            RNNCK.batch_size=len(X_train)           #len(X_train) for Batch Gradient Descent 
            
        clf = k.Sequential()
        my_init = k.initializers.VarianceScaling(
           scale = initializer['sigma'], mode = initializer['mode'], distribution = initializer['distribution'], seed = RNNCK.rng) 

        for i in range(0,nparams['hlayers']):     #Batch normalization is a technique for training very deep neural networks that standardizes 
            if nparams['batchNorm']==i:           #the inputs to a layer for each mini-batch. This has the effect of 
                clf.add(k.layers.BatchNormalization()) #stabilizing the learning process and strongly reducing training epochs
            if nparams['dropOut']==i:             #Dropout is a clever regularization method that reduces overfitting of the training dataset 
                clf.add(k.layers.Dropout(0.5))    #where some number of layer outputs are randomly ignored during training
            if i==0:
                clf.add(k.layers.Dense(nparams['nnlayers'][i], activation='relu', kernel_initializer=my_init, input_shape=(n_features,)))
            else:
                clf.add(k.layers.Dense(nparams['nnlayers'][i], activation='relu', kernel_initializer=my_init))
        
        clf.add(k.layers.Dense(1, activation='sigmoid'))
        clf.compile(optimizer=optimizer['optimizer'], loss=optimizer['loss'],learning_rate = optimizer['learning_rate'], metrics=optimizer['metrics']) 
        es=None                                  #early stopping involves monitoring the loss on the training dataset and a validation dataset 
        if nparams['earlyStop']:                 #for signs of overfitting, then the training process can be stopped.
            es = [EarlyStopping(monitor=nparams['esMetric'], patience=int(RNNCK.epochs/2))]
        model=clf.fit(X_train, y_train, epochs=RNNCK.epochs, batch_size=RNNCK.batch_size, verbose=0,validation_split=0.0,callbacks=es)
        RNNCK.net=clf
        
        yhat=(1*(clf.predict(X_train) > 0.5)).astype(int)[:, 0]
        y_score=RNNCK.predict_proba(X_train)[:, 1] # [:, 0] for the first column depending on construction
        score=0
        if t < 1:  score = np.mean((yhat - y_train)**2)
        ret={'model':RNNCK,'mse':score,'stats':None,'yhat':yhat,'probs':y_score,'cost_h':model.history}
        if table: 
            sts=RNNCK.md_Diag(RNNCK, X_test, y_test,plot=True,pr_curve=True,table=True)
            y_means, proba_means=RNNCK.plot_probCal(y_test, RNNCK.predict_proba(X_test)[:, 1])
            RNNCK.computeCalibrated(RNNCK, RNNCK.predict_proba(X_test)[:, 1], X_train,y_train, y_test, X_test)
            RNNCK.plot_costHist(model.history)
            ret['stats']=sts
            
        return ret #[score,pred,y_score,model]

########################################################
# Random Forest Forecasting (Greedy Split)
########################################################      
class RFC(DTC): 
    
    @staticmethod
    def make_params(rg1='', n_est=30, max_depth=1000, max_features=10, min_samples_split=2, min_samples_leaf=1, ccp_alpha=0):
        import itertools

        regr_ = [rg1]
        split_ = [1]
        if n_est <= 25: n_est = 100
        nestimators_ = list(range(10,n_est,25))
        crit_ = ['gini', 'entropy']
        max_depth_ =[max_depth, None]
        if min_samples_split < 2 : min_samples_split=2
        if min_samples_leaf < 1 : min_samples_leaf=1
        minss_= list(range(2,min_samples_split+1))
        minsl_ = list(range(1,min_samples_leaf+1))
        max_feat_ = ['sqrt','log2',max_features]
        ccpa_ = [ccp_alpha]
        tbl_ = [False]
        lparams=[regr_,split_,nestimators_,crit_,max_depth_,minss_,minsl_,max_feat_,ccpa_,tbl_]
        cfg_param_list = list(itertools.product(*lparams))  
        return cfg_param_list
            
    @staticmethod
    def randomForestClassifier(df,target_lbl,t = 0.7, n_est=30, crit='gini',md=None,mss=2,msl=1,maxf='sqrt',ccpa=0,table=False):
        from sklearn.ensemble import RandomForestClassifier as RFCC

        bd,cols,xl_train,yl_train,xl_test,yl_test=RFC.splitDataset(df,target_lbl,t = t)

        clf  = RFCC(n_estimators=n_est, criterion=crit, max_depth=md, min_samples_split=mss, 
                    min_samples_leaf=msl, max_features=maxf, ccp_alpha=ccpa, n_jobs=4, warm_start=True, random_state=RFC.rng)
        model=clf.fit(xl_train,yl_train)
        yhat=clf.predict(xl_train)
        y_score=clf.predict_proba(xl_train)[:, 1]
        
        score=0
        if t < 1:  score = np.mean((yhat - yl_train)**2)
        ret={'model':clf,'mse':score,'stats':None,'yhat':yhat,'probs':y_score}
        
        if table: 
            sts=RFC.md_Diag(model, xl_test, yl_test,plot=True,pr_curve=True,table=True)
            y_means, proba_means=RFC.plot_probCal(yl_test, clf.predict_proba(xl_test)[:, 1])
            RFC.computeCalibrated(model, clf.predict_proba(xl_test)[:, 1], xl_train,yl_train, yl_test, xl_test)
            #RFC.pruningCostCompl(model, xl_train, yl_train)
            ret['stats']=sts
            
        return ret #[score,pred,y_score,model]
        
########################################################
# Random Forest Forecasting (Random Split)
########################################################      
class ETC(RFC): 
    
    @staticmethod
    def randomForestClassifier(df,target_lbl,t = 0.7, n_est=30, crit='gini',md=None,mss=2,msl=1,maxf='sqrt',ccpa=0,table=False):
        from sklearn.ensemble import ExtraTreesClassifier as ETCC

        bd,cols,xl_train,yl_train,xl_test,yl_test=ETC.splitDataset(df,target_lbl,t = t)

        clf  = ETCC(n_estimators=n_est, criterion=crit, max_depth=md, min_samples_split=mss, 
                    min_samples_leaf=msl, max_features=maxf, ccp_alpha=ccpa, n_jobs=4, warm_start=True, random_state=ETC.rng)
        model=clf.fit(xl_train,yl_train)
        yhat=clf.predict(xl_train)
        y_score=clf.predict_proba(xl_train)[:, 1]
        
        score=0
        if t < 1:  score = np.mean((yhat - yl_train)**2)
        ret={'model':clf,'mse':score,'stats':None,'yhat':yhat,'probs':y_score}
        
        if table: 
            sts=ETC.md_Diag(model, xl_test, yl_test,plot=True,pr_curve=True,table=True)
            y_means, proba_means=ETC.plot_probCal(yl_test, clf.predict_proba(xl_test)[:, 1])
            ETC.computeCalibrated(model, clf.predict_proba(xl_test)[:, 1], xl_train,yl_train, yl_test, xl_test)
            #RFC.pruningCostCompl(model, xl_train, yl_train)
            ret['stats']=sts
            
        return ret #[score,pred,y_score,model]

########################################################
# Xgboost Tree aggregation
########################################################      
class XGB(DTC): 
    
    @staticmethod
    def make_params(rg1='',n_est=30, md=15, mss=2, mds=2,msl=2,obj='binary:logistic',em='auc',lr=0.3,il=[]):
        import itertools

        regr_ = [rg1]
        split_ = [1]

        n_est_=list(range(n_est,3*n_est+1,n_est))
        md_ = list(range(0,md+1,4))
        msl_= list(range(0,msl+1,1)) #0.5
        mds_= list(range(0,mds+1,1)) #0.5
        mss_= list(range(0,mss+1,1)) #0.5

        obj_=[obj]
        em_=[em]

        lr_=[0.10,0.20,0.30]
        il_=[[]]

        tbl_ = [False]

        lparams=[regr_,split_,n_est_,md_,mss_,msl_,mds_,obj_,em_,lr_,il_, tbl_]

        pl = list(itertools.product(*lparams))  
        cfg_param_list=[]
        for e in pl:
            xp={'n_est':e[2], 'md':e[3],'mss':e[4],'msl':e[5],'mds':e[6],'obj':e[7],'em':e[8],'lr':e[9],'il':e[10]}
            cfg_param_list.append(tuple(regr_+split_+[xp]+tbl_))

        return cfg_param_list
            
    @staticmethod
    def gradientBoostingClassifier(df,target_lbl,t = 0.7, xparams={'n_est':30, 'md':6,'mss':2,'msl':1,'mds':0,'obj':'binary:logistic','em':'auc','lr':0.3,'il':[]}, table=False):
        from xgboost import XGBRFClassifier as XGBCC
        
        bd,cols,xl_train,yl_train,xl_test,yl_test=XGB.splitDataset(df,target_lbl,t = t)
        #scale_pos_weight = sum(negative instances) / sum(positive instances)
        #Classification ->  objective=binary:logistic and eval_metric=auc 
        #               ->  objective=multi:softprob and eval_metric=auc
        #Regression     -> objective=reg:squaredlogerror and eval_metric=rmsle       
        #interaction_constraints = [[0, 1], [2, 3, 4]] or [['cn0', 'cn1'], ['cn2', 'cn3', 'cn4']]
        if xparams['il']==[]:clf  = XGBCC(n_estimators=xparams['n_est'], max_depth=xparams['md'], 
                                        gamma=xparams['msl'], min_child_weight=xparams['mss'], max_delta_step=xparams['mds'], 
                                        eta=xparams['lr'], objective=xparams['obj'], eval_metric=xparams['em'], 
                                          disable_default_eval_metric=1, seed=XGB.rnds)
        if xparams['il']!=[]:clf  = XGBCC(n_estimators=xparams['n_est'], max_depth=xparams['md'], 
                                        gamma=xparams['msl'], min_child_weight=xparams['mss'], max_delta_step=xparams['mds'], 
                                        eta=xparams['lr'], interaction_constraints=xparams['il'],
                                        objective=xparams['obj'], eval_metric=xparams['em'], 
                                          disable_default_eval_metric=1,seed=XGB.rnds) 
        model=clf.fit(xl_train,yl_train)
        yhat=clf.predict(xl_train)
        y_score=clf.predict_proba(xl_train)[:, 1]
        
        score=0
        if t < 1:  score = np.mean((yhat - yl_train)**2)
        ret={'model':clf,'mse':score,'stats':None,'yhat':yhat,'probs':y_score}
        
        if table: 
            sts=XGB.md_Diag(model, xl_test, yl_test,plot=True,pr_curve=True,table=True)
            y_means, proba_means=XGB.plot_probCal(yl_test, clf.predict_proba(xl_test)[:, 1])
            #XGB.computeCalibrated(model, clf.predict_proba(xl_test)[:, 1], xl_train,yl_train, yl_test, xl_test)
            #XGB.pruningCostCompl(model, xl_train, yl_train)
            ret['stats']=sts
            
        return ret #[score,pred,y_score,model]
        
