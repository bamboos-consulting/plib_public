#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone 
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# MLTrader
#
# Module including basic setup for ML models for trading strategies
# Based on ideas provided by the team at Quantra
#############################################################################################      

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import warnings
import pandas as pd
import numpy as np
import Plib.ML.Models as ml
import Plib.Signals.TAnalysis as ta
import Plib.Signals.Filters as flt

warnings.filterwarnings("ignore")

#pd.options.mode.chained_assignment = None  # default='warn'

DEBUG_PRINT=0

########################################################
# Regression Bands
########################################################      
class MLTlinreg(ml.linreg):
    
    @staticmethod
    def fs_generation(df,l_rets=[3,15,30],w_sma=[3,15,60],drop_nan=True):
        d1=ta.getVolumeGap(df)
        d1=ta.getDailyChange(d1)
        d1=ta.getOpenSpread(d1)
        d1=ta.getLaggedRets(d1,periods=l_rets)
        d1=ta.getSMAs(d1,periods=w_sma)
        fast=min(w_sma)
        d1=ta.getSMACorr(d1,lbl_sma='sma_'+str(fast))
        nan_index=max(max(w_sma),max(l_rets))
        if drop_nan:
            d1=d1[nan_index:]
            d1=d1.dropna()
        features=[c for c in d1.columns if c not in ['Open','High','Low','Close','Adjusted_close','Volume']]
        return d1,list(features)
            
    @staticmethod
    def makeStudy(df,features=[],regrnd1='fractHigh_',regrnd2='fractLow_',test_periods=252,split=0.85,table=False):
        df=df[:-test_periods].copy()
        lmU=MLTlinreg.regression(df[features],regrnd1,t = split,table=table)
        lmD=MLTlinreg.regression(df[features],regrnd2,t = split,table=table)
        return lmU,lmD
            
########################################################
# KNeighborsClassifier Forecasting
########################################################      
class MLTKNN(ml.KNNC):
                
    @staticmethod
    def fs_generation(df,l_rets=[3,15,30],w_sma=[3,15,60],drop_nan=True):
        d1=ta.getVolumeGap(df)
        d1=ta.getDailyChange(d1)
        d1=ta.getOpenSpread(d1)
        d1=ta.getLaggedRets(d1,periods=l_rets)
        d1=ta.getSMAs(d1,periods=w_sma)
        d1=ta.getMarketUpDown(d1)
        fast=min(w_sma)
        d1=ta.getSMACorr(d1,lbl_sma='sma_'+str(fast))
        nan_index=max(max(w_sma),max(l_rets))
        if drop_nan:
            d1=d1[nan_index:]
            d1=d1.dropna()
        features=[c for c in d1.columns if c not in ['Open','High','Low','Close','Adjusted_close','Volume','MarketUpDown_Close1day']]
        return d1,list(features)
    
    @staticmethod
    def makeStudy(df,features=[],regrnd1='',test_periods=252,split=0.85,table=False):
        df=df[:-test_periods].copy()
        df1 = MLTKNN.scaling(df[[*([regrnd1]+features)]],params={'method':'minmax','exclude':[]})
        stats = MLTKNN.kneighborsClassifier(df1,regrnd1,t = split,table=table)
        return stats
    
    @staticmethod
    def makeIndicator(sdata, train_periods=252, retrain_periods=180, forecast_periods=1, regrnd1='', params=(), label=''):
        #regrnd1='MarketUpDown_Close1day', params=(1, 1, 'chebyshev', 2, 'uniform', False)
        data,features=MLTKNN.fs_generation(sdata.copy())
        df1 = MLTKNN.scaling(data[[*([regrnd1]+features)]],params={'method':'minmax','exclude':[]})
        for t in range(0,len(df1)-train_periods+1):
            if t%retrain_periods==0:
                #(re)Train over the train_periods with the specified parameters and test window
                ret = MLTKNN.kneighborsClassifier(df1[:train_periods+t].copy(),regrnd1,*params)
            for d in range(0,forecast_periods):
                #Forecasts forecast_periods day using the available data
                tsignal=MLTKNN.getProbs(ret['model'],df1[0:train_periods+d+t].copy(),features)
            if t==0:
                signal = tsignal
            else:
                signal = np.append(signal, tsignal[-forecast_periods])  
        data['KNN_signal_'+label]=signal        
        return data 