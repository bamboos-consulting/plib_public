#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Market Data 
#
# Module comprising helper functions to setup data buffers
#############################################################################################      

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import multiprocessing as mp
import threading as t
import socket
import pickle
    
import Plib.Utils.Tools as tls
import Plib.Stats.SDEProc as sp
import pandas as pd
import numpy as np
import random
import time
import datetime
import signal

import warnings
warnings.filterwarnings("once")

SLEEP_TIME=0.3
MAX_SLEEP=30
DEBUG_PRINT=1
         
class Streamer():
    
    ptype=0
    pipe=None
    
    conn=None
    s=None
    addr=None
    
    stopSending=False
    HOST='127.0.0.1'
    PORT=2005
    MAX_BADCONNS=20
    WORD_SIZE=1048576   #32bit
    
    ##############################################
    # Constructor
    ##############################################                    
    def __init__(self,param={'ptype':0, 'HOST':'127.0.0.1', 'PORT':2005, 'conn':None}):
        
        self.ptype=param['ptype']
        
        if self.ptype==1: 
            self.HOST=param['HOST']
            self.PORT=param['PORT']
        if self.ptype==1: self.pipe=param['conn']
        
    ##############################################
    # Send data
    ##############################################                        
    def sendData(self,data,i,msg=0):
        if self.ptype==0: 
            msg = pickle.dumps([data,i,msg])
            try:
                self.conn.sendall(msg)
            except:
                pass
        if self.ptype==1: self.pipe.send([data,i,msg])
    
    ##############################################
    # Set the connection
    ##############################################                        
    def setConn(self):
        if self.ptype==0:
            #A socket object can be in one of three modes: blocking, non-blocking, or timeout. 
            #In blocking mode, operations block until complete or the system returns an error (such as connection timed out). 
            #In non-blocking mode, operations fail (with an error system-dependent) if they cannot be completed immediately
            #(functions from the select can be used to know when and whether a socket is available for reading or writing). 
            #In timeout mode, operations fail if they cannot be completed within the timeout specified for the socket (they raise a timeout
            #exception) or if the system returns an error. At the OS level, sockets in timeout mode are internally set in non-blocking mode. 
            #Also, the blocking and timeout modes are shared between file descriptors and socket objects that refer to the 
            #same network endpoint. This implementation detail can have visible consequences if e.g. you decide to use the fileno() of a socket.
            self.s= socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.s.bind((self.HOST, self.PORT))
            self.s.listen(self.MAX_BADCONNS)
            print('Started streaming server with PID '+str(os.getpid()))
            self.conn, self.addr = self.s.accept()
            print('Connected by', self.addr)
            
    ##############################################
    # Start the loops waiting for I/O
    ##############################################                        
    def start(self,K=20,streamer_fun=None):
        self.setConn()
        
        i=0
        while not self.stopSending:
            self.sendData(i,i)
            i=i+1
            if i==K: 
                self.stopSending=True
            time.sleep(0.3)
        self.stop()
        
            
    ##############################################
    # End the loop 
    ##############################################                        
    def stop(self):
        print('Shutdown server...')
        self.stopSending=True
        if self.conn is None: return
        if self.ptype==0: 
            msg = pickle.dumps([0,0,-1])
            self.conn.sendall(msg)
        if self.ptype==1: self.pipe.send([0,0,-1])
        
        if self.ptype==0: self.s.close()
        if self.ptype==1: self.pipe.close()
     
##############################################
# Self updating buffer based on OH
##############################################                    
class Simulator(Streamer):
    bufferRT={}
    bufferHD={}
    sec_param={}
    sim_param={}
    temp_data={}
    
    def __init__(self,sim_param={}, sec_param={},param_conn={}):
        #{'MSFT':{'price':302.1,'ltmean':300,'updf':None,'bsize':20},'AAPL':{'price':120.1,'ltmean':180,'updf':None,'bsize':20}}
        #sim_param={'proc_type':'ornstein_uhlenbeck','freq1':'1T','freq2':'D','pmissdata1':0.15,'pmissdata2':0,
        #'n_steps1':100,'n_steps2':100,'max_delay':1.3,'update_freq':20}
        self.sec_param=sec_param
        self.sim_param=sim_param
            
        #Realtime data
        for i in sec_param.keys():
            # dx(t) = k(t)*(theta(t) - x(t))*dt + sigma(t)*dw(t, dt)
            time_points=5      #Discretize time in five time points: delta=5
            n_steps=50          #INcrease with respect to sim_param['freq1']
            min_span=0.         #Range min (in days)
            max_span=2000      #Range max (in days)
            ltmean=sec_param[i]['ltmean']         #Long term mean to revert to (k)
            rev_rate=0.012        #Velocity or rate of reversion (theta)
            noise=0.6          #Noise term (sigma)
            x_0=sec_param[i]['price']              #Start price
            n_plot=20
            npaths=int(sim_param['n_tperiods1'])
            x,y=sp.ornstein_uhlenbeck(time_points,n_steps,min_span,max_span,
                                       ltmean,rev_rate,noise,x_0,n_plot,plot=False,npaths=npaths)
            index=pd.date_range(datetime.datetime.today(), periods=len(y[3]), freq=sim_param['freq1'], tz='UTC')
            for j in range(0,int(sim_param['pmissdata1']*len(index))):
                k=random.randint(0, len(index)-1)
                index.drop(index[k])
            ts=pd.DataFrame(y[3]).head(len(index))
            ts.index=index
            ts.columns=['Open']
            ts['Tkr']=i
            ts['High']=ts.Open*random.uniform(1.0, 1.02)
            ts['Low']=ts.Open*random.uniform(0.98, 1.0)
            ts['Close']=ts.Open*random.uniform(0.99, 1.02)
            ts['Volume']=ts.Open*random.uniform(300, 20000)
            ts['Bid']=0
            ts['Ask']=0
            ts['BidV']=0
            ts['AskV']=0
            ts['TWAP']=ts[['High','Low','Close']].mean(axis=1)
            ts['VWAP']=ts['TWAP']*ts['Volume']
            ts['VWAP']=(ts['VWAP']+ts['VWAP'].cumsum())/ts['Volume'].cumsum()
            ts['TS']=ts.index
            ts['TS']=pd.to_datetime(ts['TS'])
            ts=ts[['Tkr','Open','High','Low','Close','Volume','Bid','Ask','BidV','AskV','TWAP','VWAP','TS']]
            self.bufferRT[i]=ts.copy()
        print('Created '+str(len(y[3]))+' datapoints with frequency ' + str(sim_param['freq1']))
        print('Start '+str(ts.TS.min())+' - End ' + str(ts.TS.max()))
        minRT=ts.TS.min()
        
        #historical data
        for i in sec_param.keys():
            # dx(t) = k(t)*(theta(t) - x(t))*dt + sigma(t)*dw(t, dt)
            time_points=5       #Discretize time in five time points: delta=5
            n_steps=50          #INcrease with respect to sim_param['freq1']
            min_span=0.         #Range min (in days)
            max_span=2000      #Range max (in days)
            ltmean=sec_param[i]['ltmean']         #Long term mean to revert to (k)
            rev_rate=0.012        #Velocity or rate of reversion (theta)
            noise=0.6          #Noise term (sigma)
            x_0=sec_param[i]['price']              #Start price
            n_plot=20
            npaths=int(sim_param['n_tperiods2'])+int(sec_param[i]['bsize'])
            x,y=sp.ornstein_uhlenbeck(time_points,n_steps,min_span,max_span,
                                       ltmean,rev_rate,noise,x_0,n_plot,plot=False,npaths=npaths)
            
            freq=sim_param['freq2']
            back=sec_param[i]['bsize']
            tdelta=tls.getTDelta(back,freq)                                 #len(y[5])+back
            index=pd.date_range(datetime.datetime.today() - tdelta, periods=len(y[5]), freq=sim_param['freq2'], tz='UTC')
            for j in range(0,int(sim_param['pmissdata2']*len(index))):
                k=random.randint(0, len(index)-1)
                index.drop(index[k])
            index.drop(index[index <= minRT])
            ts=pd.DataFrame(y[5]).head(len(index))
            ts.index=index
            ts.columns=['Open']
            ts['Tkr']=i
            ts['High']=ts.Open*random.uniform(1.0, 1.05)
            ts['Low']=ts.Open*random.uniform(0.95, 1.0)
            ts['Close']=ts.Open*random.uniform(0.98, 1.02)
            ts['Volume']=ts.Open*random.uniform(300, 20000)
            ts['Bid']=0
            ts['Ask']=0
            ts['BidV']=0
            ts['AskV']=0
            ts['TWAP']=ts[['High','Low','Close']].mean(axis=1)
            ts['VWAP']=ts['TWAP']*ts['Volume']
            ts['VWAP']=(ts['VWAP']+ts['VWAP'].cumsum())/ts['Volume'].cumsum()
            ts['TS']=ts.index
            ts['TS']=pd.to_datetime(ts['TS'])
            ts=ts[['Tkr','Open','High','Low','Close','Volume','Bid','Ask','BidV','AskV','TWAP','VWAP','TS']]
            self.bufferHD[i]=ts.copy()
            if sec_param[i]['updf']!=None: 
                self.indicators=sec_param[i]['updf']
        print('Created '+str(len(y[5]))+' datapoints with frequency ' + str(sim_param['freq2']))
        print('Start '+str(ts.TS.min())+' - End ' + str(ts.TS.max()))
        
        #Initialize parent
        #param_conn={'ptype':0, 'HOST':'127.0.0.1', 'PORT':2005, 'conn':None}
        super().__init__(param_conn)
    
        
    ##############################################
    # Implementation of indicators function
    ##############################################                             
    @staticmethod
    def indicators():
        return
        
    ##############################################
    # Implementation of parent sendData
    ##############################################                             
    def start(self,k=10):
        super(Simulator, self).setConn()
        z=0
        f=0
        hf=tls.freqMin(self.sim_param['freq1']) #1
        lf=tls.freqMin(self.sim_param['freq2']) #60
        u=self.sim_param['update_freq']     #30
        
        i=0
        while not self.stopSending:
            #Update historical data metrics
            if (i==0) or (i % u == 0):
                f=f+u
                if f % lf==0:
                    z=z+1
                #Store in class
                if not (self.bufferHD is None):
                    for s in self.sec_param.keys():
                        self.temp_data=self.indicators(self.bufferHD[s].iloc[z:z+self.sec_param[s]['bsize']-1])
            #Select randomly a security
            t=random.randint(0, len(self.sec_param)-1)
            tkr=list(self.sec_param.keys())[t]
            #Stream data and historical metrics
            super(Simulator, self).sendData(self.temp_data,i,msg=0)
            super(Simulator, self).sendData(self.bufferRT[tkr].iloc[i],i,msg=0)
            time.sleep(random.uniform(0.00001, self.sim_param['max_delay']))
            
            i=i+1
            if i==k: 
                self.stopSending=True
        self.stop()
        
