#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Backtrader 
#
# Module including functions to perform backtesting
#############################################################################################      

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import datetime
import pandas as pd
import numpy as np
import time as t
import random
import socket
import multiprocessing
import signal
import pickle
import Plib.Utils.Tools as tls
import Plib.Portfolio.Analysis as pmp
import Plib.Ledger.Transactions as tr
import Plib.Ledger.AOrders as aor
import Plib.Ledger.EOrders as eor
from Plib.Backtester.Backtrader import Buffer
from Plib.AlgoTrader.MktData import Connector

import warnings
warnings.filterwarnings("once")
#pd.options.mode.chained_assignment = None  # default='warn'

DEBUG_PRINT=4

app=None
brkr=0
exch='SMART'
cur='USD'
clnt=0

#############################################################################################
# Class Buffer 
#
# Class including functions to store data and perform backtesting
#############################################################################################      
class Trader(Connector,Buffer):
    
    # Variables for data connection and initialization
    om=None
    last_msg=None
    algo_logic=None
    
    
    ##################################################
    # Constructor Initializing all shared variables
    ##################################################      
    def __init__(self,ordmgr=None,init_params={},tickers={},mkt_data={},rules={},param_conn={}):  
        
        self.buffer_blinders={}
        self.buffer_params={}
        self.running_copy=[]
        self.running_index=0
        self.stat1=[]
        self.stat2=[]
        self.algo_params=[]
        self.str_params={}
        
        self.freq=init_params['freq']   
        self.tz=init_params['tz']    
        tdelta=tls.getTDelta(2,init_params['freq']  )
        df=pd.date_range(datetime.datetime.today()-tdelta, periods=2, freq=init_params['freq']  , tz=init_params['tz'])
        self.index=pd.DataFrame(df,columns=['Date'])
        self.index=self.index.set_index('Date')
        self.index = self.index.sort_index(ascending=True)
        self.buffer=self.index
        
        if rules != {}: self.rules=rules                                    
        
        for s in list(tickers.keys()).copy():
            self.buffer_blinders[s]=s
            sec_params=tickers[s].copy()
            params={'Commission':sec_params[0],'Slippage':sec_params[1],'ProfTarget':sec_params[2],'StopLoss':sec_params[3]}
            self.buffer_params[s]=params
        
        ordmgr.simulator=init_params['simulator']  
        self.om=ordmgr
        self.om.enableOM(broker=init_params['brk'],exchange=init_params['exc'],db=init_params['db'],cli=init_params['cli'],in_mem=init_params['in_mem'])
        self.sid=self.om.openStrategy(init_params['str_name'],init_params['str_desc'])
        
        #param_conn={'ptype':0, 'HOST':'127.0.0.1', 'PORT':2005, 'conn':None}
        super().__init__(param_conn)
        
    ##################################################
    # Implements Parent Methods
    ##################################################      
    def acquireData(self):
        self.last_msg=super(Trader, self).acquireData()
        self.run(self.algo_logic)
        
    def start(self,algo_logic):
        self.algo_logic=algo_logic
        self.run(None)
        super(Trader, self).start() 
        
    ##################################################
    # Prepare the accounts for running
    ##################################################      
    def finalizeData(self,usd_account=100000,params={'Type':'VOID'}):
        self.buffer['Timestamp']=tls.getLTimestamp(self.tz)[1]
        super(Trader, self).finalizeData(usd_account,params)
                             
    ##################################################
    # Execute the strategy
    ##################################################      
    def run(self, algo_logic):
        
        # Custom rules - others to be added in rules factory
        def bracketingATR(i:int,sec:str,price:float,side:int):
            #to be implemented as follows:
            #HCW = max(HIGHS,win=30); LCW = min(LOWS,win=30) 
            #buy:   sl = HCW - (ATR x sl_factor) ; tp = HCW + (ATR x tp_factor)
            #sell:  sl = LCW + (ATR x sl_factor) ; tp = LCW - (ATR x tp_factor)
            if side==1:
                sl=x['HCW_'+sec].iloc[i]-(x['ATR_'+sec].iloc[i]*self.buffer_params[sec]['StopLoss'])
                tp=x['HCW_'+sec].iloc[i]+(x['ATR_'+sec].iloc[i]*self.buffer_params[sec]['ProfTarget'])
            else:
                sl=x['LCW_'+sec].iloc[i]+(x['ATR_'+sec].iloc[i]*self.buffer_params[sec]['StopLoss'])
                tp=x['LCW_'+sec].iloc[i]-(x['ATR_'+sec].iloc[i]*self.buffer_params[sec]['ProfTarget'])    
            return tp,sl
            
        def squaringAlt(i:int,sec:str,side:int,price:float,open:bool=True):
            ret=0
            if open:
                if (side != x[sec+'_last'].iloc[i-1]) and (x[sec+'_prt'].iloc[i-1] >0):
                    trade(i,str(sec),side,float(price),float(1),False)
                    ret=0
                elif x[sec+'_prt'].iloc[i-1] ==0:
                    ret=0
                else:    
                    ret=-1
            if DEBUG_PRINT> 3: print('Alternating Squaring ', ret,i,side,x[sec+'_last'].iloc[i-1],x[sec+'_prt'].iloc[i-1])
            return ret
        
        def squaringTPSL(i:int,sec:str,side:int,price:float,open:bool=True):
            ret=0
            if open:
                n=0
                if x[sec+'_last'].iloc[i] != 0:  ### i-1
                    n=checkStops(sec,price,i)
                #if DEBUG_PRINT> 2: print('Checkstops ', i, n,x[sec+'_last'].iloc[i])
                #TP/SL Precedence - Ignore signal if already traded
                if n>0:
                    #checkStops closed a trade
                    x[sec+'_last'].iloc[i]=0
                    ret= -1
                elif n==0 and x[sec+'_last'].iloc[i]!=0:
                    #checkStops did not closed a trade, skip
                    ret= -1
                elif n==0 and x[sec+'_last'].iloc[i]==0:
                    #no open trades 
                    x[sec+'_last'].iloc[i]=int(side)
                    ret=0 
            return ret                                   

        def squaringATR(i:int,sec:str,side:int,price:float,open:bool=True):
            ret=0
            if open:
                n=0
                if x[sec+'_last'].iloc[i] != 0:
                    n=checkStopsATR(sec,price,x['High_'+sec].iloc[i],x['Low_'+sec].iloc[i],i)
                #if DEBUG_PRINT> 2: print('Checkstops ', i, n,x['High_'+sec].iloc[i],x['Low_'+sec].iloc[i])
                #TP/SL Precedence - Ignore signal if already traded
                if n>0:
                    #checkStops closed a trade
                    x[sec+'_last'].iloc[i]=0
                    ret= -1
                elif n==0 and x[sec+'_last'].iloc[i]!=0:
                    #checkStops did not closed a trade, skip
                    ret= -1
                elif n==0 and x[sec+'_last'].iloc[i]==0:
                    #no open trades 
                    x[sec+'_last'].iloc[i]=int(side)
                    ret=0
            return ret                                   
        
        def slippageFixed(sec:str,price:float,side:int):
            if side==1:
                slippage=round(float(price)*(1+float(self.buffer_params[sec]['Slippage'])),4)
            else:
                slippage=round(float(price)*(1-float(self.buffer_params[sec]['Slippage'])),4)
            return 
        
        def commissionsFixed(sec:str,price:float,qty:float):
            return float(self.buffer_params[sec]['Commission'])
        
        def kellyFraction(args={'price':0,'kelly':0,'wealth':0,'fraction':0,'qty':0}):
            qty=args['qty']
            if args['price'] >0:
                bet=args['kelly']*args['wealth']
                qty=np.trunc(args['fraction']*bet/args['price'])
            return qty
    
        def fixedPercent(args={'price':0,'wealth':0,'fraction':0,'qty':0}):
            qty=args['qty']
            if args['price'] >0:
                bet=args['fraction']*args['wealth']
                qty=np.trunc(bet/args['price'])
            return qty
                              
        # Prebuilt rules
        def bracketingRule(i:int,sec:str,price:float,side:int):
            tp=price+(side)*price*float(self.buffer_params[sec]['ProfTarget'])/100
            sl=price-(side)*price*float(self.buffer_params[sec]['StopLoss'])/100
            return tp,sl
        
        def squaringRule(i:int,sec:str,side:int,price:float,open:bool=True):
            if DEBUG_PRINT> 3: print('No Squaring ', 0)
            x[sec+'_last'].iloc[i]=int(side)
            return 0
        
        def slippageRule(sec:str,price:float,side:int):
            def slippage(price:float,side:int,slippage:float):
                def slip(price,side,slippage):
                    return abs(price * (side+slippage))
                def noslip(price,side,slippage):
                    return price    
                return random.choice([slip,noslip])(price,side,slippage)
                
            return round(float(slippage(price,side,float(self.buffer_params[sec]['Slippage']))),4)
        
        def commissionsRule(sec:str,price:float,qty:float):
            return round(abs(price*qty*float(self.buffer_params[sec]['Commission'])),4)
        
        def tradefilterRule(i:int,sec:str,side:int,price:float,open:bool=True):
            if DEBUG_PRINT> 3: print('No Filter ', 0)
            return 0
        
        def moneyMgmt(args={'qty':1}):
            return args['qty']
            
        # Method to perform standard operations  
        def trade(i:int,sec:str,side:int,price:float,qty:float,open:bool=True,aoid:int=0,pri:float=0):                                                        
            x[sec+'_signal'].iloc[i]=side
            if squaringRule(i,sec,side,price,open)!=0: 
                return                                #Check squaring rule
            if tradefilterRule(i,sec,side,price,open)!=0: 
                return                             
            if DEBUG_PRINT> 2: print('Trading ',sec,x[sec+'_signal'].iloc[i],qty,open,x['Date'].iloc[i],i)
            x.Traded.iloc[i]=side                                                            #Used for montecarlo analysis
            x[sec+'_last'].iloc[i]=side
            price_ns=price
            price=slippageRule(sec,price,side)
            comm=commissionsRule(sec,price,qty)                                              #Computing commissions
            slipg=round(abs(price_ns-price),4)                                               #Computing slippage
            x[sec+'_cm'].iloc[i]=x[sec+'_cm'].iloc[i]+comm                                   #Updating commissions
            x[sec+'_sp'].iloc[i]=x[sec+'_sp'].iloc[i]+slipg                                  #Updating slippage
            x[sec+'_pr'].iloc[i]=price
            x[sec+'_sh'].iloc[i]= qty  
            tp,sl=bracketingRule(i,sec,price,side)
            x[sec+'_TP'].iloc[i]= tp
            x[sec+'_SL'].iloc[i]= sl
            x[sec+'_OT'].iloc[i]= 0
            params=[0,1,tp,sl,x['Date'].iloc[i],comm,slipg]
            if open:
                if side ==-1:   #Opening a short position                                          
                    x.Account.iloc[i]=x.Account.iloc[i] - price*qty                          #Decreasing account of invested amount
                    x.Short.iloc[i]=x.Short.iloc[i] + price*qty                              #Increasing short
                    x[sec+'_prt'].iloc[i]=x[sec+'_prt'].iloc[i]-qty                          #Updating Shares Running Total
                else:           #Opening a long position 
                    x.Account.iloc[i]=x.Account.iloc[i] - price*qty                          #Decreasing account of invested amount
                    x.Long.iloc[i]=x.Long.iloc[i] + price*qty                                #Increasing long
                    x[sec+'_prt'].iloc[i]=x[sec+'_prt'].iloc[i]+qty
                if DEBUG_PRINT> 3: print('Opening Filled/Runn_total ',x[sec+'_sh'].iloc[i],x[sec+'_prt'].iloc[i])
                if om.simulator >0:
                    aoid=aor.createAOrder(self.sid,sec, price, qty, side, 'STK','','',0,params,'O')
                    eoid=eor.createEOrder(self.sid,aoid,'','',str(price),qty,str('sotype'),str('STK'),x['Date'].iloc[i],comm,slipg)
                else:
                    om.placeOrderIB(strategy_id,sec, qty, side, trd,price,expiration='',strike=0,otype='',odate=x['Date'].iloc[i],scomp_type='O')
                if DEBUG_PRINT> 3: print('Recorded ',sec,side,qty,open)
            else:
                if side ==-1:   #closing a long with a short
                    x.Account.iloc[i]=x.Account.iloc[i] + (price)*qty                        #increasing account of gain/loss
                    x.Long.iloc[i]=x.Long.iloc[i] - pri*qty                                  #decreasing long of initial amount
                    x[sec+'_prt'].iloc[i]=x[sec+'_prt'].iloc[i]-qty
                else:           #closing a short with a long
                    x.Account.iloc[i]=x.Account.iloc[i] - (price-pri)*qty                    #increasing account of gain/loss
                    x.Short.iloc[i]=x.Short.iloc[i] - pri*qty                                #decreasing short of initial amount
                    x[sec+'_prt'].iloc[i]=x[sec+'_prt'].iloc[i]+qty
                if DEBUG_PRINT> 3: print('Closing Filled/Runn_total ',x[sec+'_sh'].iloc[i],x[sec+'_prt'].iloc[i])
                if om.simulator >0:
                    aoid=aor.createAOrder(self.sid,sec, price, qty, side, 'STK','','',0,params,'C')
                    eoid=eor.createEOrder(self.sid,aoid,'','',str(price),qty,str('sotype'),str('STK'),x['Date'].iloc[i],comm,slipg)
                else:
                    om.placeOrderIB(strategy_id,sec, qty, side, trd,price,expiration='',strike=0,otype='',odate=x['Date'].iloc[i],scomp_type='C')
                if DEBUG_PRINT> 3: print('Recorded ',sec,side,qty,open)
                                                                    
        def checkStops(l:str,mp:float,i:int):
            sel=x.iloc[:i-1,]
            df=sel[['Date',str(l)+'_sh',str(l)+'_pr',str(l)+'_last',str(l)+'_TP',str(l)+'_SL',str(l)+'_CK']].copy()
            #df=df.reset_index()
            df=df.reset_index(drop=True)
            df.columns=['DateI','QtyI','PrI','SideI','TP','SL','CK']
            df.QtyI.replace(0,np.nan,inplace=True)
            df.CK.replace(1,np.nan,inplace=True)
            df=df.dropna()
            short=df[(df.SideI==-1)&((df.TP>mp)|(df.SL<mp))]
            long=df[(df.SideI==1)&((df.TP<mp)|(df.SL>mp))]
            trades=short.append(long)
            N=len(trades)
            if N>0:
                for k in range(0,N): #N-1
                    if DEBUG_PRINT> 2: print('Closing TP/SL -----------------',i,l,int(trades.SideI.iloc[k]),trades.QtyI.iloc[k],mp)
                    trade(i,l,-1*int(trades.SideI.iloc[k]),float(mp),float(trades.QtyI.iloc[k]),False,0,float(trades.PrI.iloc[k]))
                    x[l+'_CK'].loc[x.Date==trades.DateI.iloc[k]]=1
            return len(trades)
        
        def checkStopsATR(l:str,mp:float,high:float,low:float,i:int):
            sel=x.iloc[:i-1,]
            df=sel[['Date',str(l)+'_sh',str(l)+'_pr',str(l)+'_last',str(l)+'_TP',str(l)+'_SL',str(l)+'_CK']].copy()
            #df=df.reset_index()
            df=df.reset_index(drop=True)
            df.columns=['DateI','QtyI','PrI','SideI','TP','SL','CK']
            df.QtyI.replace(0,np.nan,inplace=True)
            df.CK.replace(1,np.nan,inplace=True)
            df=df.dropna()
            short=df[(df.SideI==-1)&((df.TP>low)|(df.SL<high))]
            long=df[(df.SideI==1)&((df.TP<low)|(df.SL>high))]
            trades=short.append(long)
            N=len(trades)
            if N>0:
                for k in range(0,N): #N-1
                    if DEBUG_PRINT> 2: print('Closing TP/SL -----------------',i,l,int(trades.SideI.iloc[k]),1,mp,high,low)
                    trade(i,l,-1*int(trades.SideI.iloc[k]),float(mp),float(trades.QtyI.iloc[k]),False,0,float(trades.PrI.iloc[k]))
                    x[l+'_CK'].loc[x.Date==trades.DateI.iloc[k]]=1
            return len(trades)
        
        def closeAllTrades(sec:str,mp:float,i:int):
            for l in self.buffer_blinders.keys():
                #qty=(x[str(l)+'_sh']*x[str(l)+'_signal']).sum()
                qty=abs(x[str(l)+'_prt'].iloc[i-1])
                side=x[str(l)+'_last'].iloc[i-1]*-1
                if qty != 0:
                    if DEBUG_PRINT> 3: print('Squaring trade...',sec,side,qty)
                    trade(i,str(sec),int(side),float(mp),float(qty),False)
                    #update db with open orders executed to be closed with check on quantity                  
            if self.str_params != {}:
                if self.str_params['Type']=='CURRW':
                    x[sec+'_pr']=x['Adjusted_close_'+str(sec)]       
        
        if self.rules['Squaring'] == 1:
            squaringRule=squaringAlt
        elif self.rules['Squaring'] == 2:
            squaringRule=squaringTPSL
        elif self.rules['Squaring'] == 3:
            squaringRule=squaringATR
        if self.rules['Bracketing'] == 1:
            bracketingRule=bracketingATR
        if self.rules['Slippage'] == 1:
            slippageRule=slippageFixed
        if self.rules['Commissions'] == 1:
            commissionsRule=commissionsFixed
        if self.rules['MoneyMgmt'] == 1:
            moneyMgmt=kellyFraction
        elif self.rules['MoneyMgmt'] == 2:
            moneyMgmt=fixedPercent
                
        #Initialization
        if algo_logic is None:
            self.running_copy=self.buffer.copy()       #1
            
            #Purchase/sell the initial weights, if any               
            if self.str_params != {}:
                if self.str_params['Type']=='CURRW':
                    
                    x=self.running_copy
                    i=len(x)
                    back_data=self.hdata    
                    om=self.om     
                    strategy_id=self.sid          
                    x=x.append(x.iloc[-1].copy())
                    x.Timestamp.iloc[-1]=mkt_data['TS']
                    x.Date.iloc[-1]=mkt_data['TS']
            
                    for sec in self.buffer_blinders.keys():
                        x[sec+'_cm'].iloc[i]=0
                        x[sec+'_sp'].iloc[i]=0
                        x[sec+'_pr'].iloc[i]=0
                        x[sec+'_sh'].iloc[i]= 0  
                        x[sec+'_TP'].iloc[i]= 0
                        x[sec+'_SL'].iloc[i]= 0
                        x[sec+'_OT'].iloc[i]= 0
                    x.Invested.iloc[i]=x.Long.iloc[i-1] + x.Short.iloc[i-1]
                        
                    for s in self.algo_params.keys():
                        if s != 'Type':
                            side=1
                            lmtprice=float(self.algo_params[str(s)]['PRICE'])
                            qty=int(self.algo_params[str(s)]['AVAL'])
                            sect=str(self.algo_params[str(s)]['STYPE'])
                            trd=1
                            if qty < 0: side=-1
                            trade(i,x,trd,s,side,lmtprice,qty)        
                            
        else:
            #Run trade logic
            #Check status message
            if self.last_msg[2]==0:
                if isinstance(self.last_msg[0], pd.Series):
                    #print('received md')
                    mkt_data=self.last_msg[0].copy()
                else:
                    #print('received hd')
                    self.hdata=self.last_msg[0].copy()
                    return
            elif self.last_msg[2]==-1:
                #Server shutdown 
                return
            elif self.last_msg[2]<=-2:
                #data error
                return
            else:
                #Manage errors, emergency squaring, etc.
                pass
                
            x=self.running_copy
            i=len(x)
            back_data=self.hdata    
            om=self.om     
            strategy_id=self.sid          
            x=x.append(x.iloc[-1].copy())       #<-------- x is reassigned and a copy is created, from this point 
                                                #          modifications are not carried onwards
                                                        
            x.Timestamp.iloc[-1]=mkt_data['TS']
            x.Date.iloc[-1]=mkt_data['TS']
            
            for sec in self.buffer_blinders.keys():
                x[sec+'_cm'].iloc[i]=0
                x[sec+'_sp'].iloc[i]=0
                x[sec+'_pr'].iloc[i]=0
                x[sec+'_sh'].iloc[i]= 0  
                x[sec+'_TP'].iloc[i]= 0
                x[sec+'_SL'].iloc[i]= 0
                x[sec+'_OT'].iloc[i]= 0
            x.Invested.iloc[i]=x.Long.iloc[i-1] + x.Short.iloc[i-1]
            
            algo_logic(i,x,om,mkt_data,back_data,trade,checkStops,closeAllTrades,moneyMgmt,self.algo_params)
            self.running_copy=x.copy()          # Because of the reassignement above, x must be reassigned again
                                                # .copy() can be removed

