#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Market Data 
#
# Module comprising helper functions to setup data buffers
#############################################################################################      

import numpy as np
import pandas as pd
import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import Plib.Utils.Tools as tls
import Plib.Stats.SDEProc as sp
import pandas as pd
import numpy as np
import random

import Plib.Brokers.IBapi as ib
import Plib.DataFarm.IEXdata as iex
import threading
import time
import datetime

#from multiprocessing import Process, Pipe
import socket
import multiprocessing
import signal
import pickle

SLEEP_TIME=0.3
MAX_SLEEP=30
DEBUG_PRINT=3

#import warnings
#warnings.filterwarnings("once")


#############################################################################################
# Class Connector 
#
# Class including functions to manage socket and pipes connection to market data
#############################################################################################      

class Connector():    
    ptype=0
    pipe=None
    
    conn=None
    s=None
    addr=None
    
    stopReceiving=False
    HOST='127.0.0.1'
    PORT=2005
    WORD_SIZE=1048576     #32bit
    
    ##################################################
    # Constructor 
    ##################################################      
    def __init__(self,param={'ptype':0, 'HOST':'127.0.0.1', 'PORT':2005, 'conn':None}):
        
        self.ptype=param['ptype']
        
        if self.ptype==1: 
            self.HOST=param['HOST']
            self.PORT=param['PORT']
        if self.ptype==1: self.pipe=param['conn']
    
    ##################################################
    # Read data from connection
    ##################################################          
    def acquireData(self):
        ret=[0,0,0]
        if self.ptype==0: 
            try:                #Receive data
                plist=self.s.recv(self.WORD_SIZE) #4096 is buffer chunk size
                ret = pickle.loads(plist)
            except:
                ret=[0,0,-2]
                pass
        if self.ptype==1: ret=self.pipe.recv()
        #Check for status messages
        if ret[2]==-1: 
            self.stop()
        return ret

    ##################################################
    # Start the loop forreading
    ##################################################                  
    def start(self):
        if self.ptype==0:
            self.s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.s.connect((self.HOST, self.PORT))
        
        while not self.stopReceiving:
            self.acquireData()
                
    ##################################################
    # End the loop 
    ##################################################                         
    def stop(self):
        print('Shutdown client...')
        self.stopReceiving=True
        if self.conn is None: return
        if self.ptype==0: self.s.close()
        if self.ptype==1: self.pipe.close()
         
#############################################################################################
# Class MarketData 
#
# Class including functions to connect to streams of market data
#############################################################################################      
class MarketData():
    
    mdp=None
    clnt=0
    
    provider=''
    dfreq=''
    bmax_size=0
    buffer={}
    strategy=0
    tickers=[]
    
    def __init__(self,md_params={}):
        self.provider=md_params['prvdr']
        self.dfreq=md_params['freq']
        self.bmax_size=md_params['bsize']
        self.strategy=md_params['stgy']
        self.tickers=md_params['tkrs']
        
        if self.provider=='IEX':
            if upd_func != None: IEXMarketData.updateIndicators=upd_func
            self.mdp=IEXMarketData()
            i=1
            for s in self.tickers:
                lbl=str(self.strategy)+str(i)
                self.buffer[str(s)]={'lbl':str(lbl),'data':self.mdp.getMarketData(lbl=lbl,ticker=s,freq=freq,bsize=bsize)}
                i=i+1
            api_thread = threading.Thread(target=self.mdp.run, daemon=True)
            api_thread.start()
        
        elif (self.provider=='TWS' or self.provider=='IBG'):
            how=prvdr #'TWS'
            port=ib.IBConn[how][1]
            ip=ib.IBConn[how][2]
            client=ib.IBConn[how][0]
    
            self.mdp=ib.ibConnectDBD(port=port,ip=ip,client_id=client)
            i=1
            for s in self.tickers:
                lbl=str(self.strategy)+str(i)
                self.buffer[str(s)]={'lbl':lbl,'data':ib.getTickByTickData(int(lbl),ib.getStock(symbol=s,exchange=exchange,currency=currency),False,bsize)}
                time.sleep(0.01)
                i=i+1
            #Thread already inside ib class
       
    def stopMData(self):
        if self.provider=='TWS':
            self.mdp.disconnect()
        elif self.provider=='IEX':
            self.mdp.stop()

        
#############################################################################################
# Class IEXMarketData 
#
# Specialized Class for IEX Cloud market data
#############################################################################################      
class IEXMarketData():
    loop=True
        
    def __init__(self):
        global buffer
        global max_bsize
        global tickers
        global lastMinutes
        global mytime
        
        mytime=60
        lastMinutes=60
        max_bsize=10000
        buffer={}
        tickers={}
        
    def updateIndicators(self,data):
        return data
        
    def getMarketData(self,lbl='100',ticker='AAPL',freq='H',bsize=10000):
        global max_bsize
        global lastMinutes
        global mytime
        
        max_bsize=bsize
        if freq=='5':
            lastMinutes=max_bsize*5
            mytime=60
        elif freq=='30':
            lastMinutes=max_bsize*5
            mytime=60*5
        elif freq=='H':
            lastMinutes=max_bsize*60
            mytime=60*10
        elif freq=='4H':
            lastMinutes=max_bsize*60*4
            mytime=60*30
        elif freq=='D':
            lastMinutes=max_bsize*60*8
            mytime=60*60*6
        
        buffer[lbl]=iex.getIntradayData(ticker,last=lastMinutes)
        tkr={'ticker':ticker,'freq':freq,'lastm':lastMinutes,'pause':mytime}
        tickers[lbl]=tkr
        buffer[lbl]=self.updateIndicators(buffer[lbl])
        return buffer[lbl]
        
    def run(self):
        while self.loop:        
            for l in tickers:
                t=tickers[l]['ticker']
                lm=tickers[l]['lastm']
                p=tickers[l]['pause']
                buffer[l]=iex.getIntradayData(t,last=lm)#
                buffer[l]=self.updateIndicators(buffer[l])#
                
    def stop(self):
        self.loop=False    
