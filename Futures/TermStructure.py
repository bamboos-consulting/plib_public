#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Term Structure for Future Contracts
#
# Module comprising plot and helper functions
#############################################################################################    
import sys
import os

module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')
    
#import Plib.DataFarm.FRData as fr
#import Plib.DataFarm.Norgate as ng
#import Plib.Utils.Tools as tl

#import Plib.Keystore as kst
#ks=kst.Keystore()
#api_key=ks.eia_key
#root=ks.eia_root

import datetime
import pandas as pd
import numpy as np

##############################################
# Helper Functions
##############################################      
def getNextMonth(month,dfc):
    from itertools import cycle
    
    months=dfc.Month.unique()[::-1]
    #months=['F','G','H','J','K','M','N','Q','U','V','X','Z']
    mcycler = cycle(months)
    
    for i in range(12):     
        ret=next(mcycler)
        if ret == month: break;
    return next(mcycler)

def getMonthLbl(m):
    
    if m == 'F':
        ret='January'
    elif m == 'G':
        ret = 'February'
    elif m == 'H':
        ret = 'March'
    elif m == 'J':
        ret = 'April'
    elif m == 'K':
        ret = 'May'
    elif m == 'M':
        ret = 'June'
    elif m == 'N':
        ret = 'July'
    elif m == 'Q':
        ret = 'August'
    elif m == 'U':
        ret = 'September'
    elif m == 'V':
        ret = 'October'
    elif m == 'X':
        ret = 'November'
    elif m == 'Z':
        ret = 'December'
    
    return ret
    
##############################################
# Methods for Variables Creation
##############################################      
def getLogReturns(dfc,ret_label='Returns',pr_label='Adjusted_close'):
    dfc[ret_label]=np.log(1+dfc[pr_label].pct_change(1))
    return dfc

def getImpliedYield(dfc,lbl_price='Adjusted_close'):
    dfc['IYield']=(dfc[lbl_price]/dfc[lbl_price].shift(-1))**(1/360/(dfc['Dte']-dfc['Dte'].shift(-1)))
    return dfc

def df_rolling_skew(df,lbl_rets='Returns',roll_win=30):
    out_df = df[lbl_rets].rolling(roll_win).skew()
    df['Skew-'+str(roll_win)]=out_df
    return df.dropna()

##############################################
# Create the Continuous Future Contract
##############################################      
def createContFut(df, trading_date='2021-12-18', label='Adjusted_close', method='panama-back-adj'):
    import datetime

    #trading_date='2021-12-18'
    #front_month='M'
    #label='Adjusted_close'

    #Create temp dataframe for appending
    temp=df.copy()
    temp=temp.reset_index()
    temp=temp[['Date','Open','High','Low','Close','Adjusted_close','Volume','openInterest']]
    temp=temp.head(1)
    temp.drop(0,axis=0,inplace=True)

    #Get data for selected expiration
    df1=df.copy()
    df1=df1.reset_index()
    df1['Expiration']=pd.to_datetime(df1['Expiration'])
    df1.sort_values(['Date','Year','Month'], inplace=True, ascending=False)
    df1=df1.set_index(['Date','Expiration'])

    #Select expirations dates
    ed=pd.DataFrame(df.Expiration.unique(),columns=['Expiration'])
    ed.sort_values("Expiration", inplace=True,ascending=False)
    ed.reset_index(inplace=True)
    del ed['index']
    i=0
    first=True
    for e in ed.Expiration:
        if datetime.datetime.strptime(str(e), "%Y-%m-%d") <= datetime.datetime.strptime(trading_date, "%Y-%m-%d"):
            try:
                expi = str(ed.Expiration.values[i])
                expim1 = str(ed.Expiration.values[i+1])
                expim2 = str(ed.Expiration.values[i+2])
                #print(i, expi, expim1)

                back=df1[ (df1.index.get_level_values(0)==expim1) & (df1.index.get_level_values(1)==expim1)]
                front=df1[ (df1.index.get_level_values(0)==expim1) & (df1.index.get_level_values(1)==expi)]

                frontP=front[label][0]
                backP=back[label][0]
                if method=='panama-back-adj':
                    Delta=frontP-backP
                elif method=='ratio-back-adj':
                    Frac=frontP/backP
                    
                front=df1[ (df1.index.get_level_values(0)>=expim1) & (df1.index.get_level_values(1)==expi)]
                if first:
                    front['Date']=front.index.get_level_values(0)
                    temp=temp.append(front)
                    first=False

                backA=df1[ (df1.index.get_level_values(0)<expim1) & (df1.index.get_level_values(0)>=expim2) & (df1.index.get_level_values(1)==expi)]
                if method=='panama-back-adj':
                    backA[label]=backA[label]+Delta
                elif method=='ratio-back-adj':
                    backA[label]=backA[label]*Frac
                backA['Date']=backA.index.get_level_values(0)
                temp=temp.append(backA)

            except:
                pass;
        i=i+1
    temp=temp.set_index('Date')    
    return temp.dropna()

##################################################
# Create the Term Strucutre for the Spot Contract
##################################################      
def mk_termStruct(df,trading_date='2021-12-18',lbl1='Close',lbl2='Volume',lbl3='',idx='Month',col='Year'):
    
    df_years=len(df.Year.unique())

    months={
    'F':'F_Jan',
    'G':'G_Feb',
    'H':'H_Mar',
    'J':'J_Apr',
    'K':'K_May',
    'M':'M_Jun',
    'N':'N_Jul',
    'Q':'Q_Aug',
    'U':'U_Sep',
    'V':'V_Oct',
    'X':'X_Nov',
    'Z':'Z_Dec'}
    df2=df.copy()
    df2=df2[ (df2.index>=trading_date) ]
    
    df2['Month']=df2['Month'].map(months) 
    
    p1_pivot = pd.pivot_table(df2, values=lbl1, index=[idx],
                    columns=[col], aggfunc=np.mean)
    
    l=[]
    p1_pivot=p1_pivot.reset_index()
    for y in p1_pivot.columns:
        if y not in ['Year','Month']:
            df1=p1_pivot[['Month',str(y)]]
            df1.columns=['Month','Price']
            df1['Month']=df1['Month']+str(y)
            l.append(df1)
    p1_pivot=pd.concat(l).dropna()
    p1_pivot=p1_pivot.set_index('Month')

    p1_pivot2=p1_pivot.copy()
    p1_pivot2=p1_pivot2.iloc[:, ::-1]
    p1_pivot2['5 Year Avg'] = p1_pivot.iloc[:,:5].mean(axis=1)
    p1_pivot2=p1_pivot2[['5 Year Avg']]
    
    p1_pivot3 = pd.pivot_table(df2, values=lbl2, index=[idx],
                    columns=[col], aggfunc=np.mean)
    l=[]
    p1_pivot3=p1_pivot3.reset_index()
    for y in p1_pivot3.columns:
        if y not in ['Year','Month']:
            df1=p1_pivot3[['Month',str(y)]]
            df1.columns=['Month','Price']
            df1['Month']=df1['Month']+str(y)
            l.append(df1)
    p1_pivot3=pd.concat(l).dropna()
    p1_pivot3=p1_pivot3.set_index('Month')
    
    p1_pivot3=p1_pivot3.iloc[:, ::-1]
    p1_pivot3['5 Year Avg'] = p1_pivot3.iloc[:,:5].mean(axis=1)
    p1_pivot3=p1_pivot3[['5 Year Avg']]
    
    p1_pivot4=[]
    if lbl3 != '':
        p1_pivot4 = pd.pivot_table(df2, values=lbl3, index=[idx],
                    columns=[col], aggfunc=np.mean)
        l=[]
        p1_pivot4=p1_pivot4.reset_index()
        for y in p1_pivot4.columns:
            if y not in ['Year','Month']:
                df1=p1_pivot4[['Month',str(y)]]
                df1.columns=['Month','Price']
                df1['Month']=df1['Month']+str(y)
                l.append(df1)
        p1_pivot4=pd.concat(l).dropna()
        p1_pivot4=p1_pivot4.set_index('Month')

        p1_pivot4=p1_pivot4.iloc[:, ::-1]
        p1_pivot4['5 Year Avg'] = p1_pivot4.iloc[:,:5].mean(axis=1)
        p1_pivot4=p1_pivot4[['5 Year Avg']]
        
    return p1_pivot,p1_pivot2,p1_pivot3,p1_pivot4

########################################################
# Create the Term Strucutre for the Historical Contract
########################################################      
def mk_seasonalAvg(df,lbl1='Close',lbl2='Volume',lbl3='',idx='Month',col='Year',perc=False):
    
    df_years=len(df.Year.unique())

    months={
    'F':'F_Jan',
    'G':'G_Feb',
    'H':'H_Mar',
    'J':'J_Apr',
    'K':'K_May',
    'M':'M_Jun',
    'N':'N_Jul',
    'Q':'Q_Aug',
    'U':'U_Sep',
    'V':'V_Oct',
    'X':'X_Nov',
    'Z':'Z_Dec'}
    df2=df.copy()
    df2['Month']=df['Month'].map(months) 
    
    # Series (prices or percentiles)
    p1_pivot = pd.pivot_table(df2, values=lbl1, index=[idx],
                    columns=[col], aggfunc=np.mean)
    if perc:
        new_cols=[]
        for c in p1_pivot.columns:
            p1_pivot[str(c)+'perc']=p1_pivot[c].rank(pct=True)
            new_cols.append(str(c)+'perc')
        p1_pivot=p1_pivot[new_cols]
    
    # Averages of Series
    p1_pivot2=p1_pivot.copy()
    p1_pivot2=p1_pivot2.iloc[:, ::-1]
    p1_pivot2['5 Year Avg'] = p1_pivot.iloc[:,:5].mean(axis=1)
    p1_pivot2['10 Year Avg'] = p1_pivot.iloc[:,:10].mean(axis=1)
    p1_pivot2['15 Year Avg'] = p1_pivot.iloc[:,:15].mean(axis=1)
    p1_pivot2['20 Year Avg'] = p1_pivot.iloc[:,:20].mean(axis=1)
    p1_pivot2['30 Year Avg'] = p1_pivot.iloc[:,:30].mean(axis=1)
    if df_years < 15:
        p1_pivot2=p1_pivot2[['5 Year Avg','10 Year Avg']]
    elif df_years < 20:
        p1_pivot2=p1_pivot2[['5 Year Avg','10 Year Avg','15 Year Avg']]
    elif df_years < 30:
        p1_pivot2=p1_pivot2[['5 Year Avg','10 Year Avg','15 Year Avg','20 Year Avg']]
    else:
        p1_pivot2=p1_pivot2[['5 Year Avg','10 Year Avg','15 Year Avg','20 Year Avg','30 Year Avg']]
    
    #Average of Volumes
    p1_pivot3 = pd.pivot_table(df2, values=lbl2, index=[idx],
                    columns=[col], aggfunc=np.mean)
    p1_pivot3=p1_pivot3.iloc[:, ::-1]
    p1_pivot3['5 Year Avg'] = p1_pivot3.iloc[:,:5].mean(axis=1)
    p1_pivot3['10 Year Avg'] = p1_pivot3.iloc[:,:10].mean(axis=1)
    p1_pivot3['15 Year Avg'] = p1_pivot3.iloc[:,:15].mean(axis=1)
    p1_pivot3['20 Year Avg'] = p1_pivot3.iloc[:,:20].mean(axis=1)
    p1_pivot3['30 Year Avg'] = p1_pivot3.iloc[:,:30].mean(axis=1)
    if df_years < 15:
        p1_pivot3=p1_pivot3[['5 Year Avg','10 Year Avg']]
    elif df_years < 20:
        p1_pivot3=p1_pivot3[['5 Year Avg','10 Year Avg','15 Year Avg']]
    elif df_years < 30:
        p1_pivot3=p1_pivot3[['5 Year Avg','10 Year Avg','15 Year Avg','20 Year Avg']]
    else:
        p1_pivot3=p1_pivot3[['5 Year Avg','10 Year Avg','15 Year Avg','20 Year Avg','30 Year Avg']]
    
    #Averages of open interests
    p1_pivot4=[]
    if lbl3 != '':
        p1_pivot4 = pd.pivot_table(df2, values=lbl3, index=[idx],
                    columns=[col], aggfunc=np.mean)
        p1_pivot4=p1_pivot4.iloc[:, ::-1]
        p1_pivot4['5 Year Avg'] = p1_pivot4.iloc[:,:5].mean(axis=1)
        p1_pivot4['10 Year Avg'] = p1_pivot4.iloc[:,:10].mean(axis=1)
        p1_pivot4['15 Year Avg'] = p1_pivot4.iloc[:,:15].mean(axis=1)
        p1_pivot4['20 Year Avg'] = p1_pivot4.iloc[:,:20].mean(axis=1)
        p1_pivot4['30 Year Avg'] = p1_pivot4.iloc[:,:30].mean(axis=1)
        if df_years < 15:
            p1_pivot4=p1_pivot4[['5 Year Avg','10 Year Avg']]
        elif df_years < 20:
            p1_pivot4=p1_pivot4[['5 Year Avg','10 Year Avg','15 Year Avg']]
        elif df_years < 30:
            p1_pivot4=p1_pivot4[['5 Year Avg','10 Year Avg','15 Year Avg','20 Year Avg']]
        else:
            p1_pivot4=p1_pivot4[['5 Year Avg','10 Year Avg','15 Year Avg','20 Year Avg','30 Year Avg']]

    return p1_pivot,p1_pivot2,p1_pivot3,p1_pivot4
 
##############################################
# Create the Volatility Term Structure 
##############################################      
def volFutures(df,symbol='', years=5, lblprice='Adjusted_close'):
    import Plib.Volatility.Estimation as ve
    import math as m

    desc=symbol + ' Futures - Parkinson Estimator over ' + str(years) + ' years'
    prices=df[[lblprice]].head(252*years)
    cones, vols = ve.volconesSV(prices)
    #realized, roll30, roll, roll60, roll120
    prange_sigma=m.sqrt(vols[0].mean()*100)/100
    ve.plotDiagCones(cones, vols, desc, prices, prange_sigma)
    return cones, vols

def plotFutVols(dfc,rvol, pk_20,pk_60, pk_120):
    
    rvol.columns=['rvol']
    pk_20.columns=['pk20']
    pk_60.columns=['pk60']
    pk_120.columns=['pk120']

    dfc['rvol']=rvol
    dfc['pk20']=pk_20
    dfc['pk60']=pk_60
    dfc['pk120']=pk_120
    dfc=dfc.dropna()
    
    vols=dfc[['rvol','pk20','pk60','pk120']]
    ret=vols.plot(subplots=True,figsize=(14,14));
    return dfc

def getFutVols(df, years=5,lblprice='Adjusted_close'):
    import Plib.Volatility.Estimation as ve
    import math
    
    windows = [20, 60, 90, 120]
    
    def vpark(df):
        import math

        N = df.shape[0]
        sigma = math.sqrt((1 / (4 * N * math.log(2))) * df.sum())
        if N <= 5:
            sigma = sigma * 0.55
        elif N <= 10:
            sigma = sigma * 0.65
        elif N <= 20:
            sigma = sigma * 0.74
        elif N <= 50:
            sigma = sigma * 0.82
        elif N <= 100:
            sigma = sigma * 0.86
        elif N <= 200:
            sigma = sigma * 0.92
        return sigma

    def rollVolParkinson(price, days):
        dpd = np.log(price / price.shift(1)).dropna() ** 2
        sigma = (dpd.rolling(window=days).agg(vpark)).dropna()
        return sigma

    price=df[[lblprice]].head(252*years)
    returns = np.log(price / price.shift(1)).dropna()
    roll_VolRealized = ve.rollVolRealized(returns, windows[0]) * ve.conesAdjFactor(windows[0], returns.shape[0]) * math.sqrt(252)

    roll_Vol20 = rollVolParkinson(price, windows[0]) * ve.conesAdjFactor(windows[0], returns.shape[0]) * math.sqrt(252)
    roll_Vol60 = rollVolParkinson(price, windows[1]) * ve.conesAdjFactor(windows[0], returns.shape[0]) * math.sqrt(252)
    #roll_Vol90 = rollVolParkinson(price, windows[2]) * ve.conesAdjFactor(windows[0], returns.shape[0]) * math.sqrt(252)
    roll_Vol120 = rollVolParkinson(price, windows[3]) * ve.conesAdjFactor(windows[0], returns.shape[0]) * math.sqrt(252)
    return roll_VolRealized, roll_Vol20, roll_Vol60, roll_Vol120

##############################################
# Plot the Term Structure 
##############################################      
def plotSeasonTermStruc(symbol,pvt,avgp,avgv,avgoi,dt_start='',dt_end='',lbl_tit='Close Price',lbl_y='Price', plsrs=False):
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    from matplotlib.ticker import FormatStrFormatter
    import matplotlib.ticker as mticker
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()

    from matplotlib.pyplot import figure
    figure(figsize=(12, 12), dpi=80)

    from itertools import cycle
    lines = ["-","--","-.",":"]
    clrs = ['k','b','g','r','m']
    linecycler = cycle(lines)
    colorcycler=cycle(clrs)
    
    # The top plot consisting of the Averages
    top = plt.subplot2grid((12, 5), (2, 0), rowspan=6, colspan=5)
    top.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.4f}'))
    for i in range(avgp.shape[1]+1):
        try:
            top.plot(avgp.index, avgp.iloc[:,i-1:i], linewidth=3,color=next(colorcycler),label=avgp.iloc[:,i-1:i].columns[0])
        except:
            pass;
    plt.title('Term Structure of ' + symbol + ' - ' + lbl_tit + ' (' + dt_start + '-' + dt_end + ')')
    top.set_ylabel(lbl_y,color='black',fontsize=14)
    top.xaxis.set_visible(False)
    top.legend(loc=1)
    
    #Plot series
    if plsrs:
        top2=top.twinx()
        for i in range(pvt.shape[1]):
            try:
                top2.plot(pvt.index, pvt.iloc[:,i-1:i],color=next(colorcycler),linestyle=next(linecycler),linewidth=1,label=pvt.iloc[:,i-1:i].columns[0])
            except:
                pass;
        top2.legend(loc=1)

    # The bottom plot consisting of daily trading volume
    bottom = plt.subplot2grid((12, 5), (8, 0), rowspan=2, colspan=5, sharex=top)
    w=0.15
    for i in range(avgv.shape[1]+1):
        try:
            col=avgv.iloc[:,i-1:i].columns[0]
            bottom.bar(avgv.index, avgv[col], color=next(colorcycler),alpha=0.3,width=w)
            w=w+0.15
        except:
            pass;
    bottom.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    bottom.set_ylabel('Volume')
    
    if len(avgoi)>0:
        bottom.xaxis.set_visible(False)
        bottom2 = plt.subplot2grid((12, 5), (10, 0), rowspan=2, colspan=5, sharex=top)
        w=0.15
        for i in range(avgoi.shape[1]+1):
            try:
                col=avgoi.iloc[:,i-1:i].columns[0]
                bottom2.bar(avgoi.index, avgoi[col], color=next(colorcycler),alpha=0.3,width=w)
                w=w+0.15
            except:
                pass;
        bottom2.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
        bottom2.set_ylabel('openInterest')
    plt.xticks(rotation=30)
        
def plotWeeklyCont(symbol,df, lbl_plot1='Adjusted_close',lbl_plot2='Volume',lbl_plot3='openInterest', dt_start='',dt_end='',lbl_tit='Close Price',lbl_y='Price',lastYears=10):
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    from matplotlib.ticker import FormatStrFormatter
    import matplotlib.ticker as mticker
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()

    from matplotlib.pyplot import figure
    figure(figsize=(14, 12), dpi=80)

    from itertools import cycle
    lines = ["-","--","-.",":"]
    clrs = ['k','b','g','r','m']
    linecycler = cycle(lines)
    colorcycler=cycle(clrs)
    
    months={'F':1,
    'G':2,
    'H':3,
    'J':4,
    'K':5,
    'M':6,
    'N':7,
    'Q':8,
    'U':9,
    'V':10,
    'X':11,
    'Z':12}
    
    months2={1:'F',
    2:'G',
    3:'H',
    4:'J',
    5:'K',
    6:'M',
    7:'N',
    8:'Q',
    9:'U',
    10:'V',
    11:'X',
    12:'Z'}
    
    ohlcv_dict = {
         'Open': 'first',
         'High': 'max',
         'Low': 'min',
         'Close': 'last',
         'Adjusted_close': 'last',
         'Volume': 'sum',
         'openInterest': 'sum',
         'Month': 'last',
         'Year': 'last'
        }

    dfWC=df.copy()
    dfWC=dfWC[['Open','High','Low','Close','Adjusted_close','Volume','openInterest','Month','Year']]
    dfWC.sort_values(['Date','Year','Month'], inplace=True)
    
    dfWC['Month']=dfWC['Month'].map(months) 
    dfWC['Month'] = dfWC['Month'].astype('int')
    dfWC['Year'] = dfWC['Year'].astype('int')
    
    dfWC = dfWC.resample('W-MON').agg(ohlcv_dict)
    
    dfWC=dfWC.dropna()
    dfWC.Month=dfWC.Month.astype('int')
    dfWC.Year=dfWC.Year.astype('int')
    dfWC['Month']=dfWC['Month'].map(months2) 
    dfWC['Month'] = dfWC['Month'].astype('string')
    
    dfWC=dfWC.tail(50*lastYears)
    
    # The top plot consisting of the Averages
    top = plt.subplot2grid((12, 5), (2, 0), rowspan=6, colspan=5)
    top.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.2f}'))
    top.plot(dfWC.index, dfWC[lbl_plot1], linewidth=1,color=next(colorcycler))
    plt.title('Weekly Plot of ' + symbol + ' - ' + lbl_tit + ' (' + dt_start + '-' + dt_end + ')')
    top.set_ylabel(lbl_y,color='black',fontsize=14)
    top.xaxis.set_visible(False)
    top.legend(loc=1)


    # The bottom plot consisting of daily trading volume
    if lbl_plot2 != '':
        bottom = plt.subplot2grid((12, 5), (8, 0), rowspan=2, colspan=5, sharex=top)
        bottom.bar(dfWC.index, dfWC[lbl_plot2], color=next(colorcycler))
        bottom.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
        bottom.set_ylabel('Volume')

    if lbl_plot3 != '':
        bottom.xaxis.set_visible(False)
        bottom2 = plt.subplot2grid((12, 5), (10, 0), rowspan=2, colspan=5, sharex=top)
        bottom2.bar(dfWC.index, dfWC[lbl_plot3], color=next(colorcycler))
        bottom2.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
        bottom2.set_ylabel('openInterest')
    plt.xticks(rotation=30)

def plotMonthlyCont(symbol,df, lbl_plot1='Adjusted_close',lbl_plot2='Volume',lbl_plot3='openInterest', dt_start='',dt_end='',lbl_tit='Close Price',lbl_y='Price',lastYears=50):
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    from matplotlib.ticker import FormatStrFormatter
    import matplotlib.ticker as mticker
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()

    from matplotlib.pyplot import figure
    figure(figsize=(14, 12), dpi=80)

    from itertools import cycle
    lines = ["-","--","-.",":"]
    clrs = ['k','b','g','r','m']
    linecycler = cycle(lines)
    colorcycler=cycle(clrs)
    
    ohlcv_dict = {
         'Open': 'first',
         'High': 'max',
         'Low': 'min',
         'Close': 'last',
         'Adjusted_close': 'last',
         'Volume': 'sum',
         'openInterest': 'sum',
         'Month': 'last',
         'Year': 'last'
        }
        
    months={'F':1,
    'G':2,
    'H':3,
    'J':4,
    'K':5,
    'M':6,
    'N':7,
    'Q':8,
    'U':9,
    'V':10,
    'X':11,
    'Z':12}
    
    months2={1:'F',
    2:'G',
    3:'H',
    4:'J',
    5:'K',
    6:'M',
    7:'N',
    8:'Q',
    9:'U',
    10:'V',
    11:'X',
    12:'Z'}
    

    dfWC=df.copy()
    dfWC=dfWC[['Open','High','Low','Close','Adjusted_close','Volume','openInterest','Month','Year']]
    dfWC.sort_values(['Date','Year','Month'], inplace=True)
    
    dfWC['Month']=dfWC['Month'].map(months) 
    dfWC['Month'] = dfWC['Month'].astype('int')
    dfWC['Year'] = dfWC['Year'].astype('int')
    
    dfWC = dfWC.resample('BMS').agg(ohlcv_dict)
    
    dfWC=dfWC.dropna()
    dfWC.Month=dfWC.Month.astype('int')
    dfWC.Year=dfWC.Year.astype('int')
    dfWC['Month']=dfWC['Month'].map(months2) 
    dfWC['Month'] = dfWC['Month'].astype('string')
    
    dfWC=dfWC.tail(12*lastYears)
    
    # The top plot consisting of the Averages
    top = plt.subplot2grid((12, 5), (2, 0), rowspan=6, colspan=5)
    top.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.2f}'))
    top.plot(dfWC.index, dfWC[lbl_plot1], linewidth=1,color=next(colorcycler))
    plt.title('Monthly Plot of ' + symbol + ' - ' + lbl_tit + ' (' + dt_start + '-' + dt_end + ')')
    top.set_ylabel(lbl_y,color='black',fontsize=14)
    top.xaxis.set_visible(False)
    top.legend(loc=1)


    # The bottom plot consisting of daily trading volume
    if lbl_plot2 != '':
        bottom = plt.subplot2grid((12, 5), (8, 0), rowspan=2, colspan=5, sharex=top)
        bottom.bar(dfWC.index, dfWC[lbl_plot2], color=next(colorcycler))
        bottom.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
        bottom.set_ylabel('Volume')

    if lbl_plot3 != '':
        bottom.xaxis.set_visible(False)
        bottom2 = plt.subplot2grid((12, 5), (10, 0), rowspan=2, colspan=5, sharex=top)
        bottom2.bar(dfWC.index, dfWC[lbl_plot3], color=next(colorcycler))
        bottom2.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
        bottom2.set_ylabel('openInterest')
    plt.xticks(rotation=30)
        
