#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Commitment of Traders for Future Contracts
#
# Module comprising plot and helper functions
#############################################################################################    
import sys
import os

module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import Plib.Plotting.Plots as pl
import Plib.DataFarm.USDrought as u

import Plib.Keystore as kst
ks=kst.Keystore()
root= ks.cotxls_root #= '//Volumes/Datafarm/COT'
hdf_file=ks.cotxls_file #= 'TradersCommitment.hdf'
cot_url=ks.cotxls_url #= 'https://www.cftc.gov/files/dea/history/'
    
import datetime
import pandas as pd
import numpy as np

##############################################
# Plots
##############################################      
def getStats(df, cot, sdecomp, lbl='Close', ma_days=18, vol_days=30, trend_days=90):
    
    def volrealized(ret):
        import math
        N = ret.shape[0]
        df = (ret) ** 2
        sigma = math.sqrt((1 / (N - 1)) * df.sum())
        return sigma* math.sqrt(252)

    def rollVolRealized(df, days):
        import numpy as np
        mfun = lambda x: volrealized(x)
        rets = np.log(df / df.shift(1)).dropna()
        sigma = (rets.rolling(window=days).agg(mfun)).dropna()
        return sigma.round(2)

    def trend(array):    
        from scipy.stats import linregress
        y = np.array(array)
        x = np.arange(1,len(y)+1)
        slope, intercept, r_value, p_value, std_err = linregress(x,y)
        return slope
    
    cols=list(df.columns)
    df=df.join(cot[['OI_Net']]).interpolate().ffill().bfill()
    df=df.join(sdecomp['STL_residual']).interpolate().ffill().bfill()
    df=df.join(sdecomp['STL_trend']).interpolate().ffill().bfill()
    #ttrend=df['trend'].rolling(window=trend_days,center=True).apply(trend, raw=False)
    #df['trend']=ttrend.interpolate().ffill().bfill()
    ttrend=df['trend'].rolling(window=5,center=True).apply(trend, raw=False).ffill().fillna(0)
    df['trend']=ttrend
    df['trend']=df['trend'].rolling(window=trend_days).mean().ffill().fillna(0)
    df['MA']=df[lbl].rolling(ma_days).mean().fillna(0)
    df['index']=df.index
    df['day'] = df['index'].dt.day_name()
    df=df[df.day.isin(['Monday','Tuesday','Wednesday','Thursday','Friday'])]
    del df['index']
    del df['day']
    df['returns']=df[lbl].pct_change()
    df['returns']=df['returns'].replace([np.inf, -np.inf], np.nan).interpolate().ffill().bfill()*100
    df['Vol30d']=rollVolRealized(df[lbl],vol_days)*100
    df=df.fillna(0)
    
    ret=df.copy().round(2)
    ret.columns=cols+['OI_Net','Delta$$','Trend','MA'+str(ma_days)+'D','Rets%','Vol'+str(vol_days)+'D%']
    ret=ret[cols+['OI_Net','Delta$$','MA'+str(ma_days)+'D','Vol'+str(vol_days)+'D%','Rets%','Trend']]    

    df['index']=df.index
    df['day'] = df['index'].dt.day_name()
    
    df2=df[df.index > pd.to_datetime(df.index[-1])+pd.Timedelta(days=-360*5)]     # over last 5 years
    dfp=df2[df2.returns >0]
    dfpv=dfp.groupby(['day'])['returns'].agg(['mean']).rename({'mean':'AvgPRets'},axis=1)
    dd={}
    for d in list(df.day.unique()):
        dd[d]=round((len(dfp[dfp.day == d])/len(df2[df2.day == d])),2)
    percup=pd.DataFrame(dd.values(),index=dd.keys())*100
    percup.columns=['UP%5Y']
        
    df2=df[df.index > pd.to_datetime(df.index[-1])+pd.Timedelta(days=-31*7)] # over last 30 weeks
    dfrv=df2.groupby(['day'])['resid'].agg(['mean']).rename({'mean':'AVG$$'},axis=1)
    stats=percup.join(dfrv)
    dfp=df2[df2.returns >0]
    dfn=df2[df2.returns <0]
    dfpv=dfp.groupby(['day'])['returns'].agg(['mean']).rename({'mean':'AVGProf%'},axis=1)
    dfnv=dfn.groupby(['day'])['returns'].agg(['mean']).rename({'mean':'AVGLoss%'},axis=1)
    trend=df2.groupby(['day'])['trend'].agg(['mean']).rename({'mean':'Trend'+str(trend_days)+'D'},axis=1)
    ma=df2.groupby(['day'])['MA'].agg(['mean']).rename({'mean':'MA'+str(ma_days)+'D'},axis=1)
    Vol30d=df2.groupby(['day'])['Vol30d'].agg(['mean']).rename({'mean':'VOL'+str(vol_days)+'D%'},axis=1)
    
    day_order = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'] #, 'Saturday', 'Sunday'
    stats=(stats.join(dfpv).join(dfnv).join(trend).join(ma).join(Vol30d)).round(2)
    stats=stats.reindex(day_order, axis=0)
    return stats,ret

def prepareFuturesPlot(symbol, data, dstart, dend, lbl='Close', wasdep=['Wheat','Production','average','United States'], stats=True, statsp=('Open', 18, 30, 90), provider='kibot', traders='Producers Merchants', hdfname=''):
    from Plib.Signals.Filters import STLSeriesDecomp
    import datetime
    
    def aggregate(daily_df, frequency):
        daily_df.reset_index(inplace=True)
        daily_df['days'] = 1
        weekly_df = daily_df.resample(frequency, on='Date').agg({'Open':'first','High':'max', 'Low':'min', 'Close':'last','Volume':'sum','days':'count'})
        return weekly_df
    
    def getWasdeW(df, lbl='Close'):
        cols=df.columns    
        upsampled = df[cols[0]].resample('D')
        interpolated = upsampled.interpolate(method='linear').ffill().bfill()
        mdf=pd.DataFrame(interpolated).reset_index()
        mdf.columns=['Date','Close']
        mdf=mdf.set_index('Date')
        mdf['Open']=0
        mdf['High']=0
        mdf['Low']=0
        mdf['Volume']=0
        weekly_df=aggregate(mdf, 'W-Tue')[[lbl]]
        weekly_df.columns=[cols[0]]
        return weekly_df
        
    # Aggregate by week
    df=data.copy()
    df = aggregate(df, 'W-Tue')
    
    # Detrended indicator
    sdecomp = STLSeriesDecomp(df, lbl=lbl, freq=12, w=30, plot=False)
    
    #Obtain wasde data
    if len(wasdep) > 0:
        wasde,unit=getWasde(comm=wasdep[0], attr=wasdep[1], proj=wasdep[2],country=wasdep[3], myfile=hdfname)
        wasde=getWasdeW(wasde,lbl=lbl),unit
    else:
        wasde=([],'')
        
    # Obtain Cot data
    contract,cname=getContractID(symbol, provider=provider)
    records=getCOT(contract=contract,date='',table='Cot', myfile=hdfname)
    print('Available records: ', records.reset_index().Type.unique())
    print('Matched contract:', contract, cname, traders)
    cols=['Date','OI_All','OI_Long','OI_Short','OI_Spread']
    records=records[records.index.get_level_values('Type') == traders].reset_index()[cols].set_index('Date')
    records=records.astype(float)
    records['OI_Net']=records['OI_Long']-records['OI_Short']
    #if not pl_spread:
    records=records[['OI_All','OI_Long','OI_Short','OI_Net']]

    # Align data
    rcols=records.columns
    dcols=df.columns
    records2=df.join(records).dropna()
    records2=records2.sort_index()
    cot=records2[rcols]
    prices=records2[dcols]
    
    if stats:
        pstats, data=getStats(data.copy(), cot, sdecomp, *statsp)
    else:
        pstats=pd.DataFrame()
    
    return prices, cot, cname, wasde, records, sdecomp, pstats, data

def plotFutureContract(symbol='CL', data={}, lbl='Close', provider='kibot', dstart = '2014-12-30', dend = '2020-07-07', stats=True, statsp=('Open', 18, 30, 90),traders='Producers Merchants',drought=['conus'], wasdep=['Wheat','Production','average','United States'],sde_p=(False,0.65),ci=0.10, c_win1=10, c_win2=52, lt_win=50, winma=25, bb_sd=2, hdfname=''):
    
    if hdfname=='':
        hdfname = root + '/' + hdf_file
    
    if len(drought) > 0:
        weather=u.getDrought(str(pd.to_datetime(dstart)+pd.Timedelta(days=-360))[:10], dend, drought[0])
    else:
        weather=[]
        
    prices, cot, cname, wasde, records, sdecomp, pstats, data = prepareFuturesPlot(symbol, data, dstart, dend, 
                lbl=lbl, wasdep=wasdep, stats=stats, statsp=statsp, provider=provider, traders=traders, 
                                                                                   hdfname=hdfname)

    pl.plotUnderlyingStudyCOT(symbol + '-' + cname, dstart, dend, prices, cot, 
                              weather=weather, wasde=wasde, sdecomp=sdecomp, sde_p=sde_p, stats=pstats,
                             ci=ci, c_win1=c_win1,c_win2=c_win2,lt_win=lt_win, winma=winma, bb_sd=bb_sd)
    return data

##############################################
# Table of futures tickers across vendors
##############################################    
def getContractID(symbol, provider='kibot'):
    table=getFutSymbConvTable()
    table=table[['Name', 'Cot',provider.lower().capitalize()]]
    table.columns=['Name','Cot','Symbol']
    return table[table['Symbol']==symbol]['Cot'].values[0],table[table['Symbol']==symbol]['Name'].values[0] 
  
def getFutSymbConvTable(group='', details=False):
    from pandas import read_excel

    fname='FuturesContractDetails.xls'
    save_dir=module_path.replace('/Plib','') +'/Plib/Futures'
    df = read_excel(save_dir+'/'+fname,
                           sheet_name='Futures Contract Details',
                           skiprows=range(int(1)-1),
                           skipfooter=0)
    key=['Name','Norgate','Cot','Frdata','Kibot']
    detail=['Exchange','Contract Size','Quotation','Tick Size','Tick Value','Point Value',
             'Currency','Last Trading Day','First Notice Day']
    if group != '':
        if details:
            df=df[df.Group == group][key+detail]
        else:
            df=df[df.Group == group][key]
    else:
        if details:
            df=df[key+detail]
        else:
            df=df[key]
    return df

##############################################
# Obtain Contracts from HDF (XLS version)
##############################################      
def getCOT(contract='001602',date='2022-08-09',table='COT', myfile=''):
    if myfile == '':
        myfile=root + '/' + hdf_file
        
    store = pd.HDFStore(myfile)
    if contract == '':
        df=store.select(table)
    else:
        df=store.select(table, where = "Code=='"+contract+"'")
    store.close()
    if date != '':
        df=df[df.index.get_level_values('Date') == date]
    return df

def searchCOT(search='WHEAT', myfile=''):
    if myfile == '':
        myfile=root + '/' + hdf_file
        
    store = pd.HDFStore(myfile)
    df=store.select('Details')
    store.close()
    df=df[df.Description.str.contains(search)]
    df=df[['Description']].reset_index()
    del df['Date']
    del df['Market']
    return df.drop_duplicates().set_index('Code')

##############################################
# Extract COT from XLS
##############################################      
def readCotXls(year='2010', disagg_no_options=False):
    
    if disagg_no_options:
        base_year='2009'
        dd_base_year='2009-12-31'
        fname1='f_year'+year+'.xls'
    else:
        base_year='0000'
        fname1='f_year'+year+'.xls'        
        
    if year==base_year:
        fname=fname_by
    else:
        fname=fname1
        cot=pd.read_excel(module_path + '/Test/COT/'+fname,
                           sheet_name='XLS',
                           skiprows=range(int(1)-1),
                           skipfooter=0) 

    if year != base_year:
        cot['As_of_Date_In_Form_YYMMDD']=pd.to_datetime(cot['As_of_Date_In_Form_YYMMDD'], format='%y%m%d')
        cot['Report_Date_as_MM_DD_YYYY']=pd.to_datetime(cot['Report_Date_as_MM_DD_YYYY'], format='%m%d%Y')
    else:
        cot['Report_Date_as_MM_DD_YYYY']=pd.to_datetime(cot['Report_Date_as_MM_DD_YYYY'], format='%m%d%Y')
        cot['As_of_Date_In_Form_YYMMDD']=cot['Report_Date_as_MM_DD_YYYY']
        cot=cot[cot.Report_Date_as_MM_DD_YYYY <= dd_base_year]
        
    col_details=['Market_and_Exchange_Names','As_of_Date_In_Form_YYMMDD','Report_Date_as_MM_DD_YYYY','CFTC_Contract_Market_Code',
         'CFTC_Market_Code','Contract_Units']

    colsPM=['Market_and_Exchange_Names','As_of_Date_In_Form_YYMMDD','Report_Date_as_MM_DD_YYYY','CFTC_Contract_Market_Code',
         'CFTC_Market_Code','Open_Interest_All','Prod_Merc_Positions_Long_ALL','Prod_Merc_Positions_Short_ALL']

    colsSW=['Market_and_Exchange_Names','As_of_Date_In_Form_YYMMDD','Report_Date_as_MM_DD_YYYY','CFTC_Contract_Market_Code',
         'CFTC_Market_Code','Open_Interest_All','Swap_Positions_Long_All','Swap__Positions_Short_All','Swap__Positions_Spread_All']


    colsMM=['Market_and_Exchange_Names','As_of_Date_In_Form_YYMMDD','Report_Date_as_MM_DD_YYYY','CFTC_Contract_Market_Code',
         'CFTC_Market_Code','Open_Interest_All','M_Money_Positions_Long_ALL','M_Money_Positions_Short_ALL','M_Money_Positions_Spread_ALL']

    colsOR=['Market_and_Exchange_Names','As_of_Date_In_Form_YYMMDD','Report_Date_as_MM_DD_YYYY','CFTC_Contract_Market_Code',
         'CFTC_Market_Code','Open_Interest_All','Other_Rept_Positions_Long_ALL','Other_Rept_Positions_Short_ALL','Other_Rept_Positions_Spread_ALL']

    colsNR=['Market_and_Exchange_Names','As_of_Date_In_Form_YYMMDD','Report_Date_as_MM_DD_YYYY','CFTC_Contract_Market_Code',
         'CFTC_Market_Code','Open_Interest_All','NonRept_Positions_Long_All','NonRept_Positions_Short_All']

    colsCH=['Market_and_Exchange_Names','As_of_Date_In_Form_YYMMDD','Report_Date_as_MM_DD_YYYY','CFTC_Contract_Market_Code',
         'CFTC_Market_Code','Change_in_Open_Interest_All','Change_in_Prod_Merc_Long_All','Change_in_Prod_Merc_Short_All',
            'Change_in_Swap_Long_All','Change_in_Swap_Short_All','Change_in_Swap_Spread_All','Change_in_M_Money_Long_All',
            'Change_in_M_Money_Short_All','Change_in_M_Money_Spread_All','Change_in_Other_Rept_Long_All',
            'Change_in_Other_Rept_Short_All','Change_in_Other_Rept_Spread_All','Change_in_Tot_Rept_Long_All',
            'Change_in_Tot_Rept_Short_All','Change_in_NonRept_Long_All','Change_in_NonRept_Short_All']

    colsPCT=['Market_and_Exchange_Names','As_of_Date_In_Form_YYMMDD','Report_Date_as_MM_DD_YYYY','CFTC_Contract_Market_Code',
         'CFTC_Market_Code','Pct_of_Open_Interest_All','Pct_of_OI_Prod_Merc_Long_All','Pct_of_OI_Prod_Merc_Short_All',
             'Pct_of_OI_Swap_Long_All','Pct_of_OI_Swap_Short_All','Pct_of_OI_Swap_Spread_All','Pct_of_OI_M_Money_Long_All',
             'Pct_of_OI_M_Money_Short_All','Pct_of_OI_M_Money_Spread_All','Pct_of_OI_Other_Rept_Long_All',
             'Pct_of_OI_Other_Rept_Short_All','Pct_of_OI_Other_Rept_Spread_All','Pct_of_OI_Tot_Rept_Long_All',
             'Pct_of_OI_Tot_Rept_Short_All','Pct_of_OI_NonRept_Long_All','Pct_of_OI_NonRept_Short_All']

    colsTRD=['Market_and_Exchange_Names','As_of_Date_In_Form_YYMMDD','Report_Date_as_MM_DD_YYYY','CFTC_Contract_Market_Code',
         'CFTC_Market_Code','Traders_Tot_All','Traders_Prod_Merc_Long_All','Traders_Prod_Merc_Short_All',
             'Traders_Swap_Long_All','Traders_Swap_Short_All','Traders_Swap_Spread_All','Traders_M_Money_Long_All',
             'Traders_M_Money_Short_All','Traders_M_Money_Spread_All','Traders_Other_Rept_Long_All',
             'Traders_Other_Rept_Short_All','Traders_Other_Rept_Spread_All','Traders_Tot_Rept_Long_All',
             'Traders_Tot_Rept_Short_All']

    colsPCLTR=['Market_and_Exchange_Names','As_of_Date_In_Form_YYMMDD','Report_Date_as_MM_DD_YYYY','CFTC_Contract_Market_Code',
         'CFTC_Market_Code','Conc_Gross_LE_4_TDR_Long_All','Conc_Gross_LE_4_TDR_Short_All','Conc_Gross_LE_8_TDR_Long_All',
               'Conc_Gross_LE_8_TDR_Short_All','Conc_Net_LE_4_TDR_Long_All','Conc_Net_LE_4_TDR_Short_All',
               'Conc_Net_LE_8_TDR_Long_All','Conc_Net_LE_8_TDR_Short_All']

    cot_details=cot[col_details]
    cot_details.columns=['Description','DateAsOf','Date','Code','Market','Units']
    cot_details=cot_details[['Date','Code','Description','Market','Units','DateAsOf']]
    cot_details['Units']=cot_details['Units'].astype(str).str.replace('(CONTRACTS OF ', '', regex=False)
    cot_details['Units']=cot_details['Units'].astype(str).str.replace(')', '', regex=False)
    
    cot_swapdealers=cot[colsSW]
    cot_swapdealers.columns=['Description','DateAsOf','Date','Code','Market','OI_All','OI_Long','OI_Short','OI_Spread']
    cot_swapdealers=cot_swapdealers[['Date','Code','Description','Market','OI_All','OI_Long','OI_Short','OI_Spread','DateAsOf']]
    cot_swapdealers['Type']='Swap Dealers'
    
    cot_prodmerch=cot[colsPM]
    cot_prodmerch.columns=['Description','DateAsOf','Date','Code','Market','OI_All','OI_Long','OI_Short']
    cot_prodmerch['OI_Spread']=0
    cot_prodmerch=cot_prodmerch[['Date','Code','Description','Market','OI_All','OI_Long','OI_Short','OI_Spread','DateAsOf']]
    cot_prodmerch['Type']='Producers Merchants'
    
    cot_mngdmoney=cot[colsMM]
    cot_mngdmoney.columns=['Description','DateAsOf','Date','Code','Market','OI_All','OI_Long','OI_Short','OI_Spread']
    cot_mngdmoney=cot_mngdmoney[['Date','Code','Description','Market','OI_All','OI_Long','OI_Short','OI_Spread','DateAsOf']]
    cot_mngdmoney['Type']='Managed Money'
    
    cot_other=cot[colsOR]
    cot_other.columns=['Description','DateAsOf','Date','Code','Market','OI_All','OI_Long','OI_Short','OI_Spread']
    cot_other=cot_other[['Date','Code','Description','Market','OI_All','OI_Long','OI_Short','OI_Spread','DateAsOf']]
    cot_other['Type']='Other Reportables'
    
    cot_nonrep=cot[colsNR]
    cot_nonrep['OI_Spread']=0
    cot_nonrep.columns=['Description','DateAsOf','Date','Code','Market','OI_All','OI_Long','OI_Short','OI_Spread']
    cot_nonrep=cot_nonrep[['Date','Code','Description','Market','OI_All','OI_Long','OI_Short','OI_Spread','DateAsOf']]
    cot_nonrep['Type']='Non-Reportables'
    
    cot_positions=cot_swapdealers.append(cot_prodmerch).append(cot_mngdmoney).append(cot_other).append(cot_nonrep)

    cot_percent=cot[colsPCT]
    cot_percent['OI_ProdMercSpread']=0
    cot_percent.columns=['Description','DateAsOf','Date','Code','Market','OI_All',
                         'OI_ProdMercLong','OI_ProdMercShort',
                         'OI_SwapDlrLong','OI_SwapDlrShort','OI_SwapDlrSpread',
                         'OI_MgdMoneyLong','OI_MgdMoneyShort','OI_MgdMoneySpread',
                         'OI_OthersLong','OI_OthersShort','OI_OthersSpread',
                         'totrep1','totrep2',
                         'OI_NonRepLong','OI_NonRepShort',
                         'OI_ProdMercSpread']
    cot_percent=cot_percent[['Date','Code','Description','Market','OI_All',
                         'OI_ProdMercLong','OI_ProdMercShort','OI_ProdMercSpread',
                         'OI_SwapDlrLong','OI_SwapDlrShort','OI_SwapDlrSpread',
                         'OI_MgdMoneyLong','OI_MgdMoneyShort','OI_MgdMoneySpread',
                         'OI_OthersLong','OI_OthersShort','OI_OthersSpread',
                         'OI_NonRepLong','OI_NonRepShort','DateAsOf']]
    cot_percent['Type']='Percentages'
    
    cot_traders=cot[colsTRD]
    cot_traders['OI_ProdMercSpread']=0
    cot_traders.columns=['Description','DateAsOf','Date','Code','Market','OI_All',
                         'OI_ProdMercLong','OI_ProdMercShort',
                         'OI_SwapDlrLong','OI_SwapDlrShort','OI_SwapDlrSpread',
                         'OI_MgdMoneyLong','OI_MgdMoneyShort','OI_MgdMoneySpread',
                         'OI_OthersLong','OI_OthersShort','OI_OthersSpread',
                         'totrep1','totrep2','OI_ProdMercSpread']
    cot_traders['OI_NonRepLong']=0
    cot_traders['OI_NonRepShort']=0
    cot_traders=cot_traders[['Date','Code','Description','Market','OI_All',
                         'OI_ProdMercLong','OI_ProdMercShort','OI_ProdMercSpread',
                         'OI_SwapDlrLong','OI_SwapDlrShort','OI_SwapDlrSpread',
                         'OI_MgdMoneyLong','OI_MgdMoneyShort','OI_MgdMoneySpread',
                         'OI_OthersLong','OI_OthersShort','OI_OthersSpread',
                         'OI_NonRepLong','OI_NonRepShort','DateAsOf']]
    cot_traders['Type']='Number of Traders'
    
    cot_stats=cot_percent.append(cot_traders)

    cot_largest=cot[colsPCLTR]
    cot_largest.columns=['Description','DateAsOf','Date','Code','Market',
                'Gross4PctLong','Gross4PctShort','Gross8PctLong',
               'Gross8PctShort','Net4PctLong','Net4PctShort',
               'Net8PctLong','Net8PctShort']
    cot_largest=cot_largest[['Description','Date','Code','Market',
                'Gross4PctLong','Gross4PctShort','Gross8PctLong',
               'Gross8PctShort','Net4PctLong','Net4PctShort',
               'Net8PctLong','Net8PctShort','DateAsOf']]
    
    dfl=[cot_details,cot_positions,cot_stats,cot_largest]
    for d in dfl:
        for c in d.columns:
            if c not in ['DateAsOf','Date']:
                d[c] = d[c].astype(str)
            if c in ['DateAsOf','Date']:
                d[c] = pd.to_datetime(d[c])
                
    dfl=[cot_details,cot_largest]
    for d in dfl:
        d.index = pd.MultiIndex.from_arrays(d[['Date','Code','Market']].values.T, names=['Date','Code','Market'])
        del d['Date']
        del d['Code']
        del d['Market']
    dfl=[cot_positions,cot_stats]
    for d in dfl:
        d.index = pd.MultiIndex.from_arrays(d[['Date','Code','Market','Type']].values.T, names=['Date','Code','Market','Type'])
        del d['Date']
        del d['Code']
        del d['Market']
        del d['Type']
        
    cot_details=cot_details[['Description','Units']].join(cot_largest[['Gross4PctLong','Gross4PctShort',
            'Gross8PctLong','Gross8PctShort','Net4PctLong','Net4PctShort','Net8PctLong','Net8PctShort','DateAsOf']])

    return cot_details,cot_positions,cot_stats
        
def storeXLS1(max_year=2022, hdfname='', only_last=True,disagg_no_options=False):
    i=0
    if only_last:
        base_year=max_year
    else:
        if disagg_no_options:
            base_year=2010
        else:
            base_year=2014
    for y in [str(i) for i in range(base_year,max_year+1)]   :
        tcot_details,tcot_positions,tcot_stats = readCotXls(year=y,disagg_no_options=disagg_no_options)
        if i==0:
            cot_details,cot_positions,cot_stats = tcot_details.copy(),tcot_positions.copy(),tcot_stats.copy()
            i=i+1
        else:
            cot_details=cot_details.append(tcot_details)
            cot_positions=cot_positions.append(tcot_positions)
            cot_stats=cot_stats.append(tcot_stats)
            
    if hdfname == '':
        hdfname=root + '/' + hdf_file
        
    storeXLS3(cot_details, table='Details', myfile=hdfname)
    storeXLS3(cot_stats, table='Stats', myfile=hdfname)
    storeXLS3(cot_positions, table='Cot', myfile=hdfname)                

##############################################
# Store Contracts in HDF
##############################################      
def storeXLS3(records, table='Wasde', myfile='', lbls=('Date','DateAsOf')):
    if myfile == '':
        myfile=root + '/' + hdf_file
    
    try:
        store = pd.HDFStore(myfile)
        c = store.select_column(table,lbls[0]) 
        where = pd.DatetimeIndex(c).to_series().between(str(records[lbls[1]].min()),str(records[lbls[1]].max()))
        arch=store.select(table,where = where)
        store=store.close()
        idx_archives = arch.index
        idx_updates = records.index
        idx3=idx_updates.difference(idx_archives)
        records=records.loc[idx3]
    except:
        pass;
    store = pd.HDFStore(myfile)
    if len(records)>0:
        store.append(table, records, format='table',complevel=2, complib='lzo',data_columns=records.iloc[[0]].columns.tolist())
        print('Added ',len(records),' records')
    store=store.close()
    
def removeKey(table='Wasde',myfile=''):
    if myfile == '':
        myfile=root + '/' + hdf_file
    
    store = pd.HDFStore(myfile)
    store.remove(table)
    store=store.close()
    
##############################################
# Download Compresses Updates from Website 
##############################################      
def downloadCOT(year=2022, save_dir='/Test/COT', disagg_no_options=False):
    import os
    
    if disagg_no_options:
        fname = 'fut_disagg_xls_'+str(year)+'.zip'
        fname_xls = 'f_year.xls'
        fname_xls2 = 'f_year'+str(year)+'.xls'
        url_zip=cot_url+ fname 
    else:
        fname = 'dea_com_xls_'+str(year)+'.zip'
        fname_xls = 'annualof.xls'
        fname_xls2 = 'annualof'+str(year)+'.xls'
        url_zip=cot_url+ fname  
    
    def download(url,save_dir,fname='temp.zip',attempts=1):
        import requests
        import os, sys, os.path
        
        while attempts > 0:
            #resume_headers = {'Range':'bytes=0-2000000'}
            r = requests.get(url, stream=True)#, headers=resume_header)
            try:
                total_length = r.headers.get('content-length')
                if total_length is None: total_length=1
                dl = 0
                total_length = int(total_length)
                done=0
                with open(save_dir+'/'+fname,'wb') as f:
                    for chunk in r.iter_content(chunk_size=1024*1024):
                        dl += len(chunk)
                        f.write(chunk)
                        done = int(50 * dl / total_length)
                        sys.stdout.write("\r[%s%s]" % ('=' * done, ' ' * (50-done)) )    
                        sys.stdout.flush()
                    print("Download complete.")
                    attempts=attempts-1
            except Exception as ex:
                print('Error while downloading')
                attempts=attempts-1
    
    def extract_zip(dir1,dir2,fnz,fnc):
        from zipfile import ZipFile
        from zipfile import BadZipfile
        import os, sys, os.path

        fname_zip=dir1+'/'+fnz
        fname_csv=dir2+'/'+fnc

        ret=0
        print('Unzipping File ' + fname_zip)
        try:
            with ZipFile(fname_zip, 'r') as zipObj:
                if not os.path.isfile(fname_csv):
                    zipObj.extractall(dir2) 
        except BadZipfile:
            ermsg(err_msg='Error while unzipping ' + fname_zip + ' to ' + fname_csv)
            print('Error while unzipping ' + fname_zip)
            ret=-1
        return ret

    download(url_zip,save_dir,fname=fname,attempts=1)
    extract_zip(save_dir,save_dir,fname,fname_xls)
    if os.path.isfile(save_dir+'/'+fname_xls2):
        os.remove(save_dir+'/'+fname_xls2)
    os.rename(save_dir+'/'+fname_xls, save_dir+'/'+fname_xls2)
    
    return save_dir+'/'+fname_xls2

##############################################
# Wasde Production/Use data
##############################################      
#helper
def readWasde(max_year=2022, max_month=8, only_last=True):
    from datetime import datetime
    
    df=pd.DataFrame()
    
    if not only_last:
        fname1='oce-wasde-report-data-2010-04-to-2015-12.csv'
        fname2='oce-wasde-report-data-2016-01-to-2020-12.csv'

        df1=pd.read_csv(module_path + '/Test/wasde/' + fname1)
        df1=df1[['ReleaseDate','WasdeNumber','Commodity', 'ReportTitle', 'Attribute',
               'ReliabilityProjection', 'Region', 'MarketYear',
               'ProjEstFlag', 'AnnualQuarterFlag', 'Value', 'Unit']]
        df2=pd.read_csv(module_path + '/Test/wasde/' + fname2)
        df2=df2[['ReleaseDate','WasdeNumber','Commodity', 'ReportTitle', 'Attribute',
               'ReliabilityProjection', 'Region', 'MarketYear',
               'ProjEstFlag', 'AnnualQuarterFlag', 'Value', 'Unit']]
        df=df1.append(df2)
        base_year=2021
        base_month=1
    else:
        base_year=max_year
        base_month=max_month

    #curr_m=int(datetime.now().month)
    for y in range(base_year,max_year+1):
        for m in range(base_month,max_month+1):
            if len(str(m))==1:
                m='0'+str(m)
            suffix=str(y)+'-'+str(m)
            fname='oce-wasde-report-data-'+suffix+'.csv'
            dft=pd.read_csv(module_path + '/Test/wasde/' + fname)
            dft=dft[['ReleaseDate','WasdeNumber','Commodity', 'ReportTitle', 'Attribute',
                   'ReliabilityProjection', 'Region', 'MarketYear',
                   'ProjEstFlag', 'AnnualQuarterFlag', 'Value', 'Unit']]
            df=df.append(dft)

    for c in df.columns:
        if c not in ['ReleaseDate']:
            df[c] = df[c].astype(str)
        if c in ['ReleaseDate']:
            df[c] = pd.to_datetime(df[c])
    return df

def createWasde(myfile=''):
    if myfile == '':
        myfile=root + '/' + hdf_file    
    df=readWasde(max_year=2022, max_month=8, only_last=False)
    storeXLS3(df, table='Wasde', myfile=myfile, lbls=('ReleaseDate','ReleaseDate'))

def downloadWasde(year='2022',month='09',save_dir='//Users/rob',myname='wasde',myfile='',update=False):
    import datetime
    from os.path import exists
    
    url='https://www.usda.gov/sites/default/files/documents/oce-wasde-report-data-'+year+'-'+month+'.csv'

    if myfile == '':
        myfile=root + '/' + hdf_file    
    
    def download(url,save_dir,fname='temp.zip',attempts=1):
        import requests
        import os, sys, os.path
        import os
        
        while attempts > 0:
            #resume_headers = {'Range':'bytes=0-2000000'}
            r = requests.get(url, stream=True)#, headers=resume_header)
            try:
                total_length = r.headers.get('content-length')
                if total_length is None: total_length=1
                dl = 0
                total_length = int(total_length)
                done=0
                with open(save_dir+'/'+fname,'wb') as f:
                    for chunk in r.iter_content(chunk_size=1024*1024):
                        dl += len(chunk)
                        f.write(chunk)
                        done = int(50 * dl / total_length)
                        sys.stdout.write("\r[%s%s]" % ('=' * done, ' ' * (50-done)) )    
                        sys.stdout.flush()
                    print("Download complete.")
                    attempts=attempts-1
            except Exception as ex:
                print('Error while downloading')
                attempts=attempts-1
                os.remove(save_dir+'/'+fname)

    
    mdate=str(datetime.date.today()).replace('-','')
    myname=myname+mdate
    download(url,save_dir,fname=myname+'.csv',attempts=1)
    if not exists(save_dir +'/'+ myname+'.csv'):
        return pd.DataFrame()
    
    try:
        df=pd.read_csv(save_dir +'/'+ myname+'.csv')
        df=df[['ReleaseDate','WasdeNumber','Commodity', 'ReportTitle', 'Attribute',
                   'ReliabilityProjection', 'Region', 'MarketYear',
                   'ProjEstFlag', 'AnnualQuarterFlag', 'Value', 'Unit']]

        for c in df.columns:
            if c not in ['ReleaseDate']:
                df[c] = df[c].astype(str)
            if c in ['ReleaseDate']:
                df[c] = pd.to_datetime(df[c])
        if update:
            storeXLS3(df, table='Wasde', myfile=myfile, lbls=('ReleaseDate','ReleaseDate'))
    except:
        print('File not available yet....')
        df=pd.DataFrame()
    return df
    
def getWasdeCommList(myfile=''):
    if myfile == '':
        myfile=root + '/' + hdf_file    
    store = pd.HDFStore(myfile)
    df=store.select('Wasde')
    
    df_sel=df[(df['Attribute']=='Domestic Use') 
               & (df['ReliabilityProjection']=='average') 
               & (df['Region']=='United States') ]
    cl=list(df_sel.Commodity.unique())
    return cl

def getWasdeSeries(comm='Wheat',myfile=''):
    if myfile == '':
        myfile=root + '/' + hdf_file    
    store = pd.HDFStore(myfile)
    df=store.select('Wasde', where = "Commodity=='"+comm+"'")
    
    al=list(df.Attribute.unique())
    return al

def getWasde(comm='Wheat', attr='Domestic Use', proj='average',country='United States', myfile=''):
    if myfile == '':
        myfile=root + '/' + hdf_file    
    store = pd.HDFStore(myfile)
    df=store.select('Wasde', where = "Commodity=='"+comm+"'")
    
    df_sel=df[(df['Commodity']==comm) & (df['Attribute']==attr) 
           & (df['ReliabilityProjection']==proj) 
           & (df['Region']==country) ]
    unit=df_sel.Unit.unique()[0]
    df_sel=df_sel[['ReleaseDate','Value']]
    df_sel=df_sel.drop_duplicates()
    df_sel=df_sel.set_index('ReleaseDate')
    df_sel=df_sel.sort_index()
    df_sel.columns=[attr.replace(' ','')]
    df_sel=df_sel.astype(float)
    return df_sel,unit
    
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################

##############################################      
# Helper Functions (old version)
##############################################      
def prepareData(cot_hist, table='COT'):
    def getData(cot_hist, cot_date='2022-08-09'):
        index_values = pd.Series(cot_hist[cot_date][table].index.values)
        index = pd.Series(np.arange(1,len(index_values)+1),index=index_values).index.map('_'.join)
        df = cot_hist[cot_date][table].reset_index()
        df.index = index
        return df

    df=pd.DataFrame()
    for k in cot_hist.keys():
        df=df.append(getData(cot_hist, cot_date=k))
        
    if table=='COT':
        df['Date'] = pd.to_datetime(df['Date'])
        df['Code'] = df['Code'].astype(str)
        df['Type'] = df['Type'].astype(str)
        df['COT'] = df['COT'].astype(str)
        df['long'] = df['long'].astype(str)
        df['short'] = df['short'].astype(str)
        df['spreads'] = df['spreads'].astype(str)
    else:
        df.columns=['Date','Code','Name','Exchange','Nominal value','Open interest','Change in oi','Total traders']
        df['Date'] = pd.to_datetime(df['Date'])
        df['Code'] = df['Code'].astype(str)
        df['Name'] = df['Name'].astype(str)
        df['Exchange'] = df['Exchange'].astype(str)
        df['Nominal value'] = df['Nominal value'].astype(str)
        df['Open interest'] = df['Open interest'].astype(str)
        df['Change in oi'] = df['Change in oi'].astype(str)
        df['Total traders'] = df['Total traders'].astype(str)
        df=df.reset_index()
        del df['index']
    return df

def process2(lines_of_cot, debug=False):
    from datetime import datetime
    
    contracts={}
    idx = 0
    for idx in range(0, len(lines_of_cot)):
        if 'Code-' in lines_of_cot[idx]:
            results = {}
            data = lines_of_cot[idx:idx+19]
            
            cid = data[0].split('Code-')[0].split('-')
            cname=data[0].split('Code-')[0]
            cexchange = cid[len(cid)-1].strip()
            cname=cname.replace(cexchange,'').strip()
            cname=cname[:len(cname)-2]
            cid=data[0].split('-')
            ccontract = cid[len(cid)-1]
            
            contracts['Contract_'+str(idx)] = {'code': ccontract.strip(),
                                      'name': cname.strip(),
                                      'exchange': cexchange.strip()}        
    return pd.DataFrame(contracts).T

def process(lines_of_cot, commodity_name='EURO FX', debug=False):
    from datetime import datetime
    
    idx = 0
    for idx in range(0, len(lines_of_cot)):
        if commodity_name in lines_of_cot[idx]:
            results = {}
            data = lines_of_cot[idx:idx+19]
            
            cot_date = str(datetime.strptime(data[1].split()[5], '%m/%d/%y'))[:10]
            
            cid = data[0].split('Code-')[0].split('-')
            cname=data[0].split('Code-')[0]
            cexchange = cid[len(cid)-1].strip()
            cname=cname.replace(cexchange,'').strip()
            cname=cname[:len(cname)-2]
            cid=data[0].split('-')
            ccontract = cid[len(cid)-1]
            
            openint = data[7].split(':')[1]
            contract_nomval = data[7].split('OPEN INTEREST:')[0].replace('(','').replace(')','')#.split('EUR')[1].split(')')[0]
            
            nonreppos_long = 0
            nonreppos_short = 0
            cnonreppos_long = 0
            cnonreppos_short = 0
            if len(data[9].split())>7:
                nonreppos_long = data[9].split()[7]
                nonreppos_short = data[9].split()[8]
            if len(data[12].split())>7:
                cnonreppos_long = data[12].split()[7]
                cnonreppos_short = data[12].split()[8]
            if len(data[15].split())>7:
                pnonreppos_long = data[15].split()[7]
                pnonreppos_short = data[15].split()[8]
            
            copenint = data[11].split(':')[1].replace(')','')
            tot_traders = data[17].split(':')[1].replace(')','')
            
            traders = " ".join(data[18].split()).split(" ")
            current = " ".join(data[9].split()).split(" ")
            changes = " ".join(data[12].split()).split(" ")
            ointst = " ".join(data[15].split()).split(" ")
            results['non-commercial'] = {'current': {'long': current[0],
                                                     'short': current[1], 
                                                     'spreads': current[2]},
                                         'changes': {'long': changes[0],
                                                     'short': changes[1],
                                                     'spreads': changes[2]},
                                         'perc_ointerest': {'long': ointst[0],
                                                            'short': ointst[1],
                                                            'spreads': ointst[2]},
                                         'traders': {'long': traders[0],
                                                     'short': traders[1],
                                                     'spreads': traders[2]}}
            
            results['commercial'] = {'current': {'long': current[3],
                                                 'short': current[4]},
                                     'changes': {'long': changes[3],
                                                 'short': changes[4]},
                                     'perc_ointerest': {'long': ointst[3],
                                                 'short': ointst[4]},
                                     'traders': {'long': traders[3],
                                                 'short': traders[4]}}
            
            results['non-reportable'] = {'current': {'long': nonreppos_long,
                                                 'short': nonreppos_short},
                                         'changes': {'long': cnonreppos_long,
                                                 'short': cnonreppos_short},
                                         'perc_ointerest': {'long': pnonreppos_long,
                                                 'short': pnonreppos_short}}
            
            results['details'] = {'contract': {'name': cname.strip(),
                                              'code': ccontract.strip(),
                                              'exchange': cexchange.strip(),
                                              'nominal value': contract_nomval.strip(),
                                              'open interest': openint.strip(),
                                              'change in oi': copenint.strip(),
                                              'total traders': tot_traders.strip(),
                                              'date': cot_date.strip()}}
            
            return results   # Returns the first contract with the name
        
    return 0

def _download_report(date_to_get='latest', debug=False):
    import urllib.request

    user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'
    headers={'User-Agent':user_agent,} 
    tmp_file='latest.htm'
    
    if date_to_get == 'latest':
        base_url="http://www.cftc.gov/dea/futures/deacmesf.htm"
        if debug: print(base_url)
        
        request=urllib.request.Request(base_url,None,headers) 
        response = urllib.request.urlopen(request)
        fhand = open(tmp_file, 'wb')
        fhand.write(response.read())
        fhand.close()

    else:
        base_url = 'http://www.cftc.gov/files/dea/cotarchives/%year%/futures/deacmesf%month%%day%%Year%.htm'
        year, month, day = date_to_get.split('-')
        base_url = base_url.replace('%year%', year).replace('%month%', month). replace('%day%', day).replace('%Year%', year[2:])
        if debug: print(base_url)
        
        request=urllib.request.Request(base_url,None,headers) 
        response = urllib.request.urlopen(request)
        fhand = open(tmp_file, 'wb')
        fhand.write(response.read())
        fhand.close()

    return tmp_file

def getContracts(report_date = '2022-08-09', debug=False):
    report = _download_report(report_date, debug=debug)
    with open(report, 'r') as fd:
        cot = process2(fd.readlines(), debug=debug)
    
    return cot

##############################################
# COTs from CFTC.gov (old version)
##############################################      
def downloadCOTs():
    def getWeekdays(dstart='2005-09-06', dend='2022-08-15', days=['Tuesday']):
        from datetime import datetime, timedelta

        ddays={'Sunday':6,
              'Monday':0,
              'Tuesday':1,
              'Wednesday':2,
              'Thursday':3,
              'Friday':4,
              'Saturday':5,
             }
        drange=[ddays[d] for d in days]
    
        start = datetime(int(dstart[0:4]), int(dstart[5:7]), int(dstart[8:10]))
        end = datetime(int(dend[0:4]), int(dend[5:7]), int(dend[8:10]))
        days = (start + timedelta(days=i) for i in range((end - start).days + 1))
        return [d for d in days if d.weekday() in drange ]

    cot_hist={}
    for d in pd.DataFrame(getWeekdays(days=['Tuesday'])).values:
        cot_date = str(str(d)[2:12])
        try:
            cot,det = getCommOfTraders(report_date = cot_date, contract_name='ALL')
            cot_hist[cot_date] = {'COT':cot,'Details':det}
        except:
            print('Not able to download for :', cot_date)
            pass;
    return cot_hist

##############################################
# Obtain COT fora specific date/contract
##############################################      
def getCommOfTraders(report_date = '2022-08-09', contract_name='EURO FX', debug=False):

    def prepdf(df,cot_date,cot_type,cot_code):
        df['Date']=cot_date
        df['Type']=cot_type
        df['Code']=cot_code
        if cot_type != 'Non Commercial': df['spreads']=0
        df=df.reset_index()
        df.index = pd.MultiIndex.from_arrays(df[['Date','Code','Type','index']].values.T, names=['Date','Code','Type' ,'COT'])
        del df['index']
        del df['Date']
        del df['Type']
        del df['Code']
        return df
    
    def prepcot(cot):
        cot_date=cot['details']['contract']['date']
        cot_code=cot['details']['contract']['code']
        
        noncomm=pd.DataFrame(cot['non-commercial']).T
        noncomm=prepdf(noncomm,cot_date,'Non Commercial',cot_code)
        comm=pd.DataFrame(cot['commercial']).T
        comm=prepdf(comm,cot_date,'Commercial',cot_code)
        nonrep=pd.DataFrame(cot['non-reportable']).T
        nonrep=prepdf(nonrep,cot_date,'Non Reportable',cot_code)
        cot_report=noncomm.append(comm).append(nonrep)
        
        cot_details=pd.DataFrame(cot['details']).T.set_index('date')
        #cot_details=pd.DataFrame(cot['details']).T
        #cot_details.columns=['Date','Code','Name','Exchange','Nominal value','Open interest','Change in oi','Total traders']]
        #cot_details.index = pd.MultiIndex.from_arrays(cot_details[['Date','Code']].values.T, names=['Date','Code'])
        #cot_details=cot_details[['Name','Exchange','Nominal value','Open interest','Change in oi','Total traders']]
        cot_details=cot_details[['code','name','exchange','nominal value','open interest','change in oi','total traders']]
        
        return cot_report, cot_details

    report = _download_report(report_date, debug=debug)
    if contract_name != 'ALL':
        with open(report, 'r') as fd:
            cot = process(fd.readlines(), contract_name, debug=debug)
        cot_report, cot_details = prepcot(cot)
    else:
        i=0
        with open(report, 'r') as fd:
            ctrcs = process2(fd.readlines(), debug=debug)
        for i in range(len(ctrcs)):
            cname=ctrcs['name'].iloc[i]
            with open(report, 'r') as fd:
                cot = process(fd.readlines(), cname, debug=debug)
                if i==0:
                    cot_report, cot_details = prepcot(cot)
                else:
                    cot_report1, cot_details1 = prepcot(cot)
                    cot_report=cot_report.append(cot_report1)
                    cot_details=cot_details.append(cot_details1)
                i=i+1
    return cot_report, cot_details

##############################################
# Obtain/Store Contracts (old version)
##############################################      
def getContract(contract='239742',table='COT', myfile='TradersCommitment.hdf'):
    store = pd.HDFStore(myfile)
    df=store.select(table, where = "Code=='"+contract+"'")
    store.close()
    if table=='COT':
        df.index = pd.MultiIndex.from_arrays(df[['Date','Code','Type','COT']].values.T, names=['Date','Code','Type' ,'COT'])
        del df['Date']
        del df['Code']
        del df['Type']
        del df['COT']
    else:
        df=df.set_index('Date')
    return df

def getContract2(date1='2022-06-09',date2='2022-08-09',table='COT', myfile='TradersCommitment.hdf'):
    store = pd.HDFStore(myfile)
    c = store.select_column(table,'Date') 
    where = pd.DatetimeIndex(c).to_series().between(date1,date2)
    df=store.select(table,where = where)
    store.close()
    if table=='COT':
        df.index = pd.MultiIndex.from_arrays(df[['Date','Code','Type','COT']].values.T, names=['Date','Code','Type' ,'COT'])
        del df['Date']
        del df['Code']
        del df['Type']
        del df['COT']
    else:
        df=df.set_index('Date')
    return df

def getContract3(contract='037021', ddate='2022-08-09', myfile='TradersCommitment.hdf'):
    cot=getContract(contract=contract,table='COT', myfile='TradersCommitment.hdf')
    det=getContract(contract=contract,table='Details', myfile='TradersCommitment.hdf')
    cot=cot[cot.index.get_level_values(0)==ddate]
    det=det[det.index==ddate]
    display(det,cot)
    return det,cot

def storeCOT(records, table='COT', myfile='TradersCommitment.hdf'):
    try:
        store = pd.HDFStore(myfile)
        c = store.select_column(table,'Date') 
        where = pd.DatetimeIndex(c).to_series().between(str(records.Date.min()),str(records.Date.max()))
        arch=store.select(table,where = where)
        store=store.close()
        idx_archives = arch.index
        idx_updates = records.index
        idx3=idx_updates.difference(idx_archives)
        records=records.loc[idx3]
    except:
        pass;
    store = pd.HDFStore(myfile)
    if len(records)>0:
        store.append(table, records, format='table',complevel=2, complib='lzo',data_columns=records.iloc[[0]].columns.tolist())
        print('Added ',len(records),' records')
    store=store.close()
