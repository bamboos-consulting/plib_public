#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Plots 
#
# Module comprising functions to plot portfolios
#############################################################################################      


#import monthly_returns_heatmap as mrh
from matplotlib import gridspec
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.ticker as ticker
import numpy as np;np.random.seed(0)
import scipy
import pandas as pd
import numpy as np
import calendar
import seaborn as sns#; sns.set()
from matplotlib.ticker import PercentFormatter
from datetime import date    

# Need to import the matplotlib_converters from pandas,
# whenever you try to plot the columns of a dataframe using a for loop
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

import warnings
warnings.filterwarnings("once")

##############################################
# Plots Portfolio Optimization from Simulation
##############################################               
def plotPortfolioOptimization(portfolios,fs=(12, 8)):
    
    # Minimum Risk Portfolio
    df=portfolios.loc[portfolios.Ann_Variance == portfolios.Ann_Variance.min()]
    minv_x, minv_y = df.Ann_Variance.iloc[0], df.Ann_Returns.iloc[0]

    # Maximum Sharpe Portfolio
    df=portfolios.loc[portfolios.Sharpe_Ratio == portfolios.Sharpe_Ratio.max()]
    maxs_x, maxs_y = df.Ann_Variance.iloc[0], df.Ann_Returns.iloc[0]

    # Risk Parity Portfolio
    df=portfolios.loc[portfolios.RiskContr_SSE == portfolios.RiskContr_SSE.min()]
    minrp_x, minrp_y = df.Ann_Variance.iloc[0], df.Ann_Returns.iloc[0]
    
    # Geom. Returns Portfolio
    df=portfolios.loc[portfolios.Geom_Rets == portfolios.Geom_Rets.max()]
    mingrp_x, mingrp_y = df.Ann_Variance.iloc[0], df.Ann_Returns.iloc[0]
    

    # Plotting it visually
    fig = plt.figure(figsize=fs)
    plt.scatter(x='Ann_Variance', y='Ann_Returns', data=portfolios, label='Simulated Portfolios')
    plt.scatter(x=minv_x, y=minv_y, marker='*', s=200, color='orange', label='Minimum Risk Portfolio')
    plt.scatter(x=maxs_x, y=maxs_y, marker='*', s=200, color='red', label='Max Returns/Risk Portfolio')
    plt.scatter(x=minrp_x, y=minrp_y, marker='*', s=200, color='brown', label='Risk Parity Portfolio')
    plt.scatter(x=mingrp_x, y=mingrp_y, marker='*', s=200, color='green', label='Geom. Returns Portfolio')
    
    plt.xlabel('Annualized Volatility')
    plt.ylabel('Annualized Returns')
    plt.title('Portfolio Risk Vs Returns')
    plt.legend(loc='best', fontsize=14)
    plt.grid()
    plt.show()

##############################################
# Plots Uses of cash
##############################################               
def plotTradesAnalysis(r,tradesheet,fs=(15,4)):

    fig = plt.figure(figsize=fs)
    gs = gridspec.GridSpec(3, 10)
    ax1 = plt.subplot(gs[0:2, 2:10])
    for name in ['Account','Long','Short']:
        ax1.plot(r[name][:-1],label=name,linewidth=3)
        ax1.legend(loc=2)  
        ax1.tick_params(axis='x', labelrotation=20)
        plt.title("Uses of Cash")
        plt.ylabel("Currency")
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    name='Position'
    ax2.plot(r[name][:-1], color='k',label=name,linestyle='-.', linewidth=1)
    plt.grid(which="major", color='k', linestyle='-.', linewidth=0.2)
    fig.add_subplot(ax1)
    
    ax7= plt.subplot(gs[0:3, 0:2])
    col = [ round(x,2) for x in list(tradesheet.Value.values[:]) ]
    df=pd.DataFrame(tradesheet.Parameters.values)
    df['Value']=col
    df.columns=['Param','Value']
    bbox=[0, 0, 1, 1]
    ax7.axis('off')
    mpl_table = ax7.table(cellText = df.values, bbox=bbox, colLabels=df.columns)
    mpl_table.auto_set_font_size(False)
    mpl_table.set_fontsize(8)
    fig.add_subplot(ax7)

    fig.tight_layout(rect=[0, 0.03, 1, 0.95])
    plt.show()

##############################################
# Plots Various measures of return and sheets
##############################################               
def plotReturnAnalysis(risk_free_rate,tearsheet,returns,cumprod_ret,peak_index,trough_index,cumprod_market_ret,leverage=1.2,fs=(15,7),lbl_pr='portfolio',lbl_mk='market',log_rets=False):
    
    fig = plt.figure(figsize=(15, 7))
    gs = gridspec.GridSpec(3, 10)

    # Plot the cumulative product of returns
    ax1 = plt.subplot(gs[0, 2:6])
    plt.plot(cumprod_ret)
    plt.plot([peak_index, trough_index], [cumprod_ret[peak_index], cumprod_ret[trough_index]], 'o', color='r',
             markersize=5)
    plt.title("Cumulative Returns")
    plt.ylabel('Returns')
    plt.legend(["Cumulative Returns", "Peak and Trough"])
    plt.xticks(rotation=20)
    plt.grid(which="major", color='k', linestyle='-.', linewidth=0.5)
    fig.add_subplot(ax1)

    # Plotting the running maximum drawdown
    ax2 = plt.subplot(gs[0, 6:10])
    if log_rets:
        wealth_index=1000*np.exp((returns[lbl_pr]).cumsum())
    else:
        wealth_index=1000*(1+returns[lbl_pr]).cumprod()
    previous_peaks=wealth_index.cummax()
    drawdown=(wealth_index-previous_peaks)/previous_peaks
    plt.plot(drawdown, color='r')
    plt.plot([peak_index, trough_index], [drawdown[peak_index], drawdown[trough_index]], 'o', color='r',
             markersize=5)
    plt.title("Running Maximum Drawdown")
    plt.ylabel('Maximum Drawdown')
    plt.legend(["Running Maximum Markdown", "Peak and Trough"])
    plt.xticks()
    plt.yticks()
    plt.xticks(rotation=20)
    plt.grid(which="major", color='k', linestyle='-.', linewidth=0.2)
    plt.fill_between(drawdown.index, drawdown, alpha=0.5, color='r', linewidth=0)
    fig.add_subplot(ax2)

    # Plot the histogram of returns of the portfolio
    ax3 = plt.subplot(gs[1, 2:6])
    returns.dropna(inplace=True)
    plt.grid(which="major", color='k', linestyle='-.', linewidth=0.5)
    plt.hist(returns[lbl_pr], histtype='bar', alpha=0.5, ec='black')
    plt.title("Returns")
    plt.xlabel("Returns in %")
    plt.ylabel("Number of Days")
    plt.xticks()
    plt.yticks()
    plt.xticks(rotation=20)
    fig.add_subplot(ax3)

    # RETURNS AGAINST BENCHMARK WITHOUT LEVERAGE
    ax4 = plt.subplot(gs[1, 6:10])
    if log_rets:
        cumprod_ret_leverage_2 = np.exp((returns[lbl_pr]).cumsum())-1
    else:
        cumprod_ret_leverage_2 = (returns[lbl_pr] + 1).cumprod()-1
    plt.plot(cumprod_ret_leverage_2, label='Portfolio')
    plt.plot(cumprod_market_ret, label='S&P500')
    plt.title("S&P 500 Vs Portfolio without Leverage")
    plt.ylabel("Annualized Returns")
    plt.legend(["Portfolio", "S&P500"])
    plt.xticks()
    plt.yticks()
    plt.xticks(rotation=20)
    plt.grid(which="major", color='k', linestyle='-.', linewidth=0.5)
    fig.add_subplot(ax4)

    if leverage > 1:
        ax5 = plt.subplot(gs[2, 6:10])
        if log_rets:
            cumprod_ret_leverage_2 = leverage*np.exp((returns[lbl_pr]).cumsum())-1
        else:
            cumprod_ret_leverage_2 = leverage*(returns[lbl_pr] + 1).cumprod()-1
        plt.plot(cumprod_ret_leverage_2, label='Portfolio')
        plt.plot(cumprod_market_ret, label='S&P500')
        plt.title("S&P 500 Vs Portfolio with Leverage = " + str(leverage))
        plt.ylabel("Annualized Returns")
        plt.legend(["Portfolio", "S&P500"])
        plt.xticks()
        plt.yticks()
        plt.xticks(rotation=20)
        plt.grid(which="major", color='k', linestyle='-.', linewidth=0.5)
        fig.add_subplot(ax5)
        
    #df=pd.DataFrame()
    #df['sharpe']=pmp.roll_sharpe(returns.copy(),int(len(returns/5)),risk_free_rate,lbl_pr)
    #returns['sortino']=pmp.roll_sortino(returns,int(len(returns[lbl_pr])/5),risk_free_rate,lbl_pr)
    ax6 = plt.subplot(gs[2, 2:6])
    plt.plot(returns['sortino'], label='Sortino ratio')
    plt.axhline(y=returns['sortino'].mean(), color='r', linestyle='-')
    plt.title("Rolling " + str(int(len(returns[lbl_pr])/5))+ "-days Sortino ratio")
    plt.ylabel("Sortino")
    plt.xticks()
    plt.yticks()
    plt.xticks(rotation=20)
    plt.grid(which="major", color='k', linestyle='-.', linewidth=0.5)
    fig.add_subplot(ax6)
        
    ax7= plt.subplot(gs[0:3, 0:2])
    col = [ round(x,4) for x in list(tearsheet.Value.values[:]) ]
    df=pd.DataFrame(tearsheet.Parameters.values)
    df['Value']=col
    df.columns=['Param','Value']
    bbox=[0, 0, 1, 1]
    ax7.axis('off')
    mpl_table = ax7.table(cellText = df.values, bbox=bbox, colLabels=df.columns)
    mpl_table.auto_set_font_size(False)
    mpl_table.set_fontsize(8)
    fig.add_subplot(ax7)
    
    fig.tight_layout(rect=[0, 0.03, 1, 0.95])
    plt.show()
    
    rets=prepareMonthlyReturns(returns,lbl_rtn='portfolio')
    ref_month=rets.min()[1]
    ref_year=rets.min()[0]
    ret=plotMonthlyHeatMap(rets.round(4),ref_month,ref_year,lbl_rtn='portfolio',fs=fs)
     
    
    fig.tight_layout(rect=[0, 0.03, 1, 0.95])
    plt.show()

##############################################
# FF-5 Plots and regression
##############################################               
def plotPerfAttribution(mdata1,mdata2,model,fs=(10,3)):
    fig = plt.figure(figsize=fs)
    gs = gridspec.GridSpec(10, 21)

    ax1 = plt.subplot(gs[0:4, 0:6])
    g=plt.hist(mdata1.port_excess, bins=20, color='c', edgecolor='k', alpha=0.65)
    g=plt.axvline(model.intercept_, color='k', linestyle='dashed', linewidth=3)
    plt.title('Alpha: {:.4f}'.format(round(model.intercept_,4)), fontsize=8)
    fig.add_subplot(ax1)

    ax2 = plt.subplot(gs[0:4, 7:13])
    g=plt.hist(mdata2.mkt_excess, bins=20, color='c', edgecolor='k', alpha=0.65)
    g=plt.axvline(model.coef_[0], color='k', linestyle='dashed', linewidth=3)
    plt.title('MKT: {:.4f}'.format(round(model.coef_[0],4)), fontsize=8)
    fig.add_subplot(ax2)

    ax3 = plt.subplot(gs[0:4, 14:21])
    g=plt.hist(mdata2.SMB, bins=20, color='c', edgecolor='k', alpha=0.65)
    g=plt.axvline(model.coef_[1], color='k', linestyle='dashed', linewidth=3)
    plt.title('SMB: {:.4f}'.format(round(model.coef_[1],4)), fontsize=8)
    fig.add_subplot(ax3)

    ax4 = plt.subplot(gs[6:10, 0:6])
    g=plt.hist(mdata2.HML, bins=20, color='c', edgecolor='k', alpha=0.65)
    g=plt.axvline(model.coef_[2], color='k', linestyle='dashed', linewidth=3)
    plt.title('HML: {:.4f}'.format(round(model.coef_[2],4)), fontsize=8)
    fig.add_subplot(ax4)

    ax5 = plt.subplot(gs[6:10, 7:13])
    g=plt.hist(mdata2.RMW, bins=20, color='c', edgecolor='k', alpha=0.65)
    g=plt.axvline(model.coef_[3], color='k', linestyle='dashed', linewidth=3)
    plt.title('RMW: {:.4f}'.format(round(model.coef_[3],4)), fontsize=8)
    fig.add_subplot(ax5)

    ax6 = plt.subplot(gs[6:10, 14:21])
    g=plt.hist(mdata2.CMA, bins=20, color='c', edgecolor='k', alpha=0.65)
    g=plt.axvline(model.coef_[4], color='k', linestyle='dashed', linewidth=3)
    plt.title('CMA: {:.4f}'.format(round(model.coef_[4],4)), fontsize=8)
    fig.add_subplot(ax6)

    plt.show()

##############################################
# Montecarlo probability and simulations
##############################################               
def plotMontecarlo(brets,sret,smdd,dmc,dmcp,N,fs=(10, 10),bins=50,title='Montecarlo',conf_lev=0,log_rets=False):
    import Plib.Stats.Utils as su
    import matplotlib.ticker as mtick
    
    if conf_lev!=0:
        cut=int((100-100*conf_lev)/2)
        percentile=dmc[['Returns']].copy()
        percentile=percentile.sort_values('Returns', ascending=False)
        percentile=percentile[cut:len(percentile)-cut]
        print('The '+str(100*conf_lev)+'% confidence level is: '+str(round(100*percentile.min()[0],3))+'% '+ str(round(100*percentile.max()[0],3))+'%')
    
    fig = plt.figure(figsize=fs)
    gs = gridspec.GridSpec(8, 10)
    gs.update(wspace=-0.15, hspace=-0.15) # set the spacing between axes. 
    
    ax1 = plt.subplot(gs[0:2, 0:10])
    plt.grid(which="major", color='k', linestyle='-.', linewidth=0.5)
    n, x, _ = plt.hist(dmc['Returns'], histtype='bar', alpha=0.5, ec='black',bins=bins,density=True)
    density=scipy.stats.gaussian_kde(dmc['Returns'])
    plt.plot(x, density(x))
    plt.title(title + " Simulation of Trades Permutations (" +str(N)+ " samples)")
    #plt.xlabel("Returns")
    plt.ylabel("Simulations #")
    plt.axvline(sret, color='k', linestyle='dashed', linewidth=2,label='Strategy Return')
    plt.xticks()
    plt.yticks()
    plt.xticks(rotation=20)
    ax1.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    prob=su.get_gaussianProb(sret, x.max(), 50, dmc['Returns'])
    tprob='  Prob. strategy rets ('+str(round(sret,7))+') occurring by chance: ' + str(prob)
    style = dict(size=10, color='gray')
    ax1.text(x.min()*1.1, n.max()*0.9, tprob, **style)
    ax1.xaxis.set_major_formatter(mtick.PercentFormatter())
    fig.add_subplot(ax1)

    ax2 = plt.subplot(gs[3:5, 0:10])
    plt.grid(which="major", color='k', linestyle='-.', linewidth=0.5)
    n, x, _ = plt.hist(dmc['MaxDD'], histtype='bar', alpha=0.5, ec='black',bins=bins,density=True)
    density=scipy.stats.gaussian_kde(dmc['MaxDD'])
    plt.plot(x, density(x))
    plt.axvline(smdd, color='k', linestyle='dashed', linewidth=2,label='Strategy MDD')
    #plt.xlabel("MaxDD")
    plt.ylabel("Simulations #")
    plt.xticks()
    plt.yticks()
    ax2.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.xticks(rotation=20)
    ax2.xaxis.set_major_formatter(mtick.PercentFormatter())
    fig.add_subplot(ax2)
    
    ax3 = plt.subplot(gs[6:8, 0:10])
    plt.grid(which="major", color='k', linestyle='-.', linewidth=0.5)
    x=(dmcp.T).index
    for i in range(0,len(dmcp)):
        if log_rets:
            y=100 * np.exp(((dmcp.T)[i]).cumsum())-1
        else:
            y=((dmcp.T)[i] + 1).cumprod() * 100
        try:
            plt.plot(x, y)
        except:
            pass
    if log_rets:
        y=100 * np.exp((brets['Returns']).cumsum())-1
    else:
        y=(brets['Returns'] + 1).cumprod() * 100
    try:
        plt.plot(x, y, color='black', linestyle='dashed',linewidth=2, label='Backtest Returns')
    except:
        pass
    plt.ylabel("Returns")
    plt.xlabel("Days")
    plt.yticks()
    plt.xticks(rotation=20)
    ax3.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    fig.add_subplot(ax3)
    
    #fig.tight_layout(rect=[0, 0.03, 1, 0.95])
    
    plt.show()

##############################################
# Highlight highest hit ratio on stock plot
##############################################               
def plotBestHR(seq,pf,hr=0,fig=(18,9)):
    import matplotlib.pyplot as plt

    fig = plt.figure(figsize=fig)
    ax = plt.axes()

    plt.title('Highest Hit Ratio ('+str(hr)+'%) for Strategy',fontsize=18)

    plt=pf.plot(ax=ax)

    for i in range(0,len(seq)):
        a=seq['Date Start'].iloc[i]
        b=seq['Date End'].iloc[i]
        plt.axvspan(a, b, color='y', alpha=0.5, lw=0)
 
##############################################
# Highlight sequence of trades producing x USD
##############################################               
def plotBestSequence(seq,data={},sumt=0,fig=(18,9)):
    import matplotlib.pyplot as plt
    fig = plt.figure(figsize=fig)
    ax = plt.axes()

    plt.title('Shortest Sequence of Trades for $' + str(sumt),fontsize=18)
    
    a=seq.head(1)['Date Start']
    b=seq.head(1)['Date End']

    plt=data.plot(ax=ax)
    plt.axvspan(a, b, color='y', alpha=0.5, lw=0)

##############################################
# Arrange Rets in tabular format (daily data)
##############################################           
def prepareMonthlyReturns(asset_returns,lbl_rtn='Returns',lbl_date='date',normed=False):
    from pandas.tseries.offsets import CustomBusinessMonthBegin
    from pandas.tseries.holiday import USFederalHolidayCalendar
    bmth_us = CustomBusinessMonthBegin(calendar=USFederalHolidayCalendar())
    
    normed_returns=asset_returns
    #normed_returns=asset_returns.replace([np.inf, -np.inf], np.nan).interpolate()
    if normed: 
        normed_returns = (normed_returns - normed_returns.mean()) / normed_returns.std()
    #normed_returns=normed_returns.replace([np.inf, -np.inf], np.nan).interpolate()
    
    # Compute monthly average
    portfolio_monthly_mean=normed_returns.resample(bmth_us).mean()
    portfolio_monthly_count=normed_returns.resample(bmth_us).count()
    normed_returns=(portfolio_monthly_mean + 1) ** (portfolio_monthly_count) - 1

    normed_returns.reset_index(inplace=True)
    
    normed_returns['month2'] = normed_returns[lbl_date].dt.month
    normed_returns['Year'] = normed_returns[lbl_date].dt.year

    normed_returns['Month'] = normed_returns['month2'].apply(lambda x: calendar.month_name[x])
    normed_returns.reset_index(inplace=True)
    normed_returns=normed_returns[['Year','Month',lbl_rtn]]    
    return normed_returns
 
##############################################
# Arrange Vols in tabular format (daily data)
##############################################       
def prepareMonthlyVols(asset_vols,lbl_rtn='Vol-21',lbl_date='Date'):
    from pandas.tseries.offsets import CustomBusinessMonthBegin
    from pandas.tseries.holiday import USFederalHolidayCalendar
    bmth_us = CustomBusinessMonthBegin(calendar=USFederalHolidayCalendar())

    # Compute monthly average
    asset_vols[lbl_rtn]=asset_vols[lbl_rtn]*((21)**0.5)
    asset_vols=asset_vols.resample(bmth_us).mean()

    asset_vols.reset_index(inplace=True)

    asset_vols['month2'] = asset_vols[lbl_date].dt.month
    asset_vols['Year'] = asset_vols[lbl_date].dt.year

    asset_vols['Month'] = asset_vols['month2'].apply(lambda x: calendar.month_name[x])
    asset_vols.reset_index(inplace=True)
    asset_vols=asset_vols[['Year','Month',lbl_rtn]]
    return asset_vols

##############################################
# Arrange Trades in tabular format (daily data)
##############################################       
def prepareMonthlyHR(asset_hr,lbl_rtn='HR-21',lbl_date='Date'):
    from pandas.tseries.offsets import CustomBusinessMonthBegin
    from pandas.tseries.holiday import USFederalHolidayCalendar
    bmth_us = CustomBusinessMonthBegin(calendar=USFederalHolidayCalendar())

    # Compute monthly average
    asset_hr=asset_hr.resample(bmth_us).mean()

    asset_hr.reset_index(inplace=True)

    asset_hr['month2'] = asset_hr[lbl_date].dt.month
    asset_hr['Year'] = asset_hr[lbl_date].dt.year

    asset_hr['Month'] = asset_hr['month2'].apply(lambda x: calendar.month_name[x])
    asset_hr.reset_index(inplace=True)
    asset_hr=asset_hr[['Year','Month',lbl_rtn]]
    return asset_hr

##############################################
# Heatmap from daily data
##############################################        
def plotMonthlyHeatMap(rets,ref_month,ref_year,t='',lbl_rtn='Returns',fs=(18,9)):
    rets["month"] = rets["Month"].astype("category")
    rets["month"].cat.set_categories(["January", "February", "March", "April", "May", "June","July","August","September","October","November","December"],inplace=True)
    rets = rets.pivot("month", "Year", lbl_rtn)
    
    # Define the plot
    fig, ax = plt.subplots(figsize=fs)
    #title = t
    #ttl = ax.title
    #ttl.set_position([0.5,1.05])
    
    sns.heatmap(rets, center=rets.loc[ref_month, ref_year],linewidths=.5,cmap="YlGnBu",annot=True,ax=ax,fmt=".2%",cbar_kws={'format': '%.2f%%'})
    cbar = ax.collections[0].colorbar
    cbar.ax.yaxis.set_major_formatter(PercentFormatter(1, 0))
    
    # Display the Heatmap
    plt.ylabel("Months")
    plt.xticks(rotation=15)
    plt.tight_layout()
    if t != '':
        plt.title(t, fontsize = 14)
    
    plt.show()
    return rets

##############################################
# Heatmap of trades from hourly data
##############################################         
def plotTradesHeatmap(trades,fs=(8, 8),tz='America/New_York'):
   
    tr=trades[['perc','side','duration','tstart']]
    tr['tstart']=tr['tstart'].apply(lambda x: pd.to_datetime(x))
    tr['tstart_day_name'] = tr['tstart'].dt.day_name();#dt.weekday_name
    tr['tstart_time_hour'] = tr['tstart'].dt.hour;
    tr_day_hour2 = pd.pivot_table(tr[['tstart_day_name', 'tstart_time_hour', 'perc']], index=['tstart_day_name', 'tstart_time_hour'], aggfunc='mean')
    tr_day_hour3 = tr_day_hour2.unstack(level=0)
    #morning_hours = []
    #for hour in range(1, 12):
    #    detailed_hour = str(hour) + "am"
    #    morning_hours.append(detailed_hour)
    #afternoon_hours = []
    #for hour in range(1, 12):
    #    detailed_hour = str(hour) + "pm"
    #    afternoon_hours.append(detailed_hour)
    #detailed_hours = ["12am"] + morning_hours + ["12pm"] + afternoon_hours
    day_short_names = ['Mon', 'Tues', 'Wed', 'Thurs', 'Fri']
    tr_day_hour3 = tr_day_hour3.replace([np.inf, -np.inf], np.nan)
    tr_day_hour3=tr_day_hour3.fillna(0)
    
    
    f, ax = plt.subplots(figsize=fs)
    #sns.set(font_scale=0.8)
    #sns.set_context("talk")
    #yticklabels=detailed_hours
    ax = sns.heatmap(tr_day_hour3,cmap="YlGnBu",annot=True,annot_kws={"size": 10},linewidths=.5,xticklabels=day_short_names,ax=ax,fmt=".2%",cbar_kws={'format': '%.2f%%'})
    ax.figure.axes[-1].yaxis.label.set_size(10)
    cbar = ax.collections[0].colorbar
    cbar.ax.yaxis.set_major_formatter(PercentFormatter(1, 0))
    cbar.ax.tick_params(labelsize=10)
    plt.title("Heatmap of Trades Returns by Day and Hour of Day", fontsize = 14)
    plt.ylabel('')
    plt.yticks(rotation=0)
    plt.xlabel('')
    f.tight_layout(rect=[0, 0.03, 1, 0.95])
    plt.show() 
    
    return tr

##############################################
# Heatmap of vols from hourly data
##############################################            
def plotVolHeatmap(vols,sec,lbl_vol,fs=(8, 8),tz='America/New_York'):
    #Requires a df with OHLCAV data and vols with 'Vol-30D' col name
    tr=vols[[lbl_vol,'Date']]
    tr['Date'] = pd.to_datetime(tr['Date'],utc=True)
    tr['Date']=tr['Date'].apply(lambda x: pd.to_datetime(x).tz_convert(tz))
    tr['tstart_day_name'] = tr['Date'].dt.day_name();#dt.weekday_name
    tr['tstart_time_hour'] = tr['Date'].dt.hour;
    tr_day_hour2 = pd.pivot_table(tr[['tstart_day_name', 'tstart_time_hour', lbl_vol]], index=['tstart_day_name', 'tstart_time_hour'], aggfunc='mean')
    tr_day_hour3 = tr_day_hour2.unstack(level=0)

    #morning_hours = []
    #for hour in range(1, 12):
    #    detailed_hour = str(hour) + "am"
    #    morning_hours.append(detailed_hour)
    #afternoon_hours = []
    #for hour in range(1, 12):
    #    detailed_hour = str(hour) + "pm"
    #    afternoon_hours.append(detailed_hour)
    #detailed_hours = ["12am"] + morning_hours + ["12pm"] + afternoon_hours
    day_short_names = ['Mon', 'Tues', 'Wed', 'Thurs', 'Fri']
    tr_day_hour3 = tr_day_hour3.replace([np.inf, -np.inf], np.nan)
    tr_day_hour3=tr_day_hour3.fillna(0)
    f, ax = plt.subplots(figsize=fs)
    #sns.set(font_scale=0.8)
    #sns.set_context("talk")
    #yticklabels=detailed_hours
    ax = sns.heatmap(tr_day_hour3,cmap="YlGnBu",annot=True,annot_kws={"size": 10},linewidths=.5,xticklabels=day_short_names,ax=ax,fmt=".2%",cbar_kws={'format': '%.2f%%'})
    ax.figure.axes[-1].yaxis.label.set_size(10)
    cbar = ax.collections[0].colorbar
    cbar.ax.tick_params(labelsize=10)
    plt.title(sec + " Heatmap of Volatility by Day and Hour of Day", fontsize = 13)
    plt.ylabel('')
    plt.yticks(rotation=0)
    plt.xlabel('')
    f.tight_layout(rect=[0, 0.03, 1, 0.95])
    plt.show() 
    
    return tr