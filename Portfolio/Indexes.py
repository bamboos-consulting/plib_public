#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Indexes 
#
# Module comprising functions to study indexes 
#############################################################################################      

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path+"/")

#Common Libraries
import numpy as np
import pandas as pd
import datetime

import warnings
warnings.filterwarnings("once")


##############################################
# Retrieve Index Constitutents
##############################################
def getIndexConst(index='IWM'):
	url='https://www.ishares.com/us/literature/pcf/pcf-iwm-en_us.csv'
	data = pd.read_csv(url,header=2)
	data.columns=['Index','Sec_Name','SecId','SecTicker','N_Shares']
	return data
    
##############################################
# Download Data for Momentum Portfolio
##############################################    
def prepareMomData(stocks,d1='2017-04-10 00:00:00',d2='2022-04-15 00:00:00'):
    from scipy import stats
    import Plib.DataFarm.FRData as fr

    cols= ['Ticker', 
            'Price', 
            'Market Cap',
            'One-Year Price Return', 
            'One-Year Return Percentile',
            'Six-Month Price Return',
            'Six-Month Return Percentile',
            'Three-Month Price Return',
            'Three-Month Return Percentile',
            'One-Month Price Return',
            'One-Month Return Percentile',
            'HQM Score'
            ]

    i=0
    mmod=int(len(stocks)/6)
    for t in stocks:
        try:
            df2=fr.getData('fund',t)[['MarketCapitalization']]
            df2.columns=['Market Cap']
            df2=df2.interpolate().dropna()

            df1=fr.getData('stock',t,d1=d1,d2=d2,freq='D')
            df1=df1[['Adjusted_close']]
            df1.columns=['Price']

            df2.index=df2.index.tz_localize('America/New_York')
            temp = pd.merge(df1, df2, how="outer",left_index=True, right_index=True).bfill().interpolate().dropna()
            temp['Ticker']=str(t)
            temp['One-Year Price Return']= temp.Price.pct_change(252)
            temp['One-Year Return Percentile']= 0
            temp['Six-Month Price Return']=temp.Price.pct_change(periods = 6)
            temp['Six-Month Return Percentile']=0
            temp['Three-Month Price Return']=temp.Price.pct_change(periods = 3)
            temp['Three-Month Return Percentile']=0
            temp['One-Month Price Return']=temp.Price.pct_change(21)
            temp['One-Month Return Percentile']=0
            temp['HQM Score']=0
            temp=temp[cols].tail(1)
            if i==0:
                hqm_dataframe=temp.copy()
            else:
                hqm_dataframe=hqm_dataframe.append(temp)
        except:
            pass;
        i=i+1
        if i % mmod==0: print('Collecting data: ',i,' of ',len(stocks))
    hqm_dataframe=hqm_dataframe.reset_index()    
    del hqm_dataframe['index']   

    return hqm_dataframe

##############################################
# Download Data for Value Portfolio
##############################################    
def prepareValueData(stocks,d1='2017-04-10 00:00:00',d2='2022-04-15 00:00:00'):
    from scipy import stats
    import Plib.DataFarm.FRData as fr
    
    cols=['Ticker',
            'Price',
            'Number of Shares to Buy', 
            'Price-to-Earnings Ratio',
            'PE Percentile',
            'Price-to-Book Ratio',
            'PB Percentile',
            'Price-to-Sales Ratio',
            'PS Percentile',
            'EV/EBITDA',
            'EV/EBITDA Percentile',
            'EV/GP',
            'EV/GP Percentile',
            'RV Score']
    
    i=0
    mmod=int(len(stocks)/6)
    for t in stocks:
        try:
            df2=fr.getData('fund',t)
            df2=df2[['MarketCapitalization','PriceToEarningsRatio','PriceToBookValue',
                'PriceToSales','EnterpriseValuetoEBITDA','EnterpriseValuation','GrossProfit']]
            df2.columns=['Market Cap','Price-to-Earnings Ratio','Price-to-Book Ratio','Price-to-Sales Ratio','EV/EBITDA','EV','GP']
            df2=df2.interpolate().dropna()
            df2['EV/GP'] = df2['EV']/df2['GP']
            df2=df2.interpolate().dropna()
            del df2['EV']
            del df2['GP']

            df1=fr.getData('stock',t,d1=d1,d2=d2,freq='D')
            df1=df1[['Adjusted_close']]
            df1.columns=['Price']

            df2.index=df2.index.tz_localize('America/New_York')
            temp = pd.merge(df1, df2, how="outer",left_index=True, right_index=True).bfill().interpolate().dropna()
            temp['Ticker']=str(t)
            temp['Number of Shares to Buy']= 0
            temp['PE Percentile']= 0
            temp['PB Percentile']=0
            temp['PS Percentile']=0
            temp['EV/EBITDA Percentile']=0
            temp['EV/GP Percentile']=0
            temp['RV Score']=0
            temp=temp[cols].tail(1)
            if i==0:
                rv_dataframe=temp.copy()
            else:
                rv_dataframe=rv_dataframe.append(temp)
        except:
            pass;
        i=i+1
        if i % mmod==0: print('Collecting data: ',i,' of ',len(stocks))
    rv_dataframe=rv_dataframe.reset_index()    
    del rv_dataframe['index']   

    return rv_dataframe
    
##############################################
# Prepare Stats for Momentum Portfolio
##############################################        
def getMomentumStockQty(hqm_dataframe, portfolio_size, top_s=50):
    import math
    from scipy import stats
    
    time_periods = [
                'One-Year',
                'Six-Month',
                'Three-Month',
                'One-Month'
                ]
    for time_period in time_periods:
        hqm_dataframe[f'{time_period} Price Return'] =hqm_dataframe[f'{time_period} Price Return'].astype(dtype = 'float64')

    for row in hqm_dataframe.index:
        for time_period in time_periods:
            hqm_dataframe.loc[row, f'{time_period} Return Percentile'] = stats.percentileofscore(hqm_dataframe[f'{time_period} Price Return'], hqm_dataframe.loc[row, f'{time_period} Price Return'])/100

    # Print each percentile score to make sure it was calculated properly
    #for time_period in time_periods:
    #    print(hqm_dataframe[f'{time_period} Return Percentile'])

    from statistics import mean

    for row in hqm_dataframe.index:
        momentum_percentiles = []
        for time_period in time_periods:
            momentum_percentiles.append(hqm_dataframe.loc[row, f'{time_period} Return Percentile'])
        hqm_dataframe.loc[row, 'HQM Score'] = mean(momentum_percentiles)
    
    
    hqm_dataframe.sort_values(by = 'HQM Score', ascending = False)
    hqm_dataframe = hqm_dataframe[:top_s+1]

    position_size = float(portfolio_size) / len(hqm_dataframe.index)
    for i in range(0, len(hqm_dataframe['Ticker'])-1):
        hqm_dataframe.loc[i, 'Number of Shares to Buy'] = math.floor(position_size / hqm_dataframe['Price'][i])
    return hqm_dataframe[:-1]

##############################################
# Prepare Stats for Value Portfolio
##############################################    
def getValueStockQty(rv_dataframe, portfolio_size, top_s=50):
    import math 
    from scipy import stats
    
    for column in ['Price-to-Earnings Ratio', 'Price-to-Book Ratio','Price-to-Sales Ratio',  'EV/EBITDA','EV/GP']:
        rv_dataframe[column].fillna(rv_dataframe[column].mean(), inplace = True)

    metrics = {
                'Price-to-Earnings Ratio': 'PE Percentile',
                'Price-to-Book Ratio':'PB Percentile',
                'Price-to-Sales Ratio': 'PS Percentile',
                'EV/EBITDA':'EV/EBITDA Percentile',
                'EV/GP':'EV/GP Percentile'
    }

    for row in rv_dataframe.index:
        for metric in metrics.keys():
            rv_dataframe.loc[row, metrics[metric]] = stats.percentileofscore(rv_dataframe[metric], rv_dataframe.loc[row, metric])/100

    # Print each percentile score to make sure it was calculated properly
    #for metric in metrics.values():
    #    print(rv_dataframe[metric])

    from statistics import mean

    for row in rv_dataframe.index:
        value_percentiles = []
        for metric in metrics.keys():
            value_percentiles.append(rv_dataframe.loc[row, metrics[metric]])
        rv_dataframe.loc[row, 'RV Score'] = mean(value_percentiles)
    
    rv_dataframe.sort_values(by = 'RV Score', inplace = True)
    rv_dataframe = rv_dataframe[:top_s]
    rv_dataframe.reset_index(drop = True, inplace = True)

    position_size = float(portfolio_size) / len(rv_dataframe.index)
    for i in range(0, len(rv_dataframe['Ticker'])-1):
        rv_dataframe.loc[i, 'Number of Shares to Buy'] = math.floor(position_size / rv_dataframe['Price'][i])
    return rv_dataframe
    
##########################################################
# Create a equally weighted index from a dataframe of
# stocks and a dictionary of marketcapitalizations
##########################################################              
def getReportingDates(hflist,t='MSFT'):
    l=list(hflist[t].sort_index().index.date)
    return [str(e) for e in l]

def getTopNTickers(hflist, cols, top=100, qdate='2020-04-30'):

    dmc={}
    for k in cols:
        df=hflist[k].iloc[hflist[k].index <= pd.to_datetime(qdate)].fillna(0)
        df=df.sort_index()
        if len(df) > 0:
            sd=str(df.tail(1).index.date[0])
            mc=df.tail(1).iloc[0][0]
            #additional selection on fundamentals data here
            #...
            dmc[k]=mc
    #Sort selected firms by market cap
    sortdmc=dict(sorted(dmc.items(), key=lambda x:x[1]))
    return list(sortdmc)[-top:]

def getTopNTickers2(hflist, cols, top=100, qdate='2020-04-30'):
    
    dmc={}
    #First pass: filter by fundamental data in second column
    for k in cols:
        df=hflist[k].iloc[hflist[k].index <= pd.to_datetime(qdate)].fillna(0)
        df=df.sort_index()
        if len(df) > 0:
            sd=str(df.tail(1).index.date[0])
            mc=df.tail(1).iloc[0][1]                      # 0 is first columns of dataframe
            dmc[k]=mc
    sortdmc=dict(sorted(dmc.items(), key=lambda x:x[1]))  # 1 is first columns of dataframe
    first_pass = list(sortdmc)[-3*top:]
    #Second pass: rank the filtered by market cap
    for k in first_pass:
        df=hflist[k].iloc[hflist[k].index <= pd.to_datetime(qdate)].fillna(0)
        df=df.sort_index()
        if len(df) > 0:
            sd=str(df.tail(1).index.date[0])
            mc=df.tail(1).iloc[0][0]
            dmc[k]=mc
    sortdmc=dict(sorted(dmc.items(), key=lambda x:x[1]))
    return list(sortdmc)[-top:]
        
def createEWIndex(stocks_quarter,cols, stocks, mcd, top=50,Z=5, iname='SP500', D=1, lbl='MarketCapitalization'):
    # Obtains total market capitalization and divisor
    d3t=mcd['AAPL'][mcd['AAPL'].columns[0]].copy()
    for t in [c for c in cols if c not in ['AAPL']]: 
        d3t=d3t+mcd[t][mcd[t].columns[0]].fillna(0)
    d3div=(1-((d3t/d3t.shift(1).fillna(1))-1)).interpolate().fillna(1)
    ic=stocks_quarter.copy(); ic['index']=0;ic=ic[['index']]
    
    for t in cols:
        # Selects stocks prices in quarter and convert index from timestamp
        s1=stocks[t].copy();s1.index=s1.index.date
        s2=stocks_quarter['index'].copy()#;s2.index=s2.index.date
        stk=pd.merge(s1,s2, left_index=True,right_index=True, how ="inner")[[t]].interpolate()
        # Obtains fundamentals in quarter
        d3=mcd[t][mcd[t].columns[0]].copy()
        d3.index=d3.index.date
        d3=pd.merge(d3, d3t, left_index=True,right_index=True, how ="inner").interpolate().fillna(0)
        d3=pd.merge(d3, d3div, left_index=True,right_index=True, how ="inner").interpolate().fillna(1)
        # Merge prices and fundamentals
        df=pd.merge(stk, d3, left_index=True,right_index=True, how ="outer").interpolate().fillna(0)
        df=df[(df.index >= pd.to_datetime(stk.index.min())) & (df.index <= pd.to_datetime(stk.index.max()))]
        df.columns=[str(t),lbl+'_'+str(t),lbl+'_tot','div']   #df=df.groupby(df.index).first()
        # Compute index weight and contribution for the stock t
        contrib=(df[t]*Z*((df[lbl+'_tot']/df['div'])/(top*df[t]))).interpolate().fillna(0)
        if len(contrib)>0:
            ic['index']=ic['index'] + contrib
        
    ic['Div']=df['div']
    ic.columns=[iname + ' - (Equal Weight)','Divisor']
    return ic.interpolate()

def getIndex(stocks, fdata,qmax='2022-07-31',qmin='2010-01-31',iname='SP500', Z=5, top=10, selfunction=None, lbl='MarketCapitalization'):
    # Defines fundamentals dimension: nrec
    nrec=len(fdata['AAPL'].iloc[(fdata['AAPL'].index <= pd.to_datetime(qmax)) & (fdata['AAPL'].index >= pd.to_datetime(qmin))])
    tickers=[]
    # Removes tickers not satisfying the requirement above
    for k in list(fdata.keys()):
        mysel=fdata[k].iloc[(fdata[k].index <= pd.to_datetime(qmax)) & (fdata[k].index >= pd.to_datetime(qmin))]
        mysel['test']=mysel[lbl]/mysel[lbl]
        if len(mysel['test'].dropna())==nrec:
            tickers.append(k)
    
    # Defines a index for date reference
    def mark_quarters(x):
        return([str(quarters[i]) for i in range(1,len(quarters)) if x <= quarters[i] and x>= quarters[i-1]])

    quarters=fdata['AAPL'].index.copy()
    quarters=quarters.date

    stocks['index']=stocks['AAPL'].copy()
    stocks['index'].columns=['index']
    stocks['index'].index=stocks['index'].index.date
    stocks['index']['Date']=stocks['index'].index
    stocks['index']['Quarter'] = stocks['index']['Date'].apply(lambda x: ''.join(mark_quarters(x)))
    stocks['index']['Quarter'][ stocks['index'].index > quarters[-1] ] = str(quarters[-1])
    
    first=0
    for q in stocks['index'].Quarter.unique():
        # Select the date interval for the quarter
        stocks_quarter=stocks['index'][stocks['index'].Quarter==q]
        mdate=stocks_quarter.tail(1).index[0] #date
        # Select the tickers having the fundamentals in the date interval
        cols=selfunction(fdata,tickers, top=top, qdate=mdate)
        cols=[l for l in cols if l in stocks.keys()]
        
        if first==0:
            EW_index=createEWIndex(stocks_quarter,cols,stocks,fdata,top=top,Z=Z,iname=iname, D=1,lbl=lbl)
            first=first+1
        else:
            temp=createEWIndex(stocks_quarter,cols, stocks,fdata,top=top,Z=Z,iname=iname, D=1,lbl=lbl)
            EW_index=EW_index.append(temp)
    return EW_index.interpolate()
