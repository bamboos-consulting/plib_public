#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Analysis 
#
# Module comprising functions to analyze portfolios
#############################################################################################      

import numpy as np
import pandas as pd
import math
import Plib.Portfolio.Measures as pmp
import Plib.Portfolio.Plots as pl

#Intel(R) Extension for Scikit-learn dynamically patches scikit-learn estimators to use oneDAL as the underlying solver
#from daal4py.sklearn import patch_sklearn
from sklearnex import patch_sklearn
patch_sklearn()

##############################################
# Simple analysis and plot returns
##############################################      
def getCAGR(rets,log_rets=False,annualized=True):
    
    trets=np.array(rets.dropna())
    N=len(trets)
    if N==0:N=1     #find a better alternative
    period=1
    if annualized: period=365.25
    
    # Note: CAGR for negative yields in the last period is not computable 
    if log_rets:
        cagr=-1+np.exp((period/N)*(np.log(1+trets)).sum())
    else:
        cagr=-1+np.product(1+trets)**(period/N)
    return cagr
    
##############################################
# Analyze and plot returns
##############################################      
def returnAnalysis(rf=0.01, t_days=252,returns=[],retmkt=[],profits=0,ConfLev=0.95,leverage=1.2,fs=(15,7),lbl_pr='portfolio',lbl_mk='market',silent=False,log_rets=False):
    import numpy as np
    import scipy
    
    def acorr(op_samples, mean, separation, norm = 1):
        return ((op_samples[:op_samples.size-separation] - mean)*(op_samples[separation:]- mean)).ravel().mean() / norm
    
    risk_free_rate = rf
    trading_days = t_days
    
    #Simple returns or log returns ---------------------------------------------------|
    maximum_drawdown,peak_index,trough_index = pmp.maxDrawdown(returns,lbl_pr,log_rets)
    #mdd_duration = int(abs(peak_index-trough_index).days)
    mdd_duration=(abs(peak_index-trough_index) / np.timedelta64(1, 'D'))
    cumprod_ret, cumprod_market_ret = pmp.cumProdRt(returns,retmkt,lbl_pr,lbl_mk,log_rets)
    str_returns=cumprod_ret[-1]
    mkt_returns=cumprod_market_ret[-1]
        
    annual_returns,average_daily_returns,daily_std_dev,annual_volatility,skewness,excess_kurtosis=pmp.portMoments(returns,trading_days,lbl_pr)
    sharpe_ratio = pmp.sharpeRatio(returns,risk_free_rate,trading_days,lbl_pr)
    sortino_ratio = pmp.sortinoRatio(returns,risk_free_rate,trading_days,lbl_pr)
    #treynor_ratio, beta = pmp.treynorRatio(returns,risk_free_rate,trading_days,lbl_pr,lbl_mk) #
    information_ratio = pmp.informationRatio(returns,retmkt,trading_days,lbl_pr,lbl_mk) #
    #maximum_drawdown=max_dd_with_dates(returns)
    omega_ratio = pmp.omegaRatio(returns,risk_free_rate,trading_days,lbl_pr) #---> rf=MAR
    # the "t-test" is being applied in modern finance to determine if a series of historical returns is reliably 
    # superior to a risk-equivalent benchmark (i.e., it has a t-statistic of 2 or higher). A simple t-stat has the
    # equation t-stat = (average x √Observations) / standard deviation.
    # A sharpe ratio can be transformed in a t-stat by considering that a t-stat constructed to test the null hypothesis 
    # that the average return is zero has equation t-stat = average / ()√Observations / standard deviation). As a result,
    # the sharpe ratio can be considered as SR = t-stat/√Observations. Thus, fixed the number of observation, a higher Sharpe ratio
    # implies a higher t-statistic, which in turn implies a higher significance level (lower p-value) for the investment strategy.
    tstat=(sharpe_ratio)*(len(returns)/trading_days)**0.5 #other measures require detrending of the data before computing the measure  
    pval= (1 - scipy.stats.t.cdf(tstat,df=(len(returns)/trading_days)-1))
    # Finally, some overstatement exists because it is know that researchers have tried 200 strategies and choose to present the most
    # profitable one (largest Sharpe ratio). Thus, under the assumption all the strategies are profitable multiple testing 
    # decreases the power of our p-value because of an increasing probability of a false positive after 200 tests of strategies.
    # By computing the SR from the adjusted p-value one can find that the SR is reduced roughly of 60-70&% for 200 tests.  
    pfp= (1-(1-pval)**200) 
    # Similarly,  because the VaR is related to Sharpe ratio by VaR(α)/standard deviation =SR−zα where zα is the z-score for 
    # the (1 − α)-th percentile of a standard normal distribution, the adjusted VaR is more negative (about 60% for α = 1%).
    var=pmp.histNVaR(returns,ConfLev,lbl_pr)*np.sqrt(20)
    #tvar,tcvar=pmp.tVaR(returns,ConfLev,lbl_pr)
    #tvar,tcvar=tvar*np.sqrt(20),tcvar*np.sqrt(20)  
    if ConfLev > 0.95:
        adj_var=var*1.60
    else:
        adj_var=var*1.45
    # Form a table to get the values of all risk measures in structured manner
    tearsheet = pd.DataFrame({'Parameters': ['Str. Rets','Mkt Rets','Avg Daily Rets','Avd Daily Vol','Ann.ed Returns', 'Ann.ed Vol', 'Sharpe Ratio', 't-stat','Pvalue','Adj Pvalue','Sortino Ratio', 'Omega Ratio',
                                         'Inform. Ratio', 'Skewness', 'Kurtosis','30d HVaR'+str(int(ConfLev*100)),'30d adj VaR'+str(int(ConfLev*100)),'MDD Duration','Max Drawdown %'],
                          'Value': [str_returns,mkt_returns,average_daily_returns,daily_std_dev**2,100*annual_returns, 100*annual_volatility, sharpe_ratio, tstat,pval,pfp,sortino_ratio,omega_ratio,
                                    information_ratio, skewness, excess_kurtosis, 100*var,100*adj_var,mdd_duration,100*maximum_drawdown]})   
    
    returns['sortino']=pmp.roll_sortino(returns,int(len(returns[lbl_pr])/5),risk_free_rate,lbl_pr)
    
    if not silent:
        pl.plotReturnAnalysis(risk_free_rate,tearsheet,returns,cumprod_ret,peak_index,trough_index,cumprod_market_ret,leverage=leverage,fs=fs,lbl_pr=lbl_pr,log_rets=log_rets)
    return tearsheet

def tradesAnalysis(r,trades,silent=False,profits=0,fs=(12,5),log_rets=False):
    import pandas as pd
    import numpy as np
    
    def getConsCount(df,label,ctype='pos'):
        if ctype=='pos':
            df['cons']=np.where(df[label]>0, 1, 0)
        elif ctype=='neg':
            df['cons']=np.where(df[label]<=0, 1, 0)    
        y=df['cons']
        return (y * (y.groupby((y != y.shift()).cumsum()).cumcount() + 1)).max()
    
    rrr=0
    win_r=0
    kelly=0
    profit_factor=0
    expectancy=0
    expectunity=0
    quality=0
    grets=getCAGR(trades.perc,log_rets)
    avg_trade=(((trades.QtyI*trades.pricei)+(trades.QtyF*trades.pricef))/2).mean()
    winners=trades[trades['wl']==1]
    loosers=trades[trades['wl']!=1]
    
    total_trades=len(trades)
    total_wins=len(winners)
    total_loss=len(loosers)
    max_win=winners.usd.max()
    max_loss=loosers.usd.min()
    max_cwin=getConsCount(trades,'usd','pos')
    max_closs=getConsCount(trades,'usd','neg')
    avg_win=winners.usd.mean()
    avg_loss=loosers.usd.mean()
    avg_pwin=winners.perc.mean()
    avg_ploss=loosers.perc.mean()
    if total_trades!=0:
        win_r=np.round(total_wins / total_trades, 4)
    if avg_loss!=0:
        rrr=np.round(abs(avg_win / avg_loss), 2)
    if loosers.usd.sum()!=0:
        profit_factor=winners.usd.sum()/abs(loosers.usd.sum())
    win_duration=winners.duration.mean()
    losr_duration=loosers.duration.mean()
    if avg_loss !=0:
        expectancy=(avg_win*avg_pwin + avg_loss*avg_ploss)/-(avg_loss)
        expectunity=expectancy*total_trades
        quality=(expectancy/trades.usd.std())*(total_trades**0.5)
    ac=trades.usd.autocorr(lag=1)     
    if avg_loss!=0:
        kelly=((rrr)*avg_pwin-avg_ploss)/(rrr)
    
    if profits!=0:
        tradesheet = pd.DataFrame({'Parameters': ['Profits','Geom.Rets %','Total trades', 'Wins %', 'Average Win', 'Average Loss', 'Max Win','Max Loss',
                                             'Max Cons. Wins', 'Max Cons. Loss', 'Avg Win Durat','Avg Loss Durat','Payoff (x:1)',
                                             'Profit Factor','Expectancy','Exp.Opp.ty','S.Quality','Acorr(1)','Avg Trade','Kelly %'],
                              'Value': [profits,100*grets,total_trades, 100*win_r, avg_win, avg_loss,max_win, max_loss, max_cwin,
                                        max_closs, win_duration,losr_duration,rrr, profit_factor,expectancy,expectunity,quality,ac,avg_trade,100*kelly]})   
        
    else:
        tradesheet = pd.DataFrame({'Parameters': ['Total trades','Geom.Rets %', 'Wins %', 'Average Win', 'Average Loss', 'Max Win','Max Loss',
                                             'Max Cons. Wins', 'Max Cons. Loss','Avg Win Durat','Avg Loss Durat', 'Payoff (x:1)', 
                                             'Profit Factor','Expectancy','Exp.Opp.ty','S.Quality','Acorr(1)','Avg Trade','Kelly %'],
                              'Value': [total_trades,100*grets, 100*win_r, avg_win, avg_loss,max_win, max_loss, max_cwin,
                                        max_closs, win_duration,losr_duration,rrr, profit_factor,expectancy,expectunity,quality,ac,avg_trade,100*kelly]})   
    if not silent:
        pl.plotTradesAnalysis(r,tradesheet,fs=fs)     
    return tradesheet

########################################################################################
# Monte Carlo permutation method for rule testing was developed by Dr. Timothy Masters 
# The rule output is a series of 1/-1 representing long/short trades and their return in
# the chronological order of the backtest. As a reference, this series is compared with
# the detrended one-day-forward market returns; such a comparison is done multiple
# times by permuting without replacement the market series. Specifically, the return of
# each permutation is computed multiplying the rule pattern (i.e., the sequence of 1/-1)
# to the detrended one-day-forward market return; an average is computed. Finally, all 
# the averages are reported into a relative frequency histogram to assess the rule.
# Because thr market returns are detrended, the histogram is centered in zero; instead, 
# the rules's final return is plotted somewhere on the histogram. The farthest from zero
# the highest the rule's significance. Indeed, the relative frequency histogram has the
# sum of the bars to the right of the rule's final return equal to the p-value of the
# non-zero t-test of the rule (like a pdf); the less dispersed the data, the better.
########################################################################################
def montecarloAnalysis(d,ltavg_rets=0,N=100):
    import sklearn        
    from scipy import signal
    
    def max_dd(returns):
        r = returns.add(1).cumprod()
        dd = r.div(r.cummax()).sub(1)
        mdd = dd.min()
        return mdd*100       #to show nicely on the montecarlo plot
    
    np.random.seed()
    
    mc=d[['Traded','Returns','MRet']]            #---------------------> rule, returns and market returns
    if ltavg_rets != 0:
        mc['MRet']=mc['MRet'] - ltavg_rets       # detrend with the long term average mean if the backtest is shorter than the entire history
    else:
        mc['MRet']=mc['MRet']-mc['MRet'].mean()  #---------------------> rule, returns and detrended market returns
    
    mcp=mc.reset_index()[['Traded']]             #---------------------> rule only
    mcr=mc[['MRet']]                             #---------------------> detrended market returns only
    dmc=pd.DataFrame()
    dmcp=pd.DataFrame(np.zeros(len(mcr)))
    del dmcp[0]
    for i in range(0,N):
        perm = sklearn.utils.shuffle(mcr)
        perm=(perm.reset_index())[['MRet']]      #---------------------> permutation of detrended market returns
        dmcp[str(i)]=perm
        
        #Joining rule trades with permutated detrended market returns 
        mc=pd.merge(mcp,perm,left_index=True, right_index=True,how='outer').dropna()
        #Computing averages and mdd for the permutation
        data = {'Returns':  [(mc['Traded']*mc['MRet']).mean()],'MaxDD': [max_dd(mc['Traded']*mc['MRet'])]}
        
        #Collecting all averages and mdd
        dmc=dmc.append(pd.DataFrame(data))    
    
    return dmc.values,(dmcp.T).values

########################################################################################
# The bootstrap derives a sampling distribution of the test statistic by resampling with 
# replacement from an original sample. The bootstrap theorem assures us that it will 
# converge to a correct sampling distribution as the sample size goes to infinity. 
# From a practical standpoint, this means that given a single sample of observations 
# bootstrapping can produce the sampling distribution needed to test the significance of 
# a trading system or rule. The sequence of steps involved requires demeaning the 
# original sample and resampling with replacement each of the returns. This procedure
# should be repeated N times and each time the average and mdd is computed.
########################################################################################
def bootstrapAnalysis(d,ltavg_rets=0,N=100):
    import sklearn        
    from scipy import signal
    
    def max_dd(returns):
        r = returns.add(1).cumprod()
        dd = r.div(r.cummax()).sub(1)
        mdd = dd.min()
        return mdd*100       #to show nicely on the montecarlo plot
    
    np.random.seed()
    
    mc=d[['Traded','Returns']]                          #---------------------> rule, returns and market returns
    mc['MRet']=mc['Returns']-mc['Returns'].mean()       #---------------------> rule, returns and detrended market returns
    
    mcp=mc.reset_index()[['Traded']]         #---------------------> rule only
    mcr=mc[['MRet']]                         #---------------------> detrended market returns only
    dmc=pd.DataFrame()
    dmcp=pd.DataFrame(np.zeros(len(mcr)))
    del dmcp[0]
    for i in range(0,N):
        perm = sklearn.utils.resample(mcr,replace=True,n_samples=len(mcr))
        perm=(perm.reset_index())[['MRet']]  #---------------------> permutation of detrended market returns
        dmcp[str(i)]=perm
        
        #Joining rule trades with permutated detrended returns 
        mc=pd.merge(mcp,perm,left_index=True, right_index=True,how='outer').dropna()
        #Computing averages and mdd for the permutation
        data = {'Returns':  [(mc['Traded']*mc['MRet']).mean()],'MaxDD': [max_dd(mc['Traded']*mc['MRet'])]}

        #Collecting all averages and mdd
        dmc=dmc.append(pd.DataFrame(data))     
    
    return dmc.values,(dmcp.T).values
    
def volatilityAnalysis(df,sec,periods=21,silent=False,fs=(8,8),tz='America/New_York'):
    from Plib.Volatility.Estimation import rollVolRealized
    #Requires a df with OHLCAV data and vols with label col name and date with Dates col name
    rets=df.pct_change().fillna(0)
    vols=rollVolRealized(rets,periods).dropna().to_frame()
    vols.columns=['Vol-'+str(periods)]
    vols=vols.reset_index()
    if not silent:
        r=pl.plotVolHeatmap(vols,sec,'Vol-'+str(periods),fs,tz)
    return vols,r
      
def volatilityMAnalysis(df,sec,periods=21,silent=False,fs=(8,8),tz='America/New_York'):
    from Plib.Volatility.Estimation import rollVolRealized
    #Requires a df with OHLCAV data and vols with label col name and date with Dates col name
    rets=df.pct_change().fillna(0)
    vols=rollVolRealized(rets,periods).dropna().to_frame()
    
    if not silent:
        vols.columns=['Vol-'+str(periods)]
        vols2=pl.prepareMonthlyVols(vols,lbl_rtn='Vol-'+str(periods),lbl_date='Date')
        ref_month=vols2.min()[1]
        ref_year=vols2.min()[0]
        title=sec + ' Monthly HeatMap of Rolling '+str(periods)+' Realized Volatility'
        vols3=pl.plotMonthlyHeatMap(vols2.round(4),ref_month,ref_year,t=title,lbl_rtn='Vol-'+str(periods),fs=fs)
    return vols,vols3

def tradesFilters(trades,silent=False,fs=(8, 8),tz='America/New_York'):
    
    if not silent:
        tr=pl.plotTradesHeatmap(trades,fs,tz)
    return tr

def breakupAnalysis(d,period='Y'):
    df=d[['Traded']].reset_index()
    dfb=d[['Returns']].reset_index()

    df1=df[df.Traded > 0].groupby(pd.Grouper(key='Date',freq=period)).sum()
    df1.columns=['Pos_trades']
    df2=df[df.Traded < 0].groupby(pd.Grouper(key='Date',freq=period)).sum()
    df2.Traded=abs(df2.Traded)
    df2.columns=['Neg_trades']
    df3=dfb[dfb.Returns > 0].groupby(pd.Grouper(key='Date',freq=period)).mean()
    df3.columns=['Avg Wins %']
    df4=dfb[dfb.Returns < 0].groupby(pd.Grouper(key='Date',freq=period)).mean()
    df4.columns=['Avg Loss %']
    res=df1.join(df2, how='right', rsuffix='r',lsuffix='l')
    res['Hit Ratio']=res.Pos_trades/(res.Pos_trades+res.Neg_trades)
    if len(df3) >0: res=res.join(df3, how='right', rsuffix='r',lsuffix='l')
    if len(df4) >0: res=res.join(df4, how='right', rsuffix='r',lsuffix='l')
    return res.fillna(0)    

def hitratioMAnalysis(df,silent=False,fs=(14,8)):
    import Plib.Portfolio.Plots as pl
        
    if not silent:
        df.columns=['Hit Ratio']
        df2=pl.prepareMonthlyHR(df,lbl_rtn='Hit Ratio',lbl_date='Date')
        ref_month=df2.min()[1]
        ref_year=df2.min()[0]
        title='Monthly HeatMap of Hit Ratio'
        df3=pl.plotMonthlyHeatMap(df2.round(4),ref_month,ref_year,t=title,lbl_rtn='Hit Ratio',fs=fs)

    return df,df3

def selHRMonths(df,pf,hr,side,silent=False,fs=(14,8)):
    seq={}
    if not silent:
        seq=df[df['Hit Ratio']>hr].reset_index()
        seq['Date Start']=pd.to_datetime(seq['Date']).dt.date
        seq['Date Start']=seq['Date Start'].apply(lambda x: x.strftime('%Y-%m-01'))
        seq['Date End']=pd.to_datetime(seq['Date']).dt.date
        pl.plotBestHR(seq,pf,hr,fs)
    return seq
        
def tradeSeqAnalysis(trades,sumt,top=3,fs=(18,9),silent=False,data={}):
    def findSublists(myseq, sumt):
            ret=[]
            for i in range(len(myseq)):
                tot = 0
                for j in range(i, len(myseq)):
                    tot += myseq[j][1]
                    if tot >= sumt:
                        ret.append(myseq[i:j+1])
            return ret

    df=trades[['tend','usd']]
    df.columns=['Date','WL']
    df=df.sort_values(by=['Date'])

    Trades_list=[]
    for i in range(0,len(df)):
        item=[df.Date.iloc[i],int(df.WL.iloc[i])]
        Trades_list.append(item)
    
    ret=findSublists(Trades_list,sumt)
    
    date_list=[]
    for k in range(0,len(ret)):
        date_list.append([ret[k][0][0],ret[k][-1][0]])
    df = pd.DataFrame(date_list,columns=['Date Start','Date End'])
    df['Duration']=(df['Date End']-df['Date Start'])
    df=df.sort_values(by=['Duration'])
    
    if not silent==True:
        if len(df.head(top)) >0:
            if int(df.head(top).Duration.mean().days)>0:
                pl.plotBestSequence(df,data,sumt,fs)
    return df.head(top)
    
##################################################
# Compute Portfolio Statistics
##################################################      
def getStats(pprices,preturns,days,MAR,ConfLev,freq,decimals=4):
    import numpy as np
    import scipy
   
    #Retrieve strategy components, realtime and historical hourly data, and prepare value and returns at market prices
    #accordingly to initial purchase price
    #def printStatsBySid(sid,index_symbol='SPY',MAR=0.01,days=15,freq=252, ConfLev=0.95,useBroker=True)
    #def getPortPrices(asset_prices,weights)
    #def getPortReturns(asset_prices,weights)
    #Now take this array of prices and returns and prepare measures
    
    #clean data and prepare space
    pprices.columns=['prices']
    preturns.columns=['returns']
    returns = preturns.replace([np.inf, -np.inf], np.nan)
    returns=returns.dropna()
    
    risk_free_rate = MAR
    trading_days = 252
    
    annual_returns,average_daily_returns,daily_std_dev,annual_volatility,skewness,excess_kurtosis=pmp.portMoments(returns,trading_days,lbl_pr)
    sharpe_ratio = pmp.sharpeRatio(returns,risk_free_rate,trading_days,lbl_pr)
    sortino_ratio = pmp.sortinoRatio(returns,risk_free_rate,trading_days,lbl_pr)
    maximum_drawdown,peak_index,trough_index = pmp.maxDrawdown(returns,lbl_pr)
    mdd_duration=(abs(peak_index-trough_index) / np.timedelta64(1, 'D'))
    
    omega_ratio = pmp.omegaRatio(returns,MAR,trading_days,lbl_pr)
    var,cvar = pmp.norm_VaR(annual_returns,annual_volatility,ConfLev)
    
    # Form a table to get the values of all risk measures in structured manner
    tearsheet = pd.DataFrame({'Parameters': ['Annual Returns', 'Ann. Volatility', 'Sharpe Ratio', 'Sortino Ratio', 'Omega Ratio',
                                             'Skewness', 'Kurtosis','VaR95','CVar95','MDD Duration','Max Drawdown %'],
                          'Value': [annual_returns, annual_volatility, sharpe_ratio, sortino_ratio,omega_ratio, 
                                    skewness, excess_kurtosis, var,cvar,mdd_duration,maximum_drawdown]})   

    return tearsheet.round(decimals)

##############################################
# Fama-Factors Performance Attribution
##############################################      
def get_fama_french():
    import urllib.request
    import zipfile
    
    ff_url = "https://mba.tuck.dartmouth.edu/pages/faculty/ken.french/ftp/F-F_Research_Data_5_Factors_2x3_CSV.zip"
    urllib.request.urlretrieve(ff_url,'fama_french.zip')
    zip_file = zipfile.ZipFile('fama_french.zip', 'r')
    zip_file.extractall()
    zip_file.close()
    ff_factors = pd.read_csv('F-F_Research_Data_5_Factors_2x3.CSV', skiprows = 3, index_col = 0)
    ff_row = ff_factors.isnull().any(1).to_numpy().nonzero()[0][0]
    ff_factors = pd.read_csv('F-F_Research_Data_5_Factors_2x3.CSV', skiprows = 3, nrows = ff_row, index_col = 0)
    ff_factors.index = pd.to_datetime(ff_factors.index, format= '%Y%m')
    ff_factors.index = ff_factors.index + pd.offsets.MonthEnd()
    ff_factors = ff_factors.apply(lambda x: x/ 100)
    return ff_factors

def perfAttribution(symbol,dt_start,dt_end,stats=True,silent=False,fs=(10,6),dwld=True,price_data=[]):
    from sklearn.model_selection import train_test_split
    from sklearn.linear_model import LinearRegression
    from sklearn import metrics
    import Plib.DataFarm.IEXdata as iex

    ff_data = get_fama_french()
    ff_last = ff_data.index[ff_data.shape[0] - 1].date()
    
    if dwld:
        df=iex.get_eod_data(symbol,dt_start,dt_end)
        price_data=df[['Adjusted_close']]
    price_data = price_data.loc[:ff_last]
    price = price_data.resample('M').last()
    ret_data = price.pct_change()[1:]
    ret_data = pd.DataFrame(ret_data)
    ret_data.columns = ['portfolio']
        
    all_data = pd.merge(pd.DataFrame(ret_data),ff_data, how = 'inner', left_index= True, right_index= True)
    all_data.rename(columns={"Mkt-RF":"mkt_excess"}, inplace=True)
    all_data['port_excess'] = all_data['portfolio'] - all_data['RF']

    X = all_data[['mkt_excess','SMB','HML','RMW','CMA']]
    y = all_data['port_excess']
    X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=0.2, random_state=0)
    data={}
    data['x_train']=X_train
    data['x_test']=X_valid
    data['y_train']=y_train
    data['y_valid']=y_valid
    data['all']=all_data
    data['price_data']=price_data
    data['ret_data']=ret_data
    
    regressor = LinearRegression()
    regressor.fit(X_train, y_train)
    
    r_sq = regressor.score(X_train, y_train)
    
    if stats:
        try:
            print('FF-5 Performance Attribution')
            print('coefficient of determination:', round(r_sq,4))
        except:
            pass
        try:
            mae=metrics.mean_absolute_error(y_valid, y_pred)
            print('Mean Absolute Error:', round(mae,4))
        except:
            pass
        try:
            mse=metrics.mean_squared_error(y_valid, y_pred)
            print('Mean Squared Error:', round(mse,4))
        except:
            pass
        try:
            rmse=np.sqrt(metrics.mean_squared_error(y_valid, y_pred))
            print('Root Mean Squared Error:', round(rmse,4))
        except:
            pass
        print('str_excess = ',round(regressor.intercept_,4),' + ',
            round(regressor.coef_[0],4),'mkt_excess + ',
            round(regressor.coef_[1],4),'SMB + ',
            round(regressor.coef_[2],4),'HML + ',
            round(regressor.coef_[3],4),'RMW + ',
            round(regressor.coef_[4],4),'CMA')    
    
    if not silent:
        pl.plotPerfAttribution(all_data[['port_excess']],all_data[['mkt_excess','SMB','HML','RMW','CMA']],regressor,fs)
    return regressor,data

##############################################
# Portfolio Optimization with Simulation
##############################################      
def portfolioSimulation(stocks_data, stocks_list, metrics_list, N=2500, trading_days = 252):

    no_of_stocks = len(stocks_list)

    # risk budgeting optimization
    def calculate_portfolio_var(w,V):
        # function that calculates portfolio risk
        w = np.matrix(w)
        return (w*V*w.T)[0,0]

    def calculate_risk_contribution(w,V):
        # function that calculates asset contribution to total risk
        w = np.matrix(w)
        sigma = np.sqrt(calculate_portfolio_var(w,V))
        # Marginal Risk Contribution
        MRC = V*w.T
        # Risk Contribution
        RC = np.multiply(MRC,w.T)/sigma
        return RC

    def risk_budget_objective(x,V,x_t):
        # calculate portfolio risk
        sig_p =  np.sqrt(calculate_portfolio_var(x,V)) # portfolio sigma
        risk_target = np.asmatrix(np.multiply(sig_p,x_t))
        asset_RC = calculate_risk_contribution(x,V)
        J = sum(np.square(asset_RC-risk_target.T))[0,0] # sum of squared error
        return J

    x_t = [0.25, 0.25, 0.25, 0.25] # your risk budget percent of total portfolio risk (equal risk)

    np.random.seed()
    daily_returns = stocks_data.pct_change().dropna()
    mean_returns = daily_returns.mean()
    demean_returns = stocks_data.pct_change().dropna().copy()
    for stock in stocks_list:
        demean_returns[stock] = demean_returns[stock] - mean_returns[stock]
    cov_matrix = demean_returns.T.dot(demean_returns) / len(stocks_data)

    # Define equal allocations weights
    w = 1 / len(stocks_list)
    weights = np.full(len(stocks_list), w)
    # Calculate portfolio variance
    portfolio_variance = (weights.T.dot(cov_matrix.values)).dot(weights)

    # Optimizing weights
    portfolios = pd.DataFrame(columns=stocks_list+metrics_list)

    # Define temporary dictionary to hold data within a for loop
    new_data = {}

    # Generate 2500 portfolios
    for i in range(0, N):

        # Generate random weights which equals to 1
        weights = np.reshape(np.random.dirichlet(np.ones(no_of_stocks), size=1), newshape=(no_of_stocks,))

        # Calculate daily returns of each portfolio
        #daily_port_returns = (weights[0] * daily_returns[stocks_list[0]]) + \
        #                (weights[1] * daily_returns[stocks_list[1]]) + \
        #                (weights[2] * daily_returns[stocks_list[2]]) + \
        #                (weights[3] * daily_returns[stocks_list[3]])

        daily_port_returns=pd.Series(np.zeros(len(daily_returns)))
        daily_port_returns.index=daily_returns.index
        for i in range(0,len(stocks_list)):
            daily_port_returns=daily_port_returns + (weights[i] * daily_returns[stocks_list[i]])

        #########################
        #Compute metrics
        #########################
        # Calculate cumulative returns of each portfolio
        cum_returns = (1 + daily_port_returns).cumprod() - 1

        # Calculate annualized returns of each portfolio
        n = len(daily_port_returns)
        ann_returns = ((1 + cum_returns[-1]) ** (trading_days/n)) - 1

        # Calculate daily returns of each portfolio
        daily_variance = (weights.T.dot(cov_matrix.values)).dot(weights)

        # Calculate annualized variance of each portfolio
        ann_variance = daily_variance * np.sqrt(trading_days)

        # Create a new column with maximum returns per unit of risk
        sharpe = (ann_returns / ann_variance)**0.5

        # Risk parity - multiplied by 1000 because very small
        risk_contr_sse= 1000*risk_budget_objective(weights,cov_matrix.values,x_t)
        
        geom_rets=np.product(1+daily_port_returns)**(252/(n))-1

        metrics=[ann_returns,ann_variance,cum_returns[-1],sharpe,risk_contr_sse,geom_rets]
        #########################

        # Append data to the dataframe
        #new_data['AAPL'] = np.round(weights[0], 3)
        #new_data['FB'] = np.round(weights[1], 3)
        #new_data['GOOG'] = np.round(weights[2], 3)
        #new_data['NFLX'] = np.round(weights[3], 3)
        #new_data['Ann_Returns'] = round(ann_returns, 3)
        #new_data['Ann_Variance'] = round(ann_variance, 6)

        for i in range(0,len(stocks_list)):
            new_data[stocks_list[i]] = np.round(weights[i], 3)

        for i in range(len(metrics_list)):
            new_data[metrics_list[i]] = np.round(metrics[i], 6)

        portfolios = portfolios.append(new_data, ignore_index=True)
    return portfolios.values

#############################################################################################
# Deprecated functions
#############################################################################################      
def VaRsim_GBM_JD():
    import matplotlib.pyplot as plt
    import scipy.stats as scs
    from scipy.stats import norm
    
    S0 = 100
    r = 0.05
    sigma = 0.25
    # 30 day var
    T = 30 / 365.
    I = 10000
    # end of period values for geometric brownian motion
    ST = S0 * np.exp((r - 0.5 * sigma ** 2) * T
                     + sigma * np.sqrt(T) * npr.standard_normal(I))
    # absolute profits and losses per simulation and sort
    R_gbm = np.sort(ST - S0)
    percs = [0.01, 0.1, 1., 2.5, 5.0, 10.0]
    var = scs.scoreatpercentile(R_gbm, percs)
    print("%16s %16s" % ('Confidence Level', 'Value-at-Risk (GBM)'))
    print(33 * "-")
    for pair in zip(percs, var):
        print("%16.2f %16.3f" % (100 - pair[0], -pair[1]))

    S0 = 100.
    r = 0.05
    sigma = 0.2
    lamb = 0.75
    # negative mean -> left fat tail
    mu = -0.6
    delta = 0.25
    T = 1.0
    M = 50
    I = 10000

    # daily var
    dt = 30. / 365 / M
    rj = lamb * (np.exp(mu + 0.5 * delta ** 2) - 1)
    S = np.zeros((M + 1, I))
    S[0] = S0
    sn1 = npr.standard_normal((M + 1, I))
    sn2 = npr.standard_normal((M + 1, I))
    poi = npr.poisson(lamb * dt, (M + 1, I))
    for t in range(1, M + 1, 1):
        S[t] = S[t - 1] * (np.exp((r - rj - 0.5 * sigma ** 2) * dt
                                  + sigma * np.sqrt(dt) * sn1[t])
                           + (np.exp(mu + delta * sn2[t]) - 1)
                           * poi[t])
        S[t] = np.maximum(S[t], 0)
    R_jd = np.sort(S[-1] - S0)
    plt.hist(R_jd, bins=50)
    plt.xlabel('absolute return')
    plt.ylabel('frequency')
    plt.grid(True)
    plt.show()
    percs = [0.01, 0.1, 1., 2.5, 5.0, 10.0]
    var = scs.scoreatpercentile(R_jd, percs)
    print("%16s %16s" % ('Confidence Level', 'Value-at-Risk (JD)'))
    print(33 * "-")
    for pair in zip(percs, var):
        print("%16.2f %16.3f" % (100 - pair[0], -pair[1]))
    percs = list(np.arange(0.0, 10.1, 0.1))
    gbm_var = scs.scoreatpercentile(R_gbm, percs)
    jd_var = scs.scoreatpercentile(R_jd, percs)
    plt.plot(percs, gbm_var, 'b', lw=1.5, label='GBM')
    plt.plot(percs, jd_var, 'r', lw=1.5, label='JD')
    plt.legend(loc=4)
    plt.xlabel('100 - confidence level [%]')
    plt.ylabel('value-at-risk')
    plt.grid(True)
    plt.ylim(ymax=0.0)
    plt.show()
def VaRsim_normGBM(days, sigma, drift, ConfLev):
    import matplotlib.pyplot as plt
    
    def random_walk(startprice):
        price = np.zeros(days)
        shock = np.zeros(days)
        price[0] = startprice
        for i in range(1, days):
            shock[i] = np.random.normal(loc=mu * dt, scale=sigma * np.sqrt(dt))
            price[i] = max(0, price[i - 1] + shock[i] * price[i - 1])
        return price

    # days = 300  # time horizon
    dt = 1 / float(days)
    # sigma = 0.04  # volatility
    # mu = 0.05  # drift (average growth rate)
    mu = drift
    runs = 10000
    simulations = np.zeros(runs)
    for run in range(runs):
        simulations[run] = random_walk(1.0)[days - 1]
    q = np.percentile(simulations, (1 - ConfLev) * 100)
    plt.hist(simulations, density=True, bins=30, histtype='stepfilled', alpha=0.5)
    plt.figtext(0.6, 0.8, "Start price: 1€")
    plt.figtext(0.6, 0.7, "Mean final price: {:.3}€".format(simulations.mean()))
    plt.figtext(0.6, 0.6, "VaR(" + str(ConfLev) + "): {:.3}€".format(1 - q))
    plt.figtext(0.15, 0.6, "q(" + str(ConfLev) + "): {:.3}€".format(q))
    plt.axvline(x=q, linewidth=4, color='r')
    plt.title("Final price distribution after {} days".format(days), weight='bold');
    plt.show()
    return -(1 - q) / 1
def test_VaR_Models():
    mu = 0.05
    sigma = 0.1
    days = 30
    conf = 0.95
    #mv, es = norm_VaR(mu, sigma, conf)
    gv = VaRsim_normGBM(days, sigma, mu, conf)
    print(mv)
    print(gv)
    VaRsim_GBM_JD()
 
##############################################
# Helper functions
##############################################      
def plotHist(returns, desc):
    import scipy
    import matplotlib.pyplot as plt
    
    fig = plt.figure()
    fig.set_size_inches(10, 6)
    returns.plot()
    plt.title(desc, weight='bold');
    plt.show()
    returns.hist(bins=50, density=True, histtype='stepfilled', alpha=0.5)
    plt.title("Histogram of " + desc, weight='bold')
    returns.std()
    plt.show()

    Q = returns.dropna()
    scipy.stats.probplot(Q, dist=scipy.stats.norm, plot=plt.figure().add_subplot(111))
    plt.title("Normal QQ-Plot of " + desc, weight="bold");
    plt.show()

    tdf, tmean, tsigma = scipy.stats.t.fit(Q)
    scipy.stats.probplot(Q, dist=scipy.stats.t, sparams=(tdf, tmean, tsigma), plot=plt.figure().add_subplot(111))
    plt.title("Student QQ-plot of " + desc, weight="bold");
    plt.show()

def normTest(returns):
    import scipy.stats as scs
    from pylab import plt as pplt
    pplt.style.use('seaborn')

    def print_statistics(array):
        sta = scs.describe(array)
        print("%14s %15s" % ('statistic', 'value'))
        print(30 * "-")
        print("%14s %15.5f" % ('size', sta[0]))
        print("%14s %15.5f" % ('min', sta[1][0]))
        print("%14s %15.5f" % ('max', sta[1][1]))
        print("%14s %15.5f" % ('mean', sta[2]))
        print("%14s %15.5f" % ('std', np.sqrt(sta[3])))
        print("%14s %15.5f" % ('skew', sta[4]))
        print("%14s %15.5f" % ('kurtosis', sta[5]))

    def normality_valids(arr):
        print("Skew of data set  %14.3f" % scs.skew(arr))
        print("Skew test p-value %14.3f" % scs.skewtest(arr)[1])
        print("Kurt of data set  %14.3f" % scs.kurtosis(arr))
        print("Kurt test p-value %14.3f" % scs.kurtosistest(arr)[1])
        print("Norm test p-value %14.3f" % scs.normaltest(arr)[1])

    print_statistics(returns.values)
    normality_valids(returns.values)

def test_normality(data2):
    data2.hist(bins=50, figsize=(10, 6))
    x.hist(bins=50, figsize=(10, 6))
    normTest(x['BOND'])
    y = np.log(data2 / data2.shift(1)).dropna()
    y.hist(bins=50, figsize=(10, 6))

##############################################
# Shrinkage of Covariance Matrix (Ledoit)
##############################################      
def covMarket(*args):
    # if __name__ == "__main__":
    # shrink is a constant
    # 16202057941895145
    x = args[0]
    if len(args) > 1:
        shrink = args[1]
    t = np.shape(x)[0]
    n = np.shape(x)[1]
    meanx = np.mean(x, axis=0)
    x = x - np.tile(meanx, (t, 1))
    xmkt = np.mean(x.reshape(n, t), axis=0)
    xmkt = np.reshape(xmkt, (t, 1))
    htile = np.transpose(np.hstack((x, xmkt)))
    sample = np.cov(htile) * (t - 1) / t
    covmkt = sample[:n, n]
    varmkt = sample[n, n]
    sample = np.delete(sample, n, axis=1)
    sample = np.delete(sample, n, axis=0)
    prior = (covmkt * covmkt.reshape(n, 1)) / varmkt
    np.fill_diagonal(prior, np.diag(sample))
    if (len(args) < 2 or shrink == -1):  # compute shrinkage parameters
        c = math.pow(np.linalg.norm(sample - prior, 'fro'), 2)
        y = np.power(x, 2)
        sum1 = np.sum(np.sum(np.power(sample, 2)))
        sum2 = np.sum(np.sum(np.matmul(y.T, y), axis=0))
        p = 1 / t * sum2 - sum1
        # r is divided into diagonal
        # and off-diagonal terms, and the off-diagonal term
        # is itself divided into smaller terms
        y1 = np.sum(np.sum(np.power(y, 2)))
        rdiag = 1 / t * y1 - np.sum(np.power(np.diag(sample), 2))
        z = x * np.tile(xmkt, (1, n))
        v1 = 1 / t * np.matmul(y.T, z) - np.tile(covmkt.reshape(n, 1), (1, n)) * sample
        t1 = np.tile(covmkt.reshape(n, 1), (1, n))
        roff1 = np.sum(np.sum(v1 * t1.T)) / varmkt - np.sum(np.diag(v1) * covmkt, axis=0) / varmkt
        v3 = 1 / t * np.matmul(z.T, z) - varmkt * sample
        roff3 = np.sum(np.sum(v3 * np.matmul(covmkt, covmkt.T))) / np.power(varmkt, 2) - np.sum(
            np.diag(v3) * np.power(covmkt, 2)) / np.power(varmkt, 2)
        roff = 2 * roff1 - roff3
        r = rdiag + roff
        k = (p - r) / c
        shrinkage = max(0, min(1, k / t))
    else:
        shrinkage = shrink
    sigma = shrinkage * prior + (1 - shrinkage) * sample

    return sigma, shrinkage
    
def test_shrinkage1():
    from scipy.linalg import toeplitz, cholesky
    from sklearn.covariance import LedoitWolf
    
    # simulation covariance matrix (AR(1) process)
    np.random.seed(0)
    n_features = 100
    r = 0.1
    real_cov = toeplitz(r ** np.arange(n_features))
    coloring_matrix = cholesky(real_cov)
    n_samples_range = np.arange(6, 31, 1)
    repeat = 100
    lw_mse = np.zeros((n_samples_range.size, repeat))
    lw_shrinkage = np.zeros((n_samples_range.size, repeat))
    X = np.dot(np.random.normal(size=(n_samples_range.size, n_features)), coloring_matrix.T)

    # Market Covariance Method
    sigma, shrinkage = covMarket(X)
    print(sigma, shrinkage)

    # Standard LW Method
    lw = LedoitWolf(store_precision=False, assume_centered=True)
    lw.fit(X)
    lwsigma = lw.covariance_
    lwshrinkage = lw.shrinkage_
    print(lwsigma, lwshrinkage)

def test_shrinkage2(data2):
    x = data2.pct_change()[1:].dropna()
    sigma, shrinkage = covMarket(x.values)
    print(sigma, shrinkage)

##############################################
# Portfolio Evaluation Functions and Plots 
##############################################      
def portOptimizationMaxSharpe(aweights, drets):
    import scipy.optimize as sco
    
    def port_ret(w, rets):
        return np.sum(rets.mean() * w) * 252

    def port_vol(w, rets):
        return np.sqrt(np.dot(w.T, np.dot(rets.cov() * 252, w)))

    def min_func_sharpe(aweights):
        return -port_ret(aweights, drets) / port_vol(aweights, drets)

    noa = len(drets.columns)
    cons = ({'type': 'eq', 'fun': lambda x: np.sum(x) - 1})
    bnds = tuple((0, 1) for x in range(noa))
    eweights = np.array(noa * [1. / noa, ])

    opts = sco.minimize(min_func_sharpe, eweights, method='SLSQP', bounds=bnds, constraints=cons)

    rweights = opts['x'].round(4)
    rpreturn = port_ret(opts['x'].round(4), drets)
    rpvol = port_vol(opts['x'].round(4), drets)
    rpsharpe = rpreturn / rpvol

    return rweights, rpreturn, rpvol, rpsharpe

def portOptimizationMinVol(aweights, drets):
    import scipy.optimize as sco
    
    def port_ret(w, rets):
        return np.sum(rets.mean() * w) * 252

    def port_vol(w, rets):
        return np.sqrt(np.dot(w.T, np.dot(rets.cov() * 252, w)))

    noa = len(drets.columns)
    cons = ({'type': 'eq', 'fun': lambda x: np.sum(x) - 1})
    bnds = tuple((0, 1) for x in range(noa))
    eweights = np.array(noa * [1. / noa, ])

    opts = sco.minimize(port_vol, eweights, drets, method='SLSQP', bounds=bnds, constraints=cons)

    rweights = opts['x'].round(4)
    rpreturn = port_ret(opts['x'].round(4), drets)
    rpvol = port_vol(opts['x'].round(4), drets)
    rpsharpe = rpreturn / rpvol

    return rweights, rpreturn, rpvol, rpsharpe

def plotEfficientFrontier(aweights, drets):
    import scipy.optimize as sco
    import matplotlib.pyplot as plt
    
    def port_ret(w, rets):
        return np.sum(rets.mean() * w) * 252

    def port_vol(w, rets):
        return np.sqrt(np.dot(w.T, np.dot(rets.cov() * 252, w)))

    noa = len(drets.columns)
    cons = ({'type': 'eq', 'fun': lambda x: port_ret(x, drets) - tret}, {'type': 'eq', 'fun': lambda x: np.sum(x) - 1})
    bnds = tuple((0, 1) for x in aweights)
    trets = np.linspace(0.05, 0.2, 50)
    tvols = []
    eweights = np.array(noa * [1. / noa, ])

    for tret in trets:
        res = sco.minimize(port_vol, eweights, drets, method='SLSQP', bounds=bnds, constraints=cons)
        tvols.append(res['fun'])
    tvols = np.array(tvols)

    msrw, mspr, mspv, mssh = portOptimizationMaxSharpe(aweights, drets)
    mvrw, mvpr, mvpv, mvsh = portOptimizationMinVol(aweights, drets)

    prets = []
    pvols = []
    for p in range(2500):
        mweights = np.random.random(noa)
        mweights /= np.sum(mweights)
        prets.append(port_ret(mweights, drets))
        pvols.append(port_vol(mweights, drets))
    prets = np.array(prets)
    pvols = np.array(pvols)

    plt.figure(figsize=(10, 6))
    plt.scatter(pvols, prets, c=prets / pvols, marker='.', alpha=0.8, cmap='coolwarm')
    plt.plot(tvols, trets, 'b', lw=4.0)
    plt.plot(mvpv, mvpr, 'y*', markersize=15.0)
    plt.plot(mspv, mspr, 'r*', markersize=15.0)
    plt.xlabel('expected volatility')
    plt.ylabel('expected return')
    plt.colorbar(label='Sharpe ratio')
    plt.show()

    return 0
    
def test_portf_opt(weights, x):
    rweights, rpreturn, rpvol, rpsharpe = portOptimizationMaxSharpe(weights, x)
    print(rweights)

    rweights, rpreturn, rpvol, rpsharpe = portOptimizationMinVol(weights, x)
    print(rweights)

    r = plotEfficientFrontier(weights, x)

def test():
    import Plots
    data1 = pd.read_csv("dataPA.csv", index_col=0, parse_dates=True).dropna()
    data2 = pd.read_csv("dataAL.csv", index_col=0, parse_dates=True).dropna()
    weights=[0.33,0.33,0.34]

    x = data2.pct_change()[1:].dropna()
    sigma, shrinkage = covMarket(x.values)
    print(sigma, shrinkage)

    data2.hist(bins=50,figsize=(10,6))
    x.hist(bins=50,figsize=(10,6))
    normTest(x['BOND'])
    y= np.log(data2 / data2.shift(1)).dropna()
    y.hist(bins=50,figsize=(10,6))

    #a = x[['BOND', 'EQUITY']].copy()
    #mplots=Plots.corrStudy(a,('BONDS','STOCKS'),30)
    #mplots.corrPlot()

    rweights,rpreturn,rpvol,rpsharpe = portOptimizationMaxSharpe(weights,x)
    print(rweights)

    rweights,rpreturn,rpvol,rpsharpe = portOptimizationMinVol(weights,x)
    print(rweights)

    r=plotEfficientFrontier(weights,x)

    df1=pd.DataFrame(data2.dot(weights),columns=['portfolio'])/100000
    df2=data2['EQUITY']/1000
    mydf=pd.concat([df1['portfolio'], df2.rename('market')], axis=1).dropna()
    #r=portfolioAnalysis(0.001,mydf)

##############################################
# Functions to create models from data
##############################################      
def best_fit_distribution(data, bins=200, ax=None):
    import scipy.stats as st
    
    """Model data by finding best fit distribution to data"""
    # Get histogram of original data
    y, x = np.histogram(data, bins=bins, density=True)
    x = (x + np.roll(x, -1))[:-1] / 2.0

    # Distributions to check
    DISTRIBUTIONS = [
        st.alpha, st.anglit, st.arcsine, st.beta, st.betaprime, st.bradford, st.burr, st.cauchy, st.chi, st.chi2,
        st.cosine,
        st.dgamma, st.dweibull, st.erlang, st.expon, st.exponnorm, st.exponweib, st.exponpow, st.f, st.fatiguelife,
        st.fisk,
        st.foldcauchy, st.foldnorm, st.frechet_r, st.frechet_l, st.genlogistic, st.genpareto, st.gennorm, st.genexpon,
        st.genextreme, st.gausshyper, st.gamma, st.gengamma, st.genhalflogistic, st.gilbrat, st.gompertz, st.gumbel_r,
        st.gumbel_l, st.halfcauchy, st.halflogistic, st.halfnorm, st.halfgennorm, st.hypsecant, st.invgamma,
        st.invgauss,
        st.invweibull, st.johnsonsb, st.johnsonsu, st.ksone, st.kstwobign, st.laplace, st.levy, st.levy_l,
        st.logistic, st.loggamma, st.loglaplace, st.lognorm, st.lomax, st.maxwell, st.mielke, st.nakagami, st.ncx2,
        st.ncf,
        st.nct, st.norm, st.pareto, st.pearson3, st.powerlaw, st.powerlognorm, st.powernorm, st.rdist, st.reciprocal,
        st.rayleigh, st.rice, st.recipinvgauss, st.semicircular, st.t, st.triang, st.truncexpon, st.truncnorm,
        st.tukeylambda,
        st.uniform, st.vonmises, st.vonmises_line, st.wald, st.weibull_min, st.weibull_max, st.wrapcauchy
    ]

    # Best holders
    best_distribution = st.norm
    best_params = (0.0, 1.0)
    best_sse = np.inf

    # Estimate distribution parameters from data
    for distribution in DISTRIBUTIONS:
        # print(distribution)
        # Try to fit the distribution
        try:
            # Ignore warnings from data that can't be fit
            with warnings.catch_warnings():
                warnings.filterwarnings("once")

                # fit dist to data
                params = distribution.fit(data)

                # Separate parts of parameters
                arg = params[:-2]
                loc = params[-2]
                scale = params[-1]

                # Calculate fitted PDF and error with fit in distribution
                pdf = distribution.pdf(x, loc=loc, scale=scale, *arg)
                sse = np.sum(np.power(y - pdf, 2.0))

                # if axis pass in add to plot
                try:
                    if ax:
                        pd.Series(pdf, x).plot(ax=ax)
                    end
                except Exception:
                    pass

                # identify if this distribution is better
                if best_sse > sse > 0:
                    best_distribution = distribution
                    best_params = params
                    best_sse = sse

        except Exception:
            pass

    return (best_distribution.name, best_params)

def make_pdf(dist, params, size=10000):
    """Generate distributions's Probability Distribution Function """

    # Separate parts of parameters
    arg = params[:-2]
    loc = params[-2]
    scale = params[-1]

    # Get sane start and end points of distribution
    start = dist.ppf(0.01, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.01, loc=loc, scale=scale)
    end = dist.ppf(0.99, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.99, loc=loc, scale=scale)

    # Build PDF and turn into pandas Series
    x = np.linspace(start, end, size)
    y = dist.pdf(x, loc=loc, scale=scale, *arg)
    pdf = pd.Series(y, x)

    return pdf
