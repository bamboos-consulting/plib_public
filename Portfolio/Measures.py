#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Measures 
#
# Module comprising helper functions to record transactions
#############################################################################################      


#Common Libraries
import numpy as np
import pandas as pd
import math
import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

##################################################
# Compute Portfolio Prices and Returns from series
##################################################      
def getPortPrices(asset_prices,weights):
    weighted_prices = pd.DataFrame(data=np.zeros(shape=(len(asset_prices.index), asset_prices.shape[1])), 
                                 columns=asset_prices.columns.values,
                                 index=asset_prices.index)
    weighted_prices = (weights * asset_prices)
    port_value = weighted_prices.sum(axis=1)
    df=pd.DataFrame(port_value,columns=['prices'])
    return df

def getPortReturns(asset_prices,weights):
    returns = pd.DataFrame(data=np.zeros(shape=(len(asset_prices.index), asset_prices.shape[1])), 
                                 columns=asset_prices.columns.values,
                                 index=asset_prices.index)
    returns=asset_prices.pct_change()[1:]
    returns=returns.replace([np.inf, -np.inf], np.nan).interpolate()
    weighted_returns = (weights * returns)
    port_ret = weighted_returns.sum(axis=1)
    df=pd.DataFrame(port_ret,columns=['returns'])
    return df

##################################################
# Moments: avd daily rets, annualized rets, stdev
#          ann. vol, skewness, kurtosis
##################################################      
def portMoments(returns=[],t_days=252,lbl_pr='portfolio'):
    # Calculate the average daily returns
    average_daily_returns = returns[lbl_pr].mean()
    # Calculate the annual returns by multiplying average daily returns with number of trading days
    annual_returns = ((1 + average_daily_returns) ** (t_days/len(returns)) - 1)
    # Calculate the daily standard deviation
    daily_std_dev = returns[lbl_pr].std()
    # Calculate the annualised volatility
    annual_volatility = daily_std_dev * np.sqrt(t_days)
    # Calculate the skewness using the skew() function
    skewness = returns[lbl_pr].skew()
    # Calculate the excess kurtosis using the kurtosis() function
    excess_kurtosis = returns[lbl_pr].kurtosis()
    return annual_returns,average_daily_returns,daily_std_dev,annual_volatility,skewness,excess_kurtosis

##################################################
# Geometric Returns
##################################################      
def geometricRets(returns=[],t_days=252,lbl_pr='portfolio', ann=False,log_rets=False):
    
    trets=np.array(returns[lbl_pr].dropna())
    if not ann:
        period=1
    else:
        period=t_days
    if log_rets:
        grets=np.exp((period/len(trets))*(np.log(1+trets)).sum())-1
    else:
        grets=np.product(1+trets)**(period/len(trets))-1
    return grets
    
######################################################################
# Sharpe ratio
######################################################################      
def sharpeRatio(returns=[],rf=0,t_days=252,lbl_pr='portfolio'):
    # Calculate the sharpe ratio using the given formula
    # It is multiplied by square root of trading days to get the annualized value
    daily_risk_free_return = rf / t_days
    excess_daily_returns = returns[lbl_pr] - daily_risk_free_return
    sharpe_ratio = (excess_daily_returns.mean() / excess_daily_returns.std()) * np.sqrt(t_days)
    return sharpe_ratio

######################################################################
# Deflated Sharpe ratio: the probability of a fake for a strategy
# Inspired from the work of Marcos Lopéz dePrado and Gautier Marti
# https://gmarti.gitlab.io/qfin/2018/05/30/deflated-sharpe-ratio.html    
######################################################################      
def compute_deflated_sharpe_ratio(estimated_sharpe,skew,kurtosis,test_days=252,nb_trials=100):
    from scipy.stats import norm

    gamma = 0.5772156649015328606
    e = np.exp(1)
    sharpe_variance= 1/test_days
                         
    def approximate_expected_maximum_sharpe(mean_sharpe, var_sharpe, nb_trials):
        return mean_sharpe + np.sqrt(var_sharpe) * (
            (1 - gamma) * norm.ppf(1 - 1 / nb_trials) + gamma * norm.ppf(1 - 1 / (nb_trials * e)))
    
    SR0 = approximate_expected_maximum_sharpe(0, sharpe_variance, nb_trials)
    
    p= norm.cdf(((estimated_sharpe - SR0) * np.sqrt(backtest_horizon - 1)) 
                    / np.sqrt(1 - skew * estimated_sharpe + ((kurtosis - 1) / 4) * estimated_sharpe**2))
    return 1-p
    
##################################################
# Sortino ratio
##################################################      
def sortinoRatio(returns=[],rf=0,t_days=252,lbl_pr='portfolio'):
    # It is multiplied by the square root of trading days to get the annualized value
    average_daily_returns = returns[lbl_pr].mean()
    daily_risk_free_return = rf / t_days
    excess_daily_returns = returns[lbl_pr] - daily_risk_free_return
    net_returns = returns[lbl_pr] - average_daily_returns
    negative_returns = net_returns[net_returns < 0]
    semi_dev = np.sqrt(np.sum((negative_returns ** 2)) / len(returns))
    sortino_ratio = (excess_daily_returns.mean() / semi_dev) * np.sqrt(t_days)
    return sortino_ratio

##################################################
# Treynor ratio
##################################################      
def treynorRatio(returns=[],rf=0,t_days=252,lbl_pr='portfolio',lbl_mk='market'):
    daily_risk_free_return = rf / t_days
    excess_daily_returns = returns[lbl_pr] - daily_risk_free_return
    # Calculate the covariance between market returns and portfolio returns
    covariance_matrix = returns[[lbl_mk, lbl_pr]].cov()
    covariance_value = covariance_matrix.iloc[0][lbl_pr]
    # Calculate the market variance
    market_variance = returns[lbl_mk].var()
    # Calculate the beta value using the given formula
    beta = covariance_value / market_variance
    # Calculate the Treynor ratio using the above formula
    # It is multiplied by trading days to get the annualized value
    treynor_ratio = (excess_daily_returns.mean() * t_days) / beta
    return treynor_ratio, beta

##################################################
# Information ratio
##################################################      
def informationRatio(ret1=[],ret2=[],t_days=252,lbl_pr='portfolio',lbl_mk='market'):
    average_daily_returns = ret1[lbl_pr].mean()
    information_ratio = ((average_daily_returns - ret2[lbl_mk].mean()) / (
            ret1[lbl_pr] - ret2[lbl_mk]).std()) * np.sqrt(t_days)
    return information_ratio
    
##################################################
# Cumlative product
##################################################      
def cumProdRt(ret1=[],ret2=[],lbl_pr='portfolio',lbl_mk='market',log_rets=False, as_pct=False):
    # Cumulative product of portfolio returns
    if log_rets:
        cumprod_ret = np.exp(ret1[lbl_pr].cumsum())-1
        cumprod_market_ret = np.exp(ret2[lbl_mk].cumsum())-1
    else:
        cumprod_ret = (1+ret1[lbl_pr]).cumprod()-1
        cumprod_market_ret = (1+ret2[lbl_mk]).cumprod()-1
    if as_pct:
        cumprod_ret = cumprod_ret*100
        cumprod_market_ret = cumprod_market_ret*100
    return cumprod_ret, cumprod_market_ret

##################################################
# Max Drawdown
##################################################      
def maxDrawdown(returns=[],lbl_pr='portfolio',log_rets=False):
    # Calculate the maximum drawdown using the given formula
    if log_rets:
        cumprod_ret = np.exp(returns[lbl_pr].cumsum())
    else:
        cumprod_ret = (returns[lbl_pr] + 1).cumprod()
    cumprod_ret.index = pd.to_datetime(cumprod_ret.index)
    trough_index = (np.maximum.accumulate(cumprod_ret) - cumprod_ret).idxmax()
    peak_index = cumprod_ret.loc[:trough_index].idxmax()
    maximum_drawdown = (cumprod_ret[trough_index] - cumprod_ret[peak_index]) / cumprod_ret[peak_index]
    return maximum_drawdown,peak_index,trough_index

def max_dd_with_dates(returns):
    r = returns.add(1).cumprod()
    dd = r.div(r.cummax()).sub(1)
    mdd = dd.min()
    end = dd.idxmin()
    start = r.loc[:end].idxmax()
    return mdd, start, end
    
def drawdownHistory(return_series: pd.Series,wealth=1000,log_rets=False):
    if log_rets:
        wealth_index = wealth*(np.exp(returns[lbl_pr].cumsum()))
    else:
        wealth_index = wealth*((1+return_series).cumprod())
    previous_peaks = wealth_index.cummax()
    drawdowns = (wealth_index - previous_peaks)/previous_peaks
    return pd.DataFrame({"Wealth": wealth_index, 
                         "Previous Peak": previous_peaks, 
                         "Drawdown": drawdowns})
 
##################################################
# Omega ratio
##################################################      
def omegaRatio(returns=[],daily_thres=0.012,t_days=252,lbl_pr='portfolio'):
    annualized_threshold=((daily_thres + 1) ** np.sqrt(1/t_days) - 1)
    excess_returns = returns[lbl_pr] - annualized_threshold
    mask1 = excess_returns.values >0
    mask2 = excess_returns.values <0
    pos_rets=pd.Series(excess_returns.values[mask1], excess_returns.index[mask1])
    neg_rets=pd.Series(excess_returns.values[mask2], excess_returns.index[mask2])
    omega= pos_rets.sum()/(-neg_rets.sum())
    return omega

##################################################
# Rolling ratios
##################################################      
def roll_sharpe(returns,days,rf,lbl_pr):
    def sharpeRatio(ret,rf):
        daily_risk_free_return = rf / 252
        excess_daily_returns = ret - daily_risk_free_return
        sharpe_ratio = (excess_daily_returns.mean() / excess_daily_returns.std()) * np.sqrt(252)
        return sharpe_ratio
    mfun = lambda x: sharpeRatio(x,rf)
    return returns[lbl_pr].rolling(window=days).agg(mfun)

def roll_sortino(returns,days,rf,lbl_pr):
    def sortinoRatio(ret,rf):    
        average_daily_returns = ret.mean()
        daily_risk_free_return = rf / 252
        excess_daily_returns = ret - daily_risk_free_return
        net_returns = ret - average_daily_returns
        negative_returns = net_returns[net_returns < 0]
        semi_dev = np.sqrt(np.sum((negative_returns ** 2)) / len(ret))
        sortino_ratio = (excess_daily_returns.mean() / semi_dev) * np.sqrt(252)
        return sortino_ratio
    mfun = lambda x: sortinoRatio(x,rf)
    return returns[lbl_pr].rolling(window=days).agg(mfun)

##############################################
# Value at Risk
##############################################      
def normVaR(returns=[], ConfLev=0.95,lbl_pr='portfolio'):
    from scipy.stats import norm
    
    df, mean, sigma = norm.fit(returns[lbl_pr])
    var=norm.ppf(1-ConfLev,mean,sigma)
    tail_loss = t.expect( lambda y: y, args = (df,), loc = mean, scale = sigma, lb = var )
    cvar = (1 / (1 - ConfLev)) * tail_loss
    return var,cvar

def histNVaR(returns=[], ConfLev=0.95,lbl_pr='portfolio'):
    df=returns.copy()
    df.sort_values(lbl_pr,inplace=True, ascending=True)
    return df[lbl_pr].quantile(1-ConfLev)

def tVaR(returns=[], ConfLev=0.95,lbl_pr='portfolio'):
    from scipy.stats import t
    
    df, mean, sigma = t.fit(returns[lbl_pr])
    var= t.ppf(1-ConfLev,mean,sigma)
    tail_loss = t.expect( lambda y: y, args = (df,), loc = mean, scale = sigma, lb = var )
    cvar = (1 / (1 - ConfLev)) * tail_loss
    return var, cvar
    
def CovMatrixVar(returns, weights, ConfLev, n_sec=3,VC=[]):
    #Statistical approach using VC matrix
    #Might be filtered and supplied or computed within
    from scipy.stats import norm
    
    port_mu = returns.dot(weights).mean()
    std = np.std(returns, ddof=1)

    V = np.diag(std)
    C = returns.corr(method='spearman') #method='spearman'
    if VC == []:
        VC = V.dot(C)
    VCV = VC.dot(V)
    W = pd.DataFrame(np.array(weights).reshape(1, n_sec))
    WVCV = W.dot(VCV)
    Wt = np.transpose(W)
    
    port_sigma = (WVCV.dot(Wt)) ** 0.5
    VaR95 = -(-port_mu + norm.ppf(ConfLev) * port_sigma)
    Phi = (1 / ((2 * math.pi) ** 0.5)) * np.exp(-0.5 * (norm.ppf(ConfLev)) ** 2)
    ES = -(-port_mu + port_sigma * Phi / (1 - ConfLev))

    return VaR95, ES
    
    
#def norm_VaR(port_mu=0, port_sigma=1, ConfLev=0.95, horizon=5):
#    from scipy.stats import norm
#    
#    port_sigma = port_sigma * np.sqrt(horizon/252) #the volatility over the days horizon
#    VaR95 = -(-port_mu + norm.ppf(ConfLev) * port_sigma)
#    Phi = (1 / ((2 * math.pi) ** 0.5)) * np.exp(-0.5 * (norm.ppf(ConfLev)) ** 2)
#    ES = -(-port_mu + port_sigma * Phi / (1 - ConfLev))
#    return 100*VaR95, 100*ES

#def t_VaR(returns=[], ConfLev=0.95,lbl_pr='portfolio'):
#    import scipy
#    
#    tdf, tmean, tsigma = scipy.stats.t.fit(returns[lbl_pr])
#    support = np.linspace(returns[lbl_pr].min(), returns[lbl_pr].max(), 100)
#    return 100*returns[lbl_pr].quantile(1 - ConfLev),support,tdf, tmean, tsigma

 