#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Options Pricing Models 
#
# Module comprising plot and helper functions
#############################################################################################      

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path+"/")

#Common Libraries
import math as m
from scipy.stats import norm
import numpy as np
import pandas as pd
import py_vollib.black_scholes_merton as bsm
import py_vollib.black_scholes as bs
import py_vollib.black_scholes.implied_volatility as bsiv
import py_vollib.black_scholes_merton.implied_volatility as bsmiv
import py_vollib.black_scholes_merton.greeks.analytical as ga
import py_vollib.black_scholes_merton.greeks.numerical as gn 
import statistics as st

pd.options.mode.chained_assignment = None  # default='warn'

# Overide analytical solutions with 2 or 3
#S->price,K -> strike,T,s,r -> interest rate,t,dv -> dividend,  t=0.25 and T=1
# t -> t = 0
CONFIG_BS = 2
CONFIG_GKS = 2


##############################################
# LogNormal Fitting for Probability Calcs
##############################################      
def getLogNormProb(prices, option, prb,around=0.10,hbins=50,xvals=500):
    from scipy.stats import norm
    from scipy.stats import lognorm
    import scipy
    
    hist, bins = np.histogram(prices.Adjusted_close, bins=hbins)
    #hist, bins = scipy.histogram(prices.Adjusted_close, bins=hbins)
    shape, loc, scale = lognorm.fit(prices.Adjusted_close, floc=0) #x0 is rawdata x-axis
    #x = np.linspace(bins.min(), bins.max(), num=xvals) # values for x-axis
    #y = lognorm.pdf(x, shape, loc=0, scale=scale) # probability distribution
      
    maxPO=option['payoff'].max()
    moption=option[(option['payoff']>=(prb*maxPO*(1-around))) & (option['payoff']<=(prb*maxPO*(1+around)))]
    p50_M=moption['und_price'].max()
    p50_m=moption['und_price'].min()
    pop50=100*abs(lognorm.cdf(p50_m, shape, 0, scale)-lognorm.cdf(p50_M, shape, 0, scale))

    moption=option[(option['payoff']>=(maxPO*(1-around))) & (option['payoff']<=(maxPO*(1+around)))]
    pMax_M=moption['und_price'].max()
    pMax_m=moption['und_price'].min()
    popMax=100*abs(lognorm.cdf(pMax_m, shape, 0, scale)-lognorm.cdf(pMax_M, shape, 0, scale))
        
    maxS,minS=round(option[option['payoff']>=0]['und_price'].max(),4),round(option[option['payoff']>=0]['und_price'].min(),4)
    pop=100*abs(lognorm.cdf(minS, shape, 0, scale)-lognorm.cdf(maxS, shape, 0, scale))
    RR=1/(round(abs(option['payoff'].min())/option['payoff'].max(),2))
    
    maxprof=option['payoff'].max()
    minprof=option['payoff'].min()
    
    ret={}
    ret['shape'],ret['loc'],ret['scale']=shape, loc, scale
    ret['pop'],ret['RR'],ret['maxprof'],ret['minprof']=pop,RR, maxprof,minprof
    ret['maxPO'],ret['pop50'],ret['popMax']=maxPO,pop50, popMax
    ret['p50_M'],ret['p50_m']= p50_M,p50_m
    
    return ret

##############################################
# Montecarlo Simulation 
##############################################      
def mcSimul(S, T, r, q, sigma, steps=100, N=200000, plot=False):
    #S = Current stock Price
    #K = Strike Price
    #T = Time to maturity 1 year = 1, 1 months = 1/12
    #r = risk free interest rate
    #q = dividend yield
    # sigma = volatility 
    # output  [steps,N] Matrix of asset paths 
    dt = T/steps
    ST = np.log(S) +  np.cumsum(((r - q - sigma**2/2)*dt +\
                              sigma*np.sqrt(dt) * \
                              np.random.normal(size=(steps,N))),axis=0)
    paths= np.exp(ST)
    payoffs = np.maximum(paths[-1]-K, 0)
    option_price = np.mean(payoffs)*np.exp(-r*T) #discounting back to present value

    if plot:
        plt.plot(paths);
        plt.xlabel("Time Increments")
        plt.ylabel("Stock Price")
        plt.title("Geometric Brownian Motion")

    return option_price

##############################################
# BlackScholes
##############################################      
def D1(S,K,T,s,r,t,dv=0):
    return (m.log(S/K)+(r-dv + 0.5*(s**2))*(T-t)) / s*((T-t)**0.5)

def D2(S,K,T,s,r,t,dv=0):
    return D1(S,K,T,s,r,t,dv)-s*((T-t)**0.5)
        
def BS(S,K,T,s,r,dv=0,side='C'):
    if CONFIG_BS == 1:
        ret = BS_t(S,K,T,s,r,0,dv,side)
    elif CONFIG_BS == 2:
        ret = bsm.black_scholes_merton(side.lower(), S, K, T, r, s, dv) 
    elif CONFIG_BS == 3:
        ret = bs.black_scholes(side.lower(), S, K, T, r, s) 
    return ret

def BS_t(S,K,T,s,r,t,dv=0,side='C'):
    if CONFIG_BS == 1:
        N = norm.cdf
        d1 = D1(S,K,T,s,r,t,dv=0)
        d2 = D2(S,K,T,s,r,t,dv=0)
        price=0
        if side == 'C':
            price=S * np.exp(-dv*(T-t)) * N(d1) - np.exp(-r*(T-t)) * K * N(d2)
        else:
            price=K * np.exp(-r*(T-t)) * N(-d2) - S * np.exp(-dv*(T-t)) * N(-d1)
    elif CONFIG_BS == 2:
        price = bsm.black_scholes_merton(side.lower(), S, K, T, r, s, dv) 
    elif CONFIG_BS == 3:
        price = bs.black_scholes(side.lower(), S, K, T, r, s)
    return price
    
def BS_curr(S,K,T,s,r,t,rfree=0,side='C'):
    return BS_t(S,K,T,s,r,t,rfree,side)

def BS_forward(S,K,T,s,r,t,Tf,side='C'):
    N = norm.cdf
    def D1_fut(K,T,s,t,fwd_p):
        return (m.log(fwd_p/K)+(0.5*(s**2))*(T-t)) / s*((T-t)**0.5)
    def D2_fut(K,T,s,t,fwd_p):
        return D1_fut(K,T,s,t,fwd_p)-s*((T-t)**0.5)
    
    fwd_p = S * np.exp(r*(Tf-t))
    price=0
    if side == 'C':
        price = np.exp(-r*(T-t)) * (fwd_p * N(D1_fut(K,T,s,t,fwd_p)) - N(D2_fut(K,T,s,t,fwd_p)))
    else:
        price = np.exp(-r*(T-t)) * (K * N(-D2_fut(K,T,s,t,fwd_p)) - fwd_p * N(-D1_fut(K,T,s,t,fwd_p)))
    return price
    
def BS_future(fwd_p,K,T,s,r,t,side='C'):
    N = norm.cdf
    def D1_fut(K,T,s,t,fwd_p):
        return (m.log(fwd_p/K)+(0.5*(s**2))*(T-t)) / s*((T-t)**0.5)
    def D2_fut(K,T,s,t,fwd_p):
        return D1_fut(K,T,s,t,fwd_p)-s*((T-t)**0.5)
        
    price=0
    if side == 'C':
        price = np.exp(-r*(T-t)) * (fwd_p * N(D1_fut(K,T,s,t,fwd_p)) - K * N(D2_fut(K,T,s,t,fwd_p)))
    else:
        price = np.exp(-r*(T-t)) * (K * N(-D2_fut(K,T,s,t,fwd_p)) - fwd_p * N(-D1_fut(K,T,s,t,fwd_p)))
    return price

def BS_greeks(theta, delta, gamma, S_,s_,r_):
    BS_pde = (theta + r_ * S_ * delta + 0.5*(s_**2)*(S_**2)*gamma)
    return BS_pde / r_

##############################################
# Payoff
##############################################      
def BS_payoff(S,K,T,s,r,t,dv=0,side='C',LongShort=1,prange=[0.30,0.30],step=0.25):
    sT = np.arange((1-prange[0])*S,(1+prange[1])*S,step) 
    return [LongShort*BS_t(i,K,T,s,r,t,dv,side)*100 for i in sT]

def beta(S,K,T,s,r,dv=0,side='C', beta_scty=1):
    return (S/BS(S,K,T,s,r,dv,side))*delta(S,K,T,s,r,dv,side)*beta_scty 
    
##############################################
#  GREEKS
##############################################      
def delta(S,K,T,s,r,dv=0,side='C'):
    N = norm.cdf
    delta_ = 0
    if CONFIG_GKS == 1:
        if side == 'C':
            delta_ = np.exp(-dv*(T)) * N( D1(S,K,T,s,r,0,dv) ) 
        else:
            delta_ = -np.exp(-dv*(T)) * N(-D1(S,K,T,s,r,0,dv))
    elif CONFIG_GKS == 2:
        delta_ = ga.delta(side.lower(),S,K,T,r,s,dv)
    elif CONFIG_GKS == 3:
        delta_ = gn.delta(side.lower(),S,K,T,r,s,dv)    
    return delta_
        
def gamma(S,K,T,s,r,dv=0):
    side='C'
    gamma_=0
    if CONFIG_GKS == 1:
        N_phi = norm.pdf
        gamma_ = np.exp(-dv*(T))*(N_phi(D1(S,K,T,s,r,0,dv))/(S*s*np.sqrt(T)))
    elif CONFIG_GKS == 2:
        gamma_ = ga.gamma(side.lower(),S,K,T,r,s,dv)
    elif CONFIG_GKS == 3:
        gamma_ = gn.gamma(side.lower(),S,K,T,r,s,dv)
    return gamma_
    
def vega(S,K,T,s,r,dv=0):
    side='C'
    vega_=0
    if CONFIG_GKS == 1:
        N_phi = norm.pdf
        vega_ = np.exp(-dv*(T))*S*np.sqrt(T)*N_phi(D1(S,K,T,s,r,0,dv)) 
    elif CONFIG_GKS == 2:
        vega_ = ga.vega(side.lower(),S,K,T,r,s,dv)
    elif CONFIG_GKS == 3:
        vega_ = gn.vega(side.lower(),S,K,T,r,s,dv)
    return vega_
    
def theta(S,K,T,s,r,dv=0,side='C'):
    theta_ = 0
    if CONFIG_GKS ==1:
        N_phi = norm.pdf
        N = norm.cdf
        p1 = - S*N_phi(D1(S,K,T,s,r,0,dv))*s / (2 * np.sqrt(T))
        if side == 'C':
            p2 = -r*K*np.exp(-r*T)*N(D2(S,K,T,s,r,0,dv)) + dv*S*np.exp(-dv*(T))*N(D1(S,K,T,s,r,0,dv)) 
            theta_ = p1 + p2
        else:
            p2 = +r*K*np.exp(-r*T)*N(-D2(S,K,T,s,r,0,dv)) - dv*S*np.exp(-dv*(T))*N(-D1(S,K,T,s,r,0,dv)) 
            theta_ = p1 + p2
    elif CONFIG_GKS == 2:
        theta_ = ga.theta(side.lower(),S,K,T,r,s,dv)
    elif CONFIG_GKS == 3:
        theta_ = gn.theta(side.lower(),S,K,T,r,s,dv)
    return theta_
    
def rho(S,K,T,s,r,dv=0,side='C'):
    
    rho_=0
    if CONFIG_GKS == 1:    
        N = norm.cdf
        if side =='C':
            rho_=K*T*np.exp(-r*T)*N(D2(S,K,T,s,r,0,dv))
        else:
            rho_=-K*T*np.exp(-r*T)*N(-D2(S,K,T,s,r,0,dv))
    elif CONFIG_GKS == 2:
        rho_ = ga.rho(side.lower(),S,K,T,r,s,dv)
    elif CONFIG_GKS == 3:
        rho_ = gn.rho(side.lower(),S,K,T,r,s,dv)
    return rho_

def psi(S,K,T,s,r,dv=0,side='C'):
    N = norm.cdf
    if side =='C':
        res=-S*T*np.exp(-dv*(T))*N(D1(S,K,T,s,r,0,dv))
    else:
        res=S*T*np.exp(-dv*(T))*N(-D1(S,K,T,s,r,0,dv))
    return res

def elasticity(S,K,T,s,r,dv=0,side='C'):
    return S*delta(S,K,T,s,r,dv,side)/BS(S,K,T,s,r,dv,side)
    
##############################################
# Hedging
##############################################      
def delta_hedge(S,K,T,s,r,t,dv=0,side='C'):
    N=norm.cdf
    shares=N(D1(S,K,T,s,r,t,dv=0))
    option=BS_t(S,K,T,s,r,t,dv,side)
    cash=option-shares*S
    invest=shares*S
    return shares,invest,cash

def rebalance(S,K,T,s,r,t,dv,side,shares,cash):
    nshares,ninvest,ncash=delta_hedge(S,K,T,s,r,t,dv,side)
    print('New hedge requires to set',np.round(nshares,4),'shares')
    print('Buy/Sell ',np.round(nshares -shares,4),'shares and repay',np.round(S*(nshares-shares),4), 'cash')
    return np.round(nshares -shares,4),np.round(S*(nshares-shares),4)

def makeDHTable(S,K,T,s,r,t,dv=0,side='C',prange=8):
    from Plib.Utils.Tools import highlight_row_
    
    shares,invest,cash=delta_hedge(S,K,T,s,r,t,dv,side)
    RHEtable = pd.DataFrame()
    print('Hedging requires ',np.round(shares,4),'shares and ',np.round(cash,4), 'cash')
    print('Negative cash implies financing',np.round(invest,4),'with',abs(np.round(cash,4)))
    for i in range(-int(prange),int(prange)+1):
        new_price=S+i
        A=shares*new_price+cash
        L=BS_t(new_price,K,T,s,r,t,dv,side)
        RHE=L/A-1
        d={'price':new_price,'Asset':A,'Liability':L,'HRE':RHE}
        RHEtable=RHEtable.append(d, ignore_index=True)
    RHEtable=RHEtable[['price','Asset','Liability','HRE']]    
    RHEtableRHEtable.round(4)
    highlight_row=lambda x: highlight_row_(x, price=S,clr='yellow')
    return RHEtable.style.apply(highlight_row, axis=1)
    
def deltagamma_hedge(p):
    #p={'S1' : 43,'K1' : 45,'T1' : 1,'s1' : 0.25,'r1' : 0.03,'t1' : 0,'dv1': 0,'side1' : 'C',
    #  'S2' : 43,'K2' : 45,'T2' : 0.25,'s2' : 0.25,'r2' : 0.03,'t2' : 0,'dv2': 0,'side2' : 'P'}
    
    delta_c=delta(p['S1'],p['K1'],p['T1'],p['s1'],p['r1'],p['dv1'],p['side1'])
    gamma_c=gamma(p['S1'],p['K1'],p['T1'],p['s1'],p['r1'],p['dv1'])
    call_v=BS(p['S1'],p['K1'],p['T1'],p['s1'],p['r1'],p['dv1'],p['side1'])
    
    delta_p=delta(p['S2'],p['K2'],p['T2'],p['s2'],p['r2'],p['dv2'],p['side2'])
    gamma_p=gamma(p['S2'],p['K2'],p['T2'],p['s2'],p['r2'],p['dv2'])
    put_v=BS(p['S2'],p['K2'],p['T2'],p['s2'],p['r2'],p['dv2'],p['side2'])
    
    dp=gamma_c/gamma_p
    ds=delta_c-(dp*delta_p)
    db=call_v-ds*p['S1']-dp*put_v
    
    return dp,ds,db

def makeDGHTable(p,prange=8):
    from Plib.Utils.Tools import highlight_row_
    
    dp,ds,db=deltagamma_hedge(p)
    print('Hedging requires ',np.round(ds,4),'units of underlying')
    print('Hedging requires ',np.round(dp,4),'units of another derivative and ',np.round(db,4), 'cash')
    
    RHEtable = pd.DataFrame()
    for i in range(-int(prange),int(prange)+1):
        new_price=p['S1']+i
        A=db+ds*new_price+dp*BS(new_price,p['K2'],p['T2'],p['s2'],p['r2'],p['dv2'],p['side2'])
        L=BS(new_price,p['K1'],p['T1'],p['s1'],p['r1'],p['dv1'],p['side1'])
        RHE=L/A-1
        d={'price':new_price,'Asset':A,'Liability':L,'HRE':RHE}
        RHEtable=RHEtable.append(d, ignore_index=True)
    RHEtable=RHEtable[['price','Asset','Liability','HRE']]   
    RHEtable.round(4)
    highlight_row=lambda x: highlight_row_(x, price=p['S1'],clr='yellow')
    return RHEtable.style.apply(highlight_row, axis=1)

def hedgingBands(S,K,T,s,r,t,dv,side,lbd=0.01,g=0.0003):
    #t=0
    gamma_h = gamma(S,K,T,s,r,dv)
    delta_h = delta(S,K,T,s,r,dv,side)
    w = (3/2 * (np.exp(-r*(T-t)) * (lbd*S*gamma_h**2)) / g)**(1/3)
    return delta_h - w, delta_h + w

def hedgingPnL(S,K,T,s,r,t,dv,N,PnL): 
    # PnL after N hedges, interval of one std
    vega_h=vega(S,K,T,s,r,dv)
    pnl_std= (np.sqrt(np.pi/4)*vega_h*s)/np.sqrt(N)
    return PnL-pnl_std,PnL+pnl_std

##############################################
# Volatility
##############################################      
def BS_iv(opt_value, S, K, T, r, dv, type_='C'):
    from scipy.optimize import minimize_scalar  
    
    def call_obj(s):
        return abs(BS(S,K,T,s,r,dv,'C') - opt_value)
    
    def put_obj(s):
        return abs(BS(S,K,T,s,r,dv,'P') - opt_value)
    
    if CONFIG_BS == 1:
        if type_ == 'C':
            res = minimize_scalar(call_obj, bounds=(0.01,6), method='bounded')
            ret = res.x
        else:
            res = minimize_scalar(put_obj, bounds=(0.01,6), method='bounded')
            ret = res.x
    elif CONFIG_BS == 2:
        ret = bsmiv.implied_volatility(opt_value, S, K, T, r, dv,type_.lower())
    elif CONFIG_BS == 3:
        ret = bsiv.implied_volatility(opt_value, S, K, T, r,type_.lower())
    return ret

def strikeVol(dlt,S,K,T,s,r,dv=0,side='C',future=False):
    from scipy.stats import norm
    
    b = r - dv
    if side=='C':
        strike_ = S * np.exp( -norm.ppf(dlt * np.exp(T*(r-b))) * s*np.sqrt(T) + (b + 0.5*s**2)*T )
    else:
        strike_ = S * np.exp( norm.ppf(-dlt * np.exp(T*(r-b))) * s*np.sqrt(T) + (b + 0.5*s**2)*T )
    if future:
        print('Use the future price to obtain the delta')
    return strike_        

def ivElast(S,K,T,s,r,dv=0,side='C'):
    return s*elasticity(S,K,T,s,r,dv,side)
    
##############################################
# Greeks Plots
##############################################      
def plotDelta(S, K, r, sigma, dv=0):
    import matplotlib.pyplot as plt
    
    Ts = [1,0.75,0.5,0.25,0.1,0.05]
    for T in Ts:
        plt.plot(range(1,S*2), [delta(i,K,T,s,r,dv,side='C') for i in range(1,S*2)], label='Delta Call'+f'T = {T}')
        plt.plot(range(1,S*2), [delta(i,K,T,s,r,dv,side='P') for i in range(1,S*2)], label='Delta Put'+f'T = {T}')
    plt.xlabel('$S_0$')
    plt.ylabel('Delta')
    plt.title('Stock Price Effect on Delta for Calls/Puts' )
    plt.axvline(K, color='black', linestyle='dashed', linewidth=2,label="Strike")
    plt.legend()

def plotGamma(S, K, r, sigma, dv=0):
    import matplotlib.pyplot as plt
    
    Ts = [1,0.75,0.5,0.25,0.1,0.05]
    for T in Ts:
        plt.plot(range(1,S*2), [gamma(i,K,T,s,r,dv) for i in range(1,S*2)], label='Gamma'+f'T = {T}')

    plt.title('Gamma by changing $S_0$')
    plt.legend()
    plt.xlabel('$S_0$')
    plt.ylabel('Gamma')

def plotVega(S, K, r, sigma, dv=0):
    import matplotlib.pyplot as plt
    
    Ts = [1,0.75,0.5,0.25,0.1,0.05]
    for T in Ts:
        #plt.plot(vega(prices,K,t,sigma,r,dv), label=f'T = {t}')
        plt.plot(range(1,S*2), [vega(i,K,T,s,r,dv) for i in range(1,S*2)], label='Vega'+f'T = {T}')

    plt.legend()
    plt.xlabel('$S_0$')
    plt.ylabel('Vega')
    plt.title('Vega Decrease with Time')

def plotTheta(S, K, r, sigma, dv=0):
    import matplotlib.pyplot as plt
    
    Ts=[1,0.75,0.5,0.25,0.1,0.05]
    for T in Ts:
        plt.plot(range(1,S*2), [theta(i,K,T,s,r,dv,side='C') for i in range(1,S*2)], label='Theta Call'+f'T = {T}')
        plt.plot(range(1,S*2), [theta(i,K,T,s,r,dv,side='P') for i in range(1,S*2)], label='Theta Put'+f'T = {T}')
        
    plt.legend()
    plt.title('Theta of a call')
    plt.xlabel('$S_0$')
    plt.ylabel('Theta')

def plotGreeks(S=43, K=45, r=0.03, s=0.25, dv=0, side='C', title='', fs=(12,10)):
    import matplotlib.pyplot as plt
    
    def plotMoneyness(axes,subtitle,und=0):
        def swapPositions(t):
            return t[1],t[0]
        
        axes.axvline(K, color='gray', linestyle='-', linewidth=0.5,label="Strike")
        if und !=0 : axes.axvline(und, color='gray', linestyle='-.', linewidth=0.5,label="Underlying")
        x =  [K,S*2]
        y = axes.get_ylim() # min,max
        if side=='P': x=[1,K];y=swapPositions(y)
        axes.fill_between(x, y[0], y[1],
                     facecolor="orange", # The fill color
                     alpha=0.03)          # Transparency of the fill
        axes.title.set_text(subtitle)
        axes.legend()
    
    Ts=[1,0.75,0.5,0.25,0.1,0.05]
    fig, axes = plt.subplots(ncols=2, nrows=2, constrained_layout=True,figsize=fs)
    fig.suptitle(title, fontsize=16)
    
    axes[0,0].plot([], [], ' ', label="T in years")
    axes[0,1].plot([], [], ' ', label="T in years")
    axes[1,0].plot([], [], ' ', label="T in years")
    axes[1,1].plot([], [], ' ', label="T in years")
    
    for T in Ts:
        delta_ = [delta(i,K,T,s,r,dv,side) for i in range(1,S*2)]
        gamma_ = [gamma(i,K,T,s,r,dv) for i in range(1,S*2)]
        vega_ = [vega(i,K,T,s,r,dv) for i in range(1,S*2)]
        theta_ = [theta(i,K,T,s,r,dv,side) for i in range(1,S*2)]
        axes[0,0].plot(delta_, label=f'T = {T}')
        axes[0,1].plot(gamma_, label=f'T = {T}')
        axes[1,0].plot(vega_, label=f'T = {T}')
        axes[1,1].plot(theta_, label=f'T = {T}')
    
    plotMoneyness(axes[0,0], 'Delta', S)
    plotMoneyness(axes[0,1], 'Gamma', S)
    plotMoneyness(axes[1,0], 'Vega', S)
    plotMoneyness(axes[1,1], 'Theta (time decay)', S)

def plotGreeks2(S, opts_list, title='', fs=(12,10)):
    import matplotlib.pyplot as plt
    import pylab as plot
    params = {'legend.fontsize': 8,
              'legend.handlelength': 2}
    plot.rcParams.update(params)
    
    lbl_fs=12
    
    def delta2(S, T, K, r, s, dv, side):
        return delta(S,K,T,s,r,dv,side)
    def gamma2(S, T, K, r, s, dv, side):
        return gamma(S,K,T,s,r,dv)
    def vega2(S, T, K, r, s, dv, side):
        return vega(S,K,T,s,r,dv)
    def theta2(S, T, K, r, s, dv, side):
        return theta(S,K,T,s,r,dv,side)
    
    def plotMoneyness(axes,subtitle,und=0):
        def swapPositions(t):
            return t[1],t[0]
        
        for e in opts_list:
            K=e[0];fcol='orange';side=e[-1]
            axes.axvline(K, color='gray', linestyle='-', linewidth=0.5,label="Strike")
            if und !=0 : axes.axvline(und, color='gray', linestyle='-.', linewidth=0.5,label="Underlying")
            x =  [K,S*2]
            y = axes.get_ylim() # min,max
            if side=='P': x=[1,K];y=swapPositions(y);fcol='blue'
            axes.fill_between(x, y[0], y[1],
                         facecolor=fcol, # The fill color
                     alpha=0.03)          # Transparency of the fill
            #axes.title.set_text(subtitle)
            axes.set_title(subtitle , size=lbl_fs)
            axes.legend()
    
    Ts=[1,0.75,0.5,0.25,0.1,0.05]
    fig, axes = plt.subplots(ncols=2, nrows=2, constrained_layout=True,figsize=fs)
    fig.suptitle(title, fontsize=lbl_fs)
    
    axes[0,0].plot([], [], ' ', label="T in years")
    axes[0,1].plot([], [], ' ', label="T in years")
    axes[1,0].plot([], [], ' ', label="T in years")
    axes[1,0].set_xlabel('Strike', fontsize = lbl_fs)
    axes[1,1].plot([], [], ' ', label="T in years")
    axes[1,1].set_xlabel('Strike', fontsize = lbl_fs)
    axes[0,0].xaxis.set_tick_params(labelsize=lbl_fs)
    axes[0,0].yaxis.set_tick_params(labelsize=lbl_fs)
    axes[0,1].xaxis.set_tick_params(labelsize=lbl_fs)
    axes[0,1].yaxis.set_tick_params(labelsize=lbl_fs)
    axes[1,0].xaxis.set_tick_params(labelsize=lbl_fs)
    axes[1,0].yaxis.set_tick_params(labelsize=lbl_fs)
    axes[1,1].xaxis.set_tick_params(labelsize=lbl_fs)
    axes[1,1].yaxis.set_tick_params(labelsize=lbl_fs)
    
    for T in Ts:
        delta_ = [sum([delta2(*[i,T,*e]) for e in opts_list]) for i in range(1,S*2)]
        gamma_ = [sum([gamma2(*[i,T,*e]) for e in opts_list]) for i in range(1,S*2)]
        vega_ = [sum([vega2(*[i,T,*e]) for e in opts_list]) for i in range(1,S*2)]
        theta_ = [sum([theta2(*[i,T,*e]) for e in opts_list]) for i in range(1,S*2)]
        axes[0,0].plot(delta_, label=f'T = {T}')
        axes[0,1].plot(gamma_, label=f'T = {T}')
        axes[1,0].plot(vega_, label=f'T = {T}')
        axes[1,1].plot(theta_, label=f'T = {T}')

    plotMoneyness(axes[0,0], 'Delta', S)
    plotMoneyness(axes[0,1], 'Gamma', S)
    plotMoneyness(axes[1,0], 'Vega', S)
    plotMoneyness(axes[1,1], 'Theta (time decay)', S)
    
def plotGreeksPayoff(S=43, K=45, r=0.03, s=0.25, dv=0, side='C',LongShort=1,prange=[0.30,0.30],step=0.25,fs=(18,12)):
    import matplotlib.pyplot as plt

    T=2
    sT = np.arange((1-prange[0])*S,(1+prange[1])*S,step) 
    Ts = [0.25,0.5,0.75,1,1.5,2]

    delta_o = delta(S,K,T,s,r,dv,side)
    gamma_o = gamma(S,K,T,s,r,dv)
    theta_o = theta(S,K,T,s,r,dv,side)

    fig, axes = plt.subplots(3, 2,figsize=fs)

    #Impact of time
    Tpayoff = [LongShort*BS_greeks(theta(S,K,T1,s,r,dv,side), delta(S,K,T1,s,r,dv,side), gamma(S,K,T1,s,r,dv), S_=S,s_=s,r_=r) for T1 in Ts]
    Tpayoff_theta = [LongShort*BS_greeks(theta(S,K,T1,s,r,dv,side), delta_o, gamma_o, S_=S,s_=s,r_=r) for T1 in Ts]
    Tpayoff_thetagamma = [LongShort*BS_greeks(theta(S,K,T1,s,r,dv,side), delta_o, gamma(S,K,T1,s,r,dv), S_=S,s_=s,r_=r) for T1 in Ts]

    axes[0,0].plot(Ts,Tpayoff_theta,label='Theta only S='+str(S))
    axes[0,0].plot(Ts,Tpayoff_thetagamma,label='Theta and Gamma S='+str(S))
    axes[0,0].plot(Ts,Tpayoff,label='Payoff S='+str(S))
    #axes[0,0].set_xlabel('$T$')
    axes[0,0].set_ylabel('Payoff')
    axes[0,0].legend()
    axes[0,0].invert_xaxis()
    axes[0,0].set_title('Time decay Effect on Payoff as f(Greeks)' )
    #plt.axvline(K, color='black', linestyle='dashed', linewidth=2,label="Strike")

    Spayoff = [LongShort*BS_greeks(theta(S1,K,T,s,r,dv,side), delta(S1,K,T,s,r,dv,side), gamma(S1,K,T,s,r,dv), S_=S1,s_=s,r_=r) for S1 in sT]
    Spayoff_theta = [LongShort*BS_greeks(theta(S1,K,T,s,r,dv,side), delta_o, gamma_o, S_=S1,s_=s,r_=r) for S1 in sT]
    Spayoff_thetagamma = [LongShort*BS_greeks(theta(S1,K,T,s,r,dv,side), delta_o, gamma(S1,K,T,s,r,dv), S_=S,s_=s,r_=r) for S1 in sT]

    axes[0,1].plot(sT,Spayoff_theta,label='Theta only T='+str(T))
    axes[0,1].plot(sT,Spayoff_thetagamma,label='Theta and Gamma T='+str(T))
    axes[0,1].plot(sT,Spayoff,label='Payoff T='+str(T))
    axes[0,1].set_xlabel('$S$')
    axes[0,1].set_ylabel('Payoff')
    axes[0,1].legend()
    #axes[0,1].invert_xaxis()
    axes[0,1].set_title('Spot Effect on Payoff as f(Greeks)' )

    STs = [0.25,0.75,1.5]
    for t in STs:
        Spayoff = [LongShort*BS_greeks(theta(S1,K,t,s,r,dv,side), delta(S1,K,t,s,r,dv,side), gamma(S1,K,t,s,r,dv), S_=S1,s_=s,r_=r) for S1 in sT]
        Spayoff_theta = [LongShort*BS_greeks(theta(S1,K,t,s,r,dv,side), delta_o, gamma_o, S_=S1,s_=s,r_=r) for S1 in sT]
        Spayoff_thetagamma = [LongShort*BS_greeks(theta(S1,K,t,s,r,dv,side), delta_o, gamma(S1,K,t,s,r,dv), S_=S,s_=s,r_=r) for S1 in sT]

        #axes[1,1].plot(sT,Spayoff_theta,label='Theta only')
        axes[1,1].plot(sT,Spayoff_thetagamma,label='Theta and Gamma T='+str(t))
        axes[1,1].set_xlabel('$S$')
        axes[1,1].set_ylabel('Payoff')
        axes[1,1].legend()

        axes[2,1].plot(sT,Spayoff,label='Payoff T='+str(t))
        axes[2,1].set_xlabel('$S$')
        axes[2,1].set_ylabel('Payoff')
        axes[2,1].legend()

    STs = [0.25,0.75,1.5]
    for t in STs:
        Spayoff = [LongShort*BS_greeks(theta(S1,K,t,s,r,dv,side), delta(S1,K,t,s,r,dv,side), gamma(S1,K,t,s,r,dv), S_=S1,s_=s,r_=r) for S1 in sT]
        Spayoff_delta = [LongShort*BS_greeks(theta_o, delta(S1,K,t,s,r,dv,side), gamma_o, S_=S1,s_=s,r_=r) for S1 in sT]
        Spayoff_deltagamma = [LongShort*BS_greeks(theta_o, delta(S1,K,t,s,r,dv,side), gamma(S1,K,t,s,r,dv), S_=S,s_=s,r_=r) for S1 in sT]

        #axes[1,1].plot(sT,Spayoff_theta,label='Theta only')
        axes[1,0].plot(sT,Spayoff_delta,label='Delta T='+str(t))
        axes[1,0].set_xlabel('$S$')
        axes[1,0].set_ylabel('Payoff')
        axes[1,0].legend()
        axes[1,0].set_title('Spot Effect on Payoff as f(Greeks)' )

        axes[2,0].plot(sT,Spayoff_deltagamma,label='Delta and Gamma T='+str(t))
        axes[2,0].set_xlabel('$S$')
        axes[2,0].set_ylabel('Payoff')
        axes[2,0].legend()

def plotGreeksPayoff2(S, opts_list, LongShort=1, r=0.03, prange=[0.30,0.30],step=0.25,fs=(14,12)):
    import matplotlib.pyplot as plt
    import pylab as plot
    params = {'legend.fontsize': 8,
              'legend.handlelength': 2}
    plot.rcParams.update(params)
    
    lbl_fs=12
    
    def delta2(S, T, K, r, s, dv, side):
        return delta(S,K,T,s,r,dv,side)
    def gamma2(S, T, K, r, s, dv, side):
        return gamma(S,K,T,s,r,dv)
    def vega2(S, T, K, r, s, dv, side):
        return vega(S,K,T,s,r,dv)
    def theta2(S, T, K, r, s, dv, side):
        return theta(S,K,T,s,r,dv,side)
    
    T=2
    sT = np.arange((1-prange[0])*S,(1+prange[1])*S,step) 
    Ts = [0.25,0.5,0.75,1,1.5,2]

    delta_o = sum([delta2(*[S,T,*e]) for e in opts_list])
    gamma_o = sum([gamma2(*[S,T,*e]) for e in opts_list])
    theta_o = sum([theta2(*[S,T,*e]) for e in opts_list])
    s = sum([e[2] for e in opts_list])
    
    fig, axes = plt.subplots(3, 2,figsize=fs)

    #Impact of time
    Tpayoff = [LongShort*BS_greeks(sum([theta2(*[S,T1,*e]) for e in opts_list]), sum([delta2(*[S,T1,*e]) for e in opts_list]), sum([gamma2(*[S,T1,*e]) for e in opts_list]), S_=S,s_=s,r_=r) for T1 in Ts]
    Tpayoff_theta = [LongShort*BS_greeks(sum([theta2(*[S,T1,*e]) for e in opts_list]), delta_o, gamma_o, S_=S,s_=s,r_=r) for T1 in Ts]
    Tpayoff_thetagamma = [LongShort*BS_greeks(sum([theta2(*[S,T1,*e]) for e in opts_list]), delta_o, sum([gamma2(*[S,T1,*e]) for e in opts_list]), S_=S,s_=s,r_=r) for T1 in Ts]

    axes[0,0].plot(Ts,Tpayoff_theta,label='Theta only S='+str(S))
    axes[0,0].plot(Ts,Tpayoff_thetagamma,label='Theta and Gamma S='+str(S))
    axes[0,0].plot(Ts,Tpayoff,label='Payoff S='+str(S))
    #axes[0,0].set_xlabel('$T$')
    axes[0,0].set_ylabel('Payoff', fontsize = lbl_fs)
    axes[0,0].legend()
    axes[0,0].invert_xaxis()
    axes[0,0].set_title('Time decay Effect on Payoff as f(Greeks)' , size=lbl_fs)
    #plt.axvline(K, color='black', linestyle='dashed', linewidth=2,label="Strike")

    Spayoff = [LongShort*BS_greeks(sum([theta2(*[S1,T,*e]) for e in opts_list]), sum([delta2(*[S1,T,*e]) for e in opts_list]), sum([gamma2(*[S1,T,*e]) for e in opts_list]), S_=S1,s_=s,r_=r) for S1 in sT]
    Spayoff_theta = [LongShort*BS_greeks(sum([theta2(*[S1,T,*e]) for e in opts_list]), delta_o, gamma_o, S_=S1,s_=s,r_=r) for S1 in sT]
    Spayoff_thetagamma = [LongShort*BS_greeks(sum([theta2(*[S1,T,*e]) for e in opts_list]), delta_o, sum([gamma2(*[S1,T,*e]) for e in opts_list]), S_=S,s_=s,r_=r) for S1 in sT]

    axes[0,1].plot(sT,Spayoff_theta,label='Theta only T='+str(T))
    axes[0,1].plot(sT,Spayoff_thetagamma,label='Theta and Gamma T='+str(T))
    axes[0,1].plot(sT,Spayoff,label='Payoff T='+str(T))
    axes[0,1].set_xlabel('$S$', fontsize = lbl_fs)
    axes[0,1].set_ylabel('Payoff', fontsize = lbl_fs)
    axes[0,1].legend()
    #axes[0,1].invert_xaxis()
    axes[0,1].set_title('Spot Effect on Payoff as f(Greeks)' , size=lbl_fs)

    STs = [0.25,0.75,1.5]
    for t in STs:
        Spayoff = [LongShort*BS_greeks(sum([theta2(*[S1,t,*e]) for e in opts_list]), sum([delta2(*[S1,t,*e]) for e in opts_list]), sum([gamma2(*[S1,t,*e]) for e in opts_list]), S_=S1,s_=s,r_=r) for S1 in sT]
        Spayoff_theta = [LongShort*BS_greeks(sum([theta2(*[S1,t,*e]) for e in opts_list]), delta_o, gamma_o, S_=S1,s_=s,r_=r) for S1 in sT]
        Spayoff_thetagamma = [LongShort*BS_greeks(sum([theta2(*[S1,t,*e]) for e in opts_list]), delta_o, sum([gamma2(*[S1,t,*e]) for e in opts_list]), S_=S,s_=s,r_=r) for S1 in sT]

        #axes[1,1].plot(sT,Spayoff_theta,label='Theta only')
        axes[1,1].plot(sT,Spayoff_thetagamma,label='Theta and Gamma T='+str(t))
        axes[1,1].set_xlabel('$S$', fontsize = lbl_fs)
        axes[1,1].set_ylabel('Payoff', fontsize = lbl_fs)
        axes[1,1].legend()

        axes[2,1].plot(sT,Spayoff,label='Payoff T='+str(t))
        axes[2,1].set_xlabel('$S$', fontsize = lbl_fs)
        axes[2,1].set_ylabel('Payoff', fontsize = lbl_fs)
        axes[2,1].legend()

    STs = [0.25,0.75,1.5]
    for t in STs:
        Spayoff = [LongShort*BS_greeks(sum([theta2(*[S1,t,*e]) for e in opts_list]), sum([delta2(*[S1,t,*e]) for e in opts_list]), sum([gamma2(*[S1,t,*e]) for e in opts_list]), S_=S1,s_=s,r_=r) for S1 in sT]
        Spayoff_delta = [LongShort*BS_greeks(theta_o, sum([delta2(*[S1,t,*e]) for e in opts_list]), gamma_o, S_=S1,s_=s,r_=r) for S1 in sT]
        Spayoff_deltagamma = [LongShort*BS_greeks(theta_o, sum([delta2(*[S1,t,*e]) for e in opts_list]), sum([gamma2(*[S1,t,*e]) for e in opts_list]), S_=S,s_=s,r_=r) for S1 in sT]

        #axes[1,1].plot(sT,Spayoff_theta,label='Theta only')
        axes[1,0].plot(sT,Spayoff_delta,label='Delta T='+str(t))
        axes[1,0].set_xlabel('$S$', fontsize = lbl_fs)
        axes[1,0].set_ylabel('Payoff', fontsize = lbl_fs)
        axes[1,0].legend()
        axes[1,0].set_title('Spot Effect on Payoff as f(Greeks)' , size=lbl_fs)

        axes[2,0].plot(sT,Spayoff_deltagamma,label='Delta and Gamma T='+str(t))
        axes[2,0].set_xlabel('$S$', fontsize = lbl_fs)
        axes[2,0].set_ylabel('Payoff', fontsize = lbl_fs)
        axes[2,0].legend()
    
    axes[0,0].xaxis.set_tick_params(labelsize=lbl_fs)
    axes[0,0].yaxis.set_tick_params(labelsize=lbl_fs)
    axes[0,1].xaxis.set_tick_params(labelsize=lbl_fs)
    axes[0,1].yaxis.set_tick_params(labelsize=lbl_fs)
    axes[1,1].xaxis.set_tick_params(labelsize=lbl_fs)
    axes[1,1].yaxis.set_tick_params(labelsize=lbl_fs)
    axes[2,1].xaxis.set_tick_params(labelsize=lbl_fs)
    axes[2,1].yaxis.set_tick_params(labelsize=lbl_fs)
    axes[1,0].xaxis.set_tick_params(labelsize=lbl_fs)
    axes[1,0].yaxis.set_tick_params(labelsize=lbl_fs)
    axes[2,0].xaxis.set_tick_params(labelsize=lbl_fs)
    axes[2,0].yaxis.set_tick_params(labelsize=lbl_fs)
    
##############################################
# Merton Jump
# m = 0 # meean of jump size
# v = 0.3 # standard deviation of jump
# lam = 1 # intensity of jump i.e. number of jumps per annum
##############################################      
def merton_jump_call(S,K,T,sigma,r, dv, m , v, lam):
    p = 0
    for k in range(40):
        r_k = r - lam*(m-1) + (k*np.log(m) ) / T
        sigma_k = np.sqrt( sigma**2 + (k* v** 2) / T)
        k_fact = np.math.factorial(k)
        p += (np.exp(-m*lam*T) * (m*lam*T)**k / (k_fact))  * BS(S,K,T,sigma_k,r_k,dv,'C')
    return p 

def merton_jump_put(S, K, T, sigma, r,dv, m , v, lam):
    p = 0 # price of option
    for k in range(40):
        r_k = r - lam*(m-1) + (k*np.log(m) ) / T
        sigma_k = np.sqrt( sigma**2 + (k* v** 2) / T)
        k_fact = np.math.factorial(k) # 
        p += (np.exp(-m*lam*T) * (m*lam*T)**k / (k_fact)) * BS(S,K,T,sigma_k,r_k,dv,'P')
    return p 

def merton_calibration(T,S,r,x0, bounds, strikes, prices):
    from scipy.optimize import minimize
    
    def optimal_params(x, mkt_prices, strikes):
        candidate_prices = merton_jump_call(S, strikes, T,
                                            sigma=x[0], r=r, dv=x[4], m= x[1] ,
                                            v=x[2],lam= x[3])
        return np.linalg.norm(mkt_prices - candidate_prices, 2)


    #T = df['T'].values[0]
    #S = df.F.values[0]
    #r = 0 
    #x0 = [0.15, 1, 0.1, 1, 0] # initial guess for algorithm
    #bounds = ((0.01, np.inf) , (0.01, 2), (1e-5, np.inf) , (0, 5), (-1, 1)) #bounds as described above
    #strikes = df.Strike.values
    #prices = df.Midpoint.values

    res = minimize(optimal_params, method='SLSQP',  x0=x0, args=(prices, strikes),
                      bounds = bounds, tol=1e-20, 
                      options={"maxiter":1000})
    sigt = res.x[0] #Volatlity
    mt = res.x[1] #Jump Mean
    vt = res.x[2] #Jump Std
    lamt = res.x[3] #intensity
    
    return sigt,mt,vt,lamt
      
##############################################
# Corrado-Su Original Model
##############################################      
def CND(x, mu, sigma):
    t = x-mu;
    y = 0.5*m.erf(-t/(sigma*np.sqrt(2.0)));
    if y>1.0:
        y = 1.0;
    return y

def ND(x, mu, sigma):
    u = (x-mu)/abs(sigma)
    y = (1/(np.sqrt(2*np.pi)*abs(sigma)))*np.exp(-u*u/2)
    return y

def CorradoSu(S, X, T, r, vol, Skew, Kurt, side='C'):
    d1 = (np.log(S / X) + (r + (vol ** 2) / 2.0) * T) / (vol * np.sqrt(T))
    d2 = d1 - vol * np.sqrt(T)
    Q4 = 1 / 24.0 * S * vol * np.sqrt(T) * ((d1 ** 2 - 1 - 3 * vol * np.sqrt(T) * d2) * ND(d1,0,1) + (vol ** 3) * np.sqrt(T ** 2) * CND(d1,0,1))
    Q3 = 1 / 6.0 * S * vol * np.sqrt(T) * ((2 * vol * np.sqrt(T) - d1) * ND(d1,0,1) - (vol ** 2) * T * CND(d1,0,1))

    CallPrice = BS(S,X,T,vol,r,0,'C') + Skew * Q3 + (Kurt - 3) * Q4
        
    if (side == 'C'):
        CorradoSu = CallPrice
    else:
        CorradoSu = CallPrice - S + X * np.exp(-r * T)
    
    return CorradoSu

##############################################
# Corrado-Su Corrected Model
##############################################      
def CorradoSu_BRcorr(S, X, T, r, vol, Skew, Kurt, side='C'):
    d1 = (np.log(S / X) + (r + (vol ** 2) / 2.0) * T) / (vol * np.sqrt(T))
    d2 = d1 - vol * np.sqrt(T)
    Q4 = 1 / 24.0 * S * vol * np.sqrt(T) * ((d1 ** 2 - 1 - 3 * vol * np.sqrt(T) * d2) * ND(d1,0,1) + (vol ** 3) * np.sqrt(T ** 2) * CND(d1,0,1))
    Q3 = 1 / 6.0 * S * vol * np.sqrt(T) * ((2 * vol * np.sqrt(T) - d1) * ND(d1,0,1) + (vol ** 2) * T * CND(d1,0,1))
                
                
    CallPrice = BS(S,X,T,vol,r,0,'C') + Skew * Q3 + (Kurt - 3) * Q4
        
    if (side == 'C'):
        CorradoSu = CallPrice
    else:
        CorradoSu = CallPrice - S + X * np.exp(-r * T)
    
    return CorradoSu



##############################################
# VIX Replication
##############################################      
import numpy as np
import pandas as pd
import datetime as dt


def import_yields():
    """Import yields.
    Date,Days,Rate
    20090101,9,0.38
    20090101,37,0.38
    """
    yields = pd.read_csv('../data/yields.csv',
                         converters={'Date': lambda x: dt.datetime.strptime(x, '%Y%m%d')})
    return yields.set_index(['Date', 'Days'])


def import_options():
    """Import options.
    Expiration,Days,Strike,Call Bid,Call Ask,Put Bid,Put Ask
    20090110,9,200,717.6,722.8,0,0.05
    20090110,9,250,667.6,672.9,0,0.05
    20090110,9,300,617.9,622.9,0,0.05
    20090110,9,350,567.9,572.9,0,0.05
    20090110,9,375,542.9,547.9,0,0.1
    """
    # Function to parse dates of '20090101' format
    raw_options = pd.read_csv('../data/options.csv',
                              converters={'Expiration': lambda x: dt.datetime.strptime(x, '%Y%m%d')})

    # Function to convert days to internal timedelta format
    raw_options['Date'] = raw_options['Expiration'] - raw_options['Days'].map(lambda x: dt.timedelta(days=int(x)))
    # Convert integer strikes to float!
    # Otherwise it may lead to accumulation of errors.
    raw_options['Strike'] = raw_options['Strike'].astype(float)

    return raw_options


def clean_options(options):
    """Clean and index option data set.
    """
    # Since VIX is computed for the date of option quotations,
    # we do not really need Expiration
    options.set_index(['Date', 'Days', 'Strike'], inplace=True)
    options.drop('Expiration', axis=1, inplace=True)

    # Do some renaming and separate calls from puts
    calls = options[['Call Bid', 'Call Ask']].rename(columns={'Call Bid': 'Bid', 'Call Ask': 'Ask'})
    puts = options[['Put Bid', 'Put Ask']].rename(columns={'Put Bid': 'Bid', 'Put Ask': 'Ask'})

    # Add a column indicating the type of the option
    calls['CP'], puts['CP'] = 'C', 'P'

    # Merge calls and puts
    options = pd.concat([calls, puts])

    # Reindex and sort
    options.reset_index(inplace=True)
    options.set_index(['Date', 'Days', 'CP', 'Strike'], inplace=True)
    options.sort_index(inplace=True)

    return options


def bid_ask_average(options):
    """Compute bid/ask average
    """

    # This step is used further to filter out in-the-money options.

    options['Premium'] = (options['Bid'] + options['Ask']) / 2
    options = options[options['Bid'] > 0]['Premium'].unstack('CP')
    return options


def put_call_parity(options):
    """Find put-call parity.
    """

    # Find the absolute difference
    options['CPdiff'] = (options['C'] - options['P']).abs()
    # Mark the minimum for each date/term
    grouped = options['CPdiff'].groupby(level=['Date', 'Days'])
    options['min'] = grouped.transform(lambda x: x == x.min())
    return options


def forward_price(options, yields):
    """Compute forward price.
    """
    # Leave only at-the-money options
    df = options[options['min'] == 1].reset_index('Strike')
    df = df.groupby(level=['Date', 'Days']).first().reset_index()
    # Merge with risk-free rate
    df = pd.merge(df, yields.reset_index(), how='left')

    # Compute the implied forward
    df['Forward'] = df['CPdiff'] * np.exp(df['Rate'] * df['Days'] / 36500)
    df['Forward'] += df['Strike']
    forward = df.set_index(['Date', 'Days'])[['Forward']]
    return forward


def at_the_money_strike(options, forward):
    """Compute at-the-money strike.
    """
    # Merge options with implied forward price
    left = options.reset_index().set_index(['Date', 'Days'])
    df = pd.merge(left, forward, left_index=True, right_index=True)
    # Compute at-the-money strike
    df = df[df['Strike'] < df['Forward']]['Strike']
    mid_strike = df.groupby(level=['Date', 'Days']).max()
    mid_strike = pd.DataFrame({'Mid Strike': mid_strike})
    return mid_strike


def leave_out_of_the_money(options, mid_strike):
    """Separate out-of-the-money calls and puts.
    """
    # Go back to original data and reindex it
    left = options.reset_index()
    left.set_index(['Date', 'Days'], inplace=True)
    left.drop('Premium', axis=1, inplace=True)
    # Merge with at-the-money strike
    df = pd.merge(left, mid_strike, left_index=True, right_index=True)
    # Separate out-of-the-money calls and puts
    P = (df['Strike'] <= df['Mid Strike']) & (df['CP'] == 'P')
    C = (df['Strike'] >= df['Mid Strike']) & (df['CP'] == 'C')
    puts, calls = df[P], df[C]
    return puts, calls


def remove_crazy_quotes(calls, puts):
    """Remove all quotes after two consequtive zero bids.
    """
    # Indicator of zero bid
    calls['zero_bid'] = (calls['Bid'] == 0).astype(int)
    # Accumulate number of zero bids starting at-the-money
    grouped = calls.groupby(level=['Date', 'Days'])['zero_bid']
    calls['zero_bid_accum'] = grouped.cumsum()

    # Sort puts in reverse order inside date/term
    grouped = puts.groupby(level=['Date', 'Days'])
    puts = grouped.apply(lambda x: x.sort_values(by='Strike', ascending=False))
    # Indicator of zero bid
    puts['zero_bid'] = (puts['Bid'] == 0).astype(int)
    # Accumulate number of zero bids starting at-the-money
    grouped = puts.groupby(level=['Date', 'Days'])['zero_bid']
    puts['zero_bid_accum'] = grouped.cumsum()

    # Merge puts and calls
    options3 = pd.concat([calls, puts]).reset_index()
    # Throw away bad stuff
    options3 = options3[(options3['zero_bid_accum'] < 2) & (options3['Bid'] > 0)]
    # Compute option premium as bid/ask average
    options3['Premium'] = (options3['Bid'] + options3['Ask']) / 2
    options3.set_index(['Date', 'Days', 'CP', 'Strike'], inplace=True)
    options3 = options3['Premium'].unstack('CP')

    return options3


def out_of_the_money_options(options3, mid_strike):
    """Compute out-of-the-money option price.
    """
    # Merge wth at-the-money strike price
    left = options3.reset_index().set_index(['Date', 'Days'])
    df = pd.merge(left, mid_strike, left_index=True, right_index=True)

    # Conditions to separate out-of-the-money puts and calls
    condition1 = df['Strike'] < df['Mid Strike']
    condition2 = df['Strike'] > df['Mid Strike']
    # At-the-money we have two quotes, so take the average
    df['Premium'] = (df['P'] + df['C']) / 2
    # Remove in-the-money options
    df.loc[condition1, 'Premium'] = df.loc[condition1, 'P']
    df.loc[condition2, 'Premium'] = df.loc[condition2, 'C']

    options4 = df[['Strike', 'Mid Strike', 'Premium']].copy()
    return options4


def f(group):
    new = group.copy()
    new.iloc[1:-1] = np.array((group.iloc[2:] - group.iloc[:-2]) / 2)
    new.iloc[0] = group.iloc[1] - group.iloc[0]
    new.iloc[-1] = group.iloc[-1] - group.iloc[-2]
    return new


def strike_diff(options):
    """Compute difference between adjoining strikes
    """
    options['dK'] = options.groupby(level=['Date', 'Days'])['Strike'].apply(f)
    return options


def strike_contribution(options4, yields):
    """Compute contribution of each strike.
    """

    # Merge with risk-free rate
    contrib = pd.merge(options4, yields, left_index=True, right_index=True)
    contrib.reset_index(inplace=True)

    contrib['sigma2'] = contrib['dK'] / contrib['Strike'] ** 2
    contrib['sigma2'] *= (contrib['Premium'] * np.exp(contrib['Rate'] * contrib['Days'] / 36500))

    return contrib


def each_period_vol(contrib, mid_strike, forward):
    """Compute each preiod index.
    """

    # Sum up contributions from all strikes
    sigma2 = contrib.groupby(['Date', 'Days'])[['sigma2']].sum() * 2

    # Merge at-the-money strike and implied forward
    sigma2['Mid Strike'] = mid_strike
    sigma2['Forward'] = forward

    # Compute variance for each term
    sigma2['sigma2'] -= (sigma2['Forward'] / sigma2['Mid Strike'] - 1) ** 2
    sigma2['sigma2'] /= sigma2.index.get_level_values(1).astype(float) / 365
    sigma2 = sigma2[['sigma2']]

    return sigma2


def near_next_term(group):
    """This function determines near- and next-term
    if there are several maturities in the data.
    """
    days = np.array(group['Days'])
    sigma2 = np.array(group['sigma2'])

    if days.min() < 30:
        T1 = days[days < 30].max()
        T2 = days[days > T1].min()
    elif (days.min() == 30) or (days.max() == 30):
        T1 = T2 = 30
    else:
        T1 = days.min()
        T2 = days[days > T1].min()

    sigma_T1 = sigma2[days == T1][0]
    sigma_T2 = sigma2[days == T2][0]
    data = [{'T1': T1, 'T2': T2, 'sigma2_T1': sigma_T1, 'sigma2_T2': sigma_T2}]

    return pd.DataFrame(data)


def interpolate_vol(sigma2):
    """Compute interpolated index.
    """
    grouped = sigma2.reset_index().groupby('Date')
    two_sigmas = grouped.apply(near_next_term).groupby(level='Date').first()
    return two_sigmas


def interpolate_vix(two_sigmas):
    """Interpolate the VIX.
    """

    df = two_sigmas.copy()

    for t in ['T1', 'T2']:
        # Convert to fraction of the year
        df['days_' + t] = df[t].astype(float) / 365
        # Convert to miutes
        df[t] = (df[t] - 1) * 1440 + 510 + 930

    denom = df['T2'] - df['T1']
    if denom[0] > 0:
        coef1 = df['days_T1'] * (df['T2'] - 30 * 1440) / denom
        coef2 = df['days_T2'] * (30 * 1440 - df['T1']) / denom
    else:
        coef1 = coef2 = df['days_T1']
    df['sigma2_T1'] = df['sigma2_T1'] * coef1
    df['sigma2_T2'] = df['sigma2_T2'] * coef2
    df['VIX'] = ((df['sigma2_T1'] + df['sigma2_T2']) * 365 / 30) ** .5 * 100

    return df


def whitepaper():
    ### Import yields
    yields = import_yields()

    ### Import options
    raw_options = import_options()

    # Uncomment this block to check that VIX is computed,
    # when there are options with exactly 30 days to expire.
    #    yields.reset_index('Days', inplace=True)
    #    yields['Days'] += 21
    #    yields.set_index('Days', inplace=True, append=True)
    #    raw_options['Days'] += 21

    ### Do some cleaning and indexing
    options = clean_options(raw_options)

    ### Compute bid/ask average
    options2 = bid_ask_average(options)

    ### Put-call parity
    options2 = put_call_parity(options2)

    ### Compute forward price
    forward = forward_price(options2, yields)

    ### Compute at-the-money strike
    mid_strike = at_the_money_strike(options2, forward)

    ### Separate out-of-the-money calls and puts
    puts, calls = leave_out_of_the_money(options, mid_strike)

    ### Remove all quotes after two consequtive zero bids
    options3 = remove_crazy_quotes(calls, puts)

    ### Compute out-of-the-money option price
    options4 = out_of_the_money_options(options3, mid_strike)

    ### Compute difference between adjoining strikes
    options4 = strike_diff(options4)

    ### Compute contribution of each strike
    contrib = strike_contribution(options4, yields)

    ### Compute each preiod index
    sigma2 = each_period_vol(contrib, mid_strike, forward)

    ### Compute interpolated index
    two_sigmas = interpolate_vol(sigma2)

    ### Interpolate the VIX
    df = interpolate_vix(two_sigmas)

    return df[['VIX']]


if __name__ == '__main__':
    vixvalue = whitepaper()
    print(vixvalue)