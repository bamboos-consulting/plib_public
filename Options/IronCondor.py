#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Iron Condor strategy 
#
# Module comprising various helper functions
#############################################################################################      


def getExpIronCondor3(trad_day,underlying,options,prange=0.40,win=(45,120,6)):
    fbo3,fbo4,co=getExpIronCondor2(trad_day,underlying,options,prange,win)
    fbo4=fbo4[['EDate1','Strike1','Strike3']]
    fbo3=fbo3[['EDate1','Strike2','Strike4']]
    fbo3=fbo3.set_index('EDate1')
    fbo4=fbo4.set_index('EDate1')
    fbo=fbo4.merge(fbo3, left_on='EDate1', right_on='EDate1')
    fbo=fbo.reset_index()
    fbo=fbo[['EDate1','Strike1','Strike2','Strike4','Strike3']]
    fbo.columns=['EDate1','Strike1','Strike2','Strike3','Strike4']
    return fbo,co
    
def getExpIronCondor2(trad_day,underlying,options,prange=0.40,win=(45,120,6)):
    
    oe1,fbo1,co1=getExpIronCondor(trad_day,underlying,options,2*prange,win)
    oe2,fbo2,co2=getExpIronCondor(trad_day,underlying,options,2*prange,win)
    fbo3=fbo1.append(fbo2).drop_duplicates().reset_index(inplace=False).drop(['index'], axis=1)
    
    oe1,fbo1,co1=getExpIronCondor(trad_day,underlying,options,4*prange,win)
    oe2,fbo2,co2=getExpIronCondor(trad_day,underlying,options,4*prange,win)
    fbo4=fbo1.append(fbo2).drop_duplicates().reset_index(inplace=False).drop(['index'], axis=1)
    
    oe3=oe1.append(oe2)
    co3=co1.append(co2).drop_duplicates().reset_index(inplace=False)
    
    return fbo3,fbo4,co3

def getExpIronCondor(trad_day,underlying,options,prange=0.40,win=(45,120,6)):
    import pandas as pd
    import numpy as np
    import datetime as d
    
    # Set Max days 120 days to trading date
    date_1 = d.datetime.strptime(trad_day, "%Y-%m-%d")
    end_date = date_1 + d.timedelta(days=win[1])
    trad_max= end_date.strftime('%Y-%m-%d') 
    
    # Add 45 days to trading date
    date_1 = d.datetime.strptime(trad_day, "%Y-%m-%d")
    end_date = date_1 + d.timedelta(days=win[0])
    trad_day= end_date.strftime('%Y-%m-%d') 
    
    # Sell OTM Call Vertical Spread
    # Sell OTM Put Vertical Spread
    #prange commands the strikes to be selected in the range und_price +- prange%
    #win[0] is the front month minimum distance from today
    #win[1] is the back month maximum distance from today
    #win[3] is the distance between the two couple of strikes
    #win=(45,120,6)
    
    #expirations
    oe=options[(options['expirationDate']>trad_day) & (options['expirationDate']<trad_max)]
    oe=pd.DataFrame(oe['expirationDate'].unique())
    oe.columns=['Edate']
    oe=oe.reset_index(inplace=False)
    oe=oe.drop(['index'], axis=1)
    oe_max=int(len(oe)/10)
    if oe_max <= 1: oe_max=len(oe)
        
    oe2=options[(options['type']=='PUT') & (options['expirationDate']>trad_day) &
                (options['expirationDate']<trad_max) & 
                (options['strike']<underlying*(1-prange))]
    oe2.sort_values(by=['strike','expirationDate'], ascending=False,inplace=True)
    oe2=oe2.reset_index(inplace=False)
    #oe2=oe2.drop(['index'], axis=1)
    #oe2_max=int(len(oe2)/10)
    #if oe2_max <= 1: oe2_max=len(oe2)
    
    oe3=options[(options['type']=='CALL') & (options['expirationDate']>trad_day) & 
                (options['expirationDate']<trad_max) & 
                (options['strike']>underlying*(1+prange))]
    oe3.sort_values(by=['strike','expirationDate'], ascending=True,inplace=True)
    oe3=oe3.reset_index(inplace=False)
    #oe3=oe3.drop(['index'], axis=1)
    #oe3_max=int(len(oe3)/10)
    #if oe3_max <= 1: oe3_max=len(oe3)
    
    numberOfRows=1+win[2]
    fbo = pd.DataFrame(index=np.arange(0, numberOfRows), columns=('EDate1', 'EDate2','Strike1','Strike2','Strike3','Strike4'))
    
    for i in range(0,len(oe)):
        temp1=oe2[oe2['expirationDate']==oe.Edate.iloc[i]]
        temp2=oe3[oe3['expirationDate']==oe.Edate.iloc[i]]

        # Buy a put, strike A, Sell a put, strike B, Sell a call, strike C, Buy a call, strike D, A<B<C<D
        new_row = {'EDate1':oe.Edate.iloc[i], 'EDate2':oe.Edate.iloc[i],'Strike1':temp1.strike.iloc[win[2]],
                'Strike2':temp1.strike.iloc[0],'Strike3':temp2.strike.iloc[0], 'Strike4':temp2.strike.iloc[2*win[2]]}
        fbo = fbo.append(new_row, ignore_index=True)
        fbo=fbo.dropna()
    
    fbo=fbo.reset_index()
    fbo=fbo.drop(['index'], axis=1)

    caloptions=options[['type','expirationDate','strike','lastPrice','dte','impliedVolatility','delta','gamma','theta','vega','rho']]
    co=caloptions[(caloptions['expirationDate']>trad_day) & (caloptions['expirationDate']<trad_max)]
    return oe3,fbo.drop_duplicates(),co.drop_duplicates()
    
def getIronCondors(i,co,fbo,prange=0.08):
    co1=co[(co['type']=='PUT') & (co['expirationDate']==fbo.EDate1.iloc[i]) & ((co['strike']==fbo.Strike1.iloc[i]) &
            (co['strike']==fbo.Strike1.iloc[i]))].copy()
    co1=co1[co1['dte']==co1['dte'].max()]
    co2=co[(co['type']=='PUT' ) & (co['expirationDate']==fbo.EDate1.iloc[i]) & ((co['strike']==fbo.Strike2.iloc[i]) &
            (co['strike']==fbo.Strike2.iloc[i]))].copy()
    co2=co2[co2['dte']==co2['dte'].max()]
    cob1=co[(co['type']=='CALL') & (co['expirationDate']==fbo.EDate1.iloc[i]) & ((co['strike']==fbo.Strike3.iloc[i]) & 
            (co['strike']==fbo.Strike3.iloc[i]))].copy()
    cob1=cob1[cob1['dte']==cob1['dte'].max()]
    cob2=co[(co['type']=='CALL' ) & (co['expirationDate']==fbo.EDate1.iloc[i]) & ((co['strike']==fbo.Strike4.iloc[i]) & 
            (co['strike']==fbo.Strike4.iloc[i]))].copy()
    cob2=cob2[cob2['dte']==cob2['dte'].max()]
    fb1=co1.merge(co2, on='expirationDate', how='inner')
    fb2=cob1.merge(cob2, on='expirationDate', how='inner',suffixes=('_z', '_w'))

    fb=fb1.merge(fb2, on='expirationDate', how='inner')
    fb['expirationDate_x']=fbo.EDate1.iloc[i]
    fb['expirationDate_y']=fbo.EDate1.iloc[i]
    fb['expirationDate_z']=fbo.EDate1.iloc[i]
    fb['expirationDate_w']=fbo.EDate1.iloc[i]

    fb=fb[['expirationDate_x','type_x','strike_x','lastPrice_x','impliedVolatility_x','dte_x',
           'expirationDate_y','type_y','strike_y','lastPrice_y','impliedVolatility_y','dte_y',
           'expirationDate_z','type_z','strike_z','lastPrice_z','impliedVolatility_z','dte_z',
           'expirationDate_w','type_w','strike_w','lastPrice_w','impliedVolatility_w','dte_w',
           'delta_x','gamma_x','theta_x','vega_x','rho_x','delta_y','gamma_y','theta_y','vega_y','rho_y',
           'delta_z','gamma_z','theta_z','vega_z','rho_z','delta_w','gamma_w','theta_w','vega_w','rho_w']]

    #fb=fb[(fb['strike_x']!=fb['strike_y']) & (fb['strike_z']!=fb['strike_w'])]
    fb=fb.reset_index()
    fb=fb.drop(['index'], axis=1)
    return fb.drop_duplicates()
    
def getICLegs(s,fb):
    def getType(s):
        mt='p'
        if s=='CALL': mt='c'
        return mt
    
    ftype,btype=getType(fb.type_x.iloc[s]),getType(fb.type_y.iloc[s])
    fstrike,bstrike=fb.strike_x.iloc[s],fb.strike_y.iloc[s]
    fcallp,bcallp=fb.lastPrice_x.iloc[s],fb.lastPrice_y.iloc[s]
    fiv,biv=fb.impliedVolatility_x.iloc[s],fb.impliedVolatility_y.iloc[s]
    fdtm,bdtm=fb.dte_x.iloc[s],fb.dte_y.iloc[s]
    deltaf,deltab=fb.delta_x.iloc[s],fb.delta_y.iloc[s]
    vegaf,vegab=fb.vega_x.iloc[s],fb.vega_y.iloc[s]
    gammaf,gammab=fb.gamma_x.iloc[s],fb.gamma_y.iloc[s]
    thetaf,thetab=fb.theta_x.iloc[s],fb.theta_y.iloc[s]
    
    ftype2,btype2=getType(fb.type_z.iloc[s]),getType(fb.type_w.iloc[s])
    fstrike2,bstrike2=fb.strike_z.iloc[s],fb.strike_w.iloc[s]
    fcallp2,bcallp2=fb.lastPrice_z.iloc[s],fb.lastPrice_w.iloc[s]
    fiv2,biv2=fb.impliedVolatility_z.iloc[s],fb.impliedVolatility_w.iloc[s]
    fdtm2,bdtm2=fb.dte_z.iloc[s],fb.dte_w.iloc[s]
    deltaf2,deltab2=fb.delta_z.iloc[s],fb.delta_w.iloc[s]
    vegaf2,vegab2=fb.vega_z.iloc[s],fb.vega_w.iloc[s]
    gammaf2,gammab2=fb.gamma_z.iloc[s],fb.gamma_w.iloc[s]
    thetaf2,thetab2=fb.theta_z.iloc[s],fb.theta_w.iloc[s]
    
    t_options=[fstrike,fcallp,fiv,fdtm,deltaf,vegaf,gammaf,thetaf,ftype, 
              bstrike,bcallp,biv,bdtm,deltab,vegab,gammab,thetab,btype,
              fstrike2,fcallp2,fiv2,fdtm2,deltaf2,vegaf2,gammaf2,thetaf2,ftype2, 
              bstrike2,bcallp2,biv2,bdtm2,deltab2,vegab2,gammab2,thetab2,btype2]
    
    return t_options
    
def getICLegs2(s,fb, sigma=0.22, ir=0.03):
    ftype,btype=fb.type_x.iloc[s][0],fb.type_y.iloc[s][0]
    fstrike,bstrike=fb.strike_x.iloc[s],fb.strike_y.iloc[s]
    ftype2,btype2=fb.type_z.iloc[s][0],fb.type_w.iloc[s][0]
    fstrike2,bstrike2=fb.strike_z.iloc[s],fb.strike_w.iloc[s]
    #fiv,biv=fb.impliedVolatility_x.iloc[s],fb.impliedVolatility_y.iloc[s]
    t_options=[(int(fstrike),ir,sigma,0,ftype), (int(bstrike),ir,sigma,0,btype),
               (int(fstrike2),ir,sigma,0,ftype2), (int(bstrike2),ir,sigma,0,btype2)]
    return t_options
    
def getICLegs3(s,fb, S=100, ir=0.03):
    ftype,btype=fb.type_x.iloc[s][0],fb.type_y.iloc[s][0]
    fstrike,bstrike=fb.strike_x.iloc[s],fb.strike_y.iloc[s]
    fdtm,bdtm=fb.dte_x.iloc[s],fb.dte_y.iloc[s]
    fiv,biv=fb.impliedVolatility_x.iloc[s],fb.impliedVolatility_y.iloc[s]    
    
    ftype2,btype2=fb.type_z.iloc[s][0],fb.type_w.iloc[s][0]
    fstrike2,bstrike2=fb.strike_z.iloc[s],fb.strike_w.iloc[s]
    fdtm2,bdtm2=fb.dte_z.iloc[s],fb.dte_w.iloc[s]
    fiv2,biv2=fb.impliedVolatility_z.iloc[s],fb.impliedVolatility_w.iloc[s]    
    
    t_options={'S1' : S,'K1' : int(fstrike),'T1' : fdtm,'s1' : fiv,'r1' : ir,'t1' : 0,'dv1': 0,'side1' : ftype,
               'S2' : S,'K2' : bstrike,'T2' : bdtm,'s2' : biv,'r2' : ir,'t2' : 0,'dv2': 0,'side2' : btype,
               'S1' : S,'K1' : int(fstrike2),'T1' : fdtm2,'s1' : fiv2,'r1' : ir,'t1' : 0,'dv1': 0,'side1' : ftype2,
               'S2' : S,'K2' : bstrike2,'T2' : bdtm2,'s2' : biv2,'r2' : ir,'t2' : 0,'dv2': 0,'side2' : btype2}
    return t_options

def IronCondor(sLegs,und, irate,desc,prange=0.42,LongShort=1,greeks=False,sigma=0,plot=False):
    #import py_vollib.black_scholes
    #import py_vollib.black_scholes.greeks.analytical as greeks
    import numpy as np
    import pandas as pd
    import Plib.Options.Models as md
    pd.options.display.float_format = '{:,.6f}'.format
    
    
    fstrike1,fcallp1,fiv1,fdtm1,deltaf1,vegaf1,gammaf1,thetaf1,ftype1=sLegs[0:9]
    bstrike1,bcallp1,biv1,bdtm1,deltab1,vegab1,gammab1,thetab1,btype1=sLegs[9:18]
    fstrike2,fcallp2,fiv2,fdtm2,deltaf2,vegaf2,gammaf2,thetaf2,ftype2=sLegs[18:27]
    bstrike2,bcallp2,biv2,bdtm2,deltab2,vegab2,gammab2,thetab2,btype2=sLegs[27:36]
    
    if greeks:
        deltaf1=md.delta(und,fstrike1,fdtm1,sigma,irate,dv=0,side=ftype1)
        gammaf1=md.vega(und,fstrike1,fdtm1,sigma,irate,dv=0)
        thetaf1=md.gamma(und,fstrike1,fdtm1,sigma,irate,dv=0)
        vegaf1=md.theta(und,fstrike1,fdtm1,sigma,irate,dv=0,side=ftype1)

        deltab1=md.delta(und,bstrike1,bdtm1,sigma,irate,dv=0,side=btype1)
        vegab1=md.vega(und,bstrike1,bdtm1,sigma,irate,dv=0)
        gammab1=md.gamma(und,bstrike1,bdtm1,sigma,irate,dv=0)
        thetab1=md.theta(und,bstrike1,bdtm1,sigma,irate,dv=0,side=btype1)
    
        deltaf2=md.delta(und,fstrike2,fdtm2,sigma,irate,dv=0,side=ftype2)
        gammaf2=md.vega(und,fstrike2,fdtm2,sigma,irate,dv=0)
        thetaf2=md.gamma(und,fstrike2,fdtm2,sigma,irate,dv=0)
        vegaf2=md.theta(und,fstrike2,fdtm2,sigma,irate,dv=0,side=ftype2)

        deltab2=md.delta(und,bstrike2,bdtm2,sigma,irate,dv=0,side=btype2)
        vegab2=md.vega(und,bstrike2,bdtm2,sigma,irate,dv=0)
        gammab2=md.gamma(und,bstrike2,bdtm2,sigma,irate,dv=0)
        thetab2=md.theta(und,bstrike2,bdtm2,sigma,irate,dv=0,side=btype2)
        
    #if greeks:
    #    deltaf1=greeks.delta(ftype1, und, fstrike1, fdtm1, irate, sigma)
    #    gammaf1=greeks.gamma(ftype1, und, fstrike1, fdtm1, irate, sigma)
    #    thetaf1=greeks.theta(ftype1, und, fstrike1, fdtm1,irate, sigma)
    #    vegaf1=greeks.vega(ftype1, und, fstrike1, fdtm1, irate, sigma)
    #    deltab1=greeks.delta(btype1, und, bstrike1, bdtm1, irate, sigma)
    #    gammab1=greeks.gamma(btype1, und, bstrike1, bdtm1, irate, sigma)
    #    thetab1=greeks.theta(btype1, und, bstrike1, bdtm1,irate, sigma)
    #    vegafb=greeks.vega(btype1, und, bstrike1, bdtm1, irate, sigma)
    #    deltaf2=greeks.delta(ftype2, und, fstrike2, fdtm2, irate, sigma)
    #    gammaf2=greeks.gamma(ftype2, und, fstrike2, fdtm2, irate, sigma)
    #    thetaf2=greeks.theta(ftype2, und, fstrike2, fdtm2,irate, sigma)
    #    vegaf2=greeks.vega(ftype2, und, fstrike2, fdtm2, irate, sigma)
    #    deltab2=greeks.delta(btype2, und, bstrike2, bdtm2, irate, sigma)
    #    gammab2=greeks.gamma(btype2, und, bstrike2, bdtm2, irate, sigma)
    #    thetab2=greeks.theta(btype2, und, bstrike2, bdtm2,irate, sigma)
    #    vegab2=greeks.vega(btype2, und, bstrike2, bdtm2, irate, sigma)
        
    setup_cost = -LongShort*bcallp1*100 + LongShort*fcallp1*100 +LongShort*bcallp2*100 - LongShort*fcallp2*100 
    # Range of values for underlying
    sT = np.arange((1-prange)*und,(1+prange)*und,0.25) 
    df = pd.DataFrame()
    df['und_price'] = sT
    df['foptp1'] = np.nan
    df['boptp1'] = np.nan
    df['foptp2'] = np.nan
    df['boptp2'] = np.nan
    # Calculating call price for different possible values of Underlying
    # Note alla values assumed to be in annualized form
    #for i in range(0,len(df)):
    #    df.loc[i,'foptp1'] = 100*py_vollib.black_scholes.black_scholes(ftype1, df.iloc[i]['und_price'], fstrike1, fdtm1, irate, fiv1)
    #    df.loc[i,'boptp1'] = 100*py_vollib.black_scholes.black_scholes(btype1, df.iloc[i]['und_price'], bstrike1, bdtm1, irate, biv1)
    #    df.loc[i,'foptp2'] = 100*py_vollib.black_scholes.black_scholes(ftype2, df.iloc[i]['und_price'], fstrike2, fdtm2, irate, fiv2)
    #    df.loc[i,'boptp2'] = 100*py_vollib.black_scholes.black_scholes(btype2, df.iloc[i]['und_price'], bstrike2, bdtm2, irate, biv2)
    
    for i in range(0,len(df)):
        df.loc[i,'foptp1'] = 100*md.BS(df.iloc[i]['und_price'],fstrike1,fdtm1,fiv1,irate,dv=0,side=ftype1)
        df.loc[i,'boptp1'] = 100*md.BS(df.iloc[i]['und_price'],bstrike1,bdtm1,biv1,irate,dv=0,side=btype1)
        df.loc[i,'foptp2'] = 100*md.BS(df.iloc[i]['und_price'],fstrike2,fdtm2,fiv2,irate,dv=0,side=ftype2)
        df.loc[i,'boptp2'] = 100*md.BS(df.iloc[i]['und_price'],bstrike2,bdtm2,biv2,irate,dv=0,side=btype2)
    
    df['payoff'] = -LongShort*df.boptp1 + LongShort*df.foptp1 +LongShort*df.boptp2 - LongShort*df.foptp2 - setup_cost
    df['delta']=(-LongShort*100*deltab1+LongShort*100*deltaf1+LongShort*100*deltab2-LongShort*100*deltaf2)/100
    df['vega']=(-LongShort*100*vegab1+LongShort*100*vegaf1+LongShort*100*vegab2-LongShort*100*vegaf2)/100
    df['gamma']=(-LongShort*100*gammab1+LongShort*100*gammaf1+LongShort*100*gammab2-LongShort*100*gammaf2)/100
    df['theta']=(-LongShort*100*thetab1+LongShort*100*thetaf1+LongShort*100*thetab2-LongShort*100*thetaf2)/100
    df['setup']=setup_cost
    df['hedge']=0
    if df['delta'].mean() != 0: df['hedge']=100/df['delta']
    df['prob']=0
    if plot:
        plt.figure(figsize=(10,5))
        plt.ylabel("payoff")
        plt.xlabel("Underlying Price")
        plt.plot(sT,df.payoff)
        plt.title(desc)
        plt.show()
    return df