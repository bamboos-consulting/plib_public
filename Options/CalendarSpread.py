#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Calendar Spread strategy 
#
# Module comprising various helper functions
#############################################################################################      

import numpy as np
import pandas as pd
pd.options.mode.chained_assignment = None  # default='warn'

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path+"/")

def getExpDCalSpread(side,trad_day,underlying_price,options,prange=0.08,win=(30,150,5,3)):
    import datetime as d
    
    #prange commands the strikes to be selected in the range und_price +- prange%
    #win[0] is the front month minimum distance from today
    #win[1] is the back month maximum distance from today
    #win[2] is the distance in expirations between front and back
    #win[3] is the number of expirations to be included from the above distance
    #win=(30,150,6,3)
    
    eval_data=trad_day
    # Set Max days 150 days to trading date
    date_1 = d.datetime.strptime(trad_day, "%Y-%m-%d")
    end_date = date_1 + d.timedelta(days=win[1])
    trad_max= end_date.strftime('%Y-%m-%d') 
    
    # Add 30 days to trading date
    date_1 = d.datetime.strptime(trad_day, "%Y-%m-%d")
    end_date = date_1 + d.timedelta(days=win[0])
    trad_day= end_date.strftime('%Y-%m-%d') 
    
    # Select expirations from trad_day chain
    options=options[options.index==eval_data]
    oe=options[(options['expirationDate']>=trad_day) & (options['expirationDate']<=trad_max)]
    oe=pd.DataFrame(oe['expirationDate'].unique())
    oe.columns=['Edate']
    oe=oe.reset_index(inplace=False)
    oe=oe.drop(['index'], axis=1)
    
    numberOfRows = 2+win[2]
    fbo = pd.DataFrame(index=np.arange(0, numberOfRows), columns=('EDate1', 'EDate2') )
    for i in range(0,len(oe)):
        if i< len(oe)-numberOfRows:
            for k in range(0,win[3]):
                new_row = {'EDate1':oe.Edate.iloc[i], 'EDate2':oe.Edate.iloc[i+k+win[2]]}
                fbo = fbo.append(new_row, ignore_index=True)            
            #new_row = {'EDate1':oe.Edate.iloc[i], 'EDate2':oe.Edate.iloc[i+win[2]]}
            #fbo = fbo.append(new_row, ignore_index=True)
            #new_row = {'EDate1':oe.Edate.iloc[i], 'EDate2':oe.Edate.iloc[i+1+win[2]]}
            #fbo = fbo.append(new_row, ignore_index=True)
            #new_row = {'EDate1':oe.Edate.iloc[i], 'EDate2':oe.Edate.iloc[i+2+win[2]]}
            #fbo = fbo.append(new_row, ignore_index=True)
        elif i== len(oe)-numberOfRows:
            new_row = {'EDate1':oe.Edate.iloc[i], 'EDate2':oe.Edate.iloc[i+win[2]]}
            fbo = fbo.append(new_row, ignore_index=True)
            new_row = {'EDate1':oe.Edate.iloc[i], 'EDate2':oe.Edate.iloc[i+1+win[2]]}
            fbo = fbo.append(new_row, ignore_index=True)
    fbo=fbo.dropna()
    fbo=fbo.reset_index()
    fbo=fbo.drop(['index'], axis=1)

    caloptions=options[['type','expirationDate','strike','lastPrice','dte','impliedVolatility','delta','gamma','theta','vega','rho']]
    co=caloptions[(caloptions['type']==side) & 
                  (caloptions['expirationDate']>=trad_day) & (caloptions['expirationDate']<=trad_max) &
                  (caloptions['strike']<=underlying_price*(1+prange)) & (caloptions['strike']>=underlying_price*(1-prange))]
    return fbo.drop_duplicates(),co.drop_duplicates()
    
def getCalSpreads(i,co,fbo):
    co1=co[co['expirationDate']==fbo.EDate1.iloc[i]]
    co2=co[co['expirationDate']==fbo.EDate2.iloc[i]]
    fb=fbo.merge(co1, left_on='EDate1', right_on='expirationDate')  
    fb=fb.merge(co2, left_on='EDate2', right_on='expirationDate')
    fb=fb[['expirationDate_x','type_x','strike_x','lastPrice_x','impliedVolatility_x','dte_x','expirationDate_y','type_y','strike_y','lastPrice_y','impliedVolatility_y','dte_y','delta_x','gamma_x','theta_x','vega_x','rho_x','delta_y','gamma_y','theta_y','vega_y','rho_y']]
    fb=fb[fb['strike_x']==fb['strike_y']]
    fb=fb.reset_index()
    fb=fb.drop(['index'], axis=1)
    return fb.drop_duplicates()

def getCSLegsTrader(symbol,s,fb):
    def getType(s):
        mt='p'
        if s=='CALL': 
            mt='c'
        return mt
    
    ftype,btype=getType(fb.type_x.iloc[s]),getType(fb.type_y.iloc[s])
    fstrike,bstrike=fb.strike_x.iloc[s],fb.strike_y.iloc[s]
    fcallp,bcallp=fb.lastPrice_x.iloc[s],fb.lastPrice_y.iloc[s]
    fexp,bexp=fb.expirationDate_x.iloc[s],fb.expirationDate_y.iloc[s]    
    return symbol,(fexp,fstrike,ftype,fcallp),(bexp,bstrike,btype,bcallp)
    
def getCSLegs(s,fb):
    def getType(s):
        mt='p'
        if s=='CALL': 
            mt='c'
        return mt
    
    ftype,btype=getType(fb.type_x.iloc[s]),getType(fb.type_y.iloc[s])
    fstrike,bstrike=fb.strike_x.iloc[s],fb.strike_y.iloc[s]
    fcallp,bcallp=fb.lastPrice_x.iloc[s],fb.lastPrice_y.iloc[s]
    fiv,biv=fb.impliedVolatility_x.iloc[s],fb.impliedVolatility_y.iloc[s]
    fdtm,bdtm=fb.dte_x.iloc[s],fb.dte_y.iloc[s]
    deltaf,deltab=fb.delta_x.iloc[s],fb.delta_y.iloc[s]
    vegaf,vegab=fb.vega_x.iloc[s],fb.vega_y.iloc[s]
    gammaf,gammab=fb.gamma_x.iloc[s],fb.gamma_y.iloc[s]
    thetaf,thetab=fb.theta_x.iloc[s],fb.theta_y.iloc[s]
    
    t_options=[fstrike,fcallp,fiv,fdtm,deltaf,vegaf,gammaf,thetaf,ftype, \
              bstrike,bcallp,biv,bdtm,deltab,vegab,gammab,thetab,btype]
    
    return t_options

def getCSLegs2(s,fb, sigma=0.22, ir=0.03):
    ftype,btype=fb.type_x.iloc[s][0],fb.type_y.iloc[s][0]
    fstrike,bstrike=fb.strike_x.iloc[s],fb.strike_y.iloc[s]
    #fiv,biv=fb.impliedVolatility_x.iloc[s],fb.impliedVolatility_y.iloc[s]
    t_options=[(int(fstrike),ir,sigma,0,ftype), (int(bstrike),ir,sigma,0,btype)]
    return t_options
    
def getCSLegs3(s,fb, S=100, ir=0.03):
    ftype,btype=fb.type_x.iloc[s][0],fb.type_y.iloc[s][0]
    fstrike,bstrike=fb.strike_x.iloc[s],fb.strike_y.iloc[s]
    fdtm,bdtm=fb.dte_x.iloc[s],fb.dte_y.iloc[s]
    fiv,biv=fb.impliedVolatility_x.iloc[s],fb.impliedVolatility_y.iloc[s]    
    t_options={'S1' : S,'K1' : int(fstrike),'T1' : fdtm,'s1' : fiv,'r1' : ir,'t1' : 0,'dv1': 0,'side1' : ftype,
       'S2' : S,'K2' : bstrike,'T2' : bdtm,'s2' : biv,'r2' : ir,'t2' : 0,'dv2': 0,'side2' : btype}
    return t_options
    
def calendarSpread(sLegs,und, irate,desc,prange=0.42,LongShort=1,greeks=False,sigma=0,plot=False):
    import Plib.Options.Models as md
    pd.options.display.float_format = '{:,.6f}'.format
    
    # Front call = near-term expiration
    # Back call = far-term expiration
    # Sell Front, Buy Back
    fstrike1,fcallp1,fiv1,fdtm1,deltaf1,vegaf1,gammaf1,thetaf1,ftype1=sLegs[0:9]
    bstrike1,bcallp1,biv1,bdtm1,deltab1,vegab1,gammab1,thetab1,btype1=sLegs[9:18]

    if greeks:
        deltaf1=md.delta(und,fstrike1,fdtm1,sigma,irate,dv=0,side=ftype1)
        gammaf1=md.vega(und,fstrike1,fdtm1,sigma,irate,dv=0)
        thetaf1=md.gamma(und,fstrike1,fdtm1,sigma,irate,dv=0)
        vegaf1=md.theta(und,fstrike1,fdtm1,sigma,irate,dv=0,side=ftype1)
        deltab1=md.delta(und,bstrike1,bdtm1,sigma,irate,dv=0,side=btype1)
        vegab1=md.vega(und,bstrike1,bdtm1,sigma,irate,dv=0)
        gammab1=md.gamma(und,bstrike1,bdtm1,sigma,irate,dv=0)
        thetab1=md.theta(und,bstrike1,bdtm1,sigma,irate,dv=0,side=btype1)
        
    setup_cost = LongShort*bcallp1*100 - LongShort*fcallp1*100
    # Range of values for underlying
    sT = np.arange((1-prange)*und,(1+prange)*und,0.25) 
    df = pd.DataFrame()
    df['und_price'] = sT
    df['foptp'] = np.nan
    df['boptp'] = np.nan
    # Calculating call price for different possible values of Underlying
    # Note alla values assumed to be in annualized form
    for i in range(0,len(df)):
        df.loc[i,'foptp1'] = 100*md.BS(df.iloc[i]['und_price'],fstrike1,fdtm1,fiv1,irate,dv=0,side=ftype1)
        df.loc[i,'boptp1'] = 100*md.BS(df.iloc[i]['und_price'],bstrike1,bdtm1,biv1,irate,dv=0,side=btype1)
        
    df['payoff'] = LongShort*df.boptp1 - LongShort*df.foptp1 - setup_cost
    df['delta'] = (LongShort*100*deltab1-LongShort*100*deltaf1)#/100   #on 100 scale
    df['vega'] = (LongShort*100*vegab1-LongShort*100*vegaf1)/100
    df['gamma'] = (LongShort*100*gammab1-LongShort*100*gammaf1)/100
    df['theta'] = (LongShort*100*thetab1-LongShort*100*thetaf1)/100
    df['setup'] = setup_cost
    df['hedge']=0
    if df['delta'].mean() != 0: df['hedge']=100/df['delta']
    df['prob']=0
    if plot:
        plt.figure(figsize=(10,5))
        plt.ylabel("payoff")
        plt.xlabel("Underlying Price")
        plt.plot(sT,df.payoff)
        plt.title(desc)
        plt.show()
    return df

    
    
