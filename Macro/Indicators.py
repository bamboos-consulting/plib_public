#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Macro Indicators and Plots 
#
# Module comprising plot and helper functions
#############################################################################################      

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path+"/")

import Plib.Utils.Tools as tls


##############################################
# Shiller CAPE
##############################################         
def getShillerCAPE(reqd=True):
    import Plib.Utils.Tools as ut
    from dateutil import parser
    from time import gmtime, strftime
    from datetime import timedelta, date
    import numpy as np
    import matplotlib.pyplot as plt
    import pandas as pd
    pd.options.mode.chained_assignment = None
    
    url = 'http://www.econ.yale.edu/~shiller/data/ie_data.xls'
    data=ut.getExcelFromUrl(url,'Data',8,reqd,myname='CAPE')
    format = '%Y-%m'
    data['Mdate']=pd.to_datetime('1990-01', format=format)
    last_a='00'
    for i in range(0,len(data)-1):
        a=str(data['Date'].iloc[i]).split('.')
        if last_a[1]=='09':
            a[1]='10'
        last_a=a
        data['Mdate'].iloc[i] = pd.to_datetime(a[0]+'-'+a[1], format=format)
    data=data[:-1]
    data["Mdate"]= data["Mdate"].dt.strftime("%Y-%m")
    data=data[['Mdate','Rate GS10','CAPE','P','E','Earnings.1','CPI','Price','Real Return', 'Real Return.1','Yield','Returns.2']].interpolate()
    data=data[['Mdate','Rate GS10','CAPE','P','E','Earnings.1','CPI','Price','Real Return', 'Real Return.1','Yield','Returns.2']]
    data.columns=['Date','LT Rates','CAPE','Price','Earnings','RealEarnings','CPI','RealPrice','AnnReal10YStockRets','AnnReal10YBondsRets','ExcessCapeYield','ExcessAnnReal10YRetsS-B']
    data['Date']=pd.to_datetime(data['Date'])
    data=data.set_index('Date').sort_index().interpolate().ffill().backfill()
    data=data.sort_index()
    
    return data

##############################################
# INflation and CBDI
##############################################         
def getCBondsDistressIndex(reqd=True):
    import Plib.Utils.Tools as ut
    from dateutil import parser
    from time import gmtime, strftime
    from datetime import timedelta, date
    import numpy as np
    import matplotlib.pyplot as plt
    import pandas as pd
    pd.options.mode.chained_assignment = None
    
    url = 'https://www.newyorkfed.org/medialibrary/research/interactives/cmdi/downloads/Market%20CMDI.xlsx'
    data=ut.getExcelFromUrl(url,'Index Data',6,reqd,myname='CBDI')
    data=data[list(data.columns[:4])]
    data.columns=['Date','Market CMDI','IG CMDI','HY CMDI']
    data['Date']=pd.to_datetime(data['Date'])
    data=data.set_index('Date')
    return data
                    
##############################################
# Get the yield curve from FRED
##############################################         
def getYieldCurve(year,wait=1,years='1'):
    import Plib.DataFarm.Fred as fa
    
    df=fa.getYieldCurve(year)
    return df
    
