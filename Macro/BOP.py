#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Balance of Payment Plots 
#
# Module including helper functions 
#############################################################################################      

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import Plib.DataFarm.IMF as imf
import Plib.Macro.Cycle as c

##############################################
# Balance of Payments Helper Functions
##############################################        
def searchBOPSeries(searchTerm='Balance of Payments'):
    imf.searchIMFSeries(searchTerm)
            
def searchBOPAGG(searchTerm='BOPAGG'):
    imf.searchIMFDatastructure(searchTerm)

def printBOPAGGseries(searchTerm='CL_INDICATOR_BOPAGG'):
    imf.showIMFCodeList(searchTerm)
    
def printBOPAGGfreq(searchTerm='CL_FREQ'):
    imf.showIMFCodeList(searchTerm)
    
def printBOPAGGareas(searchTerm='CL_AREA_BOPAGG'):
    imf.showIMFCodeList(searchTerm)
    
##############################################
# Balance of Payments Components Plots
##############################################        

def plotCurrAccComponents(dataset="BOP",start_d="2000",end_d="2020",country="US",frequency="Q",whichrs='JHDUSRGDPBR',fs=(30,15), save=False):
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    import Plib.Plotting.Plots as pl
    
    recessions=c.getRecessions(whichrs)
    
    name='services'
    aserie='BS_BP6_USD'
    df1=imf.getDataImf(name,aserie,dataset,country,frequency,start_d,end_d)

    name='goods'
    bserie='BG_BP6_USD'
    df2=imf.getDataImf(name,bserie,dataset,country,frequency,start_d,end_d)

    name='primaryi'
    cserie='BIP_BP6_USD'
    df3=imf.getDataImf(name,cserie,dataset,country,frequency,start_d,end_d)

    name='secondaryi'
    dserie='BIS_BP6_USD'
    df4=imf.getDataImf(name,dserie,dataset,country,frequency,start_d,end_d)

    name='total'
    eserie='BCA_BP6_USD'
    df5=imf.getDataImf(name,eserie,dataset,country,frequency,start_d,end_d)
    
    fig = plt.figure(figsize=fs,clear=True,constrained_layout=False)
    ax = plt.gca()
    df1.plot(kind='line',x='Date',y='services', color='orange',linestyle='--',linewidth=3,ax=ax)
    df2.plot(kind='line',x='Date',y='goods', color='red',linestyle='--',linewidth=3, ax=ax)
    df3.plot(kind='line',x='Date',y='primaryi',linestyle='-.',linewidth=3, color='blue', ax=ax)
    df4.plot(kind='line',x='Date',y='secondaryi',linestyle='-.',linewidth=3, color='green', ax=ax)
    df5.plot(kind='line',x='Date',y='total', color='black',linewidth=5, ax=ax)
    
    for i in range(len(recessions)):
        d = recessions[i]
        c.addRec(ax, df1, d, color='y', alpha=0.05, lw=0)
        c.addRec(ax, df2, d, color='y', alpha=0.05, lw=0)
        c.addRec(ax, df3, d, color='y', alpha=0.05, lw=0)
        c.addRec(ax, df4, d, color='y', alpha=0.05, lw=0)
        c.addRec(ax, df5, d, color='y', alpha=0.05, lw=0)
    
    plt.xlabel('Year', fontsize=18)
    plt.ylabel('USD', fontsize=18)
    plt.legend(loc=2, prop={'size': 16})
    plt.xticks(fontsize=16, rotation=0)
    plt.yticks(fontsize=16, rotation=0)
    plt.title(country + ' - Current Account Components, US Dollars', fontsize=18);
     
    pl.customize_grid(ax,x=3,y=3)
    
    plt.show()
    if save:
        fig.savefig('CA_accounts_'+country+'.png', dpi=fig.dpi, bbox_inches='tight', pad_inches=0.3)
    
    return df1,df2,df3,df4,df5

def plotBOPSeries(imf_series,syear,eyear,stitle,country,freq,whichrs='JHDUSRGDPBR',fs=(30,15), save=False):
    import requests  
    import pandas as pd
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    import Plib.Plotting.Plots as pl
    
    recessions=c.getRecessions(whichrs)
    
    IMF_Ret_Series=imf.getDataImf(imf_series,imf_series,'BOP',country,freq,syear,eyear)
    #url = 'http://dataservices.imf.org/REST/SDMX_JSON.svc/CompactData/BOP/'+ freq +'.'+ country +'.'+ imf_series +'.?startPeriod=' + syear + '&endPeriod=' + eyear
    #data = requests.get(url).json()
    #IMF_Ret_Series = pd.DataFrame(data['CompactData']['DataSet']['Series']['Obs'])
    #IMF_Ret_Series.columns = ['DATE',imf_series];
    #IMF_Ret_Series = IMF_Ret_Series.set_index(pd.to_datetime(IMF_Ret_Series['DATE']))[imf_series].astype('float')
    
    try:
        fig = plt.figure(figsize=(15,8),clear=True,constrained_layout=False)
        ax = plt.gca()

        IMF_Ret_Series.plot(grid=True, figsize=fs, color="blue", linewidth=2,)
        plt.ylabel(imf_series)
        plt.xlabel('Year')
        plt.title(country + ' - ' + stitle);
        pl.customize_grid(ax,x=3,y=3)
        
        for i in range(len(recessions)):
            d = recessions[i]
            c.addRec(ax, IMF_Ret_Series, d, color='y', alpha=0.25, lw=0)
        
        plt.show()
        mytit=stitle.split(',')
        if save:
            fig.savefig(mytit[0]+'_'+country+'.png', dpi=fig.dpi, bbox_inches='tight', pad_inches=0.3)
    except:
        print()
    return IMF_Ret_Series
    
def plotCapitalAccount(imf_series='BK_BP6_USD',syear='2000',eyear='2020',stitle='Capital Account, Total, Net, US Dollars',country='US',freq='Q',whichrs='JHDUSRGDPBR',fs=(30,15)):
    ret=plotBOPSeries(imf_series,syear,eyear,stitle,country,freq,whichrs=whichrs,fs=fs)
    return ret
    
def plotCurrentAccount(imf_series='BCA_BP6_USD',syear='2000',eyear='2020',stitle='Current Account, Total, Net, US Dollars',country='US',freq='Q',whichrs='JHDUSRGDPBR',fs=(30,15)):
    ret=plotBOPSeries(imf_series,syear,eyear,stitle,country,freq,whichrs=whichrs,fs=fs)
    return ret
   
def plotFinancialAccount(imf_series='BFF_BP6_USD',syear='2000',eyear='2020',stitle='Financial account, Financial derivatives (other than reserves) and employee stock options, US Dollars',country='US',freq='Q',whichrs='JHDUSRGDPBR',fs=(30,15)):
    ret=plotBOPSeries(imf_series,syear,eyear,stitle,country,freq,whichrs=whichrs,fs=fs)
    return ret
    
def plotFinancialAccountNet(imf_series='BFD_BP6_USD',syear='2000',eyear='2020',stitle='Financial account, Net lending (+) / net borrowing (-) (balance from financial account), Direct investment, US Dollars',country='US',freq='Q',whichrs='JHDUSRGDPBR',fs=(30,15)):
    ret=plotBOPSeries(imf_series,syear,eyear,stitle,country,freq,whichrs=whichrs,fs=fs)
    return ret
    
def plotFinancialAccountOther(imf_series='BFOA_BP6_USD',syear='2000',eyear='2020',stitle='Financial account, Other investment, Net acquisition of financial assets, US Dollars',country='US',freq='Q',whichrs='JHDUSRGDPBR',fs=(30,15)):
    ret=plotBOPSeries(imf_series,syear,eyear,stitle,country,freq,whichrs=whichrs,fs=fs)
    return ret

def plotFinancialAccountPortfolio(imf_series='BFP_BP6_USD',syear='2000',eyear='2020',stitle='Financial account, Portfolio investment, US Dollars',country='US',freq='Q',whichrs='JHDUSRGDPBR',fs=(30,15)):
    ret=plotBOPSeries(imf_series,syear,eyear,stitle,country,freq,whichrs=whichrs,fs=fs)
    return ret
