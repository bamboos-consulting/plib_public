#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Economic Cycle Indicators 
#
# Module including helper functions 
#############################################################################################      

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

##############################################
# Decompose a macro series in its components
##############################################
def STLSeriesDecomp(valid_data, lbl='adj_close', freq=360, w=30, fs=(15,10),plot=True):
    from statsmodels.tsa.seasonal import STL
    import matplotlib.pyplot as plt
    
    decomposed_series = STL(valid_data[lbl],period=freq).fit()
    
    trend = decomposed_series.trend
    seasonal = decomposed_series.seasonal
    residual = decomposed_series.resid
    
    if plot:
        fig, ax = plt.subplots(5, 1, figsize=fs)

        decomposed_series.observed.plot(ax=ax[0])
        ax[0].set_title("Time series", fontsize=16)
        ax[0].set(xlabel="", ylabel="Price")

        decomposed_series.trend.plot(ax=ax[1])
        ax[1].set(xlabel="", ylabel="Trend")

        decomposed_series.seasonal.plot(ax=ax[2])
        ax[2].set(xlabel="", ylabel="Seasonal")

        decomposed_series.seasonal[0:w].plot(ax=ax[3])
        ax[3].set(xlabel="", ylabel="Seasonal window="+str(w))

        decomposed_series.resid.plot(ax=ax[4])
        ax[4].set(xlabel="Date", ylabel="Residual")
    if plot:
        import Plib.Plotting.Plots as pl
        pl.customize_grid(ax[0],x=2,y=2)
        pl.customize_grid(ax[1],x=2,y=2)
        pl.customize_grid(ax[2],x=2,y=2)
        pl.customize_grid(ax[3],x=2,y=2)
        pl.customize_grid(ax[4],x=2,y=2)
        plt.tight_layout()
    
    return {'STL_trend':trend,'STL_cycle':seasonal,'STL_residual':residual}

##############################################
# Decompose a macro series in its components
# using Hodrick-Prescott
##############################################
def HodrickPrescottSeriesDecomp(dta,lbl='realgdp',freq='Q',plot=True,fs=(15, 10)):
    import statsmodels.api as sm
    import matplotlib.pyplot as plt
    
    #Best for trend-stationary series
    if freq=='Q':
        lambda_=1600
    elif freq=='Y':
        lambda_=6.25 #(1600/4**4) 
    elif freq=='M':
        lambda_=129600 #(1600*3**4)
    
    #Hodrick-Prescott
    cycle, trend = sm.tsa.filters.hpfilter(dta[lbl], lambda_)
    decomposed_series = dta[[lbl]]
    
    if plot:
        fig, ax = plt.subplots(3, 1, figsize=fs)

        decomposed_series.plot(ax=ax[0])
        ax[0].set_title("Time series", fontsize=16)
        ax[0].set(xlabel="", ylabel="Series")

        trend.plot(ax=ax[1])
        ax[1].set(xlabel="", ylabel="Trend Hodrick-Prescott")

        cycle.plot(ax=ax[2])
        ax[2].set(xlabel="", ylabel="Cycle Hodrick-Prescott")
    if plot:
        import Plib.Plotting.Plots as pl
        pl.customize_grid(ax[0],x=2,y=2)
        pl.customize_grid(ax[1],x=2,y=2)
        pl.customize_grid(ax[2],x=2,y=2)
        plt.tight_layout()
    
    return {'HP_trend':trend,'HP_cycle':cycle}

##############################################
# Decompose a macro series in its components
# using Christiano-Fitzgerald and Baxter-King
##############################################
def CFBKSeriesDecomp(dta,lbl='infl',freq='Q',plot=True,fs=(15, 10)):
    import statsmodels.api as sm
    import matplotlib.pyplot as plt
    
    #Best for stationary series
    
    #Christiano Fitzgerald asymmetric random walk filter
    cycle, trend = sm.tsa.filters.cffilter(dta[[lbl]],low=6, high=32, drift=True)
    #Baxter-King filter
    cycles = sm.tsa.filters.bkfilter(dta[[lbl]], low=6, high=32, K=12)
    decomposed_series = dta[[lbl]]
    
    if plot:
        fig, ax = plt.subplots(4, 1, figsize=fs)

        decomposed_series.plot(ax=ax[0])
        ax[0].set_title("Time series", fontsize=16)
        ax[0].set(xlabel="", ylabel="Series")

        trend.plot(ax=ax[1])
        ax[1].set(xlabel="", ylabel="Trend")

        cycle.plot(ax=ax[2])
        ax[2].set(xlabel="", ylabel="Cycle Christiano-Fitzgerald")

        cycles.plot(ax=ax[3])
        ax[3].set(xlabel="", ylabel="Cycle Baxter-King")
    if plot:
        import Plib.Plotting.Plots as pl
        pl.customize_grid(ax[0],x=2,y=2)
        pl.customize_grid(ax[1],x=2,y=2)
        pl.customize_grid(ax[2],x=2,y=2)
        pl.customize_grid(ax[3],x=2,y=2)
        plt.tight_layout()
    
    return {'CF_trend':trend,'CF_cycle':cycle,'BK_cycle':cycles}

##############################################
# Helper functions
##############################################         
def getRecessions(which='JHDUSRGDPBR'):
    import Plib.DataFarm.Fred as F
    import pandas as pd
    
    #USRECP->For this time series, the recession begins the first day of the period of the peak and ends on the last day of the period before the trough.
    #USREC->For this time series, the recession begins the first day of the period following a peak and ends on the last day of the period of the trough.
    #USRECM->For this time series, the recession begins midpoint of the period of the peak and ends midpoint of the period of the trough.
    
    #Default: NBER JHDUSRGDPBR series
    #when the GDP-based recession indicator index rises above 67%, the economy is determined to be in a recession
    #The next time the GDP-based recession indicator index falls below 33%, the recession is determined to be over
    
    f=F.getFred()
    rcs=f.get_series(which, observation_start='1860-01-01', observation_end='2022-09-01')
    rcs.name='rcs'
    rcs=pd.DataFrame(rcs)
    l=[]
    sd='';ed=''
    for i in range(1,len(rcs)-1):
        pc=rcs.rcs.iloc[i-1]
        cc=rcs.rcs.iloc[i]
        nc=rcs.rcs.iloc[i+1]

        if cc==1 and pc==0 and nc==1:
            sd=str(rcs.index[i].date())
        if cc==1 and pc==1 and nc==0:
            ed=str(rcs.index[i].date())
        if sd!='' and ed!='':
            l.append((sd,ed))
            sd='';ed=''
    return l

def addRec(ax, df, d, color='y', alpha=0.25, lw=0):
    import pandas as pd
    dmin=str(df.index.min().date())
    dmax=str(df.index.max().date())

    if pd.to_datetime(d[0]) >= pd.to_datetime(dmin) and pd.to_datetime(d[1]) <= pd.to_datetime(dmax):
        ax.axvspan(d[0], d[1], color=color, alpha=alpha, lw=lw)
    if pd.to_datetime(d[0]) < pd.to_datetime(dmin) and pd.to_datetime(d[1]) <= pd.to_datetime(dmax) and pd.to_datetime(d[1]) > pd.to_datetime(dmin):
        ax.axvspan(dmin, d[1], color=color, alpha=alpha, lw=lw)
    if pd.to_datetime(d[0]) >= pd.to_datetime(dmin) and pd.to_datetime(d[1]) > pd.to_datetime(dmax) and pd.to_datetime(d[0]) < pd.to_datetime(dmax):
        ax.axvspan(d[0], dmax, color=color, alpha=alpha, lw=lw)
        
##############################################
# Download and plots Business Cycle Components
##############################################
def cycleComponents(start_d,end_d,whichrs='JHDUSRGDPBR', fs=(20, 20)):
    
    def plotGrid(df1,df2,df3,df4,tl,d1,d2,recess,thresh,fs):
        import numpy as np
        import pandas as pd
        import math
        import datetime
        import matplotlib.pyplot as plt
        from pandas.plotting import register_matplotlib_converters
        register_matplotlib_converters()
        import matplotlib.pyplot as plt
        from matplotlib.gridspec import GridSpec
        #from IPython.core.pylabtools import figsize
        
        fig = plt.figure(figsize=fs,clear=True,constrained_layout=False)      
    
        gs = GridSpec(6, 11, figure=fig)
    
        d=tl[0]
        ax1 = plt.subplot(gs.new_subplotspec((0, 0), colspan=10))
        ax1.plot(df1.iloc[:,0],color='g',label=d[0])
        if thresh[0] != 0:
            ax1.axhline(y = thresh[0], color = 'r', linestyle = ':')
        ax1.legend(loc='upper left')
        ax1.title.set_text(d[2])
    
        d=tl[1]
        ax2 = plt.subplot(gs.new_subplotspec((1, 0), colspan=10))
        ax2.plot(df2.iloc[:,0],color='g',label=d[0])
        if thresh[1] != 0:
            ax2.axhline(y = thresh[1], color = 'r', linestyle = ':')
        ax2.legend(loc='upper left')
        ax2.title.set_text(d[2])
    
        d=tl[2]
        ax3 = plt.subplot(gs.new_subplotspec((2, 0), colspan=10))
        ax3.plot(df3.iloc[:,0],color='g',label=d[0])
        if thresh[2] != 0:
            ax3.axhline(y = thresh[2], color = 'r', linestyle = ':')
        ax3.legend(loc='upper left')
        ax3.title.set_text(d[2])
 
        d=tl[3]
        ax4 = plt.subplot(gs.new_subplotspec((3, 0), colspan=10))
        ax4.plot(df4.iloc[:,0],color='g',label=d[0])
        if thresh[3] != 0:
            ax4.axhline(y = thresh[3], color = 'r', linestyle = ':')
        ax4.legend(loc='upper right')
        ax4.title.set_text(d[2])

        plt.subplots_adjust(hspace = 0.15, wspace=0)
    
        title='Business Cycle Components\nFrom ' + d1 + ' to ' + d2 + '\n'
        fig.suptitle(title, fontsize=16, y=0.93)
    
        for i in range(len(recess)):
            d = recess[i]
            addRec(ax1, df1, d, color='y', alpha=0.25, lw=0)
            addRec(ax2, df2, d, color='y', alpha=0.25, lw=0)
            addRec(ax3, df3, d, color='y', alpha=0.25, lw=0)
            addRec(ax4, df4, d, color='y', alpha=0.25, lw=0)        

        import Plib.Plotting.Plots as pl
        pl.customize_grid(ax1,x=2,y=2)
        pl.customize_grid(ax2,x=2,y=2)
        pl.customize_grid(ax3,x=2,y=2)
        pl.customize_grid(ax4,x=2,y=2)
        #fig.savefig('Cycles' + d2 + '.png', dpi=fig.dpi, bbox_inches='tight', pad_inches=0.3)
        
        #plt.tight_layout()
        return fig
    
    import Plib.DataFarm.Fred as f
    import numpy as np
    import pandas as pd
    import datetime
    import math

    fred = f.getFred()
    #fred = Fred(fred_api)
    #start_d='1999-01-01'
    #end_d='2019-10-10'

    recessions=getRecessions(whichrs)
    
    titleLabels=[('GDP','','GDP'),
                 ('Inventories',' ','Manufacturers Inventories'),
                 ('Housing','','Transactions House Price Index'),
                 ('fixed investment',' ','Nonfinancial Corporate Capital Expenditures')]

    thresholds=[0,0,0,-0.02]
    
    #Gross Domestic Product (A191RP1Q027SBEA)
    s = fred.get_series('A191RP1Q027SBEA', observation_start=start_d, observation_end=end_d)
    df1 = {}
    df1['GDP'] =s
    df1 = pd.DataFrame(df1)

    #Manufacturers Inventories change (MNFCTRMPCIMSA)
    k = fred.get_series('MNFCTRMPCIMSA', observation_start=start_d, observation_end=end_d)
    df2 = {}
    df2['Inventories'] = k
    df2 = pd.DataFrame(df2).pct_change()

    #All-Transactions House Price Index for the United States (USSTHPI)
    m = fred.get_series('USSTHPI', observation_start=start_d, observation_end=end_d)
    df3 = {}
    df3['Housing'] = m
    df3 = pd.DataFrame(df3).pct_change()

    #Nonfinancial corporate business; total capital expenditures, Flow (BOGZ1FA105050005Q)
    r = fred.get_series('BOGZ1FA105050005Q', observation_start=start_d, observation_end=end_d)
    df4 = {}
    df4['Capex'] = r
    df4 = pd.DataFrame(df4).pct_change()
    
    mf=plotGrid(df1,df2,df3,df4,titleLabels,start_d,end_d,recessions,thresholds,fs)
         
##############################################
# Download and plots leading indicators
##############################################
def leadingIndicators(start_d,end_d,whichrs='JHDUSRGDPBR',fs=(20, 20)):
    
    def plotGrid(df1,df2,df3,df4,df5,df6,df7,df8,tl,d1,d2,recess):
        import numpy as np
        import pandas as pd
        import math
        import datetime
        import matplotlib.pyplot as plt
        from pandas.plotting import register_matplotlib_converters
        register_matplotlib_converters()
        import matplotlib.pyplot as plt
        from matplotlib.gridspec import GridSpec
        from IPython.core.pylabtools import figsize
    
        fig = plt.figure(figsize=fs, clear=True,constrained_layout=False)
    
        gs = GridSpec(6, 21, figure=fig)
    
        d=tl[0]
        ax1 = plt.subplot(gs.new_subplotspec((0, 0), colspan=10))
        ax2 = ax1.twinx()
        ax1.plot(df1.iloc[:,0],color='g',label=d[0])
        ax2.plot(df1.iloc[:,1],color='b',label=d[1])
        ax1.legend(loc='upper left')
        ax2.legend(loc='upper right')
        ax1.title.set_text(d[2])
    
        d=tl[1]
        ax2 = plt.subplot(gs.new_subplotspec((1, 0), colspan=10))
        ax3 = ax2.twinx()
        ax2.plot(df2.iloc[:,0],color='g',label=d[0])
        ax3.plot(df2.iloc[:,1],color='b',label=d[1])
        ax2.legend(loc='upper left')
        ax3.legend(loc='upper right')
        ax2.title.set_text(d[2])
    
        d=tl[2]
        ax3 = plt.subplot(gs.new_subplotspec((2, 0), colspan=10))
        ax4 = ax3.twinx()
        ax3.plot(df3.iloc[:,0],color='g',label=d[0])
        ax4.plot(df3.iloc[:,1],color='b',label=d[1])
        ax3.legend(loc='upper left')
        ax4.legend(loc='upper right')
        ax3.title.set_text(d[2])
 
        d=tl[3]
        ax4 = plt.subplot(gs.new_subplotspec((3, 0), colspan=10))
        ax4.plot(df4.iloc[:,0],color='g',label=d[0])
        ax4.legend(loc='upper right')
        ax4.title.set_text(d[2])

        d=tl[4]
        ax5 = plt.subplot(gs.new_subplotspec((0, 11), colspan=10))
        ax5.plot(df5.iloc[:,0],color='g',label=d[0])
        ax5.legend(loc='upper right')
        ax5.title.set_text(d[2])
    
        d=tl[5]
        ax6 = plt.subplot(gs.new_subplotspec((1, 11), colspan=10))
        ax6.plot(df6.iloc[:,0],color='g',label=d[0])
        ax6.legend(loc='upper right')
        ax6.title.set_text(d[2])
    
        d=tl[6]
        ax7 = plt.subplot(gs.new_subplotspec((2, 11), colspan=10))
        ax7.plot(df7.iloc[:,0],color='g',label=d[0])
        ax7.legend(loc='upper right')
        ax7.title.set_text(d[2])
    
        d=tl[7]
        ax8 = plt.subplot(gs.new_subplotspec((3, 11), colspan=10))
        ax8.plot(df8.iloc[:,0],color='g',label=d[0])
        ax8.legend(loc='upper right')
        ax8.title.set_text(d[2])
    
        plt.subplots_adjust(hspace = 0.8,wspace=1.0)
    
        title='Leading Indicators\nFrom ' + d1 + ' to ' + d2 + '\n'
        fig.suptitle(title, fontsize=16)
    
        for i in range(len(recess)):
            d = recess[i]
            addRec(ax1, df1, d, color='y', alpha=0.25, lw=0)
            addRec(ax2, df2, d, color='y', alpha=0.25, lw=0)
            addRec(ax3, df3, d, color='y', alpha=0.25, lw=0)
            addRec(ax4, df4, d, color='y', alpha=0.25, lw=0)
            addRec(ax5, df5, d, color='y', alpha=0.25, lw=0)
            addRec(ax6, df6, d, color='y', alpha=0.25, lw=0)
            addRec(ax7, df7, d, color='y', alpha=0.25, lw=0)
            addRec(ax8, df8, d, color='y', alpha=0.25, lw=0)
        
        import Plib.Plotting.Plots as pl
        pl.customize_grid(ax1,x=2,y=2)
        pl.customize_grid(ax2,x=2,y=2)
        pl.customize_grid(ax3,x=2,y=2)
        pl.customize_grid(ax4,x=2,y=2)
        pl.customize_grid(ax5,x=2,y=2)
        pl.customize_grid(ax6,x=2,y=2)
        pl.customize_grid(ax7,x=2,y=2)
        pl.customize_grid(ax8,x=2,y=2)
        #fig.savefig('LIndic' + d2 + '.png', dpi=fig.dpi, bbox_inches='tight', pad_inches=0.3)
    
        #print(mcorr)
        #print(lreturns.iloc[:,0].rolling(window=20).corr(lreturns.iloc[:,1]).mean())
        #print(lreturns.iloc[:,0].rolling(window=60).corr(lreturns.iloc[:,1]).mean())
        #print(lreturns.iloc[:,0].rolling(window=252*4).corr(lreturns.iloc[:,1]).mean())
    
        return fig
        
    import Plib.DataFarm.Fred as f
    import numpy as np
    import pandas as pd
    import datetime
    import math

    fred = f.getFred()
    
    recessions=getRecessions(whichrs)

    titleLabels=[('Manufacturing','Construction','Weekly Hours Worked'),
             ('Manufacturing','Construction','Total Employment'),
             ('Current New Orders','Future New Orders','New Orders Diffusion Index - New York'),
             ('Consumer Sentiment',' ','Consumer Sentiment Survey'),
             ('Interest Rate',' ','Interest Rate Spread 10YT-FF'),
             ('New Orders Durable',' ','New Orders Durable minus Defense'),
             ('Credit',' ','National Credit Conditions'),
             ('Housing',' ','New Private Housing by Building Permits')]


    s = fred.get_series('AWHMAN', observation_start=start_d, observation_end=end_d)
    t = fred.get_series('CES2000000007', observation_start=start_d, observation_end=end_d)

    df1 = {}
    df1['Manufacturing'] =s
    df1['Construction'] =t
    df1 = pd.DataFrame(df1)

    s = fred.get_series('MANEMP', observation_start=start_d, observation_end=end_d)
    t = fred.get_series('USCONS', observation_start=start_d, observation_end=end_d)

    df2 = {}
    df2['Manufacturing'] =s
    df2['Construction'] =t
    df2 = pd.DataFrame(df2)

    #Current New Orders; Diffusion Index for New York
    m = fred.get_series('NOCDISA066MSFRBNY', observation_start=start_d, observation_end=end_d)
    #Future New Orders; Diffusion Index for New York
    n = fred.get_series('NOFDISA066MSFRBNY', observation_start=start_d, observation_end=end_d)
    df3 = {}
    df3['Current New Orders'] = m
    df3['Future New Orders'] = n
    df3 = pd.DataFrame(df3)

    #University of Michigan: Consumer Sentiment
    k = fred.get_series('UMCSENT', observation_start=start_d, observation_end=end_d)
    df4 = {}
    df4['Consumer Sentiment'] = k
    df4 = pd.DataFrame(df4)

    #10-Year Treasury Constant Maturity Minus Federal Funds Rate (T10YFF)
    l = fred.get_series('T10YFF', observation_start=start_d, observation_end=end_d)
    df5 = {}
    df5['Interest Rates Spread'] = l
    df5 = pd.DataFrame(df5)

    #Value of Manufacturers' New Orders for Consumer Goods: Durable Goods Excluding Defense Industries (ADXDNO)
    p = fred.get_series('ADXDNO', observation_start=start_d, observation_end=end_d)
    df6 = {}
    df6['New Orders Durable'] = p
    df6 = pd.DataFrame(df6)

    #Chicago Fed National Financial Conditions Credit Subindex (NFCICREDIT)
    q = fred.get_series('NFCICREDIT', observation_start=start_d, observation_end=end_d)

    df7 = {}
    df7['Financial Conditions Credit'] = q
    df7 = pd.DataFrame(df7)

    #New Private Housing Units Authorized by Building Permits (PERMIT)
    r = fred.get_series('PERMIT', observation_start=start_d, observation_end=end_d)

    df8 = {}
    df8['Housing'] = r
    df8 = pd.DataFrame(df8)

    #Real Gross Domestic Product (A191RL1Q225SBEA)
    s = fred.get_series('A191RL1Q225SBEA', observation_start=start_d, observation_end=end_d)
    df9 = {}
    df9['GDP'] = s
    df9 = pd.DataFrame(df9)
    mf=plotGrid(df1,df2,df3,df4,df5,df6,df7,df8,titleLabels,start_d,end_d,recessions)

##############################################
# Download and plots coincident indicators
##############################################
def coincidentIndicators(start_d,end_d,whichrs='JHDUSRGDPBR',fs=(15, 15)):
    #start_d='2018-01-01'
    #end_d='2019-10-10'
    
    def plotGrid(df1,df2,df3,df4,df5,tl,d1,d2,recess):
        import numpy as np
        import pandas as pd
        import math
        import datetime
        import matplotlib.pyplot as plt
        from pandas.plotting import register_matplotlib_converters
        register_matplotlib_converters()
        import matplotlib.pyplot as plt
        from matplotlib.gridspec import GridSpec
        from IPython.core.pylabtools import figsize
        
        fig = plt.figure(figsize=fs,clear=True,constrained_layout=False)
    
        gs = GridSpec(6, 11, figure=fig)
    
        d=tl[0]
        ax1 = plt.subplot(gs.new_subplotspec((0, 0), colspan=10))
        ax1.plot(df1.iloc[:,0],color='g',label=d[0])
        ax1.legend(loc='upper left')
        ax1.title.set_text(d[2])
    
        d=tl[1]
        ax2 = plt.subplot(gs.new_subplotspec((1, 0), colspan=10))
        ax2.plot(df2.iloc[:,0],color='g',label=d[0])
        ax2.legend(loc='upper left')
        ax2.title.set_text(d[2])
    
        d=tl[2]
        ax3 = plt.subplot(gs.new_subplotspec((2, 0), colspan=10))
        ax4 = ax3.twinx()
        ax3.plot(df3.iloc[:,0],color='g',label=d[0])
        ax4.plot(df3.iloc[:,1],color='b',label=d[1])
        ax3.legend(loc='upper left')
        ax4.legend(loc='upper right')
        ax3.title.set_text(d[2])
 
        d=tl[3]
        ax4 = plt.subplot(gs.new_subplotspec((3, 0), colspan=10))
        ax4.plot(df4.iloc[:,0],color='g',label=d[0])
        ax4.legend(loc='upper right')
        ax4.title.set_text(d[2])

        d=tl[4]
        ax5 = plt.subplot(gs.new_subplotspec((4, 0), colspan=10))
        ax5.plot(df5.iloc[:,0],color='g',label=d[0])
        ax5.legend(loc='upper right')
        ax5.title.set_text(d[2])

        plt.subplots_adjust(hspace = 0.8,wspace=1.0)
    
        title='Coincident Indicators\nFrom ' + d1 + ' to ' + d2 + '\n'
        fig.suptitle(title, fontsize=16)
    
        for i in range(len(recess)):
            d = recess[i]
            addRec(ax1, df1, d, color='y', alpha=0.25, lw=0)
            addRec(ax2, df2, d, color='y', alpha=0.25, lw=0)
            addRec(ax3, df3, d, color='y', alpha=0.25, lw=0)
            addRec(ax4, df4, d, color='y', alpha=0.25, lw=0)
            addRec(ax5, df5, d, color='y', alpha=0.25, lw=0)

        
        import Plib.Plotting.Plots as pl
        pl.customize_grid(ax1,x=2,y=2)
        pl.customize_grid(ax2,x=2,y=2)
        pl.customize_grid(ax3,x=2,y=2)
        pl.customize_grid(ax4,x=2,y=2)
        pl.customize_grid(ax5,x=2,y=2)
        #fig.savefig('LIndic' + d2 + '.png', dpi=fig.dpi, bbox_inches='tight', pad_inches=0.3)
    
        #print(mcorr)
        #print(lreturns.iloc[:,0].rolling(window=20).corr(lreturns.iloc[:,1]).mean())
        #print(lreturns.iloc[:,0].rolling(window=60).corr(lreturns.iloc[:,1]).mean())
        #print(lreturns.iloc[:,0].rolling(window=252*4).corr(lreturns.iloc[:,1]).mean())
    
        return fig
    
    import Plib.DataFarm.Fred as f
    import numpy as np
    import pandas as pd
    import datetime
    import math

    fred = f.getFred()
    
    recessions=getRecessions(whichrs)

    titleLabels=[('Total Retail Sales','','Volume of Total Retail Trade Sales'),
    ('Payrolls','','Total Nonfarm Payrolls'),
    ('Personal Income','Consumption Expenditure','Personal Income - Consumer Expenditure'),
    ('Transfer Payments',' ','Government Transfer Payments'),
    ('Industrial Production',' ','Industrial Production Index')]

    #Volume of Total Retail Trade sales for the United States (SLRTTO01USQ661S)
    s = fred.get_series('SLRTTO01USQ661S', observation_start=start_d, observation_end=end_d)
    #All Employees: Total Nonfarm Payrolls (PAYEMS)
    t = fred.get_series('PAYEMS', observation_start=start_d, observation_end=end_d)

    df1 = {}
    df1['Retail'] =s
    df1 = pd.DataFrame(df1)

    df2 = {}
    df2['Payrolls'] = t
    df2 = pd.DataFrame(df2)

    #Personal Income (PI)
    m = fred.get_series('PI', observation_start=start_d, observation_end=end_d)
    #Personal Consumption Expenditures (PCE)
    n = fred.get_series('PCE', observation_start=start_d, observation_end=end_d)

    df3 = {}
    df3['Personal Income'] = m
    df3['Consumer Expenditure'] = n
    df3 = pd.DataFrame(df3)

    #Federal government current transfer payments: Government social benefits: To persons (B087RC1Q027SBEA)
    k = fred.get_series('B087RC1Q027SBEA', observation_start=start_d, observation_end=end_d)

    df4 = {}
    df4['Transfer Payments'] = k
    df4 = pd.DataFrame(df4)

    #Industrial Production Index (INDPRO)
    l = fred.get_series('INDPRO', observation_start=start_d, observation_end=end_d)

    df5 = {}
    df5['Industrial Production'] = l
    df5 = pd.DataFrame(df5)

    mf=plotGrid(df1,df2,df3,df4,df5,titleLabels,start_d,end_d,recessions)
      
##############################################
# Download and plots lagging indicators
##############################################
def laggingIndicators(start_d,end_d,whichrs='JHDUSRGDPBR',fs=(20, 20)):
    
    def plotGrid(df1,df2,df3,df4,df5,df6,tl,d1,d2,recess):
        import numpy as np
        import pandas as pd
        import math
        import datetime
        import matplotlib.pyplot as plt
        from pandas.plotting import register_matplotlib_converters
        register_matplotlib_converters()
        import matplotlib.pyplot as plt
        from matplotlib.gridspec import GridSpec
        from IPython.core.pylabtools import figsize
        
        fig = plt.figure(figsize=fs,clear=True,constrained_layout=False)
    
        gs = GridSpec(6, 21, figure=fig)
    
        d=tl[0]
        ax1 = plt.subplot(gs.new_subplotspec((0, 0), colspan=10))
        ax2 = ax1.twinx()
        ax1.plot(df1.iloc[:,0],color='g',label=d[0])
        ax2.plot(df1.iloc[:,1],color='b',label=d[1])
        ax1.legend(loc='upper left')
        ax2.legend(loc='upper right')
        ax1.title.set_text(d[2])
    
        d=tl[1]
        ax2 = plt.subplot(gs.new_subplotspec((1, 0), colspan=10))
        ax2.plot(df2.iloc[:,0],color='g',label=d[0])
        ax2.legend(loc='upper left')
        ax2.title.set_text(d[2])
    
        d=tl[2]
        ax3 = plt.subplot(gs.new_subplotspec((2, 0), colspan=10))
        ax4 = ax3.twinx()
        ax3.plot(df3.iloc[:,0],color='g',label=d[0])
        ax4.plot(df1.iloc[:,1],color='b',label=d[1])
        ax3.legend(loc='upper left')
        ax4.legend(loc='upper right')
        ax3.title.set_text(d[2])
 
        d=tl[3]
        ax4 = plt.subplot(gs.new_subplotspec((2, 11), colspan=10))
        ax4.plot(df4.iloc[:,0],color='g',label=d[0])
        ax4.legend(loc='upper right')
        ax4.title.set_text(d[2])

        d=tl[4]
        ax5 = plt.subplot(gs.new_subplotspec((0, 11), colspan=10))
        ax6 = ax5.twinx()
        ax5.plot(df5.iloc[:,0],color='g',label=d[0])
        ax6.plot(df1.iloc[:,1],color='b',label=d[1])
        ax5.legend(loc='upper right')
        ax6.legend(loc='upper right')
        ax5.title.set_text(d[2])
    
        d=tl[5]
        ax6 = plt.subplot(gs.new_subplotspec((1, 11), colspan=10))
        ax6.plot(df6.iloc[:,0],color='g',label=d[0])
        ax6.legend(loc='upper right')
        ax6.title.set_text(d[2])
     
        plt.subplots_adjust(hspace = 0.8,wspace=1.0)
    
        title='Lagging Indicators\nFrom ' + d1 + ' to ' + d2 + '\n'
        fig.suptitle(title, fontsize=16)
    
        for i in range(len(recess)):
            d = recess[i]
            addRec(ax1, df1, d, color='y', alpha=0.25, lw=0)
            addRec(ax2, df2, d, color='y', alpha=0.25, lw=0)
            addRec(ax3, df3, d, color='y', alpha=0.25, lw=0)
            addRec(ax4, df4, d, color='y', alpha=0.25, lw=0)
            addRec(ax5, df5, d, color='y', alpha=0.25, lw=0)
            addRec(ax6, df6, d, color='y', alpha=0.25, lw=0)
        
        import Plib.Plotting.Plots as pl
        pl.customize_grid(ax1,x=2,y=2)
        pl.customize_grid(ax2,x=2,y=2)
        pl.customize_grid(ax3,x=2,y=2)
        pl.customize_grid(ax4,x=2,y=2)
        pl.customize_grid(ax5,x=2,y=2)
        pl.customize_grid(ax6,x=2,y=2)
        #fig.savefig('LIndic' + d2 + '.png', dpi=fig.dpi, bbox_inches='tight', pad_inches=0.3)
    
        #print(mcorr)
        #print(lreturns.iloc[:,0].rolling(window=20).corr(lreturns.iloc[:,1]).mean())
        #print(lreturns.iloc[:,0].rolling(window=60).corr(lreturns.iloc[:,1]).mean())
        #print(lreturns.iloc[:,0].rolling(window=252*4).corr(lreturns.iloc[:,1]).mean())
    
        return fig
    
    import Plib.DataFarm.Fred as f
    import numpy as np
    import pandas as pd
    import datetime
    import math

    fred = f.getFred()
    
    recessions=getRecessions(whichrs) 

    titleLabels=[('Prime Loan','Comm/Ind Loans','Bank Prime Loan - Volume Commercial/Industrial Loans'),
    ('Ratio Consumer Credit to Income','','Ratio of Consumer Credit to Personal Income'),
    ('Consumer Price Index','Housing','CP Index Services Less Housing - Housing Transaction Index'),
    ('Inventories to Sales',' ','Inventories to Sales Ratio'),
    ('Consumer Credit','Commercial Loans','Consumer Credit - Industrial and Commercial Loans'),
    ('Duration of Unemployment',' ','Average Duration of Unemployment')]

    #Bank Prime Loan Rate (MPRIME)
    s = fred.get_series('MPRIME', observation_start=start_d, observation_end=end_d)
    #Commercial and Industrial Loans, All Commercial Banks (BUSLOANS)
    t = fred.get_series('BUSLOANS', observation_start=start_d, observation_end=end_d)

    df1 = {}
    df1['Prime Loans'] =s
    df1['Comm Loans'] =t
    df1 = pd.DataFrame(df1)

    #Total Consumer Credit Owned and Securitized, Outstanding (TOTALSL)
    p = fred.get_series('TOTALSL', observation_start=start_d, observation_end=end_d)
    #Personal Income (PI)
    q = fred.get_series('PI', observation_start=start_d, observation_end=end_d)
    p=s.divide(q)
    df2 = {}
    df2['Personal Income'] =p
    df2 = pd.DataFrame(df2)

    #Consumer Price Index for All Urban Consumers: Services (CUSR0000SAS)
    m = fred.get_series('CUSR0000SAS', observation_start=start_d, observation_end=end_d)
    #Consumer Price Index for All Urban Consumers: Housing (CPIHOSSL)
    x = fred.get_series('CPIHOSSL', observation_start=start_d, observation_end=end_d)
    #All-Transactions House Price Index for the United States (USSTHPI)
    j = fred.get_series('USSTHPI', observation_start=start_d, observation_end=end_d)
    y=m.subtract(x)
    df3 = {}
    df3['CPE'] = y
    df3['Housing'] = j
    df3 = pd.DataFrame(df3)

    #Total Business: Inventories to Sales Ratio (ISRATIO)
    n = fred.get_series('ISRATIO', observation_start=start_d, observation_end=end_d)

    df4 = {}
    df4['InventoriesSales'] = n
    df4 = pd.DataFrame(df4)

    #Total Consumer Credit Owned and Securitized, Outstanding (TOTALSL)
    k = fred.get_series('TOTALSL', observation_start=start_d, observation_end=end_d)
    #Commercial and Industrial Loans, All Commercial Banks (BUSLOANS)
    l = fred.get_series('BUSLOANS', observation_start=start_d, observation_end=end_d)

    df5 = {}
    df5['Consumer Credit'] = k
    df5['Comm Loans'] = l
    df5 = pd.DataFrame(df5)

    #Average (Mean) Duration of Unemployment (UEMPMEAN)
    z = fred.get_series('UEMPMEAN', observation_start=start_d, observation_end=end_d)

    df6 = {}
    df6['Unempl Duration'] = z
    df6 = pd.DataFrame(df6)

    mf=plotGrid(df1,df2,df3,df4,df5,df6,titleLabels,start_d,end_d,recessions)