#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Timeseries 
#
# Module comprising helper functions to perform timeseries analysis
#############################################################################################      

from math import sqrt
from warnings import catch_warnings
from warnings import filterwarnings
from numpy import array
from ast import literal_eval
import itertools
import multiprocessing
from itertools import product
from typing import List

import numpy as np
import pandas as pd
import scipy
import math

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

DEBUG_PRINT=0

#################################################################
# gridSearch Class
# Parameters:   data: the data to be studies as df['col']
#               data_params={}, additional parameters if required
#               modelf      the function to be searched
#               modelfsc    the function scoring the model
#                           default mean_squared_error
#               n_test      the splits for the train/test data
#                           more accurate if higher
#               parallel    execution with multi cpu
#               nproc       n. processors
#               nres        top results to output
#################################################################    
class gridSearch():
    
    def __init__(self,data,data_params={},modelf=None,modelsc=None,lparams=[],n_test = 4,parallel=True,nproc=4,test_treshold=0.7,lbl_lparams=[]):
        self.fit_test_treshold=test_treshold
        self.nproc=nproc
        self.data=data
        self.data_params=data_params
        self.lbl_lparams=lbl_lparams
        
        self.model=modelf
        if modelsc != None:
            self.measure_rmse=modelsc
            
        # model configs
        cfg_param_list = list(itertools.product(*lparams))  
        print('Total Iterations to be Computed: ', len(cfg_param_list))
        print('')      
        # grid search
        self.scores = self.grid_search(data,data_params,cfg_param_list, n_test,parallel)
    
    def getAll(self):
        df=pd.DataFrame(self.scores)
        df.columns=['Parameters','RMSE','AIC','BIC']
        return df
    
    def getPDesc(self):
        return self.lbl_lparams
        
    def getModelParams(self,i):
        ret=[]
        if len(self.scores)>0:
            ret=literal_eval(self.scores[i][0])
        return ret
        
    @staticmethod
    def model(data,params,data_params):
        return
    
    @staticmethod
    def measure_rmse(actual, predicted):
        from sklearn.metrics import mean_squared_error

        return sqrt(mean_squared_error(actual, predicted))

    def near_train_test_split(self,data, n_test):
        #n_test=int(0.77*len(data))
        return data[:-n_test], data[-n_test:]

    def model_validation(self,data, n_test, params):
        aic=0
        bic=0
        rt_score=0
        predictions = list()
        # split dataset
        train, test = self.near_train_test_split(data, n_test)
        # seed data with training dataset
        data = [x for x in train]
        # step over each time-step in the test set
        for i in range(len(test)):
            # fit model and make forecast for data
            yhat,rtest,raic,rbic = self.model(data, params,self.data_params)
            #Check model preconditions/postconditions
            if rtest: rt_score=rt_score+1
            # store forecast in list of predictions
            predictions.append(yhat)
            # add actual observation to data for the next loop
            data.append(test[i])
            # store fit measures
            aic=aic+raic
            bic=bic+rbic
        if aic !=0: aic=aic/len(test)   #take the average fit
        if bic !=0: bic=bic/len(test)   #take the average fit
        # estimate prediction error on model preconditions/postconditions
        if rt_score > len(test)*self.fit_test_treshold:
            error = self.measure_rmse(test, predictions)
        else:
            error = None
        return error,aic,bic

    def score_model(self,data, n_test, params,data_params, debug=False):
        # score a model, return None on failure
        result = None
        aic=0
        bic=0
        # convert config to a key
        key = str(params)
        # show all warnings and fail on exception if debugging
        if debug:
            result,aic,bic = self.model_validation(data, n_test, params)
        else:
            # one failure during model validation suggests an unstable config
            try:
                # never show warnings when grid searching, too noisy
                with catch_warnings():
                    filterwarnings("ignore")
                    result,aic,bic = self.model_validation(data, n_test, params)
            except:
                error = None
        # check for an interesting result
        if DEBUG_PRINT > 1:
            if result is not None:
                print(' > Model[%s] %.3f' % (key, result))
        return (key, result,aic,bic)

    def grid_search(self,data, data_params,cfg_param_list, n_test, parallel=False,debug=False):
        scores = []
        if parallel:
            args = [(data, n_test, params,data_params,debug) for params in cfg_param_list]
            with multiprocessing.Pool(processes=self.nproc) as pool:
                scores = pool.starmap(self.score_model, args)
                pool.close()
        else:
            scores = [self.score_model(data, n_test, params,data_params,debug) for params in cfg_param_list]
        # remove empty results
        scores = [r for r in scores if r[1] != None]
        # sort configs by error, asc
        scores.sort(key=lambda tup: tup[1])
        return scores

    def forecast(self,train,test,params):
        predictions = list()
        data = [x for x in train]
        # step over each time-step in the test set
        for i in range(len(test)):
            # fit model and make forecast for data
            yhat,rtest,raic,rbic = self.model(data, params,self.data_params)
            # store forecast in list of predictions
            predictions.append(yhat)
            # add actual observation to data for the next loop
            data.append(test[i])
            #if i % 500 ==0: print(len(test),i,yhat)
        ret=pd.DataFrame(predictions,index=test.index)
        ret.columns=['Prediction']
        return ret
             
#################################################################
# Diagnostic Plots
#################################################################    
def plotPrediction(train_data,valid_data, title='',lblplot='adj_close',lblpred='SES', lblmodel='',predict=False, fitted=False, fs=(12,8)):
    import matplotlib.pyplot as pll
    import Plib.Plotting.Plots as pl
    #plt.figure(figsize=fs)
    fig, plt = pll.subplots(figsize=fs)
    
    #In case the two dataframe have voids in between
    #train_data = train_data.reindex(pd.date_range(str(train_data.index.date.min()), str(valid_data.index.date.min()))).ffill()

    plt.plot(train_data.index, train_data[lblplot], label='train_data')
    plt.plot(valid_data.index,valid_data[lblplot], label='valid_data')
    if predict:
        plt.plot(valid_data.index,valid_data[lblpred], label=lblmodel)
    if fitted:
        plt.plot(train_data.index,train_data['fitted'], label='fitted')
    pll.legend(loc='best')
    pll.title(title)
    pl.customize_grid(plt,x=2,y=2)
    pll.show()
    
def normTest(data,title='', lblrets='log_returns',lblclose='Adj Close', fs=(15, 4), plot=True):
    import matplotlib.dates as mdates
    import matplotlib as mpl
    from matplotlib import gridspec
    import matplotlib.pyplot as plt    
    from scipy.stats import norm
    from scipy.stats import lognorm
    from statsmodels.stats.stattools import jarque_bera
    import scipy.stats as scs
    import statsmodels.api as sm
    import Plib.Plotting.Plots as pl
    
    #df2=data.copy()
    df2=data[[lblclose]]
    df2[lblrets]= np.log(df2[lblclose] / df2[lblclose].shift(1))
    df2.columns=[lblclose,lblrets]

    #df['Close']=df['Adj Close']
    
    df2[lblrets]=df2[lblrets].bfill().ffill()
    df2.dropna(axis='rows', how='any', inplace=True)
    jb_test_stat, pvalue, _, _ = jarque_bera(df2[lblrets])
    
    sepI='------------------------ Summary Statistics for '+ title +' Log Returns ------------------------'
    print(sepI)
    print(f'Range of dates: {min(df2.index.date)} to {max(df2.index.date)}')
    print(f'Number of observations: {df2.shape[0]}')
    print(f"Mean: {df2['log_returns'].mean():.4f}")
    print(f"Median: {df2['log_returns'].median():.4f}")
    print(f"Min: {df2['log_returns'].min():.4f}")
    print(f"Max: {df2['log_returns'].max():.4f}")
    print(f"Standard Deviation: {df2['log_returns'].std():.4f}")
    print(f"Skewness: {df2['log_returns'].skew():.4f}")
    print(f"Kurtosis: {df2['log_returns'].kurtosis():.4f}") 
    print(f"Jarque-Bera statistic: {jb_test_stat:.2f} with p-value: {pvalue:.2f}")
    ci=0.05
    if pvalue < ci:
        print("Returns are likely not normally distributed.")
    else:
        print("Returns are likely normally distributed.")
    rt=tableSesonality(df2[lblrets])
    sepF=''
    for i in range(0,len(sepI)): sepF=sepF+'-'
    print(sepF)
    
    if plot:
        fig = plt.figure(figsize=fs)
        fig.tight_layout()

        #Prices
        top = plt.subplot2grid((6, 6), (0, 0), rowspan=6, colspan=4)
        top.plot(df2.index, df2[lblclose], color='black',label='O-U Process')
        top.xaxis_date()
        top.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
        top.set_title("Prices", fontsize=15)
        plt.setp(top.xaxis.get_majorticklabels(), rotation=20)

        #Lognormal Fit
        try:
            stop = plt.subplot2grid((6,6), (0, 4), rowspan=6, colspan=3, sharey=top)            
            n, bins, patches = stop.hist(df2[lblclose], bins=50,density=1,orientation=u'horizontal',alpha = 0.4,label='Histogram of Prices')
            shape, loc, scale = lognorm.fit(df2[lblclose], floc=0) #x0 is rawdata x-axis
            x = np.linspace(bins.min(), bins.max(), num=400) # values for x-axis
            y = lognorm.pdf(x, shape, loc=0, scale=scale) # probability distribution
            stop.plot(y, x, 'r--',alpha = 0.4,label=f"LogNorm({shape:.4f}, {scale:.5f})",color='green')
            l=stop.legend(loc='best')
            stop.yaxis.tick_right()
            stop.set_title("LogNormal Fit", fontsize=15)
        except:
            pass;
            
        fig = plt.figure(figsize=fs)
        fig.tight_layout()

        #LogReturns
        top2 = plt.subplot2grid((6, 6), (0, 0), rowspan=6, colspan=4)
        top2.plot(df2.index,df2[lblrets],color='black',label='log returns (%)')
        top2.xaxis_date()
        top2.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.2f}'))
        top2.set_title("Log Returns", fontsize=15)
        plt.setp(top2.xaxis.get_majorticklabels(), rotation=20)

        #Normal Fit
        log_returns_range = np.linspace(min(df2[lblrets]), max(df2[lblrets]), num=1000)
        μ = df2[lblrets].mean()
        σ = df2[lblrets].std()
        norm_pdf = scs.norm.pdf(log_returns_range, loc=μ, scale=σ)
        stop2 = plt.subplot2grid((6,6), (0, 4), rowspan=6, colspan=3, sharey=top2)            
        n, bins, patches = stop2.hist(df2[lblrets], bins=50,density=1,orientation=u'horizontal',alpha = 0.4,label='Histogram of LogReturns')
        stop2.plot(norm_pdf,log_returns_range , 'r--',alpha = 0.4,label=f"N({μ:.4f}, {σ**2:.5f})",color='green')
        stop2.yaxis.tick_right()
        l=stop2.legend(loc='best')
        stop2.set_title("Normal Fit", fontsize=15)


        fig = plt.figure(figsize=fs)
        fig.tight_layout()

        #Volatility
        df2['ma_std_252'] = df2[lblrets].rolling(window=252).std()
        df2['ma_std_21'] = df2[lblrets].rolling(window=21).std()
        top3 = plt.subplot2grid((6, 6), (0, 0), rowspan=6, colspan=4)
        top3.plot(df2.index, df2['ma_std_21'], color='green',label="21-day MA of volatility")
        top3.plot(df2.index, df2['ma_std_252'], color='red',label="252-day MA of volatility")
        top3.xaxis_date()
        top3.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.2f}'))
        top3.set_title("Moving volatility", fontsize=15)
        plt.setp(top3.xaxis.get_majorticklabels(), rotation=20)

        #QQplot
        stop3 = plt.subplot2grid((6,6), (0, 4), rowspan=6, colspan=3)#, sharey=top3)            
        qq_plot = sm.qqplot(df2[lblrets], line='s', ax=stop3)
        stop3.set_title("Q-Q plot", fontsize=15)
        stop3.yaxis.tick_right()
        
        pl.customize_grid(top)
        pl.customize_grid(top2)
        pl.customize_grid(top3)
        pl.customize_grid(stop3)
        pl.customize_grid(stop)
        pl.customize_grid(stop2)
        
    return rt

def tableSesonality(data_col,dec=4,key='mean'):
    from IPython.display import display_html 
    
    key_l=['count','mean','std','min','25%','50%','75%','max']
    if key !='all': key_l=[key] 
        
    df3=data_col
    weekly=pd.DataFrame(df3.groupby([df3.index.to_series().dt.day_name()]).describe()).round(dec)
    monthly=pd.DataFrame(df3.groupby([df3.index.strftime('%B')]).describe()).round(dec)
    quarter=df3.resample('Q', convention='end').agg('mean')
    quarter=pd.DataFrame(quarter.groupby([quarter.index.strftime('%B')]).describe()).round(dec)
    semester=df3.resample('2Q', convention='end',closed='left').last()
    semester=pd.DataFrame(semester.groupby([semester.index.strftime('%B')]).describe()).round(dec)

    df1_styler = weekly[key_l].style.set_table_attributes("style='display:inline'").set_caption('BY WEEK')
    df2_styler = monthly[key_l].style.set_table_attributes("style='display:inline'").set_caption('BY MONTH')
    df3_styler = quarter[key_l].style.set_table_attributes("style='display:inline'").set_caption('BY QUARTER')
    df4_styler = semester[key_l].style.set_table_attributes("style='display:inline'").set_caption('BY SEMESTER')
    
    space = "\xa0" * 10
    display_html(df1_styler._repr_html_()+ space + df2_styler._repr_html_() + space +
                 df3_styler._repr_html_() + space +
                 df4_styler._repr_html_(), raw=True)
    return weekly, monthly,quarter,semester

def residuals_diagnostics(model,ci=0.05, lags=0,figsize=(15, 9), n_lags=10):
    import Plib.Plotting.Plots as pl
    import statsmodels.api as sm
    import scipy.stats as scs
    import seaborn as sns
    sns.set()
    
    # Creating placeholder subplots
    M = 2
    N = 2
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(M, N, figsize=figsize)

    try:
        resids_ = model.resid()
    except:
        resids_ = model.resid
    resids_ = (resids_ - np.nanmean(resids_)) / np.nanstd(resids_)
    resids_nonmissing = resids_[~(np.isnan(resids_))]
    
    # Plotting residuals over time
    sns.lineplot(x=np.arange(len(resids_)), 
                 y=resids_, ax=ax1)
    ax1.set_title('Standardized residuals')

    # Plotting the distribution of residuals
    x_lim = (-1.96 * 2, 1.96 * 2)
    r_range = np.linspace(x_lim[0], x_lim[1])
    norm_pdf = scs.norm.pdf(r_range)
    
    sns.distplot(resids_nonmissing, hist=True, kde=True, 
                 norm_hist=True, ax=ax2)
    ax2.plot(r_range, norm_pdf, color='green', linewidth=2, label='N(0,1)')
    ax2.set_title('Distribution of standardized residuals')
    ax2.set_xlim(x_lim)
    ax2.legend()
        
    # Q-Q plot
    qq = sm.qqplot(resids_nonmissing, line='s', ax=ax3)
    ## 's' is for standardized line to compare the plot with a normal distribution
    ax3.set_title('Q-Q plot')

    # ACF plot
    sm.graphics.tsa.plot_acf(resids_, lags=n_lags, ax=ax4, alpha=0.05)
    ax4.set_title('ACF plot')
    
    j=jarquebera(resids_,ci)
    l=ljungbox(resids_,ci, lags)
    
    #Not available for all models
    try:
        bg=breuschgodfrey(model, lags)
    except:
        pass
    pl.customize_grid(ax1)
    pl.customize_grid(ax2)
    pl.customize_grid(ax3)
    pl.customize_grid(ax4)    

#################################################################
# Regression Residuals Tests
#################################################################        
def ljungbox(residuals,ci=0.05, lags=0,plot=True,fs=(10, 6)):
    import statsmodels.api as sm
    import seaborn as sns
    sns.set()
    
    ljung_box_results = sm.stats.acorr_ljungbox(residuals, return_df=True)
    if lags==0:
        lags=ljung_box_results[ljung_box_results['lb_pvalue']>ci].index.max()+1
    ljung_box_test_stat=ljung_box_results['lb_stat'].iloc[lags]
    pvalue=ljung_box_results['lb_pvalue'].iloc[lags]
    print(f"Ljung-Box statistic at {lags:.0f} lags: {ljung_box_test_stat:.2f} with p-value: {pvalue:.2f}")
    if pvalue < ci:
        print("Residuals are likely not independent.")
    else:
        print("Residuals are likely to be independent.")

    if plot:
        fig, ax = plt.subplots(1, figsize=fs)
        s=sns.scatterplot(x=range(len(ljung_box_results.lb_pvalue)), y=ljung_box_results.lb_pvalue, ax=ax)
        s=ax.axhline(0.05, ls='--', color='red')
        s=ax.set(title=f"Ljung-Box test results (after modeling stock prices)", xlabel='Lags', ylabel='p-value')
    return ljung_box_results,lags

def jarquebera(residuals, ci=0.05):
    from statsmodels.stats.stattools import jarque_bera
    
    jb_test_stat, pvalue, _, _ = jarque_bera(residuals)
    print(f"Jarque-Bera statistic: {jb_test_stat:.2f} with p-value: {pvalue:.2f}")

    if pvalue < ci:
        print("Residuals are likely not normally distributed.")
    else:
        print("Residuals are likely normally distributed.")

def breuschgodfrey(model, n_lags=10):
    import statsmodels.stats.diagnostic as dg

    bg_test_stat, pvalue, fvalue, _ = dg.acorr_breusch_godfrey(model, n_lags)
    print(f"Breusch–Godfrey statistic: {bg_test_stat:.2f} with p-value: {pvalue:.2f} and f-value: {fvalue:.2f}")

    if pvalue < 0.05:
        print("Our residuals are likely to be autocorrelated up to lag ", n_lags)
    else:
        print("Our residuals are not likely to be autocorrelated.")

#################################################################
# Holt Winters Exponential Smoothing
#################################################################    
def SES_test(train_data,valid_data,lbl='adj_close',optimized=True,alpha=0.6,plot=True, plot_fitted=True):
    from statsmodels.tsa.api import SimpleExpSmoothing
    from sklearn.metrics import mean_squared_error
    title='Simple Exponential Smoothing'
    model = SimpleExpSmoothing(endog=np.asarray(train_data[lbl]),initialization_method='estimated')
    if optimized:
        model = model.fit(optimized=True)
    else:
        model = model.fit(smoothing_level=alpha)
    valid_data['SimpleExpSmooth'] = model.forecast(len(valid_data)) #Out-of-sample forecasts
    fitted=[]
    if plot_fitted:
        train_data['fitted']=model.fittedvalues
    rmse = math.sqrt(mean_squared_error(valid_data[lbl], valid_data['SimpleExpSmooth']))
    print('The RMSE value for '+ title +' is', rmse)
    print(model.summary())
    if plot:
        plotPrediction(train_data,valid_data, title=title + ' Method',
               lblplot=lbl,lblpred='SimpleExpSmooth', 
               lblmodel=title,predict=True,fitted=plot_fitted)
    return model

def DES_test(train_data,valid_data,lbl='adj_close',optimized=False,alpha=0.6,beta=0.001, plot=True, plot_fitted=False):
    from statsmodels.tsa.api import ExponentialSmoothing
    from sklearn.metrics import mean_squared_error
    title='Double Exponential Smoothing'
    model = ExponentialSmoothing(endog=np.asarray(train_data[lbl]),trend='add',initialization_method='estimated')
    if optimized:
        model = model.fit(optimized=True)
    else:
        model = model.fit(smoothing_level=alpha,smoothing_slope=beta)
    valid_data['DoubleExpSmooth'] = model.forecast(len(valid_data)) #Out-of-sample forecasts
    if plot_fitted:
        train_data['fitted']=model.fittedvalues
    rmse = math.sqrt(mean_squared_error(valid_data[lbl], valid_data['DoubleExpSmooth']))
    print('The RMSE value for '+ title +' is', rmse)
    print(model.summary())
    if plot:
        plotPrediction(train_data,valid_data, title=title + ' Method',
               lblplot=lbl,lblpred='DoubleExpSmooth', 
               lblmodel=title,predict=True,fitted=plot_fitted)
    return model

def TES_test(train_data,valid_data,lbl='adj_close',optimized=False,alpha=0.6,beta=0.001, gamma=0.8,seas_periods=5,ctype=['add','mul'],plot=True, plot_fitted=False,stats=False):
    from statsmodels.tsa.api import ExponentialSmoothing
    from sklearn.metrics import mean_squared_error
    title='Triple Exponential Smoothing'
    model = ExponentialSmoothing(endog=np.asarray(train_data[lbl]),trend=ctype[0],seasonal=ctype[1],seasonal_periods=seas_periods, initialization_method='estimated')
    if optimized:
        model = model.fit(optimized=True)
    else:
        model = model.fit(smoothing_level=alpha,smoothing_slope=beta,smoothing_seasonal=gamma)
    valid_data['TripleExpSmooth'] = model.forecast(len(valid_data)) #Out-of-sample forecasts
    if plot_fitted:
        train_data['fitted']=model.fittedvalues
    if stats:
        rmse = math.sqrt(mean_squared_error(valid_data[lbl], valid_data['TripleExpSmooth']))
        print('The RMSE value for '+ title +' is', rmse)
        print(model.summary())
    if plot:
        plotPrediction(train_data,valid_data, title=title + ' Method',
               lblplot=lbl,lblpred='TripleExpSmooth', 
               lblmodel=title,predict=True,fitted=plot_fitted)
    return model

def model_diag(model, train_data,valid_data,lbl='adj_close',plot=True, plot_fitted=False):
    from statsmodels.tsa.api import ExponentialSmoothing
    from sklearn.metrics import mean_squared_error
    title='Exponential Smoothing'
    valid_data['ExpSmooth'] = model.forecast(len(valid_data)) #Out-of-sample forecasts
    if plot_fitted:
        train_data['fitted']=model.fittedvalues
    try:
        rmse = math.sqrt(mean_squared_error(valid_data[lbl], valid_data['ExpSmooth']))
        print('The RMSE value for '+ title +' is', rmse)
    except:
        pass;
    print(model.summary())
    if plot:
        plotPrediction(train_data,valid_data, title=title + ' Method',
               lblplot=lbl,lblpred='ExpSmooth', 
               lblmodel=title,predict=True,fitted=plot_fitted)

def expSmoothing(data, params,data_params):
    from statsmodels.tsa.holtwinters import ExponentialSmoothing

    t,d,s,p,r = params

    m = ExponentialSmoothing(np.asarray(data),seasonal_periods=p,
                                     trend=t, seasonal=s,damped_trend=d, initialization_method='estimated')
    m=m.fit(optimized=True, remove_bias=r)
    
    yhat = m.forecast(1)
    
    return yhat[0],True,m.aic,m.bic

def holt_winters(x,y,seasonp=5,n_test=2,parallel=True):
    
    t_ = ['add', 'mul', None]  #
    d_ = [True, False]   #
    s_ = ['add', 'mul', None]  ##,#
    p_ = [seasonp]              ##,#
    r_ = [True, False]

    lparams=[t_,d_,s_,p_,r_]
    lbl_lparams=['trend','damped_trend','seasonal','seasonal_periods', 'remove_bias']

    g=gridSearch(y,data_params={},
                 modelf=expSmoothing,
                 lparams=lparams,
                 n_test = n_test,parallel=parallel,lbl_lparams=lbl_lparams)
    
    df=pd.DataFrame()
    df['x']=x
    df['y']=y
    
    from statsmodels.tsa.holtwinters import ExponentialSmoothing

    t,d,s,p,r = g.getModelParams(0)
    m = ExponentialSmoothing(np.asarray(df.x),seasonal_periods=p,
                                     trend=t, seasonal=s,damped_trend=d)
    m = m.fit(optimized=True, remove_bias=r)
    m.forecast(len(x))
    return m,g    

#################################################################
# Exponential Smoothing Indicator for backtesting
#################################################################        
def tesIndicator(sdata, lbl='adj_close', train_periods=252, retrain_periods=180, forecast_periods=1, params=(), label=''):
    from statsmodels.tsa.api import ExponentialSmoothing
    from sklearn.metrics import mean_squared_error
    import Plib.Stats.Timeseries as ts
    
    for t in range(0,len(sdata)-train_periods):
        if t%retrain_periods==0:
            #(re)Train over the train_periods with the specified parameters and test window
            ret = TES_test(sdata[:train_periods+t].copy(),
                              sdata[train_periods+t+1:train_periods+t+2].copy(),lbl, *params)
        for d in range(0,forecast_periods):
            #Forecasts forecast_periods day using the available data
            tsignal = ret.forecast(d+t+1) #Out-of-sample forecasts
        if t==0:
            signal = np.append(np.zeros(train_periods),tsignal)
        else:
            signal = np.append(signal, tsignal[-forecast_periods])  
    sdata['TES_signal_'+label]=signal        
    return sdata 
    
#################################################################
# Stationarity - Tests and Diagnostic Plots
#################################################################    
def unit_root_tests(timeseries, ci=0.05,table=True):
    if table: 
        tf='-------------------- Unit Root Tests -----------------------------'
        print(tf)
    
    cv=str(int(ci*100))+'%'
    b1=adf_test(timeseries,cv,table)
    print('')
    b2=kpss_test(timeseries,cv,table)
    
    if table: 
        tf2=''
        for i in range(0,len(tf)):
            tf2=tf2+'-'
        print(tf2)
    
    return b1,b2

def adf_test(timeseries, cv='5%',table=True):
    from statsmodels.tsa.stattools import adfuller

    #Perform Dickey-Fuller test:
    if table: 
        print ('Results of Dickey-Fuller Test:')
    dftest = adfuller(timeseries, autolag='AIC')
    dfoutput=pd.Series(dftest[0:4], index=['Test Statistic','p-value','#Lags Used','Number of Observations Used'])

    if table:
        for key,value in dftest[4].items():
            dfoutput['Critical Value (%s)'%key] = value
        print (dfoutput)
        
    stat=False
    if cv=='1%': pv=0.01
    if cv=='5%': pv=0.05
    if cv=='10%': pv=0.1
    if (dftest[0] < dftest[4][cv]):#(dftest[0] < dftest[4][cv]) and (dftest[1] < pv):
        if table: print('The series is stationary')
        stat=True
    else:
        if table: print('The series is not stationary')
    return stat
    
def kpss_test(timeseries, cv='5%',table=True):
    from statsmodels.tsa.stattools import kpss

    if table: 
        print ('Results of KPSS Test:')
    kpsstest = kpss(timeseries, regression='c')
    kpss_output = pd.Series(kpsstest[0:3], index=['Test Statistic','p-value','Lags Used'])
    
    if table: 
        for key,value in kpsstest[3].items():
            kpss_output['Critical Value (%s)'%key] = value
        print (kpss_output)
        
    stat=False
    if cv=='1%': pv=0.01
    if cv=='2.5%': pv=0.025
    if cv=='5%': pv=0.05
    if cv=='10%': pv=0.1
    if (kpsstest[0] < kpsstest[3][cv]):#(kpsstest[0] < kpsstest[3][cv]) and (kpsstest[1] < pv):
        if table: print('The series is stationary')
        stat=True
    else:
        if table: print('The series is not stationary')
    return stat

def seriesDifferencing(df1, diff=1,exp=False,lbl='adj_close',tlog=True,n_lags=10,cin=0.05,w=12,fs=(12,6),plot=True):
    import statsmodels.api as sm
    from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
    import Plib.Plotting.Plots as pl
    
    df=df1.copy()
    
    if diff>=0:
        title="Series Differenced with d="+str(diff)
        if tlog: 
            df[lbl] = np.log(df[lbl])
            title="Series Log Differenced with d="+str(diff)
        df[lbl] = df[lbl] - df[lbl].shift(diff)
    elif diff<0:
        title="Series Demeaned (Rolling) with w="+str(diff)
        if tlog: 
            df[lbl] = np.log(df[lbl])
            if exp:
                title="Series Log Exp-Demeaned with w="+str(diff)
            else:
                title="Series Log Demeaned with w="+str(diff)
        if exp:
            df[lbl] = df[lbl] - df[lbl].ewm(halflife=abs(diff),min_periods=0,adjust=True).mean()
        else:
            df[lbl] = df[lbl] - df[lbl].rolling(window=abs(diff)).mean()
            
    fig, ax = plt.subplots(figsize=(fs[0],int(fs[1]/2)))
    x = np.arange(0,len(df),1)
    plt.plot(x, df[lbl], label='stationary series')
    rolling_mean = df[lbl].rolling(window=w).mean() 
    rolling_std = df[lbl].rolling(window=w).std()
    plt.plot(x, rolling_mean, label='rolling mean')
    plt.plot(x, rolling_std, label='rolling std')
    #ax.set_xticks(x)
    #ax.set_xticklabels(df.index)
    plt.legend(loc='best')
    
    pl.customize_grid(ax)
    
    plt.title(title)
    plt.show()
    
    #ACF
    acf, ci = sm.tsa.acf(df[lbl].dropna(), alpha=cin)
    acf=pd.DataFrame(acf)
    acf.columns=['acf']
    ci=pd.DataFrame(ci)-1
    ci.columns=['l','u']
    acf=acf.join(ci)[1:]
    #lag_acf=acf[acf['acf']>acf['u']].index[0]
    test=acf[acf['acf']>acf['u']]
    lag_acf=-1
    if len(test)>0:
        lag_acf=test.index[0]
    print('ACF First Lag AR(q): ',lag_acf)
    
    #Autocorrelation plots
    if plot:
        fig, ax = plt.subplots(figsize=(fs[0],int(fs[1]/2)))
        plot_acf(df[lbl].dropna(), lags=n_lags, ax=ax)
        pl.customize_grid(ax)
        plt.show()
    
    #PACF
    pacf, ci = sm.tsa.pacf(df[lbl].dropna(), alpha=cin)
    pacf=pd.DataFrame(pacf)
    pacf.columns=['pacf']
    ci=pd.DataFrame(ci)-1
    ci.columns=['l','u']
    pacf=pacf.join(ci)[1:]
    #lag_pacf=pacf[pacf['pacf']>pacf['u']].index[0]
    test=pacf[pacf['pacf']>pacf['u']]
    lag_pacf=-1
    if len(test)>0:
        lag_pacf=test.index[0]
    print('PACF First Lag MA(p): ',lag_pacf)
    
    #Autocorrelation plots
    if plot:
        fig, ax = plt.subplots(figsize=(fs[0],int(fs[1]/2)))
        plot_pacf(df[lbl].dropna(), lags=n_lags, ax=ax)
        pl.customize_grid(ax)
        plt.show()
    
    unit_root_tests(df[lbl].dropna(),ci=cin)
    
    return df,lag_pacf,lag_acf

#################################################################
# Causality: Granger and Johansen
# Model by Vector Auto Regression
#################################################################    
def grangerCausality_test(data, vars_list=[], on_returns=True):
    from statsmodels.tsa.stattools import grangercausalitytests
    #vars_list of shape as ["realgdp", "realcons"],["realgdp", "realinv"],["realcons", "realinv"]
    
    for l in vars_list:
        if on_returns:
            mdata = data[l].pct_change().dropna()
        else:
            mdata = data[l].dropna()
        #Performing test on for realgdp and realcons.
        gc_res = grangercausalitytests(mdata, 12)

def coint_johansen_test(data, alpha=0.05, length= 6):
    from statsmodels.tsa.vector_ar.vecm import coint_johansen
    
    def cointegration_test(data, alpha=alpha): 
        #Perform Johanson's Cointegration Test and Report Summary
        out = coint_johansen(data,-1,5)
        d = {'0.90':0, '0.95':1, '0.99':2}
        traces = out.lr1
        cvts = out.cvt[:, d[str(1-alpha)]]
        return traces, cvts

    def adjust(val, length= length): 
        return str(val).ljust(length)

    traces, cvts = cointegration_test(data)
    # Summary
    print('Name   ::  Test Stat > C(95%)    =>   Signif  \n', '--'*20)
    for col, trace, cvt in zip(data.columns, traces, cvts):
        print(adjust(col), ':: ', adjust(round(trace,2), 9), ">", adjust(cvt, 8), ' =>  ' , trace > cvt)
    print('')
    return traces, cvts

def VAR_test(data,freq='Q-DEC', fit_by='AIC', maxlags=None, forecast_periods=20, stats=True, plot=True):
    from statsmodels.tsa.api import VAR
    
    def getBest(x, col='AIC'):
        # obtains the value of lag valueVAR(p) selecting the AIC for which other parameters are generating minimums
        df=pd.DataFrame(x.summary().data[1:],columns=x.summary().data[0:1])
        substring = '*'
        df=df[[col]]
        mask=df.applymap(lambda x: substring in x.lower() if isinstance(x,str) else False).to_numpy()
        return df[mask].index[0]

    var = VAR(data,freq=freq)
    x = var.select_order(maxlags, trend='c') #if None, defaults to 12 * (nobs/100.)**(1./4)
    # Fit the VAR at every lag till getBest lag based on fit_by measure of fit 
    VAR_fit = var.fit(getBest(x, col=fit_by))
    if stats:
        print(x.summary())
        print(VAR_fit.summary())
    if plot:
        VAR_fit.plot();
        VAR_fit.plot_forecast(forecast_periods);
    return VAR_fit
    
#################################################################
# ARIMA/SARIMA with GridSearch
#################################################################    
def arima_test(df,y1='2018',y2='2019',lbl='Adj Close',tlog=True,params=(1,1,1,'n'),plot=False):
    from statsmodels.tsa.arima.model import ARIMA
    import math
    from sklearn.metrics import mean_squared_error
    
    train_data = df[:y1] #df.loc[:y1]
    valid_data = df[y2:]
    p,i,q,t=params
    
    title='ARIMA'
    if tlog:
        model = ARIMA(np.log(train_data[lbl]), order=(p,i,q),trend=t)
    else:
        model = ARIMA(train_data[lbl], order=(p,i,q),trend=t)
    model = model.fit()
    #valid_data['Arima'] = model.predict(n_periods=horizon)   # predict over same timeframe
    valid_data['Arima']=pd.DataFrame(model.forecast(valid_data.shape[0])).values
    if tlog:
        valid_data['Arima']=np.exp(valid_data['Arima'])
    rmse = math.sqrt(mean_squared_error(valid_data[lbl], valid_data['Arima']))
    print('The RMSE value for '+ title +' is', rmse)
    print(model.summary())
    if plot:
        residuals_diagnostics(model,lags=p)
        plotPrediction(train_data,valid_data, title=title + ' Method',
                       lblplot=lbl,lblpred='Arima', 
                       lblmodel=title,predict=True)
    return model

def sarima_test(df,y1='2018',y2='2019',lbl='Adj Close',tlog=True,params=(1,1,1,'n',1,1,1,7),plot=False):
    from statsmodels.tsa.statespace import sarimax
    import math
    from sklearn.metrics import mean_squared_error
    
    train_data = df[:y1] #df.loc[:y1]
    valid_data = df[y2:]
    p,i,q,t,P,D,Q,m=params
    
    title='SARIMA'
    if tlog:
        model = sarimax.SARIMAX(np.log(train_data[lbl]), seasonal_order=(P,D,Q,m), order=(p,i,q),trend=t)
    else:
        model = sarimax.SARIMAX(train_data[lbl], seasonal_order=(P,D,Q,m), order=(p,i,q),trend=t)
    model = model.fit()
    valid_data['SARIMA']=pd.DataFrame(model.forecast(valid_data.shape[0])).values
    if tlog:
        valid_data['SARIMA']=np.exp(valid_data['SARIMA'])
    rmse = math.sqrt(mean_squared_error(valid_data[lbl], valid_data['SARIMA']))
    print('The RMSE value for '+ title +' is', rmse)
    print(model.summary())
    if plot:
        residuals_diagnostics(model,lags=p)
        plotPrediction(train_data,valid_data, title=title + ' Method',
                   lblplot=lbl,lblpred='SARIMA', 
                   lblmodel=title,predict=True)
    return model

###########  GridSearch
def sarima(data, params,data_params,use_test=False):
    from statsmodels.tsa.statespace import sarimax
    
    p,q,i,t,l,P,D,Q,m = params
    
    if l:
        model = sarimax.SARIMAX(np.asarray(np.log(data)), seasonal_order=(P,D,Q,m), order=(p,i,q),trend=t)
    else:
        model = sarimax.SARIMAX(np.asarray(data), seasonal_order=(P,D,Q,m), order=(p,i,q),trend=t)
    model = model.fit()
    yhat=model.forecast(1)
    if l: yhat = np.exp(yhat)
    
    test=True

    return yhat[0],test,model.aic,model.bic

def arima(data, params,data_params,use_test=False):
    from statsmodels.tsa.arima.model import ARIMA
    from statsmodels.stats.stattools import jarque_bera
    
    p,q,i,t,l = params
    
    if l:
        model = ARIMA(np.asarray(np.log(data)), order=(p,i,q),trend=t)
    else:
        model = ARIMA(np.asarray(data), order=(p,i,q),trend=t)
    model = model.fit()
    yhat=model.forecast(1) #horizon=1
    if l: yhat = np.exp(yhat)
    
    test=True
    if use_test:
        jb_test_stat, pvalue, _, _ = jarque_bera(model.resid)
        if pvalue < 0.05:
            test=False

    return yhat[0],test,model.aic,model.bic

def arimaGS(x,y,n_test=2,parallel=False):
    
    #list(range(0,3)) -> [0,1,2]
    enforce_stationarity=True
    enforce_invertibility=True
    
    p_ = list(range(1,4)) 
    q_ = list(range(1,2))   
    i_ = list(range(1,2))
    t_ = ['n'] #deterministic trend           
    l_ = [False,True]
    
    lparams=[p_,q_,i_,t_,l_]
    lbl_lparams=['p','q','i','trend', 'log']
    
    g=gridSearch(y,data_params={},
                 modelf=arima,
                 lparams=lparams,
                 n_test = n_test,parallel=parallel,lbl_lparams=lbl_lparams)
    
    df=pd.DataFrame()
    df['x']=x
    df['y']=y
    
    from statsmodels.tsa.arima.model import ARIMA
    
    p,q,i,t,l = g.getModelParams(0)
    
    if l:
        m = ARIMA(np.asarray(np.log(df.y)), order=(p,i,q),trend=t)
    else:
        m = ARIMA(np.asarray(df.y), order=(p,i,q),trend=t)
    m = m.fit()
    
    yhat= m.predict(start=df.iloc[:, 0].index[0], end=df.iloc[:, 0].index[-1])
    
    if l: yhat = np.exp(yhat)
        
    return m,yhat,g
    
def sarimaGS(x,y,n_test=2,parallel=False):

    time_varying_regression=False
    mle_regression=True
    simple_differencing=False
    enforce_stationarity=True
    enforce_invertibility=True
    hamilton_representation=False

    p_ = list(range(1,3)) 
    q_ = list(range(1,3))   
    i_ = list(range(1,3))
    t_ = ['n']#,'c','t','ct'] #deterministic trend           
    l_ = [False,True]
    P_ = list(range(0,1))
    D_ = list(range(0,1))
    Q_ = list(range(0,1))
    m_ = [0,4,2,12] #,52,21,12,4,2] #Seasonal differencing in weekly, bi-weekly, monthly, quarterly, and semsterly 
    
    lparams=[p_,q_,i_,t_,l_,P_,D_,Q_,m_]
    lbl_lparams=['p','q','i','trend', 'log','P','I','Q','seas_diff']
    

    g=gridSearch(y,data_params={},
                 modelf=sarima,
                 lparams=lparams,
                 n_test = n_test,parallel=parallel,lbl_lparams=lbl_lparams)
    
    df=pd.DataFrame()
    df['x']=x
    df['y']=y
    
    from statsmodels.tsa.statespace import sarimax

    p,q,i,t,l,P,D,Q,m = g.getModelParams(0)
    
    if l:
        m = sarimax.SARIMAX(np.log(df.y), seasonal_order=(P,D,Q,m), order=(p,i,q),trend=t)
    else:
        m = sarimax.SARIMAX(df.y, seasonal_order=(P,D,Q,m), order=(p,i,q),trend=t)
    m = m.fit()
    
    yhat= m.predict(start=df.iloc[:, 0].index[0], end=df.iloc[:, 0].index[-1])
    
    if l: yhat = np.exp(yhat)
        
    return m,yhat,g

#################################################################
# PmdArima Auto search
#################################################################    
def pmdarima_test(df,y1='2014',y2='2015',lbl='adj_close',tlog=False,seasonal=True,trace=False,seasonality=12,stationary=True,maxiter=10, plot=False):
    import pmdarima as pm
    import math
    from sklearn.metrics import mean_squared_error
    
    train_data = df[:y1]
    valid_data = df[y2:]
    
    title='PDArima'
    if tlog:
        arima_fit = pm.auto_arima(np.log(train_data[lbl]), error_action='ignore', 
                        suppress_warnings=True, seasonal=seasonal,stationary=stationary,
                        m=seasonality,maxiter=maxiter)
    else:
        arima_fit = pm.auto_arima(train_data[lbl], error_action='ignore', 
                        suppress_warnings=True, seasonal=seasonal,stationary=stationary,
                        m=seasonality,maxiter=maxiter)
    
    arima_fcast = arima_fit.predict(n_periods=len(valid_data), return_conf_int=True, alpha=0.05)        
    valid_data['PDArima'] = arima_fcast[0]
    if tlog:
        valid_data['PDArima']=np.exp(valid_data['PDArima'])
    rmse = math.sqrt(mean_squared_error(valid_data[lbl], valid_data['PDArima']))
    p_, d_, q_ = arima_fit.order
    print('The RMSE value for '+ title +' is', rmse)
    print('Model: ',arima_fit)
    print(arima_fit.summary())
    if plot:
        residuals_diagnostics(arima_fit,lags=p_)
        plotPrediction(train_data,valid_data, title=title + ' Method',
                       lblplot=lbl,lblpred='PDArima', 
                       lblmodel=title,predict=True)
    return arima_fit

#################################################################
# ARCH 
#################################################################    
def arch_test(df,y1='2018',y2='2019',lbl='Adj Close',params=(1,1,1,'Zero','ARCH'),plot=False):
    import arch
    import math
    from sklearn.metrics import mean_squared_error
    
    train_data = df[:y1] #df.loc[:y1]
    valid_data = df[y2:]
    p,o,q,mean,vol=params
    
    title=vol
    arch_model = arch.arch_model(train_data[lbl], vol=vol, mean=mean, p=p, o=o, q=q)
    arch_model_fitted = arch_model.fit()

    valid_data[vol]=arch_model_fitted.forecast(horizon=1).mean['h.1']    # the alignment is the day after the last day in train data
    print(arch_model_fitted.summary())
    if plot:
        residuals_diagnostics(arch_model_fitted,lags=p)
        arch_model_fitted.plot(annualize='D');
    return arch_model,arch_model_fitted
        
#################################################################
# ARIMA + GARCH
#################################################################    
def garchIndicator(sdata, lbl='adj_close', train_periods=4000, forecast_periods=15, params=(), label='',simul=False):
    from statsmodels.tsa.arima.model import ARIMA
    import arch
    
    p_,i_,q_,t_=params
    
    for t in range(0,len(sdata)-train_periods):
        #(re)Train over the train_periods with the specified parameters and test window
        model = ARIMA(sdata[:train_periods+t].copy()[lbl], order=(p_,i_,q_),trend=t_) #, enforce_stationarity=True
        model = model.fit()
        try:
            resids_ = model.resid()
        except:
            resids_ = model.resid
        resids_ = (resids_ - np.nanmean(resids_)) / np.nanstd(resids_)
        resids_nonmissing = resids_[~(np.isnan(resids_))]
        garch_model = arch.arch_model(resids_nonmissing, vol='GARCH', p=p_, o=i_, q=q_)
        garch_model_fitted = garch_model.fit(show_warning=False,disp='off')

        if t==0:
            signal=sdata.copy()
            signal[lbl+'_price']=0
            signal[lbl+'_vol']=0
        
        if t%forecast_periods==0:
            #Forecasts forecast_periods day using the available data
            predicted_prices=model.forecast(forecast_periods)  
            
            if simul:
                garch_model_forecast = garch_model.simulate(garch_model_fitted.params, forecast_periods)
                forecast=pd.DataFrame(predicted_prices)
                forecast['error']=garch_model_forecast.data.values
                forecast['price']=forecast.predicted_mean+forecast.error
                forecast['vol']=garch_model_forecast.volatility.values
                forecast=forecast[['price','vol']]
            else:
                garch_model_forecast = garch_model_fitted.forecast(horizon=forecast_periods)
                forecast=pd.DataFrame(predicted_prices)
                forecast['cmean']=garch_model_forecast.mean.dropna().T.values
                forecast['vol']=garch_model_forecast.variance.dropna().T.values
                forecast['price']=forecast.predicted_mean+forecast.cmean
                forecast=forecast[['price','vol']]
                
            if train_periods+t+forecast_periods > len(sdata):
                buf=len(sdata)-(train_periods+t)
            else:
                buf=forecast_periods
            signal[lbl+'_price'].iloc[train_periods+t:train_periods+t+buf]=forecast.price.values[:buf]
            signal[lbl+'_vol'].iloc[train_periods+t:train_periods+t+buf]=forecast.vol.values[:buf]
            
    sdata['GARCH_price_'+label]=signal[lbl+'_price']
    sdata['GARCH_vol_'+label]=signal[lbl+'_vol']
    return sdata 
    
def pdmGarch_test(df,y1='2018',y2='2019',lbl='adj_close',plot=False):
    import pmdarima as pm
    import arch
    import math
    from sklearn.metrics import mean_squared_error
    
    train_data = df[:y1] #df.loc[:y1]
    valid_data = df[y2:]
    
    ## fitting ARIMA on adjusted close prices
    arima_fit = pm.auto_arima(train_data[lbl], error_action='ignore', 
                               suppress_warnings=True, seasonal=False)
    p_, d_, q_ = arima_fit.order
    arima_fit_residuals = arima_fit.arima_res_.resid.copy()
    arima_fit_residuals = (arima_fit_residuals - np.nanmean(arima_fit_residuals)) / np.nanstd(arima_fit_residuals)
    resids_nonmissing = arima_fit_residuals[~(np.isnan(arima_fit_residuals))]
    # fitting a GARCH(1,1) model after fitting ARIMA on the prices
    garch_model = arch.arch_model(resids_nonmissing, vol='GARCH', p=p_, o=d_, q=q_)
    garch_model_fitted = garch_model.fit()

    if plot:
        print(arima_fit.summary())
        residuals_diagnostics(arima_fit,lags=p_)
        print(garch_model_fitted.summary())
        residuals_diagnostics(garch_model_fitted,lags=p_)

    # Using ARIMA to predict prices
    predicted_prices = arima_fit.predict(n_periods=1)[0]
    # Using GARCH to predict the residuals
    garch_model_forecast = garch_model_fitted.forecast(horizon=1)
    predicted_residual = garch_model_forecast.mean['h.1'].iloc[-1]
    # Combining both models
    predicted_price_range = (predicted_prices-predicted_residual, predicted_prices+predicted_residual)

    print('Forecasted bands for price: ', predicted_price_range)

    return arima_fit,garch_model_fitted

def arima_garch(data, params,data_params,use_test=False):
    from statsmodels.tsa.arima.model import ARIMA
    from statsmodels.stats.stattools import jarque_bera
    import arch
    
    p,q,i,t,l = params
    
    if l:
        model = ARIMA(np.asarray(np.log(data)), order=(p,i,q),trend=t)
    else:
        model = ARIMA(np.asarray(data), order=(p,i,q),trend=t)
    model = model.fit()
    yhat=model.forecast(1) #horizon=1
    if l: yhat = np.exp(yhat)
    
    test=True
    if use_test:
        jb_test_stat, pvalue, _, _ = jarque_bera(model.resid)
        if pvalue < 0.05:
            test=False
    
    # Specifying an ARCH(1) model 
    arima_fit_residuals = model.resid.copy()
    arima_fit_residuals = (arima_fit_residuals - np.nanmean(arima_fit_residuals)) / np.nanstd(arima_fit_residuals)
    resids_nonmissing = arima_fit_residuals[~(np.isnan(arima_fit_residuals))]
    arch_model1 = arch.arch_model(resids_nonmissing, vol='GARCH', p=p, o=i, q=q)

    # Estimating the model
    arch_model1_fitted = arch_model1.fit(update_freq = 5,disp='off')

    # Use GARCH to predict the residual
    garch_forecast = arch_model1_fitted.forecast(horizon=1,reindex=False)

    predicted_et = garch_forecast.mean['h.1'].iloc[-1]
    # Combine both models' output: yt = mu + et
    prediction = yhat[0] + predicted_et
    
    return prediction,test,model.aic,model.bic

def arima_garchGS(x,y,n_test=2,parallel=False,plot=True):
    
    #list(range(0,3)) -> [0,1,2]
    enforce_stationarity=True
    enforce_invertibility=True
    
    p_ = list(range(3,4)) 
    q_ = list(range(1,2))   
    i_ = list(range(1,2))
    t_ = ['n'] #deterministic trend           
    l_ = [False,True]
    
    lparams=[p_,q_,i_,t_,l_]
    lbl_lparams=['p','q','i','trend', 'log']
    
    g=gridSearch(y,data_params={},
                 modelf=arima_garch,
                 lparams=lparams,
                 n_test = n_test,parallel=parallel,lbl_lparams=lbl_lparams)
    
    df=pd.DataFrame()
    df['x']=x
    df['y']=y
    
    from statsmodels.tsa.arima.model import ARIMA
    import arch
    
    p,q,i,t,l = g.getModelParams(0)
    
    if l:
        m = ARIMA(np.asarray(np.log(df.y)), order=(p,i,q),trend=t)
    else:
        m = ARIMA(np.asarray(df.y), order=(p,i,q),trend=t)
    m = m.fit()
    
    yhat= m.predict(start=df.iloc[:, 0].index[0], end=df.iloc[:, 0].index[-1])
    
    if l: yhat = np.exp(yhat)
        
    # Specifying an ARCH(1) model 
    arima_fit_residuals = m.resid.copy()
    arima_fit_residuals = (arima_fit_residuals - np.nanmean(arima_fit_residuals)) / np.nanstd(arima_fit_residuals)
    resids_nonmissing = arima_fit_residuals[~(np.isnan(arima_fit_residuals))]
    arch_model1 = arch.arch_model(resids_nonmissing, vol='GARCH', p=p, o=i, q=q) #m.resid

    # Estimating the model
    arch_model1_fitted = arch_model1.fit(update_freq = 5,disp='off')

    # Use GARCH to predict the residual
    garch_forecast = arch_model1_fitted.forecast(horizon=1,reindex=False)

    predicted_et = garch_forecast.mean['h.1'].iloc[-1]
    # Combine both models' output: yt = mu + et
    yhat = yhat + predicted_et
    
    if plot:
        print(m.summary())
        residuals_diagnostics(m,lags=p)
        print(arch_model1_fitted.summary())
        residuals_diagnostics(arch_model1_fitted,lags=p)
    
    return m,arch_model1_fitted,yhat,g

def garchPredict(data,horizon=5,params=(1,1,1,'n'), lbl='adj_close'):
    from statsmodels.tsa.arima.model import ARIMA
    import arch
    import datetime
    
    p_,i_,q_,t_=params
    
    for idx in range(0,horizon):
        if idx==0: df=data.copy()
        mydate=df.iloc[[-1]].index + datetime.timedelta(days=1)
            
        # Fitting ARIMA on prices and GARCH on residuals
        model = ARIMA(df[lbl], order=(p_,i_,q_),trend=t_,enforce_stationarity=True)
        model = model.fit()
        try:
            resids_ = model.resid()
        except:
            resids_ = model.resid
        resids_ = (resids_ - np.nanmean(resids_)) / np.nanstd(resids_)
        resids_nonmissing = resids_[~(np.isnan(resids_))]
        garch_model = arch.arch_model(resids_nonmissing, vol='GARCH',p=p_, o=i_, q=q_)
        garch_model_fitted = garch_model.fit()

        # Predicting prices and volatility
        predicted_prices=model.forecast(1)                     # predict in the future
        garch_model_forecast = garch_model_fitted.forecast(horizon=1, start=mydate)
        predicted_residual = garch_model_forecast.mean['h.1'].iloc[-1]
        expected_prices = ((predicted_prices-predicted_residual) + (predicted_prices+predicted_residual))/2
        df=df.append(pd.DataFrame(index=mydate))
        df[df.columns[0]].iloc[-1]=expected_prices

    return df
 