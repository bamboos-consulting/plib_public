#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Regression 
#
# Module comprising helper functions to perform regression
#############################################################################################      

#Common Libraries
import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path+"/")

import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
from matplotlib.pylab import rcParams
rcParams['figure.figsize'] = 8, 4

import warnings
warnings.filterwarnings("once")

import numpy as np
import pandas as pd

#Intel(R) Extension for Scikit-learn dynamically patches scikit-learn estimators to use oneDAL as the underlying solver
#from daal4py.sklearn import patch_sklearn
from sklearnex import patch_sklearn
patch_sklearn()

from sklearn.base import BaseEstimator, TransformerMixin

#################################################
# GridSearchByFit class and diagnostic methods
#################################################      
class GaussianFeatures(BaseEstimator, TransformerMixin):
    #Uniformly spaced Gaussian features for one-dimensional input

    def __init__(self, N, width_factor=2.0):
        self.N = N
        self.width_factor = width_factor
    
    @staticmethod
    def _gauss_basis(x, y, width, axis=None):
        arg = (x - y) / width
        return np.exp(-0.5 * np.sum(arg ** 2, axis))
        
    def fit(self, X, y=None):
        # create N centers spread along the data range
        self.centers_ = np.linspace(X.min(), X.max(), self.N)
        self.width_ = self.width_factor * (self.centers_[1] - self.centers_[0])
        return self
        
    def transform(self, X):
        return self._gauss_basis(X[:, :, np.newaxis], self.centers_,
                                 self.width_, axis=1)

class gridSearch():
    
    def __init__(self,data,y_col,fmodel,n_jobs=4):
        from sklearn.pipeline import make_pipeline
        
        self.n_jobs=n_jobs
        
        X,y=splitMatrix(data,y_col)

        self.fmodel=fmodel
        
        init, param_grid, features, model=self.fmodel()

        pipeline=make_pipeline(features,model)
        
        self.result,self.model=self.hyperTuning(X, y, pipeline, param_grid, 
                                nj=self.n_jobs)
       
    @staticmethod
    def hyperTuning(xnl, ynl,pipeline,param_grid,k_folds=3,nj=4,grid_or_rand=1):
        from sklearn.model_selection import GridSearchCV
        from sklearn.model_selection import RandomizedSearchCV
        from sklearn.model_selection import TimeSeriesSplit 
        from sklearn.model_selection import train_test_split 

        #Prepare the grid for the search
        if grid_or_rand==0:
            grid = GridSearchCV(pipeline, param_grid, cv=TimeSeriesSplit(n_splits=k_folds),n_jobs=nj)
        else:
            grid = RandomizedSearchCV(pipeline, param_grid, n_iter=len(param_grid),cv=TimeSeriesSplit(n_splits=k_folds),n_jobs=nj)
            
        grid.fit(xnl, ynl);
        
        #Print only 2D data
        bestmodel = grid.best_estimator_
        
        #Split the dataset in two subsets
        xnl_train, xnl_test, ynl_train, ynl_test = train_test_split(xnl, ynl)
        yfit = bestmodel.fit(xnl_train, ynl_train).predict(xnl_train)
        if xnl.shape[1]==1:
            #Compare original data with trained model
            plt.scatter(xnl, ynl)
            plt.plot(xfit, yfit);
        
        return grid.best_params_ ,grid.best_estimator_

    @staticmethod
    def fmodel():
        return 

def getEquation(gmodel):
    model_name=gmodel.steps[1][0]
    model=gmodel.steps[1][1]
    if model_name in ['linearregression','ridge']:
        eq='y = ' + str(model.intercept_)
        for i in range(len(model.coef_[0])):
            eq= eq + ' + ' + str(model.coef_[0][i].round(4)) + 'x^' + str(i+1)
    else:
        eq='y = ' + str(model.intercept_)
        for i in range(len(model.coef_.round(4))):
            eq= eq + ' + ' + str(model.coef_[i].round(4)) + 'x^' + str(i+1)
    print(eq)
    
def demeaning(data):
    #ddata=demeaning([xl,yl])
    from sklearn.preprocessing import StandardScaler

    scaler = StandardScaler()
    scaler.fit(data)
    return scaler.transform(data)

def splitMatrix(data,y_col,scale=False):
    target_column = [y_col] 
    predictors = list(set(list(data.columns))-set(target_column))
    if scale: data[predictors] = data[predictors]/data[predictors].max()
    #X = data[predictors].values
    #y = data[target_column].values
    X=np.asarray(data[predictors])
    y=np.asarray(data[target_column])
    return X,y

def basis_plot(model, x,y,xfit,title=None):
    fig, ax = plt.subplots(2, sharex=True)
    model.fit(x, y)
    ax[0].scatter(x, y)
    ax[0].plot(xfit, model.predict(xfit[:, np.newaxis]))
    ax[0].set(xlabel='x', ylabel='y', ylim=(-1.5, 1.5))
 
    if title:
        ax[0].set_title(title)
    
    #print(model.steps[1][0])
    #print(model.steps[0][1].centers_,model.steps[1][1].coef_)
    if model.steps[1][0] in ['lasso','elasticnet']:
        ax[1].plot(model.steps[0][1].centers_,model.steps[1][1].coef_)
    else:
        ax[1].plot(model.steps[0][1].centers_,model.steps[1][1].coef_[0])
    ax[1].set(xlabel='basis location',
              ylabel='coefficient')
    
#################################################
# Regression Models for gridSearchByFit with HT
#################################################      
def linreg_GS():
    from sklearn.linear_model import LinearRegression
    from sklearn.impute import SimpleImputer
    
    param_grid = {'linearregression__fit_intercept': [True, False],
                  'linearregression__normalize': [True, False]}
    
    features = SimpleImputer(missing_values=np.nan, strategy='most_frequent')
    
    model = LinearRegression()
    
    return {},param_grid, features, model 

def linreg_GS_gauss(init={'gauss_w':5,'gauss_ft':10}):
    from sklearn.linear_model import LinearRegression
    from sklearn.preprocessing import PolynomialFeatures
    
    param_grid = {'gaussianfeatures__N': list(range(1, init['gauss_ft'])), 
                  'gaussianfeatures__width_factor': np.linspace(0,init['gauss_w'],init['gauss_ft']),
                  'linearregression__fit_intercept': [True, False],
                  'linearregression__normalize': [True, False]}
    
    features = GaussianFeatures(init['gauss_ft'])
    
    model = LinearRegression()
    
    return init,param_grid, features, model 

def linreg_GS_poly(init={'n_pol':18}):
    from sklearn.linear_model import LinearRegression
    from sklearn.preprocessing import PolynomialFeatures
    
    param_grid = {'polynomialfeatures__degree': np.arange(init['n_pol']),
                  'linearregression__fit_intercept': [True, False],
                  'linearregression__normalize': [True, False]}
    
    features = PolynomialFeatures(init['n_pol'])
    
    model = LinearRegression()
    
    return init,param_grid, features, model 

def lassoReg_GS(alpha_i=0.5,init={'alpha':10}):
    from sklearn.linear_model import Lasso
    from sklearn.impute import SimpleImputer
    
    param_grid = {'lasso__alpha': np.linspace(0.0001,1,init['alpha']),
                  'lasso__fit_intercept': [True, False]}
    
    features = SimpleImputer(missing_values=np.nan, strategy='most_frequent')
    
    model = Lasso(alpha=alpha_i,fit_intercept=True)
    
    return init, param_grid, features, model 

def lassoReg_GS_gauss(alpha_i=0.5,init={'gauss_w':5,'gauss_ft':10,'alpha':10}):
    from sklearn.linear_model import Lasso
    
    param_grid = {'gaussianfeatures__N': list(range(1, init['gauss_ft'])), 
                  'gaussianfeatures__width_factor': np.linspace(0,init['gauss_w'],init['gauss_ft']),
                  'lasso__alpha': np.linspace(0.001,1,init['alpha']),
                  'lasso__fit_intercept': [True, False]}
    
    features = GaussianFeatures(init['gauss_ft'])
    
    model = Lasso(alpha=alpha_i,fit_intercept=True)
    
    return init, param_grid, features, model 
    
def ridgeReg_GS(alpha_i=0.5,init={'alpha':10}):
    from sklearn.linear_model import Ridge
    from sklearn.impute import SimpleImputer
    
    param_grid = {'ridge__alpha': np.linspace(0.001,1,init['alpha']),
                  'ridge__fit_intercept': [True, False]}
    
    features = SimpleImputer(missing_values=np.nan, strategy='most_frequent')
    
    model = Ridge(alpha=alpha_i,fit_intercept=True)
    
    return init, param_grid, features, model 
    
def ridgeReg_GS_gauss(alpha_i=0.5,init={'gauss_w':5,'gauss_ft':10,'alpha':10}):
    from sklearn.linear_model import Ridge
    
    param_grid = {'gaussianfeatures__N': list(range(1, init['gauss_ft'])), 
                  'gaussianfeatures__width_factor': np.linspace(0,init['gauss_w'],init['gauss_ft']),
                  'ridge__alpha': np.linspace(0.001,1,init['alpha']),
                  'ridge__fit_intercept': [True, False]}
    
    features = GaussianFeatures(init['gauss_ft'])
    
    model = Ridge(alpha=alpha_i,fit_intercept=True)
    
    return init, param_grid, features, model 

def elasticReg_GS(alpha_i=0.5,l1_i=0.5, init={'alpha':10,'l1_ratio':0.2}):
    from sklearn.linear_model import ElasticNet
    from sklearn.impute import SimpleImputer
    
    param_grid = {'elasticnet__alpha': np.linspace(0.001,1,init['alpha']),
                  'elasticnet__l1_ratio': list(np.arange(0,1.1,init['l1_ratio']).round(2)),
                  'elasticnet__fit_intercept': [True, False]}
    
    features = SimpleImputer(missing_values=np.nan, strategy='most_frequent')
    
    model = ElasticNet(alpha=alpha_i,
                       l1_ratio=l1_i,
                       fit_intercept=True,
                       max_iter=1000, copy_X=True, tol=0.0001)
    
    return init, param_grid, features, model 
    
def elasticReg_GS_gauss(alpha_i=0.5,l1_i=0.5,init={'gauss_w':5,'gauss_ft':10,'alpha':10,'l1_ratio':0.2}):
    from sklearn.linear_model import ElasticNet
    
    param_grid = {'gaussianfeatures__N': list(range(1, init['gauss_ft'])), 
                  'gaussianfeatures__width_factor': np.linspace(0,init['gauss_w'],init['gauss_ft']),
                  'elasticnet__alpha': np.linspace(0.001,1,init['alpha']),
                  'elasticnet__l1_ratio': list(np.arange(0,1.1,init['l1_ratio']).round(2)),
                  'elasticnet__fit_intercept': [True, False]}
    
    features = GaussianFeatures(init['gauss_ft'])
    
    model = ElasticNet(alpha=alpha_i,
                       l1_ratio=l1_i,
                       fit_intercept=True,
                       max_iter=1000, copy_X=True, tol=0.0001)
    
    return init, param_grid, features, model 

#################################################
# Regression Models
#################################################      
def linReg(xl,yl,params={},t=0.75, Plot=False,table=False):
    from sklearn.pipeline import make_pipeline
    from sklearn.model_selection import train_test_split #, test_size=4, random_state=4
    from sklearn.metrics import accuracy_score
    from sklearn.linear_model import LinearRegression
    
    #Split the dataset in two subsets
    if t==1:
        xl_train,yl_train=xl,yl
    else:
        xl_train, xl_test, yl_train, yl_test = train_test_split(xl, yl,train_size=t)
    if params=={}:
        linmodel = LinearRegression(fit_intercept=True)
    else:
        linmodel = LinearRegression(fit_intercept=params['linearregression__fit_intercept'],
                                   normalize=params['linearregression__normalize'])
    #Fit on the train part
    linmodel.fit(xl_train, yl_train)
    
    #Predict over the evenly spaced set derived from xnl
    yhat = linmodel.predict(xl_train)
    
    mse = np.mean((yhat - yl_train)**2)

    if Plot:
        #Compare original data with trained model
        if xl.shape[1]<2:
            plt.scatter(xl, yl)
            plt.plot(xl_train, yhat);
            
    return yhat,mse,linmodel

def printStats(lm, X, y, table=True):
    from scipy import stats
    
    # https://stackoverflow.com/questions/43238173/python-convert-matrix-to-positive-semi-definite/43244194
    def get_near_psd(A):
        C = (A + A.T)/2
        eigval, eigvec = np.linalg.eig(C)
        eigval[eigval < 0] = 0

        return eigvec.dot(np.diag(eigval)).dot(eigvec.T)
        
    def is_pos_semidef(x):
        return np.all(np.linalg.eigvals(x) >= 0)
    
    #https://stackoverflow.com/questions/27928275/find-p-value-significance-in-scikit-learn-linearregression
    #lm = LinearRegression()
    #lm.fit(X,y)
    params = np.append(lm.intercept_,lm.coef_)
    yhat = lm.predict(X)
    newX = np.append(np.ones((len(X),1)), X, axis=1)
    MSE = (sum((y-yhat)**2))/(len(newX)-len(newX[0]))
    var_b = MSE*(np.linalg.inv(np.dot(newX.T,newX)).diagonal())
    sd_b = np.sqrt(var_b)
    ts_b = params/ sd_b
    #p_values =[2*(1-stats.t.cdf(np.abs(i),(len(newX)-len(newX[0])))) for i in ts_b]
    p_values =[2*(1-stats.t.cdf(np.abs(i),(len(newX)-len(newX[0])-1))) for i in ts_b]
    sd_b = np.round(sd_b,3)
    ts_b = np.round(ts_b,3)
    p_values = np.round(p_values,3)
    params = np.round(params,4)
    c_list=[params,sd_b,ts_b,p_values]
    c_df = pd.DataFrame()
    c_df["Coefficients"],c_df["Standard Errors"],c_df["t values"],c_df["Probabilities"] = c_list
    
    #https://stackoverflow.com/questions/42033720/python-sklearn-multiple-linear-regression-display-r-squared
    SS_Residual = sum((y-yhat)**2)       
    SS_Total = sum((y-np.mean(y))**2)     
    r_squared = 1 - (float(SS_Residual))/SS_Total
    adjusted_r_squared = 1 - (1-r_squared)*(len(y)-1)/(len(y)-X.shape[1]-1)
    r_list=[SS_Residual,SS_Total,r_squared,adjusted_r_squared]
    r_df = pd.DataFrame()
    r_df["SS_Residual"],r_df["SS_Total"],r_df["R_squared"],r_df["AdjR_squared"] = r_list
    
    if table:
        print('MSE:', MSE)
        print(r_df)
        print(c_df)
        
    return MSE,c_df,r_df

def polyReg(xnl,ynl,powers_to_plot=[0],papprox=4,params={},t=0.75,Plot=False,Diagn=False,table=False):
    from sklearn.pipeline import make_pipeline
    from sklearn.model_selection import train_test_split #, test_size=4, random_state=4
    from sklearn.metrics import accuracy_score
    from sklearn.linear_model import LinearRegression
    from sklearn.preprocessing import PolynomialFeatures
    
    #Split the dataset in two subsets
    if t==1:
        xnl_train,ynl_train=xnl,ynl
    else:
        xnl_train, xnl_test, ynl_train, ynl_test = train_test_split(xnl, ynl,train_size=t)
    if params=={}:
        poly_model = make_pipeline(PolynomialFeatures(papprox),
                                   LinearRegression())
    else:
        poly_model = make_pipeline(PolynomialFeatures(params['polynomialfeatures__degree']),
                            LinearRegression(fit_intercept=params['linearregression__fit_intercept'],
                            normalize=params['linearregression__normalize']))
        
    #Fit on the train part
    poly_model.fit(xnl_train, ynl_train)
    #Predict over the evenly spaced set derived from xnl
    xfit = np.linspace(xnl.min(), xnl.max(), ynl_train.shape[0])
    yhat = poly_model.predict(xfit[:, np.newaxis])
    
    if Diagn:
        if papprox in powers_to_plot:
            plt.subplot(powers_to_plot[papprox])
            plt.tight_layout()
            plt.plot(xfit,yhat)
            plt.plot(xnl,ynl,'.')
            plt.title('Plot for power: %d'%papprox)    
    if Plot:
        #Compare original data with trained model
        if xnl.shape[1]<2:
            plt.scatter(xnl, ynl)
            plt.plot(xfit, yhat);
    mse=0
    if t < 1:
        #Compare trained model with test dataset
        R2_train=poly_model.score(xnl_train, ynl_train)
        R2_test=poly_model.score(xnl_test, ynl_test)
        mse = np.mean((yhat - ynl_train)**2)
    if table:
        if t < 1: print("R2 Train: % 5.2f, R2 Test: % 5.2f, MSE: % 5.2f" %(R2_train, R2_test,mse))
        eq='y = ' + str((poly_model.steps[1][1].intercept_.round(4)))
        for i in range(len(poly_model.steps[1][1].coef_[0])):
            eq= eq + ' + ' + str(((poly_model.steps[1][1].coef_[0][i].round(4)))) + 'x^' + str(i)
        print(eq)
    return yhat,mse,poly_model
                              
def gaussReg(xnl, ynl,powers_to_plot=[0],g_approx=3,t=0.75,Plot=False,Diagn=False,table=False):
    from sklearn.pipeline import make_pipeline
    from sklearn.model_selection import train_test_split #, test_size=4, random_state=4
    from sklearn.metrics import accuracy_score
    from sklearn.linear_model import LinearRegression
    from sklearn.preprocessing import PolynomialFeatures
    
    #Split the dataset in two subsets
    if t==1:
        xnl_train,ynl_train=xnl,ynl
    else:
        xnl_train, xnl_test, ynl_train, ynl_test = train_test_split(xnl, ynl,train_size=t)
    gauss_model = make_pipeline(GaussianFeatures(g_approx),
                                LinearRegression())
    #Fit on the train part
    gauss_model.fit(xnl_train, ynl_train)
    #Predict over the evenly spaced set derived from xnl
    xfit = np.linspace(xnl.min(), xnl.max(), ynl_train.shape[0])
    yhat = gauss_model.predict(xfit[:, np.newaxis])
    
    if Diagn:
        if g_approx in powers_to_plot:
            plt.subplot(powers_to_plot[g_approx])
            plt.tight_layout()
            plt.plot(xfit,yhat)
            plt.plot(xnl,ynl,'.')
            plt.title('Plot for gaussian: %d'%g_approx)   
    if Plot:
        #Compare original data with trained model
        if xnl.shape[1]<2:
            plt.scatter(xnl, ynl)
            plt.plot(xfit, yhat);
    mse=0
    if t < 1:
        #Compare trained model with test dataset
        R2_train=gauss_model.score(xnl_train, ynl_train)
        R2_test=gauss_model.score(xnl_test, ynl_test)
        mse = np.mean((yhat - ynl_train)**2)
    if table:
        if t < 1:print("R2 Train: % 5.2f, R2 Test: % 5.2f, MSE: % 5.2f" %(R2_train, R2_test,mse))
        eq='y = ' + str((gauss_model.steps[1][1].intercept_.round(4)))
        for i in range(len(gauss_model.steps[1][1].coef_[0])):
            eq= eq + ' + ' + str(((gauss_model.steps[1][1].coef_[0][i].round(4)))) + 'x^' + str(i)
        print(eq)
        if xnl.shape[1]<2:
            basis_plot(gauss_model,xnl,ynl,xfit)
    return yhat,mse,gauss_model

def ridgeReg(xnl, ynl,alphas_to_plot=[0],ridge_a=0.1,t=0.75,Plot=False,Diagn=False,table=False):
    from sklearn.pipeline import make_pipeline
    from sklearn.model_selection import train_test_split #, test_size=4, random_state=4
    from sklearn.metrics import accuracy_score
    from sklearn.linear_model import Ridge
    from sklearn.impute import SimpleImputer
    
    #Split the dataset in two subsets
    if t==1:
        xnl_train,ynl_train=xnl,ynl
    else:
        xnl_train, xnl_test, ynl_train, ynl_test = train_test_split(xnl, ynl,train_size=t)
    #L2 regularization
    #In the limit α→0, we recover the standard linear regression result; 
    #in the limit α→∞, all model responses will be suppressed. 
    ridge_model = make_pipeline(SimpleImputer(missing_values=np.nan, strategy='most_frequent'), Ridge(alpha=ridge_a))
    #Fit on the train part
    ridge_model.fit(xnl_train, ynl_train)
    #Predict over the evenly spaced set derived from xnl
    if xnl.shape[1]<2:
        xfit = np.linspace(xnl.min(), xnl.max(), ynl_train.shape[0])
        yhat = ridge_model.predict(xfit[:, np.newaxis])
    else:
        xfit = xnl_train
        yhat = ridge_model.predict(xfit)
    
    if Diagn and xnl.shape[1]<2:
        if ridge_a in alphas_to_plot:
            plt.subplot(alphas_to_plot[ridge_a])
            plt.tight_layout()
            plt.plot(xfit,yhat)
            plt.plot(xnl,ynl,'.')
            plt.title('Plot for alpha: %.3g'%ridge_a)
    if Plot:
        #Compare original data with trained model
        if xnl.shape[1]<2:
            plt.scatter(xnl, ynl)
            plt.plot(xfit, yhat);
    mse=0
    if t < 1:
        #Compare trained model with test dataset
        R2_train=ridge_model.score(xnl_train, ynl_train)
        R2_test=ridge_model.score(xnl_test, ynl_test)
        mse = np.mean((yhat - ynl_train)**2)
    if table:
        if t < 1:print("R2 Train: % 5.2f, R2 Test: % 5.2f, MSE: % 5.2f" %(R2_train, R2_test,mse))
        eq='y = ' + str((ridge_model.steps[1][1].intercept_.round(4)))
        for i in range(len(ridge_model.steps[1][1].coef_[0])):
            eq= eq + ' + ' + str(((ridge_model.steps[1][1].coef_[0][i].round(4)))) + 'x' + str(i)
        print(eq)
    return yhat,mse,ridge_model
            
def lassoReg(xnl, ynl,alphas_to_plot=[0],lasso_a=0.1,t=0.75,Plot=False,Diagn=False,table=False):
    from sklearn.pipeline import make_pipeline
    from sklearn.model_selection import train_test_split #, test_size=4, random_state=4
    from sklearn.metrics import accuracy_score
    from sklearn.linear_model import Lasso
    from sklearn.impute import SimpleImputer
    
    #Split the dataset in two subsets
    if t==1:
        xnl_train,ynl_train=xnl,ynl
    else:
        xnl_train, xnl_test, ynl_train, ynl_test = train_test_split(xnl, ynl,train_size=t)
    #L1 regularization
    lasso_model = make_pipeline(SimpleImputer(missing_values=np.nan, strategy='most_frequent'), Lasso(alpha=lasso_a))
    #Fit on the train part
    lasso_model.fit(xnl_train, ynl_train)
    #Predict over the evenly spaced set derived from xnl
    if xnl.shape[1]<2:
        xfit = np.linspace(xnl.min(), xnl.max(), ynl_train.shape[0])
        yhat = lasso_model.predict(xfit[:, np.newaxis])
    else:
        xfit = xnl_train
        yhat = lasso_model.predict(xfit)
    
    if Diagn and xnl.shape[1]<2:
        if lasso_a in alphas_to_plot:
            plt.subplot(alphas_to_plot[lasso_a])
            plt.tight_layout()
            plt.plot(xfit,yhat)
            plt.plot(xnl,ynl,'.')
            plt.title('Plot for alpha: %.3g'%lasso_a)
    if Plot:
        #Compare original data with trained model
        if xnl.shape[1]<2:
            plt.scatter(xnl, ynl)
            plt.plot(xfit, yhat);
    mse=0
    if t < 1:
        #Compare trained model with test dataset
        R2_train=lasso_model.score(xnl_train, ynl_train)
        R2_test=lasso_model.score(xnl_test, ynl_test)
        mse = np.mean((yhat - ynl_train)**2)
    if table:
        if t < 1:print("R2 Train: % 5.2f, R2 Test: % 5.2f, MSE: % 5.2f" %(R2_train, R2_test,mse))
        eq='y = ' + str((lasso_model.steps[1][1].intercept_.round(4)))
        if xnl.shape[1]<2:
            for i in range(len(lasso_model.steps[1][1].coef_)):
                eq= eq + ' + ' + str(((lasso_model.steps[1][1].coef_[i].round(4)))) + 'x' + str(i)        
        else:
            for i in range(len(lasso_model.steps[1][1].coef_)):
                eq= eq + ' + ' + str(((lasso_model.steps[1][1].coef_[i].round(4)))) + 'x' + str(i)
        print(eq)
    return yhat,mse,lasso_model
            
def elasticReg(xnl, ynl,alphas_to_plot=[0],alpha=0.1,l1=0.5,t=0.75,Plot=False,Diagn=False,table=False):
    from sklearn.pipeline import make_pipeline
    from sklearn.model_selection import train_test_split #, test_size=4, random_state=4
    from sklearn.metrics import accuracy_score
    from sklearn.linear_model import ElasticNet
    from sklearn.impute import SimpleImputer
    
    #Split the dataset in two subsets
    if t==1:
        xnl_train,ynl_train=xnl,ynl
    else:
        xnl_train, xnl_test, ynl_train, ynl_test = train_test_split(xnl, ynl,train_size=t)
    #L1 regularization
    elastic_model = make_pipeline(SimpleImputer(missing_values=np.nan, strategy='most_frequent'), ElasticNet(alpha=alpha,l1_ratio=l1))
    #Fit on the train part
    elastic_model.fit(xnl_train, ynl_train)
    #Predict over the evenly spaced set derived from xnl
    if xnl.shape[1]<2:
        xfit = np.linspace(xnl.min(), xnl.max(), ynl_train.shape[0])
        yhat = elastic_model.predict(xfit[:, np.newaxis])
    else:
        xfit = xnl_train
        yhat = elastic_model.predict(xfit)
    
    if Diagn and xnl.shape[1]<2:
        if alpha in alphas_to_plot:
            plt.subplot(alphas_to_plot[alpha])
            plt.tight_layout()
            plt.plot(xfit,yhat)
            plt.plot(xnl,ynl,'.')
            plt.title('Plot for alpha: %.3g'%alpha)
    if Plot:
        #Compare original data with trained model
        if xnl.shape[1]<2:
            plt.scatter(xnl, ynl)
            plt.plot(xfit, yhat);
    mse=0
    if t < 1:
        #Compare trained model with test dataset
        R2_train=elastic_model.score(xnl_train, ynl_train)
        R2_test=elastic_model.score(xnl_test, ynl_test)
        mse = np.mean((yhat - ynl_train)**2)
    if table:
        if t < 1:print("R2 Train: % 5.2f, R2 Test: % 5.2f, MSE: % 5.2f" %(R2_train, R2_test,mse))
        eq='y = ' + str((elastic_model.steps[1][1].intercept_.round(4)))
        if xnl.shape[1]<2:
            for i in range(len(elastic_model.steps[1][1].coef_)):
                eq= eq + ' + ' + str(((elastic_model.steps[1][1].coef_[i].round(4)))) + 'x' + str(i)        
        else:
            for i in range(len(elastic_model.steps[1][1].coef_)):
                eq= eq + ' + ' + str(((elastic_model.steps[1][1].coef_[i].round(4)))) + 'x' + str(i)
        print(eq)
    return yhat,mse,elastic_model
     
def ridgeReg_gauss(xnl, ynl,alphas_to_plot=[0],g_approx=3,ridge_a=0.1,t=0.75,Plot=False,Diagn=False,table=False):
    from sklearn.pipeline import make_pipeline
    from sklearn.model_selection import train_test_split #, test_size=4, random_state=4
    from sklearn.metrics import accuracy_score
    from sklearn.linear_model import Ridge
    
    #Split the dataset in two subsets
    if t==1:
        xnl_train,ynl_train=xnl,ynl
    else:
        xnl_train, xnl_test, ynl_train, ynl_test = train_test_split(xnl, ynl,train_size=t)
    #L2 regularization
    #In the limit α→0, we recover the standard linear regression result; 
    #in the limit α→∞, all model responses will be suppressed. 
    ridge_model = make_pipeline(GaussianFeatures(g_approx), Ridge(alpha=ridge_a))
    #Fit on the train part
    ridge_model.fit(xnl_train, ynl_train)
    #Predict over the evenly spaced set derived from xnl
    xfit = np.linspace(xnl.min(), xnl.max(), ynl_train.shape[0])
    yhat = ridge_model.predict(xfit[:, np.newaxis])
    
    if Diagn:
        if ridge_a in alphas_to_plot:
            plt.subplot(alphas_to_plot[ridge_a])
            plt.tight_layout()
            plt.plot(xfit,yhat)
            plt.plot(xnl,ynl,'.')
            plt.title('Plot for alpha: %.3g'%ridge_a)
    if Plot:
        #Compare original data with trained model
        if xnl.shape[1]<2:
            plt.scatter(xnl, ynl)
            plt.plot(xfit, yhat);
    mse=0
    if t < 1:
        #Compare trained model with test dataset
        R2_train=ridge_model.score(xnl_train, ynl_train)
        R2_test=ridge_model.score(xnl_test, ynl_test)
        mse = np.mean((yhat - ynl_train)**2)
    if table:
        if t < 1:print("R2 Train: % 5.2f, R2 Test: % 5.2f, MSE: % 5.2f" %(R2_train, R2_test,mse))
        eq='y = ' + str((ridge_model.steps[1][1].intercept_.round(4)))
        for i in range(len(ridge_model.steps[1][1].coef_)):
            eq= eq + ' + ' + str(((ridge_model.steps[1][1].coef_[i].round(4)))) + 'x^' + str(i)
        print(eq)
        if xnl.shape[1]<2:
            basis_plot(ridge_model, xnl,ynl,xfit,title='Ridge Regression')
    return yhat,mse,ridge_model
            
def lassoReg_gauss(xnl, ynl,alphas_to_plot=[0],g_approx=18,lasso_a=0.1,t=0.75,Plot=False,Diagn=False,table=False):
    from sklearn.pipeline import make_pipeline
    from sklearn.model_selection import train_test_split #, test_size=4, random_state=4
    from sklearn.metrics import accuracy_score
    from sklearn.linear_model import Lasso

    #Split the dataset in two subsets
    if t==1:
        xnl_train,ynl_train=xnl,ynl
    else:
        xnl_train, xnl_test, ynl_train, ynl_test = train_test_split(xnl, ynl,train_size=t)
    #L1 regularization
    lasso_model = make_pipeline(GaussianFeatures(g_approx), Lasso(alpha=lasso_a))
    #Fit on the train part
    lasso_model.fit(xnl_train, ynl_train)
    #Predict over the evenly spaced set derived from xnl
    xfit = np.linspace(xnl.min(), xnl.max(), ynl_train.shape[0])
    yhat = lasso_model.predict(xfit[:, np.newaxis])
    
    if Diagn:
        if lasso_a in alphas_to_plot:
            plt.subplot(alphas_to_plot[lasso_a])
            plt.tight_layout()
            plt.plot(xfit,yhat)
            plt.plot(xnl,ynl,'.')
            plt.title('Plot for alpha: %.3g'%lasso_a)
    if Plot:
        #Compare original data with trained model
        if xnl.shape[1]<2:
            plt.scatter(xnl, ynl)
            plt.plot(xfit, yhat);
    mse=0
    if t < 1:
        #Compare trained model with test dataset
        R2_train=lasso_model.score(xnl_train, ynl_train)
        R2_test=lasso_model.score(xnl_test, ynl_test)
        mse = np.mean((yhat - ynl_train)**2)
    if table:
        if t < 1:print("R2 Train: % 5.2f, R2 Test: % 5.2f, MSE: % 5.2f" %(R2_train, R2_test,mse))
        eq='y = ' + str((lasso_model.steps[1][1].intercept_.round(4)))
        for i in range(len(lasso_model.steps[1][1].coef_)):
            eq= eq + ' + ' + str(((lasso_model.steps[1][1].coef_[i].round(4)))) + 'x^' + str(i)
        print(eq)
        if xnl.shape[1]<2:
            basis_plot(lasso_model, xnl,ynl,xfit,title='Lasso Regression')
    return yhat,mse,lasso_model
    
def elasticReg_gauss(xnl, ynl,alphas_to_plot=[0],g_approx=18,alpha=0.1,l1=0.5,t=0.75,Plot=False,Diagn=False,table=False):
    from sklearn.pipeline import make_pipeline
    from sklearn.model_selection import train_test_split #, test_size=4, random_state=4
    from sklearn.metrics import accuracy_score
    from sklearn.linear_model import ElasticNet

    #Split the dataset in two subsets
    if t==1:
        xnl_train,ynl_train=xnl,ynl
    else:
        xnl_train, xnl_test, ynl_train, ynl_test = train_test_split(xnl, ynl,train_size=t)
    #L1 regularization
    elastic_model = make_pipeline(GaussianFeatures(g_approx), ElasticNet(alpha=alpha,l1_ratio=l1))
    #Fit on the train part
    elastic_model.fit(xnl_train, ynl_train)
    #Predict over the evenly spaced set derived from xnl
    xfit = np.linspace(xnl.min(), xnl.max(), ynl_train.shape[0])
    yhat = elastic_model.predict(xfit[:, np.newaxis])
    
    if Diagn:
        if alpha in alphas_to_plot:
            plt.subplot(alphas_to_plot[alpha])
            plt.tight_layout()
            plt.plot(xfit,yhat)
            plt.plot(xnl,ynl,'.')
            plt.title('Plot for alpha: %.3g'%alpha)
    if Plot:
        #Compare original data with trained model
        if xnl.shape[1]<2:
            plt.scatter(xnl, ynl)
            plt.plot(xfit, yhat);
    mse=0
    if t < 1:
        #Compare trained model with test dataset
        R2_train=elastic_model.score(xnl_train, ynl_train)
        R2_test=elastic_model.score(xnl_test, ynl_test)
        mse = np.mean((yhat - ynl_train)**2)
    if table:
        if t < 1:print("R2 Train: % 5.2f, R2 Test: % 5.2f, MSE: % 5.2f" %(R2_train, R2_test,mse))
        eq='y = ' + str((elastic_model.steps[1][1].intercept_.round(4)))
        for i in range(len(elastic_model.steps[1][1].coef_)):
            eq= eq + ' + ' + str(((elastic_model.steps[1][1].coef_[i].round(4)))) + 'x^' + str(i)
        print(eq)
        if xnl.shape[1]<2:
            basis_plot(elastic_model, xnl,ynl,xfit,title='Lasso Regression')
    return yhat,mse,elastic_model
