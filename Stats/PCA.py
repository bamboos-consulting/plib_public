#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# PCA 
#
# Module including functions to compute principal component analysis on stock indices
#############################################################################################      

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path+"/")

import Plib.Plotting.Timing as pt

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from dateutil import parser
from time import gmtime, strftime
from datetime import timedelta, date

#Intel(R) Extension for Scikit-learn dynamically patches scikit-learn estimators to use oneDAL as the underlying solver
#from daal4py.sklearn import patch_sklearn
from sklearnex import patch_sklearn
patch_sklearn()

##############################################
# Returns company's metadata
##############################################    

##################################################
# Obtain assets prices, returns, demeaned returns
##################################################    
def getAssetPrices(SP_tickers,index_ticker,dt_start, dt_end,dwld=True,hist={},provider='mstack',assets='stock'):
    import time
    #import Plib.DataFarm.MStack as datafarm1
    if provider=='mstack':
        import Plib.DataFarm.MStack as datafarm1
    elif provider=='kibot':
        import Plib.DataFarm.Kibot as datafarm1
    else:
        import Plib.DataFarm.FRData as datafarm1
    
    startt=time.perf_counter()    
    #first_ticker=SP_tickers[0]
    symbol = index_ticker
    if dwld:
        if provider=='mstack':
            df = datafarm1.get_eod_data(symbol, dt_start, dt_end)
        elif provider=='kibot':
            df = datafarm1.get_eod_data(symbol, dt_start, dt_end, adj=0)
        else:
            df = datafarm1.getData('indices',symbol,d1=dt_start,d2=dt_end,freq='D')
    else:
        df=hist[str(symbol)]
        df = df.sort_index(ascending=True)
        mask = (df.index >= dt_start) & (df.index <= dt_end)
        df = df.loc[mask]
    df=df.drop(df.columns[[0, 1, 2, 4,5]], axis=1)
    df.columns = [symbol]
    spy_comp=df
    counter=1

    mmod=int(len(SP_tickers)/6)
    for x in SP_tickers:
        symbol=x

        if symbol != index_ticker: 
            try:
                if dwld:
                    if provider=='mstack':
                        df = datafarm1.get_eod_data(symbol, dt_start, dt_end)
                    elif provider=='kibot':
                        df = datafarm1.get_eod_data(symbol, dt_start, dt_end, adj=0)
                    else:
                        df = datafarm1.getData(assets,symbol,d1=dt_start,d2=dt_end,freq='D')
                else:
                    df=hist[str(symbol)]
                    df = df.sort_index(ascending=True)
                    mask = (df.index >= dt_start) & (df.index <= dt_end)
                    df = df.loc[mask]
                df=df.drop(df.columns[[0, 1, 2, 4,5]], axis=1)
                df.columns = [symbol]
                spy_comp[symbol]=df
                counter=counter+1
                if counter % mmod == 0:
                    endt=time.perf_counter()
                    print('Obtained ', counter,' securities of ', str(len(SP_tickers)), ' - ',str(endt-startt),' seconds')
            except:
                print('Security not available: ', counter,symbol)

    cols_at_end = [index_ticker]
    df = spy_comp[[c for c in spy_comp if c not in cols_at_end] 
            + [c for c in cols_at_end if c in spy_comp]]
    
    df['Date'] = pd.to_datetime(df.index)
    df.set_index('Date',inplace=True)
    return df

def getDemeanedReturns(asset_prices):
    asset_returns = pd.DataFrame(data=np.zeros(shape=(len(asset_prices.index), asset_prices.shape[1])), 
                                 columns=asset_prices.columns.values,
                                 index=asset_prices.index)
    normed_returns = asset_returns
    asset_returns=asset_prices.pct_change()[1:]
    asset_returns=asset_returns.replace([np.inf, -np.inf], np.nan).interpolate()

    normed_returns = (asset_returns - asset_returns.mean()) / asset_returns.std()
    normed_returns=normed_returns.replace([np.inf, -np.inf], np.nan).interpolate()
    return asset_returns,normed_returns

##################################################
# PCA Functions
##################################################    
def getPCA(normed_returns,index_ticker,n_comp=20):
    from sklearn.decomposition import PCA as sklearnPCA
    sklearn_pca = sklearnPCA(n_components=n_comp) # let's look at the first 20 components
    cov_matrix = normed_returns.loc[:, normed_returns.columns != index_ticker].cov()  
    pc = sklearn_pca.fit(cov_matrix)
    # plot the variance explained by pcs
    fig=plt.figure(figsize=(12,8), dpi= 100, facecolor='w', edgecolor='k')
    plt.bar(range(20),sklearn_pca.explained_variance_ratio_)
    plt.title('variance explained by pc')
    plt.show()
    return pc, cov_matrix,sklearn_pca

def plotPCAExplainedVariance(sklearn_pca,normed_returns,asset_prices,index_ticker):
    # check the explained variance reatio
    sklearn_pca.explained_variance_ratio_

    # get the Principal components
    pcs =sklearn_pca.components_

    # first component
    pc1 = pcs[0,:]
    # normalized to 1 
    pc_w = np.asmatrix(pc1/sum(pc1)).T

    # apply our first componenet as weight of the stocks
    pc1_ret = normed_returns.loc[:, normed_returns.columns != index_ticker].values*pc_w

    # plot the total return index of the first PC portfolio
    pc_ret = pd.DataFrame(data =pc1_ret, index= normed_returns.index)
    pc_ret_idx = pc_ret+1
    pc_ret_idx= pc_ret_idx.cumprod()
    pc_ret_idx.columns =['pc1']

    indu_index=normed_returns[index_ticker]
    pc_ret_idx[index_ticker] = indu_index[1:]
    pc_ret_idx.plot(subplots=True,title ='PC portfolio vs Market',layout =[1,2],figsize=(12,8 ))
    
    # plot the weights in the PC
    weights_df = pd.DataFrame(data = pc_w*100,index = asset_prices.columns.values[:-1])
    weights_df.columns=['weights']
    weights_df.plot.bar(title='PCA portfolio weights',rot =45,fontsize =8,figsize=(20,6 ))
    
def fitPCA(normed_returns,asset_returns,dt_train,index_ticker,var_thres):
    from sklearn.decomposition import PCA
    import seaborn as sns

    stock_tickers = normed_returns.columns.values[:-1]
    assert index_ticker not in stock_tickers, "By accident included index: " + index_ticker
    
    #datetime.datetime(2020, 1, 18) 
    train_end = dt_train

    df_train = None
    df_test = None
    df_raw_train = None
    df_raw_test = None
    
    df_train = normed_returns[normed_returns.index <= train_end].copy()
    df_test = normed_returns[normed_returns.index > train_end].copy()

    df_raw_train = asset_returns[asset_returns.index <= train_end].copy()
    df_raw_test = asset_returns[asset_returns.index > train_end].copy()

    print('Train dataset:', df_train.shape)
    print('Test dataset:', df_test.shape)

    n_tickers = len(stock_tickers)
    pca = None
    cov_matrix = pd.DataFrame(data=np.ones(shape=(n_tickers, n_tickers)), columns=stock_tickers)
    cov_matrix_raw = cov_matrix
    num_comp=0
    
    if df_train is not None and df_raw_train is not None:
        stock_tickers = asset_returns.columns.values[:-1]
        assert index_ticker not in stock_tickers, "By accident included index: " + index_ticker

        cov_matrix = df_train.loc[:, df_train.columns != index_ticker].cov()   
        cov_matrix=cov_matrix.replace([np.inf, -np.inf], np.nan).interpolate()

        # computing PCA on S&P 500 stocks
        pca = PCA().fit(cov_matrix)
        # not normed covariance matrix
        cov_matrix_raw = df_raw_train.loc[:, df_raw_train.columns != index_ticker].cov()  
        cov_matrix_raw=cov_matrix_raw.replace([np.inf, -np.inf], np.nan).interpolate()

        cov_raw_df = pd.DataFrame({'Variance': np.diag(cov_matrix_raw)}, index=stock_tickers)    
        # cumulative variance explained
        var_threshold = var_thres
        var_explained = np.cumsum(pca.explained_variance_ratio_)
        num_comp = np.where(np.logical_not(var_explained < var_threshold))[0][0] + 1  # +1 due to zero based-arrays
        print('%d components explain %.2f%% of variance' %(num_comp, 100* var_threshold))

        if pca is not None:
            bar_width = 0.9
            n_asset = int((1 / 10) * normed_returns.shape[1])
            x_indx = np.arange(n_asset)
            fig, ax = plt.subplots()
            fig.set_size_inches(12, 4)
            # Eigenvalues are measured as percentage of explained variance.
            rects = ax.bar(x_indx, pca.explained_variance_ratio_[:n_asset], bar_width, color='deepskyblue')
            ax.set_xticks(x_indx + bar_width / 2)
            ax.set_xticklabels(list(range(n_asset)), rotation=45)
            ax.set_title('Percent variance explained')
            ax.legend((rects[0],), ('Percent variance explained by principal components',))
    if pca is not None:
        projected = pca.fit_transform(cov_matrix)
    return pca,cov_matrix,projected,df_train,df_test,df_raw_train,df_raw_test,num_comp

def getComponent(asset_returns,pca,K):
    stock_tickers = asset_returns.columns.values[:-1]
    pc_w = np.zeros(len(stock_tickers))
    eigen_prtf1 = pd.DataFrame(data ={'weights': pc_w.squeeze()*100}, index = stock_tickers)
    if pca is not None:
        pcs = pca.components_

        # normalized to 1 
        pc_w = pcs[:, K-1] / sum(pcs[:, K-1])

        eigen_prtf = pd.DataFrame(data ={'weights': pc_w.squeeze()*100}, index = stock_tickers)
        eigen_prtf.sort_values(by=['weights'], ascending=False, inplace=True)
        print('Sum of weights of eigen-portfolio N.'+str(K)+': %.2f' % np.sum(eigen_prtf))
        eigen_prtf.plot(title='Eigen-portfolio N.'+str(K)+' weights', 
                         figsize=(12,6), 
                         xticks=range(0, len(stock_tickers),10), 
                         rot=45, 
                         linewidth=3)
    return eigen_prtf

##################################################
# Performance Functions
##################################################    
def plotPerformanceEigenPortfolio(df_raw_test,df_test,eigen_prtf,index_ticker,desc_eport):
    eigen_prtf_returns = np.dot(df_raw_test.loc[:, eigen_prtf.index], eigen_prtf / 100)
    eigen_prtf_returns = pd.Series(eigen_prtf_returns.squeeze(), index=df_test.index)
    er, vol, sharpe = sharpe_ratio(eigen_prtf_returns)
    
    print(desc_eport+':\nReturn = %.2f%%\nVolatility = %.2f%%\nSharpe = %.2f' % (round(er*100,2), round(vol*100,2), round(sharpe,2)))
    year_frac = (eigen_prtf_returns.index[-1] - eigen_prtf_returns.index[0]).days / 252

    df_plot = pd.DataFrame({'EP': eigen_prtf_returns, 'SPX': df_raw_test.loc[:, index_ticker]}, index=df_test.index)
    np.cumprod(df_plot + 1).plot(title='Returns of the market-cap weighted index vs. '+ desc_eport, 
                             figsize=(12,6), linewidth=3)

##################################################
# Helper Functions
##################################################    
def addDescription1(symbol_list,portfolio,ticker_des):
    tf=portfolio.head(len(symbol_list))
    short_p2=tf.reset_index()
    ret=pd.merge(short_p2, ticker_des, left_on='index', right_on='symbol')
    del ret['index']
    del ret['has_intraday']
    del ret['has_eod']
    ret=ret[['symbol','name','weights']]
    return ret

def addDescription2(symbol_list,portfolio,ticker_des):
    tf=portfolio.tail(len(symbol_list))
    short_p2=tf.reset_index()
    ret=pd.merge(short_p2, ticker_des, left_on='index', right_on='symbol')
    del ret['index']
    del ret['has_intraday']
    del ret['has_eod']
    ret=ret[['symbol','name','weights']]
    return ret

def addIndustrySector(symbol_list,portfolio):
    import Plib.DataFarm.Fprep as datafarm2

    cols=['symbol','beta','mktCap','industry','sector']
    df=pd.DataFrame(cols)
    df=get_metaByTicker(symbol_list.index[0])
    for my_symbol in symbol_list.index:
        if my_symbol != symbol_list.index[0]:
            temp=datafarm2.get_metaByTicker(my_symbol)
            df = df.append(temp, ignore_index=True)

    tf=portfolio.tail(len(symbol_list))
    short_p2=tf.reset_index()
    ret=pd.merge(short_p2, df, left_on='index', right_on='symbol')
    return ret

def sharpe_ratio(ts_returns, periods_per_year=252):
    annualized_return = 0.
    annualized_vol = 0.
    annualized_sharpe = 0.
    
    n_years = ts_returns.shape[0] / periods_per_year
    annualized_return = np.power(np.prod(1 + ts_returns),(1 / n_years)) - 1
    annualized_vol = ts_returns.std() * np.sqrt(periods_per_year)
    annualized_sharpe = annualized_return / annualized_vol
    return annualized_return, annualized_vol, annualized_sharpe

def getExchangesByCountry(edf,country):
    filter = edf["country"]==country
    df1=edf.where(filter, inplace = False) 
    df1=df1.dropna()
    return df1

def getTickers(mic):
    import Plib.DataFarm.MStack as datafarm1
    
    tdf=datafarm1.getExchangeTickers(mic)
    print('Total stocks: ',len(tdf))
    return tdf,sorted(tdf['symbol'])


##################################################
# Wrapper Functions
##################################################    
def simulatePortfolios(sims,pca,asset_returns,df_raw_test,df_test):
    n_portfolios = sims
    annualized_ret = np.array([0.] * n_portfolios)
    sharpe_metric = np.array([0.] * n_portfolios)
    annualized_vol = np.array([0.] * n_portfolios)
    sim_prtf={}
    idx_highest_sharpe = 0 # index into sharpe_metric which identifies a portfolio with rhe highest Sharpe ratio

    if pca is not None:
        stock_tickers = asset_returns.columns.values[:-1]
        pcs = pca.components_
        for ix in range(n_portfolios):
            pc_w = pcs[:, ix] / sum(pcs[:, ix])
            eigen_prtfix = pd.DataFrame(data ={'weights': pc_w.squeeze()*100}, index = stock_tickers)
            eigen_prtfix.sort_values(by=['weights'], ascending=False, inplace=True)

            eigen_prtix_returns = np.dot(df_raw_test.loc[:, eigen_prtfix.index], eigen_prtfix / 100)
            eigen_prtix_returns = pd.Series(eigen_prtix_returns.squeeze(), index=df_test.index)
            er, vol, sharpe = sharpe_ratio(eigen_prtix_returns)
            annualized_ret[ix] = round(er,2)
            annualized_vol[ix] = round(vol,2)
            sharpe_metric[ix] = round(sharpe,2)
            sim_prtf[ix]=eigen_prtfix
            
        # find portfolio with the highest Sharpe ratio
        idx_highest_sharpe = np.nanargmax(sharpe_metric)
        
        print('Eigen portfolio #%d with the highest Sharpe. Return %.2f%%, vol = %.2f%%, Sharpe = %.2f' % 
              (idx_highest_sharpe,
               annualized_ret[idx_highest_sharpe]*100, 
               annualized_vol[idx_highest_sharpe]*100, 
               sharpe_metric[idx_highest_sharpe]))

        fig, ax = plt.subplots()
        fig.set_size_inches(12, 4)
        ax.plot(sharpe_metric, linewidth=3)
        ax.set_title('Sharpe ratio of eigen-portfolios')
        ax.set_ylabel('Sharpe ratio')
        ax.set_xlabel('Portfolios')
        
        results = pd.DataFrame(data={'Return': annualized_ret, 'Vol': annualized_vol, 'Sharpe': sharpe_metric})
        results.dropna(inplace=True)
        results.sort_values(by=['Sharpe'], ascending=False, inplace=True)
        
        display(results.head(10)); display(sim_prtf[idx_highest_sharpe].head(20))
        
        return results,sim_prtf
        
def preparePCA(index_ticker,assets='stock',provider='frdata',download=False,show=True,data={},bck1='PCA_data_FRD',bck2='PCA_params_FRD'):
    import pickle
    import Plib.DataFarm.FRData as fr
    import Plib.DataFarm.Kibot as kb
 
    save=False
    if provider=='frdata':
        tickers=list((fr.getTickers(assets)).values)
    elif provider=='kibot':
        tickers=kb.getTickers(assets)
    tickers=[t for t in tickers if not ((str(t).find('-')>0) or (str(t).find('_')>0) or (str(t).find('.')>0))]
    
    today_date=strftime("%Y-%m-%d %H:%M:%S", gmtime())
    dt_start = (parser.parse(today_date) + timedelta(days=-120)).strftime("%Y-%m-%d")
    dt_end = (parser.parse(today_date) + timedelta(days=-3)).strftime("%Y-%m-%d")
    dt_train=(parser.parse(today_date) + timedelta(days=-40)).strftime("%Y-%m-%d")
    
    if download:
        #Set the dates interval for downloading data
        asset_prices=getAssetPrices(tickers,index_ticker,dt_start, dt_end,provider=provider,assets=assets)
        save=True
    elif not download and data !={}:
        idx = kb.get_eod_data(index_ticker, dt_start, dt_end)[['Close']]
        idx.columns=[index_ticker]
        asset_prices=pt.dictdf2df2(data,dt_start,dt_end, lcols=tickers)
        asset_prices=asset_prices.join(idx).interpolate().ffill().fillna(0)
        save=True
    if save:
        asset_prices.to_pickle(bck1)
        other_data={}
        other_data['index_ticker']=index_ticker
        other_data['tickers']=tickers
        other_data['today_date']=today_date
        other_data['dt_start']=dt_start
        other_data['dt_end']=dt_end
        other_data['dt_train']=dt_train
        with open(bck2, 'wb') as handle:
            pickle.dump(other_data, handle, protocol=pickle.HIGHEST_PROTOCOL)        
    if not download and data == {}:
        with open(bck2, 'rb') as handle:
            other_data = pickle.load(handle)
        today_date= other_data['today_date']
        dt_start = other_data['dt_start']
        dt_end = other_data['dt_end']
        dt_train= other_data['dt_train']
        tickers=other_data['tickers']
        asset_prices = pd.read_pickle(bck1)
        asset_prices['Date'] = pd.to_datetime(asset_prices.index)
        asset_prices.set_index('Date',inplace=True)
    if show:
        n_stocks_show = 12
        print('Asset prices shape', asset_prices.shape)
        display(asset_prices.iloc[:, :n_stocks_show].head())

    #Compute asset returns and centre the data by demeaning
    #matrix X <- asset_returns
    #matrix B <- normed_returns
    asset_returns,normed_returns=getDemeanedReturns(asset_prices)
    if show:
        display(normed_returns.iloc[-5:, -10:].head())

    #Check for NaN values and remove tickers
    nan_list=normed_returns.columns[normed_returns.isna().any()].tolist()
    for s in nan_list:
        del asset_prices[s]
        del asset_returns[s]
        del normed_returns[s]
        
    return asset_prices,asset_returns,normed_returns,dt_train
    
def computePCA(index_ticker,normed_returns,asset_prices,asset_returns,dt_train,n_comp=20,var_thres=0.8):
    #Obtain the Covariance Matrix 
    #matrix C <- cov_matrix
    pc, cov_matrix,sklearn_pca=getPCA(normed_returns,index_ticker,n_comp=n_comp)
    plotPCAExplainedVariance(sklearn_pca,normed_returns,asset_prices,index_ticker)

    #Compute covariance matrix using training data set
    #Fit PCA model to cov_matrix and assign fitted model to pca
    pca,cov_matrix,projected,df_train,df_test,df_raw_train,df_raw_test,num_comp=fitPCA(normed_returns,asset_returns,dt_train,index_ticker,var_thres)
    return pca,df_raw_test,df_test,num_comp

def computeEigenPorts(index_ticker,pca,num_comp,asset_returns,df_raw_test,df_test):
    eigenPort={}
    for i in range(1,num_comp+1):
        #Compute the Eigen-portfolios representing the directions of maximum variance in the data
        eigenPort[i]=getComponent(asset_returns,pca,i)
        if df_raw_test is not None:
            plotPerformanceEigenPortfolio(df_raw_test,df_test,eigenPort[i],index_ticker,'Eigen-portfolio N.'+str(i))

def createLongShort(pca,asset_returns,portN,assets,firstTickers=100,provider='kibot'):
    eigen_prtf=getComponent(asset_returns,pca,portN)
    myport=eigen_prtf.reset_index()
    myport.columns=['Ticker','Weights']
    myport_long=myport.head(firstTickers)
    myport_short=myport.tail(firstTickers)

    if assets=='stock':
        if provider=='frdata':
            import Plib.DataFarm.FRData as fr
            stocks=fr.getFundD()
            stocks=stocks[['Ticker','Industry','Sector','Description']]
        elif provider=='kibot':
            import Plib.DataFarm.Kibot as kb
            stocks=kb.getTickers(tname=assets,ext=True)
            stocks=stocks[['Symbol','Industry','Sector','Description']]
            stocks.columns=['Ticker','Industry','Sector','Description']
        myport_long=pd.merge(myport_long, stocks, on ='Ticker', how ="inner")
        myport_short=pd.merge(myport_short, stocks, on ='Ticker', how ="inner")
        display(myport_long);display(myport_short)

