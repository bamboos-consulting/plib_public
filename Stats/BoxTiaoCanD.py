#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Timeseries 
#
# Module comprising helper functions to perform timeseries analysis
#############################################################################################      
from __future__ import annotations

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

try:
    from typing import Literal
except ImportError:
    from typing_extensions import Literal,Protocol
from typing import Optional, NoReturn, Union
from collections.abc import Sequence
from dataclasses import dataclass

import numpy as np
from statsmodels.distributions.empirical_distribution import ECDF
from statsmodels.tsa.stattools import adfuller

#from btcd import decompose, RegressionModel, CanDecompSolution


"""Box-Tiao Canonical Decomposition."""

from dataclasses import dataclass
#from typing import Protocol, Any
from typing import Any

import numpy as np
from scipy.linalg import fractional_matrix_power


# ------------------------------------------------------------------------------
# Regression Model Interface


class RegressionModel(Protocol):
    """
    Regression model interface.
    Sklearn regression model compliant.
    """

    def fit(self, X: np.ndarray, y: np.ndarray) -> Any:
        """
        Fit model to X and y.
        Sklearn Regression model compliant.
        - X is a numpy 2D array of shape (n_samples, n_features)
        - y is a numpy 1D array of shape (n_samples,)
        """

    def predict(self, X: np.ndarray) -> np.ndarray:
        """
        Model prediction method.
        - X is a numpy 2D array of shape (n_samples, n_features)
        returns a 1D array of shape (X.shape[0],).
        """


# ------------------------------------------------------------------------------
# Decomposition Public


@dataclass
class CanDecompSolution:
    lambdas: np.ndarray  # 1D array, lambdas are indexed in ascending order.
    w_vecs: np.ndarray  # 2D array, columns are indexed as lambdas.


def decompose(
    p_mat: np.ndarray, regression_model: RegressionModel, max_lag: int = 1
) -> CanDecompSolution:
    """
    All canonical decomposition steps, calculate final answer.
    p_mat is of shape(t,n), i.e. each column is a time series.
    """
    B_sqrt_inv = _get_B_sqrt_inv(p_mat)
    A = _get_A(p_mat, regression_model, max_lag=max_lag)
    D = np.matmul(np.matmul(B_sqrt_inv, A), B_sqrt_inv)
    eigen_solution = _get_eigensolution(D)
    return CanDecompSolution(
        lambdas=eigen_solution.eigval, 
        w_vecs=np.matmul(B_sqrt_inv, eigen_solution.eigvec)
    )


# ------------------------------------------------------------------------------
# Private


def _get_expected_dyadic_prod(V: np.ndarray) -> np.ndarray:
    """
    Returns E[v^T v] for dim(v) = n and t observations.
    V shape is (t, n).
    """
    return (1.0 / V.shape[0]) * np.matmul(V.T, V)


# ---------------------------------------------------
# B


def _get_B(p_mat: np.ndarray) -> np.ndarray:
    """Rows of p_mat represent t index, columns represent each path."""
    return _get_expected_dyadic_prod(p_mat)


def _get_mat_sqrt(V: np.ndarray) -> np.ndarray:
    """Square root of matrix V."""
    return fractional_matrix_power(V, 0.5)


def _get_inv_mat(mat: np.ndarray) -> np.ndarray:
    """Matrix inverse."""
    return np.linalg.inv(mat)


def _get_B_sqrt_inv(p_mat: np.ndarray) -> np.ndarray:
    """Rows of p_mat represent t index, columns represent each path."""
    B = _get_B(p_mat)
    B_sqrt = _get_mat_sqrt(B)
    return _get_inv_mat(B_sqrt)


# ---------------------------------------------------
# A


def _get_X(p_mat: np.ndarray, max_lag: int) -> np.ndarray:
    """
    Lags the columns of p_mat into the feature array X.
    Returns a 2D array of (T-max_lag, n_features)
    - p_mat is of shape (T-max_lag, n)
    * n_features = max_lag * n.
    """
    return np.concatenate(
        [p_mat[max_lag - lag : -lag, :] for lag in range(1, max_lag + 1)], axis=1
    )


def _get_y(p_mat: np.ndarray, p_mat_col_idx: int, max_lag: int) -> np.ndarray:
    """
    Returns a 1D array which corresonds to a specific column of p_mat,
    with the first max_lag idxs trimmed.
    the index of this column is p_mat_col_idx
    """
    return p_mat[max_lag:, p_mat_col_idx]


def _get_q_t(
    regression_model: RegressionModel, X: np.ndarray, y: np.ndarray
) -> np.ndarray:
    """
    Expected value for p_t (q model) using RegressionModel.
    - X is a numpy 2D array of shape (T-max_lag, n_features)
    - y is a numpy 1D array of shape (T-max_lag,)
    """
    regression_model.fit(X, y)
    return regression_model.predict(X)


def _get_A(
    p_mat: np.ndarray, regression_model: RegressionModel, max_lag: int = 1
) -> np.ndarray:
    """Estimate A using an instance of RegressionModel."""
    X = _get_X(p_mat, max_lag)
    qs = []
    # model each column j of p_mat.
    for j in range(p_mat.shape[1]):
        y = _get_y(p_mat, j, max_lag)
        q_j = _get_q_t(regression_model, X, y)
        qs.append(q_j)
    q_mat = np.asarray(qs).T
    return _get_expected_dyadic_prod(q_mat)

# ---------------------------------------------------
# Eigensolution


@dataclass
class _EigenSolution:
    eigval: np.ndarray  # 1D array, eigen values indexed in ascending order.
    eigvec: np.ndarray  # 2D array, columns are indexed as eigval.


def _get_eigensolution(mat: np.ndarray) -> _EigenSolution:
    eigenvalues, eigenvectors = np.linalg.eigh(mat)
    return _EigenSolution(eigval=eigenvalues, eigvec=eigenvectors)
    
    
# ----------------------------------------------------------------
# Probabilities


class BTCDProbTable:
    """
    - T is the sample size (number of observations).
    - N is the number of time series in the model.
    - max_lag the maximum number of time lags considered in the model.
    - regression_model is an interface for the regression model.
    - n_MC is the number of trials in out Monte Carlo Simulation.
    """

    def __init__(
        self,
        T: int,
        N: int,
        max_lag: int,
        regression_model: RegressionModel,
        n_MC: int = 1_000,
    ) -> None:
        self.T = T
        self.N = N
        self.max_lag = max_lag
        self.regression_model = regression_model
        self.n_MC = n_MC

        self._sim_has_run = False
        self._min_lambda_cdf: Optional[ECDF] = None
        self._max_lambda_cdf: Optional[ECDF] = None

    def run_simulation(self) -> None:
        """
        Run Monte Carlo simulation and get CDFs and ICDFs for
        min_lambda and max_lambda.
        """
        sim_result = run_btcd_montecarlo_sim(
            self.T, self.N, self.max_lag, self.regression_model, n_MC=self.n_MC
        )
        self._min_lambda_cdf, self._max_lambda_cdf = get_BTCD_cdfs(sim_result)
        self._sim_has_run = True

    def get_prob(
        self,
        lambda_value: Union[float, int, np.ndarray, Sequence],
        prob_type: Literal["stationary", "trending"],
    ) -> Union[np.float64, np.ndarray]:
        """
        - lambda_value can be a scalar or an array_like,
            just like numpy vectorized functions.
        - prob_type uses min_lambdas for stationary and max_lambdas for trending
            but flips the the CDF so that smaller lambda -> larger probability for
            stationary, but larger lambda -> larger probability for trending.
        Returns a np.float64 is lambda_value was a scalar, or np.ndarray
        if prob was array_like.
        """
        if not self._sim_has_run:
            self._handle_no_sim()
        if prob_type == "stationary":
            # complementary probability
            return 1.0 - self._min_lambda_cdf(lambda_value)  # type: ignore
        elif prob_type == "trending":
            return self._max_lambda_cdf(lambda_value)  # type: ignore

    @staticmethod
    def _handle_no_sim() -> NoReturn:
        """Raise exception when no simulation has run."""
        raise RuntimeError("run_simulation method has not been called.")

# ----------------------------------------------------------------
# brownian motions


def get_brownian_motions(
    T: int, N: int, random_state: Optional[int] = None
) -> np.ndarray:
    """
    - T is the sample size.
    - N the number of processes.
    - random_state an optional integer to reproduce results.
    Returns a 2D array of shape (T, N), where each column is one process.
    The resulting processes are uncorrelated, all increments are i.i.d
    """
    rng = np.random.default_rng(random_state)
    return rng.normal(size=(T, N)).cumsum(axis=0)


# ----------------------------------------------------------------
# BTCD Monte Carlo Simulation


@dataclass
class LambdaSimResult:
    lambdas: np.ndarray  # the values of lambda from the simulation
    dickey_fuller: np.ndarray  # the values of dickey fuller test applied to S_t


@dataclass
class BTCDMonteCarloResult:
    min_lambda: LambdaSimResult  # results for the decomposition with min lambda
    max_lambda: LambdaSimResult  # results for the decomposition with max lambda


def run_btcd_montecarlo_sim(
    T: int,
    N: int,
    max_lag: int,
    regression_model: RegressionModel,
    n_MC: int = 1_000,
) -> BTCDMonteCarloResult:
    """
    Run the Monte Carlo Simulation.
    - T is the sample size for each brownian motion.
    - N is the number of brownian motions.
    - max_lag the max_lag to be considered in BTCD.
    - n_MC the number of trials in the simulation.
    """
    lambdas_min, DFs_min = [], []
    lambdas_max, DFs_max = [], []
    for _ in range(n_MC):
        # new brownian_motions in each iteration
        brownian_motions = get_brownian_motions(T=T, N=N)
        # apply the decomposition to the brownian motions
        decomp_solution = decompose(
            brownian_motions, regression_model, max_lag=max_lag
        )
        # get the results for min, max and append them
        lambda_min, DF_min = _get_lambda_sim_result_n(
            brownian_motions, decomp_solution, "min"
        )
        lambdas_min.append(lambda_min)
        DFs_min.append(DF_min)
        lambda_max, DF_max = _get_lambda_sim_result_n(
            brownian_motions, decomp_solution, "max"
        )
        lambdas_max.append(lambda_max)
        DFs_max.append(DF_max)
    # Return an instance of BTCDMonteCarloResult.
    return BTCDMonteCarloResult(
        min_lambda=LambdaSimResult(
            lambdas=np.asarray(lambdas_min), dickey_fuller=np.asarray(DFs_min)
        ),
        max_lambda=LambdaSimResult(
            lambdas=np.asarray(lambdas_max), dickey_fuller=np.asarray(DFs_max)
        ),
    )


def _get_lambda_sim_result_n(
    brownian_motions: np.ndarray,
    decomp_solution: CanDecompSolution,
    opt: Literal["min", "max"],
) -> tuple[float, float]:
    """
    - brownian_motions is 2D array of shape (T, N),
        where each column is one process.
    Returns tuple of (lambda (BTCD), dickey fuller test statistic).
    """
    if opt == "min":
        lambda_ = decomp_solution.lambdas[0]
        w_vec = decomp_solution.w_vecs[:, 0]
    elif opt == "max":
        lambda_ = decomp_solution.lambdas[-1]
        w_vec = decomp_solution.w_vecs[:, -1]

    S_t = np.dot(w_vec, brownian_motions.T)
    return lambda_, float(adfuller(S_t, regression="c", maxlag=1)[0])


# ----------------------------------------------------------------
# CDFs


def get_BTCD_cdfs(sim_result: BTCDMonteCarloResult) -> tuple[ECDF, ECDF]:
    """
    Returns a tuple of statsmodels ECDF instances, which are themselves
    callables (functions), the first element of the tuple corresponds
    to min_lambda, the second to max_lambda, i.e.
    (min_lambda_cdf, max_lambda_cdf).
    """
    return ECDF(sim_result.min_lambda.lambdas), ECDF(
        sim_result.max_lambda.lambdas
    )