#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
# Wildely inspired by Stuart Reid (http://www.stuartreid.co.za)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Create SDE processes
#
# Module comprising plot and helper functions
#############################################################################################      

import math
import numpy
import random
import decimal
import scipy.linalg
import numpy.random as nrand
import matplotlib.pyplot as plt


#################################################################
# Parameters of the models
#################################################################    
class ModelParameters:
    
    def __init__(self,
                 all_s0, all_time, all_delta, all_sigma, gbm_mu,
                 jumps_lamda=0.0, jumps_sigma=0.0, jumps_mu=0.0,
                 cir_a=0.0, cir_mu=0.0, all_r0=0.0, cir_rho=0.0,
                 ou_a=0.0, ou_mu=0.0,
                 heston_a=0.0, heston_mu=0.0, heston_vol0=0.0, paths=15):
        # This is the starting asset value
        self.all_s0 = all_s0
        # This is the amount of time to simulate for
        self.all_time = all_time
        # This is the delta, the rate of time e.g. 1/252 = daily, 1/12 = monthly
        self.all_delta = all_delta
        # This is the volatility of the stochastic processes
        self.all_sigma = all_sigma
        # This is the annual drift factor for geometric brownian motion
        self.gbm_mu = gbm_mu
        # This is the probability of a jump happening at each point in time
        self.lamda = jumps_lamda
        # This is the volatility of the jump size
        self.jumps_sigma = jumps_sigma
        # This is the average jump size
        self.jumps_mu = jumps_mu
        # This is the rate of mean reversion for Cox Ingersoll Ross
        self.cir_a = cir_a
        # This is the long run average interest rate for Cox Ingersoll Ross
        self.cir_mu = cir_mu
        # This is the starting interest rate value
        self.all_r0 = all_r0
        # This is the correlation between the wiener processes of the Heston model
        self.cir_rho = cir_rho
        # This is the rate of mean reversion for Ornstein Uhlenbeck
        self.ou_a = ou_a
        # This is the long run average interest rate for Ornstein Uhlenbeck
        self.ou_mu = ou_mu
        # This is the rate of mean reversion for volatility in the Heston model
        self.heston_a = heston_a
        # This is the long run average volatility for the Heston model
        self.heston_mu = heston_mu
        # This is the starting volatility value for the Heston model
        self.heston_vol0 = heston_vol0
        # Number of paths in the stochastic process
        self.paths = paths

#################################################################
# Utility functions - Plots and Conversions
#################################################################    
def plot_stochastic_processes(processes, title, fs=(15,8)):
    plt.style.use(['bmh'])
    fig, ax = plt.subplots(1,figsize=fs)
    fig.suptitle(title, fontsize=16)
    ax.set_xlabel('Time, t')
    ax.set_ylabel('Simulated Asset Price')

    x_axis = numpy.arange(0, len(processes[0]), 1)
    for i in range(len(processes)):
        plt.plot(x_axis, processes[i],label=str(i))
    ax.legend()
    plt.show()

def convert_to_returns(log_returns):
    return numpy.exp(log_returns)

def convert_to_prices(param, log_returns):
    returns = convert_to_returns(log_returns)
    # A sequence of prices starting with param.all_s0
    price_sequence = [param.all_s0]
    for i in range(1, len(returns)):
        # Add the price at t-1 * return at t
        price_sequence.append(price_sequence[i - 1] * returns[i - 1])
    return numpy.array(price_sequence)

#################################################################
# Brownian and Geometric Brownian Motion
#################################################################    
def brownian_motion_log_returns(param):
    sqrt_delta_sigma = math.sqrt(param.all_delta) * param.all_sigma
    return nrand.normal(loc=0, scale=sqrt_delta_sigma, size=param.all_time)

def brownian_motion_levels(param):
    return convert_to_prices(param, brownian_motion_log_returns(param))

def geometric_brownian_motion_log_returns(param):
    assert isinstance(param, ModelParameters)
    wiener_process = numpy.array(brownian_motion_log_returns(param))
    sigma_pow_mu_delta = (param.gbm_mu - 0.5 * math.pow(param.all_sigma, 2.0)) * param.all_delta
    return wiener_process + sigma_pow_mu_delta

def geometric_brownian_motion_levels(param):
    return convert_to_prices(param, geometric_brownian_motion_log_returns(param))

#################################################################
# Jump and GBMJ Diffusion 
#################################################################    
def jump_diffusion_process(param):
    assert isinstance(param, ModelParameters)
    s_n = time = 0
    small_lamda = -(1.0 / param.lamda)
    jump_sizes = []
    for k in range(0, param.all_time):
        jump_sizes.append(0.0)
    while s_n < param.all_time:
        s_n += small_lamda * math.log(random.uniform(0, 1))
        for j in range(0, param.all_time):
            if time * param.all_delta <= s_n * param.all_delta <= (j + 1) * param.all_delta:
                # print("was true")
                jump_sizes[j] += random.normalvariate(param.jumps_mu, param.jumps_sigma)
                break
        time += 1
    return jump_sizes

def geometric_brownian_motion_jump_diffusion_log_returns(param):
    assert isinstance(param, ModelParameters)
    jump_diffusion = jump_diffusion_process(param)
    geometric_brownian_motion = geometric_brownian_motion_log_returns(param)
    return numpy.add(jump_diffusion, geometric_brownian_motion)

def geometric_brownian_motion_jump_diffusion_levels(param):
    return convert_to_prices(param, geometric_brownian_motion_jump_diffusion_log_returns(param))

#################################################################
# Heston GBM and Volatility
#################################################################    
def heston_construct_correlated_path(param, brownian_motion_one):
    # We do not multiply by sigma here, we do that in the Heston model
    sqrt_delta = math.sqrt(param.all_delta)
    # Construct a path correlated to the first path
    brownian_motion_two = []
    for i in range(param.all_time - 1):
        term_one = param.cir_rho * brownian_motion_one[i]
        term_two = math.sqrt(1 - math.pow(param.cir_rho, 2.0)) * random.normalvariate(0, sqrt_delta)
        brownian_motion_two.append(term_one + term_two)
    return numpy.array(brownian_motion_one), numpy.array(brownian_motion_two)

def get_correlated_geometric_brownian_motions(param, correlation_matrix, n):
    assert isinstance(param, ModelParameters)
    decomposition = scipy.linalg.cholesky(correlation_matrix, lower=False)
    uncorrelated_paths = []
    sqrt_delta_sigma = math.sqrt(param.all_delta) * param.all_sigma
    # Construct uncorrelated paths to convert into correlated paths
    for i in range(param.all_time):
        uncorrelated_random_numbers = []
        for j in range(n):
            uncorrelated_random_numbers.append(random.normalvariate(0, sqrt_delta_sigma))
        uncorrelated_paths.append(numpy.array(uncorrelated_random_numbers))
    uncorrelated_matrix = numpy.matrix(uncorrelated_paths)
    correlated_matrix = uncorrelated_matrix * decomposition
    assert isinstance(correlated_matrix, numpy.matrix)
    # The rest of this method just extracts paths from the matrix
    extracted_paths = []
    for i in range(1, n + 1):
        extracted_paths.append([])
    for j in range(0, len(correlated_matrix)*n - n, n):
        for i in range(n):
            extracted_paths[i].append(correlated_matrix.item(j + i))
    return extracted_paths

def cox_ingersoll_ross_heston(param):
    # We don't multiply by sigma here because we do that in heston
    sqrt_delta_sigma = math.sqrt(param.all_delta) * param.all_sigma
    brownian_motion_volatility = nrand.normal(loc=0, scale=sqrt_delta_sigma, size=param.all_time)
    a, mu, zero = param.heston_a, param.heston_mu, param.heston_vol0
    volatilities = [zero]
    for i in range(1, param.all_time):
        drift = a * (mu - volatilities[i-1]) * param.all_delta
        randomness = math.sqrt(max(volatilities[i - 1], 0.05)) * brownian_motion_volatility[i - 1]
        volatilities.append(max(volatilities[i - 1], 0.05) + drift + randomness)
    return numpy.array(brownian_motion_volatility), numpy.array(volatilities)

def heston_model_levels(param):
    assert isinstance(param, ModelParameters)
    # Get two correlated brownian motion sequences for the volatility parameter and the underlying asset
    # brownian_motion_market, brownian_motion_vol = get_correlated_paths_simple(param)
    brownian, cir_process = cox_ingersoll_ross_heston(param)
    brownian, brownian_motion_market = heston_construct_correlated_path(param, brownian)

    heston_market_price_levels = [param.all_s0]
    for i in range(1, param.all_time):
        drift = param.gbm_mu * heston_market_price_levels[i - 1] * param.all_delta
        vol = cir_process[i - 1] * heston_market_price_levels[i - 1] * brownian_motion_market[i - 1]
        heston_market_price_levels.append(heston_market_price_levels[i - 1] + drift + vol)
    return numpy.array(heston_market_price_levels), numpy.array(cir_process)

#################################################################
# CIR
#################################################################    
def cox_ingersoll_ross_levels(param):
    brownian_motion = brownian_motion_log_returns(param)
    # Setup the parameters for interest rates
    a, mu, zero = param.cir_a, param.cir_mu, param.all_r0
    # Assumes output is in levels
    levels = [zero]
    for i in range(1, param.all_time):
        drift = a * (mu - levels[i-1]) * param.all_delta
        # The main difference between this and the Ornstein Uhlenbeck model is that we multiply the 'random'
        # component by the square-root of the previous level i.e. the process has level dependent interest rates.
        randomness = math.sqrt(levels[i - 1]) * brownian_motion[i - 1]
        levels.append(levels[i - 1] + drift + randomness)
    return numpy.array(levels)

#################################################################
# Ornstein Uhlenbeck
#################################################################    
def ornstein_uhlenbeck_levels(param):
    ou_levels = [param.all_r0]
    brownian_motion_returns = brownian_motion_log_returns(param)
    for i in range(1, param.all_time):
        drift = param.ou_a * (param.ou_mu - ou_levels[i-1]) * param.all_delta
        randomness = brownian_motion_returns[i - 1]
        ou_levels.append(ou_levels[i - 1] + drift + randomness)
    return ou_levels

#################################################################
# Getters
#################################################################    
def getBrownianMotion(mp, plot=False):
    model= brownian_motion_levels
    return getProcesses(mp, model, label='Brownian Motion', plot=plot)

def getGeometricBrownianMotion(mp, plot=False):
    model= geometric_brownian_motion_levels
    return getProcesses(mp, model, label='Geometric Brownian Motion', plot=plot)

def getGBMJumpDiffusion(mp, plot=False):
    model= geometric_brownian_motion_jump_diffusion_levels
    return getProcesses(mp, model, label='Jump Diffusion Geometric Brownian Motion (Merton)', plot=plot)

def getCIR(mp, plot=False):
    model= cox_ingersoll_ross_levels
    return getProcesses(mp, model, label='Cox Ingersoll Ross', plot=plot)

def getOrnsteinUhlenbeck(mp, plot=False):
    model= ornstein_uhlenbeck_levels
    return getProcesses(mp, model, label='Ornstein Uhlenbeck', plot=plot)

def getProcesses(mp, model, label='Brownian Motion', plot=False):
    processes = []
    for i in range(mp.paths):
        processes.append(model(mp))
    if plot:
        plot_stochastic_processes(processes, label)
    return numpy.asarray(processes)

def getPPart(mp,family,upDown=0.10):
    top=[]
    mid=[]
    bottom=[]
    for i in range(1,mp.paths):
        proc=family[i][mp.all_time-1]
        if proc > mp.all_s0*(1+upDown):
            top.append(i)
        elif (proc >= mp.all_s0*(1-upDown)) and (proc <= mp.all_s0*(1+upDown)):
            mid.append(i)
        elif proc < mp.all_s0*(1-upDown):
            bottom.append(i)
    return top,mid,bottom

def getSingleProcess(mp,family,upDown=0.10, which='t'):
    import random
    
    if which=='t':
        res=[]
        for i in range(1,mp.paths):
            proc=family[i][mp.all_time-1]
            if proc > mp.all_s0*(1+upDown): res.append(i)
        p=family[random.randint(0, len(res))]
    elif which=='m':
        res=[]
        for i in range(1,mp.paths):
            proc=family[i][mp.all_time-1]
            if (proc >= mp.all_s0*(1-upDown)) and (proc <= mp.all_s0*(1+upDown)): res.append(i)
        p=family[random.randint(0, len(res))]
    elif which=='b':
        res=[]
        for i in range(1,15):
            proc=family[i][mp.all_time-1]
            if proc < mp.all_s0*(1-upDown): res.append(i)
        p=family[random.randint(0, len(res))]
    return numpy.asarray(p)

def getTSDataframe(array, freq='d', tz='UTC'):
    import datetime
    import pandas as pd
    
    if len(array.shape)>1:
        l=array.shape[1]
        c=array.shape[0]
    else:
        l=array.shape[0]
        c=1
        
    index=pd.date_range(datetime.datetime.today(), periods=l, freq=freq, tz=tz)

    ts=pd.DataFrame(array).head(len(index))
    if c>1:
        ts=ts.T
        cols=[]
        for i in range(0,ts.shape[1]):
            cols.append('Prices_'+str(i))
        ts.columns=cols
    else:
        ts.columns=['Price']
    ts['Date']=index
    ts=ts.set_index('Date')
    return ts
    
##############################################
# Create the family of O-U processes
##############################################
def ornstein_uhlenbeck(time_points=5,n_steps=100,min_span=0.,max_span=1.,ltmean=0.3,rev_rate=1.1,noise=0.5,x_0=1,n_plot=2,plot=True,npaths=1000):
    import sdepy as sde
    import numpy as np
    import matplotlib.pyplot as plt

    #Discretize time in five time points
    coarse_timeline = (0., 0.25, 0.5, 0.75, 1.0) 
    #Produce a discrete timeline of (Five time points x 100 steps)=500 evenly spaced numbers spanning 0-1
    timeline = np.linspace(min_span, max_span, time_points*n_steps) 
    timeline.shape
    #Produce a scalar process in 100*1000 paths computed at 5 time points (coarse_timeline) with 100 steps in between
    #k=speed or lambda; theta=mu
    x=sde.ornstein_uhlenbeck_process(x0=x_0, theta=ltmean, k=rev_rate,sigma=noise, paths=npaths, steps=n_steps)(coarse_timeline)
    #x.shape
    #Produce a scalar process computed on a fine-grained timeline (timeline) and 1000 paths, 
    #using one integration step for each point in the timeline (no steps parameter):
    #k=speed or lambda; theta=mu
    x=sde.ornstein_uhlenbeck_process(x0=x_0, theta=ltmean, k=rev_rate,sigma=noise, paths=npaths, steps=n_steps)(timeline)
    #x.shape
    #Plot the first n_plot paths
    if plot:
        fig = plt.figure(figsize=(8, 4))
        gr = plt.plot(timeline, x[:, :n_plot])
        plt.xticks(rotation=20)
        plt.show()
    return timeline,x

##############################################
# Returns a Heston process with Stochatic Vol
##############################################         
def heston(time_points=5,n_steps=100,min_span=0.,max_span=1.,ltmean=0.3,rev_rate=1.1,noise=0.5,x_0=1,v_0=1,rho=0.57,n_plot=2,plot=True):
    import sdepy as sde
    import numpy as np
    import matplotlib.pyplot as plt

    #Discretize time in five time points
    coarse_timeline = (0., 0.25, 0.5, 0.75, 1.0) 
    #Produce a discrete timeline of (Five time points x 100 steps)=500 evenly spaced numbers spanning 0-1
    timeline = np.linspace(min_span, max_span, time_points*n_steps) 
    timeline.shape
    #Produce a scalar process in 100*1000 paths computed at 5 time points (coarse_timeline) with 100 steps in between
    #k=speed or lambda; theta=mu
    x=sde.full_heston_process(x0=x_0, y0=v_0,theta=ltmean, k=rev_rate,sigma=noise, rho=rho,paths=n_steps*1000, steps=n_steps)(coarse_timeline)
    x[0].shape
    #Produce a scalar process computed on a fine-grained timeline (timeline) and 1000 paths, 
    #using one integration step for each point in the timeline (no steps parameter):
    x=sde.full_heston_process(x0=x_0, y0=v_0,theta=ltmean, k=rev_rate,sigma=noise, rho=rho,paths=n_steps*1000, steps=n_steps)(timeline)
    x[1].shape
    #Plot the first n_plot paths
    if plot:
        fig = plt.figure(figsize=(8, 4))
        #gr1 = plt.plot(timeline, x[0][:, :n_plot])
        #plt.xticks(rotation=20)
        #plt.title('Stock process')
        #plt.show()
        gr2 = plt.plot(timeline, x[1][:, :n_plot])
        plt.xticks(rotation=20)
        plt.title('Volatility process')
        plt.show()
    return timeline,x

####################################################
# Create the family of H processes for stock and vol
####################################################
def getHestonPrice(kappa,theta,lambd,rho,X0,V0,T,K,r,otype='c'):
    import numpy as np

    I=complex(0,1)
    P, umax, N = 0, 1000, 10000
    du=umax/N
    aa= theta*kappa*T/lambd**2
    bb= -2*theta*kappa/lambd**2
    for i in range (1,N) :
        u2=i*du
        u1=complex(u2,-1)
        a1=rho*lambd*u1*I
        a2=rho*lambd*u2*I
        d1=np.sqrt((a1-kappa)**2+lambd**2*(u1*I+u1**2))
        d2=np.sqrt((a2-kappa)**2+lambd**2*(u2*I+u2**2))
        g1=(kappa-a1-d1)/(kappa-a1+d1)
        g2=(kappa-a2-d2)/(kappa-a2+d2)
        b1=np.exp(u1*I*(np.log(X0/K)+r*T))*( (1-g1*np.exp(-d1*T))/(1-g1) )**bb
        b2=np.exp(u2*I*(np.log(X0/K)+r*T))*( (1-g2*np.exp(-d2*T))/(1-g2) )**bb
        phi1=b1*np.exp(aa*(kappa-a1-d1)\
           +V0*(kappa-a1-d1)*(1-np.exp(-d1*T))/(1-g1*np.exp(-d1*T))/lambd**2)
        phi2=b2*np.exp(aa*(kappa-a2-d2)\
           +V0*(kappa-a2-d2)*(1-np.exp(-d2*T))/(1-g2*np.exp(-d2*T))/lambd**2)
        P+= ((phi1-phi2)/(u2*I))*du
        ret=K*np.real((X0/K-np.exp(-r*T))/2+P/np.pi)
        if otype=='p':
            ret=ret-X0+K*np.exp(-r*T)
    return ret 

##############################################
# Fit a process to find its OU parameters
##############################################             
def fitOU(S,ndelta):  
    import numpy as np
    import math
    
    n = np.shape(S)[0] - 1  
    Sx  = np.sum(S[:-1])  
    Sy  = np.sum(S[1:])  
    Sxx = np.dot(S[:-1], S[:-1])  
    Sxy = np.dot(S[:-1], S[1:])  
    Syy = np.dot(S[1:], S[1:])  
    a  = ( n*Sxy - Sx*Sy ) / ( n*Sxx - math.pow(Sx,2))  
    b  = ( Sy - a*Sx ) / n  
    sd = math.sqrt((n*Syy - math.pow(Sy,2) - a*(n*Sxy - Sx*Sy) )/(n*(n-2)))  
    delta  = ndelta
    mu     = b/(1-a)  
    sigma  = sd * math.sqrt( -2* math.log(a)/(delta*(1-math.pow(a,2))))  
    lvel=-math.log(a)/delta
    print('Regression method fit (kappa_m,theta_l,lambda_n): ',mu, sigma, lvel)
    return mu, sigma, lvel
    
##############################################
# Fit volatility parameters for Heston
##############################################         
def fitH(start_param,p1_K_T,p2_K_T,p3_K_T,rho,X0,V0,r,otype='c',tol=1e-14):
    import numpy as np
    from scipy.optimize import broyden1    

    def Fitness(x):
        return    [(p1_K_T[0]-getHestonPrice(x[0],x[1],x[2],p1_K_T[2],p1_K_T[1],rho,X0,V0,r,otype)), \
                  (p2_K_T[0]-getHestonPrice(x[0],x[1],x[2],p2_K_T[2],p2_K_T[1],rho,X0,V0,r,otype)), \
                  (p3_K_T[0]-getHestonPrice(x[0],x[1],x[2],p3_K_T[2],p3_K_T[1],rho,X0,V0,r,otype))]

    fit_param = broyden1(Fitness, start_param, f_tol=tol)
    return fit_param

##############################################
# Returns a random path
##############################################         
def select_a_path(x,y,plot=True):
    import matplotlib.pyplot as plt
    import random
    
    ret_path = random.randint(1, len(x))
    print('Path selected: ', ret_path)
    if plot:
        fig = plt.figure(figsize=(8, 4))
        gr = plt.plot(x, y[:, ret_path-1:ret_path])
        plt.xticks(rotation=20)
        plt.show()
    return y[:, ret_path-1:ret_path],plt

##############################################
# Returns a specific path
##############################################         
def select_n_path(x,y, n_path,plot=True):
    import matplotlib.pyplot as plt

    if n_path>len(y):
        return 0
    else:
        ret_path = n_path
    if plot:
        fig = plt.figure(figsize=(8, 4))
        gr = plt.plot(x, y[:, ret_path-1:ret_path])
        plt.xticks(rotation=20)
        plt.show()
    return y[:, ret_path-1:ret_path],plt

##############################################
# Returns a rescaled path
##############################################         
def rescale_path(x,y, mul,scale,plot=True):
    import matplotlib.pyplot as plt
    
    y=y*mul + scale
    if plot:
        fig = plt.figure(figsize=(8, 4))
        gr = plt.plot(x, y)
        plt.xticks(rotation=20)
        plt.show()
    return y,plt

#################################################
# Plots the family with a slider to select a path
#################################################         
def plotPathsForSel(x,y,gr_adj=1.5):
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib.widgets import Slider, Button, RadioButtons

    ret_path=1
    axis_color = 'lightgoldenrodyellow'
    fig = plt.figure(figsize=(8, 4))
    fig.subplots_adjust(left=0.25, bottom=0.25)

    ax = fig.add_subplot(1, 1, 1)
    line, = ax.plot(x, y[:, ret_path-1:ret_path])
    line.axes.set_ylim(int(np.amin(y)*gr_adj),int(np.amax(y)*gr_adj))
    ax.set_title('Simulated Processes')
    plt.ylabel("Y")
    plt.grid()
    plt.xticks(rotation=20)

    amp_slider_ax  = fig.add_axes([0.25, 0.1, 0.65, 0.03], facecolor=axis_color)
    amp_slider = Slider(amp_slider_ax, 'Path', 1, len(y[:, ret_path-1:ret_path]), valinit=1,valfmt="%i")

    def update(val):
        line.set_ydata(y[:, int(amp_slider.val)-1:int(amp_slider.val)])
        #line.axes.set_ylim(int(np.amin(y[:, int(amp_slider.val)-1:int(amp_slider.val)])*1.1),int(np.amax(y[:, int(amp_slider.val)-1:int(amp_slider.val)])*1.1))
        fig.canvas.draw_idle()

    amp_slider.on_changed(update);
    return plt,fig, ax, amp_slider

##############################################
# Transform the x axis in datetime
##############################################         
def linspace2datetime(x,dstart):
    import datetime
    import matplotlib.dates
    w=x+1
    z = matplotlib.dates.num2date(w)
    a=dstart.split('-')
    today=datetime.date(int(a[0]), int(a[1]), int(a[2]))
    diff=today-z[0].date()
    i=0
    for x in z:
        z[i]=z[i]+ datetime.timedelta(days=diff.days)
        i=i+1
    return z

##############################################
# Returns a dataframe from a path
##############################################         
def getDataframe(z,y4,plot=False):
    import numpy as np
    import pandas as pd
    
    arr = np.array([z])
    df1 = pd.DataFrame(data=arr.flatten())
    df2 = pd.DataFrame(data=y4)
    df=pd.concat([df1, df2], axis=1)
    df.columns=['Date','Close']
    df['Date'] = pd.to_datetime(df['Date']).dt.date
    df['Date'] = df['Date'].astype('datetime64[ns]')
    df = df[['Date', 'Close']].groupby('Date').mean()
    if plot:
        df.plot(rot=20)
    return df

##############################################
# Plot histogram and lognormal fit
##############################################             
def plotLogNormFit(df,lbl='Close', ndelta=500,fs=(15,6)):
    import matplotlib.dates as mdates
    import matplotlib as mpl
    from matplotlib import gridspec
    import matplotlib.pyplot as plt    
    from scipy.stats import norm
    from scipy.stats import lognorm
    import numpy as np
    
    fig = plt.figure(figsize=fs)
    fig.tight_layout()
    
    top = plt.subplot2grid((6, 6), (0, 0), rowspan=6, colspan=4)
    top.plot(df.index, df[lbl], color='black',label='O-U Process')
    top.xaxis_date()
    top.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:,.0f}'))
    plt.title('Simulated Process')
    plt.setp(top.xaxis.get_majorticklabels(), rotation=20)

    stop2 = plt.subplot2grid((6,6), (0, 4), rowspan=6, colspan=3, sharey=top)            
    n, bins, patches = stop2.hist(df[lbl], bins=50,density=1,orientation=u'horizontal',alpha = 0.4,label='Histogram of Prices')
    shape, loc, scale = lognorm.fit(df[lbl], floc=0) #x0 is rawdata x-axis
    x = np.linspace(bins.min(), bins.max(), num=400) # values for x-axis
    y = lognorm.pdf(x, shape, loc=0, scale=scale) # probability distribution
    stop2.plot(y, x, 'r--',alpha = 0.3,label='Fitted LogNorm')
    stop2.yaxis.tick_right()
    print('LogNormal fit: ',scale,shape, loc)
    fitOU(df[lbl],ndelta)
    
