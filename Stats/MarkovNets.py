#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Timeseries 
#
# Module comprising helper functions to perform timeseries analysis
#############################################################################################      

from math import sqrt
from warnings import catch_warnings
from warnings import filterwarnings
from numpy import array
from ast import literal_eval
import itertools
import multiprocessing
from itertools import product
from typing import List

import numpy as np
import pandas as pd
import scipy

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

DEBUG_PRINT=0

#################################################################
# MarkovNets: Models, Plots, and Diagnostics
#################################################################    
def mkn_makeNet(data, table=True,plot=True):
    import Plib.Utils.Plotting as mkn

    state_space = data[['prev_state', 'state']]
    state_space_matrix = data.groupby(['prev_state',
                                      'state']).size().unstack()
    transition_matrix = state_space_matrix.apply(lambda x: x/float(x.sum()), axis=1) 
    
    if table:
        print('Frequency Tables: ')
        print(state_space_matrix); print()
        print(state_space_matrix.sum()); print()
        print('Transition Matrix: ')
        print(transition_matrix) ; print() 
        print(transition_matrix.sum(axis=1))
    
    if plot:
        P=transition_matrix.round(2).values
        nodes_lbls=list(transition_matrix.columns)
        mc = mkn.MarkovChain(P, nodes_lbls)
        mc.draw()

def mkn_fitModel(endog,exog,n_regimes=3,trend='n', switching_variance=False,s_reps=50,table=True):
    import statsmodels.api as sm
    
    # Fit the 3-regime model
    mod_2 = sm.tsa.MarkovRegression(endog=endog, k_regimes=n_regimes, exog=exog, trend=trend,switching_variance=switching_variance)
    res_2 = mod_2.fit(search_reps=s_reps)

    if table:
        print(res_2.summary())
        print(res_2.expected_durations)

    return res_2

def mkn_diagnostics(series,model,fs=(10,7)): 
    import Plib.Plotting.Plots as pl
       
    #Diagnostics
    fig, axes = plt.subplots(4, figsize=fs,sharex=True)
    ax = axes[0]
    ax.plot(series)
    ax.set(title='Target variable')
    ax = axes[1]
    ax.plot(model.smoothed_marginal_probabilities[0])
    ax.set(title='Smoothed probability of down regime')
    ax = axes[2]
    ax.plot(model.smoothed_marginal_probabilities[1])
    ax.set(title='Smoothed probability of no_change regime')
    ax = axes[3]
    ax.plot(model.smoothed_marginal_probabilities[2])
    ax.set(title='Smoothed probability of up regime')
    
    pl.customize_grid(axes[0])
    pl.customize_grid(axes[1])
    pl.customize_grid(axes[2])
    pl.customize_grid(axes[3])
    plt.tight_layout()
           
def mkn_prediction(model,data,n_forecast=20,table=True,plot=True):
    import Plib.Signals.Filters as fl
    predict = model.predict()
    predict = pd.DataFrame(predict.tail(n_forecast))
    predict.rename(columns ={0: 'Predicted'}, inplace=True)
    combine = pd.concat([predict, data.tail(n_forecast)], axis=1)
    combine = combine.reset_index()
    
    if plot:
        N=len(combine)
        x = np.linspace(0,N*2,N)
        y=combine.returns
        yhat=combine.Predicted
        fl.plotSmoother(x,y,yhat,linkedh=True)
    
    if table: print(combine)
    
    return combine

#################################################################
# MarkovNets: Emissions
#################################################################    
def mkn_mapStatesEmissions(states_series,code={'up':1, 'down':-1, 'no_change':0},ntrans=100):
    #Extract last n transitions
    ntrans=100
    states = list(map(str, (states_series[-ntrans:]).tolist()))
    states=[code.get(n, n) for n in states]
    return states

def gaussian_emissions(states: List[int], mus: List[float], sigmas: List[float]) -> List[float]:
    from scipy.stats import norm
    emissions = []
    for state in states:
        loc = mus[state]
        scale = sigmas[state]
        e = norm.rvs(loc=loc, scale=scale)
        emissions.append(e)
    return emissions

#rvs(mu, loc=0, size=1, random_state=None)
def poisson_emissions(states: List[int], lam: List[float]) -> List[int]:
    from scipy.stats import poisson
    emissions = []
    for state in states:
        rate = lam[state]
        e = poisson.rvs(rate)
        emissions.append(e)
    return emissions

#rvs(a, loc=0, scale=1, size=1, random_state=None)
def gamma_emissions(states: List[int], lam: List[float]) -> List[int]:
    from scipy.stats import gamma
    emissions = []
    for state in states:
        rate = lam[state]
        e = gamma.rvs(rate)
        emissions.append(e)
    return emissions

def ar_gaussian_heteroskedastic_emissions(states: List[int], k: float, sigmas: List[float]) -> List[float]:
    from scipy.stats import norm
    emissions = []
    prev_loc = 0
    for state in states:
        e = norm.rvs(loc=k * prev_loc, scale=sigmas[state])
        emissions.append(e)
        prev_loc = e
    return emissions

def ar_gaussian_homoskedastic_emissions(states: List[int], k: float, mus: List[float]) -> List[float]:
    from scipy.stats import norm
    emissions = []
    prev_loc = 0
    for state in states:
        e = norm.rvs(loc=k * prev_loc + mus[state], scale=1)
        emissions.append(e)
        prev_loc = e
    return emissions

def gaussian_homoskedastic_emissions(states: List[int], mus: List[float]) -> List[float]:
    from scipy.stats import norm
    emissions = []
    prev_loc = 0
    for state in states:
        e = norm.rvs(loc=mus[state], scale=1)
        emissions.append(e)
        prev_loc = e
    return emissions

def plot_emissions(states, emissions):
    import matplotlib.pyplot as plt
    import seaborn as sns

    fig, axes = plt.subplots(figsize=(16, 8), nrows=2, ncols=1, sharex=True)

    axes[0].plot(states)
    axes[0].set_title("States")
    axes[1].plot(emissions)
    axes[1].set_title("Emissions")
    sns.despine();

