#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Utils 
#
# Module comprising statistical help functions
#############################################################################################      

#Intel(R) Extension for Scikit-learn dynamically patches scikit-learn estimators to use oneDAL as the underlying solver
#from daal4py.sklearn import patch_sklearn
from sklearnex import patch_sklearn
patch_sklearn()

#################################################
# Get Correlation for non-normal series
#################################################      
def getCorrelation(s1,s2,ctype='kendall',stats=True):
    from scipy.stats import kendalltau, spearmanr
    if ctype=='kendall':
        coeff, pvalue = kendalltau(s1, s2) 
    else:
        coeff, pvalue = spearmanr(s1, s2)
    if stats:
        print('Kendall correlation coefficient: %.3f' % coeff) # interpret the significance
        significance = 0.05
        if pvalue > significance:
            print('Samples are uncorrelated with p=%.3f' % pvalue)
        else: 
            print('Samples are correlated with p=%.3f' % pvalue)

#################################################
# Get probability from kernel density estimation
#################################################      
def get_gaussianProb(start_value, end_value, eval_points, df):
    import numpy as np
    from sklearn.neighbors import KernelDensity
    
    kd = KernelDensity(kernel='gaussian', bandwidth=0.5).fit(np.array(df).reshape(-1, 1))
    N = eval_points                                      
    step = (end_value - start_value) / (N - 1)

    x = np.linspace(start_value, end_value, N)[:, np.newaxis]  
    kd_vals = np.exp(kd.score_samples(x)) 
    probability = np.sum(kd_vals * step) 
    return probability.round(4)
 
#################################################
# Compute Percentile capping and normalize sample
# on standardized normal distribution
#################################################      
def getSeriesZScore(df,col,limit=.15):
    import math
    
    #Select series
    df1=df[col]

    #Assume population as dataframe content
    muP=df1.mean()
    sdP=df1.std()

    if limit >0 :
        #Percentile Capping: values that are less than the value at (limit x 10)th percentile 
        #are replaced by the value at (limit x 10)th percentile, and values that are greater 
        #than the value at (100-(limit x 10))th percentile are replaced by the value at (100-(limit x 10))th percentile
        df1 = df1[df1.between(df1.quantile(limit), df1.quantile(1-limit))] # without outliers
    
        #Assume sample as winsorized dataframe
        muS=df1.mean()
        sdS=df1.std()
    
        ZScore=(muS-muP)/(sdS*math.sqrt(len(df1)))
    else:
        df['ZScore']=(df[col]-df[col].mean())/df[col].std()
        df1=df
        ZScore=0
    return ZScore,df1

##################################################
# Obtain Best Distribution Fit for a given series
# (from standardized)
##################################################          
def getBestDistFit(y):
    import numpy as np
    import pandas as pd
    import scipy
    from sklearn.preprocessing import StandardScaler
    
    # Create an index array (x) for data
    x = np.arange(len(y))
    size=len(y)
    
    #Rescale data
    sc=StandardScaler() 
    sc.fit(y)
    y_std =sc.transform(y)
    y_std = y_std.flatten()

    dist_names = ['beta',
              'cauchy',
              'expon',
              'gamma',
              'lognorm',
              'norm',
              'pearson3',
              't',
              'triang',
              'uniform',
              'weibull_min', 
              'weibull_max']

    # Set up empty lists to stroe results
    chi_square = []
    p_values = []

    # Set up 50 bins for chi-square test
    # Observed data will be approximately evenly distrubuted aross all bins
    percentile_bins = np.linspace(0,100,51)
    percentile_cutoffs = np.percentile(y_std, percentile_bins)
    observed_frequency, bins = (np.histogram(y_std, bins=percentile_cutoffs))
    cum_observed_frequency = np.cumsum(observed_frequency)

    # Loop through candidate distributions

    for distribution in dist_names:
        # Set up distribution and get fitted distribution parameters
        dist = getattr(scipy.stats, distribution)
        param = dist.fit(y_std)

        # Obtain the KS test P statistic, round it to 5 decimal places
        p = scipy.stats.kstest(y_std, distribution, args=param)[1]
        p = np.around(p, 5)
        p_values.append(p)    

        # Get expected counts in percentile bins
        # This is based on a 'cumulative distrubution function' (cdf)
        cdf_fitted = dist.cdf(percentile_cutoffs, *param[:-2], loc=param[-2], 
                              scale=param[-1])
        expected_frequency = []
        for bin in range(len(percentile_bins)-1):
            expected_cdf_area = cdf_fitted[bin+1] - cdf_fitted[bin]
            expected_frequency.append(expected_cdf_area)

        # calculate chi-squared
        expected_frequency = np.array(expected_frequency) * size
        cum_expected_frequency = np.cumsum(expected_frequency)
        ss = sum (((cum_expected_frequency - cum_observed_frequency) ** 2) / cum_observed_frequency)
        chi_square.append(ss)

    # Collate results and sort by goodness of fit (best at top)

    results = pd.DataFrame()
    results['Distribution'] = dist_names
    results['chi_square'] = chi_square
    results['p_value'] = p_values
    results.sort_values(['chi_square'], inplace=True)
    
    return results,x

##################################################
# Plot Best Distribution Fit for a given series
# 
##################################################          
def plotBestFit(y,x,results,ndistp=3):
    import numpy as np
    import pandas as pd
    import scipy
    import matplotlib.pyplot as plt
    #from pandas.plotting import register_matplotlib_converters
    #register_matplotlib_converters()

    # Divide the observed data into 100 bins for plotting (this can be changed)
    number_of_bins = 100
    bin_cutoffs = np.linspace(np.percentile(y,0), np.percentile(y,99),number_of_bins)

    # Create the plot
    h = plt.hist(y, bins = bin_cutoffs, color='0.75')

    # Get the top three distributions from the previous phase
    number_distributions_to_plot = ndistp
    dist_names = results['Distribution'].iloc[0:number_distributions_to_plot]

    # Create an empty list to stroe fitted distribution parameters
    parameters = []

    # Loop through the distributions ot get line fit and paraemters

    for dist_name in dist_names:
        # Set up distribution and store distribution paraemters
        dist = getattr(scipy.stats, dist_name)
        param = dist.fit(y)
        parameters.append(param)

        # Get line for each distribution (and scale to match observed data)
        pdf_fitted = dist.pdf(x, *param[:-2], loc=param[-2], scale=param[-1])

        scale_pdf = np.trapz (h[0], h[1][:-1]) / np.trapz (pdf_fitted, x)
        pdf_fitted *= scale_pdf

        # Add the line to the plot
        plt.plot(pdf_fitted, label=dist_name)

        # Set the plot x axis to contain 99% of the data
        # This can be removed, but sometimes outlier data makes the plot less clear
        plt.xlim(0,np.percentile(y,99))
    # Add legend and display plot

    plt.legend()
    plt.show()

    # Store distribution paraemters in a dataframe (this could also be saved)
    dist_parameters = pd.DataFrame()
    dist_parameters['Distribution'] = (
            results['Distribution'].iloc[0:number_distributions_to_plot])
    dist_parameters['Distribution parameters'] = parameters

    # Print parameter results
    print ('\nDistribution parameters:')
    print ('------------------------')

    for index, row in dist_parameters.iterrows():
        print ('\nDistribution:', row[0])
        print ('Parameters:', row[1] )

