#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Transactions 
#
# Module comprising helper functions to record transactions
#############################################################################################      


#Common Libraries
import numpy as np
import pandas as pd
import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')
import Plib.Utils.DbAccess as adb
import Plib.Utils.Tools as tls

#import warnings
#warnings.filterwarnings("once")
DEF_VERBOSE_PRINT=0

path = '/Plib/Ledger'
filesql = module_path + path + '/ledger.sql'
ltz="Europe/Rome"
bcli=0

##############################################
# Create Database and Basic setup
##############################################    
def createDB(db,force=False):
    if os.path.isfile(db) and not force:
        print('Db already exists, use force=True to erase.')
    else:
        adb.setParams(db,0)
        conn=adb.cn(db)
        cursor = conn.cursor()
        sql_file = open(filesql)
        sql_as_string = sql_file.read()
        cursor.executescript(sql_as_string)
        cursor.close()
        conn.close()
        
        setupTables()
        setupIB()

def setupTables(cli=0):
    data=[(1,cli,'BRKS',0),
          (2,cli,'ORDS',0),
          (3,cli,'EXCHS',0),
          (4,cli,'STRGS',0),
          (5,cli,'AORDS',0),
          (6,cli,'EORDS',0),
          (7,cli,'STSCS',0),
          (8,cli,'RORDS',0)]
    df=pd.DataFrame(data)
    df.columns=['id','cli','name','value']
    adb.dataframe_insert('CTRS',df)
    
def setupIB():
    id_b=createAnagBroker('Interactive Brokers Ltd',6,'')
    id_e=createAnagExchange('CBOE Option Exchange','XBAT','America/Chicago','USD',1,'SMART')
    id_o=createAnagOrder(id_b,'Market',0)
        
##############################################
# Helper functions
##############################################               
def getTimezone(exchange):
    q="select timezone from EXCHS where mic='" + str(exchange) + "'"
    r=adb.getRs(q)
    if len(r)==0:
        tz=''
    else:
        tz=str(r[0][0]) 
    return tz

def getCurrency(exchange):
    q="select currency from EXCHS where id='" + str(exchange) + "'"
    r=adb.getRs(q)
    if len(r)==0:
        cur=''
    else:
        cur=str(r[0][0])
    return cur

def getExchName2ById(id):
    q="select name2 from EXCHS where id='" + str(id) + "'"
    r=adb.getRs(q)
    if len(r)==0:
        name2=''
    else:
        name2=str(r[0][0])
    return name2
        
def getValById(id):
    global bcli
    
    q='select value from CTRS where id=' + str(id) + ' and cli=' + str(bcli)
    r=adb.getRs(q)
    if len(r)==0:
        val=0
    else:
        val=int(r[0][0]) 
    return val

def getBkrParamById(id):
    q='select param1 from BRKS where id=' + str(id)
    r=adb.getRs(q)
    if len(r)==0:
        par=''
    else:
        par=str(r[0][0]) 
    return par

def getValByName(name):
    global bcli
    
    q="select value from CTRS where name=" + str(name) + "' and cli=" + str(bcli)
    r=adb.getRs(q)
    if len(r)==0:
        val=0
    else:
        val=int(r[0][0]) 
    return val

def getNextId(table=''):
    global bcli
    
    if table=='':
        q='select count(*)+1 as id from CTRS where cli=' + str(bcli)
    else:
        if table in ('BRKS','ORDS','EXCHS'):
            q="select value+1 as id from CTRS where name='" + table + "' and cli=" + str(0)
        else:
            q="select value+1 as id from CTRS where name='" + table + "' and cli=" + str(bcli)
    r=adb.getRs(q)
    if len(r)==0:
        id=1
    else:
        id=r[0][0] 
    return int(id)

def updateNextId(id,table=''):
    table_name='CTRS' 
    row={'value': int(id),'name':str(table)}
    adb.simple_update(table_name,row)
    
def createId(name,value):
    global bcli
    
    id=int(getNextId())
    adb.simple_insert('CTRS',{
        'id': int(id),
        'cli': int(bcli),
        'name': str(name),
        'value': int(value)})
    return id

def getCodedId(sid=1,nextID=100,cli=''):
    baseid=int(str(cli)+str(sid)+str(nextID))
    return baseid
    
def printTable(table='CTRS'):
    r=adb.getRs('select * from ' + str(table))
    for row in r:
        print(row)
               
##############################################
# Manage trading transactions parameters
##############################################      
def createAnagBroker(name,decimals,param1):
    id=int(getNextId(table='BRKS'))
    adb.simple_insert('BRKS',{
        'id': int(id),
        'name': str(name),
        'decimals': int(decimals),
        'param1': str(param1)
        })
    adb.simple_update2('CTRS' ,{'value': int(id),'name':str('BRKS')})
    return id

def createAnagExchange(name,mic,timezone,currency,broker=1,name2=''):
    id=int(getNextId(table='EXCHS'))
    adb.simple_insert('EXCHS',{
        'id': int(id),
        'brk_id': int(broker),
        'name': str(name),
        'name2': str(name2),
        'mic': str(mic),
        'timezone': str(timezone),
        'currency': str(currency)
        })
    adb.simple_update2('CTRS' ,{'value': int(id),'name':str('EXCHS')})
    return id

def createAnagOrder(b_id,name,o_cat):
    id=int(getNextId(table='ORDS'))
    adb.simple_insert('ORDS',{
        'id': int(id),
        'b_id': int(b_id),
        'name': str(name),
        'o_cat': int(o_cat)
        })
    adb.simple_update2('CTRS' ,{'value': int(id),'name':str('ORDS')})
    return id
 
def startLedger(broker,exchange,fdb,params,in_mem=False):
    global bcli
    
    #sync time
    tls.syncTime()

    #setup broker
    if broker==1:
        #set db filename
        adb.setParams(fdb,params[1],in_mem)
        #Setup id for orders
        bcli=params[1]
        q="select count(*) from CTRS where cli=" + str(bcli)
        r=adb.getRs(q)
        if int(r[0][0]) == 0:
            setupTables(bcli)
    
    #Import user defined functions in sqlite        
    adb.createUDF()
        
##############################################
# Manage strategies
##############################################      
def createStr(name,name2,stype='SINGLE'):
    global bcli
    
    sid=getNextId(table='STRGS')
    adb.simple_insert('STRGS',{
        'id': int(sid),
        'cli': int(bcli),
        'name': str(name),
        'name2': str(name2),
        'datei': str(tls.getLTimestamp()[0]),
        'stype': str(stype)
        })
    updateNextId(sid,'STRGS')
    return sid

def addALegToStr(sid,oid,o_type='O'):
    global bcli
    
    adb.simple_insert('STRGS_COMP',{
        'id': int(sid),
        'cli': int(bcli),
        'ao_id': int(oid),
        'o_type': str(o_type)
        })

def getStratEO(sid):
    global bcli
    
    q='SELECT StratId, AOBrkID, Symbol, Qty, Price, TotP, SType,SecType,OSide,Exp,PC, Strike from EOST'
    q=q+' where StratId=' + str(sid) 
    q=q+' and Cli=' + str(bcli)
    r=adb.getRs(q)
    return r
           
#def getNetEOBySid(sid,sec):
#    global bcli
#    
#    q='SELECT Symbol, SUM(Qty), AVG(Price),SUM(Qty*Price),SecType,Exp,PC,Strike FROM EOST'
#    q=q+' WHERE StratId=' + str(sid) 
#    q=q+' AND Cli=' + str(bcli)
#    q=q+" AND Symbol=' + str(sec)+ "'"
#    q=q+' GROUP BY Symbol,SecType,Exp,PC,Strike'
#    r=adb.getRs(q)
#    return r
           
def sellStr():
    print()

##############################################
# Manage hedging of strategies
##############################################      
def hedgeBySym():
    print()
    
def hedgeByCurr():
    print()
    
def hedgeByStr():
    print()

      

    
    
    
    
    
    
    