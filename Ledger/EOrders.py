#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# EOrders 
#
# Module comprising helper functions to record transactions
#############################################################################################      

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')
import Plib.Ledger.Transactions as trn
import Plib.Utils.Tools as tls
import Plib.Utils.DbAccess as adb

bkr=1
exch=1
bcli=102

##############################################
# Init the module
##############################################         
def setParams(db,brk,exc,cli,in_mem=False):
    global bkr
    global exch
    global bcli
    
    adb.setParams(db,cli,in_mem)
    bkr=brk
    exch=exc
    bcli=cli

##############################################
# Manage executed trading orders
##############################################             
def createEOrder(sid, aoid, permid, euid,price, qty,s_otype,sect,other='',comm=0,slippage=0):
    global bcli
    global bkr
    global exch
    
    eoid=trn.getNextId(table='EORDS')
    adb.simple_insert('EORDS',{
        'id': int(eoid),
        'cli': int(bcli),
        'b_id': int(bkr),
        's_id': int(sid),
        'o_id': int(aoid),
        'uid': str(permid),
        'euid': str(euid),
        'price': str(price),
        'qty': str(qty),
        'comm': str(0),
        'timestamp': str(tls.getLTimestamp()[0]),
        'sec_type' : str(sect),
        'other' : str(other),
        'comm' : str(comm),
        'slippage' : str(slippage)        
        })
    trn.updateNextId(eoid,'EORDS')
    
    #print(eoid,aoid,sid)
    adb.simple_update('STRGS_COMP' ,{'eo_id': int(eoid),'ao_id':int(aoid)},' AND id=' + str(sid))
               
    return eoid

#################################################
# Retrieve EOrders for final squaring
#################################################                     
def getEOBySid(sid,sec):
    global bcli
    global bkr
    global exch
    
    q="SELECT AOrderId, AOBrkID, Symbol, Side, QtyF, PriceF, PT, SL,EOTs,sect FROM EOSBT"
    q=q + " WHERE Sid="+str(sid)+" AND Symbol='" + str(sec) + "'"
    q=q + " ORDER BY EOTs desc"
    r=adb.getRs(q)
    return r

#################################################
# Retrieve EOrders for bracketing check (trader)
#################################################                     
def getEOBySidST(sid):
    q="SELECT AOrderId, EOrderId, AOBrkID, Symbol, Side, QtyF, PriceF,sect,PriceI,QtyF,OType,EOTs FROM EOSBT"
    q=q + " WHERE Sid="+str(sid)
    q=q + " AND OType in (3,4)" #SL and TP
    q=q + " AND AOrderId not in (SELECT CAO.id FROM CAORDS AS CAO)"
    r=adb.getRs(q)
    return r

#################################################
# Retrieve EOrders for bracketing check (backtarder)
#################################################                     
def getEOBySidMP(sid,sec,mp):
    global bcli
    global bkr
    global exch
    
    q="SELECT AOrderId, AOBrkID, Symbol, Side, QtyF, PriceF, PT, SL,EOTs,sect FROM EOSBT"
    q=q + " WHERE Sid="+str(sid)+" AND Side =1 AND Symbol='" + str(sec) + "' AND"
    q=q + " (PT<="+str(mp)+" OR SL >= "+str(mp)+")"
    q=q + " UNION "
    q=q + " SELECT AOrderId, AOBrkID, Symbol, Side, QtyF, PriceF, PT, SL,EOTs,sect FROM EOSBT"
    q=q + " WHERE Sid="+str(sid)+" AND Side =-1 AND Symbol='" + str(sec) + "' AND"
    q=q + " (SL<="+str(mp)+" OR PT >= "+str(mp)+")"
    #Exclude orders already closed
    q=q + " ORDER BY EOTs desc"
    r=adb.getRs(q)
    print(r)
    return r

#################################################
# Retrieve AOrders for status update to executed
#################################################             
def getStrByBrkUID(uid,combo=False):
    q="SELECT StratId, Cli, AOrderId, AOBrkID, SType, AOTs, SECT,SType FROM AOST"
    q=q + " WHERE AOBrkID="+str(uid)
    q=q + " AND Cli="+str(bcli)
    if combo:
        q=q + " AND AOrderId not in (select ao_id from STRGS_COMP where id= StratId and eo_id <> 0)"
    q=q + " ORDER BY AOTs asc"
    r=adb.getRs(q)
    if len(r)==0:
        val1,val2,val3,val4,val5=0,0,'','',''
    else:
        val1,val2,val3,val4,val5=int(r[0][0]),str(r[0][2]),str(r[0][4]),str(r[0][6]),str(r[0][7])      
    return val1,val2,val3,val4,val5

#################################################
# Delete AOrders 
#################################################                 
def delOrder(eoid):
    adb.simple_delete('EORDS' ,{'id': str(eoid)})
    adb.simple_delete('STRGS_COMP' ,{'eo_id': str(eoid)})
            
        