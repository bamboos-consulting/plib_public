#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Aorders 
#
# Module comprising helper functions to record transactions
#############################################################################################      


import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')
import Plib.Ledger.Transactions as trn
import Plib.Utils.Tools as tls
import Plib.Utils.DbAccess as adb
import pandas as pd

bkr=1
exch=1
bcli=102

##############################################
# Init the module
##############################################         
def setParams(db,brk,exc,cli,in_mem=False):
    global bkr
    global exch
    global bcli
    
    adb.setParams(db,cli,in_mem)
    bkr=brk
    exch=exc
    bcli=cli

##############################################
# Manage rejected trading orders
#(160, 102, 331, '289', '1608314740.662907', 1,1)
#(sid,aoid,brkid, ibcli, lts,exchid,uid,code,msg)
##############################################         
def createROrder(ret,req_id,code,msg):
    sid=ret[0][0]
    aoid=ret[0][2]
    brkid=ret[0][6]
    ibcli=ret[0][1]
    lts=ret[0][4]
    exchid=ret[0][5]
    uid=req_id
    
    q='SELECT o_spec,o_side,symbol,price,qty,timestamp,sec_type FROM AORDS'
    q=q + ' WHERE id='+str(aoid)
    q=q + ' AND cli='+str(ibcli)
    q=q + ' AND uid='+str(uid)
    q=q + ' AND b_id='+str(brkid)
    q=q + ' AND e_id='+str(exchid)
    q=q + ' AND s_id='+str(sid)
    r=adb.getRs(q)
    
    if len(r)>0: 
        oid=trn.getNextId(table='RORDS')
        adb.simple_insert('RORDS',{
            'id': int(oid),
            'ao_id': int(aoid),
            'cli': int(ibcli),
            'uid': str(uid),
            'b_id': int(brkid),
            'e_id': int(exchid),
            's_id': int(sid),            
            'o_spec': int(r[0][0]),
            'o_side': str(r[0][1]),
            'symbol': str(r[0][2]),
            'price': str(r[0][3]),
            'qty': int(r[0][4]),
            'timestamp': str(r[0][5]),
            'ecode': str(code),
            'emsg': str(msg),
            'sec_type': str(r[0][6])
            })
        if int(r[0][0])==0:
            pass
        else:
            q='SELECT pr1 FROM AORDS_PRMTS'
            q=q + ' WHERE ao_id='+str(aoid)
            q=q + ' AND cli='+str(ibcli)
            q=q + ' AND b_id='+str(brkid)
            q=q + ' AND s_id='+str(sid)
            r1=adb.getRs(q)
            if len(r1)>0:
                adb.simple_insert('RORDS_PRMTS',{
                    'id': int(oid),
                    'ao_id': int(aoid),
                    'cli': int(ibcli),
                    'b_id': int(brkid),
                    's_id': int(sid),
                    'pr1': str(r1[0][0])
                    })
        trn.updateNextId(oid,'RORDS')
        delOrder(aoid)
    else:
        oid=0
    return oid

##############################################
# Manage active trading orders
##############################################         
def createAOrder(sid,symbol, price, qty, o_side, sect,expiration,otype,strike,params,scomp_type='O'):
    global bkr
    global exch
    global bcli
    
    oid=trn.getNextId(table='AORDS')
    adb.simple_insert('AORDS',{
        'id': int(oid),
        'cli': int(bcli),
        'uid': str(params[0]),
        'b_id': int(bkr),
        'e_id': int(exch),
        's_id': int(sid),
        'o_spec': int(params[1]),
        'o_side': str(o_side),
        'symbol': str(symbol),
        'price': str(price),
        'qty': str(qty),
        'timestamp': str(tls.getLTimestamp()[0]),
        'sec_type': str(sect),   #CMB STK OPT FUT
        'expmonth': str(expiration),
        'right': str(otype),
        'strike': int(strike)
        })
    if params[1]!=0:
        adb.simple_insert('AORDS_PRMTS',{
            'id': int(oid),
            'cli': int(bcli),
            'b_id': int(bkr),
            's_id': int(sid),
            'pr1': str(params[2]),
            'pr2': str(params[3]),
            'other': str(params[4]),
            'pr3': str(params[5]),
            'pr4': str(params[6])
            })
    trn.updateNextId(oid,'AORDS')
    
    trn.addALegToStr(sid,oid,scomp_type)
    
    return oid

def createCAOrder(oid, sid,symbol, price, qty, o_side, sect, br_id,ordtype):
    global bkr
    global exch
    global bcli
    
    adb.simple_insert('CAORDS',{
        'id': int(oid),
        'cli': int(bcli),
        'uid': str(br_id),
        'b_id': int(bkr),
        'e_id': int(exch),
        's_id': int(sid),
        'o_spec': int(ordtype),
        'o_side': str(o_side),
        'symbol': str(symbol),
        'price': str(price),
        'qty': str(qty),
        'timestamp': str(tls.getLTimestamp()[0]),
        'sec_type': str(sect)
        })

def getAOByBrkUID(uid):
    global bcli
    global bkr
    global exch
    
    q="SELECT StratId, Cli, AOrderId, AOBrkID, AOTs, AOE, AOBRK FROM AOSTD"
    q=q + " WHERE AOBrkID='"+str(uid)+"'"
    q=q + " AND Cli="+str(bcli)
    q=q + " ORDER BY AOTs asc"
    r=adb.getRs(q)
    return r

def getAOBySidMP(sid,sec,mp):
    global bcli
    global bkr
    global exch
    
    q="SELECT AOrderId, AOBrkID, Symbol, Side, Qty, Price, PT, SL,AOTs,sect FROM AOOSBT"
    q=q + " WHERE Sid="+str(sid)+" AND Side =1 AND Symbol='" + str(sec) + "' AND"
    q=q + " (PT<="+str(mp)+" OR SL >= "+str(mp)+")"
    q=q + " UNION "
    q=q + " SELECT AOrderId, AOBrkID, Symbol, Side, Qty, Price, PT, SL,AOTs,sect FROM AOOSBT"
    q=q + " WHERE Sid="+str(sid)+" AND Side =-1 AND Symbol='" + str(sec) + "' AND"
    q=q + " (SL<="+str(mp)+" OR PT >= "+str(mp)+")"
    q=q + " ORDER BY AOTs desc"
    r=adb.getRs(q)
    return r

def getAOBySid(sid,sec):
    global bcli
    global bkr
    global exch
    
    q="SELECT AOrderId, AOBrkID, Symbol, Side, Qty, Price, PT, SL,AOTs,sect FROM AOOSBT"
    q=q + " WHERE Sid="+str(sid)+" AND Symbol='" + str(sec) + "'"
    q=q + " ORDER BY AOTs desc"
    r=adb.getRs(q)
    return r

def getLastTradeBySid(sid,sec):
    global bcli
    global bkr
    global exch
    
    q='SELECT Symbol, PriceF, QtyF, side, sect, exp, right, strike, EOTs FROM EOSBT'
    q=q + " WHERE Sid=" + str(sid)
    q=q + " AND Symbol='" + str(sec) +"'"
    q=q + " AND Cli="+str(bcli)
    q=q + " ORDER BY EOTs DESC"
    df= adb.getDf(q)
    return df

def getTradesBySid(sid,net_comm=False,freq='H',tz='America/New_York'):
    global bcli
    global bkr
    global exch
    
    #['sec','wl','perc','usd','traded', 'QtyI', 'QtyF']
    if net_comm:                            
        calc_perc='(((PriceF*QtyF)-CommPF)/((PriceI*QtyI)-CommPI))-1'
        q='SELECT Symbol as sec, CASE WHEN ((PriceF*QtyF)-(PriceI*QtyI))-(CommPI+CommPF)>0 THEN 1 ELSE 0 END as wl, '+str(calc_perc)+' as perc,((PriceF*QtyF)-(PriceI*QtyI))-(CommPI+CommPF) as usd, PriceI,PriceF,CommPI,CommPF,SlipI,SlipF,side, TStart, TEnd,QtyI,QtyF FROM EOSBT'
        q=q + " WHERE Sid=" + str(sid)
        q=q + " AND Cli="+str(bcli)
    else:
        calc_perc='((PriceF*QtyF)/(PriceI*QtyI))-1'
        q='SELECT Symbol as sec, CASE WHEN ((PriceF*QtyF)-(PriceI*QtyI))>0 THEN 1 ELSE 0 END as wl, '+str(calc_perc)+' as perc,(PriceF*QtyF)-(PriceI*QtyI) as usd, PriceI,PriceF,CommPI,CommPF,SlipI,SlipF,side, TStart, TEnd,QtyI,QtyF FROM EOSBT'
        q=q + " WHERE Sid=" + str(sid)
        q=q + " AND Cli="+str(bcli)
    q=q + " ORDER BY TStart ASC"
    df= adb.getDf(q)
    df.columns=['sec','wl','perc','usd','pricei','pricef','commpi','commpf','slipi','slipf','side','tstart','tend','QtyI','QtyF']
    
    df['tstart']=pd.to_datetime(df['tstart'], format="%Y-%m-%d %H:%M:%S%z")
    df['tend']=pd.to_datetime(df['tend'], format="%Y-%m-%d %H:%M:%S%z")
    
    if freq=='H':
        df['duration']=(df.tend-df.tstart).astype('timedelta64[h]')
    elif freq =='D':
        df['tstart'] =pd.to_datetime(df['tstart'],utc=True).dt.tz_convert(tz)
        df['tend'] =pd.to_datetime(df['tend'],utc=True).dt.tz_convert(tz)
        
        df['duration']=(df.tend-df.tstart).astype('timedelta64[D]')
    else:
        df['duration']=0
    return df

def delOrder(aoid):    
    adb.simple_delete('AORDS' ,{'id': str(aoid)})
    adb.simple_delete('AORDS_PRMTS' ,{'id': str(aoid)})

            