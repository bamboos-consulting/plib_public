DROP TABLE IF EXISTS `BRKS`;
CREATE TABLE IF NOT EXISTS `BRKS` (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
	decimals INTEGER NOT NULL,
	param1 TEXT DEFAULT ''
);

DROP TABLE IF EXISTS `ORDS`;
CREATE TABLE IF NOT EXISTS `ORDS` (
    id INTEGER PRIMARY KEY,
	b_id INTEGER NOT NULL,
    name TEXT NOT NULL,
	o_cat INTEGER NOT NULL
);

DROP TABLE IF EXISTS `EXCHS`;
CREATE TABLE IF NOT EXISTS `EXCHS` (
    id INTEGER PRIMARY KEY,
	brk_id TEXT DEFAULT 0,
	name TEXT NOT NULL,
	name2 TEXT DEFAULT '',
	mic TEXT NOT NULL,
	timezone TEXT NOT NULL,
	currency TEXT NOT NULL
);

DROP TABLE IF EXISTS `CTRS`;
CREATE TABLE IF NOT EXISTS `CTRS` (
    id INTEGER NOT NULL,
    cli INTEGER NOT NULL,
	name TEXT NOT NULL,
	value INTEGER NOT NULL,
	PRIMARY KEY(id,cli,name)
);

DROP TABLE IF EXISTS `STRGS`;
CREATE TABLE IF NOT EXISTS `STRGS` (
    id INTEGER NOT NULL,
	cli INTEGER NOT NULL,
	name TEXT NOT NULL,
	name2 TEXT DEFAULT '',
	datei TEXT NOT NULL,
	datef TEXT DEFAULT '1900-01-01 00:00:00',
	stype TEXT DEFAULT 'SINGLE',
	PRIMARY KEY(id,cli)
);

DROP TABLE IF EXISTS `STRGS_COMP`;
CREATE TABLE IF NOT EXISTS `STRGS_COMP` (
    id INTEGER NOT NULL,
	cli INTEGER NOT NULL,
	ao_id INTEGER NOT NULL,
	eo_id INTEGER DEFAULT 0,
	o_type TEXT DEFAULT 'O'
);

DROP TABLE IF EXISTS `RORDS`;
CREATE TABLE IF NOT EXISTS `RORDS` (
    id INTEGER NOT NULL,
	ao_id INTEGER NOT NULL,
	cli INTEGER NOT NULL,
	uid TEXT DEFAULT '',
	b_id INTEGER NOT NULL,
	e_id INTEGER NOT NULL,
	s_id INTEGER NOT NULL,
	o_group INTEGER DEFAULT 0,
	o_spec INTEGER NOT NULL,
	o_side TEXT NOT NULL,
	symbol TEXT NOT NULL,
	price DECIMAL(20,6) NOT NULL,
	qty INTEGER NOT NULL,
	timestamp TEXT DEFAULT '1900-01-01 00:00:00',
	ecode TEXT DEFAULT '',
	emsg TEXT DEFAULT '',
	sec_type TEXT DEFAULT 'STK'
);

DROP TABLE IF EXISTS `RORDS_PRMTS`;
CREATE TABLE IF NOT EXISTS `RORDS_PRMTS` (
    id INTEGER NOT NULL,
	ao_id INTEGER NOT NULL,
	cli INTEGER NOT NULL,
	b_id INTEGER NOT NULL,
	s_id INTEGER NOT NULL,
	pr1 DECIMAL(20,6) DEFAULT 0,
	pr2 DECIMAL(20,6) DEFAULT 0,
	pr3 DECIMAL(20,6) DEFAULT 0,
	pr4 DECIMAL(20,6) DEFAULT 0,
	perc DEFAULT 0,
	offset DECIMAL(20,6) DEFAULT 0,
	volperc float DEFAULT 0
);

DROP TABLE IF EXISTS `AORDS`;
CREATE TABLE IF NOT EXISTS `AORDS` (
    id INTEGER NOT NULL,
	cli INTEGER NOT NULL,
	uid TEXT DEFAULT '',
	b_id INTEGER NOT NULL,
	e_id INTEGER NOT NULL,
	s_id INTEGER NOT NULL,
	o_group INTEGER DEFAULT 0,
	o_spec INTEGER NOT NULL,
	o_side TEXT NOT NULL,
	symbol TEXT NOT NULL,
	price DECIMAL(20,6) NOT NULL,
	qty DECIMAL(20,6) NOT NULL,
	timestamp TEXT DEFAULT '1900-01-01 00:00:00',
	sec_type TEXT DEFAULT 'STK',
    expmonth TEXT DEFAULT '',
    right TEXT DEFAULT '',
    strike INTEGER DEFAULT 0,
	PRIMARY KEY(id,cli)
);

DROP TABLE IF EXISTS `CAORDS`;
CREATE TABLE IF NOT EXISTS `CAORDS` (
    id INTEGER NOT NULL,
	cli INTEGER NOT NULL,
	uid TEXT DEFAULT '',
	b_id INTEGER NOT NULL,
	e_id INTEGER NOT NULL,
	s_id INTEGER NOT NULL,
	o_group INTEGER DEFAULT 0,
	o_spec INTEGER NOT NULL,
	o_side TEXT NOT NULL,
	symbol TEXT NOT NULL,
	price DECIMAL(20,6) NOT NULL,
	qty DECIMAL(20,6) NOT NULL,
	timestamp TEXT DEFAULT '1900-01-01 00:00:00',
	sec_type TEXT DEFAULT 'STK',
    expmonth TEXT DEFAULT '',
    right TEXT DEFAULT '',
    strike INTEGER DEFAULT 0,
	PRIMARY KEY(id,cli)
);

DROP TABLE IF EXISTS `AORDS_PRMTS`;
CREATE TABLE IF NOT EXISTS `AORDS_PRMTS` (
    id INTEGER NOT NULL,
	cli INTEGER NOT NULL,
	b_id INTEGER NOT NULL,
	s_id INTEGER NOT NULL,
	pr1 DECIMAL(20,6) DEFAULT 0,
	pr2 DECIMAL(20,6) DEFAULT 0,
	pr3 DECIMAL(20,6) DEFAULT 0,
	pr4 DECIMAL(20,6) DEFAULT 0,
	perc DEFAULT 0,
	offset DECIMAL(20,6) DEFAULT 0,
	volperc float DEFAULT 0,
	other TEXT DEFAULT '',
	PRIMARY KEY(id,cli)
);

DROP TABLE IF EXISTS `EORDS`;
CREATE TABLE IF NOT EXISTS `EORDS` (
    id INTEGER NOT NULL,
	cli INTEGER NOT NULL,
	b_id INTEGER NOT NULL,
	s_id INTEGER NOT NULL,
	o_id INTEGER NOT NULL,
	uid TEXT DEFAULT '',
	euid TEXT DEFAULT '',
	price DECIMAL(20,6) NOT NULL,
	qty DECIMAL(20,6) NOT NULL,
	comm DECIMAL(20,6) DEFAULT '0',
	slippage DECIMAL(20,6) DEFAULT '0',
	timestamp TEXT DEFAULT '1900-01-01 00:00:00',
	sec_type TEXT DEFAULT 'STK',
	other TEXT DEFAULT '',
	PRIMARY KEY(id,cli)
);

DROP TABLE IF EXISTS `STSCS`;
CREATE TABLE IF NOT EXISTS `STSCS` (
    id INTEGER NOT NULL,
	cli INTEGER NOT NULL,
	s_id INTEGER NOT NULL,
	PRIMARY KEY(id,cli)
);

--Executed orders for opening a live strategy 
DROP VIEW IF EXISTS EOOS;
CREATE VIEW IF NOT EXISTS EOOS as
SELECT AO.symbol as Symbol, EO.qty as Qty, EO.price as Price, (EO.qty * EO.price) as TotP
FROM STRGS as ST, STRGS_COMP as STC, AORDS as AO, EORDS AS EO
WHERE 	ST.id = STC.id AND 
		STC.ao_id = AO.id AND 
		STC.eo_id = EO.id AND
		STC.o_type = 'O' AND 
		ST.datef = '1900-01-01 00:00:00'
GROUP BY AO.symbol, EO.qty, EO.price;

--Executed orders for closing a live strategy
DROP VIEW IF EXISTS EOCS;
CREATE VIEW IF NOT EXISTS EOCS as
SELECT AO.symbol as Symbol, EO.qty as Qty, EO.price as Price, (EO.qty * EO.price) as TotP
FROM STRGS as ST, STRGS_COMP as STC, AORDS as AO, EORDS AS EO
WHERE 	ST.id = STC.id AND 
		STC.ao_id = AO.id AND 
		STC.eo_id = EO.id AND
		STC.o_type = 'C' AND 
		ST.datef = '1900-01-01 00:00:00'
GROUP BY AO.symbol, EO.qty, EO.price;

--Executed orders for a live strategy by id
DROP VIEW IF EXISTS EOST;
CREATE VIEW IF NOT EXISTS EOST as
SELECT DISTINCT ST.id as StratId, ST.cli as Cli, AO.id as AOrderId, AO.uid as AOBrkID, AO.symbol as Symbol, 
EO.qty as Qty, EO.price as Price, (EO.qty * EO.price) as TotP, ST.stype as SType, EO.sec_type as SecType, 
AO.o_side as OSide, AO.expmonth as Exp, AO.right as PC, AO.strike as Strike
FROM STRGS as ST, STRGS_COMP as STC, AORDS as AO, EORDS AS EO
WHERE 	ST.id = STC.id AND 
		STC.ao_id = AO.id AND
		STC.eo_id = EO.id AND
		STC.o_type in ('O') AND 
		ST.datef = '1900-01-01 00:00:00';
		
--Executed orders for a live strategy
DROP VIEW IF EXISTS EOS;
CREATE VIEW IF NOT EXISTS EOS as
SELECT AO.symbol as Symbol, EO.qty as Qty, EO.price as Price, (EO.qty * EO.price) as TotP
FROM STRGS as ST, STRGS_COMP as STC, AORDS as AO, EORDS AS EO
WHERE 	ST.id = STC.id AND 
		STC.ao_id = AO.id AND 
		STC.eo_id = EO.id AND
		STC.o_type in ('O','C') AND 
		ST.datef = '1900-01-01 00:00:00'
GROUP BY AO.symbol, AO.qty, AO.price;

--Active orders for a live strategy
DROP VIEW IF EXISTS AOS;
CREATE VIEW IF NOT EXISTS AOS as
SELECT AO.symbol as Symbol, AO.qty as Qty, AO.price as Price, (AO.qty * AO.price) as TotP
FROM STRGS as ST, STRGS_COMP as STC, AORDS as AO
WHERE 	ST.id = STC.id AND 
		STC.ao_id = AO.id AND 
		STC.o_type in ('O','C') AND 
		STC.eo_id not in (SELECT EO.id FROM EORDS AS EO) AND
		ST.datef = '1900-01-01 00:00:00'
GROUP BY AO.symbol, AO.qty, AO.price;

--Active orders for a live strategy by id
DROP VIEW IF EXISTS AOST;
CREATE VIEW IF NOT EXISTS AOST as
SELECT DISTINCT ST.id as StratId, ST.cli as Cli, AO.id as AOrderId, AO.uid as AOBrkID, STC.o_type as SType, 
AO.timestamp as AOTs, AO.sec_type as SECT,ST.stype as SType
FROM STRGS as ST, STRGS_COMP as STC, AORDS as AO
WHERE 	ST.id = STC.id AND 
		STC.ao_id = AO.id AND 
		STC.o_type in ('O','C') AND 
		STC.eo_id not in (SELECT EO.id FROM EORDS AS EO) AND
		ST.datef = '1900-01-01 00:00:00';
		
--Active orders for a live strategy without filled orders by id
DROP VIEW IF EXISTS AOSTD;
CREATE VIEW IF NOT EXISTS AOSTD as
SELECT DISTINCT ST.id as StratId, ST.cli as Cli, AO.id as AOrderId, AO.uid as AOBrkID, STC.o_type as SType, 
AO.timestamp as AOTs, AO.e_id as AOE, AO.b_id as AOBRK
FROM STRGS as ST, STRGS_COMP as STC, AORDS as AO
WHERE 	ST.id = STC.id AND 
		STC.ao_id = AO.id AND 
		STC.o_type in ('O','C') AND 
		STC.eo_id not in (SELECT EO.id FROM EORDS AS EO WHERE EO.qty > 0) AND
		ST.datef = '1900-01-01 00:00:00';

--Views for BACKTESTING 
-----------------------------------------------------------

--Active orders for profit stop and stop losses 
DROP VIEW IF EXISTS AOOSBT;
CREATE VIEW IF NOT EXISTS AOOSBT as
SELECT AO.symbol as Symbol, AO.qty as Qty, AO.price as Price, AOP.pr1 as PT, AOP.pr2 as SL,AO.id as AOrderId, 
AO.uid as AOBrkID, ST.id as Sid,AO.timestamp as AOTs, AO.o_side as Side,AO.sec_type as sect
FROM STRGS as ST, STRGS_COMP as STC, AORDS as AO, AORDS_PRMTS as AOP
WHERE 	ST.id = STC.id AND 
		STC.ao_id = AO.id AND 
		AO.id = AOP.id AND
		AO.cli = AOP.cli AND
		AO.b_id = AOP.b_id AND
		AO.s_id = AOP.s_id AND
		STC.o_type in ('O','C') AND 
		STC.eo_id not in (SELECT EO.id FROM EORDS AS EO) AND
		ST.datef = '1900-01-01 00:00:00';

--all trades		 last modification added OType,AOBrkID,Cli
DROP VIEW IF EXISTS EOSBT;
CREATE VIEW IF NOT EXISTS EOSBT as
SELECT distinct AO.id as AOrderId, AO.symbol as Symbol, AO.qty as QtyI, AO.price as PriceI, AOP.pr1 as PT, AOP.pr2 as SL,AO.o_side as Side,
ST.id as Sid,EO.id as EOrderId, EO.qty as QtyF, EO.price as PriceF,AO.timestamp as AOTs,EO.timestamp as EOTs,
AOP.other as TStart,EO.other as TEnd,AOP.pr3 as CommPI, EO.comm as CommPF,AOP.pr4 as SlipI, EO.slippage as SlipF,AO.o_spec as OType,
AO.uid as AOBrkID,AO.cli as Cli,AO.b_id as Brk_id,AO.sec_type as sect,AO.expmonth as exp,AO.right as right,AO.strike as strike
FROM STRGS as ST, STRGS_COMP as STC, AORDS as AO, EORDS AS EO, AORDS_PRMTS as AOP
WHERE 	ST.id = STC.id AND 
		STC.ao_id = AO.id AND 
		AO.id = AOP.id AND
		AO.cli = AOP.cli AND
		AO.b_id = AOP.b_id AND
		AO.s_id = AOP.s_id AND
		STC.eo_id = EO.id AND
		EO.cli = AO.cli AND
		EO.b_id = AO.b_id AND
		EO.s_id = AO.s_id AND
		EO.o_id = AO.id AND
		ST.datef = '1900-01-01 00:00:00';

--Active orders for opening a live strategy
DROP VIEW IF EXISTS AOOS;
CREATE VIEW IF NOT EXISTS AOOS as
SELECT AO.symbol as Symbol, AO.qty as Qty, AO.price as Price, (AO.qty * AO.price) as TotP
FROM STRGS as ST, STRGS_COMP as STC, AORDS as AO
WHERE 	ST.id = STC.id AND 
		STC.ao_id = AO.id AND 
		STC.o_type = 'O' AND 
		STC.eo_id not in (SELECT EO.id FROM EORDS AS EO) AND
		ST.datef = '1900-01-01 00:00:00'
GROUP BY AO.symbol, AO.qty, AO.price;

--Active orders for closing a live strategy
DROP VIEW IF EXISTS AOCS;
CREATE VIEW IF NOT EXISTS AOCS as
SELECT AO.symbol as Symbol, AO.qty as Qty, AO.price as Price, (AO.qty * AO.price) as TotP
FROM STRGS as ST, STRGS_COMP as STC, AORDS as AO
WHERE 	ST.id = STC.id AND 
		STC.ao_id = AO.id AND 
		STC.o_type = 'C' AND 
		STC.eo_id not in (SELECT EO.id FROM EORDS AS EO) AND
		ST.datef = '1900-01-01 00:00:00'
GROUP BY AO.symbol, AO.qty, AO.price;
