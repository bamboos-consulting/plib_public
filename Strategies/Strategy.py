#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Strategy
#
# Module comprising classes including methods and procedures to test and deploy strategies
#############################################################################################      


import pickle
import sys
import os
from IPython.display import clear_output

module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import Plib.Backtester.Backtrader as bt
import Plib.Backtester.VectScreener as scr
import Plib.Signals.TAnalysis as signalsfarm
import Plib.Utils.Tools as tls

import warnings
warnings.filterwarnings("ignore")

#############################################################################################################
# StrategyBT 
#
# Class to be used as template for Backtester prototyping
#
#############################################################################################################          
class StrategyBT:
    
    #############################################################################################
    # Methods                                                                                   #
    #############################################################################################    
    @staticmethod
    def setupData(data={},tickers=[],params=[],opt_params={},algo_params={},init_params={}):
        return 0
        
    @staticmethod
    def logic(i,x,om,mdata, hdata,trade,checkStops,closeAllTrades,moneyMgmt,algo_params):
        pass;
        
    def getHistData(self,stocks,dt_start,dt_end,tz,freq,init_data=False,fname=''):
        import Plib.DataFarm.Kibot as datafarm
        
        if not init_data:
            with open(fname, 'rb') as handle:
                hdata = pickle.load(handle) 
        else:
            hdata={}
            for s in stocks:
                hdata[s]={}
                hdata[s]['0']=datafarm.get_eod_data(s,dt_start,dt_end,tz=self.tz)
                hdata[s]['1']=datafarm.get_eod_data(self.indext,dt_start,dt_end,tz=self.tz)
                #hdata[s]['tickers']=[s,self.indext]
                with open(fname, 'wb') as handle:
                    pickle.dump(hdata, handle, protocol=pickle.HIGHEST_PROTOCOL)  
        return hdata
    
    def getParameters(self):
        return self.params,self.algo_params,self.stat_params,self.init_params
    
    @classmethod
    def testStrategy(cls, stock='MSFT', pkl=False, fname='SMACrossover.pickle'):
        #stg=SMACrossover()
        stg = cls()
        params,algo_params,stat_params,init_params=stg.getParameters()
        hdata=stg.getHistData([stock],init_params['sdate'],init_params['edate'],init_params['tz'],init_params['freq'],init_data=pkl,fname=fname)
        setupData,logic=stg.setupData,stg.logic
        
        data=hdata[stock].copy()
        tickers=algo_params['Issues']
        tickers[0]=stock
        b=setupData(data,tickers,params,{},algo_params,init_params)
    
        b.run(logic)
        r,ts=b.printSnapshot(type='all-pa',MAR=stat_params['MAR'],ConfLev=stat_params['ConfLev'],net_comm=stat_params['net_comm'],log_rets=stat_params['log_rets'],leverage=stat_params['Leverage'])
        return b, ts
    
    @classmethod
    def testStrategy2(cls, stocks=['GS','AMZN','MSFT','AAPL'], pkl=False, fname='SMACrossover.pickle'):
        #stg=SMACrossover()
        stg = cls()
        params,algo_params,stat_params,init_params=stg.getParameters()
        hdata=stg.getHistData(stocks,init_params['sdate'],init_params['edate'],init_params['tz'],init_params['freq'],init_data=pkl,fname=fname)
        setupData,logic=stg.setupData,stg.logic
        df,dfr=bt.nstocksAnalysis(stocks,hdata,setupData,logic,params,algo_params,stat_params,init_params)
        return df,dfr

    #############################################################################################
    # Parameters                                                                                   #
    #############################################################################################    
    tz='America/New_York'
    
    indext = 'SPY'      # index for benchmark 
    params=[10,60]
    init_params={'freq':'D', 'tz':'America/New_York', 'db':'','in_mem':True,'str_name':'trade',
                 'str_desc':'','sdate':'2006-12-28','edate':'2009-04-30',
                   'brk':1,'exc':1,'cli':100,'cur':'USD'}
    algo_params={'Type':'KELLY','Issue':{'KELLY':0.13,'CURR':2500,'FRAC':0.1,'STYPE':'STK'},'Issues':['',indext]} # Empty space for issue traded in logic
    stat_params={'freq':'D','MAR':-0.001,'ConfLev':0.95,'net_comm':False,
                 'Leverage':1.25,'log_rets':True,'SnapType':'silent'}
    
#############################################################################################################
# StrategyVT
#
# Class to be used as template for vecTrader prototyping
#
#############################################################################################################          
class StrategyVT:
    
    #############################################################################################
    # Methods                                                                                   #
    #############################################################################################    
    @staticmethod
    def setupData(data={},algo_params={}):
        return 0
    
    @staticmethod
    def logic(self,x,hdata,algo_params):
        pass;
        
    def getHistData(self,assets='etf',startd='2000-04-10', endd='2022-07-15',freq='D',download=False,fname='scan',tz='America/New_York'):
        hdata,stocks = scr.prepareData(assets=assets, startd=startd, endd=endd, freq=freq, 
                                       bck1=fname, download=False, first=0, show=True,tz=tz,provider='kibot')
        return hdata,stocks
        
    def getParameters(self):
        return self.data_params,self.algo_params,self.screen_params
    
    def getOptmParams(self):
        return self.range_idx,self.ranges,self.metrics
    
    @classmethod
    def testStrategy(cls, stock='ABOS', download=False, fname='scan', run_optimiz=True):
        from pandas import to_datetime
        
        stg = cls()
        hdata,stocks = stg.getHistData(assets='etf',startd='2000-04-10', endd='2022-07-15',freq='D',download=False,fname=fname,tz='America/New_York')
        data_params,algo_params,screen_params = stg.getParameters()
        data_params['signals']=stg.logic
        data_params['data']=stg.setupData
        print('Testing Strategy on a single stock')
        hdata[stock]['0']['Date']=hdata[stock]['0'].index.date
        hdata[stock]['0']['Date']=to_datetime(hdata[stock]['0']['Date'])
        hdata[stock]['0'][['Date','Open', 'High', 'Low', 'Close', 'Adjusted_close', 'Volume']]
        ema_strategy=scr.vecTrader(hdata[stock]['0'], data_params,algo_params,stock)
        ema_strategy.run()
        ema_strategy.chartSignals(['Date'],data_params['params'],[data_params[dp] for dp in data_params['params']])
        ema_strategy.addTracker(hdata[cls.indext]['0'].copy(),'index',freq='D',fix_tz=False,fix_frq=False)
        r,s=ema_strategy.printSnapshot(type='all') 
        display(ema_strategy.trades)
        ema_strategy.plot_performance()
        display(ema_strategy.yearly_df)
        if run_optimiz:
            range_idx,ranges,metrics = stg.getOptmParams()
            ret = scr.vecTrader.Performance_matrix(hdata[stock]['0'], range_idx=range_idx, ranges=ranges,
                                         metrics = metrics, 
                                         str_params=data_params,
                                         algo_params=algo_params,
                                         optimal_sol = True, tables=True)
              
    @classmethod
    def testStrategy2(cls, download=False,fname='scan', run_filtered=True, run_optimiz=True):
        stg = cls()
        hdata,stocks = stg.getHistData(assets='etf',startd='2000-04-10', endd='2022-07-15',freq='D',download=False,fname=fname,tz='America/New_York')
        data_params,algo_params,screen_params = stg.getParameters()
        data_params['signals']=stg.logic
        data_params['data']=stg.setupData
        if not run_filtered:
            print('Testing Strategy on All Stocks')
            res=scr.nstocksAnalysis(stocks,hdata,stg.setupData,stg.logic,data_params,algo_params)
            dpl=res.mask(res.eq(0).all(axis=1)).dropna(axis=1).T.sort_values('HitRatio',ascending=False).T
            display(dpl)
        if run_filtered:
            print('Testing Strategy on Filtered Stocks')
            #stocks=list(res.mask(res.eq(0).all(axis=1)).dropna(axis=1).columns)
            filt=scr.nstocksSelector(stocks,hdata,stg.setupData,stg.logic,data_params,algo_params,screen_params)
            display(filt.T.sort_values('SharpeRatio',ascending=False).T)
        if run_optimiz:
            print('Optimizing Strategy on Filtered Stocks')
            range_idx,ranges,metrics = stg.getOptmParams()
            dfr=scr.nstocksOptimizer(stocks,hdata,stg.setupData,stg.logic,range_idx=range_idx, ranges=ranges,
                             metrics=metrics,str_params=data_params,algo_params=algo_params)
            display(dfr)
            
    #############################################################################################
    # Parameters                                                                                   #
    #############################################################################################        
    tz='America/New_York'
    
    indext = 'A'      # index for benchmark 
    
    param1_range=list(range(15,27,3))
    param2_range=list(range(50,80,5))
    param3_range=list(range(130,180,5))
    range_idx=param1_range
    ranges=[param2_range, param3_range]    
    metrics=['CAGR','MDD','Sharpe']
        
    data_params={'MAt':'exponential','emaFast':20,'emaMid':65,'emaSlow':130,'params':['emaFast','emaMid','emaSlow'],'signals':logic,'data':setupData,'allocation':10000}
    algo_params={'Type':'KELLY','MSFT':{'KELLY':0.13,'CURR':2500,'FRAC':0.1,'STYPE':'STK'}}
    screen_params={'perc':0.07,'h_periods':60,'h_last':20,'vol_tr':10**7,'min_tmonths':10,'min_max_overtime':200,
                   'weights':[1,1,1,1],'lbl':'Close', 'lbl2':'Volume','treshold':2}
    