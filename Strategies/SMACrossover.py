#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# SMACrossover 
#
# Module comprising classes including methods and procedures to test and deploy SMACrossover
#############################################################################################      

import numpy as np
import pickle
import sys
import os
from IPython.display import clear_output

module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import Plib.Backtester.Backtrader as bt
import Plib.Signals.TAnalysis as signalsfarm
import Plib.Utils.Tools as tls
import Plib.Strategies.Strategy as stg
import warnings
warnings.filterwarnings("ignore")


class SMACrossoverVT(stg.StrategyVT):
    
    #############################################################################################
    # Methods                                                                                   #
    #############################################################################################    
    @staticmethod
    def setupData(data={},algo_params={}):
        #Arguments in params are optimizable
        w1=algo_params['SMA']
        w2=algo_params['LMA']
        if algo_params['MAt']:
            df3=signalsfarm.getSMA(data.copy(),'Close',w1,'SMA').fillna(0)
            df4=signalsfarm.getSMA(df3,'Close',w2,'LMA').fillna(0)
        else:
            df3=signalsfarm.getEMA(data.copy(),'Close',w1,'SMA').fillna(0)
            df4=signalsfarm.getEMA(df3,'Close',w2,'LMA').fillna(0) 
        df4['_lag1_SMA'] = df4.SMA.shift(1)
        df4['_lag1_LMA'] = df4.LMA.shift(1)
        return df4
    
    @staticmethod
    def logic(self,x,hdata,algo_params):
        ## self.buffer -> x
        ## template for creating signal:   
        buy_mask = (x.SMA > x.LMA) & (x._lag1_SMA < x._lag1_LMA)
        sell_mask = (x.SMA < x.LMA) & (x._lag1_SMA > x._lag1_LMA)

        x['Traded'] = np.nan
        x.loc[buy_mask,'Traded'] = +1
        x.loc[sell_mask,'Traded'] = -1
        x['Position']=x['Traded']
        x.Traded = x.Traded.fillna(method="ffill")
    
        return x
    
    #############################################################################################
    # Parameters                                                                                   #
    #############################################################################################    
    indext = 'A'      # index for benchmark 
    
    param1_range=list(range(15,27,3))
    param2_range=list(range(50,80,5))
    range_idx=param1_range
    ranges=[param2_range]    
    metrics=['CAGR','MDD','Sharpe']
    
    data_params={'MAt':'exponential','SMA':20,'LMA':60,'params':['SMA','LMA'],'signals':logic,'data':setupData,'allocation':10000}
    algo_params={'Type':'KELLY','MSFT':{'KELLY':0.13,'CURR':2500,'FRAC':0.1,'STYPE':'STK'}}
    screen_params={'perc':0.07,'h_periods':60,'h_last':20,'vol_tr':10**7,'min_tmonths':10,'min_max_overtime':200,
                   'weights':[1,1,1,1],'lbl':'Close', 'lbl2':'Volume','treshold':2}
    

class SMACrossoverBT(stg.StrategyBT):
    
    #############################################################################################
    # Methods                                                                                   #
    #############################################################################################    
    @staticmethod
    def setupData(data={},tickers=[],params=[],opt_params={},algo_params={},init_params={}):
        #0 no squaring, 1 squaringAlt, 2 squaringTPSL
        #0 % bracketing, 1 bracketingADR
        #0 random slippage, 1 slippageFixed
        #0 % commissions, 1 commissionsFixed  
        #0 no money mgmt, 1 kelly fraction
        rules={'Squaring':0,'Slippage':0,'Commissions':0,'Bracketing':0,'MoneyMgmt':1}
        b=bt.Buffer(init_params=init_params,rules=rules)    
        #Arguments in params are optimizable
        w1=int(params[0])
        w2=int(params[1])
        df3=signalsfarm.getSMA(data['0'].copy(),'Close',w1,'smaFast').fillna(0)
        df4=signalsfarm.getSMA(df3,'Close',w2,'smaSlow').fillna(0)
        #Slippage, Commission %, TP%, SL%
        sec_params=[0.0015 ,0.001,10,5]
        #Control for strange characters in tickers names
        b.addData(df4, tickers[0], sec_params, init_params['freq'], fix_tz=True, fix_frq=True)
        b.addTracker(data['1'].copy(), tickers[1], init_params['freq'], fix_tz=True, fix_frq=True)
        b.finalizeData(usd_account=2500,params=algo_params)
        return b
    
    @staticmethod
    def logic(i,x,om,mdata, hdata,trade,checkStops,closeAllTrades,moneyMgmt,algo_params):
        issue=algo_params['Issues'][0]
    
        #Obtain market price
        mprice=x['Close_'+issue].iloc[i-1]  
    
        #Obtain best quantity to trade
        qty=moneyMgmt(args={'price':mprice,'kelly':algo_params['Issue']['KELLY'],
              'wealth':algo_params['Issue']['CURR'],
              'fraction':algo_params['Issue']['FRAC'],'qty':1})

        #Close all trades at the end
        if i >= len(x)-1:
            #print('Final squaring')
            closeAllTrades(issue,mprice,i)
        else:
            #Trade with LS-MA Crossover 
            day=1+x['Date'].iloc[i].weekday()
            hour=x['Date'].iloc[i].hour
            #if (day not in (5,6)) and hour in (9,10,11,12,13,14,15,16):  #Since trading on next day, loop from sunday to friday
            if hdata['smaFast_'+issue].iloc[i-1]!=0 and hdata['smaSlow_'+issue].iloc[i-1] !=0:
                if (hdata['smaFast_'+issue].iloc[i] > hdata['smaSlow_'+issue].iloc[i]) and (hdata['smaFast_'+issue].iloc[i-1] < hdata['smaSlow_'+issue].iloc[i-1]):
                    if x.Account.iloc[0] > qty*mprice:
                        trade(i,issue,-1,mprice,qty)

                elif (hdata['smaFast_'+issue].iloc[i] > hdata['smaSlow_'+issue].iloc[i]) and (hdata['smaFast_'+issue].iloc[i-1] < hdata['smaSlow_'+issue].iloc[i-1]):
                    if x.Account.iloc[0] > qty*mprice:
                        trade(i,issue,1,mprice,qty)

    #############################################################################################
    # Parameters                                                                                   #
    #############################################################################################    
    indext = 'SPY'      # index for benchmark 
    params=[10,60]
    init_params={'freq':'D', 'tz':'America/New_York', 'db':'','in_mem':True,'str_name':'trade',
                 'str_desc':'','sdate':'2006-12-28','edate':'2009-04-30',
                   'brk':1,'exc':1,'cli':100,'cur':'USD'}
    algo_params={'Type':'KELLY','Issue':{'KELLY':0.13,'CURR':2500,'FRAC':0.1,'STYPE':'STK'},'Issues':['',indext]} # Empty space for issue traded in logic
    stat_params={'freq':'D','MAR':-0.001,'ConfLev':0.95,'net_comm':False,
                 'Leverage':1.25,'log_rets':True,'SnapType':'silent'}
    
    
