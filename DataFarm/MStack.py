#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# MStack 
#
# Module including functions to download data from Market Stack
#############################################################################################      

# Check path during tests
import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path+"/")

#import warnings
#warnings.filterwarnings("once")
import Plib.DataFarm.PReader as prd
import pandas as pd

ORIGIN_TZ='UTC'
    
#Key to access data
import Plib.Keystore as kst
ks=kst.Keystore()
a_key=ks.mstack_key
root=ks.mstack_root

##############################################
# Wrapper to substitute IEX data
##############################################
def get_eod_data(symbol, dt_start, dt_end):
    return get_mstackHdata(symbol, dt_start, dt_end)
    
##############################################
# Historical data download
##############################################
def get_mstackHdata(symbol, dt_start, dt_end,limit=500,tz='America/New_York'):
    from pandas.io.json import json_normalize
    
    url=root + '/tickers/' + symbol + '/eod?access_key='+a_key+'&symbols='+symbol+'&date_from='+dt_start+'&date_to='+dt_end
      
    resp = prd.requestJson2RespData(url,limit=limit,offset=0)
    df = json_normalize(resp['data']['eod'])
    if not df.empty:  
        if limit >0:
            offset=int(resp['pagination']['offset'])
            count=int(resp['pagination']['count'])
            total=int(resp['pagination']['total'])
            retrieved=count+offset
            while retrieved < total:
                resp2 = prd.requestJson2RespData(url,limit=limit,offset=retrieved)
                temp = json_normalize(resp2['data']['eod'])
                df=df.append(temp)
                retrieved=len(df)
        df=df[['date','open','high','low','close','adj_close','volume']]
        df.rename(columns={'date':'Date','open':'Open','high':'High','low':'Low','close':'Close','adj_close':'Adjusted_close','volume':'Volume'}, inplace=True)
        #Recover timezone information
        df['Date'] = pd.to_datetime(df['Date'],utc=True)
        df['Date'] = df['Date'].astype('datetime64[ns]')
        df['Date'] = df.Date.dt.tz_localize(ORIGIN_TZ).dt.tz_convert(tz)
        df['Date'] = df['Date'].astype('datetime64[ns]')
        df=df.set_index('Date')
        df = df.sort_index(ascending=True)
    
    return df

##############################################
# Exchanges download
##############################################
def getExchanges(limit=500):
    from pandas.io.json import json_normalize
    
    url=root + '/exchanges?access_key='+a_key
      
    resp = prd.requestJson2RespData(url,limit=limit,offset=0)
    df = json_normalize(resp['data'])
    
    if limit >0:
        offset=int(resp['pagination']['offset'])
        count=int(resp['pagination']['count'])
        total=int(resp['pagination']['total'])
        retrieved=count+offset
        while retrieved < total:
            resp2 = prd.requestJson2RespData(url,limit=limit,offset=retrieved)
            temp = json_normalize(resp2['data'])
            df=df.append(temp)
            retrieved=len(df)
    if not df.empty:  
        df=df[['name','mic','country','city','currency.name','currency.code','timezone.timezone']]
        #cols = ['Mic', 'Acronym', 'Name', 'Country', 'City']
    return df

def makeTimezones():    
    edf=getExchanges()
    edf.columns=['name','mic','country','city','currency2','currency','timezone']
    edf=edf[['name','mic','country','city','currency','timezone']]
    edf.dropna().to_csv(module_path+'/Plib/Utils/markets_timezones.csv')

##############################################
# Tickers download
##############################################
def getExchangeTickers(exchange='xnas',limit=500):
    from pandas.io.json import json_normalize
    
    url=root + '/exchanges/' + exchange +'/tickers?access_key='+a_key
      
    resp = prd.requestJson2RespData(url,limit=limit,offset=0)
    df = json_normalize(resp['data'])
    if not df.empty:
        df = json_normalize(resp['data']['tickers'])
        if limit >0:
            offset=int(resp['pagination']['offset'])
            count=int(resp['pagination']['count'])
            total=int(resp['pagination']['total'])
            retrieved=count+offset
            while retrieved < total:
                resp2 = prd.requestJson2RespData(url,limit=limit,offset=retrieved)
                temp = json_normalize(resp2['data']['tickers'])
                df=df.append(temp)
                retrieved=len(df)
    return df

##############################################
# Indices download
##############################################
def getIndexes(limit=500):
    from pandas.io.json import json_normalize

    url=root + '/exchanges/INDX/tickers?access_key='+a_key

    resp = prd.requestJson2RespData(url,limit=limit,offset=0)
    df = json_normalize(resp['data']['indexes'])

    if limit >0:
        offset=int(resp['pagination']['offset'])
        count=int(resp['pagination']['count'])
        total=int(resp['pagination']['total'])
        retrieved=count+offset
        while retrieved < total:
            resp2 = prd.requestJson2RespData(url,limit=limit,offset=retrieved)
            temp = json_normalize(resp2['data']['indexes'])
            df=df.append(temp)
            retrieved=len(df)
    if not df.empty:  
        df=df[['name','symbol','country','currency.name','currency.code']]
        return df


    