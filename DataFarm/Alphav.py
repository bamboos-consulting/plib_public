#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Alphav 
#
# Module including functions to download data from Alpha Vantage
#############################################################################################      

# Commmon Libraries
#import warnings
#warnings.filterwarnings("once")
import pandas as pd
import Plib.DataFarm.PReader as prd
import Plib.Utils.Tools as tools

ORIGIN_TZ='UTC'

#Key to access data
import Plib.Keystore as kst
ks=kst.Keystore()
a_key=ks.alpha_vantage_key
root=ks.alpha_vantage_root

##############################################
# Wrapper to substitute IEX data
##############################################
def get_eod_data(symbol, dt_start, dt_end):
    return get_alphaVHdata(symbol, dt_start, dt_end)

def get_alphaVHdata(symbol, dt_start, dt_end,tz='America/New_York'):
    from pandas.io.json import json_normalize
    import datetime
    
    url=root + 'TIME_SERIES_DAILY_ADJUSTED&symbol='+symbol+'&outputsize=full&apikey='+a_key
    resp = prd.requestJson2RespData(url)
    df=pd.DataFrame()
    if len(resp)>1:
        df1=pd.DataFrame(resp['Time Series (Daily)'].keys())
        df2=pd.DataFrame(resp['Time Series (Daily)'].values())
        df=pd.concat([df1, df2], axis=1)
        df.columns=['date','Open','High','Low','Close',
                    'Adjusted_close','Volume','Div_amount','Split_coef']
        df['Open']=pd.to_numeric(df['Open'], errors='coerce')
        df['High']=pd.to_numeric(df['High'], errors='coerce')
        df['Low']=pd.to_numeric(df['Low'], errors='coerce')
        df['Close']=pd.to_numeric(df['Close'], errors='coerce')
        df['Adjusted_close']=pd.to_numeric(df['Adjusted_close'], errors='coerce')
        df['Volume']=pd.to_numeric(df['Volume'], errors='coerce')
        #Recover timezone information
        df['Date'] = pd.to_datetime(df['date'],utc=True)
        df['Date'] = df['Date'].astype('datetime64[ns]')
        df['Date'] = df.Date.dt.tz_localize(ORIGIN_TZ).dt.tz_convert(tz)

        #Recover standard structure and presentation
        df=df[['Date','Open','High','Low','Close','Adjusted_close','Volume']]        
        ds=pd.to_datetime(dt_start,utc=True)
        de=pd.to_datetime(dt_end,utc=True)
        mask = (df['Date'] >= ds) & (df['Date'] <= de)
        df = df.loc[mask]
        df['Date'] = df['Date'].astype('datetime64[ns]')
        df=df.set_index('Date')
        df.columns=tools.caps(df.columns)
        df = df.sort_index(ascending=True)
    return df   

##############################################
# Returns company's metadata
##############################################    
def get_metabyTicker(symbol):
    from pandas.io.json import json_normalize
    
    url=root + 'OVERVIEW&symbol='+symbol+'&apikey='+a_key
    df = prd.requestJson2DF(url)
    
    return df

##############################################
# Return Forex data
##############################################        
def get_alphaVFXdata(cur1,cur2,dt_start, dt_end,tz='America/New_York'):
    from pandas.io.json import json_normalize
    
    url=root + 'FX_DAILY&from_symbol=' + cur1 + '&to_symbol='+ cur2 +'&outputsize=full&apikey='+a_key
    resp = prd.requestJson2RespData(url)
    df=pd.DataFrame()
    if len(resp)>1:
        df1=pd.DataFrame(resp['Time Series FX (Daily)'].keys())
        df2=pd.DataFrame(resp['Time Series FX (Daily)'].values())
        df=pd.concat([df1, df2], axis=1)
        df.columns=['date','Open','High','Low','Close']
        
        df['Open']=pd.to_numeric(df['Open'], errors='coerce')
        df['High']=pd.to_numeric(df['High'], errors='coerce')
        df['Low']=pd.to_numeric(df['Low'], errors='coerce')
        df['Close']=pd.to_numeric(df['Close'], errors='coerce')
        df['Date'] = pd.to_datetime(df['date'],utc=True)
        df['Date'] = df['Date'].astype('datetime64[ns]')
        df['Date'] = df.Date.dt.tz_localize(ORIGIN_TZ).dt.tz_convert(tz)
        df=df[['Date','Open','High','Low','Close']]        
        ds=pd.to_datetime(dt_start,utc=True)
        de=pd.to_datetime(dt_end,utc=True)
        mask = (df['Date'] >= ds) & (df['Date'] <= de)
        df = df.loc[mask]
        
        df['Date'] = df['Date'].astype('datetime64[ns]')
        
        df=df.set_index('Date')
        df.columns=tools.caps(df.columns)
        df = df.sort_index(ascending=True)
    return df

 ##############################################
 # Wrapper to substitute IEX data
 ##############################################
def get_eod_dataFX(cur1,cur2,dt_start, dt_end):
    return get_alphaVFXdata(cur1,cur2,dt_start, dt_end)
    