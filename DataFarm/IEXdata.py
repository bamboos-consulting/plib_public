#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# IEXdata 
#
# Module including functions to download data from IEX
#############################################################################################      


# Commmon Libraries
import pandas as pd
import numpy as np
import ssl
ssl._create_default_https_context = ssl._create_unverified_context
import os, sys, os.path

from dateutil import parser
from time import gmtime, strftime
from datetime import timedelta, date
from pandas import DataFrame
import datetime 
    
import py_vollib.black_scholes as pv
import py_vollib.black_scholes.implied_volatility as iv
import py_vollib.black_scholes.greeks.analytical as greeks

#from cachier import cachier
#cache_exp=datetime.timedelta(days=365)
#@cachier(stale_after=cache_exp)

# Key for accessing IEX
import Plib.Keystore as kst
ks=kst.Keystore()
key=ks.iex_key
root=ks.iex_root
keyp=ks.iex_keyp
rootp=ks.iex_rootp

import warnings
warnings.filterwarnings("once")

ORIGIN_TZ='UTC'
    
#def testIV():
#    from datetime import datetime
#    opt_price =0.03/2
#    strike = 650
#    r = .01
#    flag = 'c'
#    d1 = datetime.strptime('2020-08-12', '%Y-%m-%d')
#    d2 = datetime.strptime('2020-08-21', '%Y-%m-%d')
#    d=getIV(symbol,d1,d2,opt_price,strike, r, flag)
#    print(d)

##############################################
# Helper functions
##############################################
#@cachier(stale_after=cache_exp)
def getUndClosePrice(symbol,date1):
    date_format = '%Y%m%d'
    date=date1.strftime(date_format)
    url=root + '/stock/' + symbol + '/chart/date/' + date + '?chartByDay=true&token=' + key
    df = pd.read_json(url)
    close=0
    if not df.empty:
        close=df.close[0]
    return close

##############################################
# Compute Options Greeks
##############################################
def getGreek(greek,uprice,date_comp,exp_date,sigma,strike, r, flag, opt_price=0):
    def getUPrice(date,md):
        return md.loc[date,'price']
    
    def getDtm(d1,d2):
        delta = d1-d2
        return int(delta.days)/365
        
    undPrice = getUPrice(date_comp,uprice)
    t = getDtm(exp_date,date_comp)
    
    try:
        if greek=='iv':
            ret=iv.implied_volatility(opt_price, undPrice, strike, t, r, flag)
            ret=greeks.delta(flag, undPrice, strike, t, r, sigma)
        if greek=='delta':
            ret=greeks.delta(flag, undPrice, strike, t, r, sigma)
        if greek=='gamma':
            ret=greeks.gamma(flag, undPrice, strike, t, r, sigma)
        if greek=='rho':
            ret=greeks.rho(flag, undPrice, strike, t, r, sigma)
        if greek=='theta':
            ret=greeks.theta(flag, undPrice, strike, t, r, sigma)
        if greek=='vega':
            ret=greeks.vega(flag, undPrice, strike, t, r, sigma)
    except:
        ret=0
    greekBS=ret
    return greekBS

##############################################
# Options data download
##############################################
def getExpirations(symbol):
    url=root + '/stock/' + symbol + '/options?token=' + key
    df = pd.read_json(url)
    return df

def getChain(symbol,expiration):
    url=root + '/stock/' + symbol + '/options/' + expiration + '?token=' + key
    df = pd.read_json(url)
    return df
    
#@cachier(stale_after=cache_exp)
def getAllOptionChainIEX(symbol,r = .01,sigma=0.10):
    df=getExpirations(symbol)
    df.columns =['Exp']
    frames = [ getOptionChainIEX(symbol,str(s),r,sigma) for s in df['Exp'] ]
    myChain = pd.concat(frames)
    return myChain

def getOptionChainIEX(symbol,expiration,r = .01,sigma=0.10):
    import Plib.Utils.Tools as tls
    import datetime as datetime
    
    def getLookUpTable(symbol,values):
        from datetime import datetime
        import pandas as pd
    
        cols = ['date', 'price']
        dd = pd.DataFrame(columns=cols)
    
        df=pd.DataFrame(m.T)
        df.columns=['mdate']
        df['mdate']=pd.to_datetime(df.mdate).dt.date
        for n in df.mdate.unique():
            dd = dd.append({'date': n  ,'price': 
                    getLastQuotes(symbol).latestPrice.iloc[0]},ignore_index=True)

        dd['date'] = pd.to_datetime(dd['date'])
        dd.set_index('date',inplace=True)
        return dd
        
    def getType(stype,otype=0):
        ret=''
        if otype==0:
            if stype=='call':
                ret='c'
            if stype=='put':
                ret='p'
        if otype==1:
            if stype=='call':
                ret='CALL'
            if stype=='put':
                ret='PUT'
        return ret
    
    def getDtm(d1,d2):
        delta = d1-d2
        return int(delta.days)/365
    
    chain=getChain(symbol,expiration)
    
    #Endpoint changed 17dec2020
    chain['lastUpdated'] = chain.apply(lambda row: tls.epochToDt(row.updated), axis = 1)
    chain['expirationDate2'] = chain.apply(lambda row: datetime.datetime.strptime(str(row.expirationDate), '%Y%m%d'), axis = 1)
    
    m=chain.lastUpdated.unique()
    md=getLookUpTable(symbol,m)
    
    chain['lastUpdated']=pd.to_datetime(chain.lastUpdated).dt.date
    chain['expirationDate2']=pd.to_datetime(chain.expirationDate2).dt.date
    
    chain['IV'] = chain.apply(lambda row: 
        getGreek('iv',md,row.lastUpdated,row.expirationDate2,sigma,row.strikePrice,r,getType(row.side),row.closingPrice), axis = 1)
    chain['delta'] = chain.apply(lambda row: 
        getGreek('delta',md,row.lastUpdated,row.expirationDate2,sigma,row.strikePrice,r,getType(row.side)), axis = 1)
    chain['gamma'] = chain.apply(lambda row: 
        getGreek('gamma',md,row.lastUpdated,row.expirationDate2,sigma,row.strikePrice,r,getType(row.side)), axis = 1)
    chain['rho'] = chain.apply(lambda row: 
        getGreek('rho',md,row.lastUpdated,row.expirationDate2,sigma,row.strikePrice,r,getType(row.side)), axis = 1)
    chain['theta'] = chain.apply(lambda row: 
        getGreek('theta',md,row.lastUpdated,row.expirationDate2,sigma,row.strikePrice,r,getType(row.side)), axis = 1)
    chain['vega'] = chain.apply(lambda row: 
        getGreek('vega',md,row.lastUpdated,row.expirationDate2,sigma,row.strikePrice,r,getType(row.side)), axis = 1)
    chain['dte'] = chain.apply(lambda row: getDtm(row.expirationDate2,row.lastUpdated), axis = 1)
    chain['smvIV']=0
    chain['iRate']=r
    chain['und'] = chain.apply(lambda row: md.loc[row.lastUpdated,'price'], axis = 1)
    chain['phi']=0 
    chain['driftlessTheta']=0 
    chain['type']=chain.apply(lambda row: getType(row.side,1), axis = 1) 
    
    chain['expirationDate'] = pd.to_datetime(chain['expirationDate'],format="%Y%m%d")
    chain['expirationDate'] = chain['expirationDate'].astype('datetime64[ns]')
    
    chain = chain[['expirationDate','dte','strikePrice','volume','openInterest','bid','ask','closingPrice','IV','smvIV','iRate','und','delta','gamma','theta','vega','rho','phi','driftlessTheta','type']]
    chain.rename(columns={'strikePrice': 'strike', 'closingPrice': 'lastPrice', 'IV': 'impliedVolatility'}, inplace=True)
    return chain

##############################################
# Wrapper to filter historical data
##############################################
#@cachier(stale_after=cache_exp)
def get_eod_data(symbol, dt_start, dt_end,short=False,tz='America/New_York'):
    df=getStockHistData(symbol,short=short)
    df = df.sort_index(ascending=True)
    mask = (df.index >= dt_start) & (df.index <= dt_end)
    df = df.loc[mask]
    return df

##############################################
# Stock data download
##############################################
def getStockHistData(symbol,short=False,tz='America/New_York'):
    if short:
        url=root + '/stock/' + symbol + '/chart/max/?chartCloseOnly=True&token=' + key
    else:
        url=root + '/stock/' + symbol + '/chart/max/?token=' + key
    df = pd.read_json(url)
    if not df.empty:
        if short:
            df['open']=df['close']
            df['high']=df['close']
            df['low']=df['close']
            df['Adjusted_close']=df['close']
            df = df[['date','open','high','low','close','Adjusted_close','volume']]
            df.rename(columns={'date': 'Date', 'open': 'Open', 'high': 'High', 'low': 'Low', 'close': 'Close', 'volume': 'Volume'}, inplace=True) 
        else:
            df = df[['date','open','high','low','uClose','close','volume']]
            df.rename(columns={'date': 'Date', 'open': 'Open', 'high': 'High', 'low': 'Low', 'uClose': 'Close', 'close': 'Adjusted_close', 'volume': 'Volume'}, inplace=True) 
        #Recover timezone information
        df['Date'] = pd.to_datetime(df['Date'],utc=True)
        df['Date'] = df['Date'].astype('datetime64[ns]')
        df['Date'] = df.Date.dt.tz_localize(ORIGIN_TZ).dt.tz_convert(tz)
        df['Date'] = df['Date'].astype('datetime64[ns]')
        df.set_index('Date',inplace=True)
        df = df.sort_index(ascending=True)
    return df 

def getIntradayData(symbol,sdate='',last=0,tz='America/New_York'):
    arg=''
    if last!=0:
        arg='chartLast='+str(last)+'&'
    elif sdate!='':
        sdate=sdate.replace('-','')
        arg='exactDate='+str(sdate)+'&'
    url=root + '/stock/' + symbol + '/intraday-prices?'+str(arg)+'token=' + key
    df = pd.read_json(url)
    df['Date']=pd.to_datetime(df['date'],utc=True).dt.date
    df['Hour']=pd.to_datetime(df['minute']).dt.hour
    df['Minute']=pd.to_datetime(df['minute']).dt.minute
    df['Date'] = df['Date'].map(str) + ' ' + df['Hour'].map(str) + ':' + df['Minute'].map(str) 
    #Recover timezone information
    df['Date'] = pd.to_datetime(df['Date'],utc=True)
    df['Date'] = df['Date'].astype('datetime64[ns]')
    df['Date'] = df.Date.dt.tz_localize(ORIGIN_TZ).dt.tz_convert(tz) #df['Date'] = df.Date.dt.tz_localize(tz)
    df=df[['Date', 'high', 'low', 'average', 'volume',
           'notional', 'numberOfTrades', 'marketHigh', 'marketLow',
           'marketAverage', 'marketVolume', 'marketNotional',
           'marketNumberOfTrades', 'open', 'close', 'marketOpen', 'marketClose',
           'changeOverTime', 'marketChangeOverTime']].ffill()
    df.set_index('Date',inplace=True)
    df = df.sort_index(ascending=True)
    return df
    
#['symbol', 'companyName', 'primaryExchange', 'calculationPrice', 'open',
#      'openTime', 'openSource', 'close', 'closeTime', 'closeSource', 'high',
#       'highTime', 'highSource', 'low', 'lowTime', 'lowSource', 'latestPrice',
#       'latestSource', 'latestTime', 'latestUpdate', 'latestVolume',
#       'iexRealtimePrice', 'iexRealtimeSize', 'iexLastUpdated', 'delayedPrice',
#       'delayedPriceTime', 'oddLotDelayedPrice', 'oddLotDelayedPriceTime',
#       'extendedPrice', 'extendedChange', 'extendedChangePercent',
#       'extendedPriceTime', 'previousClose', 'previousVolume', 'change',
#       'changePercent', 'volume', 'iexMarketPercent', 'iexVolume',
#       'avgTotalVolume', 'iexBidPrice', 'iexBidSize', 'iexAskPrice',
#       'iexAskSize', 'iexOpen', 'iexOpenTime', 'iexClose', 'iexCloseTime',
#       'marketCap', 'peRatio', 'week52High', 'week52Low', 'ytdChange',
#       'lastTradeTime', 'isUSMarketOpen'],    
#@cachier(stale_after=cache_exp)
def getLastQuotes(symbol,tz='America/New_York'):
    import Plib.DataFarm.PReader as prd
    from pandas.io.json import json_normalize
    
    url=root + '/stock/' + symbol + '/quote?token=' + key
    resp = prd.requestJson2RespData(url)  
    df = json_normalize(resp)
    return df
    
##############################################
# Stock data download for one day only
##############################################
def getStockDataOneDay(symbol,date):
    date=date.replace('-','')
    url=root + '/stock/' + symbol + '/chart/date/' + date + '?chartByDay=true&token=' + key
    df = pd.read_json(url)
    return df
    
##############################################
# News data download
##############################################
def getNewsByStock(symbol,start_date,end_date,maxrec=10):
    #import requests
    #from pandas.io.json import json_normalize
    #from datetime import datetime
    #import pandas as pd
    
    url=root + '/time-series/news/' + symbol + '?from=' + start_date + '&to=' + end_date + '&limit=' + str(maxrec) + '&' + 'token=' + key
    df = pd.read_json(url)
    
    df=df[['datetime','headline','related','summary']]
    df['relevant']=1
    df.rename(columns={'datetime':'Date','headline':'Subject','summary':'body','related':'Source'}, inplace=True)
    
    return df

##############################################
# Fundamental data download
##############################################
def getFundData(symbol,period='annual'):
    url=root + '/time-series/fundamentals/' + symbol + '/' + period + '?token=' + key
    df = pd.read_json(url)
    return df
    
def getFundDataBS(symbol,period='quarter',many=12):
    url=root + '/stock/' + symbol + '/balance-sheet?period=' + period + '&last='+str(many)+'&token=' + key
    temp = pd.read_json(url)
    df=temp['balancesheet'].iloc[0]
    df=pd.DataFrame.from_dict(df, orient='index').T
    df['Quarter']=0
    df=df.set_index('Quarter')
    for i in range(1,len(temp)):
        s=temp['balancesheet'].iloc[i]
        s=pd.DataFrame.from_dict(s, orient='index').T
        s['Quarter']=i
        s=s.set_index('Quarter')
        df=df.append(s)
    df=df.sort_values('reportDate',ascending=False)
    return df

def getFundDataIS(symbol,period='quarter',many=12):
    url=root + '/stock/' + symbol + '/income?period=' + period + '&last='+str(many)+'&token=' + key
    temp = pd.read_json(url)
    df=temp['income'].iloc[0]
    df=pd.DataFrame.from_dict(df, orient='index').T
    df['Quarter']=0
    df=df.set_index('Quarter')
    for i in range(1,len(temp)):
        s=temp['income'].iloc[i]
        s=pd.DataFrame.from_dict(s, orient='index').T
        s['Quarter']=i
        s=s.set_index('Quarter')
        df=df.append(s)
    df=df.sort_values('reportDate',ascending=False)
    return df

def getFundDataCF(symbol,period='quarter',many=12):
    url=root + '/stock/' + symbol + '/cash-flow?period=' + period + '&last='+str(many)+'&token=' + key
    temp = pd.read_json(url)
    df=temp['cashflow'].iloc[0]
    df=pd.DataFrame.from_dict(df, orient='index').T
    df['Quarter']=0
    df=df.set_index('Quarter')
    for i in range(1,len(temp)):
        s=temp['cashflow'].iloc[i]
        s=pd.DataFrame.from_dict(s, orient='index').T
        s['Quarter']=i
        s=s.set_index('Quarter')
        df=df.append(s)
    df=df.sort_values('reportDate',ascending=False)
    return df

def getSECfiles(symbol,period='10-K',many=12):
    url=root + '/time-series/REPORTED_FINANCIALS/' + symbol + '/' + period + '?last='+str(many)+'&token=' + key
    df = pd.read_json(url)
    df=df.sort_values('date',ascending=False)
    return df

##############################################
# Get company information
##############################################
def getMetaByTicker(symbol):

    url=root + '/stock/'+symbol+'/company?token=' + key
    df = pd.read_json(url)
    return df
    
##############################################
# Get company statistics
##############################################
def getStatsByTicker(symbol):
    import Plib.DataFarm.PReader as prd
    from pandas.io.json import json_normalize
    
    url=root + '/stock/'+symbol+'/stats?token=' + key
    
    resp = prd.requestJson2RespData(url)  
    df = json_normalize(resp)
    return df
    
##############################################
# Get market information
##############################################
def getMarketStats():
    import Plib.DataFarm.PReader as prd
    from pandas.io.json import json_normalize
    
    url=root + '/stock/market/volume?token=' + key
    
    resp = prd.requestJson2RespData(url)  
    df = json_normalize(resp)
    return df

##############################################
# Future data download
##############################################
def getFutureData(symbol='ES22M:F',tz='America/New_York'):
    url=root + '/futures/' + symbol + '/chart?range=10y&token=' + key
    #print(url)
    df = pd.read_json(url)
    if not df.empty:
        #df['date']= pd.to_datetime(df['lastTradeDate'] + ' ' + df['lastTradeTime'], format='%m-%d-%Y %H:%M:%S')
        df['date']= pd.to_datetime(df['lastTradeDate'] + df['lastTradeTime'], format='%Y-%m-%d%H:%M:%S')
        df = df[['date','open','high','low','close','settlementPrice','openInterest','volume','expirationDate']]
        df.rename(columns={'date': 'Date', 'open': 'Open', 'high': 'High', 'low': 'Low', 'close': 'Close', 'settlementPrice': 'Adjusted_close', 'volume': 'Volume', 'openInterest': 'openInterest', 'expirationDate': 'expirationDate'}, inplace=True) 
        #Recover timezone information
        df['Date'] = pd.to_datetime(df['Date'],utc=True)
        df['Date'] = df['Date'].astype('datetime64[ns]')
        df['Date'] = df.Date.dt.tz_localize(ORIGIN_TZ).dt.tz_convert(tz)
        df['Date'] = df['Date'].astype('datetime64[ns]')
        df.set_index('Date',inplace=True)
        df = df.sort_index(ascending=True)
        #Remove time
        df=df.reset_index()
        df['Date'] = pd.to_datetime(df['Date']).dt.normalize()
        df.set_index('Date',inplace=True)
        
    return df 

def getFutTickers(exchange=''):
    syms=getFutTickers2(exchange)
    for s in syms.underlying:
        f=getFuturesContracts(s)
        f=f.head(1)
        try:
            print(s,f.name.values)
        except:
            x=0

def getFutTickers2(exchange=''):
    url=root + '/ref-data/futures/symbols?token=' + key
    df = pd.read_json(url)
    if exchange != '':
        df=df[df.exchange.eq(exchange)]
    return df

def getFuturesContracts(symbol=''):
    url=root + '/ref-data/futures/symbols/'+symbol+'?token=' + key
    df = pd.read_json(url)
    return df

def getFutExchanges():
    df=getFutTickers2()
    df=df[['exchangeName','exchange']]
    return df.drop_duplicates()  
    
# For compatibility    
def getContFuture(exchange,symbol,month, start_date, end_date,tz='America/New_York'):
    return getSimpleFuture(exchange,symbol,start_date, end_date,tz)
def getSimpleFuture(exchange,symbol,start_date, end_date,tz='America/New_York'):
    f=getFuturesContracts(symbol)
    symbol=f.head(1).symbol.values[0]
    df=getFutureData(symbol,tz)
    return df[['Open','High','Low','Close','Adjusted_close','openInterest','Volume']]

##############################################
# Treasuries data download
##############################################
def getTreasuries(symbol='DGS20',years='1'):
    import Plib.Utils.Tools as tls
    
    url=root + '/time-series/treasury/'+symbol+'?range=1y&token=' + key
    df = pd.read_json(url)
    ###df['date'] = df.apply(lambda row: tls.epochToDt(row.date), axis = 1)
    #df['updated'] = df.apply(lambda row: tls.epochToDt(row.updated), axis = 1)
    
    df = df[['date','key','value','updated']]
    df.rename(columns={'key': 'Symbol', 'value': 'Rate', 'date': 'Date', 'updated': 'Updated'}, inplace=True) 
    
    return df
    
def getYieldCurve(years='1'):
    symbol='DGS1MO'
    rates=getTreasuries(symbol=symbol,years=years)
    rates=rates[['Date','Rate']]
    rates.rename(columns={'Rate': symbol}, inplace=True) 

    for s in {'DGS3MO','DGS6MO','DGS1','DGS2','DGS3','DGS5','DGS7','DGS10','DGS20','DGS30'}:
        r1=getTreasuries(symbol=s,years=years)
        r1=r1[['Date','Rate']]
        r1.rename(columns={'Rate': s}, inplace=True) 
        rates=pd.merge(rates,r1,how='outer',on='Date')
    rates=rates[['Date','DGS1MO','DGS3MO','DGS6MO','DGS1','DGS2','DGS3','DGS5','DGS7','DGS10','DGS20','DGS30']]
    rates.rename(columns={'DGS1MO':'1 Mo','DGS3MO':'3 Mo','DGS6MO':'6 Mo','DGS1':'1 Yr','DGS2':'2 Yr','DGS3':'3 Yr','DGS5':'5 Yr','DGS7':'7 Yr','DGS10':'10 Yr','DGS20':'20 Yr','DGS30':'30 Yr'}, inplace=True) 
    rates=rates.set_index('Date')
    return rates
 
##############################################
# Get IPOs, Earnings, Dividends
##############################################
def getIPOs():

    url=root + '/stock/market/upcoming-ipos?token=' + key
    df = pd.read_json(url)
    return df
    
def getEarnings():
    url=root + '/stock/market/today-earnings?token=' + key
    df = pd.read_json(url)
    return df

def getDividends():
    url=root + '/time-series/advanced_dividends?range=last-week&token=' + key
    df = pd.read_json(url)
    return df

    
##############################################
# Get ticker from string
##############################################
def lookupTicker(search='apple'):
    import Plib.DataFarm.PReader as prd
    from pandas.io.json import json_normalize
    #https://sandbox.iexapis.com/stable/search/apple?token=Tsk_702b7e60f39647c0b0b8aa51df68fbee
    url=root + '/search/'+ search +'?token=' + key
    
    resp = prd.requestJson2RespData(url)  
    df = json_normalize(resp)
    return df
    
##############################################
# Get mutual funds tickers
##############################################
def mfTicker():
    import Plib.DataFarm.PReader as prd
    from pandas.io.json import json_normalize
    #https://sandbox.iexapis.com/stable/ref-data/mutual-funds/symbols?token=Tsk_702b7e60f39647c0b0b8aa51df68fbee
    url=root + '/ref-data/mutual-funds/symbols?token=' + key
    
    resp = prd.requestJson2RespData(url)  
    df = json_normalize(resp)
    return df

##############################################
# Helper function for batch download
##############################################
def getSymbols(stocks):
    def chunks(lst, n):
        #yield successive n-sized chunks from lst
        for i in range(0, len(lst), n):
            yield lst[i:i + n]

    symbol_groups = list(chunks(stocks['Ticker'], 100))
    symbol_strings = []
    for i in range(0, len(symbol_groups)):
        symbol_strings.append(','.join(symbol_groups[i]))
    
    return symbol_strings

##############################################
# Batch Download Proces from Tickers List
##############################################
def getHistCompBatch(tickers, labl='Close', test=False):
    import requests

    if test:
        key = 'Tpk_059b97af715d417d9f49f50b51b1c448'
        root='https://sandbox.iexapis.com/stable'
        
    symbol_strings = list(tickers.Ticker)

    i=0
    for symbol_string in symbol_strings:
        batch_api_call_url = f'{root}/stock/market/batch?symbols={symbol_string}&types=chart&range=2y&token={key}'
        data = requests.get(batch_api_call_url).json()

        for symbol in symbol_string.split(','):
            df=pd.DataFrame(data[symbol]['chart'])
            if len(df) > 0:
                df=df[['date','open','high','low','close','volume','change','changePercent']]
                df.columns=['Date','Open','High','Low','Close','Volume','Change','changePercent']
                df=df.set_index('Date')
                df=df.sort_values('Date')
                if i==0:
                    temp=df[[labl]].copy()
                    temp.columns=[symbol]
                else:
                    df=df[[labl]]
                    df.columns=[symbol]
                    frames = [temp, df]
                    temp = pd.concat(frames, axis=1)
            i=i+1

    return temp.interpolate()

##############################################
# Batch Download Returns from Tickers List
##############################################
def prepareMomentumData(stocks, test=False):    
    import requests
    #from scipy import stats
    
    if test:
        key = 'Tpk_059b97af715d417d9f49f50b51b1c448'
        root='https://sandbox.iexapis.com/stable'
    
    symbol_strings = getSymbols(stocks)

    hqm_columns = [
                    'Ticker', 
                    'Price', 
                    'Market Cap',
                    'One-Year Price Return', 
                    'One-Year Return Percentile',
                    'Six-Month Price Return',
                    'Six-Month Return Percentile',
                    'Three-Month Price Return',
                    'Three-Month Return Percentile',
                    'One-Month Price Return',
                    'One-Month Return Percentile',
                    'HQM Score'
                    ]

    hqm_dataframe = pd.DataFrame(columns = hqm_columns)
    
    for symbol_string in symbol_strings:
        batch_api_call_url = f'{root}/stock/market/batch/?types=stats,quote&symbols={symbol_string}&token={key}'
        data = requests.get(batch_api_call_url).json()
        for symbol in symbol_string.split(','):
            try:
                hqm_dataframe = hqm_dataframe.append(
                                                pd.Series([symbol, 
                                                           data[symbol]['quote']['latestPrice'],
                                                           data[symbol]['quote']['marketCap'],
                                                           data[symbol]['stats']['year1ChangePercent'],
                                                           'N/A',
                                                           data[symbol]['stats']['month6ChangePercent'],
                                                           'N/A',
                                                           data[symbol]['stats']['month3ChangePercent'],
                                                           'N/A',
                                                           data[symbol]['stats']['month1ChangePercent'],
                                                           'N/A',
                                                           'N/A'
                                                           ], 
                                                          index = hqm_columns), 
                                                ignore_index = True)
            except:
                pass;
    
    
    return hqm_dataframe

################################################
# Batch Download Fundamentals from Tickers List
################################################
def prepareValueData(stocks, test=False):
    import requests
    #from scipy import stats
    
    if test:
        key = 'Tpk_059b97af715d417d9f49f50b51b1c448'
        root='https://sandbox.iexapis.com/stable'
    

    symbol_strings = getSymbols(stocks)
    
    rv_columns = [
        'Ticker',
        'Price',
        'Number of Shares to Buy', 
        'Price-to-Earnings Ratio',
        'PE Percentile',
        'Price-to-Book Ratio',
        'PB Percentile',
        'Price-to-Sales Ratio',
        'PS Percentile',
        'EV/EBITDA',
        'EV/EBITDA Percentile',
        'EV/GP',
        'EV/GP Percentile',
        'RV Score'
    ]

    rv_dataframe = pd.DataFrame(columns = rv_columns)

    for symbol_string in symbol_strings:
        batch_api_call_url = f'{root}/stock/market/batch?symbols={symbol_string}&types=quote,advanced-stats&token={key}'
        data = requests.get(batch_api_call_url).json()
        for symbol in symbol_string.split(','):
            try:
                enterprise_value = data[symbol]['advanced-stats']['enterpriseValue']
                ebitda = data[symbol]['advanced-stats']['EBITDA']
                gross_profit = data[symbol]['advanced-stats']['grossProfit']
            except:
                pass;

            try:
                ev_to_ebitda = enterprise_value/ebitda
            except TypeError:
                ev_to_ebitda = np.NaN

            try:
                ev_to_gross_profit = enterprise_value/gross_profit
            except TypeError:
                ev_to_gross_profit = np.NaN

            try:
                rv_dataframe = rv_dataframe.append(
                    pd.Series([
                        symbol,
                        data[symbol]['quote']['latestPrice'],
                        'N/A',
                        data[symbol]['quote']['peRatio'],
                        'N/A',
                        data[symbol]['advanced-stats']['priceToBook'],
                        'N/A',
                        data[symbol]['advanced-stats']['priceToSales'],
                        'N/A',
                        ev_to_ebitda,
                        'N/A',
                        ev_to_gross_profit,
                        'N/A',
                        'N/A'
                ],
                index = rv_columns),
                    ignore_index = True
                )
            except:
                pass;

    

    return rv_dataframe
    
