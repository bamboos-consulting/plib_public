#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Quandl free Futures data 
#
# Module including functions to download data from Quandl
#############################################################################################      

#Common Libraries
import quandl
import pandas as pd
from pandas import DataFrame

ORIGIN_TZ='UTC'

import Plib.Keystore as kst
ks=kst.Keystore()

# Key to download data from Barchart
quandl.ApiConfig.api_key = ks.quandl_key
#symbols
#csv files stored in future_tickers directory
csv_path=ks.quandl_csv_path

#exchanges
exchanges=[
['Eurex','EUREX'],
['CME Futures','CME'],
['ICE Futures','ICE'],
['Minneapolis Grain Exchange','MGEX'],
['Montreal Exchange','MX'],
['Multi Commodities Exchange of India','MCX'],
['Osaka Dojima Commodity Exchange','ODE'],
['Osaka Securities Exchange','OSE'],
['Singapore Exchange','SGX'],
['Shanghai Futures Exchange','SHFE'],
['Tokyo Futures Exchange','TFX']]

def getFutExchanges():
    return exchanges
    
def getFutTickers(exchange):
    import pandas as pd 
    
    data=[]
    try:
        csv_file = csv_path + '/Plib/DataFarm/other/' + exchange + '.csv'
        data = pd.read_csv(csv_file) 
    except:
        print('Wrong filename...' + csv_file)
    return data

##############################################
# Continuous Future Download
##############################################    
def getContFuture(exchange,symbol,month, start_date, end_date,tz='America/New_York'):
    return getQDLData('CHRIS/'+exchange+'_'+symbol+str(month), start_date, end_date,'future')

##############################################
# Specific Future Download
##############################################    
def getSimpleFuture(exchange,symbol,start_date, end_date,tz='America/New_York'):
    return getQDLData(exchange + '/'+ symbol, start_date, end_date,'future')

##############################################
# Futures and Stock data download
# 
#Format for continuous contracts from source CHRIS is CHRIS/{EXCHANGE}_{CODE}{NUMBER}, 
#where {NUMBER} is the “depth” associated with the chained contract. 
#For instance, the front month contract has depth 1, the second month contract has depth 2, and so on.
##############################################
def getQDLData(symbol='CHRIS/CME_SP1', start_date='2000-04-24', end_date='2020-04-24',rtype='future',tz='America/New_York'):
    
    def filterBCFutures(df,symbol,tz='America/New_York'):
        if symbol == 'HKEX/00358':
            #Nominal Price	Net Change	Change (%)	Bid	Ask	P/E(x)	High	Low	Previous Close	Share Volume (000)	Turnover (000)	Lot Size
            df.rename(columns={'Nominal Price': 'Close','Previous Close': 'Open','Share Volume (000)': 'openInterest','Turnover (000)': 'Volume' }, inplace=True)
            df['Adjusted_close']=df['Close']
            df=df[['Open','High','Low','Close','Adjusted_close','openInterest','Volume']]
        if symbol == 'RICI/RICI':
            df.rename(columns={'Value': 'Close'}, inplace=True)
            df['Adjusted_close']=df['Close']
            df['Open']=df['Close']
            df['High']=df['Close']
            df['Low']=df['Close']
            df['Adjusted_close']=df['Close']
            df['openInterest']=df['Close']
            df['Volume']=df['Close']
            df=df[['Open','High','Low','Close','Adjusted_close','openInterest','Volume']]
        else:
            #Open	High	Low	Last	Change	Close	Volume	Previous Day Open Interest
            df.rename(columns={'Settle': 'Close'}, inplace=True)
            df['Adjusted_close']=df['Close']
            df.rename(columns={'Previous Day Open Interest': 'openInterest'}, inplace=True)
            #del df['Change']
            df=df[['Open','High','Low','Close','Adjusted_close','openInterest','Volume']]
            
        #Recover timezone information
        df=df.reset_index()
        df['Date'] = pd.to_datetime(df['Date'],utc=True)
        df['Date'] = df['Date'].astype('datetime64[ns]')
        df['Date'] = df.Date.dt.tz_localize(ORIGIN_TZ).dt.tz_convert(tz)
        df['Date'] = df['Date'].astype('datetime64[ns]')
        df=df.set_index('Date')
        df = df.sort_index(ascending=True)
        return df
    

    df=quandl.get(symbol, start_date=start_date, end_date=end_date)
    
    if rtype=='future':
        df=filterBCFutures(df,symbol,tz='America/New_York')
    return df

