#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Fred 
#
# Module including functions to download data from Fred
#############################################################################################      

from fredapi import Fred

import Plib.Keystore as kst
ks=kst.Keystore()
fred_api=ks.fred_api
fred = Fred(fred_api)

##############################################
# Get fred object
##############################################
def getFred():
    return fred
    
##############################################
# Get single columns series
##############################################
def get_eod_data_with_fred(symbol, dt_start, dt_end):
    import pandas as pd
    
    s = fred.get_series(symbol, observation_start=dt_start, observation_end=dt_end)
    df = {}
    df['Open']=s
    df['High']=s
    df['Low']=s
    df['Close']=s
    df['Adjusted_close'] =s
    df['Volume']=0
    df = pd.DataFrame(df)
    return df
    
def get_eod_data_with_fred2(symbol, dt_start, dt_end):
    import pandas as pd

    s = fred.get_series(symbol, observation_start=dt_start, observation_end=dt_end)
    df1 = {}
    df1['Open'] =s
    df1['High'] =s
    df1['Low'] =s
    df1['Close'] =s
    df1['Adjusted_close'] =s
    df1['Volume']=0
    series2 = pd.DataFrame(df1)
    series2['Date']=series2.index
    series2=series2.set_index('Date')
    return series2
    
def getYieldCurve(year=2022):
    import pandas as pd

    f=getFred()

    m1=f.get_series_latest_release('DGS1MO')
    m3=f.get_series_latest_release('DGS3MO')
    m6=f.get_series_latest_release('DGS6MO')
    m1y=f.get_series_latest_release('DGS1')
    m2y=f.get_series_latest_release('DGS2')
    m3y=f.get_series_latest_release('DGS3')
    m5y=f.get_series_latest_release('DGS5')
    m7y=f.get_series_latest_release('DGS7')
    m10y=f.get_series_latest_release('DGS10')
    m20y=f.get_series_latest_release('DGS20')
    m30y=f.get_series_latest_release('DGS30')

    d1=pd.DataFrame(m1)
    d1.columns=['1 Mo']
    d2=pd.DataFrame(m3)
    d2.columns=['3 Mo']
    d3=pd.DataFrame(m6)
    d3.columns=['6 Mo']
    d4=pd.DataFrame(m1y)
    d4.columns=['1 Yr']
    d5=pd.DataFrame(m2y)
    d5.columns=['2 Yr']
    d6=pd.DataFrame(m3y)
    d6.columns=['3 Yr']
    d7=pd.DataFrame(m5y)
    d7.columns=['5 Yr']
    d8=pd.DataFrame(m7y)
    d8.columns=['7 Yr']
    d9=pd.DataFrame(m10y)
    d9.columns=['10 Yr']
    d10=pd.DataFrame(m20y)
    d10.columns=['20 Yr']
    d11=pd.DataFrame(m30y)
    d11.columns=['30 Yr']
    rates=d1.join(d2).join(d3).join(d4).join(d5).join(d6).join(d7).join(d8).join(d9).join(d10).join(d11)
    cols=rates.columns
    rates2=rates.reset_index()
    ren=list(rates2.columns)
    ren[0]='Date'
    rates2.columns=ren
    rates=rates2.set_index('Date')
    
    if year != 0:
        rates=rates[rates.index.to_series().dt.year == year]
    
    return rates.dropna()