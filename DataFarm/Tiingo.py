#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Tiingo 
#
# Module including functions to download data from Tiingo
#############################################################################################      


# Commmon Libraries
#import warnings
#warnings.filterwarnings("once")
import pandas as pd
import Plib.DataFarm.PReader as prd

ORIGIN_TZ='UTC'

import Plib.Keystore as kst
ks=kst.Keystore()
  
#Key to access data
a_key=ks.tiingo_key
root=ks.tiingo_root

##############################################
# Wrapper to substitute IEX data
##############################################
def get_eod_data(symbol, dt_start, dt_end):
    return get_tiingoHdata(symbol, dt_start, dt_end)
    
##############################################
# Historical data download
##############################################
def get_tiingoHdata(symbol, dt_start, dt_end,tz='America/New_York'):
    from pandas.io.json import json_normalize
    
    url=root + '/daily/' + symbol + '/prices?startDate='+dt_start+'&endDate='+dt_end+'&token=' + a_key
    resp = prd.requestJson2RespData(url)
    df = json_normalize(resp)
    
    #resp = prd.requestJson2DF(url)
    if not df.empty:  
        df=df[['date','open','high','low','close','adjClose','adjVolume']]
        df.rename(columns={'date':'Date','open':'Open','high':'High','low':'Low','close':'Close','adjClose':'Adjusted_close','adjVolume':'Volume'}, inplace=True)
        #Recover timezone information
        df['Date'] = pd.to_datetime(df['Date'],utc=True)
        df['Date'] = df['Date'].astype('datetime64[ns]')
        df['Date'] = df.Date.dt.tz_localize(ORIGIN_TZ).dt.tz_convert(tz)
        df['Date'] = df['Date'].astype('datetime64[ns]')
        df=df.set_index('Date')
        df = df.sort_index(ascending=True)
        
    return df

