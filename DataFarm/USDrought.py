#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# US Drought Rest API  
#
# Module including functions to download data from US Drought
#############################################################################################      

import sys
import os

module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

# Commmon Libraries
#import warnings
#warnings.filterwarnings("once")
import pandas as pd
import numpy as np
import datetime
import Plib.DataFarm.PReader as prd
import Plib.Utils.Tools as tl


##############################################
# Data download
##############################################
def getDrought(start_date, end_date, area='conus'): 
    
    start_date='/'.join(start_date.split('-')[::-1][:2][::-1]+list(start_date.split('-'))[0:1])
    end_date='/'.join(end_date.split('-')[::-1][:2][::-1]+list(end_date.split('-'))[0:1])
    if area not in ['conus','total']:
        area='total'
    
    url='https://usdmdataservices.unl.edu/api/USStatistics/GetDroughtSeverityStatisticsByAreaPercent?aoi='+area+'&startdate='+start_date+'&enddate='+end_date+'&statisticsType=1'

    df=prd.requestJson2DF(url)
    if len(df)>0:
        df['MapDate'] = pd.to_datetime(df['MapDate'])
        df = df[['MapDate','None','D0','D1','D2','D3','D4']]
        df.columns=['Date','Normal','Abnormal','Moderate','Severe','Extreme','Terrible']
        df['Date'] = df['Date'].astype('datetime64[ns]')
        df=df.set_index('Date')
        df=df.astype(float)
        df=df.sort_index()
    else:
        df=pd.DataFrame()
    return df

      