#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Wrapper for APIs 
#
# Module including functions to download data
#############################################################################################      

import pandas as pd
import numpy as np

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path+"/")

# Plib imports
import Plib.DataFarm.Alphav as av

##############################################
# Stocks, ETFs, and Currencies Download
##############################################    
def get_eod_data(symbol, dt_start, dt_end,tz='America/New_York',which='yahoo',country='united states'):
    if which=='yahoo':
        return getData(symbol, dt_start, dt_end,stype='',tz=tz)
    else:
        return getData2(symbol, dt_start, dt_end,stype='index',country=country,tz=tz)
        
def get_eod_dataFX(cur1,cur2,dt_start, dt_end,tz='America/New_York',which='yahoo'):
    if which=='avant':
        return av.get_alphaVFXdata(cur1,cur2,dt_start, dt_end,tz=tz)
    else:
        symbol=cur1+cur2+'=X'
        return getData(symbol, dt_start, dt_end,stype='curr',tz=tz)

##############################################
# Continuous Future Download
##############################################    
def getContFuture(exchange,symbol,month, start_date, end_date,tz='America/New_York',which='yahoo'):
    if which=='xyz':
        return 0
    else:
        return getData(symbol, start_date, end_date,stype='future',tz=tz)

##############################################
# Specific Future Download
##############################################    
def getSimpleFuture(exchange,symbol,start_date, end_date,tz='America/New_York',which='yahoo'):
    if which=='xyz':
        return 0
    else:
        return getData(symbol, start_date, end_date,stype='future',tz=tz)

##############################################
# yfinance wrapper
##############################################    
def getData(symbol,start_date, end_date,stype='', tz='America/New_York'):
    import yfinance as yf
    
    if stype=='future':
        symbol=symbol+'=F'
    df=yf.download([symbol], start=start_date, end=end_date, progress=False)
    df.rename(columns={'Adj Close': 'Adjusted_close'}, inplace=True)
    df['Date'] = pd.to_datetime(df.index,utc=True)
    df['Date'] = df['Date'].astype('datetime64[ns]')
    df['Date'] = df.Date.dt.tz_localize('UTC').dt.tz_convert(tz)
    df['Date'] = df['Date'].astype('datetime64[ns]')
    df=df.set_index('Date')
    df = df.sort_index(ascending=True)
    if stype=='future':
        df['openInterest']=0
        df=df[['Open','High','Low','Close','Adjusted_close','openInterest','Volume']]
    elif stype=='curr':
        df=df[['Open','High','Low','Close']]   
    else:
        df=df[['Open','High','Low','Close','Adjusted_close','Volume']] 
    return df

##############################################
# investing wrapper
##############################################    
def getData2(symbol,start_date, end_date,stype='', country='united states', tz='America/New_York'):
    import investpy as ip
    from datetime import datetime
    
    sdate=datetime.strptime(start_date,'%Y-%m-%d').strftime('%d/%m/%Y')
    edate=datetime.strptime(end_date,'%Y-%m-%d').strftime('%d/%m/%Y')
    if stype=='index':
        sr = ip.search_quotes(text=symbol, products=['indices'],countries=[country], n_results=1)
        df=sr.retrieve_historical_data(from_date=sdate, to_date=edate)
    elif stype=='comm':
        sr = ip.search_quotes(text=symbol, products=['commodities'],countries=[country], n_results=1)
        df=sr.retrieve_historical_data(from_date=sdate, to_date=edate)
    elif stype=='etfs':
        sr = ip.search_quotes(text=symbol, products=['etfs'],countries=[country], n_results=1)
        df=sr.retrieve_historical_data(from_date=sdate, to_date=edate)
    elif stype=='bonds':
        sr = ip.search_quotes(text=symbol, products=['bonds'],countries=[country], n_results=1)
        df=sr.retrieve_historical_data(from_date=sdate, to_date=edate)
    df['Adjusted_close']=df['Close']
    df['Date'] = pd.to_datetime(df.index,utc=True)
    df['Date'] = df['Date'].astype('datetime64[ns]')
    df['Date'] = df.Date.dt.tz_localize('UTC').dt.tz_convert(tz)
    df['Date'] = df['Date'].astype('datetime64[ns]')
    df=df.set_index('Date')
    df = df.sort_index(ascending=True)
    df=df[['Open','High','Low','Close','Adjusted_close','Volume']] 
    return df
    