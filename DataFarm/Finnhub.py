#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Finnhub 
#
# Module including functions to download data from Finnhub
#############################################################################################      

# Commmon Libraries
#import warnings
#warnings.filterwarnings("once")
import pandas as pd
import Plib.DataFarm.PReader as prd

ORIGIN_TZ='UTC'

#Key to access data
import Plib.Keystore as kst
ks=kst.Keystore()
a_key=ks.finnhub_key
a_key2=ks.finnhub_key2
root=ks.finnhub_root

##############################################
# Wrapper to substitute IEX data
##############################################
def get_eod_data(symbol, dt_start, dt_end):
    return get_finnhHdata(symbol, dt_start, dt_end)

##############################################
# Historical Data
##############################################
def get_finnhHdata(symbol, dt_start, dt_end,resolution='D',tz='America/New_York'):
    from pandas.io.json import json_normalize
    from Plib.Utils.Tools import getTsFromSDate
    
    dt1=str(getTsFromSDate(dt_start))
    dt2=str(getTsFromSDate(dt_end))
    
    url=root + '/stock/candle?symbol='+symbol+'&resolution=' + resolution + '&from='+dt1+'&to='+dt2+'&token='+a_key
    resp = prd.requestJson2RespData(url)
    df=pd.DataFrame()
    if len(resp)>1:
        df1=pd.DataFrame(resp.values())
        df=df1.transpose()
        df.columns=['Close','High','Low','Open','Split','Timestamp','Volume']
        df['timestamp'] = pd.to_datetime(df['Timestamp'], unit='s')
        df['Date']=df.timestamp.dt.tz_localize(ORIGIN_TZ).dt.tz_convert(tz)
        df['Adjusted_close']=df['Close']
        df=df[['Date','Open','High','Low','Close','Adjusted_close','Volume']]
        df['Date'] = pd.to_datetime(df['Date'])#.dt.date
        df['Date']=df['Date'].astype('datetime64[ns]')
        df=df.set_index('Date')
        df = df.sort_index(ascending=True)
    return df
 
##############################################
# IPOs 
##############################################   
def getIPOCalendar(dt1='2020-12-01',dt2='2020-12-31'):
    from pandas.io.json import json_normalize
    
    url=root + '/calendar/ipo?from='+dt1+'&to='+dt2+'&token='+a_key
    resp = prd.requestJson2RespData(url)
    df = json_normalize(resp['ipoCalendar'])
    return df

##############################################
# Index constituents 
##############################################   
def getIndexConst(index='^GSPC'):
    #Currently support ^GSPC (S&P 500), ^NDX (Nasdaq 100), ^DJI (Dow Jones)
    #^SP500-50	S&P Communication Services Select Sector
    #^SP500-25	S&P Consumer Discretionary Select Sector
    #^SP500-30	S&P Consumer Staples Select Sector
    #^GSPE	S&P Energy Select Sector
    #^SP500-40	S&P Financial Select Sector
    #^SP500-35	S&P Health Care Select Sector
    #^SP500-20	S&P Industrial Select Sector
    #^SP500-15	S&P Materials Select Sector
    #^SP500-60	S&P Real Estate Select Sector
    #^SP500-45	S&P Technology Select Sector
    #^SP500-55	S&P Utilities Select Sector
    #^MID	S&P Mid-Cap 400
    #^SP600	S&P SmallCap 600
    #^OEX	S&P 100
    #^RUI	Russell 1000
    #^RUT	Russell 2000
    #^R25I	Russell 2500
    #^RUA	Russell 3000
    #^RUMIC	Russell Microcap
    #^RMCC	Russell Midcap
    #^RT200	Russell Top 200
    #^NDUEEGF	MSCI Emerging Market
    #^NR736471	MSCI Frontier and Select EM
    #^NDUEACWF	MSCI All Country World (ACWI)
    #^NDDUEAFE	MSCI EAFE Index
    #^M7CN	MSCI China Index
    #^NDDUEMU	MSCI Europe
    #^STOXX50E	Euro Stoxx 50
    #^STOXX	Euro Stoxx 600
    #^GDAXI	DAX
    #^AXJO	S&P/ASX 200
    #TX60.TS	S&P/TSX 60
    #^FTSE	FTSE 100
    
    url=root + '/index/constituents?symbol='+index+'&token='+a_key
    resp = prd.requestJson2RespData(url)
    df=pd.DataFrame(resp['constituents'])
    df.columns=['Tickers']
    return df     

##############################################
# Sector constituents 
##############################################        
def getSectorsConstituents():
    #^SP500-50	S&P Communication Services Select Sector
    #^SP500-25	S&P Consumer Discretionary Select Sector
    #^SP500-30	S&P Consumer Staples Select Sector
    #^GSPE	S&P Energy Select Sector
    #^SP500-40	S&P Financial Select Sector
    #^SP500-35	S&P Health Care Select Sector
    #^SP500-20	S&P Industrial Select Sector
    #^SP500-15	S&P Materials Select Sector
    #^SP500-60	S&P Real Estate Select Sector
    #^SP500-45	S&P Technology Select Sector
    #^SP500-55	S&P Utilities Select Sector

    sectickers=['^SP500-50','^SP500-25','^SP500-30','^GSPE','^SP500-40','^SP500-35','^SP500-20',
            '^SP500-15','^SP500-60','^SP500-45','^SP500-55']
    labels=['Comm_Services','Cons_Discr','Consumer_Staples','Energy',
           'Financial','Healthcare','Industrial','Materials','RealEstate','Technology','Utilities']

    i=0
    for s in sectickers:
        temp = getIndexConst(index=s)
        if i==0:
            temp.columns=[s]
            Sectors=temp.copy()
        else:
            temp.columns=[s]
            frames=[Sectors,temp]
            Sectors=pd.concat(frames,axis=1)
        i=i+1
    Sectors.columns=labels    
    return Sectors
     
     
     
     
        