#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Mboum 
#
# Module including functions to download data from Mboum
#############################################################################################      

ORIGIN_TZ='UTC'

#Key to access data
import Plib.Keystore as kst
ks=kst.Keystore()
a_key=ks.mboum_key
root=ks.mboum_root

##############################################
# Wrapper to substitute IEX data
##############################################
def get_eod_data(symbol, dt_start, dt_end):
    return get_mboumHdata(symbol, dt_start, dt_end)
    
##############################################
# Historical data download
##############################################
def get_mboumHdata(symbol, dt_start, dt_end,freq='1d',tz='America/New_York'):
    import requests
    from pandas.io.json import json_normalize
    import pandas as pd
    import json 
    
    params = {
      'date_from': dt_start,
      'date_to': dt_end}
    
    #5m|15m|30m|1h|1d|1wk|1mo|3mo
    api_result = requests.get(root + '/v1/hi/history/?symbol=' + symbol + '&interval='+freq+'&diffandsplits=true&apikey=' + a_key, params)
    api_response = api_result.json()
    response_data = json.loads(json.dumps(api_response))
    df = json_normalize(response_data['data']['eod'])
    df=df[['date','open','high','low','close','adj_close','volume']]
    df.rename(columns={'date':'Date','open':'Open','high':'High','low':'Low','close':'Close','adj_close':'Adjusted_close','volume':'Volume'}, inplace=True)
    #Recover timezone information
    df['Date'] = pd.to_datetime(df['Date'],utc=True)
    df['Date'] = df['Date'].astype('datetime64[ns]')
    df['Date'] = df.Date.dt.tz_localize(ORIGIN_TZ).dt.tz_convert(tz)
    df['Date'] = df['Date'].astype('datetime64[ns]')
    df=df.set_index('Date')
    df = df.sort_index(ascending=True)

    return df


    

##############################################
# Returns company's metadata
##############################################    
def get_metabyTicker(symbol):
    import requests
    from pandas.io.json import json_normalize
    import pandas as pd
    import json 
    
    headers = {
        'Content-Type': 'application/json'
    }
    
    api_result = requests.get(root + '/qu/quote/profile/?symbol='+symbol+'&apikey='+a_key, headers=headers)
    #print(api_result.json())
    api_response = api_result.json()
    response_data = json.loads(json.dumps(api_response))
    df = json_normalize(response_data)
    #df=df[['date','open','high','low','close','adjClose','adjVolume']]
    #df.rename(columns={'date':'Date','open':'Open','high':'High','low':'Low','close':'Close','adjClose':'Adjusted_close','adjVolume':'Volume'}, inplace=True)
    #df['Date'] = pd.to_datetime(df['Date']).dt.date
    #df['Date'] = df['Date'].astype('datetime64[ns]')
    #df=df.set_index('Date')
    #df = df.sort_index(ascending=True)
    #df=df[['symbol','beta','mktCap','industry','sector']]
    
    return df