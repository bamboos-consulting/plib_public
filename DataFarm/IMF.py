#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# IMF Webservice Data Download 
#
# Module including helper functions 
#############################################################################################      

import Plib.Keystore as kst
ks=kst.Keystore()
url1 = ks.imf_root1
url2 = ks.imf_root2

##############################################
# IMF webservice Download
##############################################
def searchIMFSeries(searchTerm):
    import requests

    key = 'Dataflow'  # Method with series information
    search_term = searchTerm  # Term to find in series names
    series_list = requests.get(f'{url1}{key}').json()\
                ['Structure']['Dataflows']['Dataflow']
    # Use dict keys to navigate through results:
    for series in series_list:
        if search_term in series['Name']['#text']:
            print(f"{series['Name']['#text']}: {series['KeyFamilyRef']['KeyFamilyID']}")

def searchIMFDatastructure(searchTerm):
    import requests
    key = 'DataStructure/' + searchTerm  # Method / series
    dimension_list = requests.get(f'{url1}{key}').json()\
                ['Structure']['KeyFamilies']['KeyFamily']\
                ['Components']['Dimension']
    for n, dimension in enumerate(dimension_list):
        print(dimension['@codelist'])
 
def showIMFCodeList(searchTerm):
    import requests  
    # Example: codes for third dimension, which is 2 in python
    key = 'CodeList/' + searchTerm
    code_list = requests.get(f'{url1}{key}').json()['Structure']['CodeLists']['CodeList']['Code']
    for code in code_list:
        print(code['Description']['#text'],":",code['@value'])

def getDataImf(name,nserie,dataset,country,frequency,start_d,end_d):
    import requests
    import pandas as pd
    
    url = url2 + dataset + '/' + frequency + '.' + country + '.' + nserie + '.?startPeriod=' + start_d +'&endPeriod=' + end_d
    data = requests.get(url).json()
    retdf = pd.DataFrame(data['CompactData']['DataSet']['Series']['Obs'])
    #retdf.columns = ['Date',name];
    if len(retdf.columns)==2:
        retdf.columns = ['Date',name];
    else:
        retdf.columns = ['Date',name,'OFFICIAL'];        
    try:
        retdf = retdf.set_index(pd.to_datetime(retdf['Date']))[name].astype('float')
    except:
        print()
    return retdf
