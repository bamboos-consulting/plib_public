#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Orats 
#
# Module comprising functions to download, store, and retrieve data from Orats
#############################################################################################      
import sys
import os

module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

from Plib.Utils.Tools import ermsg
import Plib.Keystore as kst
import gc
   
#Sleep to avoid overheating/memory crash while storing data
SLEEP_TIME=1
TABLES=['indices','curr','crypto','etf','stock','futures','ifutures','delist','fund']

import datetime
#from cachier import cachier
#cache_exp=datetime.timedelta(days=252)
#@cachier(stale_after=cache_exp)

import warnings
warnings.filterwarnings("once")

##############################################
# Functions to retrieve data from HDF storage
##############################################      
def getTables():
    return list(TABLES)

def getTickers(tname):
    import pandas as pd
    
    ks=kst.Keystore()

    if tname=='indices':
        store_dir=ks.store_dir+'/indices'
        hdfname=store_dir+'/frdata_'+ks.frd_indices_name+'.hdf'
    elif tname=='curr':
        store_dir=ks.store_dir+'/forex'
        hdfname=store_dir+'/frdata_'+ks.frd_forex_name+'.hdf'
    elif tname=='crypto':
        store_dir=ks.store_dir+'/crypto'
        hdfname=store_dir+'/frdata_'+ks.frd_crypto_name+'.hdf'
    elif tname=='etf':
        store_dir=ks.store_dir+'/etfs'
        hdfname=store_dir+'/frdata_'+ks.frd_etfs_name+'_unadj.hdf'
    elif tname=='stock':
        store_dir=ks.store_dir+'/stocks'
        hdfname=store_dir+'/frdata_'+ks.frd_stocks_name+'_unadj.hdf'
    elif tname=='futures':
        store_dir=ks.store_dir+'/futures'
        hdfname=store_dir+'/frdata_'+ks.frd_futures_name+'.hdf'
    elif tname=='ifutures':
        store_dir=ks.store_dir+'/ifutures'
        hdfname=store_dir+'/frdata_'+ks.frd_Ifutures_name+'_unadj.hdf'
    elif tname=='fund':
        store_dir=ks.store_dir+'/fundamentals'
        hdfname=store_dir+'/frdata_'+ks.frd_fdata_name+'.hdf'
    elif tname=='delist':
        store_dir=ks.store_dir+'/delisted'
        hdfname=store_dir+'/frdata_'+ks.frd_delisted_name+'.hdf'
    else:
        return pd.DataFrame([],columns=['Tickers'])   
    
    store = pd.HDFStore(hdfname)
    l=list(store.keys())
    l=[s.strip('/') for s in l]
    store.close()
    return pd.DataFrame(l,columns=['Tickers'])    

def getFundD():
    return getData('fund','Description')
    
def getSectors():
    df=getFundD()
    sectors=list(df['Sector'].dropna().unique())
    sectors.remove('-')
    return sectors,df

def getSectorsConstituents():
    import numpy as np
    import pandas as pd
    
    _,df=getSectors()

    sectors=['Healthcare',
     'Basic Materials',
     'Financial Services',
     'Consumer Defensive',
     'Industrials',
     'Technology',
     'Consumer Cyclical',
     'Real Estate',
     'Energy',
     'Utilities',
     'Communication Services']
    md={}
    for s in sectors:
        md[s]=np.asarray( df[(df['Sector']==s) & (df['Country']=='US')][['Ticker']])
    return pd.concat([pd.DataFrame(v, columns=[k]) for k, v in md.items()], axis=1)

def getIndustries():
    df=getFundD()
    industries=list(df['Industry'].dropna().unique())
    industries.remove('-')
    return industries,df
    
def get_cond(d1,d2,tz):
    import datetime
    import pytz

    d1 = datetime.datetime(int(d1[:4]), int(d1[5:7]), int(d1[8:10]), int(d1[11:13]),int( d1[14:16]), int(d1[17:19]), tzinfo=pytz.utc)
    d1=d1.astimezone(pytz.timezone(tz))
    d2 = datetime.datetime(int(d2[:4]), int(d2[5:7]), int(d2[8:10]), int(d2[11:13]),int( d2[14:16]), int(d2[17:19]), tzinfo=pytz.utc)
    d2=d2.astimezone(pytz.timezone(tz))
    return 'Date>="' + str(d1) + '" & Date<="' + str(d2) + '"'   

def getData(tname,symbol,d1='',d2='',freq='1min',expf=None,tz='America/New_York'):
    import pandas as pd

    ks=kst.Keystore()

    if tname=='indices':
        store_dir=ks.store_dir+'/indices'
        hdfname=store_dir+'/frdata_'+ks.frd_indices_name+'.hdf'
        base_tz=ks.frd_indices_tz
    elif tname=='curr':
        store_dir=ks.store_dir+'/forex'
        hdfname=store_dir+'/frdata_'+ks.frd_forex_name+'.hdf'
        base_tz=ks.frd_forex_tz
    elif tname=='crypto':
        store_dir=ks.store_dir+'/crypto'
        hdfname=store_dir+'/frdata_'+ks.frd_crypto_name+'.hdf'
        base_tz=ks.frd_crypto_tz
    elif tname=='etf':
        store_dir=ks.store_dir+'/etfs'
        hdfname=store_dir+'/frdata_'+ks.frd_etfs_name+'_unadj.hdf'
        base_tz=ks.frd_crypto_tz
    elif tname=='stock':
        store_dir=ks.store_dir+'/stocks'
        hdfname=store_dir+'/frdata_'+ks.frd_stocks_name+'_unadj.hdf'
        base_tz=ks.frd_stocks_tz
    elif tname=='futures':
        store_dir=ks.store_dir+'/futures'
        hdfname=store_dir+'/frdata_'+ks.frd_futures_name+'.hdf'
        base_tz=ks.frd_futures_tz
    elif tname=='ifutures':
        store_dir=ks.store_dir+'/ifutures'
        hdfname=store_dir+'/frdata_'+ks.frd_Ifutures_name+'_unadj.hdf'
        base_tz=ks.frd_Ifutures_tz
    elif tname=='fund':
        store_dir=ks.store_dir+'/fundamentals'
        hdfname=store_dir+'/frdata_'+ks.frd_fdata_name+'.hdf'
    elif tname=='delist':
        store_dir=ks.store_dir+'/delisted'
        hdfname=store_dir+'/frdata_'+ks.frd_delisted_name+'.hdf'
        base_tz=ks.frd_delisted_tz
    else:
        return pd.DataFrame([],columns=['Tickers'])   
    
    if tname=='fund':
        store = pd.HDFStore(hdfname)
        df=store.get(symbol)
        cols=df.columns
        df.columns=list([c.replace(' ','') for c in df.columns])
        store.close()
    else:
        date_cond=''
        if d1 != '' and d2 != '':
            date_cond=[d1,d2]
        df = get_frdata(symbol=symbol,
                      date_cond=date_cond,
                      hdfname=hdfname,
                      freq=freq,expf=expf,tz_base=base_tz,tz=tz)  
    return df
    
def getIncomeStat(df):
    import numpy as np
    l=np.arange(0, 13, 1).tolist()
    return df.iloc[:, l]

def getBalanceSheet(df):
    import numpy as np
    l=np.arange(13, 49, 1).tolist()
    return df.iloc[:, l]

def getCashflowStat(df):
    import numpy as np
    l=np.arange(49, 75, 1).tolist()
    return df.iloc[:, l]

def getValuationM(df):
    import numpy as np
    l=np.arange(75, 79, 1).tolist()
    return df.iloc[:, l]

def getRatios(df):
    import numpy as np
    l=np.arange(79, 99, 1).tolist()
    return df.iloc[:, l]
           
def getFundDataMarket(last_reporting='2022-04-30', selFunc=None, debug=True):
    import pickle
    import pandas as pd
    import time
    
    start=time.perf_counter()
    
    i=0
    flist={}
    tickers=list(getFundD().Ticker)
    stickers=list(getTickers('stock').Tickers.values)
    tickers=[t.replace('.','').replace('-','') for t in tickers if t in stickers]
    tickers=[t for t in tickers if t not in ['SCVX']]
    for s in tickers:
        #try:
        d1=selFunc(getData('fund',s))
        i=i+1
        flist[s]=d1
        if (i%1000==0) and (debug):
            end=time.perf_counter()
            print('Collecting '+str(i),' of ',str(len(tickers)), ' - ',str(end-start),' seconds')
        #except:
        #    pass;
    #end=time.perf_counter()
    return flist

##############################################
# Functions for compatibility
##############################################      
def getContFuture(exchange,symbol, month, dt_start, dt_end):
    #fr.getData('futures',sym,d1='2017-04-10 00:00:00',d2='2017-04-15 00:00:00')
    return getData('futures',symbol,d1=dt_start,d2=dt_end,freq='1min',expf=None,tz='America/New_York')
    
def get_eod_data(symbol, dt_start, dt_end):
    return getData('stock',symbol,d1=dt_start,d2=dt_end,freq='D',expf=None,tz='America/New_York')
    
##############################################
# Helper Functions for Installation
##############################################      

def frDownloadBT1():
    #  delist  ind  etfs forex  fut  ifut  crypto stock
    w=[True,True,False,False,False,False,False,False]
    print('Downloading Delisted, indices to storage...')
    retrieveStoreADJ(backup=True,what=w)

    w=[False,False,False,True,False,False,True,False]
    print('Downloading forex and crypto to storage...')
    retrieveStoreADJ(backup=True,what=w)
    
    #Futures
    w=[False,False,False,False,True,False,False,False]
    print('Downloading continuous futures to storage...')
    retrieveStoreADJ(backup=True,what=w)

def frDownloadBT2():
    #Initial Download of unadjusted data

    #IFutures
    print('Downloading individual contracts futures to storage...')
    w=[False,False,False,True]
    retrieveStoreUADJ(backup=True,what=w)
    
def frDownloadBT3():
    #Initial Download of unadjusted data    
    
    #ETFs
    w=[True,False,False,False]
    print('Downloading ETFs to storage...')
    retrieveStoreUADJ(backup=True,what=w)
    
def frDownloadBT4():
    #Initial Download of unadjusted data    
       
    #Stocks
    print('Downloading Stocks to storage...')
    w=[False,False,True,False]
    retrieveStoreUADJ(backup=True,what=w)

def frDownloadBT5():
    print('Downloading financials data and index changes to storage...')
    frFundamentalData(dwld=True,install=False)
    print('Downloading dividends/splits adj data to storage...')
    frDivSplitData(dwld=True,install=False)
    
def frDownloadADBackup():
    #  delist  ind  etfs forex  fut  ifut  crypto stock
    w=[False,False,True,False,False,False,False,False]
    print('Downloading etfs to storage...')
    retrieveStoreADJ(backup=True,what=w)

    w=[False,False,False,False,False,False,False,True]
    print('Downloading stocks to storage...')
    retrieveStoreADJ(backup=True,what=w)

    print('Downloading futures to storage...')
    w=[False,True,False,False]
    retrieveStoreUADJ(backup=True,what=w)
    
def installFRDataBT1(ddate=''):
    #Installation of backup data without download
    
    if ddate=='': return 0
    
    #Indices
    #  delist   ind  etfs forex  fut  ifut  crypto stock
    w=[False,True,False,False,False,False,False,False]
    retrieveStoreADJ(update=False,backup=False,dwld=False,what=w,bdate=ddate)

    #Forex
    #  delist   ind  etfs forex  fut  ifut  crypto stock
    w=[False,False,False,True,False,False,False,False]
    retrieveStoreADJ(update=False,backup=False,dwld=False,what=w,bdate=ddate)

    #Crypto
    #  delist   ind  etfs forex  fut  ifut  crypto stock
    w=[False,False,False,False,False,False,True,False]
    retrieveStoreADJ(update=False,backup=False,dwld=False,what=w,bdate=ddate)
    
    #Cont Futures
    #  delist   ind  etfs forex  fut  ifut  crypto stock
    w=[False,False,False,False,True,False,False,False]
    retrieveStoreADJ(update=False,backup=False,dwld=False,what=w,bdate=ddate)
    
    #Delisted
    #  delist   ind  etfs forex  fut  ifut  crypto stock
    w=[True,False,False,False,False,False,False,False]
    retrieveStoreADJ(update=False,backup=False,dwld=False,what=w,bdate=ddate)
    
def installFRDataBT2(ddate=''):
    #Installation of backup data UNADJUSTED without download

    if ddate=='': return 0
    #Individual Futures
    #  etfs  fut  stock  ifut
    w=[False,False,False,True]
    retrieveStoreUADJ(update=False,backup=False,dwld=False,what=w,bdate=ddate)

def installFRDataBT3(ddate=''):
    #Installation of backup data UNADJUSTED without download

    if ddate=='': return 0
    #ETFs
    #  etfs  fut  stock
    w=[True,False,False,False]
    retrieveStoreUADJ(update=False,backup=False,dwld=False,what=w,bdate=ddate)

def installFRDataBT4(ddate=''):
    #Installation of backup data UNADJUSTED without download

    if ddate=='': return 0
    #Stocks
    #  etfs  fut  stock
    w=[False,False,True,False,False]
    retrieveStoreUADJ(update=False,backup=False,dwld=False,what=w,bdate=ddate)

def installFRDataBT5(bdate='', update=False):
    print('Installing financial data and index changes....')
    frFundamentalData(dwld=False,install=True,bdate=bdate, update=update)
    print('Installing dividends and splits adj data....')
    frDivSplitData(dwld=False,install=True,bdate=bdate)

def updatesDwldInstall(ddate='', archives=False,downl=True,install=False,w1=[],w2=[]):
    
    ts=str(datetime.datetime.now().strftime('%Y%m%d'))
    
    if archives:
        #  delist  ind  etfs forex  fut  ifut  crypto stock
        w=[True,False,False,False,False,False,False,False]
        if downl and not install:
            print('Downloading delisted data to storage...')
            retrieveStoreADJ(update=True,backup=True,dwld=True,what=w)
            frDownloadBT5()
            print('Downloaded data with timestamp ' + ts)
        elif install:
            retrieveStoreADJ(update=True,backup=False,dwld=False,what=w,bdate=ddate)
            installFRDataBT5(bdate=ddate, update=True)
        return ts
    else:
        
        print('Downloading updates to adj data to storage...')
        if downl and not install:
            w=[False,True,True,True,True,True,True,True]
            retrieveStoreADJ(update=True,backup=True,dwld=True,what=w)
            print('Downloaded data with timestamp ' + ts)
        elif install:
            if w1 != []: w=w1
            w=[False,True,False,True,True,False,True,False]
            retrieveStoreADJ(update=True,backup=False,dwld=False,what=w,bdate=ddate)
    
    #  etfs  fut  stock  ifut
    w=[True,True,True,True]
    print('Downloading updates to unadjusted data to storage...')
    if downl and not install:
        retrieveStoreUADJ(update=True,backup=True,dwld=True,what=w)
        print('Downloaded data with timestamp ' + ts)
    elif install:
        if w2 != []: w=w2
        w=[True,False,True,True]
        retrieveStoreUADJ(update=True,backup=False,dwld=False,what=w,bdate=ddate)
    return ts
    
##############################################
# Rebuild main index or add column index
##############################################      
def rebuild_index(tname,cols=[''],i_performance=7):
    import time
    import pandas as pd
    
    ks=kst.Keystore()

    if tname=='indices':
        store_dir=ks.store_dir+'/indices'
        hdfname=store_dir+'/frdata_'+ks.frd_indices_name+'.hdf'
    elif tname=='curr':
        store_dir=ks.store_dir+'/forex'
        hdfname=store_dir+'/frdata_'+ks.frd_forex_name+'.hdf'
    elif tname=='crypto':
        store_dir=ks.store_dir+'/crypto'
        hdfname=store_dir+'/frdata_'+ks.frd_crypto_name+'.hdf'
    elif tname=='etf':
        store_dir=ks.store_dir+'/etfs'
        hdfname=store_dir+'/frdata_'+ks.frd_etfs_name+'_unadj.hdf'
    elif tname=='stock':
        store_dir=ks.store_dir+'/stocks'
        hdfname=store_dir+'/frdata_'+ks.frd_stocks_name+'_unadj.hdf'
    elif tname=='futures':
        store_dir=ks.store_dir+'/futures'
        hdfname=store_dir+'/frdata_'+ks.frd_futures_name+'.hdf'
    elif tname=='ifutures':
        store_dir=ks.store_dir+'/ifutures'
        hdfname=store_dir+'/frdata_'+ks.frd_Ifutures_name+'_unadj.hdf'
    elif tname=='fund':
        store_dir=ks.store_dir+'/fundamentals'
    elif tname=='delist':
        store_dir=ks.store_dir+'/delisted'
        hdfname=store_dir+'/frdata_'+ks.frd_delisted_name+'.hdf'
    else:
        return pd.DataFrame([],columns=['Tickers'])   

    store = pd.HDFStore(hdfname)
    for k in getTickers(tname).Tickers:
        k=str(k)
        print(store.get_storer(k).group.table)
        print(store.get_storer(k).group.table.cols.index.index)
        if cols==['']:
            store.create_table_index(k, optlevel=i_performance, kind='full')
        else:
            store.create_table_index(k, columns=cols, kind='full')
        print(store.get_storer(k).group.table)
        print(store.get_storer(k).group.table.cols.index.index)

        time.sleep(1)
    store.close()

##############################################
# Retrieve First Rate Data from HDF storage
##############################################      
def download_frdata(url,save_dir,fname='temp.zip',attempts=3):
    import requests
    import zipfile
    import os, sys, os.path
    
    if os.path.isfile(save_dir+'/'+fname):
        try:
            zip_file = zipfile.ZipFile(save_dir+'/'+fname)
            ret = zip_file.testzip()
            if ret is not None:
                os.remove(save_dir+'/'+fname)
            else:
                attempts=0
        except:
            ermsg(err_msg='Exception in reading the file ' + fname)
            print("Exception in reading the file.....")
            os.remove(save_dir+'/'+fname)
        
    while attempts > 0:
        #resume_headers = {'Range':'bytes=0-2000000'}
        r = requests.get(url, allow_redirects=True, stream=True)#, headers=resume_header)
        try:
            total_length = r.headers.get('content-length')
            if total_length is None: total_length=1
            dl = 0
            total_length = int(total_length)
            done=0
            with open(save_dir+'/'+fname,'wb') as f:
                for chunk in r.iter_content(chunk_size=1024*1024):
                    dl += len(chunk)
                    f.write(chunk)
                    done = int(50 * dl / total_length)
                    sys.stdout.write("\r[%s%s]" % ('=' * done, ' ' * (50-done)) )    
                    sys.stdout.flush()
                print("Download complete.")
                try:
                    zip_file = zipfile.ZipFile(save_dir+'/'+fname)
                    ret = zip_file.testzip()
                    if ret is not None:
                        os.remove(save_dir+'/'+fname)
                        attempts=attempts-1
                    else:
                        attempts=0
                except:
                    ermsg(err_msg='Exception in reading the file ' + fname)
                    print("Exception in reading the file.....")
                    os.remove(save_dir+'/'+fname)
                    attempts=attempts-1
        except Exception as ex:
            ermsg(err_msg='Exception in downloading the file ' + str(url))
            print('Error while downloading')
            attempts=attempts-1
                                 
##############################################
# Unzip Downloaded Data 
##############################################      
def extract_frdata(dir1,dir2,fnz,fnc):
    from zipfile import ZipFile
    from zipfile import BadZipfile
    import os, sys, os.path

    fname_zip=dir1+'/'+fnz
    fname_csv=dir2+'/'+fnc
    
    ret=0
    print('Unzipping File ' + fname_zip)
    try:
        with ZipFile(fname_zip, 'r') as zipObj:
            if not os.path.isfile(fname_csv):
                zipObj.extractall(dir2) 
    except BadZipfile:
        ermsg(err_msg='Error while unzipping ' + fname_zip + ' to ' + fname_csv)
        print('Error while unzipping ' + fname_zip)
        ret=-1
    return ret

##############################################
# Store Unzipped Data tp HDF 
##############################################     
def store_frdata(dirName1,dirName2,hdfname,col_names=[],tz='US/Eastern',delete=False,update=True):
    import pandas as pd
    import os
    import time
    
    def getListOfFiles(dirName):
        import os, sys, os.path
        listOfFile = os.listdir(dirName)
        allFiles = list()
        for entry in listOfFile:
            fullPath = os.path.join(dirName, entry)
            if os.path.isdir(fullPath):
                print('')
            else:
                allFiles.append(fullPath)    
        return allFiles

    if not update and hdfname=='frdata_us_Ifutures_unadj.hdf':
        dirName1=dirName1+'/individual_contracts_pre_2021'
    column_names=col_names
    myhdf=dirName2+'/'+hdfname

    # Get the list of all files in directory tree at given path
    listOfFiles = getListOfFiles(dirName1)
    
    store=None
    
    # Print the files
    c=1
    for elem in listOfFiles:
        
        myfile=str(elem)
        if hdfname=='frdata_us_delisted.hdf':
            check_file=str(myfile).find('_1min_UNADJUSTED.txt')
        else:
            check_file=str(myfile).find('.txt')

        if check_file != -1 :
    
            if hdfname=='frdata_us_Ifutures_unadj.hdf':
                symbol=myfile.split('.')[0].split('/')[-1]
                month=symbol[-3]
                year=symbol[-2:]
                symbol=symbol[:-3]
            elif hdfname=='frdata_us_delisted.hdf':
                symbol=myfile.split('-')[0].split('/')[-1]
            else:
                symbol=myfile.split('_')[0].split('/')[-1]
                #symbol=symbol.replace('.','')      #Dots are allowed in objects names
            
            print(hdfname, ' Symbol: ', symbol, '-', 'Processing File ' + str(listOfFiles.index(elem)+1 ) + ' of '  + str(len(listOfFiles)) + ' : ',myfile)
            
            df=pd.DataFrame()
            if hdfname=='frdata_forex.hdf':
                df = pd.read_csv(myfile,parse_dates={"Date" : [0,1]},header=None,sep=',',names=column_names,index_col=False)
            else:
                df = pd.read_csv(myfile,header=None,sep=',',names=column_names,index_col=False)
            
            if hdfname=='frdata_us_Ifutures_unadj.hdf':
                df['Month']=month
                df['Month'] = df['Month'].astype('string')
                df['Year']=year
                df['Year'] = df['Year'].astype('int64')
                df['Date'] = pd.to_datetime(df['Date'])
                df.Date=df.Date.astype('datetime64[ns]')
                df.Date=df.Date.dt.tz_localize(tz=tz, ambiguous=True)
                df=df.set_index(['Date','Month','Year'])
            else:
                df['Date'] = pd.to_datetime(df['Date'])
                df.Date=df.Date.astype('datetime64[ns]')
                df=df.set_index('Date')
                #df.index=df.index.astype('datetime64[ns]')
                df.index=df.index.tz_localize(tz=tz, ambiguous=True)
            
            #df.index.freq = 'T'
            df = df.sort_index(ascending=True)
            
            #['Date','Open', 'High', 'Low', 'Close', 'Volume']
            if 'Open' not in df.columns:
                df['Open']=0
            elif 'High' not in df.columns:
                df['High']=0
            elif 'Low' not in df.columns:
                df['Low']=0
            elif 'Close' not in df.columns:
                df['Close']=0
            elif 'Volume' not in df.columns:
                df['Volume']=0
            for my_col in df.columns:
                if my_col != 'Date':
                    if my_col in ['Month','Year']:
                        pass
                    else:
                        df[my_col] = df[my_col].astype('float64')
            
            if update:
                date_cond=[str(df.index.min()),str(df.index.max())]
                try:
                    arch=get_frdata(symbol.replace('-',''),myhdf,date_cond)  #.replace('/','')
                    idx_archives = arch.index
                    idx_updates = df.index
                    idx3=idx_updates.difference(idx_archives)
                    df=df.loc[idx3]
                except:
                    strupd='During update '
                    #ermsg(err_msg=strupd + 'New Ticker '+ symbol + ' in ' + myfile + ' to be stored in table ' + hdfname)
                    pass;      
            store = pd.HDFStore(myhdf)
            try:
                if len(df)>0:
                    #HDFStore.append has by default index=True and format = 'table' 
                    #but data_columns= None (True to use all columns)
                    #Thus, store.append(symbol, df, format='table',complevel=6, complib='lzo')
                    #creates only one index corresponding to the dataframe index
                    #hdf_key = 'hdf_key'
                    #df_cols_to_index = [...] # list of columns (labels) that should be indexed
                    #store = pd.HDFStore(hdf_filename)
                    #for chunk in pd.read_csv(csv_filename, chunksize=500000):
                    #    # don't index data columns in each iteration - we'll do it later ...
                    #    store.append(hdf_key, chunk, data_columns=df_cols_to_index, index=False)
                    #    # index data columns in HDFStore
                    #store.create_table_index(hdf_key, columns=df_cols_to_index, optlevel=9, kind='full')
                    #store.close()
                    if hdfname=='frdata_us_Ifutures_unadj.hdf':
                        store.append(symbol, df, format='table',complevel=6, complib='lzo') #.replace('/','')
                    else:
                        store.append(symbol.replace('-',''), df, format='table',complevel=6, complib='lzo')
                    print('Added ',len(df),' records')
                else:
                    print('Added 0 records')
            except:
                strupd=''
                if update: strupd='During update '
                ermsg(err_msg=strupd + 'Error processing '+ symbol + ' in ' + myfile + ' to be stored in table ' + hdfname)
                print('Error processing ', myfile)
            #print('Processed: ',elem)
        else:
            print('File ' + str(myfile) + ' not considered/found....')
        time.sleep(SLEEP_TIME)       
        c=c+1
        if c==60:
            if store!= None: store.close()
            time.sleep(SLEEP_TIME*3)
            store=None
            #store = pd.HDFStore(myhdf)
    if store!= None: 
        store.close()
    
    if delete:
        listOfFiles = getListOfFiles(dirName1)
        for elem in listOfFiles:
            myfile=str(elem)
            os.remove(myfile)
        try:
            os.rmdir(dirName1+'/individual_contracts_pre_2021')
        except:
            pass;
        try:    
            os.rmdir(dirName1+'/delisted_tickers_A-L')
        except:
            pass;
        try:
            os.rmdir(dirName1+'/delisted_tickers_M-Z') 
        except:
            pass;
        try:    
            os.rmdir(dirName1+'/delisted')                    
        except:
            pass;
            
    gc.collect() 
    print('Finished.')

##############################################
# Retrieve FRData from HDF storage
##############################################      
def get_frdata(symbol,hdfname,date_cond='',freq='1min',expf=None,tz_base='US/Eastern',tz='America/New_York'):
    import os, sys, os.path
    import pandas as pd
    import Plib.Utils.Tools as tl
    pd.options.mode.chained_assignment = None  # default='warn'


    myindex='index'
    
    ohlcv_dict = {
     'Open': 'first',
     'High': 'max',
     'Low': 'min',
     'Close': 'last',
     'Adjusted_close': 'last',
     'Volume': 'sum'
    }
    
    if hdfname.split('/')[-1]=='frdata_us_Ifutures_unadj.hdf':
        ohlcv_dict = {
         'Open': 'first',
         'High': 'max',
         'Low': 'min',
         'Close': 'last',
         'Adjusted_close': 'last',
         'Volume': 'sum',
         'Month': 'last',
         'Year': 'last'
        }
    
    months={'F':1,
    'G':2,
    'H':3,
    'J':4,
    'K':5,
    'M':6,
    'N':7,
    'Q':8,
    'U':9,
    'V':10,
    'X':11,
    'Z':12}
    
    months2={1:'F',
    2:'G',
    3:'H',
    4:'J',
    5:'K',
    6:'M',
    7:'N',
    8:'Q',
    9:'U',
    10:'V',
    11:'X',
    12:'Z'}
    
    
    store = pd.HDFStore(hdfname)
    
    if date_cond != '':
        if hdfname.split('/')[-1]!='frdata_us_Ifutures_unadj.hdf':
            c = store.select_column(symbol,myindex)
            where = pd.DatetimeIndex(c).to_series().between(date_cond[0], date_cond[1])
            df=store.select(symbol,where = where)
        else:
            df=store.select(symbol, where = get_cond(date_cond[0],date_cond[1],tz_base))
        
    else:
        df=store.get(symbol)
    store.close()
    
    if hdfname.split('/')[-1]=='frdata_us_Ifutures_unadj.hdf':
        df=df.reset_index()
        df=df.set_index('Date')
    
    if tz_base != tz:
        df.index=df.index.tz_convert(tz)
    
    df['Adjusted_close']=df['Close']
    if hdfname.split('/')[-1]=='frdata_us_Ifutures_unadj.hdf':
        df=df[['Open','High','Low','Close','Adjusted_close','Volume','Month','Year']]
        df.sort_values(['Date','Year','Month'], inplace=True)
        df['Year'] =df['Year'] +2000
        df['Year'] = df['Year'].astype('int')
    else:
        df=df[['Open','High','Low','Close','Adjusted_close','Volume']]
        #df=df[['Open','High','Low','Close','Adjusted_close','Volume']]
    
    #10T for 10min, 30T, H for hourly, D for daily, W for weekly, M for monthly 
    if freq != '1min':
        if hdfname.split('/')[-1]=='frdata_us_Ifutures_unadj.hdf':
            df=tl.getFutMonthsExp(df,expf=expf)
            
            df['Month']=df['Month'].map(months) 
            df['Month'] = df['Month'].astype('int')
            df['Year'] = df['Year'].astype('int')
            
            temp=df.head(1).copy()
            temp=temp.drop(temp.index)
            for e in df.Expiration.unique():
                if freq=='H':
                    d1=df[df.Expiration==e].resample('60Min',offset="30min").agg(ohlcv_dict)
                else:
                    d1=df[df.Expiration==e].resample(freq).agg(ohlcv_dict)
                temp=temp.append(d1)
            del temp['Dte']
            del temp['Expiration']
            
            temp=temp.dropna()
            temp.Month=temp.Month.astype('int')
            temp.Year=temp.Year.astype('int')
            temp['Month']=temp['Month'].map(months2) 
            temp['Month'] = temp['Month'].astype('string')
            
            df=temp
        else:
            if freq=='H':
                df = df.resample('60Min',base=30).agg(ohlcv_dict)
            elif freq=='W':
                df = df.resample('W-MON').agg(ohlcv_dict)
            else:
                df = df.resample(freq).agg(ohlcv_dict)
    return df.dropna()

##############################################
# Manage the Log for Download and Store
##############################################   
def store_log(df,frd_log,log_table='us_indices',delete=False):
    import pandas as pd
    import os
    
    print('Logging File ' + frd_log)
    store = pd.HDFStore(frd_log)
    store.append(log_table, df, format='table',data_columns=['segm','last_update_to','fname'])
    store.close()
    if delete:
        os.remove(frd_log)

def print_log(frd_log,log_name='us_indices'):
    import os, sys, os.path
    import pandas as pd
    pd.options.mode.chained_assignment = None  # default='warn'
    
    df=pd.DataFrame()
    if os.path.isfile(frd_log):
        store = pd.HDFStore(frd_log)
        df=store.get(log_name)
        store.close()
    return df.groupby(['segm','last_update_to','fname']).sum()

def check_log(frd_log,bdate,frd_hdf,log_name='us_indices',i=0):
    import os, sys, os.path
    import pandas as pd
    pd.options.mode.chained_assignment = None  # default='warn'
    
    log_cond='segm="'+str(i)+'" & last_update_to="'+bdate+'" & fname="'+frd_hdf+'"'    

    df=pd.DataFrame()
    if os.path.isfile(frd_log):
        store = pd.HDFStore(frd_log)
        df=store.select(log_name,where=log_cond)
        store.close()
    if len(df)>0:
        ret=str(df.finished.iloc[-1])
    else:
        ret='-1'
    return ret     

##############################################
# Manage Update and Backup of Data
##############################################   
def updateStore(i,save_dir,unzip_dir,store_dir,bdate,p_data,url,hdffn,cnames,tz,backup=False,update=True,dwld=True,delete=True):
    import pandas as pd
    from datetime import datetime
    
    str_i=''
    if len(str(i))==1:
        str_i='0'+str(i)
    bdate=str(bdate.replace('-',''))
    if update:
        frd_zip='frdata_u_'+p_data+'_'+bdate+'_'+str(i)+'.zip'
    else:
        frd_zip='frdata_'+p_data+'_'+bdate+'_'+str(i)+'.zip'
    frd_csv='frdata_'+p_data+'_'+bdate+'.txt'
    frd_hdf='frdata_'+p_data+'.hdf'   
    frd_log=store_dir+'/frdata_'+p_data+'_log.hdf'

    print('Processing data... '+frd_zip)
    if dwld: download_frdata(url,save_dir=save_dir,fname=frd_zip)

    if not backup:
        if os.path.isfile(save_dir+'/'+frd_zip):
            check=check_log(frd_log,bdate,frd_hdf,p_data,i)
            if check=='-1':
                #not present
                ret=extract_frdata(save_dir,unzip_dir,frd_zip,frd_csv)
                if p_data=='us_Ifutures':
                    unzip_dir=unzip_dir+'/contracts_pre_2021'
                elif p_data=='us_delisted':
                    try:
                        os.rename(unzip_dir+'/delisted_tickers_A-L', unzip_dir+'/delisted')
                    except:
                        pass;
                    try:
                        os.rename(unzip_dir+'/delisted_tickers_M-Z', unzip_dir+'/delisted')
                    except:
                        pass;
                    unzip_dir=unzip_dir+'/delisted'
                if ret==0:
                    print(bdate,str_i,frd_hdf) 
                    store_log(pd.DataFrame([str_i,bdate,frd_hdf,'0'],['segm','last_update_to','fname','finished']).T,frd_log,p_data)
                    store_frdata(dirName1=unzip_dir,dirName2=store_dir,hdfname=frd_hdf,col_names=cnames,tz=tz,delete=delete,update=update)
                    store_log(pd.DataFrame([str_i,bdate,frd_hdf,'1'],['segm','last_update_to','fname','finished']).T,frd_log,p_data)
                    print('Data succesfully stored...')
                else:
                    ermsg(err_msg='Check downloaded file for errors.... '+str(bdate)+' '+str(str_i)+' '+str(frd_hdf) )
                    print('Check downloaded file for errors....')
            elif check=='0':
                #unfinished
                ermsg(err_msg='Error while processing data.... ' + str(bdate)+' Delete existing data/log and retry... ' + str(bdate)+' '+str(str_i)+' '+str(frd_hdf) )
                print('Error while processing data.... ' + str(bdate)+' Delete existing data/log and retry...')
            elif check=='1':
                #already present
                ermsg(err_msg='Data already present....  '+str(bdate)+' '+str(str_i)+' '+str(frd_hdf))
                print('Data already present.... ' + str(bdate))
            else:
                ermsg(err_msg='Error consulting the log... '+str(bdate)+' '+str(str_i)+' '+str(frd_hdf))
                print('Error consulting the log...')
        else:
            print('File not found...')
            ermsg(err_msg='File not found...')
            
def retrieveStoreADJ(update=False,backup=False,dwld=True,what=[],bdate='',delete=True):
    import pandas as pd
    from datetime import datetime
    import Plib.Keystore as kst
    ks=kst.Keystore()
    
    if backup:
        save_dir=ks.backup_dir
    else:
        save_dir=ks.save_dir
    unzip_dir=save_dir+'/unzip'
    store_dir=ks.store_dir
    if bdate=='': bdate=str(datetime.today().date())
   
    #DELISTED#########################################   
    if what[0]:
        hdffn=ks.frd_delisted_name
        p_data=hdffn
        cnames=ks.frd_delisted_cols
        tz=ks.frd_delisted_tz
        #if update:
        #    url=[ks.frd_delisted]
        #else:
        #    url=ks.frd_delisted_all
        url=ks.frd_delisted_all
            
        i=0
        for u in url:
            updateStore(i,save_dir,unzip_dir,store_dir+'/delisted',bdate,p_data,u,hdffn,cnames,tz,backup,update,dwld,delete)
            i=i+1
    
    #INDICES#########################################   
    if what[1]:
        hdffn=ks.frd_indices_name
        p_data=hdffn
        cnames=ks.frd_indices_cols
        tz=ks.frd_indices_tz
        if update:
            url=[ks.frd_indices]
        else:
            url=ks.frd_indices_all
    
        i=0
        for u in url:
            updateStore(i,save_dir,unzip_dir,store_dir+'/indices',bdate,p_data,u,hdffn,cnames,tz,backup,update,dwld,delete)
            i=i+1
        
    #ETFS#########################################   
    if what[2]:
        hdffn=ks.frd_etfs_name
        p_data=hdffn
        cnames=ks.frd_etfs_cols
        tz=ks.frd_etfs_tz
        if update:
            url=[ks.frd_etfs]
        else:
            url=ks.frd_etfs_all
    
        i=0
        for u in url:
            updateStore(i,save_dir,unzip_dir,store_dir+'/etfs',bdate,p_data,u,hdffn,cnames,tz,backup,update,dwld,delete)
            
            i=i+1
    
    #FOREX#########################################   
    if what[3]:
        hdffn=ks.frd_forex_name
        p_data=hdffn
        cnames=ks.frd_forex_cols
        tz=ks.frd_forex_tz
        if update:
            url=[ks.frd_forex]
        else:
            url=ks.frd_forex_all
    
        i=0
        for u in url:
            updateStore(i,save_dir,unzip_dir,store_dir+'/forex',bdate,p_data,u,hdffn,cnames,tz,backup,update,dwld,delete)
            i=i+1
        
    #FUTURES#########################################   
    if what[4]:
        hdffn=ks.frd_futures_name
        p_data=hdffn
        cnames=ks.frd_futures_cols
        tz=ks.frd_futures_tz
        if update:
            url=[ks.frd_futures]
        else:
            url=ks.frd_futures_all
    
        i=0
        for u in url:
            updateStore(i,save_dir,unzip_dir,store_dir+'/futures',bdate,p_data,u,hdffn,cnames,tz,backup,update,dwld,delete)
            i=i+1
    
    #IFUTURES#########################################   
    if what[5]:
        hdffn=ks.frd_Ifutures_name
        p_data=hdffn
        cnames=ks.frd_Ifutures_cols
        tz=ks.frd_Ifutures_tz
        if update:
            url=[ks.frd_Ifutures]
        else:
            url=ks.frd_Ifutures_all
    
        i=0
        for u in url:
            updateStore(i,save_dir,unzip_dir,store_dir+'/ifutures',bdate,p_data,u,hdffn,cnames,tz,backup,update,dwld,delete)
            i=i+1

    #CRYPTO#########################################   
    if what[6]:
        hdffn=ks.frd_crypto_name
        p_data=hdffn
        cnames=ks.frd_crypto_cols
        tz=ks.frd_crypto_tz
        if update:
            url=[ks.frd_crypto]
        else:
            url=ks.frd_crypto_all
    
        i=0
        for u in url:
            updateStore(i,save_dir,unzip_dir,store_dir+'/crypto',bdate,p_data,u,hdffn,cnames,tz,backup,update,dwld,delete)
            i=i+1
    
    #STOCKS#########################################   
    if what[7]:
        hdffn=ks.frd_stocks_name
        p_data=hdffn
        cnames=ks.frd_stocks_cols
        tz=ks.frd_stocks_tz
        if update:
            url=[ks.frd_stocks]
        else:
            url=ks.frd_stocks_all
    
        i=0
        for u in url:
            updateStore(i,save_dir,unzip_dir,store_dir+'/stocks',bdate,p_data,u,hdffn,cnames,tz,backup,update,dwld,delete)
            i=i+1
    
def retrieveStoreUADJ(update=False,backup=False,dwld=True,what=[],bdate='',delete=True):
    import pandas as pd
    from datetime import datetime
    import Plib.Keystore as kst
    ks=kst.Keystore()
    
    if backup:
        save_dir=ks.backup_dir
    else:
        save_dir=ks.save_dir
    unzip_dir=save_dir+'/unzip'
    store_dir=ks.store_dir
    if bdate=='': bdate=str(datetime.today().date())
   
           
    #ETFS#########################################   
    if what[0]:
        hdffn=ks.frd_etfs_name+'_unadj'
        p_data=hdffn
        cnames=ks.frd_etfs_cols
        tz=ks.frd_etfs_tz
        if update:
            url=[ks.frd_etfs_uadj]
        else:
            url=ks.frd_etfs_all_uadj
    
        i=0
        for u in url:
            updateStore(i,save_dir,unzip_dir,store_dir+'/etfs',bdate,p_data,u,hdffn,cnames,tz,backup,update,dwld,delete)
            
            i=i+1
    
    #FUTURES#########################################   
    if what[1]:
        hdffn=ks.frd_futures_name+'_unadj'
        p_data=hdffn
        cnames=ks.frd_futures_cols
        tz=ks.frd_futures_tz
        if update:
            url=[ks.frd_futures_uadj]
        else:
            url=ks.frd_futures_all_uadj
    
        i=0
        for u in url:
            updateStore(i,save_dir,unzip_dir,store_dir+'/futures',bdate,p_data,u,hdffn,cnames,tz,backup,update,dwld,delete)
            i=i+1
                
    #STOCKS#########################################   
    if what[2]:
        hdffn=ks.frd_stocks_name+'_unadj'
        p_data=hdffn
        cnames=ks.frd_stocks_cols
        tz=ks.frd_stocks_tz
        if update:
            url=[ks.frd_stocks_uadj]
        else:
            url=ks.frd_stocks_all_uadj
    
        i=0
        for u in url:
            updateStore(i,save_dir,unzip_dir,store_dir+'/stocks',bdate,p_data,u,hdffn,cnames,tz,backup,update,dwld,delete)
            i=i+1

    #IFUTURES#########################################   
    if what[3]:
        hdffn=ks.frd_Ifutures_name+'_unadj'
        p_data=hdffn
        cnames=ks.frd_Ifutures_cols
        tz=ks.frd_Ifutures_tz
        if update:
            url=[ks.frd_Ifutures_uadj]
        else:
            url=ks.frd_Ifutures_all_uadj
    
        i=0
        for u in url:
            updateStore(i,save_dir,unzip_dir,store_dir+'/ifutures',bdate,p_data,u,hdffn,cnames,tz,backup,update,dwld,delete)
            i=i+1
    
##################################################
# Functions to extract and store Fundamental Data
##################################################   
def getFundIndex(excel):
    import collections

    df=extract_fdata2(excel)

    alphabet = collections.defaultdict(list)
    for word in list(df['Ticker']):
        alphabet[word[0].upper()].append(word)
    
    return df, alphabet
    
def extract_fdata(excel,ticker):
    from pandas import read_excel
    import datetime

    df = read_excel(excel,
                       sheet_name=ticker,nrows=109,
                       skiprows=lambda x: x in [0, 1,3,19,56,83,86,89,98,102,107],
                       skipfooter=0, engine='openpyxl')

    myindex=list(df[df.columns[0]].T)
    cols=[]
    for c in df.columns:
        if type(c)== datetime.datetime:
            cols.append(c)
        else:
            del df[c]

    df.columns=cols
    #df=df[1:]
    df['Date']=myindex#[1:]
    df=df.set_index('Date')
    df=df.T
    df=df.fillna(0)
    df.index=df.index.astype('datetime64[ns]')
    df=df.resample('Q-APR').mean()
    #df.index.freq = 'Q-APR'
    ticker=ticker.replace('.','')
    ticker=ticker.replace('-','')
    ticker=ticker.replace(':','')
    ticker=ticker.replace('_','')
    df['Ticker']=ticker

    return df

def extract_fdata2(excel):
    from pandas import read_excel

    df = read_excel(excel,
                       sheet_name='Companies',nrows=5931,
                       skiprows=lambda x: x in [0, 1],
                       skipfooter=0, engine='openpyxl') #
    cols=[]
    for c in df.columns:
        if c.find('Unnamed',0,7):
            cols.append(c)
        else:
            del df[c]
    return df

def storeFundData(excel,path,update=False):
    import time
    import pandas as pd
    import Plib.Keystore as kst

    ks=kst.Keystore()
    store_dir=ks.store_dir+'/fundamentals'
    myhdf=store_dir+'/frdata_'+ks.frd_fdata_name+'.hdf'
    
    print('Storing Company Descriptive Data...')
    df,alphabet=getFundIndex(excel)
    for my_col in df.columns:
        df[my_col] = df[my_col].astype('str')
    store = pd.HDFStore(myhdf)
    store.append('Description', df, format='table',complevel=6, complib='lzo')
    store=store.close()
    
    i=0
    for k in alphabet.keys():
        print('Storing Tickers starting with ',k)
        tickers=alphabet[k]
        store=None
        c=1
        for t in tickers:
            print('Storing ',t,'...')
            try:
                df2=extract_fdata(path+k+'_tickers.xlsx',t)
                #t=t.replace('.','')
                t=t.replace('-','')
                t=t.replace(':','')
                t=t.replace('_','')
                
                if update:
                    date_cond=[str(df2.index.min()),str(df2.index.max())]
                    try:
                        arch=getData('fund',t.replace('-',''),d1='',d2='')
                        #arch=get_frdata(t.replace('-',''),myhdf,date_cond)  #.replace('/','')
                        idx_archives = arch.index
                        idx_updates = df2.index
                        idx3=idx_updates.difference(idx_archives)
                        df2=df2.loc[idx3]
                    except:
                        strupd='During update '
                        pass;      
                    store = pd.HDFStore(myhdf)
                    try:
                        if len(df)>0:
                            store.append(t, df2, format='table',complevel=6, complib='lzo')
                            print('Added ',len(df2),' records')
                        else:
                            print('Added 0 records')
                    except:
                        strupd=''
                        if update: strupd='During update '
                        ermsg(err_msg=strupd + 'Error processing '+ t + ' in ' + myfile + ' to be stored in table ' + hdfname)
                        print('Error processing ', path+k+'_tickers.xlsx')
                else:
                    store = pd.HDFStore(myhdf)      
                    store.append(t, df2, format='table',complevel=6, complib='lzo')
            except:
                ermsg(err_msg='error while processing '+str(t))
                print('Error processing ', t)
            time.sleep(SLEEP_TIME)       
            c=c+1
            if c==1000:
                time.sleep(SLEEP_TIME)
                if store!= None: 
                    store.close()
                    store = None
                    store = pd.HDFStore(myhdf)
        if store!= None: 
            store.close()    

def download(url,save_dir,fname='temp.zip',attempts=1):
    import requests
    import os, sys, os.path

    while attempts > 0:
        #resume_headers = {'Range':'bytes=0-2000000'}
        r = requests.get(url, allow_redirects=True, stream=True)#, headers=resume_header)
        try:
            total_length = r.headers.get('content-length')
            if total_length is None: total_length=1
            dl = 0
            total_length = int(total_length)
            done=0
            with open(save_dir+'/'+fname,'wb') as f:
                for chunk in r.iter_content(chunk_size=1024*1024):
                    dl += len(chunk)
                    f.write(chunk)
                    done = int(50 * dl / total_length)
                    sys.stdout.write("\r[%s%s]" % ('=' * done, ' ' * (50-done)) )    
                    sys.stdout.flush()
                print("Download complete.")
                attempts=attempts-1
        except Exception as ex:
            ermsg(err_msg='error while downloading '+fname)
            print('Error while downloading')
            attempts=attempts-1

# function to start download and installation
def frFundamentalData(dwld=False,install=False, bdate='',update=False):
    import zipfile
    import os
    import shutil
    
    ks=kst.Keystore()
    if bdate=='':
        bdate=str(datetime.datetime.now().strftime('%Y%m%d'))

    #indeces changes
    zname5=ks.frd_index_name +'_'+ bdate + '.zip'
    url5=ks.frd_index
    
    #Fundamental Data
    save_dir=ks.store_dir+'/frdata/'
    ename='_US_listed_companies.xlsx'
    zname=ks.frd_fdata_name + '_'+ bdate + '.zip'
    excel=save_dir+zname
    url=ks.frd_fdata
    
    print(save_dir)
    if dwld:
        download(url5,save_dir,fname=zname5,attempts=1)
        download(url,save_dir,fname=zname,attempts=1)
    if install:
        myfile= zipfile.ZipFile(save_dir+zname)
        myfile.extractall(path=save_dir +'/unzip')
        storeFundData(ename,save_dir +'/unzip/',update=update)
        shutil.rmtree(save_dir +'/unzip')

##################################################
# Functions to manage splits and dividends
##################################################   

# function to start download and installation
def frDivSplitData(dwld=True,install=False, bdate=''):
    import zipfile
    import os
    import shutil

    ks=kst.Keystore()    
    if bdate=='':
        bdate='_'+str(datetime.datetime.now().strftime('%Y%m%d'))
    else:
        bdate='_'+bdate
        
    save_dir=ks.store_dir+'/frdata/'
    inst_dir=ks.store_dir+'/spldiv/'
    
    s='frdata_'
    
    #ETFs div/split
    zname1=s+ks.frd_splits_etfs_name + bdate  + '.zip'
    url1=ks.frd_splits_etfs
    zname2=s+ks.frd_divs_etfs_name + bdate  + '.zip'
    url2=ks.frd_divs_etfs
    #Stocks div/split
    zname3=s+ks.frd_splits_stocks_name + bdate  + '.zip'
    url3=ks.frd_splits_stocks
    zname4=s+ks.frd_divs_stocks_name + bdate  + '.zip'
    url4=ks.frd_divs_stocks
    
    print(save_dir)
    if dwld:
        download(url1,save_dir,fname=zname1,attempts=1)
        download(url2,save_dir,fname=zname2,attempts=1)
        download(url3,save_dir,fname=zname3,attempts=1)
        download(url4,save_dir,fname=zname4,attempts=1)
    if install:
        print(zname1)
        myfile= zipfile.ZipFile(save_dir+zname1)
        myfile.extractall(path=inst_dir)
        print(zname2)
        myfile= zipfile.ZipFile(save_dir+zname2)
        myfile.extractall(path=inst_dir)
        print(zname3)
        myfile= zipfile.ZipFile(save_dir+zname3)
        myfile.extractall(path=inst_dir)
        print(zname4)
        myfile= zipfile.ZipFile(save_dir+zname4)
        myfile.extractall(path=inst_dir)

# Dividend Adjustment Factor
# Subtract the dividend from the previous close price 
# and divide the result by the previous close price. 
# Then multiply back from the previous day
def divAdjF(df, ddate, lbl='Close', div=0):
    import numpy as np
    import pandas as pd
    
    prevdp=df[df.index < ddate][lbl]#[0]
    af=(prevdp-div)/prevdp
    return (np.where(df.index < ddate,df[lbl]*af,df[lbl])).round(2)
    
# Split Adjustment Factor
def splitAdjF(df, ddate, lbl='Close', lblv='Volume', sf=0):
    import numpy as np
    import pandas as pd
    
    rp=(np.where(df.index < ddate,df[lbl]*sf,df[lbl])).round(2)
    rv= (np.where(df.index < ddate,df[lblv]*(1/sf),df[lblv])).round(2)
    return rp,rv

def getAdjData(df, symbol='MSI', lbl='Close', lblv='Volume', bounds=[], inplace=False):
    import numpy as np
    import pandas as pd
    from datetime import datetime
    
    b1=datetime.strptime(bounds[0], '%Y-%m-%d %H:%M:%S')
    b1 = b1.replace(tzinfo=df.index.tz)
    b2=datetime.strptime(bounds[1], '%Y-%m-%d %H:%M:%S')
    b2 = b2.replace(tzinfo=df.index.tz)

    ks=kst.Keystore()
    inst_dir=ks.store_dir+'/spldiv/'
    
    try:
        temp= pd.read_csv(inst_dir+symbol+'_divs.txt', header=None)  
        temp.columns=['Date','Factor']
        temp.Date=pd.to_datetime(temp.Date).dt.tz_localize('UTC').dt.tz_convert(str(df.index.tz))
        temp=temp.sort_values('Date',ascending=True)
        #[df[lbl]=divAdjF(df, d[0], lbl=lbl, div=d[1]) if inplace else df['adj_'+lbl]=divAdjF(df, d[0], lbl=lbl, div=d[1]) for d in temp.values]
        for d in temp.values:
            if d[0]>=b1 and d[0]<=b2:
                ret=divAdjF(df, d[0], lbl=lbl, div=d[1])
                if inplace:
                    df[lbl]=ret
                else:
                    df['adj_'+lbl]=ret
    except:
        pass;
    
    try:
        temp= pd.read_csv(inst_dir+symbol+'.txt', header=None)  
        temp.columns=['Date','Factor']
        temp.Date=pd.to_datetime(temp.Date).dt.tz_localize('UTC').dt.tz_convert(str(df.index.tz))
        temp=temp.sort_values('Date',ascending=True)
        #[df[lbl],df[lblv]=splitAdjF(df, d[0], lbl=lbl, lblv=lblv, sf=d[1]) if inplace else df['adj_'+lbl],df['adj_'+lblv]=splitAdjF(df, d[0], lbl='adj_'+lbl, lblv=lblv, sf=d[1]) d in temp.values]
        for d in temp.values:
            if d[0]>=b1 and d[0]<=b2:
                if inplace:
                    ret=splitAdjF(df, d[0], lbl=lbl, lblv=lblv, sf=d[1])
                    df[lbl]=ret[0]
                    df[lblv]=ret[1]
                else:
                    ret=splitAdjF(df, d[0], lbl='adj_'+lbl, lblv=lblv, sf=d[1])
                    df['adj_'+lbl]=ret[0]
                    df['adj_'+lblv]=ret[1]
    except:
        pass;
    return df
  
    
    
#Installation of updates from backup
#w=[False,True,True,True,True,False,True,False]
#fr.retrieveStore(update=True,backup=False,dwld=False,what=w,bdate='20210523')
#fr.retrieveStore(update=True,backup=False,dwld=False,what=w,bdate='20210531')

#Update Download without storing the data
#Remember: the archive is incremented day by day until saturday
#w=[False,True,True,True,True,True,True,True]
#fr.retrieveStore(update=True,backup=True,dwld=True,what=w)

#Daily or weekly updates with download and data stores
#w=[True,True,True,True,True,True,True,True]
#fr.retrieveStore(update=True,backup=False,dwld=True,what=w)
