#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Norgate 
#
# Module comprising functions to download, store, and retrieve data from Norgate
#############################################################################################      
import os, sys

module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import numpy as np
import pandas as pd
import Plib.Keystore as kst
ks=kst.Keystore()
pd.options.mode.chained_assignment = None  # default='warn'
    
ORIGIN_TZ='UTC'

#from cachier import cachier
import datetime
#cache_exp=datetime.timedelta(days=252)
#@cachier(stale_after=cache_exp)


#################################################################
# Obtain List of contracts
#################################################################    
def getSymbols(t=''):
    
    save_dir=ks.hfuturedata_root
    efname=save_dir+'/desc.xlsx'
    
    if t=='cash':
        df1=pd.read_excel(efname,sheet_name=0, index_col=0)  
        df1=df1.reset_index()
        df1=df1[['Symbol','Description','Notes','Category']]
        df1=df1.sort_values('Symbol')
        df1=df1.reset_index()
        del df1['index']
        #df1.to_csv('norgate.csv')
        frdata=df1
        with pd.option_context('display.max_rows', None,
                               'display.max_columns', None,
                               'display.precision', 3,
                               ):
            display(frdata)
            
    else:
        df=pd.read_excel(efname,sheet_name=2, header=None)  
        df.columns=['desc']
        df1=df['desc'].str.split('(', 1, expand=True)
        df1.columns=['Symbol','Desc']
        df2=df1['Desc'].str.split(')', 1, expand=True)
        df2.columns=['Desc','Other']
        df3=df2['Other'].str.split(':', 1, expand=True)
        df3.columns=['cc','info']
        df=df1[['Symbol']].merge(df2[['Desc']],how='inner', left_index=True, right_index=True).merge(df3[['info']],how='inner', left_index=True, right_index=True)
        df.columns=['Symbol','Desc','Info']
        df=df.sort_values('Symbol')
        #df.to_csv('frdata.csv')
        frdata=df
        with pd.option_context('display.max_rows', None,
                               'display.max_columns', None,
                               'display.precision', 3,
                               ):
            display(frdata)
            
    return frdata
    
#################################################################
# Obtain Continuous Contracts data
#################################################################    
def getContFut(symbol,freq='D'):
    
    hfut_cfdir=ks.hfutsp_root

    ohlcv_dict = {
         'Open': 'first',
         'High': 'max',
         'Low': 'min',
         'Close': 'last',
         'Adjusted_close': 'last',
         'Volume': 'sum',
        'openInterest':'sum'
        }

    df= pd.read_csv(hfut_cfdir+'/'+symbol+'.csv',sep=',',
                    names=['Date','Open','High','Low','Close','Volume','openInterest'],
                   parse_dates=['Date'])
    df['Date'] = pd.to_datetime(df['Date'],utc=True)
    df['Date'] = df['Date'].astype('datetime64[ns]')
    #df['Date'] = df.Date.dt.tz_localize(tz)
    df.set_index('Date',inplace=True)
    df = df.sort_index(ascending=True)
    df['Adjusted_close']=df['Close']
    df=df[['Open','High','Low','Close','Adjusted_close','Volume','openInterest']]
    if freq not in ['d','D']:
        if freq=='W':
            df = df.resample('W-MON').agg(ohlcv_dict)
        else:
            df = df.resample(freq).agg(ohlcv_dict)
    return df.dropna()

#################################################################
# Obtain Cash Contracts data
#################################################################    
def getCashFut(symbol,freq='D'):
    
    hfut_cfdir=ks.hfutcash_root

    ohlcv_dict = {
         'Open': 'first',
         'High': 'max',
         'Low': 'min',
         'Close': 'last',
         'Adjusted_close': 'last',
         'Volume': 'sum',
        'openInterest':'sum'
        }

    df= pd.read_csv(hfut_cfdir+'/'+symbol+'.csv',sep=',',
                    names=['Date','Open','High','Low','Close','Volume','openInterest'],
                   parse_dates=['Date'])
    df['Date'] = pd.to_datetime(df['Date'],utc=True)
    df['Date'] = df['Date'].astype('datetime64[ns]')
    #df['Date'] = df.Date.dt.tz_localize(tz)
    df.set_index('Date',inplace=True)
    df = df.sort_index(ascending=True)
    df['Adjusted_close']=df['Close']
    df=df[['Open','High','Low','Close','Adjusted_close','Volume','openInterest']]
    if freq not in ['d','D']:
        if freq=='W':
            df = df.resample('W-MON').agg(ohlcv_dict)
        else:
            df = df.resample(freq).agg(ohlcv_dict)
    return df.dropna()

#################################################################
# Obtain Single Contract data
#################################################################    
def getSingleFut(symbol,freq='D'):
    from os import walk

    hfut_cfdir=ks.hsgfut_root

    ohlcv_dict = {
         'Open': 'first',
         'High': 'max',
         'Low': 'min',
         'Close': 'last',
         'Adjusted_close': 'last',
         'Volume': 'sum',
        'openInterest':'sum',
        'Month': 'last',
         'Year': 'last'
        }
    
    mypath=ks.hsgfut_root +'/' + symbol
    filenames = next(walk(mypath), (None, None, []))[2]  # [] if no file
    df= pd.read_csv(hfut_cfdir+'/AD/AD___1996U.csv',sep=',',
                    names=['Date','Open','High','Low','Close','Volume','openInterest'],
                   parse_dates=['Date'])
    df=df.head(1)
    df['Month']='H'
    df['Year']=17
    df = df.drop(0)
    
    for fname in filenames:
        temp= pd.read_csv(hfut_cfdir+'/'+symbol+'/'+fname,sep=',',
                    names=['Date','Open','High','Low','Close','Volume','openInterest'],
                   parse_dates=['Date'])
        try:
            my=fname.split('___')[1].split('.')[0]
        except:
            my=fname.split('__')[1].split('.')[0]
            pass;
        temp['Month']=my[4:5]
        temp['Year']=my[0:4]
        df=df.append(temp)
    
    df['Date'] = pd.to_datetime(df['Date'],utc=True)
    df['Date'] = df['Date'].astype('datetime64[ns]')
    #df['Date'] = df.Date.dt.tz_localize(tz)
    df.set_index('Date',inplace=True)
    df = df.sort_index(ascending=True)
    df['Adjusted_close']=df['Close']
    df=df[['Open','High','Low','Close','Adjusted_close','Volume','openInterest','Month','Year']]
    if freq not in ['d','D']:
        if freq=='W':
            df = df.resample('W-MON').agg(ohlcv_dict)
        else:
            df = df.resample(freq).agg(ohlcv_dict)
    df.sort_values(['Date','Year','Month'], inplace=True)
    return df.dropna()
