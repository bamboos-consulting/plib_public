#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Barchart 
#
# Module including functions to download data from Barchart
#############################################################################################      

#Common Libraries
import pandas as pd
import warnings
warnings.filterwarnings("once")
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

ORIGIN_TZ='UTC'

# Key to download data from Barchart
import Plib.Keystore as kst
ks=kst.Keystore()
api=ks.barchart_key
root=ks.barchart_root

##############################################
# Futures and Stock data download 
##############################################
def getBCData(symbol='SPY',st_date='20160801',frequency='daily',rtype='stock'):
    import suds.client as cli
    from suds.client import Client

    #minutes, nearbyMinutes, formTMinutes, daily, dailyNearest, dailyContinue, weekly, weeklyNearest, 
    #weeklyContinue, monthly, monthlyNearest, monthlyContinue, quarterly, quarterlyNearest, quarterlyContinue, 
    #yearly, yearlyNearest, yearlyContinue
    
    #Adjusted_close
    
    def filterBCStock(df):
        df.columns=['Symbol','Timestamp','Date','Open','High','Low','Close','Volume']
        #df.columns=['Close','High','Low','Open','Symbol','Timestamp','Date','Volume']
        df['Adjusted_close']=df['Close']
        df=df[['Open','High','Low','Close','Adjusted_close','Date','Volume']]
        df=df.set_index('Date')
        return df

    def filterBCFutures(df):
        df.columns=['Symbol','Timestamp','Date','Open','High','Low','Close','Volume','openInterest']
        df['Adjusted_close']=df['Close']
        df=df[['Open','High','Low','Close','Adjusted_close','Date','openInterest','Volume']]
        df=df.set_index('Date')
        return df
    
    ondemand = Client(root)
    result = ondemand.service.getHistory(api, symbol, frequency, st_date)
    
    data = [Client.dict(suds_object) for suds_object in result[0]]
    df = pd.DataFrame(data)
    
    if rtype=='stock':
        df=filterBCStock(df)
    else:
        df=filterBCFutures(df)    
    return df
