#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Orats 
#
# Module comprising functions to download, store, and retrieve data from Orats
#############################################################################################      
import datetime

#from cachier import cachier
#cache_exp=datetime.timedelta(days=365)
#@cachier(stale_after=cache_exp)

#Sleep to avoid overheating while storing data
SLEEP_TIME=2
ORIGIN_TZ='UTC'

##############################################
# Retrieve Orats Data from HDF storage
##############################################      
def getCallATM(ddate1,ddate2,symbol,u_price,threshold=0.08,greeks=False,dname2='SSD/p_orats',otype='CALL'):  
    import os, sys, os.path
    import pandas as pd
    import datetime
    from datetime import timedelta
    
    dirName2 = '//Volumes/'+ dname2
    
    date_1 = datetime.datetime.strptime(ddate1, "%Y-%m-%d")
    date_2 = datetime.datetime.strptime(ddate2, "%Y-%m-%d")
    delta = date_2 - date_1
    
    if greeks:
        options = pd.DataFrame(columns=['Date','expirationDate','dte','strike','volume','openInterest',
            'bid','ask','lastPrice','impliedVolatility','smvIV','iRate','und',
            'delta','gamma','theta','vega','rho','phi','driftlessTheta','type'])
    else:
        options = pd.DataFrame(columns=['Date','expirationDate','dte','strike','volume','openInterest',
            'bid','ask','lastPrice','impliedVolatility','smvIV','iRate','und','type'])    
    for i in range(0,delta.days):
        i_date=date_1 + datetime.timedelta(days=i)
        try:
            df=getOptionsOrats(i_date.strftime('%Y-%m-%d'),symbol,greeks,dname2)
            df=df[(df['type']==otype) & 
                (df['strike']>=df['strike']*(1-u_price*threshold)) & 
                (df['strike']<=df['strike']*(1+u_price*threshold))]
            df['Date']=i_date.strftime('%Y-%m-%d')
            df['Date'] = pd.to_datetime(df['Date'])
            options=options.append(df, ignore_index=True)
        except:
            x=0
    options.set_index('Date')        
    return options

#@cachier(stale_after=cache_exp)
def getOptionsOrats(ddate,symbol,greeks=False,dname='Datafarm/options',ddate2='',date_cond=''):#dname='SSD/p_orats'
    import os, sys, os.path
    import pandas as pd
    pd.options.mode.chained_assignment = None  # default='warn'
    
    dirName = '//Volumes/'+ dname
    
    if ddate2=='':
        sdate1=str(ddate.replace('-',''))
        sdate=str(sdate1[:-2])
        fname='ORATS_SMV_Strikes_'+sdate+'.hdf'
        hdffn=os.path.join(dirName, fname)
        if date_cond=='': date_cond='index=="'+ddate+'"'    
        store = pd.HDFStore(hdffn)
        odata=store.select(symbol,where=date_cond)
        store.close()
    else:
        sdate=str(ddate.replace('-',''))
        edate=str(ddate2.replace('-',''))
        
        dates=pd.date_range(ddate,ddate2)
        dates=dates.to_frame()
        dates.columns=['ddate']
        dates=dates.ddate.dt.strftime('%Y-%m').unique()
          
        if greeks:
            odata = pd.DataFrame(columns=['Date','expirationDate','dte','strike','volume','openInterest',
                'bid','ask','lastPrice','impliedVolatility','smvIV','iRate','und','delta','gamma','theta',
                'vega','rho','phi','driftlessTheta','type'])
        else:
            odata = pd.DataFrame(columns=['Date','expirationDate','dte','strike','volume','openInterest',
                'bid','ask','lastPrice','impliedVolatility','smvIV','iRate','und','type'])
        for hf in dates:
            hf=hf.replace('-','')
            fname='ORATS_SMV_Strikes_'+hf+'.hdf'
            hdffn=os.path.join(dirName, fname)
            if date_cond=='': date_cond='index>="'+ddate+'" & index<="'+ddate2+'"'  
            store = pd.HDFStore(hdffn)
            options=store.select(symbol,where=date_cond)
            odata=odata.append(options) #, ignore_index=True
            store.close()
            
    #['ticker','stkPx','expirDate','yte','strike','cVolu','cOi','pVolu','pOi','cBidPx','cValue','cAskPx','pBidPx','pValue',
    #'pAskPx','cBidIv','cMidIv','cAskIv','smoothSmvVol','pBidIv','pMidIv','pAskIv','iRate','divRate','residualRateData',
    #'delta','gamma','theta','vega','rho','phi','driftlessTheta','extVol','extCTheo','extPTheo','spot_px','trade_date']
    
    if greeks:
        df1=odata[['expirDate','yte','strike','cVolu','cOi','cBidPx','cAskPx','cValue','cMidIv',
            'smoothSmvVol','iRate','stkPx','delta','gamma','theta','vega','rho','phi','driftlessTheta']]
        df1.columns=['expirationDate','dte','strike','volume','openInterest','bid','ask','lastPrice',
            'impliedVolatility','smvIV','iRate','und','delta','gamma','theta','vega','rho','phi','driftlessTheta']
    else:
        df1=odata[['expirDate','yte','strike','cVolu','cOi','cBidPx',
            'cAskPx','cValue','cMidIv','smoothSmvVol','iRate','stkPx']]
        df1.columns=['expirationDate','dte','strike','volume','openInterest','bid','ask',
            'lastPrice','impliedVolatility','smvIV','iRate','und']
    df1['expirationDate'] = pd.to_datetime(df1.expirationDate)
    df1['type']='CALL'
    
    if greeks:
        df2=odata[['expirDate','yte','strike','pVolu','pOi','pBidPx','pAskPx','pValue',
            'pMidIv','smoothSmvVol','iRate','stkPx','delta','gamma','theta','vega','rho','phi','driftlessTheta']]
        df2.columns=['expirationDate','dte','strike','volume','openInterest','bid','ask','lastPrice',
            'impliedVolatility','smvIV','iRate','und','delta','gamma','theta','vega','rho','phi','driftlessTheta']
    else:
        df2=odata[['expirDate','yte','strike','cVolu','cOi','cBidPx',
            'cAskPx','cValue','cMidIv','smoothSmvVol','iRate','stkPx']]
        df2.columns=['expirationDate','dte','strike','volume','openInterest','bid','ask',
            'lastPrice','impliedVolatility','smvIV','iRate','und']
    df2['expirationDate'] = pd.to_datetime(df2.expirationDate)
    df2['type']='PUT'
    
    df3=df1.append(df2)
    return df3
      

##############################################
# Helper function for update
##############################################      
def testRange():
    import datetime
    import pandas as pd

    log=print_log('//Volumes/Datafarm/options/orats_log.hdf',log_name='orats').tail(1)
    last=(log.T).columns[0][0]

    from_date=datetime.datetime.strptime(last, '%Y%m%d') #datetime.date.today()
    to_date=datetime.date.today()

    yeard=int(last[:4])
    dd1=from_date.strftime("%Y-%m-%d")
    dd2=to_date.strftime("%Y-%m-%d")
    #Test date ranges before launching
    myrange=pd.date_range(dd1,dd2)
    print(myrange)
    n_files=len(myrange)
    print('N: ',n_files,' Year: ', yeard)
    return n_files, yeard,myrange
    
##############################################
# Connect to Orats Ftp and  download Data
##############################################      
def downloadOrats(basedir=2007,last=1,dname1='//Volumes/SSD/orats',ftpa='',uida='',pwda='',dira='',nattempts=3,ziptest=True):
    import os, sys, os.path
    import zipfile
    from ftplib import FTP

    ftp = FTP(ftpa)  
    ftp.login(uida,pwda)    
    ftp.cwd(dira)         
    ftp.retrlines('LIST') 
    ftp.retrlines('LIST') 
    ls = ftp.nlst()
    #ls.reverse()
    
    for fn in ls:
        if int(fn)>= basedir:
            ftp.cwd(fn)
            ftp.retrlines('LIST') 
            lsy = ftp.nlst()
            #lsy.reverse()
            count = len(lsy)
            curr = 0
            print("found {} files".format(count))
            for fny in lsy:
                curr += 1
                attempts=nattempts
                skip=False
                print('Processing file {} ... {} of {} ...'.format(fny, curr, count))
                if os.path.isfile(dname1 + "/" +fn+'/'+ fny.lstrip("/")):
                    if ziptest:
                        try:
                            zip_file = zipfile.ZipFile(dname1 + "/" +fn+'/'+fny.lstrip("/"))
                            ret = zip_file.testzip()
                            if ret is not None:
                                os.remove(dname1 + "/" +fn+'/'+ fny.lstrip("/"))
                            else:
                                skip=True
                        except:
                            print("Exception in reading the file.....")
                            os.remove(dname1 + "/" +fn+'/'+ fny.lstrip("/"))
                        if (curr %5==0): ftp.voidcmd('NOOP')
                    else:
                        if (curr %10==0): ftp.voidcmd('NOOP')
                while attempts >0 and not skip:
                    ftp.retrbinary('RETR ' + fny, open(dname1 + "/" +fn+'/'+ fny.lstrip("/"), 'wb').write)
                    try:
                        zip_file = zipfile.ZipFile(dname1 + "/" +fn+'/'+fny.lstrip("/"))
                        ret = zip_file.testzip()
                        if ret is not None:
                            os.remove(dname1 + "/" +fn+'/'+ fny.lstrip("/"))
                            attempts=attempts-1
                        else:
                            attempts=0
                            last=last-1
                    except:
                        print("Exception in reading the file.....")
                        os.remove(dname1 + "/" +fn+'/'+ fny.lstrip("/"))
                        attempts=attempts-1    
                if last<=0:
                    break
            ftp.cwd("../")       
        if last<=0:
            break
    ftp.quit()
    print("download complete.")

##############################################
# Extract Data for Processing
##############################################   
def extractOrats(dir1,dir2,fnz,fnc):
    from zipfile import ZipFile
    from zipfile import BadZipfile
    import os, sys, os.path

    fname_zip=dir1+'/'+fnz
    fname_csv=dir2+'/'+fnc
    
    ret=0
    print('Unzipping File ' + fname_zip)
    try:
        with ZipFile(fname_zip, 'r') as zipObj:
            if not os.path.isfile(fname_csv):
                zipObj.extractall(dir2) 
    except BadZipfile:
        print('Error while unzipping ' + fname_zip)
        ret=-1
    return ret

##############################################
# Manage the Log for Download and Store
##############################################   
def store_log(df,orats_log,log_table='orats',delete=False):
    import pandas as pd
    import os
    
    print('Logging File ' + orats_log)
    store = pd.HDFStore(orats_log)
    store.append(log_table, df, format='table',data_columns=['last_update_to','fname'])
    store.close()
    if delete:
        os.remove(orats_log)

def print_log(orats_log,log_name='orats'):
    import os, sys, os.path
    import pandas as pd
    pd.options.mode.chained_assignment = None  # default='warn'
    
    df=pd.DataFrame()
    if os.path.isfile(orats_log):
        store = pd.HDFStore(orats_log)
        df=store.get(log_name)
        store.close()
    return df.groupby(['last_update_to','fname']).sum()

def check_log(orats_log,bdate,orats_hdf,log_name='orats'):
    import os, sys, os.path
    import pandas as pd
    pd.options.mode.chained_assignment = None  # default='warn'
    
    log_cond='last_update_to="'+bdate+'" & fname="'+orats_hdf+'"'    

    df=pd.DataFrame()
    if os.path.isfile(orats_log):
        store = pd.HDFStore(orats_log)
        df=store.select(log_name,where=log_cond)
        store.close()
    if len(df)>0:
        ret=str(df.finished.iloc[-1])
    else:
        ret='-1'
    return ret     

##############################################
# Store data into HDF5 Format
##############################################         
def storeOrats(bdate,dir1,dir2,fnc,fnh,delete_csv=False,replace=False):
    import pandas as pd
    import os
    import time
    import datetime as mdt
    
    orats_csv=dir1+'/'+fnc
    orats_hdf=dir2+'/'+fnh
    
    column_names=['ticker','stkPx','expirDate','yte','strike','cVolu','cOi','pVolu','pOi','cBidPx','cValue','cAskPx','pBidPx','pValue',
    'pAskPx','cBidIv','cMidIv','cAskIv','smoothSmvVol','pBidIv','pMidIv','pAskIv','iRate','divRate','residualRateData','delta',
    'gamma','theta','vega','rho','phi','driftlessTheta','extVol','extCTheo','extPTheo','spot_px','trade_date']
    
    df = pd.read_csv(orats_csv,sep=',')
    #'2021-05-28': added opra columns
    for c in df.columns:
        if c not in column_names:
            del df[c]        
            
    df['date']=pd.to_datetime(bdate)
    df['expirDate'] = pd.to_datetime(df.expirDate, errors='coerce')
    df['trade_date'] = pd.to_datetime(df.trade_date, errors='coerce')
    df=df.set_index('date')
    
    df['stkPx']=df['stkPx'].astype('float64')
    df['yte']=df['yte'].astype('float64')
    df['strike']=df['strike'].astype('float64')
    df['cVolu']=df['cVolu'].astype('int64')
    df['cOi']=df['cOi'].astype('int64')
    df['pVolu']=df['pVolu'].astype('int64')
    df['pOi']=df['pOi'].astype('int64')
    df['cBidPx']=df['cBidPx'].astype('float64')
    df['cValue']=df['cValue'].astype('float64')
    df['cAskPx']=df['cAskPx'].astype('float64')
    df['pBidPx']=df['pBidPx'].astype('float64')
    df['pValue']=df['pValue'].astype('float64')
    df['pAskPx']=df['pAskPx'].astype('float64')
    df['cBidIv']=df['cBidIv'].astype('float64')
    df['cMidIv']=df['cMidIv'].astype('float64')
    df['cAskIv']=df['cAskIv'].astype('float64')
    df['smoothSmvVol']=df['smoothSmvVol'].astype('float64')
    df['pBidIv']=df['pBidIv'].astype('float64')
    df['pMidIv']=df['pMidIv'].astype('float64')
    df['pAskIv']=df['pAskIv'].astype('float64')
    df['iRate']=df['iRate'].astype('float64')
    df['divRate']=df['divRate'].astype('int64')  ###################### -> next year will turn it to float64 ;-)
    df['residualRateData']=df['residualRateData'].astype('float64')
    df['delta']=df['delta'].astype('float64')
    df['gamma']=df['gamma'].astype('float64')
    df['vega']=df['vega'].astype('float64')
    df['rho']=df['rho'].astype('float64')
    df['phi']=df['phi'].astype('float64')
    df['driftlessTheta']=df['driftlessTheta'].astype('float64')
    df['extVol']=df['extVol'].astype('float64')
    df['extCTheo']=df['extCTheo'].astype('float64')
    df['extPTheo']=df['extPTheo'].astype('float64')
    df['spot_px']=df['spot_px'].astype('float64')
    
    print('Storing '+orats_csv+' into HDF5 ' + orats_hdf)
    if replace:
        os.remove(orats_hdf)
    store = pd.HDFStore(orats_hdf)
    for sym in df.ticker.unique():
        store.append(sym, df[df.ticker==sym], format='table',complevel=6, complib='lzo',data_columns=['expirDate','strike','trade_date'])
    store.close()
    #time.sleep(SLEEP_TIME)
    if delete_csv:
        os.remove(orats_csv)
        
##############################################
# Helper Functionfor Periodical retrieval
##############################################  
def retrieveStore(lastDays=21,base_year=2021,dir_zip='/test',dir_csv='/csv',dir_hdf='/store',delete_csv=True,dwnl=False,dd1='',dd2=''):
    import os
    import pandas as pd
    from pandas.tseries.offsets import CustomBusinessDay
    import Plib.Keystore as kst
    ks=kst.Keystore()
    
    if dwnl: downloadOrats(base_year,lastDays,dir_zip,ftpa=ks.orats_ftp,uida=ks.orats_uid,pwda=ks.orats_pwd,dira=ks.orats_dir2)
    
    dir_zip=dir_zip +'/'+str(base_year)
    
    us_bd = CustomBusinessDay()
    if dd1=='' and dd2=='':
        idx=pd.date_range(str(base_year)+'-01-01', str(base_year)+'-12-31', freq=us_bd)
    else:
        idx=pd.date_range(dd1, dd2, freq=us_bd)
    dlog=pd.DataFrame(index=idx).reset_index()
    dlog.columns=['Date']
    orats_log=dir_hdf+'/orats_log.hdf'
    
    logfile=open(dir_hdf+'/elab.log', "a+")
    for i in range(0,len(dlog)):
        bdate=str(dlog.Date.iloc[i].date())
        bdate=str(bdate.replace('-',''))
        orats_zip='ORATS_SMV_Strikes_'+bdate+'.zip'
        orats_csv='ORATS_SMV_Strikes_'+bdate+'.csv'
        orats_hdf='ORATS_SMV_Strikes_'+str(bdate[:-2])+'.hdf'
    
        print('Processing data... '+bdate)
        if os.path.isfile(dir_zip+'/'+orats_zip):
            check=check_log(orats_log,bdate,orats_hdf)
            if check=='-1':
                #not present
                ret=extractOrats(dir_zip,dir_csv,orats_zip,orats_csv)
                if ret==0:
                    store_log(pd.DataFrame([bdate,orats_hdf,'0'],['last_update_to','fname','finished']).T,orats_log)
                    storeOrats(bdate,dir_csv,dir_hdf,orats_csv,orats_hdf,delete_csv)
                    store_log(pd.DataFrame([bdate,orats_hdf,'1'],['last_update_to','fname','finished']).T,orats_log)
                else:
                    print('Check downloaded file for errors....')
                    logfile.write('While processing data ' + bdate + ' an error while downloading '+orats_zip+' occurred.\n')
            elif check=='0':
                #unfinished
                print('Error while processing data.... ' + str(bdate)+' Delete existing data/log and retry...')
                logfile.write('While processing data ' + bdate + ' an error with the log occurred. Check the data in store.\n')
            elif check=='1':
                #already present
                print('Data already present.... ' + str(bdate))
                logfile.write('While processing data ' + bdate + ' the data was found already present. Check the data in store.\n')
        else:
            print('File not found, ',dir_zip+'/'+orats_zip)        
            logfile.write('While processing data ' + bdate + ' the file '+dir_zip+'/'+orats_zip+' was not found.\n')    
    logfile.close() 
    
    