#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# PReader 
#
# Module including functions to download data from json sources
#############################################################################################      
import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path+"/")

# Commmon Libraries
#import warnings
#warnings.filterwarnings("once")
import pandas as pd
import json 
import requests
import datetime
from datetime import timezone

##############################################
# Manage Request caching system
##############################################    
import requests_cache as rq
from datetime import timedelta
from requests_cache.backends import SQLiteCache
from requests_cache import CachedSession
session = CachedSession(
    SQLiteCache(module_path+'/Plib/__pycache__/'+'api_cache', timeout=30),
    use_cache_dir=True,                # Save files in the default user cache dir
    cache_control=True,                # Use Cache-Control headers for expiration, if available
    expire_after=timedelta(days=1),    # Otherwise expire responses after one day
    allowable_methods=['GET', 'POST'], # Cache POST requests to avoid sending the same data twice
    allowable_codes=[200, 400],        # Cache 400 responses as a solemn reminder of your failures
    #ignored_parameters=['api_key'],    # Don't match this param or save it in the cache
    match_headers=True,                # Match all request headers
    stale_if_error=True,               # In case of request errors, use stale cache data if possible
)

def removeCache():
    return rq.patcher.uninstall_cache()
    
def isCached(remove=False):
    res=rq.patcher.is_installed()
    if remove:
        rq.patcher.disabled()
        return False
    else:
        return res

def renableCache():
    if not isCached():
        return rq.patcher.install_cache('api_cache')
    else:
        return rq.patcher.enabled('api_cache')
     
     
        try:
            response = requests.post(_url, files={'file': some_file})
            response.raise_for_status()
        except requests.exceptions.HTTPError as errh:
            return "An Http Error occurred:" + repr(errh)
        except requests.exceptions.ConnectionError as errc:
            return "An Error Connecting to the API occurred:" + repr(errc)
        except requests.exceptions.Timeout as errt:
            return "A Timeout Error occurred:" + repr(errt)
        except requests.exceptions.RequestException as err:
            return "An Unknown Error occurred" + repr(err)     
     
        
##############################################
# Basic api to retrieve data
##############################################    
def requestJson2DF(url,limit=0,offset=0):
    from pandas.io.json import json_normalize
    
    if limit >0:
        pagination = {
        'limit': limit, 
        'offset': offset
        }
    headers = {
        'Content-Type': 'application/json'
    }
    if limit >0:
        api_result = requests.get(url, headers=headers,params=pagination)
    else:
        api_result = requests.get(url, headers=headers)
    api_response = api_result.json()
    response_data = json.loads(json.dumps(api_response))
    df = json_normalize(response_data)
    return df

def requestJson2RespData(url,limit=0,offset=0):
    if limit >0:
        pagination = {
        'limit': limit, 
        'offset': offset
        }
    headers = {
        'Content-Type': 'application/json'
    }
    
    if limit >0:
        api_result = requests.get(url, headers=headers,params=pagination)
    else:
        api_result = requests.get(url, headers=headers)    
    api_response = api_result.json()
    response_data = json.loads(json.dumps(api_response))
    return response_data

def requestsText(url, headers, parsedt=False, cols=[]):
    from io import StringIO
    from pandas import read_csv
    
    try:
        req = requests.get(url, headers=headers)
        if int(req.text.find('404 Symbol Not Found')) + int(req.text.find('405 Data Not Found')) > 0:
            return pd.DataFrame()
    except requests.exceptions.HTTPError as errh:
        print("An Http Error occurred")
        return pd.DataFrame()
    except requests.exceptions.ConnectionError as errc:
        print("An Error Connecting to the API occurred")
        return pd.DataFrame()
    except requests.exceptions.Timeout as errt:
        print("A Timeout Error occurred")
        return pd.DataFrame()
    except requests.exceptions.RequestException as err:
        print("An Unknown Error occurred")
        return pd.DataFrame()
    except:
        print(req.raise_for_status())
        return pd.DataFrame()
        
    if not parsedt:
        df = pd.read_csv(StringIO(req.text), header=None)
    else:
        df = pd.read_csv(StringIO(req.text), header=None, parse_dates=[['Date','Time']], dayfirst=True, names=cols)
    
    return df
    