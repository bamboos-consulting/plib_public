#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Nasdaq 
#
# Module comprising functions to download, store, and retrieve data from Nasdaq public apis
#############################################################################################      
import os, sys

module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

ORIGIN_TZ='UTC'

import warnings
warnings.filterwarnings("ignore")
from finance_calendars import finance_calendars as fc
from datetime import datetime, date
import pandas as pd    
pd.options.mode.chained_assignment = None  # default='warn'
    


#################################################################
# Obtain List of today IPOs
#################################################################    
def getIPOs():
    import datetime
    
    ipos = fc.get_priced_ipos_this_month()
    df=ipos.reset_index()
    del df['dealStatus']
    today = datetime.datetime.now().date().strftime("%Y-%m-%d")
    df=df[df['pricedDate']>=today]
    return df

#################################################################
# Obtain List of today dividends
#################################################################    
def getDividends():
    dividends = fc.get_dividends_today()
    df=dividends[['companyName','payment_Date','dividend_Rate','indicated_Annual_Dividend']]
    issues=len(df)    
    avg_rate=df[['dividend_Rate','indicated_Annual_Dividend']].mean()[0]
    avg_usd=df[['dividend_Rate','indicated_Annual_Dividend']].mean()[1]
    return df,(issues,avg_rate,avg_usd)

#################################################################
# Obtain List of today Earnings
#################################################################    
def getEarnings():
    earnings =fc.get_earnings_today()

    df=earnings[['time','name','marketCap','fiscalQuarterEnding','epsForecast','lastYearEPS']]

    df["marketCap"] = df["marketCap"].replace("[$,]", "", regex=True)#.astype(float)
    df["epsForecast"] = df["epsForecast"].replace("[(,]", "-", regex=True)#.astype(float)
    df["epsForecast"] = df["epsForecast"].replace("[),]", "", regex=True)#.astype(float)
    df["epsForecast"] = df["epsForecast"].replace("[$,]", "", regex=True)#.astype(float)
    df["lastYearEPS"] = df["lastYearEPS"].replace("[(,]", "-", regex=True)#.astype(float)
    df["lastYearEPS"] = df["lastYearEPS"].replace("[),]", "", regex=True)#.astype(float)
    df["lastYearEPS"] = df["lastYearEPS"].replace("[$,]", "", regex=True)#.astype(float)

    df['marketCap'] = pd.to_numeric(df['marketCap'],errors='coerce')
    df['epsForecast'] = pd.to_numeric(df['epsForecast'],errors='coerce')
    df['lastYearEPS'] = pd.to_numeric(df['lastYearEPS'],errors='coerce')
    
    df['EPS_Delta'] = round((df['epsForecast']-df['lastYearEPS'])/df['lastYearEPS'],2)
    df=df.dropna()
    df['Weight']=round(df['marketCap']/df['marketCap'].sum(),6)
    df['EPS_CapWeighted']=df['Weight']*df['EPS_Delta']
    
    return df, round(df['EPS_CapWeighted'].mean()*100,6)
