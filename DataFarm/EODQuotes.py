#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# EOD Quotes 
#
# Module including functions to download data from EOD Stock quotes
#############################################################################################      

# Commmon Libraries
#import warnings
#warnings.filterwarnings("once")
import pandas as pd
import datetime
import Plib.DataFarm.PReader as prd
import Plib.Utils.Tools as tl

ORIGIN_TZ='UTC'

#Key to access data
import Plib.Keystore as kst
ks=kst.Keystore()
root=ks.eod_root
key=ks.eod_key

##############################################
# Cache Management
##############################################
def install_cache():
    prd.renableCache()

def suspend_cache():
    prd.isCached(remove=True)
    
def remove_cache():
    prd.removeCache()
    
##############################################
# Wrapper to substitute IEX data
##############################################
def get_eod_data(symbol, dt_start, dt_end):
    return getData(symbol,dt_start, dt_end)

##############################################
# Wrapper to substitute IEX data
##############################################
def get_eod_dataFX(cur1,cur2,dt_start, dt_end):
    symbol=cur1+cur2+'.FOREX'
    return getData(symbol,dt_start, dt_end)
    
##############################################
# Returns search results
##############################################    
################################################
# Dictionary for symbols across data providers
################################################    
def gs(t):
    if t=='EWW': t='mexico'
    if t=='IWC': t='micro cap'
    if t=='SPHB': t='high beta'
    if t=='SPLV': t='Low Volatility'
    if t=='BNDD': t='Deflation'
    if t=='BDIY': t='Baltic'
    if t=='BPNI': t='Panamax'
    if t=='BKLN': t='Senior Loan'
    if t=='SX6R': t='STOXX Europe utilities'
    if t=='FSTUM9': t='STOXX utilities'
    if t=='FSTVM9': t='STOXX travel'
    if t=='FSTTM9': t='STOXX telecommunications'
    if t=='FSTYM9': t='STOXX technology'
    if t=='FSTRM9': t='STOXX retail'
    if t=='FSTZM9': t='STOXX personal'
    if t=='FSTMM9': t='STOXX media'
    if t=='FSTIM9': t='STOXX insurance'
    if t=='FSTGM9': t='STOXX industrial'
    if t=='FSTHM9': t='STOXX healthcare'
    if t=='FSTOM9': t='STOXX food'
    if t=='FSTFM9': t='STOXX financial'
    if t=='FSTEM9': t='600 Energy'
    if t=='FSTNM9': t='STOXX construction'
    if t=='FSTCM9': t='STOXX chemicals'
    if t=='FSTSM9': t='STOXX 600 Resources'
    if t=='FSTBM9': t='STOXX banks'
    if t=='FSTAM9': t='STOXX automobiles'
    if t=='BCOMPL': t='platinum'
    if t=='BCOMPR': t='precious metals'
    if t=='BCOMGR': t='grains'
    if t=='BCOMPB': t='lead'
    if t=='BCOMPE': t='petroleum'
    if t=='BCOMSO': t='softs'
    if t=='BCOMSN': t='tin'
    return t

def search(search_string, exchange=''):
    from pandas.io.json import json_normalize
    
    install_cache()
    endpoint='search/'
    url=root+endpoint+search_string+'?api_token='+key          
    resp = prd.requestJson2RespData(url)
    df=pd.DataFrame()
    if len(resp)>1:
        df=pd.DataFrame(resp)
    if exchange !='': df=df[df['Exchange']==exchange]
    df.columns=['Symbol','Exchange','Description','Type','Country','Currency','ISIN','previousClose','previousCloseDate']
    return df
    
##############################################
# Returns exchanges and tickers
##############################################    
def getExchanges():
    from pandas.io.json import json_normalize
    
    install_cache()
    endpoint='exchanges-list/'
    url=root+endpoint+'?api_token='+key+'&fmt=json'       
    resp = prd.requestJson2RespData(url)
    df=pd.DataFrame()
    if len(resp)>1:
        df=pd.DataFrame(resp)
    return df
    
def getExchangesTickers(ename='US', delisted = True):
    from pandas.io.json import json_normalize
    
    install_cache()
    endpoint='exchange-symbol-list/'
    if delisted:
        url=root+endpoint+ename+'?api_token='+key+'&fmt=json&delisted=1'
    else:
        url=root+endpoint+ename+'?api_token='+key+'&fmt=json'       
    resp = prd.requestJson2RespData(url)
    df=pd.DataFrame()
    if len(resp)>1:
        df=pd.DataFrame(resp)
    return df
    
def getExchangesTradingHours(ecode='US'):
    from pandas.io.json import json_normalize
    
    install_cache()
    endpoint='exchange-details/'
    url=root+endpoint+ecode+'?api_token='+key+'&fmt=json'       
    resp = prd.requestJson2RespData(url)
    df=pd.DataFrame()
    if len(resp)>1:
        df=pd.DataFrame(resp)
    return df
    
##############################################
# End of Day Historical data
##############################################    
def getData(symbol,sd='2017-01-05',ed='2017-02-10',tz='America/New_York'):
    from pandas.io.json import json_normalize
    
    endpoint='eod/'
    url=root+endpoint+symbol+'?from='+sd+'&to='+ed+'&period=d&fmt=json&api_token='+key          
    resp = prd.requestJson2RespData(url)
    df=pd.DataFrame()
    if len(resp)>1:
        df=pd.DataFrame(resp)
        df['date'] = pd.to_datetime(df['date'])#,utc=True)
        df['date'] = df['date'].astype('datetime64[ns]')
        #df['date'] = df.date.dt.tz_localize('UTC').dt.tz_convert(tz)
        df.columns=tl.caps(df.columns)
        df=df.set_index('Date')
        df = df.asfreq('d')
    return df.dropna()
    
##############################################
# Real Time Data
##############################################    
def getRTData(symbol,tz='America/New_York'):
    from pandas.io.json import json_normalize

    suspend_cache()    
    if symbol.find('.INDX')>0:
        df=getRTData('USD.FOREX')
        for v in df.columns:
            if v=='Symbol': 
                df[v]=symbol.split('.INDX')[0]
            else:
                df[v]=0.
    else:
        endpoint='real-time/'
        url=root+endpoint+symbol+'?fmt=json&api_token='+key        
        resp = prd.requestJson2RespData(url)
        df=pd.DataFrame()
        if len(resp)>1:
            df=pd.DataFrame.from_dict(resp,orient='index').T
            df['Date']=tl.toIsoDt(df.timestamp,tz=tz)
            df=df.set_index('Date')
            del df['timestamp']
            del df['gmtoffset']
            df.columns=tl.caps(df.columns)
            df.rename(columns = {'Code':'Symbol','Previousclose':'Prev_close'}, inplace = True)
    return df

def getRTData2(symbols=[],tz='America/New_York'):
    from pandas.io.json import json_normalize
    
    suspend_cache()
    symbol=symbols[0]
    slist=str(symbols[1:]).replace('[','').replace(']','').replace("'",'')
    endpoint='real-time/'
    url=root+endpoint+symbol+'?fmt=json&api_token='+key+'&fmt=json&s='+slist        
    resp = prd.requestJson2RespData(url)
    df=pd.DataFrame()
    if len(resp)>1:
        df=pd.DataFrame(resp)
        df['Date']=df['timestamp'].apply(lambda row: tl.toIsoDt(row,tz=tz))
        df=df.set_index('Date')
        del df['timestamp']
        del df['gmtoffset']
        df.columns=tl.caps(df.columns)
        df.rename(columns = {'Code':'Symbol','Previousclose':'Prev_close'}, inplace = True)
    return df

