#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Compustat 
#
# Module comprising functions to download, store, and retrieve data from COmpustat exports
#############################################################################################      
import os, sys

module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import numpy as np
import pandas as pd
import Plib.Keystore as kst
ks=kst.Keystore()
pd.options.mode.chained_assignment = None  # default='warn'
    
#from cachier import cachier
import datetime
#cache_exp=datetime.timedelta(days=252)
#@cachier(stale_after=cache_exp)


#################################################################
# Obtain historical currency pairs
#################################################################    
def getForexHistorical():
    #data.columns
    #Index(['USDAUD', 'AUDUSD', 'BRLUSD', 'CADUSD', 'CNYUSD', 'DKKUSD', 'HKDUSD',
    #       'INRUSD', 'JPYUSD', 'KRWUSD', 'MYRUSD', 'MXNUSD', 'USDNZD', 'NZDUSD',
    #       'NOKUSD', 'SGDUSD', 'ZARUSD', 'LKRUSD', 'SEKUSD', 'CHFUSD', 'TWDUSD',
    #       'THBUSD', 'USDGBP', 'GBPUSD', 'VESUSD', 'exauus', 'exbeus', 'exfnus',
    #       'exfrus', 'exgeus', 'exgrus', 'exusir', 'exirus', 'exitus', 'exneus',
    #       'expous', 'exspus', 'exusec', 'execus', 'USDEUR', 'EURUSD', 'twexb',
    #       'twexm', 'twexo', 'indexgx'],

    #australia, brazil, canada, china, denmark, hong kong, india, japan, south corea, malaysia, mexico, 
    #new zealand, norway, singapore, sf, srilanka, sweden, switzerland, taiwan, thailand, uk, venezuela,
    #austria, belgium, france, germany,greece,ireland, italy,netherland,poland,spain,ec,eu

    #TWEXO (other currencies index) is a weighted average of the foreign exchange value of the U.S. dollar 
    #against a subset of the broad index currencies that do not circulate widely outside the country of issue.
    #Countries whose currencies are included in the other important trading partners index are Mexico, China, 
    #Taiwan, Korea, Singapore, Hong Kong, Malaysia, Brazil, Thailand, Philippines, Indonesia, India, Israel, 
    #Saudi Arabia, Russia, Argentina, Venezuela, Chile and Colombia.

    #TWEXB (broad currencies index) is a weighted average of the foreign exchange value of the U.S. dollar 
    #against the currencies of a broad group of major U.S. trading partners. Broad currency index includes 
    #the Euro Area, Canada, Japan, Mexico, China, United Kingdom, Taiwan, Korea, Singapore, Hong Kong, 
    #Malaysia, Brazil, Switzerland, Thailand, Philippines, Australia, Indonesia, India, Israel, 
    #Saudi Arabia, Russia, Sweden, Argentina, Venezuela, Chile and Colombia.

    #TWEXM (major currencies index) is a weighted average of the foreign exchange values of the U.S. dollar 
    #against a subset of currencies in the broad index that circulate widely outside the country of issue.
    #Euro Area, Canada, Japan, United Kingdom, Switzerland, Australia, and Sweden
    
    mdir=ks.hcurr_root
    fname=ks.hcurr_file
    data = pd.read_csv(mdir + '/'+ fname)
    data['Date']=pd.to_datetime(data['date'])
    del data['date']
    data=data.set_index('Date')
    cols=['USDAUD', 'AUDUSD', 'BRLUSD', 'CADUSD', 'CNYUSD', 'DKKUSD', 'HKDUSD',
           'INRUSD', 'JPYUSD', 'KRWUSD', 'MYRUSD', 'MXNUSD', 'USDNZD', 'NZDUSD',
           'NOKUSD', 'SGDUSD', 'ZARUSD', 'LKRUSD', 'SEKUSD', 'CHFUSD', 'TWDUSD',
           'THBUSD', 'USDGBP', 'GBPUSD', 'VESUSD', 'exauus', 'exbeus', 'exfnus',
           'exfrus', 'exgeus', 'exgrus', 'exusir', 'exirus', 'exitus', 'exneus',
           'expous', 'exspus', 'exusec', 'execus', 'USDEUR', 'EURUSD', 'twexb',
           'twexm', 'twexo', 'indexgx']
    data.columns=cols
    return data