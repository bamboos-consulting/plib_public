#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Kibot  
#
# Module including functions to download data from EOD Stock quotes
#############################################################################################      

import sys
import os

module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

# Commmon Libraries
#import warnings
#warnings.filterwarnings("once")
import pandas as pd
import numpy as np
import datetime
import Plib.DataFarm.PReader as prd
import Plib.Utils.Tools as tl

ORIGIN_TZ='EST'
LOGGEDIN=0

#Key to access data
import Plib.Keystore as kst
ks=kst.Keystore()
root=ks.kibot_root
key=ks.kibot_key
kpath=module_path.replace('/Plib','') + '/Plib/DataFarm/other'

headers = {'User-Agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:66.0) Gecko/20100101 Firefox/66.0",
   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
   'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.7',
   'Accept-Encoding': 'gzip, deflate',
   'Accept-Language': 'en-us,en;q=0.5',
   'Connection': 'keep-alive'}


##############################################
# Symbol Categories
##############################################
def getSectors():
    kibot=pd.read_csv(kpath + '/kibot_stocks.csv')
    return list(kibot[['Sector']].dropna()['Sector'].unique())

def getIndustries():
    kibot=pd.read_csv(kpath + '/kibot_stocks.csv')
    return list( kibot[['Industry']].dropna()['Industry'].unique() )

def getTickers(tname='stock', ext=False):
    if tname=='stock':
        kibot=pd.read_csv(kpath + '/kibot_stocks.csv')
    elif tname=='etf':
        kibot=pd.read_csv(kpath + '/kibot_etfs.csv')
    elif tname=='index':
        kibot=pd.read_csv(kpath + '/kibot_indices.csv')  
    elif tname=='delisted':
        kibot=pd.read_csv(kpath + '/kibot_delisted.csv')
    else:
        if ext:
            return getTickers(tname='stock',ext=ext).append(getTickers(tname='etf',ext=ext)).append(getTickers(tname='index',ext=ext)).append(getTickers(tname='delisted',ext=ext))
        else:
            return getTickers(tname='stock',ext=ext)+getTickers(tname='etf',ext=ext)+getTickers(tname='index',ext=ext)+getTickers(tname='delisted',ext=ext)
    if ext:
        return kibot
    else:
        return list( kibot[['Symbol']].dropna()['Symbol'] )

def getSectorsConstituents():
    kibot=pd.read_csv(kpath + '/kibot_stocks.csv')
    ksectors=getSectors()
    md={}
    for s in ksectors:
        md[s]=np.asarray( kibot[kibot['Sector']==s][['Symbol']].dropna())
    return pd.concat([pd.DataFrame(v, columns=[k]) for k, v in md.items()], axis=1)

##############################################
# Search
##############################################
def search(search_string, exchange=''):
    df = getTickers(tname='all',ext=True)
    df['Exchange']='US'
    df = df[['Symbol','Exchange','Description','Industry','Sector','StartDate']].fillna('Not available')
    df = df[df.Symbol==search_string].append(df[df['Description'].astype(str).str.find(search_string)>0])
    return df[df['Description']!='Not available']
    
##############################################
# Cache Management
##############################################
def install_cache():
    prd.renableCache()

def suspend_cache():
    prd.isCached(remove=True)
    
def remove_cache():
    prd.removeCache()

##############################################
# Wrappers for compatibility
##############################################
def get_eod_data(symbol, dt_start, dt_end, adj=0,tz='America/New_York'): # MM/DD/YYYY from YYYY-MM-DD
    dt_start='/'.join(dt_start.split('-')[::-1][:2][::-1]+list(dt_start.split('-'))[0:1])
    dt_end='/'.join(dt_end.split('-')[::-1][:2][::-1]+list(dt_end.split('-'))[0:1])
    if adj == 0:
        return getData(symbol,dt_start,dt_end, unadj=True, tz=tz)
    elif adj == 1:
        return getData(symbol,dt_start,dt_end, unadj=False, tz=tz)    
    else:
        return getData2(symbol,dt_start,dt_end, tz=tz)
        
def get_eod_dataFX(cur1,cur2,dt_start, dt_end, adj=0,tz='America/New_York'):
    symbol=cur1+cur2
    dt_start='/'.join(dt_start.split('-')[::-1][:2][::-1]+list(dt_start.split('-'))[0:1])
    dt_end='/'.join(dt_end.split('-')[::-1][:2][::-1]+list(dt_end.split('-'))[0:1])
    if adj == 0:
        return getData(symbol,dt_start,dt_end, unadj=True, ctype='forex', tz=tz)
    elif adj == 1:
        return getData(symbol,dt_start,dt_end, unadj=False, ctype='forex', tz=tz)    
    else:
        return getData2(symbol,dt_start,dt_end,ctype='forex', tz=tz)

def get_eod_dataFX2(symbol,dt_start, dt_end, adj=0,tz='America/New_York'):
    dt_start='/'.join(dt_start.split('-')[::-1][:2][::-1]+list(dt_start.split('-'))[0:1])
    dt_end='/'.join(dt_end.split('-')[::-1][:2][::-1]+list(dt_end.split('-'))[0:1])
    if adj == 0:
        return getData(symbol,dt_start,dt_end, unadj=True, ctype='forex', tz=tz)
    elif adj == 1:
        return getData(symbol,dt_start,dt_end, unadj=False, ctype='forex', tz=tz)    
    else:
        return getData2(symbol,dt_start,dt_end,ctype='forex', tz=tz)

def getContFuture(symbol, dt_start, dt_end, adj=0,tz='America/New_York'):
    dt_start='/'.join(dt_start.split('-')[::-1][:2][::-1]+list(dt_start.split('-'))[0:1])
    dt_end='/'.join(dt_end.split('-')[::-1][:2][::-1]+list(dt_end.split('-'))[0:1])
    if adj == 0:
        return getData(symbol,dt_start,dt_end, unadj=True, ctype='future', tz=tz)
    elif adj == 1:
        return getData(symbol,dt_start,dt_end, unadj=False, ctype='future', tz=tz)    
    else:
        return getData2(symbol,dt_start,dt_end, ctype='future', tz=tz)

def getindex(symbol, dt_start, dt_end, adj=0,tz='America/New_York'):
    dt_start='/'.join(dt_start.split('-')[::-1][:2][::-1]+list(dt_start.split('-'))[0:1])
    dt_end='/'.join(dt_end.split('-')[::-1][:2][::-1]+list(dt_end.split('-'))[0:1])
    if adj == 0:
        return getData(symbol,dt_start,dt_end, unadj=True, ctype='index', tz=tz)
    elif adj == 1:
        return getData(symbol,dt_start,dt_end, unadj=False, ctype='index', tz=tz)    
    else:
        return getData2(symbol,dt_start,dt_end, ctype='index', tz=tz)

##############################################
# End of Day Historical data
##############################################    
def getData2(symbol,sd='05-01-2017',ed='10-02-2017',regsess_only=False, ctype='stock',freq='daily', tz='America/New_York'):
    udf=getDataAU(symbol,sd=sd,ed=ed,regsess_only=regsess_only, ctype=ctype, unadj=True, freq=freq, tz=tz)
    adf=getDataAU(symbol,sd=sd,ed=ed,regsess_only=regsess_only, ctype=ctype, unadj=False, freq=freq, tz=tz)
    adf=adf[['Adjusted_close']]
    del udf['Adjusted_close']
    df=udf.join(adf)
    return df[['Open','High','Low','Close','Adjusted_close','Volume']]
    
def getData(symbol,sd='05-01-2017',ed='10-02-2017',regsess_only=False, ctype='stock', unadj=True, freq='daily', tz='America/New_York'):
    endpoint='?action=history'
    url=root+endpoint+'&symbol='+symbol+'&interval='+freq+'&startdate='+sd+'&enddate='+ed+'&type='+str(ctype)      
    if regsess_only:
        url=url+'&regularsession=1'
    if unadj:
        url=url+'&unadjusted=1'
    
    df=prd.requestsText(url+key,headers)
    if len(df)>0 and len(df.columns)==6:
        df.columns=['Date','Open','High','Low','Close','Volume']
        df['Adjusted_close']=df['Close']
        df['Date'] = pd.to_datetime(df['Date'])
        df['Date'] = df['Date'].astype('datetime64[ns]')
        df['Date'] = df.Date.dt.tz_localize(ORIGIN_TZ, ambiguous='infer').dt.tz_convert(tz)
        df=df.set_index('Date')
        df=df[['Open','High','Low','Close','Adjusted_close','Volume']]
    else:
        df=pd.DataFrame()
    return df.dropna()

##############################################
# Realtime (Delayed) data
############################################## 
def getRTData(symbols=[], tz='America/New_York'):
    import Plib.DataFarm.PReader as prd
    
    endpoint='?action=snapshot'
    url=root+endpoint+'&symbol='+','.join(symbols)   
    
    cols=['Symbol','Date','Time','LastPrice','LastVolume','Open','High','Low','Close','Volume','ChangePercent','TimeZone']
    df=prd.requestsText(url+key,headers, parsedt=True, cols=cols)
    if len(df)>1 and len(df.columns)==11:
        df=df[df.Symbol != '404 Symbol Not FoundSymbol']
        df=df[df.Symbol != 'Symbol']
        df['Date_Time'] = df['Date_Time'].astype('datetime64[ns]')
        df['Date'] = pd.to_datetime(df['Date_Time'])
        df['Date'] = df['Date'].astype('datetime64[ns]')
        del df['Date_Time']
        df['Date'] = df.Date.dt.tz_localize(ORIGIN_TZ, ambiguous='infer').dt.tz_convert(tz)
        df=df.set_index('Date')
        for c in df.columns:
            if c not in ['Symbol','TimeZone','ChangePercent']:
                df[c] = df[c].astype(float)
        df['ChangePercent']=df['ChangePercent'].astype(str).str.replace('%', '', regex=False)
        df['ChangePercent'] = df['ChangePercent'].astype(float)  
        df['Change']=df['LastPrice']-df['Close']
        df=df[['Symbol','Open','High','Low','LastPrice','Volume','Close','Change','ChangePercent']]
        df.rename(columns = {'Close':'Prev_close'}, inplace = True)
        df.rename(columns = {'LastPrice':'Close','ChangePercent':'Change_p'}, inplace = True)
    else:
        df=pd.DataFrame()
    return df.dropna()

##############################################
# Bulk Realtime (Delayed) data
############################################## 
def getAllRT(tname='stock', tickers=[]):
    
    if tickers==[]:
        tickers=getTickers(tname=tname)
    if len(tickers) < 100:
        nc=len(tickers)
    else:
        nc=int(len(tickers)/100)

    l=[]
    ofs1=0
    ofs2=100
    for i in range(1, nc+1):
        l.append(tickers[ofs1:ofs2])
        ofs1=ofs1+100
        ofs2=ofs2+100
    if len(tickers)%100 != 0:
        l.append(tickers[ofs1:]) 
    
    allrt=[]
    for tl in l:
        df=getRTData(symbols=tl)
        if len(df)>0:
            allrt.append(df)
    df=allrt[0]
    for i in range(1, len(allrt)):
        df=df.append(allrt[i])
    return df
      