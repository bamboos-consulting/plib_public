#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Test unit 
#
# Module including functions to test functions and procedures input/output
#############################################################################################      

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import pickle
import random
import warnings
import pandas as pd
import numpy as np    
warnings.filterwarnings("once")


def loadTestData():
    #17-sep-2007 - 24-jun-2015
    #Date, High Low, Open, Close
    df=pd.read_csv('test.csv',sep=';', decimal=',')  
    df['Open'] = df['Open'].astype('float')
    df['High'] = df['High'].astype('float')
    df['Low'] = df['Low'].astype('float')
    df['Close'] = df['Close'].astype('float')
    df['Adjusted_close']=df['Close']
    df['Volume']=df['Close']*df['High']
    df['Date'] = pd.to_datetime(df['Date'],format='%d/%m/%Y')
    df = df.set_index(pd.DatetimeIndex(df['Date']))
    df.index=df.index.tz_localize('America/New_York', nonexistent='shift_forward')
    del df['Date']
    return df

def loadMinTestData():
    #19/01/2021 09:15 - 20/01/2021 14:38
    #Date, High Low, Open, Close
    df=pd.read_csv('STOCK.csv',sep=';', decimal=',')  
    df['Open'] = df['Open'].astype('float')
    df['High'] = df['High'].astype('float')
    df['Low'] = df['Low'].astype('float')
    df['Close'] = df['Close'].astype('float')
    df['Adjusted_close']=df['Close']
    df['Volume']=df['Close']*df['High']
    df['Date'] = pd.to_datetime(df['Date'],format='%d/%m/%Y %H:%M')
    df = df.set_index(pd.DatetimeIndex(df['Date']))
    df.index=df.index.tz_localize('America/New_York', nonexistent='shift_forward')
    del df['Date']
    return df
    
    
    