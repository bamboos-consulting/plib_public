#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# IBapi 
#
# Module including functions to trade with InteractiveBrokers
#############################################################################################      


# Commmon Libraries
from ibapi.client import EClient
from ibapi.wrapper import EWrapper
from ibapi.ticktype import TickTypeEnum
from ibapi.contract import * # @UnusedWildImport
from ibapi.order import * # @UnusedWildImport


import threading
import time
import pandas as pd
import sqlite3
import datetime

import warnings
warnings.filterwarnings("once")
DEF_VERBOSE_PRINT=0
SLEEP_TIME=0.3
MAX_SLEEP=30

MASTER=100
STAT_CLI=101
DEV_CLI=102
DHB_CLI=103
MKD_CLI=104

#smbutil lookup desktop-da3ttna
IPCONN='192.168.9.157'
IBConn = {
    'TWS': (DEV_CLI,7496,IPCONN),
    'MKD': (MKD_CLI,7496,IPCONN),
    'IBG': (DEV_CLI,4002,IPCONN),
    'STT': (STAT_CLI,7496,IPCONN),
    'STG': (STAT_CLI,4002,IPCONN),
    'DHT': (DHB_CLI,7496,IPCONN),
    'DHG': (DHB_CLI,4002,IPCONN)
}

db_enabled=0

##############################################
# Implementation of IB base class
##############################################    
class IBapi(EWrapper, EClient):
    import sys
    import os
    import pandas as pd
    
    module_path = os.path.abspath(os.path.join('..'))
    if module_path not in sys.path:
        sys.path.append(module_path + '/')
    import Plib.Utils.DbAccess as adb
    import Plib.Ledger.EOrders as eor
    import Plib.Ledger.AOrders as aor
    
    def __init__(self):
        EClient.__init__(self, self)        
                
    ##############################################
    # Account Management (3)
    ##############################################    
    def pnl(self, reqId: int, dailyPnL: float,unrealizedPnL: float, realizedPnL: float):
        super().pnl(reqId, dailyPnL, unrealizedPnL, realizedPnL)
        if int(reqId) != -1:
            if DEF_VERBOSE_PRINT==3:
                print("Daily PnL. ReqId:", reqId, "DailyPnL:", dailyPnL,"UnrealizedPnL:", 
                    unrealizedPnL, "RealizedPnL:", realizedPnL)
    def accountSummary(self, reqId: int, account: str, tag: str, value: str,currency: str):
        super().accountSummary(reqId, account, tag, value, currency)
        if int(reqId) != -1:
            if DEF_VERBOSE_PRINT==3:
                print("AccountSummary. ReqId:", reqId, "Account:", account,
                    "Tag: ", tag, "Value:", value, "Currency:", currency)
    def accountSummaryEnd(self, reqId: int):
        super().accountSummaryEnd(reqId)
        #print("AccountSummaryEnd. ReqId:", reqId)
    ##############################################
    # Real Time Data Management (1)
    ##############################################    
    def tickPrice(self, reqId, tickType, price, attrib):
        global buffer_tickers
        super().tickPrice(reqId, tickType, price, attrib)
        if int(reqId) != -1:
            if DEF_VERBOSE_PRINT==1:
                #Bid Price	1	Highest priced bid for the contract.
                #Ask Price	2	Lowest price offer on the contract.
                #Last Price	4	Last price at which the contract traded (does not include some trades in RTVolume).
                #High	6	High price for the day.
                #Low	7	Low price for the day.
                #Close Price	9	The last available closing price for the previous day. Adjusted price for US stocks
                #Others include highs/lows for week/year,auction price, mark price, ETF NAV, and bond yields
                print("TickPrice. TickerId:", reqId, "tickType:", tickType,"Price:", price, "CanAutoExecute:", attrib.canAutoExecute,"PastLimit:", attrib.pastLimit, end=' ')
            #{'Price':price,'BidSize':size,'AskSize':size,'LastSize':size,'Volume':size,'VolRate':value,
            #'LastTrade':value,'TradeCount':value,'IV':value}
            if reqId not in buffer_tickers.keys():
                buffer_tickers[reqId]={}
            if tickType == 1:
                buffer_tickers[reqId]['BidPrice']=float(price)
            if tickType == 2:
                buffer_tickers[reqId]['AskPrice']=float(price)
            if tickType == 4:
                buffer_tickers[reqId]['LastPrice']=float(price)
            if tickType == 6:
                buffer_tickers[reqId]['HighPrice']=float(price)
            if tickType == 7:
                buffer_tickers[reqId]['LowPrice']=float(price)
            if tickType == 9:
                buffer_tickers[reqId]['ClosePrice']=float(price)
    def tickSize(self, reqId, tickType, size: int):
        super().tickSize(reqId, tickType, size)
        if int(reqId) != -1:
            if DEF_VERBOSE_PRINT==1:
                #Bid Size	0	Number of contracts or lots offered at the bid price.
                #Ask Size	3	Number of contracts or lots offered at the ask price.
                #Last Size	5	Number of contracts or lots traded at the last price
                #Volume	8	Trading volume for the day for the selected contract (US Stocks: multiplier 100).
                #Other includes volume,open interest for stock option and futures, imbalance
                print("TickSize. TickerId:", reqId, "TickType:", tickType, "Size:", size)
            if reqId not in buffer_tickers.keys():
                buffer_tickers[reqId]={}
            if tickType == 0:
                buffer_tickers[reqId]['BidSize']=int(size)
            if tickType == 3:
                buffer_tickers[reqId]['AskSize']=int(size)
            if tickType == 5:
                buffer_tickers[reqId]['LastSize']=int(size)
            if tickType == 8:
                buffer_tickers[reqId]['Volume']=int(size)
    def tickString(self, reqId, tickType, value: str):
        #Last Timestamp	45	Time of the last trade (in UNIX time).
        #RT Volume (Time & Sales)	48	Last trade details (Including both "Last" and "Unreportable Last" trades)
        #IB Dividends	59	Contract's dividends
        #News	62	Contract's news feed
        #RT Trade Volume	77	Last trade details that excludes "Unreportable Trades". 
        #Last Exchange	84	Exchange of last traded price
        super().tickString(reqId, tickType, value)
        if int(reqId) != -1:
            if DEF_VERBOSE_PRINT==1:
                print("TickString. TickerId:", reqId, "Type:", tickType, "Value:", value)
            if reqId not in buffer_tickers.keys():
                buffer_tickers[reqId]={}
            if tickType == 45:
                buffer_tickers[reqId]['LastTrade']=int(value)
            if tickType == 48:
                buffer_tickers[reqId]['RTVol']=int(value)
            if tickType == 77:
                buffer_tickers[reqId]['RTTradeVol']=int(value)
    def tickGeneric(self, reqId, tickType, value: float):
        #Option Implied Volatility	24	at-market volatility for a maturity thirty calendar days forward for two expirations 
        #Index Future Premium	31	The number of points that the index is over the cash index.
        #Shortable	46	Describes the level of difficulty with which the contract can be sold short.
        #Halted	49	Indicates if a contract is halted.
        #Trade Count	54	Trade count for the day.	
        #Trade Rate	55	Trade count per minute.
        #Volume Rate	56	Volume per minute.
        #Bond Factor Multiplier	60	Tthe ratio of the current bond principal to the original principal
        super().tickGeneric(reqId, tickType, value)
        if int(reqId) != -1:
            if DEF_VERBOSE_PRINT==1:
                print("TickGeneric. TickerId:", reqId, "TickType:", tickType, "Value:", value)
            if reqId not in buffer_tickers.keys():
                buffer_tickers[reqId]={}
            if tickType == 54:
                buffer_tickers[reqId]['TradeCount']=float(value)
            if tickType == 55:
                buffer_tickers[reqId]['TradeRate']=float(value)
            if tickType == 56:
                buffer_tickers[reqId]['VolRate']=float(value)
            if tickType == 24:
                buffer_tickers[reqId]['FWD2MATMIV']=float(value)
            if tickType == 46:
                buffer_tickers[reqId]['Shortable']=float(value)
            if tickType == 49:
                buffer_tickers[reqId]['Halted']=int(value)
    def tickByTickBidAsk(self, reqId: int, time: int, bidPrice: float, askPrice: float, bidSize: int, askSize: int, tickAttribBidAsk):
        global buffer_tbtdata
        global max_bsize
        #data will be returned to one of the functions tickByTickAllLast, tickByTickBidAsk, or tickByTickMidPoint. 
        super().tickByTickBidAsk(reqId, time, bidPrice, askPrice, bidSize,askSize, tickAttribBidAsk)
        if int(reqId) != -1:
            mtime=datetime.datetime.fromtimestamp(time).strftime("%Y-%m-%d %H:%M:%S")
            Imbalance=(bidSize/(bidSize+askSize))
            BASpread=askPrice-bidPrice
            Midprice=(bidPrice+askPrice)/2
            WMidprice=Imbalance*askPrice+(1-Imbalance)*bidPrice
            if DEF_VERBOSE_PRINT==1:
                print("BidAsk. ReqId:", reqId,"Time:", mtime,
                    "BidPrice:", bidPrice, "AskPrice:", askPrice, "BidSize:", bidSize, "AskSize:", askSize, "BidPastLow:", 
                    tickAttribBidAsk.bidPastLow, "AskPastHigh:", tickAttribBidAsk.askPastHigh,
                    "Midprice:", round(Midprice,2), "Weighted Midprice:", round(WMidprice,2))
            cols=['Time','BidPrice','AskPrice','BidSize','AskSize','BidPastLow','AskPastHigh','Midprice','WMidprice']
            temp=pd.DataFrame(cols)
            temp['Time']=mtime
            temp['BidPrice']=bidPrice
            temp['AskPrice']=askPrice
            temp['BidSize']=bidSize
            temp['AskSize']=askSize
            temp['BidPastLow']=tickAttribBidAsk.bidPastLow
            temp['AskPastHigh']=tickAttribBidAsk.askPastHigh
            temp['Midprice']=Midprice
            temp['WMidprice']=WMidprice
            if reqId in buffer_tbtdata.keys():
                df=buffer_tbtdata[reqId]
                temp=temp[['Time','BidPrice','AskPrice','BidSize','AskSize','BidPastLow','AskPastHigh','Midprice','WMidprice']]
                temp=temp.drop_duplicates() 
                temp['Time'] = pd.to_datetime(temp['Time'])
                buffer_tbtdata[reqId]=df.append(temp)
                if len(buffer_tbtdata[reqId]) > max_bsize:
                    buffer_tbtdata[reqId] = buffer_tbtdata[reqId][:-1]
            else:
                temp=temp[['Time','BidPrice','AskPrice','BidSize','AskSize','BidPastLow','AskPastHigh','Midprice','WMidprice']]
                temp=temp.drop_duplicates() 
                temp['Time'] = pd.to_datetime(temp['Time'])
                buffer_tbtdata[reqId]=temp             
    def marketRule(self, marketRuleId, priceIncrements):
        #Market rule for forex and forex CFDs indicates default configuration (1/2 and not 1/10 pips). 
        #It can be adjusted to 1/10 pips through TWS or IB Gateway Global Configuration.
        #Some non-US securities, for instance on the SEHK exchange, have a minimum lot size. 
        #This information is not available
        global buffer_pincr
        super().marketRule(marketRuleId, priceIncrements)
        buffer_pincr[marketRuleId]=priceIncrements
        if DEF_VERBOSE_PRINT==1:
            print("Market Rule ID: ", marketRuleId)
            for priceIncrement in priceIncrements:
                print("Price Increment.", priceIncrement)
    ##############################################
    # Historical and fundamental Data (1)
    ##############################################    
    def historicalData(self, reqId, bar):
        #If reqHistoricalData was invoked with keepUpToDate = false, once all candlesticks have been received 
        #the historicalDataEnd marker will be sent. Otherwise updates of the most recent partial five-second bar 
        #will continue to be returned in real time to historicalDataUpdate. 
        #The keepUpToDate functionality can only be used with bar sizes 5 seconds or greater
        global buffer_hdata
        if int(reqId) != -1:
            if DEF_VERBOSE_PRINT==1:
                print(f'Time: {bar.date} Close: {bar.close}')
            cols=['Date','Open','High','Low','Close','Adjusted_close','Volume']
            temp=pd.DataFrame(cols)
            temp['Date']=bar.date
            temp['Open']=bar.open
            temp['High']=bar.high
            temp['Low']=bar.low
            temp['Close']=bar.close
            temp['Adjusted_close']=bar.close
            temp['Volume']=bar.volume
            temp['Wap']=0#bar.wap
            temp['Count']=0#bar.count
            if reqId in buffer_hdata.keys():
                df=buffer_hdata[reqId]
                temp=temp[['Date','Open','High','Low','Close','Adjusted_close','Volume','Wap','Count']]
                temp=temp.drop_duplicates() 
                temp['Date'] = pd.to_datetime(temp['Date'])#.dt.date
                temp['Date'] = temp['Date'].astype('datetime64[ns]')
                buffer_hdata[reqId]=df.append(temp)
            else:
                temp=temp[['Date','Open','High','Low','Close','Adjusted_close','Volume','Wap','Count']]
                temp=temp.drop_duplicates() 
                temp['Date'] = pd.to_datetime(temp['Date'])#.dt.date
                temp['Date'] = temp['Date'].astype('datetime64[ns]')
                buffer_hdata[reqId]=temp
    def historicalDataEnd(self, reqId: int, start: str, end: str):
        super().historicalDataEnd(reqId, start, end)
        if int(reqId) != -1:
            if DEF_VERBOSE_PRINT==1:
                print("HistoricalDataEnd. ReqId:", reqId, "from", start, "to", end)
            cols=['Date','Open','High','Low','Close','Adjusted_close','Volume','Wap','Count']
            temp=pd.DataFrame(cols)
            temp['Date']='1900-01-01'
            temp['Open']=0
            temp['High']=0
            temp['Low']=0
            temp['Close']=0
            temp['Adjusted_close']=0
            temp['Volume']=0
            temp['Wap']=0
            temp['Count']=0
            if reqId in buffer_hdata.keys():
                df=buffer_hdata[reqId]
                temp=temp[['Date','Open','High','Low','Close','Adjusted_close','Volume','Wap','Count']]
                temp=temp.drop_duplicates() 
                temp['Date'] = pd.to_datetime(temp['Date'])#.dt.date
                temp['Date'] = temp['Date'].astype('datetime64[ns]')
                buffer_hdata[reqId]=df.append(temp)
    def fundamentalData(self,reqId,data):
        global buffer_fdata
        super().fundamentalData(reqId,data)
        if int(reqId) != -1:
            if DEF_VERBOSE_PRINT==1:
                print("Fundamental Data. ReqId:", reqId, "Data:", data)
            buffer_fdata[reqId]=data		
    def historicalTicksBidAsk(self, reqId, ticks,done):
        global buffer_htbtdata
        #Time: 1593622859, TickAttriBidAsk: BidPastLow: 0, AskPastHigh: 0, 
        #PriceBid: 366.520000, PriceAsk: 366.560000, SizeBid: 200, SizeAsk: 300
        #data will be returned to historicalTickAllLast, historicalTickBidAsk, or historicalTickMidPoint. 
        super().historicalTicksBidAsk(reqId, ticks, done)
        if int(reqId) != -1:
            if done:
                cols=['Time','BidPrice','AskPrice','BidSize','AskSize','BidPastLow','AskPastHigh','Midprice','WMidprice']
                temp=pd.DataFrame(cols)
                temp['Time']='1900-01-01 00:00:00'
                temp['BidPrice']=0
                temp['AskPrice']=0
                temp['BidSize']=0
                temp['AskSize']=0
                temp['BidPastLow']=0
                temp['AskPastHigh']=0
                temp['Midprice']=0
                temp['WMidprice']=0
                if reqId in buffer_htbtdata.keys():
                    df=buffer_htbtdata[reqId]
                    temp=temp[['Time','BidPrice','AskPrice','BidSize','AskSize','BidPastLow','AskPastHigh','Midprice','WMidprice']]
                    temp=temp.drop_duplicates() 
                    temp['Time'] = pd.to_datetime(temp['Time'])
                    buffer_htbtdata[reqId]=df.append(temp)
            for tick in ticks:
                if DEF_VERBOSE_PRINT==1:
                    print("HistoricalTickBidAsk. ReqId:", reqId, tick)
                mtime=datetime.datetime.fromtimestamp(tick.time).strftime("%Y-%m-%d %H:%M:%S")
                Imbalance=(tick.sizeBid/(tick.sizeBid+tick.sizeAsk))
                BASpread=tick.priceAsk-tick.priceBid
                Midprice=(tick.priceBid+tick.priceAsk)/2
                WMidprice=Imbalance*tick.priceAsk+(1-Imbalance)*tick.priceBid
                cols=['Time','BidPrice','AskPrice','BidSize','AskSize','BidPastLow','AskPastHigh','Midprice','WMidprice']
                temp=pd.DataFrame(cols)
                temp['Time']=mtime
                temp['BidPrice']=tick.priceBid
                temp['AskPrice']=tick.priceAsk
                temp['BidSize']=tick.sizeBid
                temp['AskSize']=tick.sizeAsk
                temp['BidPastLow']=tick.tickAttribBidAsk.bidPastLow 
                temp['AskPastHigh']=tick.tickAttribBidAsk.askPastHigh
                temp['Midprice']=Midprice
                temp['WMidprice']=WMidprice
                if reqId in buffer_htbtdata.keys():
                    df=buffer_htbtdata[reqId]
                    temp=temp[['Time','BidPrice','AskPrice','BidSize','AskSize','BidPastLow','AskPastHigh','Midprice','WMidprice']]
                    temp=temp.drop_duplicates() 
                    temp['Time'] = pd.to_datetime(temp['Time'])
                    buffer_htbtdata[reqId]=df.append(temp)
                else:
                    temp=temp[['Time','BidPrice','AskPrice','BidSize','AskSize','BidPastLow','AskPastHigh','Midprice','WMidprice']]
                    temp=temp.drop_duplicates() 
                    temp['Time'] = pd.to_datetime(temp['Time'])
                    buffer_htbtdata[reqId]=temp
    ##############################################
    # Contract Details (2)
    ##############################################    
    def securityDefinitionOptionParameter(self, reqId: int, exchange: str,underlyingConId: int, tradingClass: str, multiplier: str,expirations: [], strikes: []):
        super().securityDefinitionOptionParameter(reqId, exchange,underlyingConId, tradingClass, multiplier, expirations, strikes)
        if DEF_VERBOSE_PRINT==1:
            print("SecurityDefinitionOptionParameter.","ReqId:", reqId, "Exchange:", exchange, "Underlying conId:", underlyingConId, 
            "TradingClass:", tradingClass, "Multiplier:", multiplier,"Expirations:", expirations, "Strikes:", str(strikes))
    def contractDetails(self, reqId, contractDetails):
        global buffer_cdet
        if int(reqId) != -1:
            if DEF_VERBOSE_PRINT==2:
                print('Contract id: ' + str(reqId), 'Min tick: ' + str(contractDetails.minTick))
                print('Contract id: ' + str(reqId), 'Market Rules id(s): ' + str(contractDetails.marketRuleIds))
                print('Contract id: ' + str(reqId), '(Und)ContractId: ' + str(contractDetails.underConId))
                print('Contract id: ' + str(reqId), 'ContractId: ' + str(contractDetails.contract.conId))
            if reqId not in buffer_cdet.keys():
                Dict = {'minTick': contractDetails.minTick, 
                        'marketRules': pd.DataFrame(str(contractDetails.marketRuleIds).split(','),columns=['rule']).rule.unique(),
                        'uconId': contractDetails.underConId,
                        'conId': contractDetails.contract.conId,
                        'exch': contractDetails.contract.exchange,
                        'contracts': list()}
                Dict['contracts'].append(contractDetails)
                buffer_cdet[reqId]=Dict
            else:
                buffer_cdet[reqId]['contracts'].append(contractDetails)
    def contractDetailsEnd(self, reqId):
        super().contractDetailsEnd(reqId)
        if int(reqId) != -1:
            if DEF_VERBOSE_PRINT==2:
                print("ContractDetailsEnd. ReqId:", reqId)
            buffer_cdet[reqId]['contracts'].append('END')
    ##############################################
    # Orders Management (2)
    ##############################################    
    def nextValidId(self, orderId: int):
        super().nextValidId(orderId)
        self.nextorderId = orderId
        if DEF_VERBOSE_PRINT==2:
            print('The next valid order id is: ', self.nextorderId)
    def openOrder(self, orderId, contract, order, orderState):
        #As long as an order is active, it is possible to retrieve it using the TWS API. 
        #Orders submitted via the TWS API will always be bound to the client Id they were submitted from 
        #meaning only the submitting client will be able to modify the placed order. 
        #Note: it is not possible to obtain cancelled or fully filled orders.
        #Active orders will be delivered via  openOrder and orderStatus callbacks. When all orders have been sent 
        #to the client application you will receive a openOrderEnd event
        super().openOrder(orderId, contract, order, orderState)
        if DEF_VERBOSE_PRINT==2:
            print('openOrder id:', orderId, contract.symbol, contract.secType, '@', contract.exchange, ':', order.action, order.orderType, order.totalQuantity, orderState.status)
    def orderStatus(self, orderId, status, filled, remaining, avgFullPrice, permId, parentId, lastFillPrice, clientId, whyHeld, mktCapPrice):
        super().orderStatus(orderId, status, filled, remaining,avgFillPrice, permId, parentId, lastFillPrice, clientId, whyHeld, mktCapPrice)
        if DEF_VERBOSE_PRINT==2:
            print('orderStatus - orderid:', orderId, 'status:', status, 'filled', filled, 'remaining', remaining, 'lastFillPrice', lastFillPrice)
    def execDetails(self, reqId, contract, execution):
        #When an order is filled either fully or partially, the execDetails and commissionReport events will deliver 
        #IBApi.Execution and IBApi.CommissionReport objects
        global db_enabled
        super().execDetails(reqId, contract, execution)
        if db_enabled>0:
            if DEF_VERBOSE_PRINT==2:
                print('Order Executed: ', reqId, contract.symbol, contract.secType, contract.currency, execution.execId, execution.orderId, execution.shares, execution.lastLiquidity)
            self.updateStatus(int(execution.orderId), int(execution.shares), str(execution.price),str(execution.permId),str(execution.execId))
    def commissionReport(self, commissionReport):
        global db_enabled
        super().commissionReport(commissionReport)
        if db_enabled>0:
            if DEF_VERBOSE_PRINT==2:
                print("CommissionReport.", commissionReport)
            self.updateCommissions(commissionReport.execId,commissionReport.commission,commissionReport.currency)
    def error(self, reqId, code, msg):
        global db_enabled
        if int(reqId) != -1:
            if DEF_VERBOSE_PRINT>=0:
                print('Error {}: {}'.format(code, msg) + ' - Order id: ' + str(reqId))
            if db_enabled>0:
                self.updateMissedOrders(reqId, code, msg)
    ##############################################
    # Connect IB to the ledger
    # Write on EORDS,RORDS
    # Delete on AORDS
    # Read on AORDS
    ##############################################   
    def setDbFile(self,db):
        self.adb.setParams(db,bcli)
    def setLedgerParams(self,db,brk,exch,cli):
        self.eor.setParams(db,brk,exch,cli)
        self.aor.setParams(db,brk,exch,cli)
    def updateCommissions(self,execId,comm,cur):
        if DEF_VERBOSE_PRINT ==2: 
            print(execId,comm,cur)
        self.adb.simple_update('EORDS' ,{'comm': str(comm),'euid':str(execId)})
    def updateStatus(self,orderUID, filled, lastFillPrice, permId, execId):     
        if DEF_VERBOSE_PRINT ==2: 
            print(orderUID, filled, lastFillPrice, permId,execId)
    
        if (int(filled) != 0):
            if DEF_VERBOSE_PRINT ==2: 
                print(orderUID, filled, lastFillPrice, permId,execId)
            sid,aoid,sotype,sect,ordr_sp_type=self.eor.getStrByBrkUID(str(orderUID))
            #if type combo select different aoid
            if ordr_sp_type != 'CMB':
                self.eor.createEOrder(int(sid),int(aoid),str(permId),str(execId),str(lastFillPrice),str(filled),str(sotype),str(sect))
            else:
                #CMB type never executed, so research excluding such type -> obtain the details for the leg having orderUID
                sid,aoid,sotype,sect,ordr_sp_type=self.eor.getStrByBrkUID(str(orderUID),True)
                self.eor.createEOrder(int(sid),int(aoid),str(permId),str(execId),str(lastFillPrice),str(filled),str(sotype),str(sect))   
    def updateMissedOrders(self,reqId, code, msg):
        if int(reqId) != -1:
            ret=self.aor.getAOByBrkUID(reqId)
            if len(ret) >0:
                roid=self.aor.createROrder(ret,reqId,code,msg)
        
##############################################
# Connect to IB and start the loop
##############################################    
def ibConnect(port=4002,ip='127.0.0.1',client_id=DEV_CLI, db=''): 
    #make object available to non-member functions to access historical data
    global buffer_tickers
    global buffer_ochain
    global buffer_hdata
    global buffer_cdet
    global buffer_tbtdata
    global buffer_htbtdata
    global buffer_pincr
    global buffer_fdata
    global bcli
    global myconn
    global bcli
    global db_enabled
    
    max_bsize=1000
    buffer_tickers={}
    buffer_hdata={}
    buffer_tbtdata={}
    buffer_htbtdata={}
    buffer_cdet={}
    buffer_pincr={}
    buffer_fdata={}
    buffer_ochain={}
    bcli=client_id
    
    app = IBapi()
    if db != '':
        db_enabled=1
        app.setDbFile(db)
        app.setLedgerParams(db,1,1,client_id)
    else:
        db_enabled=0
        
    app.connect(ip, port, client_id)
    app.nextorderId = None
    #app.reqMarketDataType(4)
    
    #Start the socket in a thread - daemon means it will run until the calling process is destroyed
    api_thread = threading.Thread(target=app.run, daemon=True)
    api_thread.start()    
    
    myconn=app
    return app

##############################################
# Helper functions for data download
##############################################    
def waitHdata(reqId,buffer=[],col='Date'):
    total_time=0
    while reqId not in buffer.keys():
        time.sleep(SLEEP_TIME)
        total_time=total_time+SLEEP_TIME
        if total_time > MAX_SLEEP:
            return 'ERROR'
    
    while '1900-01-01' != buffer[reqId][col].iloc[-1].strftime("%Y-%m-%d"):
        time.sleep(SLEEP_TIME)
        total_time=total_time+SLEEP_TIME
        if total_time > MAX_SLEEP:
            return 'ERROR'
    buffer[reqId]=buffer[reqId].iloc[:-1,:].copy()

def wait4data(reqId,buffer=[],col=''):
    total_time=0
    while reqId not in buffer.keys():
        time.sleep(SLEEP_TIME)
        total_time=total_time+SLEEP_TIME
        if total_time > MAX_SLEEP:
            return 'ERROR'
    if col != '':
        while col not in buffer[reqId].keys():
            time.sleep(SLEEP_TIME)
            total_time=total_time+SLEEP_TIME
            if total_time > MAX_SLEEP:
                return 'ERROR'
                
def getIBDuration(dt_start, dt_end):
    from datetime import datetime
    
    date_format = '%Y-%m-%d'
    duration = str((datetime.strptime(dt_end, date_format)-datetime.strptime(dt_start, date_format)).days) + ' d'
    return duration

##############################################
# Wrappers for account management
##############################################    
def reqAccSummary(reqId,group='All',tag='$LEDGER:ALL'):
    global myconn
    #$LEDGER:ALL — Single flag to relay all cash balance tags* in all currencies
    #AccountType — Identifies the IB account structure
    #NetLiquidation — Total cash value + stock value + options value + bond value
    #TotalCashValue — Total cash balance recognized at the time of trade + futures PNL
    #SettledCash — Cash recognized at the time of settlement - purchases at the time of trade - commissions - taxes - fees
    #AccruedCash — Total accrued cash value of stock, commodities and securities
    #InitMarginReq — Initial Margin requirement of whole portfolio
    #MaintMarginReq — Maintenance Margin requirement of whole portfolio
    #AvailableFunds — This value tells what you have available for trading
    #ExcessLiquidity — This value shows your margin cushion, before liquidation
    #Cushion — Excess liquidity as a percentage of net liquidation value
    #FullInitMarginReq — Initial Margin of whole portfolio with no discounts or intraday credits
    #FullMaintMarginReq — Maintenance Margin of whole portfolio with no discounts or intraday credits
    #FullAvailableFunds — Available funds of whole portfolio with no discounts or intraday credits
    #FullExcessLiquidity — Excess liquidity of whole portfolio with no discounts or intraday credits
    #Leverage — GrossPositionValue / NetLiquidation
    if tag == '':
        myconn.reqAccountSummary(reqId, group, AccountSummaryTags.AllTags)
    else:
        myconn.reqAccountSummary(reqId, group, tag)
        
def reqPnL(reqId,account):
    global myconn
    myconn.reqPnL(reqId, account, "")

def resetPnL(reqId):
    global myconn
    myconn.cancelPnL(reqId)
    
def resetAccSummary(reqId):
    global myconn
    myconn.cancelAccountSummary(reqId)

##############################################
# Wrappers for contract details
##############################################    
def getContractDetails(reqId,contract):
    #reqContractDetails takes as an argument a Contract object which may uniquely match one contract, 
    #and unlike other API functions it can also take a Contract object which matches multiple contracts in IB's database. 
    #When there are multiple matches, they will each be returned individually to the function contractDetails.
    global buffer_cdet
    global myconn
    myconn.reqContractDetails(reqId, contract) 
    
    wait4data(reqId,buffer_cdet)

    ret=buffer_cdet[reqId]
    return ret
    
def resetBufferCdet(reqId):
    global buffer_cdet
    del buffer_cdet[reqId]

def getPriceIncremByMarketRule(mrule):
    global buffer_pincr
    global myconn
    myconn.reqMarketRule(int(mrule))
    
    wait4data(reqId,buffer_pincr)
    ret=buffer_pincr[int(mrule)]
    return ret

def resetPriceIncrem(mrule):
    global buffer_pincr
    del buffer_pincr[int(mrule)]
 
##############################################
# Wrappers for realtime market data 
##############################################
def getLastTradingPrice(reqId,contract,priceType='LastPrice'):
    # (tick-price,string,generic,size)
    global buffer_tickers
    global myconn
    myconn.reqMktData(reqId, contract, '', False, False, []) 
    
    wait4data(reqId,buffer_tickers,priceType)
    ret=buffer_tickers[reqId][priceType]
    return ret
    
def resetBufferTickers(reqId):
    global buffer_tickers
    global myconn
    myconn.cancelMktData(reqId)
    #del buffer_tickers[reqId]

def getTickByTickData(reqId,contract,ref=True,max_size=100000):
    #tickByTickBidAsk
    global myconn
    global buffer_tbtdata
    global max_bsize
    
    max_bsize=max_size
    #Additionally, if a non-zero value is input for the argument numberOfTicks reqTickByTickData, 
    #historical tick data is first returned to one of the functions 
    #historicalTicksLast, historicalTicksBidAsk, or historicalTicks
    
    #params: reqId,contract,tickType: "Last", "AllLast", "BidAsk" or "MidPoint"
    #numberOfTicks	- number of ticks, 
    #ignoreSize    
    #myconn.reqTickByTickData(reqId, contract, "Last", 0, True)
    #myconn.reqTickByTickData(reqId, contract, "AllLast", 0, False)
    myconn.reqTickByTickData(reqId, contract, "BidAsk", 0, True)
    #myconn.reqTickByTickData(reqId, contract, "MidPoint", 0, False)
    wait4data(reqId,buffer_tbtdata)
    if ref:
        ret=buffer_tbtdata[reqId].copy()   
    else:
        ret=buffer_tbtdata[reqId]
    return ret
    
def readTickByTickData(reqId):
    return buffer_tbtdata[reqId].copy() 

def lastTickByTickData(reqId):
    return (buffer_tbtdata[reqId][-1]).copy() 

def resetBufferTbTData(reqId):
    global buffer_tbtdata
    global myconn
    myconn.cancelTickByTickData(reqId)
    #del buffer_tbtdata[reqId]

##############################################
# Wrappers for option data 
##############################################
def getOptionChain(reqId,symbol='FISV',exchange='SMART',currency='USD'):
    global buffer_ochain
    global myconn
    
    contract = Contract()
    contract.symbol = symbol
    contract.secType = "OPT"
    contract.exchange = exchange
    contract.currency = currency
    
    ret=getContractDetails(reqId, contract)
    
    wait4data(reqId,buffer_cdet)
    
    df=pd.DataFrame(columns=['id','symbol','strike','type','expiration','currency','mul','exchange'])
    i=0
    for c in ib.buffer_cdet[reqId]['contracts']:
        if c=='END':break
        a=c.contract
        row=[a.conId,a.symbol,a.strike,a.right,a.lastTradeDateOrContractMonth,a.currency,a.multiplier,a.exchange]
        df.loc[i] = row
        i=i+1
    return df

##############################################
# Wrappers for fundamental data 
##############################################
def reqFundDataRepSnapshot(reqId,contract):
    global buffer_fdata
    global myconn
    
    myconn.reqFundamentalData(reqId,contract,'ReportSnapshot',[])
    wait4data(reqId,buffer_fdata)
    ret=buffer_fdata[reqId]
    return ret

def reqFundDataRepFinSumm(reqId,contract):
    global buffer_fdata
    global myconn
    
    myconn.reqFundamentalData(reqId,contract,'ReportsFinSummary',[])
    wait4data(reqId,buffer_fdata)
    ret=buffer_fdata[reqId]
    return ret

def reqFundDataRepRatios(reqId,contract):
    global buffer_fdata
    global myconn
    
    myconn.reqFundamentalData(reqId,contract,'ReportRatios',[])
    wait4data(reqId,buffer_fdata)
    ret=buffer_fdata[reqId]
    return ret

def reqFundDataRepFinStmts(reqId,contract):
    global buffer_fdata
    global myconn
    
    myconn.reqFundamentalData(reqId,contract,'ReportsFinStatements',[])
    wait4data(reqId,buffer_fdata)
    ret=buffer_fdata[reqId]
    return ret

def reqFundDataRepEstimates(reqId,contract):
    global buffer_fdata
    global myconn
    
    myconn.reqFundamentalData(reqId,contract,'RESC',[])
    wait4data(reqId,buffer_fdata)
    ret=buffer_fdata[reqId]
    return ret

def reqFundDataRepStockCal(reqId,contract):
    global buffer_fdata
    global myconn
    
    myconn.reqFundamentalData(reqId,contract,'CalendarReport',[])
    wait4data(reqId,buffer_fdata)
    ret=buffer_fdata[reqId]
    return ret
    
##############################################
# Wrappers for historical data
#1 secs, 5 secs, 10 secs, 15 secs, 30 secs, 1 min, 2 mins, 3 mins, 5 mins, 10 mins, 15 mins, 20 mins, 30 mins, 1 hour,
#2 hours, 3 hours, 4 hours, 8 hours, 1 day, 1W, 1M
##############################################
def getTickByTickHData(reqId,contract,stime='20200701 12:01:00',etime='20200701 13:01:00',numberOfTicks=1000):
    global buffer_htbtdata
    global myconn
    
    #requestId, id of the request
    #contract, Contract object that is subject of query.
    #startDateTime, i.e. "20170701 12:01:00". Uses TWS timezone specified at login.
    #endDateTime, i.e. "20170701 13:01:00". In TWS timezone. Exactly one of startDateTime or endDateTime must be defined.
    #numberOfTicks, Number of distinct data points. Max is 1000 per request.
    #whatToShow, (Bid_Ask, Midpoint, or Trades) Type of data requested.
    #useRth, Data from regular trading hours (1), or all available hours (0).
    #ignoreSize, Omit updates that reflect only changes in size, and not price. Applicable to Bid_Ask data requests.
    #miscOptions Should be defined as null; reserved for internal use.
    myconn.reqHistoricalTicks(reqId, contract,stime, etime, numberOfTicks, "BID_ASK", 1, True, [])  
    waitHdata(reqId,buffer_htbtdata,'Time')
    return buffer_htbtdata[reqId].copy()

def resetTbTBufferData(reqId):
    global buffer_htbtdata
    del buffer_htbtdata[reqId]
    
def get30MinData(reqId,contract,duration,bartype,reg_trad_hours=1):
    global buffer_hdata
    global myconn
    
    #parameters
    #tickerId, A unique identifier which will serve to identify the incoming data.
    #contract, The IBApi.Contract you are interested in.
    #endDateTime, The request's end date and time (the empty string indicates current present moment).
    #durationString, The amount of time to go back from the request's given end date and time.
    #barSizeSetting, The data's granularity or Valid Bar Sizes
    #whatToShow, The type of data to retrieve. See Historical Data Types
    #useRTH, Whether (1) or not (0) to retrieve data generated only within Regular Trading Hours (RTH)
    #formatDate, The format of the incoming bars' date. for day bars, only yyyyMMdd format is available.
    #keepUpToDate, return updates of unfinished real time bars as they are available (True). 
                    #If True, and endDateTime cannot be specified.
    myconn.reqHistoricalData(reqId, contract, '', duration, '30 mins', bartype, reg_trad_hours, 1, False, [])
    waitHdata(reqId,buffer_hdata)
    return buffer_hdata[reqId].copy()
    
def getHourlyData(reqId,contract,duration,bartype,reg_trad_hours=1):
    global buffer_hdata
    global myconn
    myconn.reqHistoricalData(reqId, contract, '', duration, '1 hour', bartype, reg_trad_hours, 1, False, [])
    waitHdata(reqId,buffer_hdata)
    return buffer_hdata[reqId].copy()
    
def get4HoursData(reqId,contract,duration,bartype,reg_trad_hours=1):
    global buffer_hdata
    global myconn
    myconn.reqHistoricalData(reqId, contract, '', duration, '4 hours', bartype, reg_trad_hours, 1, False, [])
    waitHdata(reqId,buffer_hdata)
    return buffer_hdata[reqId].copy()
        
def getDailyData(reqId,contract,duration,bartype,reg_trad_hours=1):
    global buffer_hdata
    global myconn
    myconn.reqHistoricalData(reqId, contract, '', duration, '1 day', bartype, reg_trad_hours, 1, False, [])
    waitHdata(reqId,buffer_hdata)
    
    ret=buffer_hdata[reqId].copy().set_index('Date').resample('D').mean().dropna()
    ret=ret[['Open','High','Low','Close','Adjusted_close','Volume']]
    ret=ret.reset_index()
    return ret
    
def getWeeklyData(reqId,contract,duration,bartype,reg_trad_hours=1):
    global buffer_hdata
    global myconn
    myconn.reqHistoricalData(reqId, contract, '', duration, '1W', bartype, reg_trad_hours, 1, False, [])
    waitHdata(reqId,buffer_hdata)
    return buffer_hdata[reqId].copy()
    
def getMonthlyData(reqId,contract,duration,bartype,reg_trad_hours=1):
    global buffer_hdata
    global myconn
    myconn.reqHistoricalData(reqId, contract, '', duration, '1M', bartype, reg_trad_hours, 1, False, [])
    waitHdata(reqId,buffer_hdata)
    return buffer_hdata[reqId].copy()

def resetBufferData(reqId):
    global buffer_hdata
    del buffer_hdata[reqId]

##############################################
# Wrappers for historical data simplified
##############################################
def getOptionHData(reqId,symbol='IWM',exchange='SMART',cur='USD',exp='20201231',otype='P',strike=184,duration='7 d'):
    contract = getOption(symbol=symbol,exchange=exchange,currency=cur,expiration=exp,otype=otype,strike=strike)
    ret=getHourlyData(reqId,contract,duration=duration,bartype='TRADES')
    ret2=ret.copy().set_index('Date').resample('D').mean().dropna()
    ret2=ret2[['Open','High','Low','Close','Adjusted_close','Volume']]
    return ret2

def getStockHData(reqId,symbol='IWM',exchange='SMART',cur='USD',duration='7 d'):
    contract = getStock(symbol=symbol,exchange=exchange,currency=cur)
    ret=getDailyData(reqId,contract,duration=duration,bartype='TRADES').set_index('Date')
    return ret
    
def getFutureHData(reqId,symbol='IWM',exchange='SMART',cur='USD',month='201903',duration='7 d'):
    contract = getFuture(symbol=symbol,exchange=exchange,currency=cur,month=month)
    ret=getDailyData(reqId,contract,duration=duration,bartype='TRADES').set_index('Date')
    return ret

def getIndexHData(reqId,symbol='TRIN',exchange='AMEX',cur='USD',duration='3 Y'):
    ic = getIndex(symbol=str(symbol) + '-' + str(exchange),exchange=exchange,currency=cur)
    df=getDailyData(reqId,ic,duration=duration,bartype='TRADES')
    ret2=df.copy().set_index('Date').resample('D').mean().dropna()
    ret2=ret2[['Open','High','Low','Close','Adjusted_close','Volume']]
    ret2=ret2.reset_index()
    return ret2
    
##############################################
# Wrappers for real contracts
##############################################
def getStock(symbol='AAPL',exchange='SMART',currency='USD'):
    contract = Contract()
    contract.symbol = symbol
    contract.secType = 'STK'
    contract.exchange = exchange
    contract.currency = currency
    #contract.primaryExchange = "ISLAND"
    return contract

def getStock(symbol='AAPL',exchange='SMART',pexchange='ISLAND',currency='USD'):
    contract = Contract()
    contract.symbol = symbol
    contract.secType = 'STK'
    contract.exchange = exchange
    contract.primaryExchange = pexchange
    contract.currency = currency
    return contract

def getCash(symbol='EUR',exchange='IDEALPRO',currency='USD'):
    contract = Contract()
    contract.symbol = symbol
    contract.secType = 'CASH'
    contract.exchange = exchange
    contract.currency = currency
    return contract

def getCommodity(symbol='XAUUSD',exchange='SMART',currency='USD'):
    contract = Contract()
    contract.symbol = symbol
    contract.secType = 'CMDTY'
    contract.exchange = exchange
    contract.currency = currency
    return contract

def getOption(symbol='COF',exchange='ISE',currency='USD',expiration='20190315',otype='P',strike=105,mult='100'):
    contract = Contract()
    contract.symbol = symbol
    contract.secType = 'OPT'
    contract.exchange = exchange
    contract.currency = currency
    contract.lastTradeDateOrContractMonth = expiration
    contract.right = otype
    contract.strike = strike
    contract.multiplier = mult
    return contract

def getFuture(symbol='ES',exchange='GLOBEX',currency='USD',month='201903'):
    contract = Contract()
    contract.symbol = symbol
    contract.secType = 'FUT'
    contract.exchange = exchange
    contract.currency = currency
    contract.lastTradeDateOrContractMonth = month
    return contract

def getFutureOption(symbol='ES',exchange='GLOBEX',currency='USD',month='20190315',strike=2900, otype='C',mult='50'):
    contract = Contract()
    contract.symbol = symbol
    contract.secType = 'FOP'
    contract.exchange = exchange
    contract.currency = currency
    contract.lastTradeDateOrContractMonth = month
    contract.strike = strike
    contract.right = otype
    contract.multiplier = mult
    return contract

def BondWithCusip(cusip='912828C57',exchange='SMART',currency='USD'):
        
    contract = Contract()
    contract.symbol= cusip
    contract.secType = "BOND"
    contract.exchange = exchange
    contract.currency = currency
    return contract

def Bond(conId=15960357,exchange='SMART'):
    #! [bond]
    contract = Contract()
    contract.conId = conId
    contract.exchange = exchange
    #! [bond]
    return contract

def MutualFund(symbol='VINIX',exchange='FUNDSERV',currency='USD'):
    #! [fundcontract]
    contract = Contract()
    contract.symbol = symbol
    contract.secType = "FUND"
    contract.exchange = exchange
    contract.currency = currency
    #! [fundcontract]
    return contract

##############################################
# Wrappers for fictious contracts
##############################################            
def getIndex(symbol='DAX',exchange='DTB',currency='EUR'):
    
    contract = Contract()
    contract.symbol = symbol
    contract.secType = 'IND'
    contract.exchange = exchange
    contract.currency = currency
    return contract

def getContFut(symbol='ES',exchange='GLOBEX'):
    contract = Contract()
    contract.symbol = symbol
    contract.secType = 'CONTFUT'
    contract.exchange = exchange
    return contract

def getContAndExpiringFut(symbol='ES',exchange='GLOBEX'):
    contract = Contract()
    contract.symbol = symbol
    contract.secType = 'FUT+CONTFUT'
    contract.exchange = exchange
    return contract
    
##############################################
# Wrappers for COMBO contracts
##############################################
def OptionComboContractManyLegs(symbol="DBK", currency="EUR",exchange="DTB",legs=[]):
    #! [bagoptcontract]
    contract = Contract()
    contract.symbol = symbol
    contract.secType = "BAG"
    contract.currency = currency
    contract.exchange = exchange
    
    contract.comboLegs = []
    
    i=1
    for n in range(len(legs)): 
        leg1 = ComboLeg()
        
        leg1.conId = legs['Leg'+str(i)]['cdetails']['conId'] #DBK JUN 21 2019 C
        leg1.ratio = legs['Leg'+str(i)]['qty']
        leg1.action = legs['Leg'+str(i)]['side']
        leg1.exchange = legs['Leg'+str(i)]['cdetails']['exch']
        contract.comboLegs.append(leg1)
        i=i+1

    #! [bagoptcontract]
    return contract

def VixComboContract(symbol="VIX", currency="USD",exchange="CFE",quantity=1, conId1=326501438,conId2=323072528):
    #CBOE Volatility Index Future combo contract
    
    #! [bagfutcontract]
    contract = Contract()
    contract.symbol = symbol
    contract.secType = "BAG"
    contract.currency = currency
    contract.exchange = exchange

    leg1 = ComboLeg()
    leg1.conId = conId1 # VIX FUT 201903
    leg1.ratio = quantity
    leg1.action = "BUY"
    leg1.exchange = exchange

    leg2 = ComboLeg()
    leg2.conId = conId2 # VIX FUT 2019049
    leg2.ratio = quantity
    leg2.action = "SELL"
    leg2.exchange = exchange

    contract.comboLegs = []
    contract.comboLegs.append(leg1)
    contract.comboLegs.append(leg2)
    #! [bagfutcontract]
    return contract

def SmartFutureComboContract(symbol="WTI", currency="USD",exchange="SMART",quantity=1, conId1=55928698,exchange1="IPE",conId2=55850663,exchange2="IPE"):
    # WTI,COIL spread. Symbol can be defined as first leg symbol ("WTI") or currency ("USD")
    #! [smartfuturespread]
    contract = Contract()
    contract.symbol = symbol
    contract.secType = "BAG"
    contract.currency = currency
    contract.exchange = exchange

    leg1 = ComboLeg()
    leg1.conId = conId1 # WTI future June 2017
    leg1.ratio = quantity
    leg1.action = "BUY"
    leg1.exchange = exchange1

    leg2 = ComboLeg()
    leg2.conId = conId2 # COIL future June 2017
    leg2.ratio = quantity
    leg2.action = "SELL"
    leg2.exchange = exchange2

    contract.comboLegs = []
    contract.comboLegs.append(leg1)
    contract.comboLegs.append(leg2)
    #! [smartfuturespread]
    return contract

def InterCmdtyFuturesContract(symbol="CL.BZ", currency="USD",exchange="NYMEX",quantity=1, conId1=47207310,exchange1="NYMEX",conId2=47195961,exchange2="NYMEX"):
    #! [intcmdfutcontract]
    contract = Contract()
    contract.symbol = symbol #symbol is 'local symbol' of intercommodity spread. 
    contract.secType = "BAG"
    contract.currency = currency
    contract.exchange = exchange

    leg1 = ComboLeg()
    leg1.conId = conId1 #CL Dec'16 @NYMEX
    leg1.ratio = quantity
    leg1.action = "BUY"
    leg1.exchange = exchange1

    leg2 = ComboLeg()
    leg2.conId = conId2 #BZ Dec'16 @NYMEX
    leg2.ratio = quantity
    leg2.action = "SELL"
    leg2.exchange = exchange2

    contract.comboLegs = []
    contract.comboLegs.append(leg1)
    contract.comboLegs.append(leg2)
    #! [intcmdfutcontract]
    return contract

##############################################
# Wrappers for orders
##############################################
def MarketOrder(action:str, quantity:float):    
    #A Market order is an order to buy or sell at the market bid or offer price. may increase the likelihood of a fill 
    #and the speed of execution, but unlike the Limit order a Market order provides no price protection and 
    #may fill at a   price far lower/higher than the current displayed bid/ask.
    #Products: BOND, CFD, EFP, CASH, FUND, FUT, FOP, OPT, STK, WAR   
    
    #! [market]
    order = Order()
    order.action = action
    order.orderType = "MKT"
    order.totalQuantity = quantity
    #! [market]
    return order
    
def LimitOrder(action:str, quantity:float, limitPrice:float):
    #A Limit order is an order to buy or sell at a specified price or better. it ensures that if the order fills, 
    #it will not fill at a price less favorable than your limit price, but it does not guarantee a fill.
    #Products: BOND, CFD, CASH, FUT, FOP, OPT, STK, WAR
    
    # ! [limitorder]
    order = Order()
    order.action = action
    order.orderType = "LMT"
    order.totalQuantity = quantity
    order.lmtPrice = limitPrice
    # ! [limitorder]
    return order

def Stop(action:str, quantity:float, stopPrice:float):
    #/ A Stop order is an instruction to submit a buy or sell market order if and when the user-specified stop trigger price
    #is attained or penetrated. A Stop order is not guaranteed a specific execution price and may execute significantly 
    #away from its stop price. A Sell Stop order is always placed below the current market price and is typically used to
    #limit a loss or protect a profit on a long stock position. A Buy Stop order is always placed above the current market
    #price. It is typically used to limit a loss or help protect a profit on a short sale.
    #/ Products: CFD, BAG, CASH, FUT, FOP, OPT, STK, WAR
    
    # ! [stop]
    order = Order()
    order.action = action
    order.orderType = "STP"
    order.auxPrice = stopPrice
    order.totalQuantity = quantity
    # ! [stop]
    return order
    
def StopLimit(action:str, quantity:float, limitPrice:float, stopPrice:float):
    #/ A Stop-Limit order is an instruction to submit a buy or sell limit order when the user-specified stop trigger price 
    #is attained or penetrated. The order has two basic components: the stop price and the limit price. When a trade has
    #occurred at or through the stop price, the order becomes executable and enters the market as a limit order, which is 
    #an order to buy or sell at a specified price or better.
    #/ Products: CFD, CASH, FUT, FOP, OPT, STK, WAR
    
    # ! [stoplimit]
    order = Order()
    order.action = action
    order.orderType = "STP LMT"
    order.totalQuantity = quantity
    order.lmtPrice = limitPrice
    order.auxPrice = stopPrice
    # ! [stoplimit]
    return order

def TrailingStop(action:str, quantity:float, trailingPercent:float,trailStopPrice:float):
    #/ A sell trailing stop order sets the stop price at a fixed amount below the market price with an attached "trailing"
    # amount. As the market price rises, the stop price rises by the trail amount, but if the stock price falls, the stop
    #loss price doesn't change, and a market order is submitted when the stop price is hit. This technique is designed to
    #allow an investor to specify a limit on the maximum possible loss, without setting a limit on the maximum possible gain.
    #"Buy" trailing stop orders are the mirror image of sell trailing stop orders, and are most appropriate for use in
    #falling markets.
    #/ Products: CFD, CASH, FOP, FUT, OPT, STK, WAR
    
    # ! [trailingstop]
    order = Order()
    order.action = action
    order.orderType = "TRAIL"
    order.totalQuantity = quantity
    order.trailingPercent = trailingPercent
    order.trailStopPrice = trailStopPrice
    # ! [trailingstop]
    return order
    
def TrailingStopLimit(action:str, quantity:float, lmtPriceOffset:float, trailingAmount:float, trailStopPrice:float):
    #/ A trailing stop limit order is designed to allow an investor to specify a limit on the maximum possible loss, 
    #without setting a limit on the maximum possible gain. A SELL trailing stop limit moves with the market price, and
    #continually recalculates the stop trigger price at a fixed amount below the market price, based on the user-defined
    #"trailing" amount. The limit order price is also continually recalculated based on the limit offset. As the market 
    #price rises, both the stop price and the limit price rise by the trail amount and limit offset respectively, but if 
    #the stock price falls, the stop price remains unchanged, and when the stop price is hit a limit order 
    #/is submitted at the last calculated limit price. A "Buy" trailing stop limit order is the mirror image of a 
    #sell trailing stop limit, and is generally used in falling markets.
    #/ Products: BOND, CFD, CASH, FUT, FOP, OPT, STK, WAR
    
    # ! [trailingstoplimit]
    order = Order()
    order.action = action
    order.orderType = "TRAIL LIMIT"
    order.totalQuantity = quantity
    order.trailStopPrice = trailStopPrice
    order.lmtPriceOffset = lmtPriceOffset
    order.auxPrice = trailingAmount
    # ! [trailingstoplimit]
    return order

##############################################
# Wrappers for combos
##############################################
def ComboMarketOrder(action:str, quantity:float, nonGuaranteed:bool):
    #Create combination orders that include options, stock and futures legs (stock legs can be included if the order 
    #is routed through SmartRouting). Although a combination/spread order is constructed of separate legs, 
    #it is executed as a single transaction if it is routed directly to an exchange. For combination orders that are
    #SmartRouted, each leg may be executed separately to ensure best execution.
    #Products: OPT, STK, FUT
    
    # ! [combomarket]
    order = Order()
    order.action = action
    order.orderType = "MKT"
    order.totalQuantity = quantity
    if nonGuaranteed:
    
        order.smartComboRoutingParams = []
        order.smartComboRoutingParams.append(TagValue("NonGuaranteed", "1"))
    
    # ! [combomarket]
    return order

def LimitOrderForComboWithLegPrices(action:str, quantity:float,legPrices:list, nonGuaranteed:bool):
    #/ Create combination orders that include options, stock and futures legs (stock legs can be included 
    #if the order is routed through SmartRouting). Although a combination/spread order is constructed of 
    #separate legs, it is executed as a single transaction if it is routed directly to an exchange. 
    #For combination orders that are SmartRouted, each leg may be executed separately to ensure best execution.
    #/ Products: OPT, STK, FUT
    
    # ! [limitordercombolegprices]
    order = Order()
    order.action = action
    order.orderType = "LMT"
    order.totalQuantity = quantity
    order.orderComboLegs = []
    
    for price in legPrices:
        comboLeg = OrderComboLeg()
        comboLeg.price = price
        order.orderComboLegs.append(comboLeg)
               
    if nonGuaranteed:
        order.smartComboRoutingParams = []
        order.smartComboRoutingParams.append(TagValue("NonGuaranteed", "1"))
    
    # ! [limitordercombolegprices]
    return order

##############################################
# Wrappers for orders on forex
##############################################       
def LimitOrderWithCashQty(action:str, quantity:float, limitPrice:float, cashQty:float):
    #Forex orders can be placed in demonination of second currency in pair using cashQty field
    #Requires TWS or IBG 963+
    #https://www.interactivebrokers.com/en/index.php?f=23876#963-02
    
    # ! [limitorderwithcashqty]
    order = Order()
    order.action = action
    order.orderType = "LMT"
    order.totalQuantity = quantity
    order.lmtPrice = limitPrice
    order.cashQty = cashQty
    # ! [limitorderwithcashqty]
    return order

def LimitIfTouched(action:str, quantity:float, limitPrice:float, triggerPrice:float):
    #/ A Limit if Touched is an order to buy (or sell) a contract at a specified price or better, below (or above) 
    #the market. This order is held in the system until the trigger price is touched. An LIT order is similar 
    #to a stop limit order, except that an LIT sell order is placed above the current market price, 
    #and a stop limit sell order is placed below.
    #/ Products: BOND, CFD, CASH, FUT, FOP, OPT, STK, WAR
                    
    # ! [limitiftouched]
    order = Order()
    order.action = action
    order.orderType = "LIT"
    order.totalQuantity = quantity
    order.lmtPrice = limitPrice
    order.auxPrice = triggerPrice
    # ! [limitiftouched]
    return order

def BracketOrder(parentOrderId:int, action:str, quantity:float, limitPrice:float, takeProfitLimitPrice:float, stopLossPrice:float):
    #Bracket orders are designed to help limit your loss and lock in a profit by "bracketing" an order 
    #with two opposite-side orders. A BUY order is bracketed by a high-side sell limit order and a low-side sell stop order
    #A SELL order is bracketed by a high-side buy stop order and a low side buy limit order.
    #/ Products: CFD, BAG, FOP, CASH, FUT, OPT, STK, WAR
    
    #This will be our main or "parent" order
    parent = Order()
    parent.orderId = parentOrderId
    parent.action = action
    parent.orderType = "LMT"
    parent.totalQuantity = quantity
    parent.lmtPrice = limitPrice
    #The parent and children orders will need this attribute set to False to prevent accidental executions.
    #The LAST CHILD will have it set to True, 
    parent.transmit = False

    takeProfit = Order()
    takeProfit.orderId = parent.orderId + 1
    takeProfit.action = "SELL" if action == "BUY" else "BUY"
    takeProfit.orderType = "LMT"
    takeProfit.totalQuantity = quantity
    takeProfit.lmtPrice = takeProfitLimitPrice
    takeProfit.parentId = parentOrderId
    takeProfit.transmit = False

    stopLoss = Order()
    stopLoss.orderId = parent.orderId + 2
    stopLoss.action = "SELL" if action == "BUY" else "BUY"
    stopLoss.orderType = "STP"
    #Stop trigger price
    stopLoss.auxPrice = stopLossPrice
    stopLoss.totalQuantity = quantity
    stopLoss.parentId = parentOrderId
    #In this case, the low side order will be the last child being sent. Therefore, it needs to set this attribute to True 
    #to activate all its predecessors
    stopLoss.transmit = True

    bracketOrder = [parent, takeProfit, stopLoss]
    return bracketOrder

##############################################
# Wrappers for orders on futures
##############################################    
def MarketWithProtection(action:str, quantity:float):
    #/ This order type is useful for futures traders using Globex. A Market with Protection order is a market order 
    #that will be cancelled and resubmitted as a limit order if the entire order does not immediately execute 
    #at the market price. The limit price is set by Globex to be close to the current market price, slightly higher 
    #for a sell order and lower for a buy order.
    #/ Products: FUT, FOP
    
    # ! [marketwithprotection]
    order = Order()
    order.action = action
    order.orderType = "MKT PRT"
    order.totalQuantity = quantity
    # ! [marketwithprotection]
    return order

def StopWithProtection(action:str, quantity:float, stopPrice:float):
    #/ A Stop with Protection order combines the functionality of a stop limit order with a market with protection order. 
    #The order is set to trigger at a specified stop price. When the stop price is penetrated, the order is triggered as a
    #market with protection order, which means that it will fill within a specified protected price range equal to the
    #trigger price +/- the exchange-defined protection porange:int. Any portion of the order that does not fill within this
    #protected range is submitted as a limit order at the exchange-defined 
    #/ trigger price +/- the protection points.
    #/ Products: FUT
    
    # ! [stopwithprotection]
    order = Order()
    order.totalQuantity = quantity
    order.action = action
    order.orderType = "STP PRT"
    order.auxPrice = stopPrice
    # ! [stopwithprotection]
    return order

##############################################
# Wrappers for orders on options
##############################################        
def Volatility(action:str, quantity:float, volatilityPercent:float, volatilityType:int):
    #/ Specific to US options, investors are able to create and enter Volatility-type orders for options and combinations
    #rather than price orders. Option traders may wish to trade and position for movements in the price of the option
    #determined by its implied volatility. Because implied volatility is a key determinant of the premium on an option,
    #traders position in specific contract months in an effort to take advantage of perceived changes in implied volatility
    #arising before, during or after earnings or when company specific or broad market volatility is predicted to change. 
    #In order to create a Volatility order, clients must first create a Volatility Trader page from the 
    #Trading Tools menu and as they enter option contracts, premiums will display in percentage terms rather than premium.
    #The buy/sell process is the same as for regular orders priced in premium terms except that the client can limit the
    # volatility level they are willing to pay or receive.
    #/ Products: FOP, OPT
    
    # ! [volatility]
    order = Order()
    order.action = action
    order.orderType = "VOL"
    order.totalQuantity = quantity
    order.volatility = volatilityPercent#Expressed in percentage (40%)
    order.volatilityType = volatilityType# 1=daily, 2=annual
    # ! [volatility]
    return order
    
    
    
    
    
    
    
    
    
    
