#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Order Manager 
#
# Module comprising helper functions to record transactions
#############################################################################################      


#Common Libraries
import numpy as np
import pandas as pd
import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import Plib.Ledger.Transactions as tr
import Plib.Ledger.AOrders as aor
import Plib.Ledger.EOrders as eor
import Plib.Brokers.IBapi as ib
import Plib.Portfolio.Measures as pm
import Plib.Portfolio.Analysis as pmp
import time

SLEEP_TIME=0.3
MAX_SLEEP=30

#import warnings
#warnings.filterwarnings("once")
DEF_VERBOSE_PRINT = 0
DEF_ORDER_CHECKS = 0

path = '/Plib/Ledger'
filedb = module_path + path + '/ledger.db'
filedbBT = module_path + path + '/ledgerBT.db'
app=None
brkr=0
exch='SMART'
cur='USD'
clnt=0
simulator=0


##############################################
# Create Database and Test setup
##############################################    
def setupTestDb(db=''):
    global filedb
    
    if db=='':
        db=filedb
    else:
        filedb=filedb.replace('ledger.db',str(db)+'.db')
        
    tr.createDB(db,force=True)
    r=tr.printTable('EXCHS')
    r=tr.printTable('BRKS')
    r=tr.printTable('CTRS')
    
##############################################
# Setup broker, exchange, and ledger
##############################################  
def enableOM(broker='TWS',exchange=1,db='',cli=100,in_mem=False):
    global app
    global brkr
    global file_db
    global filedbBT
    global clnt
    global simulator
    

    if simulator >0:
        #global vars
        brkr=1
        clnt=cli
        
        if db=='': db=filedbBT
        IBID=simulator
    else:
        #Broker init
        if broker=='TWS' or broker=='IBG':
            port=ib.IBConn[broker][1]
            ip=ib.IBConn[broker][2]
            client=ib.IBConn[broker][0]
        
            #global vars
            brkr=1
            clnt=client
            
            if db=='': db=filedb
            app=ib.ibConnect(port=port,ip=ip,client_id=client,db=db)
            time.sleep(SLEEP_TIME)
            while app.nextorderId == None:
                time.sleep(SLEEP_TIME)
            IBID=int(app.nextorderId)-1
            
    params=[IBID,clnt]    
    aor.setParams(db,brkr,exchange,clnt,in_mem)
    eor.setParams(db,brkr,exchange,clnt,in_mem)
    tr.startLedger(brkr,exchange,db,params,in_mem)
    
    print('Connected with IB Order Manager with parameters: ',clnt,db)
    
    #Broker post init
    if broker=='TWS' or broker=='IBG':
        setExchangeIB(exchange)
        setCurrencyIB(exchange)

def disableOM():
    global brkr
    global simulator
    
    if brkr==1:
        if simulator >0:
            pass
        else:
            app.disconnect()    

def printLedger(db=filedb):
    print('Executed orders: ')
    tr.printTable('EOS')
    print()
    print('Inserted orders (not executed): ')
    tr.printTable('AOS')
    print()
    print('Inserted orders (not accepted): ')
    tr.printTable('RORDS')

def getAssetPrices(SP_tickers,index_ticker,dt_start, dt_end, scomp=[], broker=False):
    import Plib.DataFarm.IEXdata as datafarm1
    
    global clnt
    global exch
    global cur
    
    #first_ticker=SP_tickers[0]
    symbol = index_ticker
    
    if not broker:
        df = datafarm1.get_eod_data(symbol, dt_start, dt_end)
    else:
        df=ib.getStockHData(tr.getCodedId(scomp.StratId[0],0,clnt),symbol=symbol,exchange=exch,cur=cur,duration=ib.getIBDuration(dt_start, dt_end))
    df = df.sort_index(ascending=True)
    df=df.drop(df.columns[[0, 1, 2, 3,5,6,7]], axis=1)
    df.columns = [symbol]
    spy_comp=df
    counter=1
    
    for x in SP_tickers:
        symbol=x
        
        print(symbol)
        if symbol != index_ticker: 
            try:
                if not broker:
                    df = datafarm1.get_eod_data(symbol, dt_start, dt_end)
                else:
                    if scomp.SecType[counter-1]=='STK':
                        df=ib.getStockHData(tr.getCodedId(scomp.StratId[counter-1],counter,clnt),symbol=symbol,exchange=exch,cur=cur,duration=ib.getIBDuration(dt_start, dt_end))
                    elif scomp.SecType[counter-1]=='FUT':
                        df=ib.getFutureHData(tr.getCodedId(scomp.StratId[counter-1],counter,clnt),symbol=symbol,exchange=exch,cur=cur,month=scomp.Exp[counter-1],duration=ib.getIBDuration(dt_start, dt_end))
                    elif scomp.SecType[counter-1]=='OPT':
                        df=ib.getOptionHData(tr.getCodedId(scomp.StratId[counter-1],counter,clnt),symbol=symbol,exchange=exch,cur=cur,exp=scomp.Exp[counter-1],otype=scomp.PC[counter-1],strike=scomp.Strike[counter-1],duration=ib.getIBDuration(dt_start, dt_end))    
                    else:
                        return 0
                    df = df.sort_index(ascending=True)
                df=df.drop(df.columns[[0, 1, 2, 3,5,6,7]], axis=1)
                if symbol in spy_comp.columns.values:
                    df.columns = [symbol+str(counter)]
                    spy_comp[symbol+str(counter)]=df
                else:
                    df.columns = [symbol]
                    spy_comp[symbol]=df
                counter=counter+1
                print('Security: ', counter,symbol)
                #if symbol=='AAPL': break;
            except:
                print('Security not available: ', counter,symbol)
    
    cols_at_end = [index_ticker]
    df = spy_comp[[c for c in spy_comp if c not in cols_at_end] 
            + [c for c in cols_at_end if c in spy_comp]]
    
    df['Date'] = pd.to_datetime(df.index)
    df.set_index('Date',inplace=True)
    df=df.fillna(method='ffill').fillna(method='bfill')
    return df

def printStatsBySid(sid,index_symbol='SPY',MAR=0.01,days=15,freq=252, ConfLev=0.95,useBroker=True):
    #for hours freq = 252×6.5 
    from time import gmtime, strftime
    from datetime import timedelta, date
    from dateutil import parser
    import numpy as np
    
    #daily data
    today_date=strftime("%Y-%m-%d %H:%M:%S", gmtime())
    dt_start = (parser.parse(today_date) + timedelta(days=-days*2)).strftime("%Y-%m-%d")
    dt_end = (parser.parse(today_date)).strftime("%Y-%m-%d")
    
    temp=tr.getStratEO(sid)
    scomp=pd.DataFrame(data=temp,columns=['StratId', 'AOBrkID', 'Symbol', 'Qty', 'Price', 'TotP','SType','SecType','OSide','Exp','PC', 'Strike' ])
    scomp.StratId=scomp.StratId.astype('int')
    scomp.AOBrkID=scomp.AOBrkID.astype('int')
    scomp.Symbol=scomp.Symbol.astype('str')
    scomp.Qty=scomp.Qty.astype('float')
    scomp.Price=scomp.Price.astype('float')
    scomp.TotP=scomp.TotP.astype('float')
    scomp.SType=scomp.SType.astype('str')
    scomp.SecType=scomp.SecType.astype('str')
    scomp.OSide=scomp.OSide.astype('str')
    scomp.Exp=scomp.Exp.astype('str')
    scomp.PC=scomp.PC.astype('str')
    scomp.Strike=scomp.Strike.astype('float')
    
    #Check if combo
    if scomp.SecType[0]=='CMB':
        scomp=scomp.tail(len(scomp)-1)
        scomp=scomp.reset_index()
        
    strategy_tickers=scomp.Symbol
    
    prices=getAssetPrices(strategy_tickers,index_symbol,dt_start, dt_end,scomp,useBroker)
    
    #Check if option 
    scomp['Qty'] = np.where(scomp['OSide'] == 'BUY', scomp['Qty'],scomp['Qty']*-1)
    scomp['TotP'] = np.where(scomp['OSide'] == 'BUY', scomp['TotP'],scomp['TotP']*-1)
    scomp['Qty'] = np.where(scomp['SecType'] == 'OPT', scomp['Qty'] * 100,scomp['Qty'])
    scomp['TotP'] = np.where(scomp['SecType'] == 'OPT', scomp['TotP'] * 100,scomp['TotP'])
    
    #Compute strategy price and returns
    weights=scomp['Qty'].values
    #MAR=-0.001
    #days=15
    #freq=252
    #ConfLev=0.95
    
    pprices=pm.getPortPrices(prices.iloc[:, :(prices.shape[1] - 1)],weights) 
    pival=scomp['TotP'].sum()
    pprices['prices'][0]=pival
    preturns=pm.getPortReturns(prices.iloc[:, :(prices.shape[1] - 1)],weights)
    
    st1=pmp.getStats(pprices,preturns,days,MAR,ConfLev,freq)
    
    #print(df8)
    
    return st1,st2

##############################################
# Live Market Data
##############################################    
def startStockTbTMarketData(reqId=100,symbol='AAPL',exchange='SMART',currency='USD',ref=True):
    return ib.getTickByTickData(reqId,ib.getStock(symbol,exchange,currency),ref)

def readTickByTickData(reqId):
    return ib.readTickByTickData[reqId]

def lastTickByTickData(reqId):
    return ib.lastTickByTickData[reqId] 
    
##############################################
# helper functions for IB orders
##############################################    
def getPriceChecks(nextID,contract,price,minTick=0.01):
    mprice=ib.getLastTradingPrice(nextID,contract)
    ib.resetBufferTickers(nextID)
    if mprice != 'ERROR':
        print('got market price')
        if float(mprice) !=0:
            if abs(price-float(mprice))/float(mprice) >= 0.03:
                price=float(mprice)
        else:
            price=float(mprice)
        ib.resetBufferTickers(nextID)
    else:
        print('no market price')
    if minTick==0:
        cdet=ib.getContractDetails(nextID,contract)
        if float(cdet['minTick'])==0:
            minTick=0.01
        else:
            minTick=float(cdet['minTick'])
    price=minTick * round(price/minTick)
    return price

# Order id can be any number. By default we assign the app next order id
def getNewOrderId():
    global simulator
    
    if simulator >0:
        simulator= simulator+1
        return simulator,str(simulator),[str(simulator),0]
        
    while app.nextorderId == None:
        time.sleep(SLEEP_TIME)
    nextID=app.nextorderId
    uid=str(nextID)
    params=[uid,0]
    return nextID,uid,params

def setExchangeIB(id_exch):
    global exch
    
    exch=tr.getExchName2ById(id_exch)

def setCurrencyIB(id_exch,symbol='USD'):
    global cur
    
    if str(tr.getExchName2ById(id_exch))=='IDEALPRO':
        cur=symbol
    else:
        cur=tr.getCurrency(id_exch)
  
##############################################
# Create and manage IB orders
    # Write on AORDS,STRGS,CTRS
    # Delete on AORDS
##############################################    
def delOrderIB(uid):
    global app
    
    #Remove only orders without filled partial orders
    r=aor.getAOByBrkUID(str(uid))
    if len(r)>0:
        for i in range(len(r)-1):
            aoid=r[i][0] 
            aor.delOrder(aoid)
        app.cancelOrder(str(uid))

def placeOrderIB(sid,symbol, qty, o_side, trade,price=0,expiration='',strike=0,otype='',odate='',scomp_type='O'):
    global app
    global exch
    global cur
    global simulator
    
    #Get new order id
    nextID,uid,params=getNewOrderId()
    sec_type=''
    #params[1] is zero by default -> orders spec -> limit =1, bracket=2, SL =3, TP=4,combo=5,leg=6
    #params[1] is zero by default
    
    
    ##AVAILABLE CONTRACTS
    #'STK' (0-1),'CASH','CMDTY','OPT' (2-3),'FUT' (4-5),'FOP','BOND','FUND'
    if trade==0:
        sec_type='STK'
        contract = ib.getStock(symbol=symbol,exchange=exch,currency=cur)
        order = ib.MarketOrder(o_side,qty)
    elif trade==1:
        sec_type='STK'
        contract = ib.getStock(symbol=symbol,exchange=exch,currency=cur)
        if DEF_ORDER_CHECKS > 0:
            price=getPriceChecks(nextID,contract,price)
        order = ib.LimitOrder(o_side,qty,price)
    elif trade==2:
        sec_type='OPT'
        contract = ib.getOption(symbol=symbol,exchange=exch,currency=cur,expiration=expiration,otype=otype,strike=strike)
        order = ib.MarketOrder(o_side,qty)
    elif trade==3:
        sec_type='OPT'
        contract = ib.getOption(symbol=symbol,exchange=exch,currency=cur,expiration=expiration,otype=otype,strike=strike)
        if DEF_ORDER_CHECKS > 0:
            price=getPriceChecks(nextID,contract,price)
        order = ib.LimitOrder(o_side,qty,price)
    elif trade==4:    
        sec_type='FUT'
        contract = ib.getFuture(symbol=symbol,exchange=exch,currency=cur,month=expiration)
        order = ib.MarketOrder(o_side,qty)
    elif trade==5:    
        sec_type='FUT'
        contract = ib.getFuture(symbol=symbol,exchange=exch,currency=cur,month=expiration)
        if DEF_ORDER_CHECKS > 0:
            price=getPriceChecks(nextID,contract,price)
        order = ib.LimitOrder(o_side,qty,price)   
    
    #Submit orders
    aoid=aor.createAOrder(sid,symbol, price, qty, o_side, sec_type,expiration,otype,strike,params,scomp_type)
    if simulator >0:
        eoid=eor.createEOrder(sid,aoid,'','',str(price),qty,str('sotype'),str('STK'),odate,comm=0,slipg=0)
        pass
    else:
        app.placeOrder(nextID, contract, order)
        #Save next order
        nextID=app.nextValidId(nextID+1)
    return aoid,uid
    
def placeComboOrderIB(sid,symbol, trade,legs,bag_exch='',scomp_type='O'):
    global app
    global exch
    global cur
    global clnt
    global simulator
    
    #Get new order id
    nextID,uid,params=getNewOrderId()
    #params[1] is zero by default -> orders spec -> limit =1, bracket=2, SL =3, TP=4,combo=5,leg=6
    #params[1] is zero by default
    baseid=tr.getCodedId(sid,nextID)
    if bag_exch=='':
        bag_exch=exch
    
    lprices=list()
    i=1
    for n in range(len(legs)):
        #Obtain combo contracts and legs
        contract1 = ib.getOption(symbol=symbol,exchange=exch,currency=cur,expiration=legs['Leg'+str(i)]['exp'],
                            otype=legs['Leg'+str(i)]['opt_type'],strike=legs['Leg'+str(i)]['strike'])
        cdet1=ib.getContractDetails(baseid+i,contract1)
        legs['Leg'+str(i)]['contract']=contract1
        legs['Leg'+str(i)]['cdetails']=cdet1
        if trade==3:
            price1=legs['Leg'+str(i)]['price']
            if DEF_ORDER_CHECKS > 0:
                price1=getPriceChecks(baseid+i,contract1,price1)
                legs['Leg'+str(i)]['price']=price1
                lprices.append(price1)
        i=i+1

    comboCont= ib.OptionComboContractManyLegs(symbol=symbol, currency=cur,exchange=bag_exch,legs=legs)
    
    if trade==2:
        comboOrder=ib.ComboMarketOrder('BUY', 1, False)
    elif trade==3:
        comboOrder=ib.LimitOrderForComboWithLegPrices('BUY', 1, lprices, False)
    
    #Submit orders
    lret=list()
    i=1
    #Fake AOrder for combo
    aor.createAOrder(sid,symbol, legs['Leg'+str(i)]['price'], legs['Leg'+str(i)]['qty'], 
                                    legs['Leg'+str(i)]['side'], 'CMB','expiration','right',0,params,scomp_type)
                                    
    for n in range(len(legs)):
        aoid1=aor.createAOrder(sid,symbol, legs['Leg'+str(i)]['price'], legs['Leg'+str(i)]['qty'], 
                                legs['Leg'+str(i)]['side'], legs['Leg'+str(i)]['sec_type'],legs['Leg'+str(i)]['exp'],legs['Leg'+str(i)]['opt_type'],legs['Leg'+str(i)]['strike'],params,scomp_type)
        lret.append(aoid1)
        i=i+1
    
    if simulator >0:
        pass
    else:    
        app.placeOrder(nextID, comboCont, comboOrder)
        #Save next order
        nextID=app.nextValidId(nextID+1)
    return uid,lret
    
##############################################
# Delete All open orders by Strategy
# Write on AORDS,STRGS,CTRS
# Delete on AORDS
##############################################    
def deleteAllOpenOrders(sid,sec):
    r=aor.getAOBySid(sid,sec)
    recs=len(r)
    if recs>0:
        #open orders
        #AOrderId, AOBrkID, Symbol, Side, Qty, Price, PT, SL,AOTs,sect
        for k in range(0,recs):
            delOrderIB(r[k][1])
                
##############################################
# Close the net owned positions by Strategy
# Write on AORDS,STRGS,CTRS
# Delete on AORDS
##############################################    
def getNetOwnedPositions(sid,sec):
    r=tr.getNetEOBySid(sid,sec)
    recs=len(r)
    return r,recs

def getLastTrade(sid,sec):
    r= aor.getLastTradeBySid(sid,sec)
    recs=len(r)
    return r,recs
    
##############################################
# Create and manage simple strategies
##############################################        
def openStrategy(str_name1='One stock long at market price',str_name2=''):
    global brkr
    
    trade=0
    sid=tr.createStr(str_name1,str_name1)
    return sid

def strM1Stock(str_name1='One stock long at market price',str_name2='',symbol='AAPL',qty=100,o_side='BUY'):
    global brkr
    
    trade=0
    sid=tr.createStr(str_name1,str_name1)
    
    aoid,uid=placeOrderIB(sid,symbol, qty, o_side,trade)
    return sid,aoid,uid
    
def strL1Stock(str_name1='One stock long at limit price',str_name2='',symbol='AAPL',qty=100,price=110.5,o_side='BUY'):
    global brkr
    
    trade=1
    sid=tr.createStr(str_name1,str_name1)
    
    aoid,uid=placeOrderIB(sid,symbol, qty, o_side, trade,price)
    return sid,aoid,uid
     
def strM1Option(str_name1='One option long at market price',str_name2='',symbol='AAPL',qty=1,o_side='BUY',expiration='20190315',otype='P',strike=105):
    global brkr
    
    trade=3
    sid=tr.createStr(str_name1,str_name1)
    
    aoid,uid=placeOrderIB(str_name1,str_name2,symbol, qty, o_side,trade)
    return sid,aoid,uid
     
def strL1Option(str_name1='One option long at limit price',str_name2='',symbol='AAPL',qty=1,o_side='BUY',expiration='20190315',otype='P',strike=105,price=200):
    global brkr
    
    trade=4
    sid=tr.createStr(str_name1,str_name1)
    
    aoid,uid=placeOrderIB(str_name1,str_name2,symbol, qty, o_side,trade,price)
    return sid,aoid,uid
    
def strM1FOption(str_name1='One option long at market price',str_name2='',symbol='AAPL',qty=1,o_side='BUY',expiration='20190315',otype='P',strike=105):
    global brkr
    
    trade=4
    sid=tr.createStr(str_name1,str_name1)
    
    aoid,uid=placeOrderIB(str_name1,str_name2,symbol, qty, o_side,trade)
    return sid,aoid,uid 

def strL1FOption(str_name1='One option long at limit price',str_name2='',symbol='AAPL',qty=1,o_side='BUY',expiration='20190315',otype='P',strike=105,price=200):
    global brkr
    
    trade=5
    sid=tr.createStr(str_name1,str_name1)
    
    aoid,uid=placeOrderIB(str_name1,str_name2,symbol, qty, o_side,trade,price)
    return sid,aoid,uid
    
##############################################
# Create and manage order entries 
# for principal component analysis    
##############################################    
def getEigenPort(asset_prices,ep1_long,ep1_short,str_name1='PCA Long-Short',str_name2='',mmul=3,limit=0.01):
    global brkr
    
    sid=tr.createStr(str_name1,str_name2)
    
    EP=pd.DataFrame(asset_prices.iloc[-1])
    EP=EP.reset_index()
    EP.columns=['symbol','price']

    EP_long = pd.merge(EP, ep1_long, how='inner', on=['symbol'])
    EP_long['qty']=round(mmul*EP_long.price.max()/EP_long['price'],0)
    EP_short = pd.merge(EP, ep1_short, how='inner', on=['symbol'])
    EP_short['qty']=round(mmul*EP_short.price.max()/EP_short['price'],0)
    
    ##AVAILABLE CONTRACTS
    #'STK' (0-1),'CASH','CMDTY','OPT' (2-3),'FUT' (4-5),'FOP','BOND','FUND'
    i=0
    for i in range(len(EP_long)-1):
        if limit==0:
            trade=0
            price=0
            placeOrderIB(sid,EP_long.symbol[i], EP_long.qty[i], 'BUY', trade,price=0,expiration='',strike=0,otype='C')
        else:
            trade=1
            price=round(float(EP_long.price[i]*(1+limit)),2)
            placeOrderIB(sid,EP_long.symbol[i], EP_long.qty[i], 'BUY', trade,price=price,expiration='',strike=0,otype='C')
        i=i+1

    i=0
    for i in range(len(EP_short)-1):
        if limit==0:
            trade=0
            price=0
            placeOrderIB(sid,EP_short.symbol[i], EP_short.qty[i], 'SELL', trade,price=0,expiration='',strike=0,otype='C')
        else:
            trade=1
            price=round(float(EP_short.price[i]*(1+limit)),2)
            placeOrderIB(sid,EP_short.symbol[i], EP_short.qty[i], 'SELL', trade,price=price,expiration='',strike=0,otype='C')
        i=i+1
         
##############################################
# Create and manage options strategies
# fLeg=tuple(fexp,fstrike,ftype,fcallp)    
##############################################    
def getCalendarSpread(str_name1='Calendar Spread',str_name2='',qty=1,symbol='AAPL',fLeg=(),bLeg=(),LongShort=1,o_type='CALL',flimit=0.01,blimit=0.01,combo=False):
    
    if LongShort==1:
        str_name1='Long ' + str(o_type) + ' ' + str_name1
        oside=('BUY','SELL')
    elif LongShort==-1:
        str_name1='Short ' + str(o_type) + ' ' + str_name1
        oside=('SELL','BUY')
    
    ##AVAILABLE CONTRACTS
    #'STK' (0-1),'CASH','CMDTY','OPT' (2-3),'FUT' (4-5),'FOP','BOND','FUND'
    if blimit==0:
        trade=2
        bprice=0
    else:
        trade=3
        bprice=round(float(bLeg[3]*(1+blimit)),2)
    
    if flimit==0:
        trade=2
        fprice=0
    else:
        trade=3
        fprice=round(float(fLeg[3]*(1+flimit)),2)
         
    #Strategy = LongShort*bcall - LongShort*fcall    
    if not combo:
        sid=tr.createStr(str_name1,str_name1,'SINGLE')
        l1aoid,l1uid=placeOrderIB(sid,symbol,qty,oside[0],trade,bprice,bLeg[0].strftime("%Y%m%d"),bLeg[1],bLeg[2].upper())
        l2aoid,l2uid=placeOrderIB(sid,symbol,qty,oside[1],trade,fprice,fLeg[0].strftime("%Y%m%d"),fLeg[1],fLeg[2].upper())
        ret={'Leg1':{'aoid':l1aoid,'uid':l1uid},'Leg2':{'aoid':l2aoid,'uid':l2uid}}
    else:
        sid=tr.createStr(str_name1,str_name1,'COMBO')
        leg1={'side':oside[0],'price':bprice,'exp':bLeg[0].strftime("%Y%m%d"),'strike':bLeg[1],'opt_type':bLeg[2].upper(),'qty':qty,'sec_type':'OPT'}
        leg2={'side':oside[1],'price':fprice,'exp':fLeg[0].strftime("%Y%m%d"),'strike':fLeg[1],'opt_type':fLeg[2].upper(),'qty':qty,'sec_type':'OPT'}
        Legs={'Leg1':leg1,'Leg2':leg2}
        uid,lret=placeComboOrderIB(sid,symbol, trade,Legs)
        ret={}
        for i in range(len(lret)):
            leg={}
            leg['aoid']=lret[i]
            leg['uid']=uid
            ret['Leg'+str(i+1)]=leg
    return ret
    
    
    
    
    
    
    
    
    
    