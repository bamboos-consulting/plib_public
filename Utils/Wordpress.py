#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Wordpress 
#
# Module to publish on wordpress websites
#############################################################################################      

from wordpress_xmlrpc import Client
from wordpress_xmlrpc import WordPressPost
from wordpress_xmlrpc.compat import xmlrpc_client
from wordpress_xmlrpc.methods import media, posts


def wpPublish():
    bc = Client('http://www.bamboos-consulting.com/BamboosLtd/xmlrpc.php', 'roberto', 'pYi9xbHHeA7e')

    # set to the path to your file
    filename = 'test.jpg'
    # prepare metadata
    data = {
        'name': 'picture.jpg',
        'type': 'image/jpeg',  # mimetype
    }
    # read the binary file and let the XMLRPC library encode it into base64
    with open(filename, 'rb') as img:
        data['bits'] = xmlrpc_client.Binary(img.read())

    response = bc.call(media.UploadFile(data))
    # response == {
    #       'id': 6,
    #       'file': 'picture.jpg'
    #       'url': 'http://www.example.com/wp-content/uploads/2012/04/16/picture.jpg',
    #       'type': 'image/jpeg',
    # }
    attachment_id = response['id']

    test_html = "<!-- wp:quote --> <blockquote class=\"wp-block-quote\"><p>try</p></blockquote> <!-- /wp:quote --> <!-- wp:heading {\"level\":4} --> <h4>jkhkhk</h4><!-- /wp:heading --><!-- wp:paragraph --><p>jkhkkjhkh</p><!-- /wp:paragraph -->"
    post_content = bleach.clean(test_html)
    post_content = post_content.replace('&lt;', '<')
    post_content = post_content.replace('&gt;', '>')

    post = WordPressPost()
    post.title = 'MY POST TITLE'
    post.slug = 'MY-POST-TITLE'
    post.content = post_content
    # post.content = 'my content'
    post.excerpt = 'excerpt'
    post.thumbnail = attachment_id
    post.id = bc.call(posts.NewPost(post))
    post.post_status = 'publish'
    post.terms_names = {
        'post_tag': ['Construction', 'REITS'],
        'category': ['Investment', 'Trading', 'Uncategorized', 'Python'],
    }
    post.custom_fields = []
    post.custom_fields.append({
        'key': '_yoast_wpseo_focuskw',
        'value': 'my keyword'
    })
    post.custom_fields.append({
        'key': '_yoast_wpseo_metadesc',
        'value': 'my description'
    })

    bc.call(posts.EditPost(post.id, post))
