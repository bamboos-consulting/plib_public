#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Calendar 
#
# Module including helper functions to synch with google calendar
#############################################################################################      

from __future__ import print_function
import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

import pandas as pd
from datetime import timedelta
import time

from pathlib import Path
import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')
import Plib.Utils.Tools as tls
import Plib.Utils.DbAccess as adb
import Plib.DataFarm.Finnhub as fh
import Plib.DataFarm.Nasdaq as nq

import warnings
warnings.filterwarnings("once")

DEF_VERBOSE_PRINT=1
import Plib.Keystore as kst
ks=kst.Keystore()
path = ks.cal_path
filesql = module_path + path + '/calendar.sql'
db_f = module_path + path + '/calendar.db'
bcli=0
# If modifying these scopes, delete the file token.pickle.
SCOPES = ks.cal_SCOPES
cred_file1 = Path(module_path + '/' + ks.cal_cred_file1)
cred_file2 = Path(module_path + '/' + ks.cal_cred_file2)

##############################################
# Plib Calendar Database
##############################################         
def createDB(force=False):
    global db_f
    global filesql
    
    adb.setParams(db_f,bcli)
    
    if os.path.isfile(db_f) and not force:
        print('Db already exists, use force=True to erase.')
    else:
        conn=adb.cn(db_f)
        cursor = conn.cursor()
        sql_file = open(filesql)
        sql_as_string = sql_file.read()
        cursor.executescript(sql_as_string)
        cursor.close()
        conn.close()
        setupTables()

def setupTables(cli=0):
    adb.simple_insert('CTRS',{
        'id': 1,
        'cli': int(cli),
        'name': str('IPOS'),
        'value': 0})

    adb.simple_insert('CTRS',{
        'id': 2,
        'cli': int(cli),
        'name': str('EARN'),
        'value': 0})

    adb.simple_insert('CTRS',{
        'id': 3,
        'cli': int(cli),
        'name': str('ECON'),
        'value': 0})

def getNextId(table=''):
    global bcli
    
    if table=='':
        q='select count(*)+1 as id from CTRS where cli=' + str(bcli)
    else:
        q="select value+1 as id from CTRS where name='" + table + "' and cli=" + str(bcli)
    r=adb.getRs(q)
    if len(r)==0:
        id=1
    else:
        id=r[0][0] 
    return int(id)

def updateNextId(id,table=''):
    table_name='CTRS' 
    row={'value': int(id),'name':str(table)}
    adb.simple_update(table_name,row)
     
def insertEvent(tbl,gid,summ,loc,descr,sdate,stz,edate,etz,alarm):
    id=int(getNextId(table=tbl))
    adb.simple_insert('IPOS',{
        'id': int(id),
        'g_id': str(gid),
        'summary': str(summ),
        'location': str(loc),
        'description': str(descr),
        'sdate': str(sdate),
        'stz': str(stz),
        'edate': str(edate),
        'etz': str(etz),
        'alarm': str(alarm),
        })
    adb.simple_update2('CTRS' ,{'value': int(id),'name':str(tbl)})
    
##############################################
# Google Calendar APIs
##############################################         
def googleAuth():
    """Shows basic usage of the Google Calendar API.
    Prints the start and name of the next 10 events on the user's calendar.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if cred_file1.is_file():
        with open(module_path + '/token.pickle', 'rb') as token:
            creds = pickle.load(token)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open(module_path + '/token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('calendar', 'v3', credentials=creds)
    
    adb.setParams(db_f,bcli)
    return service

def getCalendars(service):
    # Call the Calendar API
    print('Getting list of calendars')
    calendars_result = service.calendarList().list().execute()

    calendars = calendars_result.get('items', [])

    if not calendars:
        if DEF_VERBOSE_PRINT>0:
            print('No calendars found.')
    for calendar in calendars:
        summary = calendar['summary']
        id = calendar['id']
        primary = "Primary" if calendar.get('primary') else ""
        if DEF_VERBOSE_PRINT>0:
            print("%s\t%s\t%s" % (summary, id, primary))
        
def getEvents(service, cal_name='IPOs'):
    # Call the Calendar API
    now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
    print('Getting the upcoming 10 events')
    events_result = service.events().list(calendarId='primary', timeMin=now,
                                        maxResults=10, singleEvents=True,
                                        orderBy='startTime').execute()
    events = events_result.get('items', [])

    if not events:
        if DEF_VERBOSE_PRINT>0:
            print('No upcoming events found.')
    if DEF_VERBOSE_PRINT>0:
        for event in events:
            start = event['start'].get('dateTime', event['start'].get('date'))
            print(start, event['summary'])
    return events

def createEvent(service,event):
    from datetime import datetime, timedelta
    
    d = datetime.now().date()
    tomorrow = datetime(d.year, d.month, d.day, 10)+timedelta(days=1)
    start = tomorrow.isoformat()
    end = (tomorrow + timedelta(hours=1)).isoformat()

    event_result = service.events().insert(calendarId='primary',
       body=event
    ).execute()

    if DEF_VERBOSE_PRINT>0:        
        print("created event")
        print("id: ", event_result['id'])
        print("summary: ", event_result['summary'])
        print("starts at: ", event_result['start']['dateTime'])
        print("ends at: ", event_result['end']['dateTime'])
    return event_result['id']

def delEvent(service,id,tbl):
    import googleapiclient
    try:
        service.events().delete(
           calendarId='primary',
           eventId=id,
        ).execute()
        
        adb.simple_delete(tbl ,{'id': str(id)})
        
    except googleapiclient.errors.HttpError:
        if DEF_VERBOSE_PRINT>0:
            print("Failed to delete event")
            print("Event deleted")

def insertCalEvents(service,market='USA'):
    getIPOsFH(service,market)
    getIPOsNQ(service,market)
    getEarnings(service,market)
    getDividends(service,market)

def getDividends(service,market='USA'):
    df,ird_avg=nq.getDividends()
    df['Date'] = pd.DatetimeIndex(df['payment_Date']) - timedelta(hours=9,minutes=30)
    i=0
    summary='Dividends: ' + str(len(df)) + ' firms are reporting today an average $' +str(round(ird_avg[2],2))+' per share'
    location='US Exchanges'
    description='Firms reported an average $' + str(round(ird_avg[2],2)) + ' or %' + str(round(ird_avg[1],4)) + ' per share'
    stz='America/New_York'
    sdate=tls.strtoLTimestamp(str(df['payment_Date'].iloc[0]),frmt='%m/%d/%Y')[1]
    edate=sdate
    etz=stz
    event=getEvent(summary,location,description,sdate,stz,edate,etz)
    q="select count(*) from IPOS where summary='" + str(summary) + "' and sdate='" + str(sdate) + "'"
    r=adb.getRs(q)
    print('IPOS',i,summary,location,description,sdate,stz,edate,etz,'')
    if r[0][0]==0:
        id=createEvent(service,event)
        insertEvent('IPOS',id,summary,location,description,sdate,stz,edate,etz,'')
    time.sleep(2) 

def getIPOsFH(service,market='USA'):
    def summaryFH(df):
        for i in range(len(df)):
            p=str(df.price.iloc[i]).split('-')
            df.price.iloc[i]=p[0]
        df['price'] = pd.to_numeric(df['price'],errors='coerce')
        i=0
        summary='IPO: ' + str(len(df)) + ' priced today for $' + str(round(df['totalSharesValue'].sum()/1000000,2)) + ' mln'
        location=df['exchange'].iloc[0]
        description=str(round(df['numberOfShares'].sum()/1000000,1)) + ' mln Shares at an average $' + str(df['price'].mean()) 
        stz='America/New_York'
        sdate=tls.strtoLTimestamp(str(df['date'].iloc[i]),frmt='%Y-%m-%d')[1]
        edate=sdate
        etz=stz
        event=getEvent(summary,location,description,sdate,stz,edate,etz)
        q="select count(*) from IPOS where summary='" + str(summary) + "' and sdate='" + str(sdate) + "'"
        r=adb.getRs(q)
        print('IPOS',i,summary,location,description,sdate,stz,edate,etz,'')
        if r[0][0]==0:
            id=createEvent(service,event)
            insertEvent('IPOS',id,summary,location,description,sdate,stz,edate,etz,'')
        time.sleep(2) 
    
    d = datetime.datetime.now().date()
    start = d.strftime("%Y-%m-%d")
    end = (datetime.datetime(d.year, d.month, d.day, 10)+timedelta(days=15)).strftime("%Y-%m-%d")
    df=fh.getIPOCalendar(start,end)
    df['Date'] = pd.DatetimeIndex(df['date']) - timedelta(hours=9,minutes=30)
    summaryFH(df.copy())
    
    for i in range(len(df)):
        summary='IPO: ' + df['symbol'].iloc[i] + ' - ' +df['name'].iloc[i]
        location=df['exchange'].iloc[i]
        description=str(df['numberOfShares'].iloc[i]) + ' Shares at USD ' + str(df['price'].iloc[i]) + '/' + str(df['totalSharesValue'].iloc[i])
        stz='America/New_York'
        sdate=tls.strtoLTimestamp(str(df['Date'].iloc[i]))[1]
        edate=sdate
        etz=stz
        event=getEvent(summary,location,description,sdate,stz,edate,etz)
        q="select count(*) from IPOS where summary='" + str(summary) + "' and sdate='" + str(sdate) + "'"
        r=adb.getRs(q)
        print('IPOS',i,summary,location,description,sdate,stz,edate,etz,'')
        if r[0][0]==0:
            id=createEvent(service,event)
            insertEvent('IPOS',id,summary,location,description,sdate,stz,edate,etz,'')
        time.sleep(2) 
        
def getIPOsNQ(service,market='USA'):
    
    def summaryNQ(df):
        df['proposedSharePrice'] = pd.to_numeric(df['proposedSharePrice'],errors='coerce')
        df["sharesOffered"] = df["sharesOffered"].replace("[,,]", "", regex=True)#.astype(float)
        df['sharesOffered'] = pd.to_numeric(df['sharesOffered'],errors='coerce')
        df["dollarValueOfSharesOffered"] = df["dollarValueOfSharesOffered"].replace("[$,]", "", regex=True)#.astype(float)
        df['dollarValueOfSharesOffered'] = pd.to_numeric(df['dollarValueOfSharesOffered'],errors='coerce')
        i=0
        summary='IPO: ' + str(len(df)) + ' priced today for $' + str(round(df['dollarValueOfSharesOffered'].sum()/1000000,2)) + ' mln'
        location=df['proposedExchange'].iloc[0]
        description=str(round(df['sharesOffered'].sum()/1000000,1)) + ' mln Shares at an average $' + str(df['proposedSharePrice'].mean()) 
        stz='America/New_York'
        sdate=tls.strtoLTimestamp(str(df['pricedDate'].iloc[i]),frmt='%m/%d/%Y')[1]
        edate=sdate
        etz=stz
        event=getEvent(summary,location,description,sdate,stz,edate,etz)
        q="select count(*) from IPOS where summary='" + str(summary) + "' and sdate='" + str(sdate) + "'"
        r=adb.getRs(q)
        print('IPOS',i,summary,location,description,sdate,stz,edate,etz,'')
        if r[0][0]==0:
            id=createEvent(service,event)
            insertEvent('IPOS',id,summary,location,description,sdate,stz,edate,etz,'')
        time.sleep(2) 
    
    df=nq.getIPOs()
    df['Date'] = pd.DatetimeIndex(df['pricedDate']) - timedelta(hours=9,minutes=30)
    summaryNQ(df.copy())
    
    for i in range(len(df)):
        summary='IPO: ' + df['proposedTickerSymbol'].iloc[i] + ' - ' +df['companyName'].iloc[i]
        location=df['proposedExchange'].iloc[i]
        description=str(df['sharesOffered'].iloc[i]) + ' Shares at USD ' + str(df['proposedSharePrice'].iloc[i]) + '/' + str(df['dollarValueOfSharesOffered'].iloc[i])
        stz='America/New_York'
        sdate=tls.strtoLTimestamp(str(df['pricedDate'].iloc[i]),frmt='%m/%d/%Y')[1]
        edate=sdate
        etz=stz
        event=getEvent(summary,location,description,sdate,stz,edate,etz)
        q="select count(*) from IPOS where summary='" + str(summary) + "' and sdate='" + str(sdate) + "'"
        r=adb.getRs(q)
        print('IPOS',i,summary,location,description,sdate,stz,edate,etz,'')
        if r[0][0]==0:
            id=createEvent(service,event)
            insertEvent('IPOS',id,summary,location,description,sdate,stz,edate,etz,'')
        time.sleep(2) 
        

def getEarnings(service,market='USA'):
    
    df,delta_avg=nq.getEarnings()
    d = datetime.datetime.now().date()
    df['pricedDate']=d
    
    df['Date'] = pd.DatetimeIndex(df['pricedDate']) - timedelta(hours=9,minutes=30)
    i=0
    summary='Earnings: ' + str(len(df)) + ' firms are reporting today - capweighted %: ' + str(round(delta_avg,4))
    location='US Exchanges'
    minmax='min/max %: ' + str(round(df.EPS_CapWeighted.min(),6)) +'/'+ str(round(df.EPS_CapWeighted.max(),6))
    description=str(len(df)) + ' firms reported an average ' + str(round(df.EPS_CapWeighted.mean(),6)) + ' capweighted inc/decr in earnings with '
    description=description+minmax
    stz='America/New_York'
    sdate=tls.strtoLTimestamp(str(df['pricedDate'].iloc[0]),frmt='%Y-%m-%d')[1]
    edate=sdate
    etz=stz
    event=getEvent(summary,location,description,sdate,stz,edate,etz)
    q="select count(*) from IPOS where summary='" + str(summary) + "' and sdate='" + str(sdate) + "'"
    r=adb.getRs(q)
    print('IPOS',i,summary,location,description,sdate,stz,edate,etz,'')
    if r[0][0]==0:
        id=createEvent(service,event)
        insertEvent('IPOS',id,summary,location,description,sdate,stz,edate,etz,'')
    time.sleep(2) 
      
def insertIPOs_old(service,market='USA'):
    df=tls.getIPOs()
    df['Date'] = pd.DatetimeIndex(df['Date']) + timedelta(hours=9,minutes=30)
    for i in range(len(df)-1):
        summary='IPO: ' + df['Name'].iloc[i]
        location=market
        description='Shares at USD ' + str(df['LPrice'].iloc[i]) + '/' + str(df['HPrice'].iloc[i])
        stz='America/New_York'
        sdate=tls.strtoLTimestamp(str(df['Date'].iloc[i]))[1]
        edate=sdate
        etz=stz
        event=getEvent(summary,location,description,sdate,stz,edate,etz)
        q="select count(*) from IPOS where summary='" + str(summary) + "' and sdate='" + str(sdate) + "'"
        r=adb.getRs(q)
        if r[0][0]==0:
            id=createEvent(service,event)
            insertEvent('IPOS',id,summary,location,description,sdate,stz,edate,etz,'')
        time.sleep(2) 
           
##############################################
# Datastructures for Google Calendar events
##############################################         
def getRepEvent(summary,location, description, sdate='2020-12-11T09:00:00-07:00',stz='America/Los_Angeles', edate='2020-12-11T09:00:00-07:00',etz='America/Los_Angeles', recurr='RRULE:FREQ=DAILY;COUNT=2',attendees=[]):
    event = {
      'summary': summary,
      'location': location,
      'description': description,
      'start': {
        'dateTime': sdate,
        'timeZone': stz,
      },
      'end': {
        'dateTime': edate,
        'timeZone': etz,
      },
      'recurrence': [
        recurr
      ],
      'attendees': [
        {'email': attendees[0]},
        {'email': attendees[1]},
      ],
      'reminders': {
        'useDefault': False,
        'overrides': [
          {'method': 'email', 'minutes': 24 * 60},
          {'method': 'popup', 'minutes': 10},
        ],
      },
    }
    return event

def getEvent(summary,location, description,sdate='2020-12-11T09:00:00-07:00',stz='America/Los_Angeles',edate='2020-12-11T09:00:00-07:00',etz='America/Los_Angeles'):
    event = {
      'summary': summary,
      'location': location,
      'description': description,
      'start': {
        'dateTime': sdate,
        'timeZone': stz,
      },
      'end': {
        'dateTime': edate,
        'timeZone': etz,
      },
      'reminders': {
        'useDefault': False,
        'overrides': [
          {'method': 'email', 'minutes': 24 * 60},
          {'method': 'popup', 'minutes': 10},
        ],
      },
    }
    return event
