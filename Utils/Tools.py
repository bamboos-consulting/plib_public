#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# Tools 
#
# Module including helper functions to download data
#############################################################################################      

from itertools import chain, cycle
from math import sqrt
from warnings import catch_warnings
from warnings import filterwarnings
from numpy import array
from ast import literal_eval
import itertools
import multiprocessing
from itertools import product
import time

import datetime
import pandas as pd
import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

DEBUG_PRINT=0

import Plib.Keystore as kst
ks=kst.Keystore()
ltz="Europe/Rome"
webdriver = module_path + ks.web_driver_path

#################################################################
# Parallelization Procedure
# Parameters:   N = total number of iterations
#
#               simfun_args, a Tuple of arguments
#               
#               simulationFunction(simfun_args,N), 
#               function returning a float array
#   
#               res_columns, columns for the float
#               array returned from simulationFunction                 
#################################################################    
def parallelExecution1(N,simfun_args,res_columns,simulationFunction,W=4):
    import multiprocessing as mp
    import numpy as np
    import pandas as pd
    import ctypes
    
    def CreateArray(n,m):
        mp_arr=mp.Array(ctypes.c_float,n*m)
        # mp_arr and arr share the same memory
        arr = np.frombuffer(mp_arr.get_obj(),ctypes.c_float)  
        # b and arr share the same memory
        b = arr.reshape((n, m))  
        return b
    
    
    def addData(srow,erow,array,simfunp, N):
        #simulationFunction has the signature (args,N)
        #where N is the number of iterations
        #The tuple of parameters is like
        #df=pd.DataFrame(); k=4; s='hello' --->  simfunp=df, k, s
        ret=simulationFunction(*simfunp, N)
        n,m=np.shape(array)
        row=0
        for nn in range(srow,erow+1):
            for mm in range(m):
                array[nn][mm]=ret[row][mm]
            row=row+1

    def getIndices(i,delta):
        s=int(i*delta)
        e=int(s+delta-1) 
        return s,e

    with mp.Manager() as manager:
        rows=N
        columns=len(res_columns)
        workers=W
        delta=int(rows/workers)
        
        if DEBUG_PRINT==2: 
            print('Launching parallelization with ' + str(N)+ ' iterations over '+str(workers)+' processes ('+str(delta)+' each)')

        Myarray=CreateArray(rows,columns)
        #simfun_args is a tuple of parameters
        #df=pd.DataFrame(); k=4; s='hello' --->  simfunp=df, k, s
        args = [(getIndices(i,delta)[0],getIndices(i,delta)[1],Myarray,simfun_args, delta) for i in range(workers)]
        processes = [mp.Process(target=addData, args=(args[i])) for i in range(workers)]

        for p in processes:
            p.start()

        for p in processes:
            p.join()
    
    return pd.DataFrame(Myarray,columns=res_columns)
    
def parallelExecution2(N,simfun_args,res_columns1,res_columns2,simulationFunction,W=4):
    import multiprocessing as mp
    import numpy as np
    import pandas as pd
    import ctypes
    
    def CreateArray(n,m):
        mp_arr=mp.Array(ctypes.c_float,n*m)
        arr = np.frombuffer(mp_arr.get_obj(),ctypes.c_float)  
        b = arr.reshape((n, m))  
        return b
    
    
    def addData(srow,erow,array1,array2,simfunp, N):
        ret1,ret2=simulationFunction(*simfunp, N)
        n,m=np.shape(array1)
        n2,m2=np.shape(array2)
        row=0
        for nn in range(srow,erow+1):
            for mm in range(m):
                array1[nn][mm]=ret1[row][mm]
            for mm2 in range(m2):
                array2[nn][mm2]=ret2[row][mm2]
            row=row+1
        
    def getIndices(i,delta):
        s=int(i*delta)
        e=int(s+delta-1) 
        return s,e

    with mp.Manager() as manager:
        rows=N
        columns1=len(res_columns1)
        columns2=len(res_columns2)
        workers=W
        delta=int(rows/workers)

        if DEBUG_PRINT==2: 
            print('Launching parallelization with ' + str(N)+ ' iterations over '+str(workers)+' processes ('+str(delta)+' each)')

        Myarray1=CreateArray(rows,columns1)
        Myarray2=CreateArray(rows,columns2)
        args = [(getIndices(i,delta)[0],getIndices(i,delta)[1],Myarray1,Myarray2,simfun_args, delta) for i in range(workers)]
        processes = [mp.Process(target=addData, args=(args[i])) for i in range(workers)]

        for p in processes:
            p.start()

        for p in processes:
            p.join()
    
    return pd.DataFrame(Myarray1,columns=res_columns1),pd.DataFrame(Myarray2,columns=res_columns2)
     
##############################################
# Import from csv, text, excel
##############################################    
def getCSV(fname,cols=['Date','Open','High','Low','Close','Adjusted_close','Volume'],idxs=[0,2,3,4,5,6,1],tz='US/Eastern'):
    
    df=pd.DataFrame()
    df_csv=pd.DataFrame()
    df_csv = pd.read_csv(fname,header=None,sep=',',index_col=False)
    df=df_csv.iloc[:,idxs]
    df['Open']=pd.to_numeric(df['Open'], errors='coerce')
    df['High']=pd.to_numeric(df['High'], errors='coerce')
    df['Low']=pd.to_numeric(df['Low'], errors='coerce')
    df['Close']=pd.to_numeric(df['Close'], errors='coerce')
    df['Adjusted_close']=pd.to_numeric(df['Adjusted_close'], errors='coerce')
    df['Volume']=pd.to_numeric(df['Volume'], errors='coerce')
    #Recover timezone information
    df['Date'] = pd.to_datetime(df['date'],utc=True)
    df['Date'] = df['Date'].astype('datetime64[ns]')
    df['Date'] = df.Date.dt.tz_localize('UTC').dt.tz_convert(tz)
    df['Date'] = df['Date'].astype('datetime64[ns]')
    df=df.set_index('Date')
    df.columns=caps(df.columns)
    df = df.sort_index(ascending=True)
    return df

def loadData(fname,sep=',',decimal='.',dformat='%d/%m/%Y', cols=['Date','Open','High','Low','Close','Adjusted_close','Volume']):
    df=pd.read_csv(fname,sep=sep, decimal=decimal)  
    df['Open'] = df['Open'].astype('float')
    df['High'] = df['High'].astype('float')
    df['Low'] = df['Low'].astype('float')
    df['Close'] = df['Close'].astype('float')
    df['Adjusted_close']=df['Adjusted_close'].astype('float')
    df['Volume']=df['Volume'].astype('float')
    df['Date'] = pd.to_datetime(df['Date'],format=dformat)
    df = df.set_index(pd.DatetimeIndex(df['Date']))
    df.index=df.index.tz_localize('America/New_York', nonexistent='shift_forward')
    del df['Date']
    df.columns=caps(df.columns)
    df = df.sort_index(ascending=True)
    return df

##############################################
# Plot all the columns
##############################################         
def multiplot(data,title=''):
    import matplotlib.pyplot as plt
    import seaborn as sns

    data.plot(lw=1.5, subplots=True, figsize=(16, 10), title=list(data.columns), layout=(3, 2), legend=False)
    plt.suptitle(title, fontsize=14)
    sns.despine()
    plt.tight_layout()
    plt.subplots_adjust(top=.95)

##############################################
# Close a HDF% file left open
##############################################         
def closeHDF(fpathname):
    import h5py
    import os
    os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"

    file= h5py.File(fpathname,'r')
    file.close()

##############################################
# Scale ranges and lists
##############################################         
def scale_number(unscaled, to_min, to_max, from_min, from_max):
    return (to_max-to_min)*(unscaled-from_min)/(from_max-from_min)+to_min

def scale_list(l, to_min, to_max):
    return [scale_number(i, to_min, to_max, min(l), max(l)) for i in l]

##############################################
# Update stubborn matplotlib params
##############################################         
def setMPL():
    import matplotlib.pyplot as plt
    SMALL_SIZE = 6
    MEDIUM_SIZE = 11
    BIGGER_SIZE = 13

    plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
    plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
    plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
    plt.rc('figure', titlesize=MEDIUM_SIZE)  # fontsize of the figure title
    plt.rcParams['grid.color'] = 'k'
    plt.rcParams['grid.linestyle'] = ':'
    plt.rcParams['grid.linewidth'] = 0.5
    plt.rcParams['axes.grid'] = True
    #Resolution
    plt.rcParams['figure.dpi'] = 80
    plt.rcParams['savefig.dpi'] = 100
    import pylab as plot
    params = {'legend.fontsize': 8,
              'legend.handlelength': 2}
    plot.rcParams.update(params)

##############################################
# Automation Helper functions
##############################################         
def getPath2OperaDrv():
    import platform
    
    mysys=platform.system()
    
    mypath = webdriver + str(mysys) + '/operadriver'
    if DEBUG_PRINT==2: 
        print(mypath)
    return mypath

##############################################
# Round to a specific multiple in the decimals
##############################################         
def pround(x, base=0.05):
    return base * round(x/base)

##############################################
# Error log to file
##############################################         
def ermsg(err_msg='description', log='frdata', path='//Volumes/Datafarm/frdata/logs'):
    sttime = datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S - ')
    error = "ERROR! {}".format(err_msg)
    log = (path + '/_err_'+ log).replace('//','/').replace('/Volumes','//Volumes')+'.txt'
    print(log)
    with open(log, 'a') as logfile:
        logfile.write(sttime + error + '\n')

##############################################
# Access pickle files
##############################################         
def rpkl(fname, path='./data/'):
    import pickle
    with open(path+fname, 'rb') as handle:
        df = pickle.load(handle) 
    return df

def wpkl(fname, df, path='./data/'):
    import pickle
    with open(path+fname, 'wb') as handle:
        pickle.dump(df, handle, protocol=pickle.HIGHEST_PROTOCOL)    

##############################################
# Export Dataframe as png
##############################################         
def exportDF(wls, png, path='./output/'):
    import dataframe_image as dfi
    
    try:
        import warnings
        warnings.simplefilter("ignore", ResourceWarning)
        dfi.export(wls,path+png)
    except:
        pass;
    return path+png
        
##############################################
# Highlight cells or rows in a dataframe
##############################################         
def highlight_row_(row,price=221,clr='yellow'):
    value = row.loc['price']
    if value == price:
        return pd.Series('background-color: yellow', row.index)
    else:
        return pd.Series('', row.index)

def highlight_cells_(val, price=5, clr='yellow'):
    color = clr if val == price else ''
    return 'background-color: {}'.format(color)
 
##############################################
# Add a row to DataFrame
##############################################          
def addRowDF(df, value=0, offset='1', freq='days'):
    #from datetime import timedelta
    from pandas import Timedelta
    last_date = df.iloc[[-1]].index
    last_date = last_date + Timedelta(offset + ' ' + freq)
    df=df.append(pd.DataFrame(index=last_date))
    df.iloc[-1]=value
    return df
    
##############################################
# Index a dataframe with US. business days
##############################################         
def getUSIndexedDF(mdf=[]):
    import pandas as pd
    
    sdate=str(mdf.index.min().strftime("%Y-%m-%d"))
    edate=str(mdf.index.max().strftime("%Y-%m-%d"))
    
    dfi=pd.date_range(start=sdate,end=edate, freq='B')
    df=pd.DataFrame(dfi,columns=['Date'])
    df=df.set_index('Date')
    df = df.sort_index(ascending=True)
    df=pd.merge(mdf,df, left_index=True, right_index=True)
    return df

##############################################
# Merge two dataframes of different frequency
# Data is propagated forward
# i.e., merging trading days with calendar
# days will replace non-trading days with
# the values of the last trading days
# sunday=saturday=friday
##############################################         
def mergeDFHigherFreq(dfhf,dflf,freq='D'):
        #B business day frequency
        #BH business hour frequency
        #H hourly frequency
        #T minutely frequency
        #S secondly frequency
        #U, microseconds - tick by tick
        temp=dflf.set_index(dflf.index.shift(0, freq=freq)).resample(freq).ffill()
        return pd.merge(dfhf,temp,left_index=True, right_index=True).ffill().bfill()
    
##############################################
# Manage notifications of trading orders
##############################################         
def send(pair, order, stop_order):
    #Replace token, chat_id & text variables
    text = f'A new trade has been placed in {pair} at {order.lmitPrice} with a stop at {stop_order.auxPrice}'
    token = 'xxx'
    
    params = {'chat_id': xxx, 'text': text, 'parse_mode': 'HTML'}
    resp = requests.post('https://api.telegram.org/bot{}/sendMessage'.format(token), params)
    resp.raise_for_status()

    send('EURUSD', order, stop_order)

##############################################
# Functions to manage dates and timestamps
##############################################
def freqMin(s,c='min'):
    if c=='min':
        if s=='1T':i=1
        if s=='2T':i=2
        if s=='3T':i=3
        if s=='5T':i=5
        if s=='10T':i=10
        if s=='20T':i=20
        if s=='30T':i=30
        if s=='60T':i=60
        if s=='H':i=60
        if s=='2H':i=120
        if s=='3H':i=180
        if s=='4H':i=240
        if s=='D':i=1440
        if s=='W':i=1440*7
        if s=='M':i=1440*7*30
    elif c=='hours':
        if s=='1T':i=1/60
        if s=='2T':i=1/30
        if s=='3T':i=1/20
        if s=='5T':i=1/12
        if s=='10T':i=1/6
        if s=='20T':i=1/3
        if s=='30T':i=1/2
        if s=='60T':i=1
        if s=='H':i=1
        if s=='2H':i=2
        if s=='3H':i=3
        if s=='4H':i=4
        if s=='D':i=24
        if s=='W':i=24*7
        if s=='M':i=24*7*30
    return i

def getDatesYTD():
    import datetime
    from datetime import date
    d2 = pd.to_datetime(date.today())
    d1 = datetime.datetime.strptime(str(date.today().year)+'-01-01', "%Y-%m-%d")
    return str(d2),str(d1)[:10], abs((d2 - d1).days)
    
def getTDelta(back,freq):
    
    tdelta=datetime.timedelta(minutes=1)
    if freq=='H':tdelta=datetime.timedelta(hours=back)
    if freq=='D':tdelta=datetime.timedelta(days=back)
    if freq=='W':tdelta=datetime.timedelta(weeks=back)
    if freq=='M':tdelta=datetime.timedelta(months=back)
    if freq.find('T')>0: tdelta=datetime.timedelta(minutes=back*int(freq.replace('T','')))
    if freq.find('H')>0: tdelta=datetime.timedelta(hours=back*int(freq.replace('H','')))
    if freq.find('D')>0: tdelta=datetime.timedelta(days=back*int(freq.replace('D','')))
    if freq.find('W')>0: tdelta=datetime.timedelta(weeks=back*int(freq.replace('W','')))
    if freq.find('M')>0: tdelta=datetime.timedelta(months=back*int(freq.replace('M','')))
    return tdelta

def getDateFromTs(ts,dt_type=0):
    dt_object = datetime.datetime.fromtimestamp(ts)
    if dt_type==0:
        ret=dt_object.strftime('%Y-%m-%d')
    if dt_type==1:    
        ret=dt_object.strftime('%Y-%m-%d, %H:%M:%S')
    if dt_type==2:    
        ret=dt_object
    return ret

def getTsFromSDate(mdate,ts_type=0):
    a=mdate.split('-')
    dt = datetime.datetime(int(a[0]), int(a[1]), int(a[2]))
    if ts_type==0:
        timestamp=datetime.datetime.timestamp(dt)
    if ts_type==1:
        timestamp = dt.replace(tzinfo=timezone.utc).timestamp()
    return int(timestamp)

def getLTimestamp(tz=ltz):
    import datetime
    import pytz
    r=datetime.datetime.utcnow()
    d = pytz.timezone(tz).localize(r)
    return datetime.datetime.timestamp(d),d.isoformat()

def getTimestamp():
    import datetime
    import pytz
    d=datetime.datetime.utcnow()
    return datetime.datetime.timestamp(d),d.isoformat()

def strtoLTimestamp(any_datetime,remote_timezone='America/New_York',frmt='%Y-%m-%d %H:%M:%S'):
    import datetime
    import pytz
    
    mydt=datetime.datetime.strptime(any_datetime, frmt)

    r=mydt.astimezone(pytz.timezone(remote_timezone))
    return str(datetime.datetime.timestamp(r)), str(r.isoformat())

def toLTimestamp(any_datetime,remote_timezone='America/New_York'):
    import datetime
    import pytz
    r=any_datetime.astimezone(pytz.timezone(remote_timezone))
    return str(datetime.datetime.timestamp(r)), str(r.isoformat())

def toIsoDt(ts,tz="America/New_York"):
    import datetime
    import pytz
    return datetime.datetime.fromtimestamp(ts).astimezone(pytz.timezone(tz)).isoformat()

def epochtoIsoDt(ts,tz="America/New_York"):
    import datetime
    import pytz
    return datetime.datetime.fromtimestamp(ts/1000).astimezone(pytz.timezone(tz)).isoformat()

def epochtoIsoDt2(ts):
    import datetime
    return datetime.datetime.fromtimestamp(ts/1000).isoformat()
    
def epochToDt(n):
    import datetime
    
    a=datetime.datetime.fromtimestamp(n/1000)
    return datetime.datetime.strptime(a.isoformat(), "%Y-%m-%dT%H:%M:%S")
    
def syncTime(server='ntp1.lsu.edu'):
    import subprocess
    command = 'sntp -sS ' + server
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=None, shell=True)
    #Launch the shell command:
    output = process.communicate()
    if DEBUG_PRINT>2:
        print(output[0])
    return output[0]    

def getDtm2(sdate1,sdate2):
    #two strings
    from datetime import datetime
    date_format = '%Y-%m-%d'
    delta = datetime.strptime(d1, date_format)-datetime.strptime(d2, date_format)
    return int(delta.days)/365

def getDtm(d1,d2):
    #two datetime
    delta = d1-d2
    return int(delta.days)/365


##############################################
# Resample to weekday offset
##############################################      
def aggregate(daily_df, frequency):
    daily_df.reset_index(inplace=True)
    daily_df['days'] = 1
    weekly_df = daily_df.resample(frequency, on='Date').agg({'Open':'first','High':'max', 'Low':'min','Close':'last','Volume':'sum','days':'count'})
    return weekly_df

##############################################
# Extract Expiration date from iFuture data
##############################################      
def getFutMonthsExp(df,expf=None,tz='America/New_York'):
    from datetime import datetime
    import pandas as pd
    
    #df['Month']=df.index.get_level_values(1)
    #df['Year']=df.index.get_level_values(2)
    months={'F':'January',
    'G':'February',
    'H':'March',
    'J':'April',
    'K':'May',
    'M':'June',
    'N':'July',
    'Q':'August',
    'U':'September',
    'V':'October',
    'X':'November',
    'Z':'December'}
    df['dte']=''
    df['dte']=df['Month'].map(months) 
    df['Month'] = df['Month'].astype('string')
    df['Year'] = df['Year'].astype('string')
    df['Dte']=df['Year']+ '-' + df['dte'] + '-' + '01' + ' 00:00:00'
    
    try:
        df['Expiration']=pd.to_datetime(df.Dte, format='%Y-%B-%d %H:%M:%S', yearfirst=True)
    except:
        pass;
    try:
        df['Expiration']=pd.to_datetime(df.Dte, format='%y-%B-%d %H:%M:%S', yearfirst=True)
    except:
        pass;
    df['Expiration2']=df['Expiration'].dt.strftime('%Y-%m-%d %H:%M:%S')
    df['Expiration']=df['Expiration2'].apply(expf)
    df['Expiration']=pd.to_datetime(df.Expiration, format='%Y-%m-%d', yearfirst=True)
    df['Date2']=pd.DatetimeIndex(df.index).normalize().date
    df['Expiration']=df['Expiration'].dt.date
    df['Dte']=(df['Expiration']-df['Date2']).dt.days

    del df['dte']
    del df['Date2']
    del df['Expiration2']
    #del df['Month']
    #del df['Year']
    return df

##########################################################
# Offset n bus_days From start day of month in date_ts
##########################################################      
def getNhBD(date_ts,start='15',bus_days=0,hformat=True,month_shift=0):
    from pandas.tseries.offsets import BMonthEnd,BMonthBegin
    from pandas.tseries.offsets import BusinessDay, Day
    from dateutil.relativedelta import relativedelta
    from datetime import datetime
    
    if hformat:
        strf="%Y-%m-%d %H:%M:%S"
    else:
        strf="%Y-%m-%d"
    date_ts=datetime.fromisoformat(date_ts)
    retv=date_ts
    if bus_days > 0:
        offset=BusinessDay(bus_days-1)
    elif bus_days < 0:
        offset=BusinessDay(bus_days+1)
    else:
        offset=BusinessDay(bus_days)
    if start=='last':
        refDate = BMonthEnd()
        retv=refDate.rollforward(date_ts)+offset
    elif start=='first':
        refDate = BMonthEnd()
        retv=refDate.rollforward(date_ts+ relativedelta(months=-1+month_shift))+BusinessDay(1)+offset
    else:
        refDate = BMonthEnd()
        retv=refDate.rollforward(date_ts+ relativedelta(months=-1+month_shift))+Day(1+int(start)-1)+offset
    
    return retv.strftime(strf)

#################################################################
# Offset n bus_days from the K-th weekday of the month in date_ts
#################################################################      
def getNthWd(date_ts,wd='wednesday',nth=3,bus_days=0,hformat=True,month_shift=0):
    from datetime import date,datetime
    from dateutil.relativedelta import relativedelta, FR, WE
    from pandas.tseries.offsets import BusinessDay
    
    if hformat:
        strf="%Y-%m-%d %H:%M:%S"
    else:
        strf="%Y-%m-%d"
    offset=BusinessDay(bus_days)
    date_ts=datetime.fromisoformat(date_ts)
    if wd=='wednesday':
        retv=(date_ts + relativedelta(day=1, weekday=WE(nth),months=month_shift) + offset).strftime(strf)
    elif wd=='friday':
        retv=(date_ts + relativedelta(day=1, weekday=FR(nth),months=month_shift) + offset).strftime(strf)
    return retv

##########################################################
# Option expiration
##########################################################      
def optionExp(date_ts,hformat=True):
    from datetime import datetime
    import calendar
    
    date_ts=datetime.fromisoformat(date_ts)
    day = 21 - (calendar.weekday(date_ts.year, date_ts.month, 1) + 2) % 7
    if hformat:
        strf="%Y-%m-%d %H:%M:%S"
    else:
        strf="%Y-%m-%d"
    return datetime(date_ts.year, date_ts.month, day).strftime(strf)

################################################################
# Display Dataframes side by side
#display_side_by_side(df1,df2,df1, titles=['Foo','Foo Bar'])
################################################################
def display_side_by_side(*args,titles=cycle([''])):
    from IPython.display import display_html
    
    html_str=''
    for df,title in zip(args, chain(titles,cycle(['</br>'])) ):
        html_str+='<th style="text-align:center"><td style="vertical-align:top">'
        html_str+=f'<h2>{title}</h2>'
        html_str+=df.to_html().replace('table','table style="display:inline"')
        html_str+='</td></th>'
    display_html(html_str,raw=True)
        
def getType(stype,otype=0):
    #change option type description
    ret=''
    if otype==0:
        if stype=='call':
            ret='c'
        if stype=='put':
            ret='p'
    if otype==1:
        if stype=='call':
            ret='CALL'
        if stype=='put':
            ret='PUT'
    return ret
   
##############################################
# Download excel file and import in dataframe
##############################################
def getExcelFromUrl(url,excel_sheet,first_row,reqd=False,save_dir='//Users/rob',myname='temp'):
    import datetime
    mdate=str(datetime.date.today()).replace('-','')
    myname=myname+mdate
    
    def download(url,save_dir,fname='temp.zip',attempts=1):
        import requests
        import os, sys, os.path
        
        while attempts > 0:
            #resume_headers = {'Range':'bytes=0-2000000'}
            r = requests.get(url, stream=True)#, headers=resume_header)
            try:
                total_length = r.headers.get('content-length')
                if total_length is None: total_length=1
                dl = 0
                total_length = int(total_length)
                done=0
                with open(save_dir+'/'+fname,'wb') as f:
                    for chunk in r.iter_content(chunk_size=1024*1024):
                        dl += len(chunk)
                        f.write(chunk)
                        done = int(50 * dl / total_length)
                        sys.stdout.write("\r[%s%s]" % ('=' * done, ' ' * (50-done)) )    
                        sys.stdout.flush()
                    print("Download complete.")
                    attempts=attempts-1
            except Exception as ex:
                print('Error while downloading')
                attempts=attempts-1
                
    if not reqd:
        df = pd.read_excel(url,
                       sheet_name=excel_sheet,
                       skiprows=range(int(first_row)-1),
                       skipfooter=0, engine='openpyxl')
    else:
        download(url,save_dir,fname=myname+'.xls',attempts=1)
        df = pd.read_excel(save_dir+'/'+myname+'.xls',
                       sheet_name=excel_sheet,
                       skiprows=range(int(first_row)-1),
                       skipfooter=0)
    return df

##############################################
# Open a Webpage in Opera
##############################################    
def openWebPage(url='',wait=30):
    import time
    from selenium import webdriver
    from selenium.webdriver.chrome import service
    from datetime import datetime
    from selenium.webdriver.common.by import By
    import pandas as pd

    # chmod 0755 operadriver
    path2operad=getPath2OperaDrv()
    webdriver_service = service.Service(path2operad)
    webdriver_service.start()
    driver = webdriver.Remote(webdriver_service.service_url, webdriver.DesiredCapabilities.OPERA)

    driver.get(url)
    input_txt = driver.page_source
    time.sleep(wait) #see the result
    driver.quit()  

##############################################
# Fill NaN in DataFrame
##############################################    
def fill_missing(df,how='linear',max_na_inseq=5):
    #{‘linear’, ‘time’, ‘index’, ‘values’, ‘nearest’, ‘zero’, ‘slinear’, 
    #‘quadratic’, ‘cubic’, ‘barycentric’, ‘krogh’, ‘polynomial’, ‘spline’, 
    #‘piecewise_polynomial’,‘from_derivatives’, ‘pchip’, ‘akima’}
    res = df.interpolate(method=how,limit=max_na_inseq)\
    .fillna(method='ffill')\
    .fillna(method='bfill')
    return res

##############################################
# Capitalize list elements
##############################################       
def caps(dl):
    ret=[]
    ret=[x.lower().capitalize() for x in dl]
    return ret
    
##############################################
# Obtain dates for plots
##############################################      
def getDates(offset=-180,today_date=''):
    from dateutil import parser
    from time import gmtime, strftime
    from datetime import timedelta, date
    
    if today_date=='':
        today_date=strftime("%Y-%m-%d %H:%M:%S", gmtime())
    dt_start = (parser.parse(today_date) + timedelta(days=offset)).strftime("%Y-%m-%d")
    #dt_start2 = (parser.parse(today_date) + timedelta(days=-180)).strftime("%Y-%m-%d")
    dt_end = (parser.parse(today_date) + timedelta(days=-1)).strftime("%Y-%m-%d")
    return dt_start,dt_end

##############################################
# Select nearest date in dataframe
##############################################       
def nearestDate(df, mdate):
    import datetime
    
    d1=mdate.split('-')
    return df.index.get_loc(datetime.datetime(int(d1[0]),int(d1[1]),int(d1[2])),method='nearest')
    
##############################################
# Search google for ticker by name
##############################################       
def getTickerByGoogleSearch(search='Vanguard 500 Index Admiral'):
    import time
    from selenium import webdriver
    from selenium.webdriver.chrome import service
    from datetime import datetime
    from selenium.webdriver.common.by import By
    import pandas as pd

    # chmod 0755 operadriver
    path2operad=getPath2OperaDrv()
    wait=1
    webdriver_service = service.Service(path2operad)
    webdriver_service.start()
    driver = webdriver.Remote(webdriver_service.service_url, webdriver.DesiredCapabilities.OPERA)

    r=search.split(' ')
    s=''
    for x in range(len(r)):
        s=s + (r[x]) + '+'
    #token_search='Vanguard+500+Index+Admiral'
    driver.get('https://www.google.com/search?&q=' + s)
    input_txt = driver.page_source
    time.sleep(wait) #see the result

    Cols=['Date', 'Debit in Margin Accounts', 'Free Credit in Margin Accounts', 'Free Credit in Debit Accounts']
    df=pd.DataFrame(columns=Cols)

    table = driver.find_elements_by_xpath("//div[@id='knowledge-finance-wholepage__entity-summary']/div/g-card-section/div")
    myrow=table[0].text.split('\n')
    r=myrow[1].split(' ')
    symbol=str(r[1])
    return symbol

##############################################
# Get IPOs calendar
##############################################          
def getIPOs():
    import time
    from selenium import webdriver
    from selenium.webdriver.chrome import service
    from datetime import datetime
    from selenium.webdriver.common.by import By
    import pandas as pd

    # chmod 0755 operadriver
    path2operad=getPath2OperaDrv()

    webdriver_service = service.Service(path2operad)
    webdriver_service.start()
    driver = webdriver.Remote(webdriver_service.service_url, webdriver.DesiredCapabilities.OPERA)

    driver.get('https://www.iposcoop.com/ipo-calendar/')
    input_txt = driver.page_source
    time.sleep(2) #see the result

    #Test in browser console
    #$x("//table[@class='standard-table ipolist']/tbody/tr")    
    table = driver.find_elements_by_xpath("//table[@class='standard-table ipolist']/tbody/tr")
    Cols=['Date','Name', 'LPrice', 'HPrice','Shares(mln)', 'Vol(mln)']
    df=pd.DataFrame(columns=Cols)
    s=set(['Priced','Sunday','Monday','Thursday','Wednesday','Tuesday','Friday','Saturday'])
    for r in range(0,len(table)):
        myrow=table[r].text.split('\n')
        offset=0
        #print(myrow)
        test=str(myrow[0]).split(' ')
        if test[len(test)-3] not in s: offset=-1
        idate=test[len(test)-4+offset]
        ivol=test[len(test)-6+offset]
        ihpr=test[len(test)-8+offset]
        ilpr=test[len(test)-9+offset]
        ishar=test[len(test)-10+offset]        
        ilead=test[len(test)-11+offset]
        test=str(myrow[0]).split(test[len(test)-11+offset])
        iname=test[0] + ' ' + ilead
        df.loc[r] = [datetime.strptime(idate,'%m/%d/%Y'),str(iname),float(ilpr),float(ihpr),float(ishar),float(ivol)]
    
    driver.quit()
    return df
