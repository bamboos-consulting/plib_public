#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# crontabIPOs 
#
# Module for execution through crontab
#
# basedir = path to the Plib installation es. /Users/rob/Desktop/PythonCode
# 
# line to be added in crontab with crontab -e
#
# cd basedir/Plib/ && python ./Utils/scheduled/ipo_cron.py
#
#############################################################################################      

#!/usr/bin/env python3

import sys
import os
module_path = os.path.abspath(os.path.join('..'))
print(module_path)
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import Plib.Utils.Calendar as cl

gservice=cl.googleAuth()
cl.insertCalEvents(gservice)