#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# reports_hourly 
#
# Module for execution through crontab
#
# basedir = path to the Plib installation es. /Users/rob/Desktop/PythonCode
# 
# line to be added in crontab with crontab -e
#
# cd basedir/Plib/ && python -W ignore ./Utils/scheduled/reports_hourly.py
#
#############################################################################################      

#!/usr/bin/env python3

import sys
import os
import shutil
import datetime
    
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

import Plib.Utils.scheduled.env_variables
#python ./Plib/Utils/scheduled/env_variables.py

#############################################################
#       LUNCH THE REPORT                                    #
#############################################################
rname = '"Report Asset Classes.ipynb"'
os.environ['RAC_REFRESH']='False'
os.system("jupyter nbconvert --output-dir="+module_path+"/Plib/Examples/output" + " --execute --to html --no-input "+module_path+"/Plib/Examples/"+rname)

#############################################################
#       EXPORT THE REPORT TO USERS                          #
#############################################################
copy_path = '//Volumes/GoogleDrive/My Drive/Condivisi/'
mdate = str(datetime.date.today()).replace('-','')
mdate=''
fname ='Report Asset Classes.html'
fname2 ='ReportAssetClasses'+ mdate +'.html'
shutil.copyfile(module_path+"/Plib/Examples/output/"+fname, copy_path+fname2)

#############################################################
#       LUNCH THE REPORT                                    #
#############################################################
rname = '"Report Market Timing.ipynb"'
os.environ['RMT_DWNLD']='False'
os.system("jupyter nbconvert --output-dir="+module_path+"/Plib/Examples/output" + " --execute --to html --no-input "+module_path+"/Plib/Examples/"+rname)

#############################################################
#       EXPORT THE REPORT TO USERS                          #
#############################################################
copy_path = '//Volumes/GoogleDrive/My Drive/Condivisi/'
mdate = str(datetime.date.today()).replace('-','')
mdate=''
fname ='Report Market Timing.html'
fname2 ='ReportMarketTiming'+ mdate +'.html'
shutil.copyfile(module_path+"/Plib/Examples/output/"+fname, copy_path+fname2)

