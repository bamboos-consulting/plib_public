#############################################################################################
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Plib: Quantitative Research and Trading Library
# https://bitbucket.org/bamboos-consulting/plib/src/master/
#
# Copyright 2018-2022 Roberto Garrone
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#############################################################################################
# DbAccess 
#
# Module comprising helper functions to record transactions
#############################################################################################      


#Common Libraries
import numpy as np
import pandas as pd
import sqlite3
import sys
import os
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path + '/')

#import warnings
#warnings.filterwarnings("once")
DEF_VERBOSE_PRINT=0

db_f=''
bcli=0
memdb=[]

def setParams(db,cli,in_mem=False):
    global db_f
    global bcli
    db_f=db
    bcli=cli
    
    if in_mem:
        global memdb
        source = sqlite3.connect(db)
        dest = sqlite3.connect('file::memory:?cache=shared', uri=True)
        source.backup(dest)
        memdb=dest
           
def cn(db):
    try:
        if memdb == []:
            sqliteConnection = sqlite3.connect(db)
            if (DEF_VERBOSE_PRINT==1):
                cursor = sqliteConnection.cursor()
                print('Database Successfully Connected (or created)')
                sqlite_select_Query = 'select sqlite_version();'
                cursor.execute(sqlite_select_Query)
                record = cursor.fetchall()
                print('SQLite Database Version is: ', record)
                cursor.close()
        else:
            sqliteConnection = sqlite3.connect("file::memory:?cache=shared", uri=True)
    except sqlite3.Error as error:
        if (DEF_VERBOSE_PRINT==1):
            print('Error while connecting to sqlite', error)
    return sqliteConnection

def c_cn(conn):
    try:
        conn.close()
    except sqlite3.Error as error:
        if DEF_VERBOSE_PRINT==1:
            print('Error while closing connection to sqlite', error)     

def createUDF():
    import numpy as np
    
    global db_f
    global bcli
    
    def nlog(n):
        return np.log(n)
        
    conn=cn(db_f)
    cursor = conn.cursor()
    conn.create_function("log", 1, nlog)
    
    #Test
    #cursor = conn.cursor()
    #cursor.execute("select log(?)", (11,))
    c_cn(conn)
    
def getDf(q):
    global db_f
    global bcli
    
    conn=cn(db_f)
    sql = q + ';'
    df = pd.read_sql_query(sql, conn)
    c_cn(conn)
    return df
                     
def getRs(q):
    global db_f
    global bcli
    
    conn=cn(db_f)
    cursor = conn.cursor()
    sql = q + ';'
    cursor.execute(sql)
    rs = cursor.fetchall()
    cursor.close()
    c_cn(conn)
    return rs

def simple_delete(table_name, row):
    global db_f
    global bcli
    
    sqliteConnection=cn(db_f)
    sql = "DELETE FROM {0} WHERE {1}=? and cli={2}".format(table_name, list(row)[-1],bcli)    
    if DEF_VERBOSE_PRINT>0:
        print(sql)
    sqliteConnection.cursor().execute(sql,tuple(row.values()))
    sqliteConnection.commit()
    c_cn(sqliteConnection)
    
def simple_update(table_name, row,add_filter=''):
    global db_f
    global bcli
    
    sqliteConnection=cn(db_f)
    cols = '=?, '.join('{}'.format(col) for col in row.keys()).replace(', '+list(row)[-1],'')
    sql = "UPDATE {0} SET {1} WHERE {2}=? and cli={3}".format(table_name, cols, list(row)[-1],bcli)    
    sql=sql + ' ' + add_filter
    if DEF_VERBOSE_PRINT>0:
        print(sql)
    sqliteConnection.cursor().execute(sql,tuple(row.values()))
    sqliteConnection.commit()
    c_cn(sqliteConnection)

def simple_update2(table_name, row):
    global db_f
    global bcli
    
    sqliteConnection=cn(db_f)
    cols = '=?, '.join('{}'.format(col) for col in row.keys()).replace(', '+list(row)[-1],'')
    sql = "UPDATE {0} SET {1} WHERE {2}=? and cli=0".format(table_name, cols, list(row)[-1])    
    if DEF_VERBOSE_PRINT>0:
        print(sql)
    sqliteConnection.cursor().execute(sql,tuple(row.values()))
    sqliteConnection.commit()
    c_cn(sqliteConnection)
    
def simple_insert(table_name, row):
    global db_f
    global bcli
    
    sqliteConnection=cn(db_f)
    cols = ', '.join('"{}"'.format(col) for col in row.keys())
    vals = ', '.join(':{}'.format(col) for col in row.keys())
    sql = 'INSERT INTO "{0}" ({1}) VALUES ({2})'.format(table_name, cols, vals)
    if DEF_VERBOSE_PRINT>0:
        print(sql)
    sqliteConnection.cursor().execute(sql, row)
    sqliteConnection.commit()
    c_cn(sqliteConnection)

def dataframe_insert(table_name,df):
    global db_f
    global bcli
    
    sqliteConnection=cn(db_f)
    tuples = list(df.itertuples(index=False, name=None))
    columns_list = df.columns.tolist()
    marks = ['?' for _ in columns_list]
    columns_list = f'({(",".join(columns_list))})'
    marks = f'({(",".join(marks))})'
    #table_name = 'whateveryouwant'
    sqliteConnection.cursor().executemany(f'INSERT OR REPLACE INTO {table_name}{columns_list} VALUES {marks}', tuples)
    sqliteConnection.commit()
    c_cn(sqliteConnection)
